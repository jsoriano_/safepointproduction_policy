﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

	<xsl:import href="PolicyTermExtendedData.xsl"/>
	<xsl:import href="BillingTransactionCommon.xsl"/>
	<xsl:import href="BORFinancial_To_BillingPostPolicy.xsl"/>
	<xsl:output omit-xml-declaration="yes"/>

	<xsl:template match="/*">
		<Billing.postPolicyRq>
			<PolicyData>
				<xsl:apply-templates select="session/data/policy"/>
				<CurrencyCulture>
					<xsl:value-of select="session/properties/@cultureCode"/>
				</CurrencyCulture>
				<PolicyCurrencyCode>
					<xsl:value-of select="session/properties/@currencyCode"/>
				</PolicyCurrencyCode>
				<BillItemExtendedData/>
			</PolicyData>
		</Billing.postPolicyRq>
	</xsl:template>

	<xsl:template match="policy">
		<AccountId>
			<xsl:value-of select="AccountId"/>
		</AccountId>
		<PolicyTermId>
			<xsl:value-of select="PolicyTermId"/>
		</PolicyTermId>
		<PolicyLineOfBusinessCode>
			<xsl:value-of select="LineOfBusinessCode"/>
		</PolicyLineOfBusinessCode>
		<PolicyProductCode>
			<xsl:value-of select="ProductCode"/>
		</PolicyProductCode>
		<PolicyIssueSystemCode>
			<xsl:value-of select="IssueSystem"/>
		</PolicyIssueSystemCode>
		<PolicyMasterCompanyCode>
			<xsl:value-of select="MasterCompany"/>
		</PolicyMasterCompanyCode>
		<PolicyReference>
			<xsl:value-of select="PolicyNumber"/>
		</PolicyReference>
		<TransactionReference>
			<xsl:value-of select="../policyAdmin/transactions/transaction[last()]/HistoryID"/>
		</TransactionReference>
		<ReasonTypeCode>
			<xsl:call-template name="LookupReasonTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="//report/entry[@type = 'onset'][1]/transaction/reasons/reason[1]/Code"/>
				</xsl:with-param>
			</xsl:call-template>
		</ReasonTypeCode>
		<Auditable>
			<xsl:value-of select="Indicators[Type='IsAuditable']/bValue"/>
		</Auditable>
		<xsl:variable name="V.TransactionEffDate" select="//report/entry[@type = 'onset'][1]/transaction/EffectiveDate"/>
		<TransactionEffectiveDate>
			<xsl:value-of select="$V.TransactionEffDate"/>
		</TransactionEffectiveDate>
		<xsl:call-template name="LookupPolicyTermTrans">
			<xsl:with-param name="EffectiveDate" select="$V.TransactionEffDate"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="LookupPolicyTermEffectiveDate">
		<xsl:param name="EffectiveDate"/>
		<xsl:variable name ="termCreationTransactions" select="(/*/_processingData/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Reissue' or Type = 'Rewrite'])" />
		<xsl:variable name="packedEffectiveDate" select="number(translate($EffectiveDate,'-',''))" />		
		<xsl:variable name="creationPolicyTermTransaction">
			<xsl:choose>
				<xsl:when test="//report/entry[@type = 'onset'][1]/@transactionType = 'FinalAudit' or //report/entry[@type = 'onset'][1]/@transactionType = 'RevisedFinalAudit' or //report/entry[@type = 'onset'][1]/@transactionType = 'VoidFinalAudit'">
					<xsl:copy-of select="msxsl:node-set($termCreationTransactions)[number(translate(EffectiveDate,'-','')) &lt;= $packedEffectiveDate][number(translate(ExpirationDate,'-','')) &gt;= $packedEffectiveDate][not(HistoryIDOriginal)]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="msxsl:node-set($termCreationTransactions)[number(translate(EffectiveDate,'-','')) &lt;= $packedEffectiveDate][number(translate(ExpirationDate,'-','')) &gt; $packedEffectiveDate][not(HistoryIDOriginal)]" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:copy-of select="msxsl:node-set($creationPolicyTermTransaction)/transaction/EffectiveDate" />
	</xsl:template>
	<xsl:template name="LookupPolicyTermTrans">
		<xsl:param name="EffectiveDate"/>
		<xsl:variable name ="termCreationTransactions" select="(/*/session/data/policyAdmin/transactions/transaction[Type = 'New' or Type = 'Renew' or Type = 'Reissue' or Type = 'Rewrite'])" />
		<xsl:variable name="packedEffectiveDate" select="number(translate($EffectiveDate,'-',''))" />
		<!--<xsl:variable name="creationPolicyTermTransaction"  select="$termCreationTransactions[number(translate(EffectiveDate,'-','')) &lt;= $packedEffectiveDate][number(translate(ExpirationDate,'-','')) &gt;= $packedEffectiveDate]" />-->
		<xsl:variable name="transGroup" select="//report/entry[@type = 'onset'][1]/@transGroup" />
		<xsl:variable name="transType" select="//report/entry[@type = 'onset'][1]/@transactionType" />
		<xsl:variable name="creationPolicyTermTransaction">
			<xsl:choose>
				<xsl:when test="$transType = 'FinalAudit' or $transType = 'RevisedFinalAudit' or $transType = 'VoidFinalAudit' or $transType = 'Endorse'">
					<xsl:copy-of select="msxsl:node-set($termCreationTransactions)[number(translate(EffectiveDate,'-','')) &lt;= $packedEffectiveDate][number(translate(ExpirationDate,'-','')) &gt;= $packedEffectiveDate]" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="msxsl:node-set($termCreationTransactions)[number(translate(EffectiveDate,'-','')) &lt;= $packedEffectiveDate][number(translate(ExpirationDate,'-','')) &gt; $packedEffectiveDate]" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>


		<PolicyTermEffectiveDate>
			<xsl:value-of select="msxsl:node-set($creationPolicyTermTransaction)/transaction/EffectiveDate" />
		</PolicyTermEffectiveDate>
		<PolicyTermExpirationDate>
			<xsl:value-of select="msxsl:node-set($creationPolicyTermTransaction)/transaction/ExpirationDate" />
		</PolicyTermExpirationDate>
		<TransactionTypeCode>
			<xsl:call-template name="LookupTransactionTypeCode">
				<xsl:with-param name="code">
					<xsl:value-of select="//report/entry[@type = 'onset'][1]/transaction/Type"/>
				</xsl:with-param>
				<xsl:with-param name="termEffectiveDate">
					<xsl:value-of select="msxsl:node-set($creationPolicyTermTransaction)/transaction/EffectiveDate"/>
				</xsl:with-param>
				<xsl:with-param name="effectiveDate">
					<xsl:value-of select="$EffectiveDate"/>
				</xsl:with-param>
			</xsl:call-template>
		</TransactionTypeCode>
		<xsl:call-template name="BuildItemList">
			<xsl:with-param name="termEffectiveDate">
				<xsl:value-of select="msxsl:node-set($creationPolicyTermTransaction)/transaction/EffectiveDate"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="PolicyTermExtendedData"/>
	</xsl:template>




	<xsl:template name="BuildItemList">
		<xsl:param name="termEffectiveDate" />
		<AmountItems>
			<xsl:for-each select="/*/_BillingWorkingDoc/TransactReport/report/entry">				
				<xsl:variable name="bOrdered">
					<xsl:call-template name="ReverseOffsetRecordsOrder">
						<xsl:with-param name="bindex" select="@index"/>
						<xsl:with-param name="btype" select="@type"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:call-template name="BuildAmountItem">
					<xsl:with-param name="entryindex" select="$bOrdered"/>
					<xsl:with-param name="tranEffectiveDate" select="$termEffectiveDate"/>
					<xsl:with-param name="recordType" select="@type"/>
				</xsl:call-template>
			</xsl:for-each>
		</AmountItems>
	</xsl:template>

	<!--Created this template to print the account items offset record in reverse order and onset record in the same order in which is being sent from policy. - Starts -->
	<xsl:template name="BuildAmountItem">
		<xsl:param name="tranEffectiveDate"/>
		<xsl:param name="entryindex"/>
		<xsl:param name="recordType" />
		<xsl:for-each select="/*/_BillingWorkingDoc/TransactReport/report/entry[@type=$recordType][position()=$entryindex]/premiums/premium">			
			<xsl:call-template name="BuildItem">
				<xsl:with-param name="termEffectiveDate">
					<xsl:value-of select="$tranEffectiveDate"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>
	<!--Created this template to print the account items offset record in reverse order and onset record in the same order in which is being sent from policy. - Ends -->
	<!--Reverse Ordering of the offset records - Starts-->
	<xsl:template name="ReverseOffsetRecordsOrder">
		<xsl:param name="bindex"/>
		<xsl:param name="btype"/>
		<xsl:variable name="bOrder">
			<xsl:if test="$btype='onset'">
				<xsl:value-of select="count(..//entry[@index=$bindex]/preceding-sibling::entry[@type='onset'])+1"/>
			</xsl:if>
			<xsl:if test="$btype='offset'">
				<xsl:value-of select="count(..//entry[@index=$bindex and @type='offset']/following-sibling::entry[@type='offset'])+1"/>
			</xsl:if>
		</xsl:variable>
		<xsl:value-of select="$bOrder"/>
	</xsl:template>
	<!--Reverse Ordering of the offset records - Ends-->

	<xsl:template name="BuildItem">
		<xsl:variable name="transaction" select="../../transaction"/>
		<xsl:variable name="isMPR">
			<xsl:value-of select="/*/session/data/policy/MPRPolicy"/>
		</xsl:variable>
		<xsl:variable name="currentReceivableGroup" select="@ReceivableGroup" />
		<xsl:variable name="currentPremiumClass" select="@premiumClass" />

		<xsl:variable name="childsCharge">
			<xsl:choose>
				<xsl:when test="$isMPR = 1">
					<xsl:if test="$transaction/Type = 'FinalAudit'">
						<xsl:value-of select="sum(../premium[@ParentReceivableGroup = $currentReceivableGroup and @premiumClass = $currentPremiumClass]/@TotalPremium)" />
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="sum(../premium[@ParentReceivableGroup = $currentReceivableGroup and @premiumClass = $currentPremiumClass]/@term)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="charge">
			<xsl:choose>
				<xsl:when test="$isMPR = 1">
					<xsl:choose>
						<xsl:when test="$transaction/Type = 'FinalAudit'">
							<!--<xsl:value-of select="@TotalPremium"/>-->
							<xsl:value-of select="@TotalPremium - $childsCharge" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$transaction/Deposit"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<!--<xsl:value-of select="@term"/>-->
					<xsl:value-of select="@term - $childsCharge" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="childsCommissionAmount">
			<xsl:if test="@CommissionType = 'FLAT'">
				<xsl:value-of select="sum(../premium[@ParentReceivableGroup = $currentReceivableGroup and @premiumClass = $currentPremiumClass and @CommissionType = 'FLAT']/@CommissionValue)"/>
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="policyTermEffectiveDate">
			<xsl:call-template name="LookupPolicyTermEffectiveDate">
				<xsl:with-param name="EffectiveDate" select="$transaction/EffectiveDate"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:variable name="transGroup" select="$transaction/../../../report/entry[@type = 'onset'][1]/@transGroup" />



		<AmountItem>
			<xsl:if test="$transaction/@id = $transGroup or $transaction/OriginalID = $transGroup">
				<CurrentTermTrans>1</CurrentTermTrans>
			</xsl:if>
			<id>
				<xsl:value-of select="$transaction/@id"/>
			</id>
			<TransactionTypeCode>
				<xsl:call-template name="LookupTransactionTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="$transaction/Type"/>
					</xsl:with-param>
					<xsl:with-param name="termEffectiveDate">
						<xsl:value-of select="$policyTermEffectiveDate"/>
					</xsl:with-param>
					<xsl:with-param name="effectiveDate">
						<xsl:value-of select="$transaction/EffectiveDate"/>
					</xsl:with-param>
					<xsl:with-param name="deprecatedBy">
						<xsl:value-of select="$transaction/DeprecatedBy"/>
					</xsl:with-param>
					<xsl:with-param name="onsetBy">
						<xsl:value-of select="$transaction/OnsetBy"/>
					</xsl:with-param>
				</xsl:call-template>
			</TransactionTypeCode>
			<ReasonTypeCode>
				<xsl:call-template name="LookupReasonTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="$transaction/reasons/reason[last()]/Code"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReasonTypeCode>
			<ReasonCode>
				<xsl:value-of select="$transaction/reasons/reason[last()]/Code"/>
			</ReasonCode>
			<ItemEffectiveDate>
				<xsl:value-of select="$transaction/EffectiveDate"/>
			</ItemEffectiveDate>
			<ItemExpirationDate>
				<xsl:value-of select="$transaction/ExpirationDate"/>
			</ItemExpirationDate>
			<CoverageReference>
				<!--<xsl:value-of select="@Line"></xsl:value-of>-->
				<xsl:choose>
					<xsl:when test="@Line != ''">
						<xsl:value-of select="@Line"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@ReceivableGroup"/>
					</xsl:otherwise>
				</xsl:choose>
			</CoverageReference>
			<ReceivableTypeCode>
				<xsl:call-template name="LookupReceivableTypeCode">
					<xsl:with-param name="code">
						<xsl:value-of select="@premiumClass"/>
					</xsl:with-param>
					<xsl:with-param name="effectiveDate">
						<xsl:value-of select="$transaction/EffectiveDate"/>
					</xsl:with-param>
					<xsl:with-param name="termEffectiveDate">
						<xsl:value-of select="$policyTermEffectiveDate"/>
					</xsl:with-param>
				</xsl:call-template>
			</ReceivableTypeCode>
			<ReceivableSubTypeCode>
				<xsl:choose>
					<xsl:when test="$isMPR = 1 and $transaction/Type != 'FinalAudit'">DPST</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</ReceivableSubTypeCode>
			<ItemAmount>
				<xsl:choose>
					<xsl:when test="$transaction/../@type = 'offset'">
						<xsl:value-of select="round($charge * 100) div 100.0 * -1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="round($charge * 100) div 100.0"/>
					</xsl:otherwise>
				</xsl:choose>
			</ItemAmount>
			<AggregationReference/>
			<CommissionGroupReference/>
			<ItemCommissionAmount>
				<xsl:if test="@CommissionType = 'FLAT'">
					<xsl:value-of select="round((@CommissionValue - $childsCommissionAmount) * 100) div 100"/>
				</xsl:if>
			</ItemCommissionAmount>
			<ItemCommissionPercent>
				<xsl:if test="@CommissionType = 'PCT'">
					<xsl:value-of select="@CommissionValue div 100"/>
				</xsl:if>
			</ItemCommissionPercent>
			<ItemCommissionPlanId>
				<xsl:value-of select="@CommissionPlanId"/>
			</ItemCommissionPlanId>
			<ItemCommissionType>
				<xsl:value-of select="@CommissionType"/>
			</ItemCommissionType>
			<UnitReference/>
			<CallingProcess>UPTM</CallingProcess>
		</AmountItem>
	</xsl:template>



	<xsl:template name="LookupReasonTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'nonPay'">NPAY</xsl:when>
			<xsl:when test="$code = 'Other'">OTHR</xsl:when>
			<xsl:when test="$code = 'PaymentReceived'">PAID</xsl:when>
			<xsl:when test="$code = ''"/>
			<xsl:otherwise>UKWN</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="LookupReceivableTypeCode">
		<xsl:param name="code"/>
		<xsl:choose>
			<xsl:when test="$code = 'BillingInterfacePremium'">PREM</xsl:when>
			<xsl:when test="$code = 'BillingInterfaceTax'">TAX</xsl:when>
			<xsl:when test="$code = 'BillingInterfaceFee'">FEE</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
