<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:RefdtSvc="urn:RefdtSvc" xmlns:xs="http://www.w3.org/2001/XMLSchema"  exclude-result-prefixes="xs msxsl xsl RefdtSvc">

	<xsl:import href="PH_Line.xsl"/>
	<xsl:import href="PH_Dwelling.xsl"/>	
	<xsl:import href="PH_SPP.xsl"/>	
	<xsl:import href="PH_Watercraft.xsl"/>		
	<xsl:import href="PH_Coverage.xsl"/>

	<xsl:template name="Policy.Extensions">
		<PolicyType>P</PolicyType>
			<xsl:for-each select="../account/additionalOtherInterest[Type='AdditionalInsured']">
				<AdditionalInsured>
					<xsl:choose>
						<xsl:when test="InterestType='Organization'">
							<Name><xsl:value-of select="Name" /></Name>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="Person.Body"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:apply-templates select="./address"/>
					<PrimaryPhone><xsl:value-of select="PrimaryPhone" /></PrimaryPhone>
				</AdditionalInsured>
			</xsl:for-each>
	</xsl:template>

	<xsl:template name="PH_Line.Relationships">
		<xsl:apply-templates select="risk/dwelling"/>
		<xsl:apply-templates select="SPP[coverage/@deleted='0']"/>
		<xsl:apply-templates select="watercraft"/>		
	</xsl:template>
	
	<xsl:template name="Dwelling.Relationships">
		<xsl:apply-templates select="../coverage"/>
		<xsl:apply-templates select="../SectionICoverages/coverage"/>
		<xsl:apply-templates select="../SectionIICoverages/coverage"/>
	</xsl:template>

	<xsl:template name="SPP.Relationships">
		<xsl:apply-templates select="coverage"/>
	</xsl:template>

	<xsl:template name="Watercraft.Relationships">
		<xsl:apply-templates select="coverage"/>
	</xsl:template>

	
	<xsl:template name="Insured.Relationships">
		<xsl:apply-templates select="person[1]"/>
		<xsl:apply-templates select="address"/>
		<xsl:call-template name="Insured.Party" />
	</xsl:template>

</xsl:stylesheet>

