﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="coverage[(Premium != '0')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="coverage[(Premium = '0') and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="coverage[(Type = 'COA') and (Premium != '0')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Dwelling</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COA') and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Dwelling</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	

	<xsl:template match="coverage[(Type = 'COB' and IncludedLimit>0)]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">OtherStructures</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COB' and (Premium != '0'))]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">OtherStructures</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	

	<xsl:template match="coverage[(Type = 'COB' and Indicator='1')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">OtherStructures</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COC' and IncludedLimit>0)]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">PersonalProperty</xsl:with-param>
			<xsl:with-param name="Limit" select="IncludedLimit"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COC'and (Premium != '0'))]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">PersonalProperty</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COC'and Indicator='1')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">PersonalProperty</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
		
	<xsl:template match="coverage[(Type = 'COD' and Indicator='1')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">LossOfUse</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COD' and IncludedLimit>0)]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">LossOfUse</xsl:with-param>
			<xsl:with-param name="Limit" select="IncludedLimit"/>
		</xsl:call-template>
	</xsl:template>	

	<xsl:template match="coverage[(Type = 'COD' and (Premium != '0'))]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">LossOfUse</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Type = 'COE') and (Premium != '0')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Liability</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/sValue"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[(Type = 'COE') and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Liability</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/sValue"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[(Type = 'COF' and (Premium != '0'))]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Medical</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/sValue"/>
		</xsl:call-template>
	</xsl:template>	

	<xsl:template match="coverage[(Type = 'COF' and Indicator='1')]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Type">Medical</xsl:with-param>
			<xsl:with-param name="Limit" select="limit/sValue"/>
		</xsl:call-template>
	</xsl:template>	
	
	<xsl:template match="coverage[(Premium != '0') and contains('Earthquake',Type)]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Ded" select="deductible/sValue"/>
			<xsl:with-param name="DedType">Percentage</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="coverage[contains('CreditCard,Watercraft',Type)]">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="limit/iValue"/>
		</xsl:call-template>
	</xsl:template>	

	<xsl:template match="coverage[Type='SPP']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="../Limit"/>					
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[Type ='CarportsPoolCages'	and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="limit/Limit"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[contains(Type,'Replacement') and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="limit/sValue"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[contains(Type,'Replacement') and (exposure/iValue > 0) and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="exposure/iValue"/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template match="coverage[Type='LimitedFungiBacteria' and Indicator='1']">
		<xsl:call-template name="Coverage.Body">
			<xsl:with-param name="Limit" select="substring-before(limit/Limit, '/') * 1000"/>
		</xsl:call-template>
	</xsl:template>

</xsl:stylesheet>