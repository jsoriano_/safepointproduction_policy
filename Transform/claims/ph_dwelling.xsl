﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="dwelling">
		<xsl:call-template name="Dwelling.Body"/>
	</xsl:template>
	
	<xsl:template name="Dwelling.Body">
		<Dwelling id="{@id}">
			<DwellingForm><xsl:value-of select="DwellingForm"/></DwellingForm>
			<BuildingType><xsl:value-of select="BuildingType"/></BuildingType>
			<UseType><xsl:value-of select="UseType"/></UseType>
			<YearBuilt><xsl:value-of select="YearBuilt"/></YearBuilt>
			<!--xsl:with-param name="Ded" select="deductible/iValue"/>
			<xsl:with-param name="HurricaneDed" select="deductible/sValue"/-->

			<xsl:call-template name="Dwelling.Relationships"/>
		</Dwelling>
	</xsl:template>

	<xsl:template name="Dwelling.Relationships"/>

</xsl:stylesheet>