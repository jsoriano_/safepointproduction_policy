﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="watercraft">
		<xsl:call-template name="Watercraft.Body"/>
	</xsl:template>
	
	<xsl:template name="Watercraft.Body">
		<Watercraft id="{@id}">
			<Type><xsl:value-of select="Type"/></Type>
			<Length><xsl:value-of select="Length"/></Length>
			<Horsepower><xsl:value-of select="Horsepower"/></Horsepower>
			<MilesPerHour><xsl:value-of select="MilesPerHour"/></MilesPerHour>

			<xsl:call-template name="Watercraft.Relationships"/>
		</Watercraft>
	</xsl:template>

	<xsl:template name="Watercraft.Relationships"/>

</xsl:stylesheet>