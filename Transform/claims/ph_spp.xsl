﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="SPP">
		<xsl:call-template name="SPP.Body"/>
	</xsl:template>
	
	<xsl:template name="SPP.Body">
		<SPP id="{@id}">
			<ScheduledItem><xsl:value-of select="Type"/></ScheduledItem>
			<BreakageCoverage><xsl:value-of select="BreakageCoverage"/></BreakageCoverage>
			<Description><xsl:value-of select="Description"/></Description>

			<xsl:call-template name="SPP.Relationships"/>
		</SPP>
	</xsl:template>

	<xsl:template name="SPP.Relationships"/>

</xsl:stylesheet>