﻿<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="line[Type = 'PersonalHome']">
		<Line type="{Type}">
			<xsl:call-template name="PH_Line.Relationships"/>
		</Line>
	</xsl:template>

	<xsl:template name="PH_Line.Relationships"/>

</xsl:stylesheet>