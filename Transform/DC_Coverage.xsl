﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="coverage">
		<xsl:call-template name="dc:Coverage.Filter" />
	</xsl:template>
	
	<xsl:template name="dc:Coverage.Filter">
		<xsl:if test="(Premium != '0' or change != '0' or prior != '0' or written != '0') or (Indicator = '1')"> 
			<xsl:call-template name="dc:Coverage.Body" />
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="dc:Coverage.Body">
		<DC_Coverage Id="{@id}">
			<Type><xsl:value-of select="Type"/></Type>
			<BaseRate><xsl:value-of select="BaseRate"/></BaseRate>
			<BasePremium><xsl:value-of select="BasePremium"/></BasePremium>
			<CancelPremium><xsl:value-of select="cancelPrem"/></CancelPremium>
			<Premium><xsl:value-of select="Premium"/></Premium>
			<Change><xsl:value-of select="change"/></Change>
			<ExposureBasis><xsl:value-of select="ExposureBasis"/></ExposureBasis>
			<FullEarnedIndicator><xsl:value-of select="FullEarnedIndicator"/></FullEarnedIndicator>
			<LossCostModifier><xsl:value-of select="LossCostModifier"/></LossCostModifier>
			<PremiumFE><xsl:value-of select="PremiumFE"/></PremiumFE>
			<Prior><xsl:value-of select="prior"/></Prior>
			<PriorTerm><xsl:value-of select="priorTerm"/></PriorTerm>
			<Written><xsl:value-of select="written"/></Written>
			<Indicator><xsl:value-of select="Indicator"/></Indicator>
			<Deleted>
				<xsl:choose>
					<xsl:when test="(@deleted=1)">
						<xsl:value-of select="1"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="0"/>
					</xsl:otherwise> 
				</xsl:choose>  
			</Deleted>
			<xsl:call-template name="dc:Coverage.Extensions"/>
			<xsl:call-template name="dc:Coverage.Relationships"/>
		</DC_Coverage>
	</xsl:template>
	<xsl:template name="dc:Coverage.Extensions"/>
	<xsl:template name="dc:Coverage.Relationships"/>
</xsl:stylesheet>