﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="no" omit-xml-declaration="yes" />

<xsl:template match="/">
	<xsl:for-each select="ManuScript.getPageUsingMergeFieldsRs">
		<xsl:copy>
			<!-- grab all the fieldInstance nodes that match the conditions for #0 -->
			<fieldDataEvents>
				<xsl:for-each select="//fieldInstance">
					<eventItem>
						<xsl:attribute name="eventType">fieldData</xsl:attribute>
						<xsl:attribute name="fromObject"><xsl:value-of select="name()"/></xsl:attribute>
						<xsl:copy-of select="@*"/> <!-- copy over all the attributes -->
						<xsl:apply-templates /> <!-- recursively copy children -->
					</eventItem>
				</xsl:for-each>
			</fieldDataEvents>
			<!-- grab all the nodes that have a caption and match the conditions for #0 -->
			<captionDataEvents>
				<xsl:for-each select="//fieldInstance | //caption">
					<xsl:if test="@caption">
						<eventItem>
							<xsl:attribute name="eventType">captionData</xsl:attribute>
							<xsl:attribute name="fromObject"><xsl:value-of select="name()"/></xsl:attribute>
							<xsl:attribute name="objectRef"><xsl:value-of select="@objectRef"/></xsl:attribute>
							<xsl:attribute name="fieldRef"><xsl:value-of select="@fieldRef"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="@caption"/></xsl:attribute>
						</eventItem>
					</xsl:if>
					<xsl:if test="name() = 'caption'">
						<eventItem>
							<xsl:attribute name="eventType">captionData</xsl:attribute>
							<xsl:attribute name="fromObject"><xsl:value-of select="name()"/></xsl:attribute>
							<xsl:attribute name="objectRef"><xsl:value-of select="@objectRef"/></xsl:attribute>
							<xsl:attribute name="fieldRef"><xsl:value-of select="@fieldRef"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
						</eventItem>
					</xsl:if>
				</xsl:for-each>
			</captionDataEvents>
			<!-- grab all the nodes that have a match for conditions #1 -->
			<showHideEvents>
			</showHideEvents>
			<messageEvents>
				<xsl:for-each select="getPage/messages/message">
					<eventItem>
						<xsl:attribute name="eventType">message</xsl:attribute>
						<xsl:copy-of select="@*"/> <!-- copy over all the attributes -->
						<xsl:apply-templates /> <!-- recursively copy children -->
					</eventItem>
				</xsl:for-each>
			</messageEvents>
		</xsl:copy>
	</xsl:for-each>
</xsl:template>
	
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>