﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">

   
	
	<xsl:variable name="var_LOB" select="//Policy/Line/@type"/>
    
      <xsl:if test="$var_LOB = 'PersonalAuto'">
		 <xsl:for-each select="//Vehicle">				
               <InsuredItem>
                  <Name>
                     <xsl:value-of select="Make" />
                  </Name>
						<ItemID><xsl:value-of select="@id" /></ItemID>
                  <Description>
                     <xsl:value-of select="concat('Model:',Model,' ','Year:',Year)" />
                  </Description>
               </InsuredItem>
            </xsl:for-each>
      </xsl:if> 
	   <xsl:if test="$var_LOB = 'CarrierProperty'">
		 <xsl:for-each select="//Building">	
			<xsl:variable name="var_LocationID" select="LocationId"/>
            <xsl:variable name="var_Address" select="//Policy/Location[@id=$var_LocationID]/Address"/>
		 
               <InsuredItem>
                  <Name>
                     <xsl:value-of select="Description" />
                  </Name>
						<ItemID><xsl:value-of select="@id" /></ItemID>
                  <Description>
                     <xsl:value-of select="concat('Address1:',$var_Address/Address1,' ','Address2:',$var_Address/Address2,' ','City:',$var_Address/City,' ','State:',$var_Address/State)" />
                  </Description>
               </InsuredItem>
            </xsl:for-each>
      </xsl:if> 
  </xsl:template>
</xsl:stylesheet>