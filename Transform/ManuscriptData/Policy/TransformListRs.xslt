﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <!--xsl:template match="@* | node()">
      <xsl:copy>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
    </xsl:template>
    <xsl:template match="@*">
      <xsl:element name="{name()}">
        <xsl:value-of select="."/>
      </xsl:element>
    </xsl:template-->
  <xsl:template match="/">
    <xsl:variable name="listRsstatus">
      <xsl:value-of select="OnlineData.listRs/@status" />
    </xsl:variable> 
	<xsl:variable name="listCount">
      <xsl:value-of select="OnlineData.listRs/policyList/@listCount" />
    </xsl:variable>
	<xsl:variable name="listCountForConsumer">
      <xsl:value-of select="count(Consumer.listConsumerQuotesAndPoliciesRs/Quotes/Quote)" />
    </xsl:variable>
	<xsl:variable name="startIndex">
      <xsl:value-of select="OnlineData.listRs/policyList/@startIndex" />
    </xsl:variable>	      
		<xsl:if test="$listCount > 0">         							
				<xsl:for-each select="OnlineData.listRs/policyList/policy">   
					  <xsl:element name="policy">							
							<PolicyID>
							  <xsl:value-of select="@policyID" />
							</PolicyID>
							<PartyID>
							  <xsl:value-of select="@clientID" />
							</PartyID>
							<PolicyStatus>
							  <xsl:value-of select="@status" />
							</PolicyStatus>
							<LOB>
							  <xsl:value-of select="@LOB" />
							</LOB>               
							<EffectiveDate>
							  <xsl:value-of select="@effectiveDate" />
							</EffectiveDate>
							<ExpirationDate>
							  <xsl:value-of select="@expirationDate" />
							</ExpirationDate>
							<PolicyNumber>
							  <xsl:value-of select="@policyNumber" />
							</PolicyNumber>							
							<Producer>
							  <xsl:value-of select="@producer" />
							</Producer>               
							<NamedInsured>
							<xsl:value-of select="@clientName" />
							</NamedInsured>							
						</xsl:element>														
				</xsl:for-each> 	
						<xsl:element name="paging">
							 <xsl:attribute name="listCount">
								<xsl:value-of select="$listCount"/>
							</xsl:attribute>
							<xsl:attribute name="startIndex">
								<xsl:value-of select="$startIndex" />
							</xsl:attribute>
							</xsl:element>				        
		 </xsl:if>
		 <xsl:if test="$listCountForConsumer > 0"> 
		  <xsl:for-each select="Consumer.listConsumerQuotesAndPoliciesRs/Quotes/Quote">   
					  <xsl:element name="policy">
						<PolicyID>
						  <xsl:value-of select="@QuoteID" />
						</PolicyID>
						<PartyID xsl:nil="true" /> 
						<PolicyStatus>
						 <xsl:value-of select="@Status" />
						</PolicyStatus>
						<LOB>
						 <xsl:value-of select="@LOB" />
						</LOB>               
						<EffectiveDate xsl:nil="true"/>
						<ExpirationDate xsl:nil="true"/>				
						<PolicyNumber>
						  <xsl:value-of select="PolicyNumber"/>
						</PolicyNumber>
						<Producer xsl:nil="true"/>				              
						<NamedInsured xsl:nil="true"/>				 
					  </xsl:element>													
				</xsl:for-each>	   
			<xsl:element name="paging">
							 <xsl:attribute name="listCount">
								<xsl:value-of select="$listCountForConsumer"/>
							</xsl:attribute>
							<xsl:attribute name="startIndex">
								<xsl:value-of select="1" />
							</xsl:attribute>
							</xsl:element>
		</xsl:if>							
  </xsl:template>
</xsl:stylesheet>