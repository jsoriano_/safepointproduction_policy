﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
  
			<xsl:for-each select="//Driver">
			<person id="{@id}">

			<FirstName><xsl:value-of select="FirstName"/></FirstName>
			<LastName><xsl:value-of select="LastName"/></LastName>
			<IsManuallyAdded>0</IsManuallyAdded>			
			<DOB><xsl:value-of select="BirthDate"/></DOB>
			</person>
			</xsl:for-each>	
			
			
			
			<xsl:for-each select="//Insured">
			<person id="{@id}">
			<Name><xsl:value-of select="Name"/></Name>
			<FirstName><xsl:value-of select="FirstName"/></FirstName>
			<LastName><xsl:value-of select="LastName"/></LastName>
			<PhoneNumber><xsl:value-of select="PrimaryPhone"/></PhoneNumber>
			<Email><xsl:value-of select="Email"/></Email>
			<ClaimInvolvementRole>PIN</ClaimInvolvementRole>
			<Address1><xsl:value-of select="Address/Address1"/></Address1>
			<Address2><xsl:value-of select="Address/Address2"/></Address2>
			<City><xsl:value-of select="Address/City"/></City>
			<State><xsl:value-of select="Address/State"/></State>
			<ZipCode><xsl:value-of select="Address/ZipCode"/></ZipCode>
			<IsManuallyAdded>0</IsManuallyAdded>	
			</person>
			</xsl:for-each>
  
   </xsl:template>
</xsl:stylesheet>