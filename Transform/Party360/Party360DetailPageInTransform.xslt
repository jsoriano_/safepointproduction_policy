﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="CustomServer.processRq/dataRequests/PartyInvolvement.getPolicyInvolvementsSummaryRq/PartyIds">
    <PartyIds>
    <xsl:element name="PartyId">
      <xsl:value-of select="PartyId"/>
    </xsl:element>
      <xsl:call-template name="SplitandReturn"/>
    <!--<xsl:for-each select="../../../customData/DuplicatePartyIds/DuplicatePartyId">
      <xsl:element name="PartyId">
        <xsl:value-of select="current()"/>
      </xsl:element>
    </xsl:for-each>-->
    </PartyIds>
  </xsl:template>

  <xsl:template match="CustomServer.processRq/dataRequests/PartyInvolvement.getBillingInvolvementsSummaryRq/PartyIds">
    <PartyIds>
    <xsl:element name="PartyId">
      <xsl:value-of select="PartyId"/>
    </xsl:element>
      <xsl:call-template name="SplitandReturn"/>
      </PartyIds>
  </xsl:template>

  <xsl:template match="CustomServer.processRq/xsltRequests/PartyInvolvement.getPolicyInvolvementsSummaryRq/PartyIds">
    <PartyIds>
    <xsl:element name="PartyId">
      <xsl:value-of select="PartyId"/>
    </xsl:element>
      <xsl:call-template name="SplitandReturn"/>
      </PartyIds>
  </xsl:template>

  <xsl:template match="CustomServer.processRq/xsltRequests/PartyInvolvement.getBillingInvolvementsSummaryRq/PartyIds">
    <PartyIds>
      <xsl:element name="PartyId">
        <xsl:value-of select="PartyId"/>
      </xsl:element>
      <xsl:call-template name="SplitandReturn"/>
    </PartyIds>
  </xsl:template>

  <xsl:template name="SplitandReturn">
    <xsl:for-each select="../../../customData/DuplicatePartyIds">
      <!--<DuplicatePartyIds>-->
      <xsl:choose>
        <xsl:when test="boolean(.)">
          <xsl:call-template name="tokenizeIds">
            <xsl:with-param name="list" select="."/>
            <xsl:with-param name="delimiter" select="','"/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise/>
      </xsl:choose>
      <!--</DuplicatePartyIds>-->
    </xsl:for-each>

  </xsl:template>

  <xsl:template name="tokenizeIds">
    <!--passed template parameter -->
    <xsl:param name="list"/>
    <xsl:param name="delimiter"/>
    <xsl:choose>
      <xsl:when test="contains($list, $delimiter)">
        <xsl:element name="PartyId">
          <!-- get everything in front of the first delimiter -->
          <xsl:value-of select="substring-before($list,$delimiter)"/>
        </xsl:element>
        <xsl:call-template name="tokenizeIds">
          <!-- store anything left in another variable -->
          <xsl:with-param name="list" select="substring-after($list,$delimiter)"/>
          <xsl:with-param name="delimiter" select="$delimiter"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$list = ''">
            <xsl:text/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:element name="PartyId">
              <xsl:value-of select="$list"/>
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>