﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="server/responses">
    <Participants>
      <xsl:for-each select="PartyInvolvement.getParticipantsRs/Participants/Participant">
        <Participant xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <xsl:variable name="varParticipantId" select="ParticipantId" />
          <xsl:call-template name="participants" />
          <xsl:for-each select="../../../PartyObj.getPartyRs/PartyRecord">
            <xsl:if test ="PartyRecordPartyId=$varParticipantId" >
              <PartyDetails>
                <xsl:call-template name="party" />
                <xsl:call-template name="location" />
                <xsl:call-template name="phone" />
                <xsl:call-template name="email" />
                <xsl:call-template name="relation" />
              </PartyDetails>
            </xsl:if>
          </xsl:for-each>
        </Participant>
      </xsl:for-each>
    </Participants>
  </xsl:template>


  <!--Participants Templates-->
  <xsl:template name="participants" >
    <PartyId>
      <xsl:value-of select="ParticipantId"/>
    </PartyId>
    <ObjectId>
      <xsl:value-of select="ObjectId"/>
    </ObjectId>
    <ObjectReference>
      <xsl:value-of select="ObjectReference"/>
    </ObjectReference>
    <ObjectTypeCode>
      <xsl:value-of select="ObjectTypeCodes/ObjectTypeCode/Code" />
    </ObjectTypeCode>
  </xsl:template>


  <!--Participants Templates-->
  <xsl:template name="partydetailRoles" >
    <RoleCodes>
      <xsl:variable name="varParticipantId" select="PartyRecordPartyId" />
      <xsl:for-each select="../../PartyInvolvement.getParticipantsRs/Participants/Participant">
        <xsl:if test ="ParticipantId=$varParticipantId" >
          <!--<PartyId>
            <xsl:value-of select="ParticipantId"></xsl:value-of>
          </PartyId>-->
          <xsl:for-each select="ObjectTypeCodes/ObjectTypeCode/RoleCodes">
            <xsl:for-each select="RoleCode">
              <Role>
                <RoleCode>
                  <xsl:value-of select="current()"/>
                </RoleCode>
              </Role>
            </xsl:for-each>
          </xsl:for-each>

        </xsl:if>
      </xsl:for-each>

    </RoleCodes>
  </xsl:template>


  <xsl:template name="RoleCodes" >
    <xsl:for-each select="ObjectTypeCodes/ObjectTypeCode/RoleCodes/RoleCode">
      <RoleCode>
        <xsl:value-of select="current()"/>
      </RoleCode>
    </xsl:for-each>
  </xsl:template>

  <!--Party Information-->
  <xsl:template name="party" >
    <PartyId>
      <xsl:value-of select="PartyRecordPartyId"/>
    </PartyId>
    <PartyName>
      <xsl:value-of select="Party/PartyName"/>
    </PartyName>
    <PartyFirstName>
      <xsl:value-of select="Party/PartyFirstName"/>
    </PartyFirstName>
    <PartyMiddleName>
      <xsl:value-of select="Party/PartyMiddleName"/>
    </PartyMiddleName>
    <PartyTypeCode>
      <xsl:value-of select="Party/PartyTypeCode"/>
    </PartyTypeCode>
    <PartyNamePrefixCode>
      <xsl:value-of select="Party/PartyNamePrefixCode"/>
    </PartyNamePrefixCode>
    <PartyNameSuffixCode>
      <xsl:value-of select="Party/PartyNameSuffixCode"/>
    </PartyNameSuffixCode>
    <xsl:call-template name="partydetailRoles" />
  </xsl:template>

  <!--Location Information-->
  <xsl:template name="location">

    <xsl:for-each select="Locations/Location">
      <xsl:if test="LocationInvolvements/LocationInvolvement/LocationInvolvementTypeCode = '_DF_'">
        <xsl:choose>
          <xsl:when test="LocationCountryCode='USA' or LocationCountryCode='CAN' or LocationCountryCode='UK'">
            <FullAddressLine1>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine1)=0 or LocationAddressLine1=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationAddressLine1"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine2)=0 or LocationAddressLine2=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine2"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine3)=0 or LocationAddressLine3=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine3"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine4)=0 or LocationAddressLine4=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine4"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine1>
            <FullAddressLine2>
              <xsl:choose>
                <xsl:when test ="count(LocationCity)=0 or LocationCity=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationCity"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationStateName)=0 or LocationStateName=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationStateName"/>

                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCode)=0 or LocationPostalCode=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCode"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCodeExtension)=0 or LocationPostalCodeExtension=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCodeExtension"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine2>
          </xsl:when>
          <xsl:when test="LocationCountryCode='IT'">
            <FullAddressLine1>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine1)=0 or LocationAddressLine1=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationAddressLine1"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine2)=0 or LocationAddressLine2=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine2"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine3)=0 or LocationAddressLine3=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine3"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine4)=0 or LocationAddressLine4=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine4"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine1>
            <FullAddressLine2>
              <xsl:choose>
                <xsl:when test ="count(LocationCity)=0 or LocationCity=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationCity"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationStateName)=0 or LocationStateName=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationStateName"/>

                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCode)=0 or LocationPostalCode=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCode"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCodeExtension)=0 or LocationPostalCodeExtension=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCodeExtension"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine2>
          </xsl:when>
          <xsl:when test="LocationCountryCode='DE'">
            <FullAddressLine1>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine1)=0 or LocationAddressLine1=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationAddressLine1"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine2)=0 or LocationAddressLine2=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine2"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine3)=0 or LocationAddressLine3=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine3"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationAddressLine4)=0 or LocationAddressLine4=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationAddressLine4"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine1>
            <FullAddressLine2>
              <xsl:choose>
                <xsl:when test ="count(LocationCity)=0 or LocationCity=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="LocationCity"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationStateName)=0 or LocationStateName=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationStateName"/>

                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCode)=0 or LocationPostalCode=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCode"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:choose>
                <xsl:when test ="count(LocationPostalCodeExtension)=0 or LocationPostalCodeExtension=''">
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="LocationPostalCodeExtension"/>
                </xsl:otherwise>
              </xsl:choose>
            </FullAddressLine2>
          </xsl:when>
        </xsl:choose>
        <LocationId>
          <xsl:value-of select="LocationId"/>
        </LocationId>
        <LocationAddressLine1>
          <xsl:value-of select="LocationAddressLine1"/>
        </LocationAddressLine1>
        <LocationAddressLine2>
          <xsl:value-of select="LocationAddressLine2"/>
        </LocationAddressLine2>
        <LocationAddressLine3>
          <xsl:value-of select="LocationAddressLine3"/>
        </LocationAddressLine3>
        <LocationAddressLine4>
          <xsl:value-of select="LocationAddressLine4"/>
        </LocationAddressLine4>
        <Attention>
          <xsl:value-of select="Attention"/>
        </Attention>
        <LocationAddressLine5>
          <xsl:value-of select="LocationAddressLine5"/>
        </LocationAddressLine5>
        <LocationCity>
          <xsl:value-of select="LocationCity"/>
        </LocationCity>
        <LocationStateCode>
          <xsl:value-of select="LocationStateCode"/>
        </LocationStateCode>
        <LocationStateName>
          <xsl:value-of select="LocationStateName"/>
        </LocationStateName>
        <LocationPostalCode>
          <xsl:value-of select="LocationPostalCode"/>
        </LocationPostalCode>
        <LocationPostalCodeExtension>
          <xsl:value-of select="LocationPostalCodeExtension"/>
        </LocationPostalCodeExtension>
        <LocationCountyCode>
          <xsl:value-of select="LocationCountyCode"/>
        </LocationCountyCode>
        <LocationCountryCode>
          <xsl:value-of select="LocationCountryCode"/>
        </LocationCountryCode>

      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <!--Phone Information-->
  <xsl:template name="phone">
    <xsl:for-each select="PartyPhones/PartyPhone">
      <xsl:if test="PrimaryIndicator = '1'">
    <PhoneCountryCode>
      <xsl:value-of select="PhoneCountryCode"/>
    </PhoneCountryCode>
    <PhoneNumber>
      <xsl:value-of select="PhoneNumber"/>
    </PhoneNumber>
      </xsl:if>
    </xsl:for-each>

  </xsl:template>

  <!--Email Information-->
  <xsl:template name ="email">
    <xsl:for-each select="PartyEmails/PartyEmail">
      <xsl:if test="PrimaryIndicator = '1'">
        <EmailAddress>
          <xsl:value-of select="EmailAddress"/>
        </EmailAddress>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>


  <!--Relation Information-->
  <xsl:template name ="relation">
    <Relationships>
      <xsl:for-each select="PartyRelationships/PartyRelationship">
        <Relationship>
          <RelatedPartyId>
            <xsl:value-of select="RelatedPartyId"/>
          </RelatedPartyId>
          <RelatedPartyName>
            <xsl:value-of select="RelatedPartyName"/>
          </RelatedPartyName>
          <RelationshipTypeCode>
            <xsl:value-of select="RelationshipTypeCode"/>
          </RelationshipTypeCode>
        </Relationship>
      </xsl:for-each>
    </Relationships>
  </xsl:template>

  <!--Relation Information--><!--
  <xsl:template name ="relation">
    <xsl:for-each select="PartyRelationships/PartyRelationship">
      <Relationships>
        <Relationship>
          <RelatedPartyName>
            <xsl:value-of select="RelatedPartyName"/>
          </RelatedPartyName>
          <RelationshipTypeCode>
            <xsl:value-of select="RelationshipTypeCode"/>
          </RelationshipTypeCode>
        </Relationship>
      </Relationships>
    </xsl:for-each>
  </xsl:template>-->


</xsl:stylesheet>