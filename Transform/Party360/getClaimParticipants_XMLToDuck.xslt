﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>

  <xsl:template match="Participants">
    <Participants>
      <xsl:for-each select="Participant">
        <xsl:if test="count(ParticipantRoles/ParticipantRole) > 0">
            <xsl:copy-of select="."/>         
        </xsl:if>
      </xsl:for-each>
      <xsl:copy-of select="ResponseInfo"/>
    </Participants>
  </xsl:template>
</xsl:stylesheet>