﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs msxsl xsl Util">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:template match="server/responses">
    <BillingAccounts>
	      <xsl:for-each select="BillingObj.getAccountSummaryRs | Send.postRs/server/responses/CustomServer.processRs/server/responses/BillingObj.getAccountSummaryRs">

              <BillingAccountDetails xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <xsl:call-template name="accountSummary" />
                <PolicyTerms>
                  <xsl:call-template name="accountPolicyTerms" />
                </PolicyTerms>
                <Participants>
                  <xsl:call-template name="accountParticipants" />
                </Participants>
              

              <xsl:for-each select="following-sibling::BillingCashManagement.paymentSearchRs[1]">

                <Payments xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <xsl:call-template name="accountPayments" />
                </Payments>

              </xsl:for-each>

              <xsl:for-each select="following-sibling::BillingObj.searchActivityLogRs[1]">
                <BillingAccountActivities xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <xsl:call-template name="accountActivity" />
                </BillingAccountActivities>
              </xsl:for-each>

              </BillingAccountDetails>

          </xsl:for-each>

    </BillingAccounts>
  </xsl:template>

  
  <!--Billing Templates-->
  <xsl:template name="accountSummary" >
    <AccountId>
      <xsl:value-of select="Account/AccountId"/>
    </AccountId>
    <AccountReference>
      <xsl:value-of select="Account/AccountReference"/>
    </AccountReference>
    <AccountStatus>
      <xsl:value-of select="Account/AccountStatus"/>
    </AccountStatus>
	<AccountLedger>
      <xsl:call-template name="accountLedgerDetails" />
    </AccountLedger>

  </xsl:template>
  
  <xsl:template name="accountLedgerDetails" >
    <CurrentInvoiceDate>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceDate"/>
    </CurrentInvoiceDate>
    <CurrentInvoiceAmount>
      <xsl:value-of select="Account/Invoices/Invoice/InvoiceTotalAmount"/>
    </CurrentInvoiceAmount>
	<CurrentInvoiceDueDate>
      <xsl:value-of select="Account/Invoices/Invoice/DueDate"/>
    </CurrentInvoiceDueDate>
	<NextScheduledInvoiceDate>
      <xsl:value-of select="Account/NextScheduledInvoice/InvoiceDate"/>
    </NextScheduledInvoiceDate>
    <NextScheduledInvoiceAmount>
      <xsl:value-of select="Account/NextScheduledInvoice/Amount"/>
    </NextScheduledInvoiceAmount>
	<NextScheduledInvoiceDueDate>
      <xsl:value-of select="Account/NextScheduledInvoice/InvoiceDueDate"/>
    </NextScheduledInvoiceDueDate>
	<OutstandingBalance>
      <xsl:value-of select="Account/OutstandingBalance"/>
    </OutstandingBalance>
    <CurrentAmountDue>
      <xsl:value-of select="Account/CurrentDueAmount"/>
    </CurrentAmountDue>
	<PastDueAmount>
      <xsl:value-of select="Account/PastDueAmount"/>
    </PastDueAmount>
	<InSuspenseAmount>
      <xsl:value-of select="Account/InSuspenseAmount"/>
    </InSuspenseAmount>
	<PendingDisbursements>
      <xsl:value-of select="Account/PendingDisbursements"/>
    </PendingDisbursements>
	<CollectionMethod>
      <xsl:value-of select="Account/CollectionMethod"/>
    </CollectionMethod>
    <EBilling>
      <xsl:value-of select="Account/AccountConfig/Invoice/@ElectronicInvoice"/>
    </EBilling>
	<NSFCounter>
      <xsl:value-of select="Account/NSFCounter"/>
    </NSFCounter>
  </xsl:template>
  
  <xsl:template name="accountPayments" >
    <TotalCount>
      <xsl:value-of select="PaymentList/SearchControl/Paging/TotalCount"/>
    </TotalCount>
    <xsl:for-each select="PaymentList/PaymentItem">
      <Payment>
        <PaymentId>
          <xsl:value-of select="ItemData/PaymentId"/>
        </PaymentId>
        <Received>
          <xsl:value-of select="ItemData/DateReceived"/>
        </Received>
		<Method>
          <xsl:value-of select="ItemData/PaymentMethod"/>
        </Method>
		<ReferenceNumber>
          <xsl:value-of select="ItemData/ReferenceNumber"/>
        </ReferenceNumber>
        <Amount>
          <xsl:value-of select="ItemData/Amount"/>
        </Amount>
		<StatusCode>
          <xsl:value-of select="ItemData/StatusCode"/>
        </StatusCode>
      </Payment>
    </xsl:for-each>
  </xsl:template>
  <xsl:template name="accountPolicyTerms" >
    <xsl:for-each select="Account/PolicyTerms/PolicyTerm">
      <PolicyTerm>
        <PolicyTermId>
          <xsl:value-of select="PolicyTermId"/>
        </PolicyTermId>
        <PolicyReference>
          <xsl:value-of select="PolicyReference"/>
        </PolicyReference>
		<PolicyLineOfBusiness>
          <xsl:value-of select="PolicyLineOfBusiness"/>
        </PolicyLineOfBusiness>
        <PolicyTermEffectiveDate>
          <xsl:value-of select="PolicyTermEffectiveDate"/>
        </PolicyTermEffectiveDate>
        <PolicyTermExpirationDate>
          <xsl:value-of select="PolicyTermExpirationDate"/>
        </PolicyTermExpirationDate>
        <PolicyTermStatus>
          <xsl:value-of select="PolicyTermStatus"/>
        </PolicyTermStatus>
        <CurrentDueAmount>
          <xsl:value-of select="CurrentDueAmount"/>
        </CurrentDueAmount>
        <PastDueAmount>
          <xsl:value-of select="PastDueAmount"/>
        </PastDueAmount>
        <OutstandingBalance>
          <xsl:value-of select="OutstandingBalance"/>
        </OutstandingBalance>
        <PASPolicyId>
          <xsl:value-of select="PASPolicyId"/>
        </PASPolicyId>
      </PolicyTerm>
    </xsl:for-each>
  </xsl:template>


  <!--Billing Participants-->
  <xsl:template name="accountParticipants" >
    <xsl:for-each select="../PartyInvolvement.getParticipantsRs/Participants/Participant | ../../../../../../../PartyInvolvement.getParticipantsRs/Participants/Participant">
      <Participant>
        <!--<PartyId>
        <xsl:value-of select="ParticipantId"/>
      </PartyId>-->
        <!--<xsl:call-template name="partydetailRoles" />-->
        <xsl:variable name="varParticipantId" select="ParticipantId" />
        <xsl:for-each select="../../../PartyObj.getPartyLiteRs/PartyRecord">
          <xsl:if test ="PartyRecordPartyId=$varParticipantId" >
            <PartyId>
              <xsl:value-of select="PartyRecordPartyId"/>
            </PartyId>
            <PartyFullName>
              <xsl:value-of select="Party/PartyCorrespondenceName"/>
            </PartyFullName>
            <PartyName>
              <xsl:value-of select="Party/PartyName"/>
            </PartyName>
            <PartyFirstName>
              <xsl:value-of select="Party/PartyFirstName"/>
            </PartyFirstName>
            <PartyMiddleName>
              <xsl:value-of select="Party/PartyMiddleName"/>
            </PartyMiddleName>
            <PartyNamePrefix>
              <xsl:value-of select="Party/PartyNamePrefix"/>
            </PartyNamePrefix>
            <PartyNameSuffix>
              <xsl:value-of select="Party/PartyNameSuffix"/>
            </PartyNameSuffix>
            <PartyTypeCode>
              <xsl:value-of select="Party/PartyTypeCode"/>
            </PartyTypeCode>
            <xsl:call-template name="partydetailRoles" />
          </xsl:if>
        </xsl:for-each>
      </Participant>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="partydetailRoles" >
    <RoleCodes>
      <xsl:variable name="varParticipantId" select="PartyRecordPartyId" />
      <xsl:for-each select="../../PartyInvolvement.getParticipantsRs/Participants/Participant">
        <xsl:if test ="ParticipantId=$varParticipantId" >
          <xsl:for-each select="ObjectTypeCodes/ObjectTypeCode/RoleCodes">
            <xsl:for-each select="RoleCode">
	      <xsl:sort select="." data-type="text" order="ascending"/>
              <Role>
                <RoleCode>
                  <xsl:value-of select="current()"/>
                </RoleCode>
              </Role>
            </xsl:for-each>
          </xsl:for-each>

        </xsl:if>
      </xsl:for-each>

    </RoleCodes>
  </xsl:template>


  <!--Billing Activities-->
<xsl:template name="accountActivity" >
    <TotalCount>
      <xsl:value-of select="CandidateList/SearchControl/Paging/TotalCount"/>
    </TotalCount>
    <xsl:for-each select="CandidateList/ListItem">
      <BillingAccountActivity>
	  <ActivityTransactiondateTime>
          <xsl:value-of select="ItemData/ActivityTransactionDateTime"/>
        </ActivityTransactiondateTime>
		<ActivityDescription>		
		<xsl:choose>
		<xsl:when test="contains(ItemData/ActivityDescription/text/@value, '^1')">
		<xsl:variable name="Desc" select="concat(substring-before(ItemData/ActivityDescription/text/@value,'^1'),normalize-space(ItemData/ActivityDescription/link[@id='1']/@linkText),substring-after(ItemData/ActivityDescription/text/@value,'^1'))" />	
		<xsl:choose>
			<xsl:when test="contains($Desc, '^2')">
				<xsl:value-of select="concat(substring-before($Desc,'^2'),ItemData/ActivityDescription/link[@id='2']/@linkText)" />
			</xsl:when>
			<xsl:otherwise>
		<xsl:value-of select="$Desc"/>
		</xsl:otherwise>
		</xsl:choose>		
		</xsl:when>		
		<xsl:otherwise>
		<xsl:value-of select="ItemData/ActivityDescription/text/@value"/>
		</xsl:otherwise>
		</xsl:choose>
        </ActivityDescription>
        <ActivityId>
          <xsl:value-of select="ItemData/ActivityId"/>
        </ActivityId>
        <PolicyTermId>
          <xsl:value-of select="ItemData/PolicyTermId"/>
        </PolicyTermId>
        <ActivityType>
          <xsl:value-of select="ItemData/ActivityType"/>
        </ActivityType>
        <ActivityAmount>
          <xsl:value-of select="ItemData/ActivityAmount"/>
        </ActivityAmount>
        <ActivityUserFullName>
          <xsl:value-of select="ItemData/ActivityUserFullName"/>
        </ActivityUserFullName>
      </BillingAccountActivity>
    </xsl:for-each>
  </xsl:template>


</xsl:stylesheet>