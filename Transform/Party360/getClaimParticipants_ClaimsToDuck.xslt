﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="xs msxsl xsl Util" xmlns:ext="http://exslt.org/common">
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:variable name="rolecodes" select="string(Util:GetValueByKey('RoleCodes'))" />
  <xsl:variable name="items">
    <xsl:call-template name="splitStringToItems">
      <xsl:with-param name="delimiter" />
      <xsl:with-param name="list" select="$rolecodes" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:template match="GetParticipantsByClaimIDResponse/Participants">
    <Participants>
      <xsl:for-each select="ParticipantDTO">
        <Participant>
          <xsl:call-template name="participantTemplate">
          </xsl:call-template>
        </Participant>
      </xsl:for-each>
      <xsl:call-template name="ResponseInfo">
        <xsl:with-param name="ReturnCode">0</xsl:with-param>
        <xsl:with-param name="NotificationResponse"></xsl:with-param>
      </xsl:call-template>
    </Participants>
  </xsl:template>

  <xsl:template name="participantTemplate">
    <xsl:copy-of select="AnalyticScore"/>
    <CIBRequestDate>
      <xsl:value-of select="string(Util:ConvertDateToDuckFormat(CIBRequestDate))"/>
    </CIBRequestDate>
    <xsl:copy-of select="ClaimID"/>
    <xsl:copy-of select="ContactNote"/>
    <xsl:copy-of select="CoverageConfirmed"/>
    <DateOfBirth>
      <xsl:value-of select="string(Util:ConvertDateToDuckFormat(DateOfBirth))"/>
    </DateOfBirth>
    <xsl:copy-of select="DependentsUnder18"/>
    <xsl:copy-of select="DriverCountry"/>
    <xsl:copy-of select="DriverState"/>
    <xsl:copy-of select="DriversLicenseNumber"/>
    <xsl:copy-of select="EFTActive"/>
    <xsl:copy-of select="Gender"/>
    <xsl:copy-of select="InsuranceExist"/>
    <xsl:copy-of select="ParticipantFullName"/>
    <xsl:copy-of select="ParticipantID"/>

    <ParticipantNameDetail>
      <xsl:call-template name="participantNameDetailTemplate">
      </xsl:call-template>
    </ParticipantNameDetail>

    <ParticipantRoles>
      <xsl:for-each select="ParticipantRolesDTO/ParticipantRoleDTO">
        <xsl:choose>
          <xsl:when test="not($rolecodes)" >
            <ParticipantRole>
              <xsl:call-template name="participantRoleDTOTemplate">
              </xsl:call-template>
            </ParticipantRole>
          </xsl:when>
          <xsl:otherwise>
            <xsl:if test="ext:node-set($items)/* = ParticipantRole">
              <ParticipantRole>
                <xsl:call-template name="participantRoleDTOTemplate">
                </xsl:call-template>
              </ParticipantRole>
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </ParticipantRoles>

    <xsl:copy-of select="PartyID"/>
    <xsl:copy-of select="PolicyAdded"/>
    <xsl:copy-of select="PrefAddressID"/>
    <xsl:copy-of select="PrefEmailID"/>
    <xsl:copy-of select="PrefNameID"/>
    <xsl:copy-of select="PrefPhoneID"/>
    <xsl:copy-of select="PreferredContactType"/>
    <xsl:copy-of select="ReportedAge"/>
    <xsl:copy-of select="TaxID"/>
    <xsl:copy-of select="ThirdPartyPolicyNumber"/>
  </xsl:template>

  <xsl:template name="participantNameDetailTemplate">
    <xsl:copy-of select="ParticipantNameDetailDTO/CountryCode"/>
    <xsl:copy-of select="ParticipantNameDetailDTO/FirstName"/>
    <xsl:copy-of select="ParticipantNameDetailDTO/LastName"/>
    <xsl:copy-of select="ParticipantNameDetailDTO/MiddleName"/>
    <xsl:copy-of select="ParticipantNameDetailDTO/PrefixCode"/>
    <xsl:copy-of select="ParticipantNameDetailDTO/SuffixCode"/>
  </xsl:template>

  <xsl:template name="participantRoleDTOTemplate" >
    <ActivationDate>
      <xsl:value-of select="string(Util:ConvertDateToDuckFormat(ActivationDate))"/>
    </ActivationDate>
    <xsl:copy-of select="ClaimID"/>
    <DeactivationDate>
      <xsl:value-of select="string(Util:ConvertDateToDuckFormat(DeactivationDate))"/>
    </DeactivationDate>
    <xsl:copy-of select="ParticipantID"/>
    <xsl:copy-of select="ParticipantRole"/>
    <xsl:copy-of select="ParticipantRoleID"/>
  </xsl:template>

  <xsl:template name="ResponseInfo">
    <xsl:param name="ReturnCode">
    </xsl:param>
    <xsl:param name="NotificationResponse"></xsl:param>
    <ResponseInfo>
      <ReturnCode>
        <xsl:value-of select="$ReturnCode"/>
      </ReturnCode>
      <xsl:choose>
        <xsl:when test="$NotificationResponse != ''">
          <NotificationResponse>
            <xsl:value-of select="$NotificationResponse"/>
          </NotificationResponse>
        </xsl:when>
        <xsl:otherwise>
          <NotificationResponse />
        </xsl:otherwise>
      </xsl:choose>
    </ResponseInfo>
  </xsl:template>

  <xsl:template name="splitStringToItems">
    <xsl:param name="list" />
    <xsl:param name="delimiter" select="','"  />
    <xsl:variable name="_delimiter">
      <xsl:choose>
        <xsl:when test="string-length($delimiter)=0">,</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$delimiter"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="newlist">
      <xsl:choose>
        <xsl:when test="contains($list, $_delimiter)">
          <xsl:value-of select="normalize-space($list)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat(normalize-space($list), $_delimiter)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="first" select="substring-before($newlist, $_delimiter)" />
    <xsl:variable name="remaining" select="substring-after($newlist, $_delimiter)" />
    <item>
      <xsl:value-of select="$first" />
    </item>
    <xsl:if test="$remaining">
      <xsl:call-template name="splitStringToItems">
        <xsl:with-param name="list" select="$remaining" />
        <xsl:with-param name="delimiter" select="$_delimiter" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>