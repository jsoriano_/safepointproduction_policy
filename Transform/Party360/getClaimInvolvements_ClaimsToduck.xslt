﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:Util="urn:Util" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="xs msxsl xsl Util" >
  <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" indent="yes"/>
  <xsl:variable name="rolecodes" select="string(Util:GetValueByKey('RoleCodes'))" />
  <xsl:variable name="items">
    <xsl:call-template name="splitStringToItems">
      <xsl:with-param name="delimiter" />
      <xsl:with-param name="list" select="$rolecodes" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:template match="ClaimsInvolvements">
    <ClaimsInvolvements>      
      <xsl:choose>
        <xsl:when test="not($rolecodes)">
          <xsl:call-template name="AllClaimsInvolvementTemplate" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="SelectClaimsInvolvementTemplate" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="ResponseInfo">
        <xsl:with-param name="ReturnCode">0</xsl:with-param>
        <xsl:with-param name="NotificationResponse"></xsl:with-param>
      </xsl:call-template>
    </ClaimsInvolvements>
  </xsl:template>

  <xsl:template name="SelectClaimsInvolvementTemplate">
    <xsl:for-each select="ClaimsInvolvement">
      <xsl:if test="msxsl:node-set($items)/* = RoleCode" >
        <ClaimsInvolvement>
          <xsl:copy-of select="ClaimID"/>          
          <xsl:copy-of select="PartyID"/>          
          <xsl:copy-of select="RoleCode"/>
          <xsl:copy-of select="ClaimsNumber"/>
		  <xsl:copy-of select="LOBCategory"/>
          <xsl:copy-of select="LOB"/>
          <xsl:copy-of select="ClaimSensitivity"/>
          <xsl:copy-of select="ClaimStatus"/>
          <ActivationDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat(ActivationDate))"/>
          </ActivationDate>
          <DeActivationDate>
            <xsl:value-of select="string(Util:ConvertDateToDuckFormat(DeActivationDate))"/>
          </DeActivationDate>
          <xsl:copy-of select="LossDate"/>
          <xsl:copy-of select="NavigationLink"/>
          <xsl:copy-of select="ClaimEnteredDate"/>
        </ClaimsInvolvement>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="AllClaimsInvolvementTemplate">
    <xsl:for-each select="ClaimsInvolvement">
      <ClaimsInvolvement>
        <xsl:copy-of select="ClaimID"/>
        <xsl:copy-of select="PartyID"/>        
        <xsl:copy-of select="RoleCode"/>
        <xsl:copy-of select="ClaimsNumber"/>
        <xsl:copy-of select="LOBCategory"/>
        <xsl:copy-of select="LOB"/>
        <xsl:copy-of select="ClaimSensitivity"/>
        <xsl:copy-of select="ClaimStatus"/>
        <ActivationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat(ActivationDate))"/>
        </ActivationDate>
        <DeActivationDate>
          <xsl:value-of select="string(Util:ConvertDateToDuckFormat(DeActivationDate))"/>
        </DeActivationDate>
        <xsl:copy-of select="LossDate"/>
        <xsl:copy-of select="NavigationLink"/>
        <xsl:copy-of select="ClaimEnteredDate"/>
      </ClaimsInvolvement>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ResponseInfo">
    <xsl:param name="ReturnCode">
    </xsl:param>
    <xsl:param name="NotificationResponse"></xsl:param>
    <ResponseInfo>
      <ReturnCode>
        <xsl:value-of select="$ReturnCode"/>
      </ReturnCode>
      <xsl:choose>
        <xsl:when test="$NotificationResponse != ''">
          <NotificationResponse>
            <xsl:value-of select="$NotificationResponse"/>
          </NotificationResponse>
        </xsl:when>
        <xsl:otherwise>
          <NotificationResponse />
        </xsl:otherwise>
      </xsl:choose>
    </ResponseInfo>
  </xsl:template>
  
  <xsl:template name="splitStringToItems">
    <xsl:param name="list" />
    <xsl:param name="delimiter" select="','"  />
    <xsl:variable name="_delimiter">
      <xsl:choose>
        <xsl:when test="string-length($delimiter)=0">,</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$delimiter"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="newlist">
      <xsl:choose>
        <xsl:when test="contains($list, $_delimiter)">
          <xsl:value-of select="normalize-space($list)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat(normalize-space($list), $_delimiter)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="first" select="substring-before($newlist, $_delimiter)" />
    <xsl:variable name="remaining" select="substring-after($newlist, $_delimiter)" />
    <item>
      <xsl:value-of select="$first" />
    </item>
    <xsl:if test="$remaining">
      <xsl:call-template name="splitStringToItems">
        <xsl:with-param name="list" select="$remaining" />
        <xsl:with-param name="delimiter" select="$_delimiter" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
