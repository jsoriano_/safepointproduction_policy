﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="PartyRecord">
		<PartyObj.addPartyInvolvementRq>
			<ReturnParty>Y</ReturnParty>
			<TransactionDate><xsl:value-of select="@transactionDate"/></TransactionDate>
			<CalledInHierarchy>N</CalledInHierarchy>
			<PartyRecord>
				<PartyRecordPartyId><xsl:value-of select="@partyId"/></PartyRecordPartyId>
				<PartyInvolvements>
					<xsl:apply-templates select="PartyInvolvement"/>
				</PartyInvolvements>
			</PartyRecord>
		</PartyObj.addPartyInvolvementRq>
	</xsl:template>
	
	<xsl:template match="PartyInvolvement">
		<xsl:copy-of select="."/>
	</xsl:template>

</xsl:stylesheet>
