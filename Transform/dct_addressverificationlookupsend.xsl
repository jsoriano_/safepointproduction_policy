<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
	xmlns:dc="ManuscriptXsltExtensions">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<!-- integration call to the defined sampleFile that will pass in the defined key to look up integration data -->
	
	<xsl:template match="*">
		<xsl:if test="dc:contextSet('Account', '1')">
			<DCT.callIntegrationRq sampleFile="DC_AddressDriverVehicleLookupSample.xml" key="Account/@firstName={dc:fieldGet('AccountInput.FirstName')},Account/@lastName={dc:fieldGet('AccountInput.LastName')},Account/@address={dc:fieldGet('AccountInput.Address1')},Account/@state={dc:fieldGet('AccountInput.State')}" />
		</xsl:if>
	</xsl:template>


</xsl:stylesheet>

