<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
	xmlns:dc="ManuscriptXsltExtensions">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<!-- Jumpstart Territory Lookup (RECEIVE) -->
	
	<xsl:template match="*">
		<root>
			<xsl:apply-templates select="*"/>
		</root>
	</xsl:template>

	<xsl:template match="Status">
		
		<!-- Name of integration status group in manuscript, used to record if the integration was successful -->
		<xsl:if test="dc:contextSet('IntegrationStatusInfo', '1')">
			<xsl:if test="dc:fieldSet('IntegrationStatusInfo.TerritoryIntegrationStatus', @status)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Account"> <!-- Location of territory info in the data file -->
		<xsl:if test="dc:contextSet('Account', '1')"> <!-- Name of group territory information is mapped to in the manuscript -->
			<xsl:if test="dc:fieldSet('AccountOutputNonShredded.Territory', @territory)"/>
			
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>

