﻿<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:dc="DuckCreek.XSL">
	<xsl:template match="stateTaxSurcharge">
		<xsl:if test="(change != '0' ) or (written != '0') or (Amount != '0')">
			<xsl:call-template name="dc:TaxSurcharge.Body"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="dc:TaxSurcharge.Body">
		<DC_TaxSurcharge Id="{@id}">
			<Type><xsl:value-of select="Type"/></Type>
			<Scope><xsl:value-of select="Scope"/></Scope>
			<Amount><xsl:value-of select="Amount"/></Amount>
			<Change><xsl:value-of select="change"/></Change>
			<Written><xsl:value-of select="written"/></Written>
			<Rate><xsl:value-of select="Rate"/></Rate>
			<xsl:call-template name="dc:TaxSurcharge.Extensions"/>
			<xsl:call-template name="dc:TaxSurcharge.Relationships"/>
		</DC_TaxSurcharge>
	</xsl:template>
	<!-- original node selection code >> <xsl:if test="(change != '0' ) or (written != '0')"> -->
	<xsl:template name="dc:TaxSurcharge.Extensions"/>
	<xsl:template name="dc:TaxSurcharge.Relationships"/>
</xsl:stylesheet>
