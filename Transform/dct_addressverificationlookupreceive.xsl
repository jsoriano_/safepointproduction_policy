<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
	xmlns:dc="ManuscriptXsltExtensions">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<!-- Jumpstart Address Verification Lookup (RECEIVE) -->
	
	<xsl:template match="*">
		<root>
			<xsl:apply-templates select="*"/>
		</root>
	</xsl:template>

	<xsl:template match="Status">
		
		<!-- Name of integration status group in manuscript, used to record if the integration was successful -->
		<xsl:if test="dc:contextSet('IntegrationStatusInfo', '1')"> 
			<xsl:if test="dc:fieldSet('IntegrationStatusInfo.AddressIntegrationStatus', @status)"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Account"> <!-- Location of address info in the data file -->
		
		<!-- Name of address verification group in manuscript, used to keep track of all addresses returned from the integration request -->
		<xsl:if test="dc:contextAdd('AddressVerification')"> 
			<xsl:if test="dc:fieldSet('AddressVerification.FirstName', @firstName)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.LastName', @lastName)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.Address1', @address)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.City', @city)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.State', @state)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.ZipCode', @zipCode)"/>
			<xsl:if test="dc:fieldSet('AddressVerification.UniqueID', ../Status/@id)"/>
|		</xsl:if>
	</xsl:template>


 
</xsl:stylesheet>

