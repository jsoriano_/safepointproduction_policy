﻿<xsl:stylesheet version="1.0" xmlns:dc="DuckCreek.XSL" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="risk">
		<xsl:call-template name="dc:L1_Risk.Body" />
	</xsl:template>
	<xsl:template name="dc:L1_Risk.Body">
		<DC_L1_Risk Id="{@id}" id="{@id}">
			<Description>
				<xsl:value-of select="Details" />
			</Description>
			<xsl:call-template name="dc:L1_Risk.Extensions" />
			<xsl:call-template name="dc:L1_Risk.Relationships" />
		</DC_L1_Risk>
	</xsl:template>
	<xsl:template name="dc:L1_Risk.Extensions" />
	<xsl:template name="dc:L1_Risk.Relationships" />
</xsl:stylesheet>