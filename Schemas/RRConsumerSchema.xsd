﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/Consumer/1.0"
         elementFormDefault="qualified"
         xmlns="http://schemas.duckcreektech.com/Server/Consumer/1.0"
         xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation>
    <xs:documentation>Requests and responses for Consumer Access</xs:documentation>
  </xs:annotation>
  <xs:element name="Consumer.listProductsRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Lists the products that are available for consumer access</xs:documentation>
    </xs:annotation>
    <xs:complexType />
  </xs:element>
  <xs:element name="Consumer.listProductsRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>List of products available for consumer access.  Note - the items element can contain multiple item elements, and the keys element can contain multiple key elements.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="items" minOccurs="1" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Contains item elements.  Each item element represents a different product.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="item">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="keys">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="key">
                            <xs:complexType>
                              <xs:attribute name="name" use="required">
                                <xs:annotation>
                                  <xs:documentation>
                                    <description />
                                  </xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                              <xs:attribute name="value" use="required">
                                <xs:annotation>
                                  <xs:documentation>
                                    <description />
                                  </xs:documentation>
                                </xs:annotation>
                              </xs:attribute>
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name="id" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="description" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="versionDate" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success/failure of the request</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.createConsumerAccountRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Creates the consumer account record.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerKey" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>This key must be unique</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.createConsumerAccountRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Response contains the new consumerID value.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>The generated consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.deleteConsumerAccountRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Deletes the consumer account record indicated by consumerID</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>The id of the consumer account record to delete.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.deleteConsumerAccountRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation></xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.loginRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Logs the user in using a consumer account.  If the consumerKey attribute is specified, the request will log the user in using only the consumerKey.  If the consumerKey is not specified, then the request will log the user in using the consumerPartialKey and the consumerQuoteNumber.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerKey" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>The full consumer key value</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerPartialKey" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Any part of the consumer key value</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteNumber" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Quote number as specified by the consumer quote process</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="language" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Language to process session in.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.loginRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation></xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="sessionID" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Session id created by login</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>The consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="userName" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>The user name that is operating on behalf of the consumer</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerAccountLockingTS" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.newQuoteRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Starts a new consumer quote.  Will create a new session if consumerID has not been specified.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="optional" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number - use this attribute when creating a new quote for a consumer that is already logged in.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="manuscript" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>ManuScript id</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="addProducerInfo" use="optional" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>Add producer info or not.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="producerName" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Producer name to use - will use first producer in agency if not specified.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="language" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Language to initialize session with.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.newQuoteRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation></xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="userName" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>User name that will be used on behalf of consumer</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="sessionID" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Session id</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.addQuoteRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Actually creates the record in the consumer quote table</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="lob" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Line of business to add the record for</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="manuscriptID" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>ManuScript ID to associate the record with</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.addQuoteRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation></xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Generated quote id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteLockingTS" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking time stamp used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.autoSaveDataRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Performs an auto save of the consumer account and consumer quote</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="optional" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer quote number - if not specified, a new quote will be added, otherwise existing quote will be updated.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="lob" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Line of business for the consumer quote</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="manuscriptID" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>ManuScript ID for the consumer quote.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="forceSave" use="optional" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>Forces the save of the session.  Defaults to false.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteLockingTS" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp for consumer quote record - used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerAccountLockingTS" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp for consumer account record - used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.autoSaveDataRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the consumer id, consumer quote id, and locking timestamps for the consumer account and consumer quote.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer quote number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteLockingTS" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp - used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerAccountLockingTS" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp- used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.listConsumerQuotesAndPoliciesRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to list the consumer quotes and actual quotes for the consumer or customer.  Request must specify either consumerID or partyID.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="optional" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="partyID" use="optional" type="xs:int">
        <xs:annotation>
          <xs:documentation>Client id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.listConsumerQuotesAndPoliciesRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the list of all consumer quotes and actual quotes or policies for a consumer or customer.  Note - the Quotes element can contain multiple Quote elements.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Quotes" minOccurs="1" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Contains Quote elements</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="Quote">
                <xs:complexType>
                  <xs:attribute name="QuoteType" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="QuoteID" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="LOB" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="ManuScriptID" use="required">
                    <xs:annotation>
                      <xs:documentation>
                        <description />
                      </xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.loadQuoteRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Used to load an exisiting consumer quote.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer id number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer quote number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.loadQuoteRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the line of business, manuscript id, consumer quote id, and locking timestamp.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="lob" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Line of business</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="manuscriptID" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>ManuScript id for the consumer quote</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer quote number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteLockingTS" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Locking timestamp - used for optimistic locking</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.getConsumerAccountInfoRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Retrieve consumer account info.  One of the attributes: consumerID, userID, or consumerKey must be specified.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="optional" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer ID number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="userID" use="optional" type="xs:int">
        <xs:annotation>
          <xs:documentation>User ID number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerKey" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>The full consumer key value</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.getConsumerAccountInfoRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Contains the consumer account info</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="ConsumerAccount" minOccurs="0" maxOccurs="1" type="ConsumerAccount">
          <xs:annotation>
            <xs:documentation>Contains the consumer account record information</xs:documentation>
          </xs:annotation>
          <xs:complexType />
        </xs:element>
      </xs:sequence>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.convertToPolicyRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Converts the specified consumer quote into an actual quote with a new business transaction started.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="consumerID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer ID number</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteID" use="required" type="xs:long">
        <xs:annotation>
          <xs:documentation>Consumer quote ID number to convert</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="shredNow" use="optional" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>True if you want to shred immediately</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.convertToPolicyRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Contains information about the converted policy</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure of the request</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="entityID" use="required" type="xs:int">
        <xs:annotation>
          <xs:documentation>The agency entity id that the converted policy is associated with</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="policyID" use="required" type="xs:int">
        <xs:annotation>
          <xs:documentation>The converte policy ID</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.validateUserNameRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Validates the specify username.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="userName" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>The username to validate</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.validateUserNameRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Contains information about the username</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Indicates success or failure</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="validUserName" use="required" type="xs:int">
        <xs:annotation>
          <xs:documentation>1 if the username is valid, 0 otherwise</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="message" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>If the specified username was not valid, message will contain an explanation as to why.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.createAccountRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>This request is used to create a user account once a quote or policy exists in the Quote table.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="policyNumber" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>The policy number of an existing policy in the system.  Optional if consumerQuoteNumber is specified instead.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="consumerQuoteNumber" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>The quote number of an existing consumer quote in the system.  Optional if policyNumber is specified instead.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="userName" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>User name for the account.  Must be unique.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="password" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Password for the account.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="passwordExpiration" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Reserved for future use - not implemented at this time.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="partyKeyList" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Reserved for future use - not implemented at this time.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="partyValList" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Reserved for future use - not implemented at this time.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Consumer.createAccountRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Returns the ID for the newly created user account.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>Request status - success or failure.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="userID" use="required" type="xs:integer">
        <xs:annotation>
          <xs:documentation>Upon success, the new user ID value.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:complexType name="ConsumerAccount" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:sequence />
    <xs:attribute name="ConsumerID" use="required" type="xs:long">
      <xs:annotation>
        <xs:documentation>Consumer ID number</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="UserID" use="required" type="xs:int">
      <xs:annotation>
        <xs:documentation>User ID number</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="ConsumerKey" use="required" type="xs:string">
      <xs:annotation>
        <xs:documentation>The full consumer key value</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    <xs:attribute name="ConsumerAccountLockingTS" use="required" type="xs:string">
      <xs:annotation>
        <xs:documentation>Locking timestamp used for optimistic locking</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
</xs:schema>