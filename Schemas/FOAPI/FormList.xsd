<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="forms">
		<xs:annotation>
			<xs:documentation>Forms list.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" ref="paging">
					<xs:annotation>
						<xs:documentation>Defines all paging elements related to forms list.</xs:documentation>
					</xs:annotation>
				</xs:element>			
				<xs:choice maxOccurs="unbounded">
					<xs:element ref="form">
						<xs:annotation>
							<xs:documentation>Defines all forms.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:choice>
				<xs:element minOccurs="0" ref="navigation">
					<xs:annotation>
						<xs:documentation>Defines all action elements related to page navigation.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element ref="utilityActions">
					<xs:annotation>
						<xs:documentation>Defines all action elements which may be executed at a product-level.</xs:documentation>
					</xs:annotation>
				</xs:element>								
			</xs:sequence>				
		</xs:complexType>
	</xs:element>
	<xs:element name="paging">
		<xs:complexType>
			<xs:attribute name="page" type="xs:int">
				<xs:annotation>
					<xs:documentation>The page number to be returned.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="perPage" type="xs:int" >
				<xs:annotation>
					<xs:documentation>The number of policies to return per page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="totalCount" type="xs:int" >
				<xs:annotation>
					<xs:documentation>The total number of records found.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="form">
		<xs:complexType>
				<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="links">
					<xs:annotation>
						<xs:documentation>Details of individual form.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Action to attach form.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute type="xs:string" name="name" use="required">
				<xs:annotation>
					<xs:documentation>Form name.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute type="xs:string" name="caption" use="required">
				<xs:annotation>
					<xs:documentation>Caption.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:int" name="paperBinNum" use="required">
				<xs:annotation>
					<xs:documentation>Paper number.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:string" name="category" use="required">
				<xs:annotation>
					<xs:documentation>Category.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:string" name="printDefault" use="required">
				<xs:annotation>
					<xs:documentation>Default option of print.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:byte" name="order" use="required">
				<xs:annotation>
					<xs:documentation>sorting order of list.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:int" name="selected" use="required">
				<xs:annotation>
					<xs:documentation>Selected or unselected form.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
            <xs:attribute type="xs:string" name="hasMerge" use="required">
				<xs:annotation>
					<xs:documentation>Whether it is an editable or non editable form.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute type="xs:string" name="topicRef" use="required">
				<xs:annotation>
					<xs:documentation>Pageset name of editable page</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute type="xs:string" name="pageRef" use="required">
				<xs:annotation>
					<xs:documentation>Name of editable page.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute type="xs:string" name="product" use="required">
				<xs:annotation>
					<xs:documentation>If the page data includes any data from external ManuScripts, the product is specified here.				</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="links">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="link">
					<xs:annotation>
						<xs:documentation>Defines the navigation link used for view and edit forms.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="link">
		<xs:complexType>
			<xs:attribute name="rel" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>The title used for the link.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="href" type="xs:string"> 
				<xs:annotation>
					<xs:documentation>The URI.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="navigation">
		<xs:annotation>
			<xs:documentation>Groups all navigation actions available for a page.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="0" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Groups the actions that can be used to call a specific page.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="utilityActions">
		<xs:annotation>
			<xs:documentation>Defines a group of product-level actions.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="action">
					<xs:annotation>
						<xs:documentation>Defines the actions that can be used to execute a product-level process.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	<xs:element name="action">
		<xs:complexType>
			<xs:sequence>
				<xs:element minOccurs="1" maxOccurs="unbounded" ref="method">
					<xs:annotation>
						<xs:documentation>Defines the group of methods for the action in a sequential processing order. Methods can be skipped if processing is not needed.</xs:documentation>
					</xs:annotation>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="caption">
				<xs:annotation>
					<xs:documentation>The on-screen label assigned to the action button or link</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="contentType" use="required">
				<xs:annotation>
					<xs:documentation>Defines the type of content to be passed when invoking the URI against the API. Supported content types include XML and JSON.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="application/xml"/>
						<xs:enumeration value="application/json"/>
						<xs:enumeration value="application/multipart/form-data"/>
						<xs:enumeration value="multipart/form-data"/>
						<xs:enumeration value="text/xml"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="description" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Describes the purpose of the URI relative to the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="id" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>A unique identifier for the action.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
	<xs:element name="method">
		<xs:complexType>
			<xs:attribute name="index" use="required" type="xs:int">
				<xs:annotation>
					<xs:documentation>Defines the index of the method to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
			<xs:attribute name="type" use="required">
				<xs:annotation>
					<xs:documentation>Defines the REST verb (e.g. GET, PUT, POST) to use when using the URI call against the API.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:enumeration value="GET"/>
						<xs:enumeration value="POST"/>
						<xs:enumeration value="DELETE"/>
						<xs:enumeration value="PUT"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
			<xs:attribute name="uri" use="required" type="xs:string">
				<xs:annotation>
					<xs:documentation>Defines the name of the URI to use when calling the API.</xs:documentation>
				</xs:annotation>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>