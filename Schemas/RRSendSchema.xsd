﻿<xs:schema targetNamespace="http://schemas.duckcreektech.com/Server/Send/1.0" elementFormDefault="qualified" xmlns="http://schemas.duckcreektech.com/Server/Send/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:annotation xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:documentation>The schema for documenting the Send object requests/responses.</xs:documentation>
  </xs:annotation>
  <xs:element name="Send.postRq">
    <xs:annotation>
      <xs:documentation>Allows the execution of an HTTP POST against a server.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <!-- Processes the child XML, if the namespace is recognized and has a schema to validate against. -->
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:any namespace="##other" processContents="lax" />
        <xs:annotation>
          <xs:documentation>
          </xs:documentation>
        </xs:annotation>
      </xs:choice>
      	<xs:attribute name="xsltOut" use="optional" type="xs:string">
		<xs:annotation>
			<xs:documentation>Specifies the path to an XSLT file, which transforms the entire response emitted from EXAMPLE Server. For example: xsltOut=&quot;$server.rootpath$\Transform\Sample.xslt&quot;</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="xsltIn" use="optional" type="xs:string">
		<xs:annotation>
			<xs:documentation>Specifies the path to an XSLT file, which transforms the entire request given to EXAMPLE Server. For example: xsltIn=&quot;$server.rootpath$\Transform\Sample.xslt&quot;</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="async"  use="optional">
		<xs:annotation>
			<xs:documentation>Specifies the asynchronous processing mode for this API request.</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:integer">
				<xs:enumeration value="0">
					<xs:annotation>
						<xs:documentation>Process synchronously (default).</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="1">
					<xs:annotation>
						<xs:documentation>Process asynchronously.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="2">
					<xs:annotation>
						<xs:documentation>Asynchronous with a semaphore. Use this option when the result of the asynchronous request is needed to continue the logical processing of the application.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="3">
					<xs:annotation>
						<xs:documentation>Places the request in the Asynchronous queue.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="4">
					<xs:annotation>
						<xs:documentation>Places the request in the Asynchronous queue and flags it to Auto Close so the session closes when the request completes.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>

	<xs:attribute name="asyncSuccess" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>
				Notification process to be performed when the request completes successfully.
				See Notification Syntax on the Solution Center. For example:
				asyncSuccess=&quot;file=c:\temp;&quot;
				Compound notifications may be used in a single notification string. For example:
				asyncSuccess=&quot;file=c:\example\aysncdata;log=ExampleLog;email=support_desk@company.com;&quot;
			</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="asyncFailure" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>
				Notification process to be performed when the request fails.
				See Notification Syntax on the Solution Center. For example:
				asyncFailure=&quot;file=c:\temp;&quot;
				Compound notifications may be used in a single notification string. For example:
				asyncFailure=&quot;file=c:\example\aysncdata;log=ExampleLog;email=support_desk@company.com;&quot;
			</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="asyncPriority" use="optional">
		<xs:annotation>
			<xs:documentation>
				Priority to place on the thread. If not specified, the Normal priority is used.
			</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="Highest">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Above Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Below Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Lowest">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	
	<xs:attribute name="responseSessionPath" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>The responseSessionPath attribute requires a specific syntax that notifies EXAMPLE Server to copy the request response to the specified session document. This syntax is &quot;documentname^xpath&quot;, where documentname^ is the name of the session document and xpath is the xpath to the desired location in the session document. If the documentname^ is ommitted, EXAMPLE Server uses the default session document.</xs:documentation>
		</xs:annotation>
	</xs:attribute>
	
	<xs:attribute name="responseSelect" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>The responseSelect parameter can be used to select an xml element of the response or to select an attribute value. The selected node will be placed in the session where responseSessionPath indicates. The responseSelect attribute is meaningful only when responseSessionPath is also specified.</xs:documentation>
		</xs:annotation>
	</xs:attribute>
      <xs:attribute name="url" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The URL to POST to.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="setDocumentFromResponse" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Indicates if the response should replace the existing session document. Other named documents besides session will remain unaffected.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeSessionData" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Indicates if the session document should be included as part of the HTTP POST along with any child XML provided to Send.postRq.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="SOAPAction" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>Specify a SOAP action, if communicating with a SOAP-compliant web service.  Consult the WSDL provided by that web service for details.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>This attribute may optionally be used to specify an XSLT file that will transform the response received from the target web server (specified in the url attribute). The path should be relative to the Server.GlobalRootPath specified in Server.config. For example: "Transform\Sample.xslt"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="timeOutInterval" type="xs:positiveInteger" use="optional">
        <xs:annotation>
          <xs:documentation>Configurable time interval (in milliseconds) to wait for the target web service to respond.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="EXAMPLEwrapper" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Indicates if the posted child XML should be wrapped with a &lt;server&gt; tag. This is helpful when posting a &lt;requests&gt; node to another remote EXAMPLE Server.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.postRs">
    <xs:annotation>
      <xs:documentation>Response to Send.postRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <!-- Processes the child XML, if the namespace is recognized and has a schema to validate against. -->
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:any namespace="##other" processContents="lax" />
        <xs:annotation>
          <xs:documentation>
          </xs:documentation>
        </xs:annotation>
      </xs:choice>
      	<xs:attribute name="status" use="required">
		<xs:annotation>
			<xs:documentation>
				The status of a request made to the server.
			</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="success">
					<xs:annotation>
						<xs:documentation>Indicates that the request executed successfully on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="failure">
					<xs:annotation>
						<xs:documentation>Indicates that the request did not execute successfully on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="pending">
					<xs:annotation>
						<xs:documentation>Indicates that the request is currently executing asynchronously on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.emailRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Ability to send an email to person(s).  The text is the text sent to the recipients.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <!-- Processes the child XML, if the namespace is recognized and has a schema to validate against. -->
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:any namespace="##other" processContents="lax" />
        <xs:annotation>
          <xs:documentation>
          </xs:documentation>
        </xs:annotation>
      </xs:choice>
      	<xs:attribute name="xsltOut" use="optional" type="xs:string">
		<xs:annotation>
			<xs:documentation>Specifies the path to an XSLT file, which transforms the entire response emitted from EXAMPLE Server. For example: xsltOut=&quot;$server.rootpath$\Transform\Sample.xslt&quot;</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="xsltIn" use="optional" type="xs:string">
		<xs:annotation>
			<xs:documentation>Specifies the path to an XSLT file, which transforms the entire request given to EXAMPLE Server. For example: xsltIn=&quot;$server.rootpath$\Transform\Sample.xslt&quot;</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="async"  use="optional">
		<xs:annotation>
			<xs:documentation>Specifies the asynchronous processing mode for this API request.</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:integer">
				<xs:enumeration value="0">
					<xs:annotation>
						<xs:documentation>Process synchronously (default).</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="1">
					<xs:annotation>
						<xs:documentation>Process asynchronously.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="2">
					<xs:annotation>
						<xs:documentation>Asynchronous with a semaphore. Use this option when the result of the asynchronous request is needed to continue the logical processing of the application.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="3">
					<xs:annotation>
						<xs:documentation>Places the request in the Asynchronous queue.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="4">
					<xs:annotation>
						<xs:documentation>Places the request in the Asynchronous queue and flags it to Auto Close so the session closes when the request completes.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>

	<xs:attribute name="asyncSuccess" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>
				Notification process to be performed when the request completes successfully.
				See Notification Syntax on the Solution Center. For example:
				asyncSuccess=&quot;file=c:\temp;&quot;
				Compound notifications may be used in a single notification string. For example:
				asyncSuccess=&quot;file=c:\example\aysncdata;log=ExampleLog;email=support_desk@company.com;&quot;
			</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="asyncFailure" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>
				Notification process to be performed when the request fails.
				See Notification Syntax on the Solution Center. For example:
				asyncFailure=&quot;file=c:\temp;&quot;
				Compound notifications may be used in a single notification string. For example:
				asyncFailure=&quot;file=c:\example\aysncdata;log=ExampleLog;email=support_desk@company.com;&quot;
			</xs:documentation>
		</xs:annotation>
	</xs:attribute>

	<xs:attribute name="asyncPriority" use="optional">
		<xs:annotation>
			<xs:documentation>
				Priority to place on the thread. If not specified, the Normal priority is used.
			</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="Highest">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Above Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Below Normal">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="Lowest">
					<xs:annotation>
						<xs:documentation></xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	
	<xs:attribute name="responseSessionPath" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>The responseSessionPath attribute requires a specific syntax that notifies EXAMPLE Server to copy the request response to the specified session document. This syntax is &quot;documentname^xpath&quot;, where documentname^ is the name of the session document and xpath is the xpath to the desired location in the session document. If the documentname^ is ommitted, EXAMPLE Server uses the default session document.</xs:documentation>
		</xs:annotation>
	</xs:attribute>
	
	<xs:attribute name="responseSelect" type="xs:string" use="optional">
		<xs:annotation>
			<xs:documentation>The responseSelect parameter can be used to select an xml element of the response or to select an attribute value. The selected node will be placed in the session where responseSessionPath indicates. The responseSelect attribute is meaningful only when responseSessionPath is also specified.</xs:documentation>
		</xs:annotation>
	</xs:attribute>
      <xs:attribute name="from" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>From e-mail address</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="recipients" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>To e-mail address(es). If sent to multiple people, separate with a comma: "user1@host.com,user2@host.com"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="subject" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>This attribute sets the subject line of the e-mail.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>If specified, this XSLT file path will transform the child XML (if any) present in Send.emailRq. It should be a file path relative to Server.config's GlobalRootDir setting. This XSLT file should transform XML into an HTML or text output to use as the body/message of the e-mail.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeSessionData" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>If True the session document will be included with the request XML. This is useful whenever data from the session's quote or policy needs to be dynamically placed into the e-mail.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="cc" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Carbon Copy e-mail address(es). If sent to multiple people, separate with a semicolon: "user1@host.com;user2@host.com"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="bcc" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Blind Carbon Copy e-mail address(es). If sent to multiple people, separate with a semicolon: "user1@host.com;user2@host.com"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.emailRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Send.emailRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      	<xs:attribute name="status" use="required">
		<xs:annotation>
			<xs:documentation>
				The status of a request made to the server.
			</xs:documentation>
		</xs:annotation>
		<xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="success">
					<xs:annotation>
						<xs:documentation>Indicates that the request executed successfully on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="failure">
					<xs:annotation>
						<xs:documentation>Indicates that the request did not execute successfully on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
				<xs:enumeration value="pending">
					<xs:annotation>
						<xs:documentation>Indicates that the request is currently executing asynchronously on the server machine.</xs:documentation>
					</xs:annotation>
				</xs:enumeration>
			</xs:restriction>
		</xs:simpleType>
	</xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.emailAttachmentRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Allows you to specify the URL for an attatchment to an email. Separate multiple file attachments by using a pipe (|) character.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:any minOccurs="0" maxOccurs="unbounded" />
      </xs:sequence>
      <xs:attribute name="from" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Who the email is from.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="recipients" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Who the email is going to be sent to, if sent to multiple people, separate with a comma.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="subject" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The subject of the email.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>If specified, it will transform the response using XSLT. It should be path/filename relative to GlobalRootDir.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeSessionData" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>If True the session document will be included with the XML.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="attachmentURL" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The URL of where the attachement is located.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="deleteAfterAttach" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Specifies whether or not this attachement should be deleted after the email is sent.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="cc" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Carbon Copy e-mail address(es). If sent to multiple people, separate with a semicolon: "user1@host.com;user2@host.com"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="bcc" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>Blind Carbon Copy e-mail address(es). If sent to multiple people, separate with a semicolon: "user1@host.com;user2@host.com"</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.emailAttachmentRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Send.emailAttachmentRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>The status of a request made to the server.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.queueRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Ability to place a message on a queue. The body under the request will be placed on the queue.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:any minOccurs="0" maxOccurs="unbounded" />
      </xs:sequence>
      <xs:attribute name="title" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Title of the message.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="queue" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Name of the queue to place message on.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeSessionData" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>If True the session document will be included with the XML.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="useTransaction" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Set to True (1) if sending to a transactional queue.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>If specified, it will transform the response using XSLT. It should be path/filename relative to GlobalRootDir.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="includeAllDocuments" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Similar to includeSessionDocument, but wraps all named documents in "Session" root tag.  If true, includeSessionData attribute will have no effect.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.queueRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Send.emailRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>The status of a request made to the server.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.writeFileRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Writes the XML in the body of the request to fileName</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:any minOccurs="0" maxOccurs="unbounded" />
      </xs:sequence>
      <xs:attribute name="fileName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>Filename of the file to be written.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="randomName" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>If "1", will create a random filename at the end of fileName.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="append" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Specify whether or not the file exists and should be appended to.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="xsltBody" type="xs:string" use="optional">
        <xs:annotation>
          <xs:documentation>If specified, it will transform the response using XSLT. It should be path/filename relative to GlobalRootDir.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="overwrite" type="xs:boolean" use="optional">
        <xs:annotation>
          <xs:documentation>Specify whether or not the file should be overwitten.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.writeFileRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Send.writeFileRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>The status of a request made to the server.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="fileName" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The file name that was written to.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.storeToFileQueueRq" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>Writes the session document to the specified URL</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="url" type="xs:string" use="required">
        <xs:annotation>
          <xs:documentation>The location to write the document to.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.storeToFileQueueRs" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:annotation>
      <xs:documentation>See Send.storeToFileQueueRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="status" use="required">
        <xs:annotation>
          <xs:documentation>The status of a request made to the server.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="success">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="failure">
              <xs:annotation>
                <xs:documentation>
                </xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.httpRq">
    <xs:annotation>
      <xs:documentation>Allows the execution of an HTTP call against a web service.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:attribute name="url" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>The HTTP address the call will be made to.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="verb" use="required" type="xs:string">
        <xs:annotation>
          <xs:documentation>The HTTP verb used when calling the url.</xs:documentation>
        </xs:annotation>
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:enumeration value="DELETE">
              <xs:annotation>
                <xs:documentation>DELETE requests that the origin server delete the resource identified by the content.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="GET">
              <xs:annotation>
                <xs:documentation>GET will retrieve whatever information is identified by the url. This method is generally considered "safe", meaning that no underlying data is changed.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="POST">
              <xs:annotation>
                <xs:documentation>POST is designed to allow a uniform method to cover the following functions:

      - Annotation of existing resources;

      - Providing a block of data, such as the result of submitting a
        form, to a data-handling process;

      - Extending a database through an append operation.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="PUT">
              <xs:annotation>
                <xs:documentation>PUT is used to add or update an existing entity.</xs:documentation>
              </xs:annotation>
            </xs:enumeration>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:sequence>
        <xs:element name="credentials" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Any credentials needed for authentication.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:attribute name="user" use="required" type="xs:string">
              <xs:annotation>
                <xs:documentation>User name or key needed for authentication</xs:documentation>
              </xs:annotation>
            </xs:attribute>
            <xs:attribute name="pass" use="required" type="xs:string">
              <xs:annotation>
                <xs:documentation>Password needed for authentication.</xs:documentation>
              </xs:annotation>
            </xs:attribute>
          </xs:complexType>
        </xs:element>
        <xs:element name="headers" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Any HTTP headers that may be required for the HTTP call.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="header" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                  <xs:documentation>HTTP header.</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                  <xs:attribute name="name" use="required" type="xs:string">
                    <xs:annotation>
                      <xs:documentation>The name of the HTTP header, such as Content-Type.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="value" use="required" type="xs:string">
                    <xs:annotation>
                      <xs:documentation>The value of the HTTP header, such as application/xml</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="query" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Any querystring parameters to be passed along with the HTTP call.</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="parameter" minOccurs="0" maxOccurs="unbounded">
                <xs:annotation>
                  <xs:documentation>HTTP querystring parameter.</xs:documentation>
                </xs:annotation>
                <xs:complexType>
                  <xs:attribute name="name" use="required" type="xs:string">
                    <xs:annotation>
                      <xs:documentation>The name of the querystring parameter such as City.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                  <xs:attribute name="value" use="required" type="xs:string">
                    <xs:annotation>
                      <xs:documentation>The value of the querystring parameter, such as London.</xs:documentation>
                    </xs:annotation>
                  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        <xs:element name="body" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>The body of the HTTP call.</xs:documentation>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="filePath" use="optional" type="xs:string">
        <xs:annotation>
          <xs:documentation>The file path to the file to be uploaded as part of the HTTP call.</xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
  <xs:element name="Send.httpRs">
    <xs:annotation>
      <xs:documentation>The response of the Send.httpRq.</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="status" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>The status code and message of the response.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="headers" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Any headers supplied in the response.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="body" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>The body of the response.</xs:documentation>
          </xs:annotation>
        </xs:element>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>