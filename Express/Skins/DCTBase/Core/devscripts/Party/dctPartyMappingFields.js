/**
* @class DCT.Util 
*/
Ext.apply(DCT.Util, {
    /**
	* 
	*/
    positionPartyTools: function (id) {
        if (DCT.Util.isPartyToolsVisible) {
            var tools = Ext.get('partyTools'); // the tools container
            var src = Ext.get(id); // the field the tools are working with
            var srcIsDate = (Ext.getCmp(id).dctType == "date");

            // figure out if we are across or down
            var layout = 'down'; //assume down
            var el = src.parent();
            while (el) {
                if (el.hasCls('downLayout')) { break; }
                if (el.hasCls('acrossLayout')) { layout = 'across'; break; }
                el = el.parent();
            }

            // show the tools
            tools.removeCls('x-hidden-display');
            var x, y;
            if (layout == 'down') {
                // put icon next to the field
                x = (src.getX()) + (src.getWidth() - 3);
                if (Ext.isGecko) {
                    x -= 2;
                }
                if (srcIsDate) {
                    x -= 15;
                }
                y = (src.getY());

                tools.setX(x);
                tools.setY(y);
                tools.dom.style.height = Ext.String.format("{0}px", src.dom.clientHeight);
                tools.removeCls('toolsAbove');
                tools.addCls('toolsRight');
                tools.slideIn((srcIsDate ? 'r' : 'l'), { duration: .2 });
            }
            else {
                // put icon above the right edge of the field
                x = (src.getX()) + (src.getWidth()) - (tools.getWidth()); // align with right edge of control
                y = (src.getTop() - (tools.getHeight()) + 1);

                tools.setX(x);
                tools.setY(y);
                tools.removeCls('toolsRight');
                tools.addCls('toolsAbove');
                tools.slideIn('b', { duration: .2 });
            }
        }
    }
});


