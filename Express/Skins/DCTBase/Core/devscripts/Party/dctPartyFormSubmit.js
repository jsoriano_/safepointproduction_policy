/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
    /**
	*
	*/
    submitPartyPage: function (page) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        if (page == 'policySummary')
            DCT.Util.getSafeElement('_policyId').value = '';
        if (page == 'partyManagement')
            DCT.Util.getSafeElement('__overrideTarget').value = 'partyIsRelationship';
        if (page == 'partyIsCandidate') DCT.Util.setSearchDataValues((Ext.getDom('lookingForSelectionId').value), (Ext.getDom('searchBySelectionId').value));
        if (page == 'partyLinkPeoplePlaces')
            DCT.Util.getSelectedParties();
        me.secureAction(page);
        me.submitForm();
    },
    /**
     *
     */
    submitPartyInvolvementsPage: function (page, partyId) {
        DCT.Util.getSafeElement('_partyId').value = partyId;
        DCT.Submit.submitPartyPage(page);
    },
    /**
	*
	*/
    submitPartyPageAction: function (page, action, eventChecked, actionButton) {
        var me = this;
        if (typeof (eventChecked) != 'undefined') {
            if (!eventChecked) {
                if (DCT.Util.isButtonDisabled(actionButton))
                    return;
            }
        }
        if (DCT.Util.validatePartyScreen(action, actionButton)) {
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = action;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
	*
	*/
    setPolicyPortal: function (portalName, xsltDir) {
        var me = this;
        document.forms[0].action = DCT.PolicySystemURL;
        DCT.Util.getSafeElement('_PortalName').value = "AdminPAS";
        DCT.Util.getSafeElement('_targetPage').value = "interview";
        DCT.Util.initializeFields();
        DCT.Util.getSafeElement('_autoSaveState').value = "";
        me.submitAction('setPortal');
    },

    /**
	*
	*/
    loadPolicyFromParty: function (iQuoteID) {
        document.forms[0].action = DCT.PolicySystemURL;
        DCT.globalQuoteID = iQuoteID;
        DCT.Util.getSafeElement('_QuoteID').value = DCT.globalQuoteID;
        DCT.Submit.addToParmArray('_submitAction', 'setPortal,load');
        DCT.FieldsChanged.clear();
        DCT.Submit.setActiveParent('dashboardHome');
        DCT.Submit.submitPageForm();
    }
});