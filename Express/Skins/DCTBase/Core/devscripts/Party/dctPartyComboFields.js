//==================================================                                         
// Party Basic Combo control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.PartyBasicComboField', {
    extend: 'DCT.SystemComboField',


    inputXType: 'dctpartybasiccombofield',
    xtype: 'dctpartybasiccombofield',
    /**
	*
	*/
    dctHideShow: function (value) {
    }
});
DCT.Util.reg('dctpartybasiccombofield', 'partyBasicComboField', 'DCT.PartyBasicComboField');           

//==================================================                                         
// Party Basic Hide Show Combo control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.PartyHideShowComboField', {
    extend: 'DCT.BasicComboField',


    inputXType: 'dctpartyhideshowcombofield',
    xtype: 'dctpartyhideshowcombofield',
    /**
	*
	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            select: {
                fn: function (combo, record, index) {
                    me.dctHideShow(combo.getValue());
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    hideShowNameAddressData: function (lookingFor, searchBy) {
        if (searchBy == 'NA' || searchBy == 'OT') {
            DCT.Util.hideShowGroup("firstNameData", (lookingFor == "P"));
            DCT.Util.hideShowGroup("middleNameData", (lookingFor == "P"));
            DCT.Util.hideShowGroup("personNameData", (lookingFor == "P"));
            DCT.Util.hideShowGroup("organizationNameData", (lookingFor == "O"));
            DCT.Util.hideShowGroup("agencyNameData", (lookingFor == "AG"));
            DCT.Util.hideShowGroup("dobData", (searchBy == "OT"));
            DCT.Util.hideShowGroup("addressData", true);
            DCT.Util.hideShowGroup("cityData", true);
            DCT.Util.hideShowGroup("stateData", true);
            DCT.Util.hideShowGroup("zipData", true);
            if (searchBy == 'NA') {
                DCT.Util.hideShowGroup("soundexData", true);
            }
        } else {
            DCT.Util.hideShowGroup("firstNameData", false);
            DCT.Util.hideShowGroup("middleNameData", false);
            DCT.Util.hideShowGroup("personNameData", false);
            DCT.Util.hideShowGroup("organizationNameData", false);
            DCT.Util.hideShowGroup("agencyNameData", false);
            DCT.Util.hideShowGroup("dobData", false);
            DCT.Util.hideShowGroup("addressData", false);
            DCT.Util.hideShowGroup("cityData", false);
            DCT.Util.hideShowGroup("stateData", false);
            DCT.Util.hideShowGroup("zipData", false);
            DCT.Util.hideShowGroup("soundexData", false);
        }
    }
});
DCT.Util.reg('dctpartyhideshowcombofield', 'partyHideShowComboField', 'DCT.PartyHideShowComboField');           

//==================================================                                         
// Party Lookup Combo control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.PartyLookUpComboField', {
    extend: 'DCT.PartyHideShowComboField',


    inputXType: 'dctpartylookupcombofield',
    xtype: 'dctpartylookupcombofield',
    /**
	*
	*/
    dctHideShow: function (value) {
        var me = this;
        var searchBy = Ext.getCmp(me.dctSearchByField).getValue();
        DCT.Util.hideShowGroup(me.dctLookingForALabel, (value == "P" || value == ""));
        DCT.Util.hideShowGroup(me.dctlookingForAnLabel, (value == "O" || value == "AG"));
        if (searchBy == 'NA' || searchBy == 'OT') {
            me.hideShowNameAddressData(value, searchBy);
        }
    }
});
DCT.Util.reg('dctpartylookupcombofield', 'partyLookUpComboField', 'DCT.PartyLookUpComboField');           

//==================================================                                         
// Party Search By Combo control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.PartySearchByComboField', {
    extend: 'DCT.PartyHideShowComboField',


    inputXType: 'dctpartysearchbycombofield',
    xtype: 'dctpartysearchbycombofield',
    /**
	*
	*/
    dctHideShow: function (value) {
        var me = this;
        var lookingFor = Ext.getCmp(me.dctLookUpField).getValue();
        DCT.Util.hideShowGroup(me.dctDataRequiredField, (value == "LC" || value == "PH" || value == "RF"));
        DCT.Util.hideShowGroup(me.dctDataRequiredPlusField, (value == "NA" || value == "OT"));
        DCT.Util.hideShowGroup(me.dctPhoneData, (value == "PH"));
        DCT.Util.hideShowGroup(me.dctLicenseData, (value == "LC"));
        DCT.Util.hideShowGroup(me.dctReferenceData, (value == "RF"));
        DCT.Util.hideShowGroup(me.dctProcessSearchField, (value != "" && lookingFor != ""));
        me.hideShowNameAddressData(lookingFor, value);
    }
});
DCT.Util.reg('dctpartysearchbycombofield', 'partySearchByComboField', 'DCT.PartySearchByComboField');

//=========================================================                                         
// Party Involvement Role Type Filter Combo control                                                               
//=========================================================                                  

/**
*
*/
Ext.define('DCT.PartyRoleFilterComboField', {
    extend: 'DCT.BasicComboField',


    inputXType: 'partyRoleFilterComboField',
    xtype: 'dctpartyrolefiltercombofield',
    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            select: {
                fn: function (combo, record, index) {
                    me.reloadInvolvements(combo.getValue());
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    reloadInvolvements: function (value) {
        var me = this;
        var gridDataStore = DCT.Util.getDataStore(me.dctGridId);
        DCT.Grid.applyFilterToDataStore(gridDataStore);
    }
});
DCT.Util.reg('dctpartyrolefiltercombofield', 'partyRoleFilterComboField', 'DCT.PartyRoleFilterComboField');
