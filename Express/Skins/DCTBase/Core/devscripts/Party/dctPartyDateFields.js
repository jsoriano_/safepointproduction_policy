/**
*
*/
Ext.define('DCT.PartyDateField', {
    extend: 'DCT.SystemDateField',


    inputXType: 'dctpartydatefield',
    xtype: 'dctpartydatefield'

});
DCT.Util.reg('dctpartydatefield', 'partyDateField', 'DCT.PartyDateField');
