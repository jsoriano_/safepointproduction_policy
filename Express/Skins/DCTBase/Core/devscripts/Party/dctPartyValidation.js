/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	validatePartyScreen: function(processScreen, fieldId){
	    var me = this;
	    var msgText = "";
	    var throwAlert = false;
	    var focusField = Ext.get(fieldId);
	    var fieldValue;
	
	    switch(processScreen){
	        case '':
	          throwAlert = true;
	          break;
	    }
	    if (throwAlert){
	        me.removeTabs(true);
	        Ext.Msg.alert(DCT.T('Alert'), msgText, function(btn){
	           me.removeTabs(false);
	           focusField.focus();
	        }, me);
	    }
	    return (!throwAlert);
	},
	/**
	*
	*/
	validateSearchData: function(){
	    var me = this;
	    var reqMsgText = DCT.T("FieldRequired");
	    var alertTitleText;
	    var throwRequiredAlert = false;
	    var focusField;
	    var fieldCmp;
	
	    var lookingFor = Ext.getCmp("lookingForSelectionId").getValue();
	    var searchBy  = Ext.getCmp("searchBySelectionId").getValue();
	
	    switch(searchBy){   
	        case 'LC':
	        	 fieldCmp = Ext.getCmp('searchLicenseId');
	           if(Ext.isEmpty(fieldCmp.getValue())){   
	              alertTitleText = DCT.T("LicenseNumber");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	           }
	           break;
	        case 'PH':
	         	 fieldCmp = Ext.getCmp('searchPhoneId');
	           if(Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("PhoneNumber");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	           }
	           break;
	        case 'RF':
	         	 fieldCmp = Ext.getCmp('searchReferenceId');
	           if(Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("ReferenceNumber");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	           }
	           break;
	        case 'NA':
	        case 'OT':
	         	 fieldCmp = Ext.getCmp('personPartyNameId');
	           if(lookingFor == "P" && Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("LastName");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	              break;
	           }
	         	 fieldCmp = Ext.getCmp('organizationPartyNameId');
	           if(lookingFor == "O" && Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("OrganizationName");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	              break;
	           }
	         	 fieldCmp = Ext.getCmp('agencyPartyNameId');
	           if(lookingFor == "AG" && Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("OrganizationOrLastName");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	              break;
	           }
	         	 fieldCmp = Ext.getCmp('searchStateId');
	           if(Ext.isEmpty(fieldCmp.getValue())){
	              alertTitleText = DCT.T("State");
	              focusField = fieldCmp;
	              throwRequiredAlert = true;
	           }
	           break;
	    }
	    if (throwRequiredAlert)
	    {
	        me.removeTabs(true);
	        Ext.Msg.alert(alertTitleText, reqMsgText, function(btn){
	           me.removeTabs(false);
	           focusField.focus();
	        }, me);
	    }
	    return (!throwRequiredAlert);
	}
});