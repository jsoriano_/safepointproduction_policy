/**
*
*/
Ext.define('DCT.PartyPhoneField', {
    extend: 'DCT.InputPhoneField',


    inputXType: 'dctpartyphonefield',
    xtype: 'dctpartyphonefield',
    /**
	*
	*/
    setDefaultListeners: function (config) {
        config.listeners = {
        };
    },
    /**
	*
	*/
    setListeners: function (config) {
    },
    /**
	*
	*/
    setDisable: function (config) {
        config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
    }

});
DCT.Util.reg('dctpartyphonefield', 'partyPhoneField', 'DCT.PartyPhoneField');

