/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    setSearchDataValues: function (lookingFor, searchBy) {
        this.clearSearchDataValues();
        switch (searchBy) {
            case 'LC':
                DCT.Util.getSafeElement('_licenseNumber').value = Ext.getCmp('searchLicenseId').getValue();
                break;
            case 'PH':
                DCT.Util.getSafeElement('_phoneNumber').value = Ext.getCmp('searchPhoneId').getValue();
                break;
            case 'RF':
                DCT.Util.getSafeElement('_referenceNumber').value = Ext.getCmp('searchReferenceId').getValue();
                break;
            case 'NA':
            case 'OT':
                this.searchNameAddressValues(lookingFor);
                break;
        }
        if (searchBy == 'OT') {
            DCT.Util.getSafeElement('_dob').value = Ext.getCmp('searchDOBId').getValue();
        } else {
            DCT.Util.getSafeElement('_soundexIndicator').value = (Ext.getCmp('searchSoundexId').getValue()) ? 'S' : ' ';
        }
        DCT.Util.getSafeElement('_partyTypeCode').value = lookingFor;
        DCT.Util.getSafeElement('_listStart').value = '0';
        DCT.Util.getSafeElement('_listLength').value = '10';
    },
    /**
	*
	*/
    searchNameAddressValues: function (lookingFor) {
        switch (lookingFor) {
            case 'P':
                DCT.Util.getSafeElement('_firstName').value = Ext.getCmp('searchFirstNameId').getValue();
                DCT.Util.getSafeElement('_middleName').value = Ext.getCmp('searchMiddleNameId').getValue();
                DCT.Util.getSafeElement('_name').value = Ext.getCmp('personPartyNameId').getValue();
                break;
            case 'O':
                DCT.Util.getSafeElement('_name').value = Ext.getCmp('organizationPartyNameId').getValue();
                break;
            case 'AG':
                DCT.Util.getSafeElement('_name').value = Ext.getCmp('agencyPartyNameId').getValue();
                break;
        }
        DCT.Util.getSafeElement('_address1').value = Ext.getCmp('searchAddress1Id').getValue();
        DCT.Util.getSafeElement('_city').value = Ext.getCmp('searchCityId').getValue();
        DCT.Util.getSafeElement('_stateCode').value = Ext.getCmp('searchStateId').getValue();
        DCT.Util.getSafeElement('_postalCode').value = Ext.getCmp('searchZipCodeId').getValue();
    },
    /**
	*
	*/
    clearSearchDataValues: function () {
        var empty = "";

        DCT.Util.getSafeElement('_licenseNumber').value = empty;
        DCT.Util.getSafeElement('_phoneNumber').value = empty;
        DCT.Util.getSafeElement('_referenceNumber').value = empty;
        DCT.Util.getSafeElement('_address1').value = empty;
        DCT.Util.getSafeElement('_city').value = empty;
        DCT.Util.getSafeElement('_stateCodeCombo').value = empty;
        DCT.Util.getSafeElement('_postalCode').value = empty;
        DCT.Util.getSafeElement('_firstName').value = empty;
        DCT.Util.getSafeElement('_middleName').value = empty;
        DCT.Util.getSafeElement('_name').value = empty;
        DCT.Util.getSafeElement('_referenceNumber').value = empty;
    },
    /**
	*
	*/
    getSelectedParties: function () {
        var selections = Ext.getCmp("searchResultsData").getSelectionModel().getSelection();
        var selectedIds = new Array();

        for (var i = 0; i < selections.length; i++) {
            selectedIds.push(selections[i].id);
        }
        DCT.Util.getSafeElement('_selectedRecords').value = selectedIds.join(',');
    }
});