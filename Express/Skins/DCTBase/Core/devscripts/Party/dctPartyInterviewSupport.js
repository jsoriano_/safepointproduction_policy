/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    isPartyToolsVisible: false,
    /**
	*
	*/
    partyPopUp: function (popUpTitle) {
        DCT.Util.getSafeElement('_popUp').value = '1';
        var parentWindow = DCT.Util.getParent();
        if (Ext.isEmpty(popUpTitle)) {
            if (parentWindow.Ext.getCmp('dctPopupWindow')) {
                popUpTitle = parentWindow.Ext.getCmp('dctPopupWindow').title;
            }
            else {
                popUpTitle = DCT.T("PartyImport_DefaultPopupTitle");
            }
        }
        var displayPartyPopup = Ext.create('DCT.PopupWindow', {
            width: 800,
            height: 360,
            modal: true,
            title: popUpTitle,
            cls: 'partyPopup',
            dctScrollbars: true,
            dctInterview: true,
            dctItemClass: 'DCT.PartyPanel',
            dctPanelConfig: {
                dctTargetPage: 'partyDataImportWizard'
            }
        });
        displayPartyPopup.show();
    },
    /**
	*
	*/
    closePartyPopup: function () {
        var popUpWindow = DCT.Util.getParent().Ext.getCmp('dctPopupWindow');
        if (popUpWindow) {
            popUpWindow.hideWindow();
        }
    },
    /**
	*
	*/
    doPartyRecordAction: function (execute, value, caption) {
        var me = this;
        me.getSafeElement('_execute').value = execute;
        me.getSafeElement('_value').value = value;
        me.getSafeElement('_caption').value = caption;
        DCT.Submit.submitPageAction('partyMappingActions', '');
    },
    /**
	*
	*/
    getPartyDataFromHistory: function (historyId) {
        var me = this;
        me.getSafeElement('_historyId').value = historyId;
        me.getSafeElement('_retrieve').value = 'history';
        DCT.Submit.submitPageAction('partyHistoryData', '');
    },
    /**
	*
	*/
    getPartyData: function (partyId) {
        var me = this;
        me.getSafeElement('_partyId').value = partyId;
        me.getSafeElement('_retrieve').value = 'party';
        DCT.Submit.submitPageAction('partyHistoryData', '');
    },
    /**
	*
	*/
    selectPartyData: function () {
        var me = this;
        if (!DCT.Util.validateFields()) {
            return false;
        }

        var partyGroupId = Ext.get('partyTools').getAttribute('dctPartyRecordMappingId');
        Ext.select('input[dctPartyRecordMappingId=' + partyGroupId + ']').addCls('partyGroupIndicator');
        var popUpTitle = Ext.get('partyTools').getAttribute('partyPopupTitle');
        var fieldObjectRef = Ext.get('partyTools').getAttribute('fieldObjectRef');
        me.getSafeElement('_partyRecordMappingId').value = partyGroupId;
        me.getSafeElement('_fieldObjectRef').value = fieldObjectRef;
        var isPopup = me.getSafeElement('_popUp').value;
        var parentWindow = DCT.Util.getParent();
        if (isPopup == 'true' || isPopup == '1') {
            parentWindow.DCT.Util.getSafeElement('_parentIsPopup').value = "1";
            DCT.Util.getSafeElement('_importPartyDataToPolicyFromPopup').value = "1";
			Ext.getDom('_targetPage').value = 'partyDataImportWizard';
			DCT.Submit.customBeforeSubmit();
			document.forms[0].submit();
		} else {
			parentWindow.DCT.Util.getSafeElement('_parentIsPopup').value = "0";
            me.partyPopUp(popUpTitle);
        }
    },
    /**
	*
	*/
    importSelectedPartyRecords: function (action, linkId) {
        var me = this;

        if (me.isButtonDisabled(linkId))
            return;

        if (DCT.Grid.selectAll) {
            //We need to import all!!!!!
            me.getSafeElement('_selectedAll').value = true;
            me.getSafeElement('_deselectedRecords').value = DCT.Grid.deselectedRecords.join(',');
        }
        else {
            var selections = Ext.getCmp("partyRecordData").getSelectionModel().getSelection();
            var selectedIds = new Array();

            Ext.each(selections, function (item, index) {
                selectedIds.push(item.id);
            });

            me.getSafeElement('_selectedRecords').value = selectedIds.join(',');
        }
        if (DCT.Util.getParent().DCT.Util.getSafeElement('_parentIsPopup').value == "1") {
            DCT.Submit.submitPageAction('', action);
        }
        else {
            Ext.getDom('_submitAction').value = action;
            me.closePopUp('3', '', '0');
        }
    },
    /**
	*
	*/
    reloadInvolvementsGrid: function (elemPrefix, doReset) {
        var pageSize = 0;
        var clearButton = Ext.get(elemPrefix + 'ClearButton');
        var gridId = elemPrefix + 'InvolvementsGrid';

        if (doReset) {
            Ext.getCmp(elemPrefix + 'PageSizeInput').setValue('');
            clearButton.addCls('btnDisabled');
            clearButton.dom.children[0].disabled = true;
            pageSize = parseInt(DCT.Util.getSafeElement('_defaultPageSize').value, 10)
        }
        else {
            clearButton.removeCls('btnDisabled');
            clearButton.dom.children[0].disabled = false;
            pageSize = parseInt(DCT.Util.getSafeElement(elemPrefix + 'PageSizeInput').value, 10);
        }
        if (pageSize > 0) {
            DCT.Util.resetPagination(gridId, pageSize);
        }
    },
    /**
	*
	*/
    resetPagination: function (gridId, pageSize) {
        var gridDataStore = DCT.Util.getDataStore(gridId);

        gridDataStore.removeAll();
        gridDataStore.setPageSize(pageSize);
        gridDataStore.loadPage(1);
        Ext.getCmp(gridId).getBottomToolbar().pageSize = pageSize;
    }
});
/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
    selectAll: false,
    deselectedRecords: new Array(),

    /**
	*
	*/
    partyMappedItemSelected: function (selectionModel, dataRecord, rowIndex) {

        if (DCT.Grid.selectAll) {
            DCT.Grid.removeItemFromDeselectedRecords(dataRecord.id);
        }
        var selectedItems = selectionModel.getCount();
        if (selectedItems > 0) {
            var suspendAnchor = Ext.get('importSelected');
            suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');
            DCT.Util.disableComponent('_processAction', true);
        }
    },
    /**
	*
	*/
    partyMappedItemDeselected: function (selectionModel, dataRecord, rowIndex) {
        if (DCT.Grid.selectAll) {
            DCT.Grid.addItemToDeselectedRecords(dataRecord.id);
        }
        var selectedItems = selectionModel.getCount();
        if (selectedItems == 0) {
            var suspendAnchor = Ext.get('importSelected');
            suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
            DCT.Util.disableComponent('_processAction', false);

        }
    },
    /**
	*
	*/
    addItemToDeselectedRecords: function (id) {
        DCT.Grid.deselectedRecords.push(id);
    },
    /**
	*
	*/
    removeItemFromDeselectedRecords: function (id) {
        Ext.each(DCT.Grid.deselectedRecords, function (item, index) {
            if (item.indexOf(id) != -1) {
                DCT.Grid.deselectedRecords.remove(id);
            }
        });
    },
    /**
	*
	*/
    selectAllRows: function () {
        DCT.Grid.selectAll = true;
        /* This is to select all the records on each page */
        Ext.getCmp("partyRecordData").getSelectionModel().selectAll();
        Ext.getCmp("partyRecordData").getSelectionModel().allSelected = true;
        Ext.getCmp("partyRecordData").getSelectionModel().setPageSelections();
    },
    /**
	*
	*/
    clearAllRows: function () {
        DCT.Grid.selectAll = false;
        /* This is to clear all the selections made on all pages */
        Ext.getCmp("partyRecordData").getSelectionModel().clearSelections();
        Ext.getCmp("partyRecordData").getSelectionModel().allSelected = false;
        Ext.getCmp("partyRecordData").getSelectionModel().clearPageSelections();
    },
    /**
	 *
	 */
    policyReferenceColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
        var objectReferenceColumnHTML = '<a href="javascript:;" id="policyInvolvementLoad" onclick="DCT.Submit.actOnQuote(\'load\',\'' + extRecord.id + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
        return objectReferenceColumnHTML;
    },
    /**
	 *
	 */
    billingReferenceColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
        var objectReferenceColumnHTML = '<a href="javascript:;" id="billingInvolvementLoad" onclick="DCT.Submit.gotoBillingAccountPage(\'' + extRecord.id + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
        return objectReferenceColumnHTML;
    },
    /**
	*
	*/
    claimsNumberColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
        var objectReferenceColumnHTML = '<a href="' + extRecord.data.NavigationLink + '" id="viewClaim">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
        return objectReferenceColumnHTML;
    },
    //TODO - add logic for other reference renderer?
    /**
	 *
	 */
    involvementRoleColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
        var involvementRoleColumnHTML = "";
        var xpathValue = "content/CodeLists/CodeList[@ListName='PTY_INVROLE']/ListEntry[@Code='" + dataValue + "']/@Description";
        var listEntry = Ext.DomQuery.select(xpathValue, extRecord.store.proxy.reader.rawData);

        if (listEntry.length > 0)
            involvementRoleColumnHTML = listEntry[0].firstChild.nodeValue;
        else
            involvementRoleColumnHTML = "(none)";

        return involvementRoleColumnHTML;
    }
});