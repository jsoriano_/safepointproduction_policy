/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/	
	popUpPartyCommon: function (action, partyContextJson) {
		var me = this;
		DCT.Util.getSafeElement('_popUp').value = '1';
		Ext.getDom('_submitAction').value = me.dctTargetPage;
		
		if (partyContextJson) {
			var array = eval("(" + partyContextJson + ")");
			for (var i = 0; i < array.length; i++) {
				var pair = array[i].split(":");
				DCT.Util.appendHiddenElement(pair[0], pair[1]);
			}
		}		
		var partyCommonPopup = Ext.create('DCT.PopupWindow',{
			cls: 'popUp360',
			modal: true,
			closable: true,
			closeToolText: DCT.T('Close'),
			dctPartyReturn: true,
			dctItemClass: 'DCT.PartyCommonPanel',
			dctPanelConfig: {
				dctTargetPage: action
			}
		});
		partyCommonPopup.show();
	}
});
