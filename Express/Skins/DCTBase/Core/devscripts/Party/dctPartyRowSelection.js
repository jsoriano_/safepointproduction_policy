/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    partyIsCandidateSelected: function (selectionModel, dataRecord, rowIndex) {
        var selectedItems = selectionModel.getCount();
        if (selectedItems > 0) {
            var suspendAnchor = Ext.get('linkSelected');
            suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');
            DCT.Util.disableComponent('_processAction', true);
        }
    },
    /**
	*
	*/
    partyIsCandidateDeselected: function (selectionModel, dataRecord, rowIndex) {
        var selectedItems = selectionModel.getCount();
        if (selectedItems == 0) {
            var suspendAnchor = Ext.get('linkSelected');
            suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
            DCT.Util.disableComponent('_processAction', false);
        }
    }
});