/**
*
*/
Ext.define('DCT.PartyZipCodeField', {
    extend: 'DCT.InputZipCodeField',


    inputXType: 'dctpartyzipcodefield',
    xtype: 'dctpartyzipcodefield',
    /**
	*
	*/
    setDefaultListeners: function (config) {
        config.listeners = {
        };
    },
    /**
	*
	*/
    setListeners: function (config) {
    },
    /**
	*
	*/
    setDisable: function (config) {
        config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
    }

});
DCT.Util.reg('dctpartyzipcodefield', 'partyZipCodeField', 'DCT.PartyZipCodeField');
