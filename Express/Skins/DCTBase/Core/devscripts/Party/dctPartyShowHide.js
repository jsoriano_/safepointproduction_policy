/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    showRelatedPartySearch: function () {
        DCT.Util.hideShowGroup("relatedPartySearch", true);
        DCT.Util.hideShowGroup("relatedPartySearchData", true);
        DCT.Util.hideShowGroup("relatedPartyLink", false);
        DCT.Util.hideShowGroup("lookingForALabel", true);
        DCT.Util.hideShowGroup("lookingForAnLabel", false);
        var lookup = Ext.getCmp('lookingForSelectionId');
        var lookupRecord = lookup.getStore().getAt(0);
        var searchby = Ext.getCmp('searchBySelectionId');
        var searchbyRecord = searchby.getStore().getAt(0);
        lookup.fireEvent('select', lookup, lookupRecord);
        searchby.fireEvent('select', searchby, searchbyRecord);
    },
    /**
	*
	*/
    hideRelatedPartySearch: function () {
        DCT.Util.hideShowGroup("relatedPartyLink", true);
        DCT.Util.hideShowGroup("processSearch", false);
    }
});
