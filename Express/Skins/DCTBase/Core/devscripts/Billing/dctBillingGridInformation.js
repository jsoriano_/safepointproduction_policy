/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid,{
	/**
	*
	*/
	getDataKeyString: function (dataRecord, addId, keyArray, extraItemsArray) {
		var returnString = '';
		var separator = '|';
		if (addId)
			returnString = returnString + dataRecord.id;
		if (Ext.isArray(keyArray)){
			if (addId)
				returnString = returnString + separator;
			for (var i = 0; i < keyArray.length; i++) {
				returnString = returnString + dataRecord.data[keyArray[i]];
				if ((i+1) < keyArray.length)
					returnString = returnString + separator;
			}
		}
		if (Ext.isArray(extraItemsArray)){
			if (Ext.isArray(keyArray) || addId)
				returnString = returnString + separator;
			for (var i = 0; i < extraItemsArray.length; i++) {
				returnString = returnString + dataRecord.data[extraItemsArray[i]];
				if ((i+1) < extraItemsArray.length)
					returnString = returnString + separator;
			}
		}
		return(returnString);
	},
	/**
	*
	*/
	getItemIdForGridProcessing: function (dataRecord) {
		return (DCT.Grid.getDataKeyString(dataRecord, false, ['ItemObjectId']));
	},
	/**
	*
	*/
	getItemIdAndItemTypeForGridProcessing: function (dataRecord) {
		return (DCT.Grid.getDataKeyString(dataRecord, false, ['ItemObjectId', 'ItemObjectType']));
	},
	/**
	*
	*/
	getAgencyAgreementKeysForGridProcessing: function (dataRecord) {
		return (DCT.Grid.getDataKeyString(dataRecord, true, ['internalAmount'], ['N']));
	}	
});