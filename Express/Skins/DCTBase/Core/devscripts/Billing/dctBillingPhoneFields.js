
/**
*
*/
Ext.define('DCT.BillingPhoneField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingphonefield',
    xtype: 'dctbillingphonefield',

    /**
    *
    */
    setFormatter: function (config) {
        var me = this;
        config.formatter = me.formatPhone;
    },
    /**
    *
    */
    setVType: function (config) {
        config.vtype = 'phone';
    }
});
DCT.Util.reg('dctbillingphonefield', 'billingPhoneField', 'DCT.BillingPhoneField');

//==================================================
// Basic Enabling Phone control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnablingPhoneField', {
    extend: 'DCT.BillingPhoneField',

    inputXType: 'dctbillingenablingphonefield',
    xtype: 'dctbillingenablingphonefield',
    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    },
    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.enableDisable(textfield.getValue(), e);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingenablingphonefield', 'billingEnablingPhoneField', 'DCT.BillingEnablingPhoneField');