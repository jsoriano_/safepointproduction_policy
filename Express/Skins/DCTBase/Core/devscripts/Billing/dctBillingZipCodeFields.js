
/**
*
*/
Ext.define('DCT.BillingZipCodeField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingzipcodefield',
    xtype: 'dctbillingzipcodefield',

    /**
    *
    */
    setVType: function (config) {
        config.vtype = 'zipcode';
    }

});
DCT.Util.reg('dctbillingzipcodefield', 'billingZipCodeField', 'DCT.BillingZipCodeField');

//==================================================
// Basic Enabling ZipCode control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnablingZipCodeField', {
    extend: 'DCT.BillingZipCodeField',

    inputXType: 'dctbillingenablingzipcodefield',
    xtype: 'dctbillingenablingzipcodefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.enableDisable(textfield.getValue(), e);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingenablingzipcodefield', 'billingEnablingZipCodeField', 'DCT.BillingEnablingZipCodeField');

//==================================================
// Billing Payment PostCode Enable text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingPostCodeEnableZipCodeField', {
    extend: 'DCT.BillingEnablingZipCodeField',

    inputXType: 'dctbillingpostcodeenablezipcodefield',
    xtype: 'dctbillingpostcodeenablezipcodefield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    }

});
DCT.Util.reg('dctbillingpostcodeenablezipcodefield', 'billingPostCodeEnableZipCodeField', 'DCT.BillingPostCodeEnableZipCodeField');
