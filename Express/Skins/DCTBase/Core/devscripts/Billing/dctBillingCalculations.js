/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	updateStatementSummaryTotals: function(dataStore, newRecords, dataOptions){
		var itemCount = "0", totalGross = "0.00", totalCommission = "0.00", totalSurcharge = "0.00", totalFee = "0.00", totalNet = "0.00", totalRemitted = "0.00", totalBalance = "0.00";
	 	if (newRecords.length > 0){
		 	statementHeaderNode = Ext.DomQuery.select('StatementHeader', dataStore.proxy.reader.rawData);
			itemCount = Ext.DomQuery.selectValue('ItemCount',statementHeaderNode);
		 	totalGross = Ext.DomQuery.selectValue('TotalGross',statementHeaderNode);
		 	totalCommission = Ext.DomQuery.selectValue('TotalCommission',statementHeaderNode);
		 	totalSurcharge = Ext.DomQuery.selectValue('TotalSurcharge',statementHeaderNode);
		 	totalFee = Ext.DomQuery.selectValue('TotalFee',statementHeaderNode);
		 	totalNet = Ext.DomQuery.selectValue('TotalNet',statementHeaderNode);
		 	totalRemitted = Ext.DomQuery.selectValue('TotalRemitted',statementHeaderNode);
		 	totalBalance = Ext.DomQuery.selectValue('Balance',statementHeaderNode);
	  }
	 	Ext.getDom("itemsTotalId").innerHTML = itemCount;
	 	Ext.getDom("grossAmountId").innerHTML = totalGross;
	 	Ext.getDom("commissionAmountId").innerHTML = totalCommission;
	 	Ext.getDom("surchargesId").innerHTML = totalSurcharge;
	 	Ext.getDom("feesId").innerHTML = totalFee;
	 	Ext.getDom("netAmountId").innerHTML = totalNet;
	 	Ext.getDom("remittedAmountId").innerHTML = totalRemitted;
	 	Ext.getDom("balanceDueId").innerHTML = totalBalance;
	},
	/**
	*
	*/
	formatAmounts: function(currencyMask, amount){
		var itemAmount;
		if (typeof(amount) == "undefined")
			amount = "0.00";
		var numberFormat = Ext.create('DCT.Formatting', {numberType: 'float', numberMask: currencyMask});
	  itemAmount = numberFormat.formatFloat(amount);
	  if (itemAmount.charAt(0)=='-')
	  	itemAmount = '(' + itemAmount.substr(1) + ')';
	  return itemAmount;
	},
	/**
	*
	*/
	setPromiseToPayListTotals: function(){
	  var paymentAmt = DCT.Util.getSafeElement('_hiddenPaymentAmount').value;
	  var totalAllocationAmount = 0.0;
	  if (Ext.get("promiseToPayListGroup")){
	  	var numberFormat = Ext.create('DCT.Formatting', {numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value});
		  for (i=1; i < Ext.getDom('tblItemListDetail').rows.length; i++){
		    var defaultAmtObj = Ext.get('_hiddenProcessDefaultAmountId_' + i);
		    var amt = (defaultAmtObj) ? defaultAmtObj.getValue() : 0.00;
		    Ext.getDom('_hiddenProcessAmountId_' + i).value = Number(amt).toFixed(2);
		    Ext.getDom('_processAmountId_' + i).value = numberFormat.formatFloat(Ext.getDom('_hiddenProcessAmountId_' + i).value);
		    totalAllocationAmount += parseFloat(amt);
		  }
		  Ext.getDom('totalSuspenseAmtId').value = paymentAmt;
		  Ext.getDom('amountRemainingTotalId').value = Number(paymentAmt - totalAllocationAmount).toFixed(2);
		  Ext.get('displayTotalRemAmount').update(numberFormat.formatFloat(Ext.getDom('amountRemainingTotalId').value));
		  Ext.getDom('amountAllocatedTotalId').value = totalAllocationAmount.toFixed(2); ;
		  Ext.get('displayTotalAmount').update(numberFormat.formatFloat(Ext.getDom('amountAllocatedTotalId').value));
	  }
	}
});