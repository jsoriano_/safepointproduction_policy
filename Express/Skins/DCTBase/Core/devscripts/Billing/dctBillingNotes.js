/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    displayNotesPopUp: function () {
        var notesPopup = Ext.getCmp('notesPopup');
        if (notesPopup) {
            return;
        }
        var notes = Ext.create('DCT.BillingNotes', {});
        notes.show();
    }
});

/**
*
*/
Ext.define('DCT.BillingNotes', {
    extend: 'Ext.Window',

    dctControl: true,
    inputXType: 'dctbillingnotes',
    xtype: 'dctbillingnotes',
    title: DCT.T('AccountNotes'),
    width: 500,
    height: 300,
    id: 'notesPopup',
    minWidth: 300,
    minHeight: 200,
    maximizable: true,
    layout: 'fit',
    plain: true,
    bodyBorder: false,
    bodyStyle: 'padding:5px;',

    /**
    *
    */
    constructor: function (config) {
        var me = this;
        me.checkControlExists(config);
        me.setNotesItems(config);
        me.setListeners(config);
        me.callParent([config]);
    },
    /**
	*
	*/
    setNotesItems: function (config) {
        var me = this;
        config.items = Ext.create('Ext.form.FormPanel', {
            defaultType: 'textfield',
            labelWidth: 55,
            bodyBorder: false,
            id: 'notesPopupForm',
            formId: 'notesForm',
            items: [{
                style: 'margin-top:5px;',
                fieldLabel: DCT.T('Title'),
                name: '_activityTitle',
                id: '_activityTitle',
                anchor: '100%'
            }, {
                fieldLabel: DCT.T('Entry'),
                xtype: 'textarea',
                name: '_activityEntry',
                id: '_activityEntry',
                anchor: '100% -45'
            }, {
                hideLabel: true,
                hidden: true,
                tabIndex: -1,
                name: '_activityTitleHeader',
                value: DCT.T('Reference_colon')
            }, {
                hideLabel: true,
                hidden: true,
                tabIndex: -1,
                name: '_activityEntryHeader',
                value: DCT.T('Notes')
            }],
            buttons: [{
                text: DCT.T('Cancel'),
                id: '_cancelButton',
                handler: function () { 
                	var me = this;
                	me.closeNotes(); 
                },
                scope: me
            }, {
                text: DCT.T('Save'),
                id: '_saveButton',
                disabled: true,
                handler: function () { 
                	var me = this;
                	me.saveNotes(); 
                },
                scope: me
            }]
        });
    },
    /**
    *
    */
    setListeners: function (config) {
        var me = this;
        config.listeners = {
            show: {
                fn: function (control) {
                	var me = this;
                  me.setFocus();
                },
                scope: me
            },
            close: {
                fn: function () {
                	var me = this;
                  me.destroy();
                },
                scope: me,
                delay: 10
            }
        };
    },
    /**
    *
    */
    closeNotes: function () {
        var me = this;
        me.close();
    },
    /**
    *
    */
    saveNotes: function () {
        var me = this;
        var url = DCT.Util.getProxyUrl();

        var notesForm = Ext.getCmp('notesPopupForm');
        var strNameValuePair = notesForm.getForm().getValues();
        var strNameValuePairMainForm = DCT.Util.getFormNameValuePairs(document.forms[0], true);

        Ext.Ajax.request({
            url: url,
            params: Ext.apply(strNameValuePair, { _targetPage: 'addActivityLog', _gridRequest: '1' }, strNameValuePairMainForm),
            scope: me,
            success: me.success,
            failure: me.failure
        });
    },
    /**
    *
    */
    success: function (response, options) {
        var me = this;
        var responseMsg = Ext.DomQuery.select('/page/content', response.responseXML)[0].firstChild;
        var status = Ext.DomQuery.selectValue('@status', responseMsg);
        if (typeof (status) == 'undefined') {
            DCT.Util.removeTabs(true);
            Ext.Msg.alert(DCT.T('Alert'), DCT.T("InformationNotSaved"), function (btn) {
                DCT.Util.removeTabs(false);
                Ext.getCmp('_cancelButton').focus();
            });
        } else {
            me.closeNotes();
            if (DCT.currentPage == 'accountSummary')
                Ext.getCmp('activityList').getStore().reload();
        }
    },
    /**
    *
    */
    failure: function (response, options) {
        var me = this;
        DCT.Util.removeTabs(true);
        Ext.Msg.alert(DCT.T('Alert'), DCT.T("InformationNotSaved"), function (btn) {
            DCT.Util.removeTabs(false);
            me.closeNotes();
        });
    },
    /**
    *
    */
    enableSave: function () {
        Ext.getCmp('_saveButton').enable();
    },
    /**
    *
    */
    disableSave: function () {
        Ext.getCmp('_saveButton').disable();
    },
    /**
    *
    */
    setButtonState: function () {
        var me = this;
        var title = Ext.getCmp('_activityTitle');
        var entry = Ext.getCmp('_activityEntry');

        if (title && entry) {
            if (title.getValue() == '' || entry.getValue() == '')
                me.disableSave();
            else
                me.enableSave();
        }
    },
    /**
    *
    */
    setFocus: function () {
        var me = this;
        var activityTitle = Ext.getCmp('_activityTitle');
        var activityEntry = Ext.getCmp('_activityEntry');
        activityTitle.focus(false, 10);
        activityTitle.on('keyup', function () { me.setButtonState(); }, me);
        activityEntry.on('keyup', function () { me.setButtonState(); }, me);
    }
});
DCT.Util.reg('dctbillingnotes', 'billingNotes', 'DCT.BillingNotes');
