
/**
*
*/
Ext.define('DCT.BillingCreditCardField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingcreditcardfield',
    xtype: 'dctbillingcreditcardfield',

    /**
    *
    */
    setValidator: function (config) {
        var me = this;
        config.validator = me.checkCreditCard;
    },
    /**
    *
    */
    setMaskRe: function (config) {
        var regChars = "[0-9-]";
        config.maskRe = new RegExp(regChars, 'i');
    }

});
DCT.Util.reg('dctbillingcreditcardfield', 'billingCreditCardField', 'DCT.BillingCreditCardField');

//==================================================
// Basic Enabling ZipCode control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnablingCreditCardField', {
    extend: 'DCT.BillingCreditCardField',

    inputXType: 'dctbillingenablingcreditcardfield',
    xtype: 'dctbillingenablingcreditcardfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.enableDisable(textfield.getValue(), e);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingenablingcreditcardfield', 'billingEnablingCreditCardField', 'DCT.BillingEnablingCreditCardField');

//==================================================
// Billing Payment PostCode Enable text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingCheckFunctionEnableCardField', {
    extend: 'DCT.BillingEnablingCreditCardField',

    inputXType: 'dctbillingcheckfunctionenablecardfield',
    xtype: 'dctbillingcheckfunctionenablecardfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    }
});
DCT.Util.reg('dctbillingcheckfunctionenablecardfield', 'billingCheckFunctionEnableCardField', 'DCT.BillingCheckFunctionEnableCardField');