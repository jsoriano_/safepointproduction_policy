/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	*
	*/
	showHideManualAllocationGroup: function (show) {
		DCT.Util.hideShowGroup("manualAllocationGroup",show);
	},
	/**
	*
	*/
	showResuspendDisbursement: function (show) {
	  DCT.Util.hideShowGroup("reBillId",!show);
	  DCT.Util.hideShowGroup("resuspendDisbursementOptions",show);
	  DCT.Util.hideShowGroup("holdOptions",show);
	},
	/**
	*
	*/
	showHideManualAllocationFields: function (show) {
		DCT.Util.hideShowGroup("allocationDetail",show);
	},
	/**
	*
	*/
	showHidePaymentFields: function (passedPaymentMethod) {
	  var paymentMethod = (Ext.isDefined(Ext.getCmp("paymentMethod"))) ? (Ext.getCmp("paymentMethod").getValue()) : passedPaymentMethod;
	  var externalPartyPayment = Ext.get("_externalPartyPaymentIndicator");
	  var manuallyAllocateCheckbox = Ext.get("manuallyAllocateCheckbox");
	  var unidentifiedGroup = Ext.get("unidentifiedGroup");
	  
	  if (externalPartyPayment) {
	      var exPary = externalPartyPayment.getValue();
	      //If the 3rdPartyPaymentIndicator is set then enable the payment redirect for:
	      //Credit Cards, Phone Cards, Debit Cards
	      var paymentMethods = Ext.get("paymentMethods");
	      var use3rdPartyPayment = false;
	      if (paymentMethods) {
	          var array = paymentMethods.getValue().split('|');
	          var count;
	          var number = array.length;
	          for (count = 0; count <= number - 1; count++) {
	              if (paymentMethod == array[count]) {
	                  use3rdPartyPayment = true;
	              }
	          }
	      }
	      if (exPary == "True" && (use3rdPartyPayment)) {
	          DCT.Util.hideShowGroup("paymentAction",false);
	          DCT.Util.hideShowGroup("redirectAction",true);
	          if (manuallyAllocateCheckbox){
	              DCT.Util.hideShowGroup("manuallyAllocateCheckbox",false);
	          }
	          if(unidentifiedGroup){
	              DCT.Util.hideShowGroup("unidentifiedGroup",false);
	          }
	          paymentMethod = "EX";
	      }else{
	          DCT.Util.hideShowGroup("paymentAction",true);
	          DCT.Util.hideShowGroup("redirectAction",false);
	          if (manuallyAllocateCheckbox){
	              DCT.Util.hideShowGroup("manuallyAllocateCheckbox",true);
	          }
	          if (unidentifiedGroup){
	              DCT.Util.hideShowGroup("unidentifiedGroup",true);
	          }
	      }
	  }
	  
		DCT.Util.hideShowGroup("checkNumberHeader",(paymentMethod == "CK" || paymentMethod == "EC"));
		DCT.Util.hideShowGroup("checkNumberInput",(paymentMethod == "CK" || paymentMethod == "EC"));
	
		DCT.Util.hideShowGroup("postmarkDateHeader",(paymentMethod == "CK"));
		DCT.Util.hideShowGroup("postmarkDateInput",(paymentMethod == "CK"));
			
		DCT.Util.hideShowGroup("checkRecvDateHeader", (paymentMethod == "CK" || paymentMethod == "EC" || paymentMethod == "CS" || paymentMethod == "CC"));
		DCT.Util.hideShowGroup("checkRecvDateInput", (paymentMethod == "CK" || paymentMethod == "EC" || paymentMethod == "CS" || paymentMethod == "CC"));
	
		DCT.Util.hideShowGroup("bankRouteNumberHeader",(paymentMethod == "EC"));
		DCT.Util.hideShowGroup("bankRouteNumberInput",(paymentMethod == "EC"));
		
		DCT.Util.hideShowGroup("bankAccountNumberHeader",(paymentMethod == "EC"));
		DCT.Util.hideShowGroup("bankAccountNumberInput",(paymentMethod == "EC"));
		
		DCT.Util.hideShowGroup("bankAcctTypeHeader",(paymentMethod == "EC"));
		DCT.Util.hideShowGroup("bankAcctTypeInput",(paymentMethod == "EC"));
		
		DCT.Util.hideShowGroup("addressHeader",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("addressInput",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("cityHeader",(paymentMethod == "EC" ||  paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("cityInput",(paymentMethod == "EC" ||  paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("stateHeader",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("stateInput",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("postalCodeHeader",(paymentMethod == "EC" ||  paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("postalCodeInput",(paymentMethod == "EC" ||  paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("countryHeader",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("countryInput",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("phoneHeader",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("phoneInput",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("emailHeader",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("emailInput",(paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("firstNameCheckHeader",(paymentMethod == "CK" || paymentMethod == "EC"));
		DCT.Util.hideShowGroup("firstNameCardHeader",( paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("firstNameInput",(paymentMethod == "CK" || paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("lastNameCheckHeader",(paymentMethod == "CK" || paymentMethod == "EC"));
		DCT.Util.hideShowGroup("lastNameCardHeader",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("lastNameInput",(paymentMethod == "CK" || paymentMethod == "EC" || paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
	
		DCT.Util.hideShowGroup("firstNameAgentHeader",(false));
		DCT.Util.hideShowGroup("lastNameAgentHeader",(false));
	
		DCT.Util.hideShowGroup("cardTypeHeader",( paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("cardTypeInput",( paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("cardNumberHeader",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("cardNumberInput",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("cardVerificationHeader",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("cardVerificationInput",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		
		DCT.Util.hideShowGroup("cardExpirationHeader",(paymentMethod == "DC" || paymentMethod=="PAC" || paymentMethod=="PAD"));
		DCT.Util.hideShowGroup("cardExpirationInput",(paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
	
		var currencyCulture = Ext.get("_hiddenCultureCode").getValue();
		if (currencyCulture == "en-GB") {
		    DCT.Util.hideShowGroup("cardReferenceNumberHeader",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    DCT.Util.hideShowGroup("cardReferenceNumberInput",(paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
			    
		    DCT.Util.hideShowGroup("cardStartDateHeader",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    DCT.Util.hideShowGroup("cardStartDateInput",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
	
		    DCT.Util.hideShowGroup("cardIssueNumberHeader",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    DCT.Util.hideShowGroup("cardIssueNumberInput",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    
		    DCT.Util.hideShowGroup("cardAuthCodeHeader",( paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    DCT.Util.hideShowGroup("cardAuthCodeInput",(paymentMethod == "DC" || paymentMethod == "PAC" || paymentMethod == "PAD"));
		    
		}
		else {
		    DCT.Util.hideShowGroup("cardReferenceNumberHeader",(false));
		    DCT.Util.hideShowGroup("cardReferenceNumberInput",(false));
		    
		    DCT.Util.hideShowGroup("cardStartDateHeader",(false));
		    DCT.Util.hideShowGroup("cardStartDateInput",(false));
	
		    DCT.Util.hideShowGroup("cardIssueNumberHeader",(false));
		    DCT.Util.hideShowGroup("cardIssueNumberInput",(false));
		    
		    DCT.Util.hideShowGroup("cardAuthCodeHeader",(false));
		    DCT.Util.hideShowGroup("cardAuthCodeInput",(false));
		}
	},
	/**
	*
	*/
	hideSearchAccountGroup: function(show){
	  DCT.Util.hideShowGroup("accountTransferToAccountSearchId",show);
	  DCT.Util.hideShowGroup("transferProcessingGroup",!show);
	},
	/**
	*
	*/
	removeReferenceFields: function (hidePaymentDetail) {
		Ext.get("accountPolicyReferenceId").remove(false);
		if (Ext.get("accountFound"))
			Ext.get("accountFound").remove(false);
		Ext.getDom("_hiddenAccountId").value = "";
		Ext.getDom("_hiddenPolicyReference").value = "";
		if (hidePaymentDetail)
			DCT.Util.showHidePaymentDetails(false);
	},
	/**
	*
	*/
	showHidePaymentDetails: function(display){
		DCT.Util.hideShowGroup("paymentDetails", display);
		DCT.Util.hideShowByClass("paymentAction", display, "x-hidden");
	},
	/**
	*
	*/
	showHideReferenceSearchFields: function (display) {
		DCT.Util.hideShowGroup("accountPolicyNameSearchId", display);
	},
	/**
	*
	*/
	showHideReferenceFields: function (display) {
		DCT.Util.hideShowGroup("accountPolicyNameSearchId", display=='acctNotFound' || display=='changeAcct');
		DCT.Util.hideShowGroup("unidentifiedFieldsId", display=='unidentified');
	},
	/**
	*
	*/
	showHidePromisesToPay: function () {
		allPayRadio = Ext.getCmp('allPromiseToPayId');
		allPayChecked = (Ext.isDefined(allPayRadio)) ? allPayRadio.getValue() : false;
		if (allPayChecked)
			DCT.Util.setPromiseToPayListTotals();
		
		if (Ext.get("promiseToPayListGroup"))
			DCT.Util.hideShowGroup("promiseToPayListGroup", allPayChecked);
	},
	/**
	*
	*/
	showHideAgencyPaymentFields: function (paymentMethod) {
		DCT.Util.hideShowGroup("checkNumberHeader", paymentMethod == "CK");
		DCT.Util.hideShowGroup("checkNumberInput", paymentMethod == "CK");
	
		DCT.Util.hideShowGroup("postmarkDateHeader", paymentMethod == "CK");
		DCT.Util.hideShowGroup("postmarkDateInput", paymentMethod == "CK");
		
		DCT.Util.hideShowGroup("checkRecvDateHeader", paymentMethod == "CK" || paymentMethod == "CS");
		DCT.Util.hideShowGroup("checkRecvDateInput", paymentMethod == "CK" || paymentMethod == "CS");
	
		DCT.Util.hideShowGroup("firstNameCheckHeader", false);
		DCT.Util.hideShowGroup("firstNameAgentHeader", false);
		DCT.Util.hideShowGroup("firstNameInput", false);
		
		DCT.Util.hideShowGroup("lastNameCheckHeader", paymentMethod == "CK");
		DCT.Util.hideShowGroup("lastNameAgentHeader", false);
		DCT.Util.hideShowGroup("lastNameInput", paymentMethod == "CK");
		
		DCT.Util.hideShowGroup("firstNameCardHeader", false);
		DCT.Util.hideShowGroup("lastNameCardHeader", false);
	
		DCT.Util.hideShowGroup("wireTransferNumberHeader", paymentMethod == "WT");
		DCT.Util.hideShowGroup("wireTransferNumberInput", paymentMethod == "WT");
		
		DCT.Util.hideShowGroup("agentIdHeader", false);
		DCT.Util.hideShowGroup("agentIdInput", false);
	}
});
