/**
*
*/
Ext.define('DCT.BillingFloatField', {
    extend: 'DCT.BillingInputNumberField',

    inputXType: 'dctbillingfloatfield',
    xtype: 'dctbillingfloatfield',

    /**
    *
    */
    setValidator: function (config) {
        var me = this;
        config.validator = me.checkFloat;
    },
    /**
    *
    */
    setFormatter: function (config) {
        var me = this;
        config.formatter = config.formatting.formatFloat;
    },
    /**
    *
    */
    setCultureSymbols: function (config) {
        var me = this;
        config.getCultureSymbols = me.getFloatSymbols;
    },
    /**
    *
    */
    setStripSymbols: function (config) {
        var me = this;
        config.getStripSymbols = me.getFloatStripSymbols;
    }

});
DCT.Util.reg('dctbillingfloatfield', 'billingFloatField', 'DCT.BillingFloatField');

/**
*
*/
Ext.define('DCT.BillingBlurFloatField', {
    extend: 'DCT.BillingFloatField',

    inputXType: 'dctbillingblurfloatfield',
    xtype: 'dctbillingblurfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            blur: {
                fn: function (field) {
                    if (!Ext.isEmpty(field.getValue())) {
                        if (field.isValid() && field.isDirty()) {
                            me.formatField();
                            me.processValidField(field);
                        } else {
                            if (!field.isValid())
                                me.processInValidField(field);
                        }
                    }
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingblurfloatfield', 'billingBlurFloatField', 'DCT.BillingBlurFloatField');           

//==================================================
// Basic Enter Key text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnterFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctbillingenterfloatfield',
    xtype: 'dctbillingenterfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (floatfield, e) {
                    me.onEnter(floatfield, e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    onEnter: function (floatfield, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            if (floatfield.isValid())
                DCT.Submit.submitBillingPageAction(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
        }
    }
});
DCT.Util.reg('dctbillingenterfloatfield', 'billingEnterFloatField', 'DCT.BillingEnterFloatField');

/**
*
*/
Ext.define('DCT.ProcessAmountFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctprocessamountfloatfield',
    xtype: 'dctprocessamountfloatfield',

    /**
    *
    */
    processValidField: function (field) {
    	var me = this;
    	if (Ext.getDom('tblItemListDetail') != null) {
    		me.calculateAvailableandTotals(field);
    	}
        DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
        me.processHold();
    },
    /**
    *
    */
    processInValidField: function (field) {
        var me = this;
        if (!field.isValid())
            DCT.Util.enableDisableActionButton(true, me.dctActionButton);
    },
    /**
    *
    */
    calculateAvailableandTotals: function (field) {
        var me = this;
        var totalAvailElement, totalRemainElement, remainingElement;
        var availableFieldId, remainingFieldId;
        var processFieldId, processElement;
        var displayRemainFieldId, displayRemainElement;
        var currentAmount;
        var hiddenCurrentAmtId, hiddenCurrentAmtElement;

        currentAmount = me.formatting.formatNumberToUS(field.getValue());
        numOfRows = Ext.getDom('tblItemListDetail').rows.length - 1;
        hiddenCurrentAmtId = '_hiddenProcessAmountId_' + me.dctRowNumber;
        hiddenCurrentAmtElement = Ext.getDom(hiddenCurrentAmtId);
        hiddenCurrentAmtElement.value = parseFloat(currentAmount);
        availableFieldId = '_hiddenAmountId_' + me.dctRowNumber;
        availableElement = Ext.getDom(availableFieldId);
        remainingFieldId = '_hiddenRemAmountId_' + me.dctRowNumber;
        remainingElement = Ext.getDom(remainingFieldId);
        var remainingAmount = Number(parseFloat(availableElement.value) - parseFloat(currentAmount)).toFixed(2);
        remainingElement.value = remainingAmount;
        var remainingAmt = me.formatting.formatFloat(remainingElement.value);
        displayRemainFieldId = '_displayRemAmountId_' + me.dctRowNumber;
        displayRemainElement = Ext.get(displayRemainFieldId);
        displayRemainElement.update(remainingAmt);
        totalAvailElement = Ext.getDom(me.dctTotalAvailableField);
        totalRemainElement = Ext.getDom(me.dctTotalRemainingField);
        me.totalAvailAmt = 0.00;
        me.totalRemainAmt = 0.00;
        var processValue, remainValue;
        for (var row = 1; row <= numOfRows; row++) {
            remainingFieldId = '_hiddenRemAmountId_' + row;
            remainingElement = Ext.getDom(remainingFieldId);
            processFieldId = '_processAmountId_' + row;
            processElement = Ext.getCmp(processFieldId);
            if (processElement.getValue() == '' || (!processElement.isValid()))
                processValue = 0;
            else {
                processValue = me.formatting.formatNumberToUS(processElement.getValue());
                processValue = processValue.replace(/[^\d-.]/g, "")
            }
            if (remainingElement.value == '')
                remainValue = 0;
            else
                remainValue = remainingElement.value;
            me.totalAvailAmt = Number(Number(me.totalAvailAmt) + Number(parseFloat(processValue))).toFixed(2);
            me.totalRemainAmt = Number(Number(me.totalRemainAmt) + Number(parseFloat(remainValue))).toFixed(2);
        }

        totalAvailElement.value = Number(me.totalAvailAmt).toFixed(2);
        totalRemainElement.value = Number(me.totalRemainAmt).toFixed(2);
        var displayTotalAvailAmt = me.formatting.formatFloat(totalAvailElement.value);
        var displayTotalRemainAmt = me.formatting.formatFloat(totalRemainElement.value);
        Ext.get('displayTotalAmount').update(displayTotalAvailAmt);
        Ext.get('displayTotalRemAmount').update(displayTotalRemainAmt);

        return true;
    },
    /**
    *
    */
    blankCheck: function () {
        var me = this;
        var reasonCheck = (Ext.isEmpty(me.dctReasonField)) ? false : true;
        if (reasonCheck) {
            if (me.totalAvailAmt == 0 || Ext.isEmpty(Ext.getCmp(me.dctReasonField).getValue()))
                return true;
            else
                return false;
        } else {
            if (me.totalAvailAmt == 0)
                return true;
            else
                return false;
        }
    },
    /**
    *
    */
    processHold: function () {
        var me = this;
        if (Ext.isDefined(me.dctHoldCheckbox)) {
            Ext.getCmp(me.dctHoldCheckbox).enable();
            Ext.getCmp(me.dctHoldCheckbox).setValue(false);
            Ext.getDom(me.dctHiddenCheckbox).value = 0;
        }
    }

});
DCT.Util.reg('dctprocessamountfloatfield', 'processAmountFloatField', 'DCT.ProcessAmountFloatField');

/**
*
*/
Ext.define('DCT.AllocationAmountFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctallocationamountfloatfield',
    xtype: 'dctallocationamountfloatfield',

    /**
    *
    */
    processValidField: function (field) {
        var me = this;
        me.calculateAllocationandTotals(field);
        if (Ext.isDefined(me.dctCheckFunction))
            DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
    },
    /**
    *
    */
    processInValidField: function (field) {
        var me = this;
        if (Ext.isDefined(me.dctCheckFunction)) {
            if (!field.isValid())
                DCT.Util.enableDisableActionButton(true, me.dctActionButton);
        }
    },
    /**
    *
    */
    calculateAllocationandTotals: function (field) {
        var me = this;
        var totalAvailElement, totalRemainElement, remainingElement;
        var availableFieldId, remainingFieldId;
        var processFieldId, processElement;
        var displayRemainFieldId, displayRemainElement;
        var currentAmount;
        var hiddenCurrentAmtId, hiddenCurrentAmtElement;
        var isAgencyLite = (Ext.get('_hiddenAgencyLiteInd') && Ext.get('_hiddenAgencyLiteInd').getValue() == 'true');

        currentAmount = me.formatting.formatNumberToUS(field.getValue());
        numOfRows = Ext.getDom('tblItemListDetail').rows.length - 1;
        hiddenCurrentAmtId = '_hiddenProcessAmountId_' + me.dctRowNumber;
        hiddenCurrentAmtElement = Ext.getDom(hiddenCurrentAmtId);
        hiddenCurrentAmtElement.value = Number(parseFloat(currentAmount)).toFixed(2);
        totalAllocatedElement = Ext.getDom('amountAllocatedTotalId');
        totalRemainElement = Ext.getDom('amountRemainingTotalId');
        totalSuspenseElement = Ext.getDom('totalSuspenseAmtId');
        var totalAllocateAmt = 0.00;
        var processValue;
        for (var row = 1; row <= numOfRows; row++) {
            processFieldId = '_processAmountId_' + row;
            processElement = Ext.getCmp(processFieldId);
            if (processElement.getValue() == '' || (!processElement.isValid()))
                processValue = 0;
            else {
                processValue = me.formatting.formatNumberToUS(processElement.getValue());
                processValue = processValue.replace(/[^\d-.]/g, "")
            }
            totalAllocateAmt = Number(Number(totalAllocateAmt) + Number(parseFloat(processValue))).toFixed(2);
        }
        totalRemainAmt = Number(totalSuspenseElement.value) - Number(totalAllocateAmt);
        if (totalRemainAmt < 0 && !isAgencyLite) {
            field.validator = me.allocationValidator;
            field.dctTotalRemaining = me.formatting.formatFloat(totalRemainElement.value);
            field.validate();
            field.setValue(me.formatting.formatFloat('0.00'));
            field.focus();
            field.validator = me.checkFloat;
        } else {
            totalAllocatedElement.value = Number(totalAllocateAmt).toFixed(2);
            totalRemainElement.value = Number(totalRemainAmt).toFixed(2);
            var displayTotalAllocateAmt = me.formatting.formatFloat(totalAllocatedElement.value);
            var displayTotalRemainAmt = me.formatting.formatFloat(totalRemainElement.value);
            Ext.get('displayTotalAmount').update(displayTotalAllocateAmt);
            Ext.get('displayTotalRemAmount').update(displayTotalRemainAmt);
        }
    },
    /**
    *
    */
    blankCheck: function () {
        var me = this;
        return (me.dctCheckFunction());
    },
    /**
    *
    */
    allocationValidator: function (fieldValue) {
        var me = this;
        if (!Ext.isEmpty(fieldValue)) {
            return DCT.T("AmountExceedsAllocation", { max: me.dctTotalRemaining });
        }
        return true;
    }
});
DCT.Util.reg('dctallocationamountfloatfield', 'allocationAmountFloatField', 'DCT.AllocationAmountFloatField');           

//==================================================
// Billing Control Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingControlNbrFloatField', {
    extend: 'DCT.BillingEnterFloatField',

    inputXType: 'dctbillingcontrolnbrfloatfield',
    xtype: 'dctbillingcontrolnbrfloatfield',

    /**
 	*
 	*/
    processValidField: function (field) {
        var me = this;
        DCT.Util.getSafeElement(me.dctHiddenControlTotal).value = me.formatting.formatNumberToUS(field.getValue());
    }
});
DCT.Util.reg('dctbillingcontrolnbrfloatfield', 'billingControlNbrFloatField', 'DCT.BillingControlNbrFloatField');
//==================================================
// Billing Edit Control Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEditControlFloatField', {
    extend: 'DCT.BillingControlNbrFloatField',

    inputXType: 'dctbillingeditcontrolfloatfield',
    xtype: 'dctbillingeditcontrolfloatfield',

    /**
 	*
 	*/
    onEnter: function (value, e) {
    }
});
DCT.Util.reg('dctbillingeditcontrolfloatfield', 'billingEditControlFloatField', 'DCT.BillingEditControlFloatField');
//==================================================
// Billing Payment Amount text control 
//==================================================

/**
*
*/
Ext.define('DCT.PaymentAmountFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctpaymentamountfloatfield',
    xtype: 'dctpaymentamountfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    if (floatfield.isValid()) {
                        DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
                        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(floatfield.getValue());
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processValidField: function (field) {
        var me = this;
        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(field.getValue());
        if (Ext.isDefined(me.dctCheckFunction))
            DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
        if (Ext.isDefined(me.dctSetTotalsFunction))
            me.dctSetTotalsFunction();
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.dctCheckFunction());
    }
});
DCT.Util.reg('dctpaymentamountfloatfield', 'paymentAmountFloatField', 'DCT.PaymentAmountFloatField');           
//==================================================
// Billing Commission Percent text control 
//==================================================

/**
*
*/
Ext.define('DCT.CommissionPercentFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctcommissionpercentfloatfield',
    xtype: 'dctcommissionpercentfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    DCT.Util.enableDisableActionButton((floatfield.getValue() == ''), me.dctActionButton);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processValidField: function (field) {
        var me = this;
        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(field.getValue());
    }
});
DCT.Util.reg('dctcommissionpercentfloatfield', 'commissionPercentFloatField', 'DCT.CommissionPercentFloatField');           
//==================================================
// Billing AddStatement float control 
//==================================================

/**
*
*/
Ext.define('DCT.AddStatementFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctaddstatementfloatfield',
    xtype: 'dctaddstatementfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processValidField: function (field) {
        var me = this;
        me.setItemAmounts(field);
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.checkIfAddItemFieldsBlank());
    }
});
DCT.Util.reg('dctaddstatementfloatfield', 'addStatementFloatField', 'DCT.AddStatementFloatField');           
//==================================================
// Billing AddStatement float control 
//==================================================

/**
*
*/
Ext.define('DCT.OtherChargeFloatField', {
    extend: 'DCT.AddStatementFloatField',

    inputXType: 'dctotherchargefloatfield',
    xtype: 'dctotherchargefloatfield',

    /**
 	*
 	*/
    processValidField: function (field) {
        var me = this;
        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(field.getValue());
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.checkIfAddItemFieldsBlank());
    }
});
DCT.Util.reg('dctotherchargefloatfield', 'otherChargeFloatField', 'DCT.OtherChargeFloatField');

/**
*
*/
Ext.define('DCT.OtherAmountFloatField', {
    extend: 'DCT.BillingBlurFloatField',

    inputXType: 'dctotheramountfloatfield',
    xtype: 'dctotheramountfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    if (floatfield.isValid()) {
                        DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
                        me.processKeyUp(floatfield, e);
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processValidField: function (field) {
        var me = this;
        me.setManualAllocationTotals(field, field.getValue());
    },
    /**
	*
	*/
    processInValidField: function (field) {
        if (!field.isValid())
            DCT.Util.enableDisableActionButton(true, me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.dctCheckFunction());
    },
    /**
	*
	*/
    processKeyUp: function (floatfield, e) {
        var me = this;
        var otherAmount = floatfield.formatting.formatNumberToUS(floatfield.getValue());
        if (parseFloat(otherAmount) > 0)
            DCT.Util.showHideManualAllocationGroup(Ext.getCmp(me.dctPaymentTypeField) != 'A');
        else
            DCT.Util.showHideManualAllocationGroup(false);
        var extOtherAmount = Ext.getCmp(me.dctOtherAmountRadio);
        extOtherAmount.setValue(true);
    }

});
DCT.Util.reg('dctotheramountfloatfield', 'otherAmountFloatField', 'DCT.OtherAmountFloatField');           

//==================================================
// Billing Control Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingSearchPaymentFloatField', {
    extend: 'DCT.BillingEnterFloatField',

    inputXType: 'dctbillingsearchpaymentfloatfield',
    xtype: 'dctbillingsearchpaymentfloatfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    if (floatfield.isValid()) {
                        DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
                        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(floatfield.getValue());
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processValidField: function (field) {
        var me = this;
        DCT.Util.getSafeElement(me.dctHiddenField).value = me.formatting.formatNumberToUS(field.getValue());
    },
    /**
    *
    */
    processInValidField: function (field) {
        var me = this;
        if (!field.isValid())
            DCT.Util.enableDisableActionButton(true, me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.checkIfPaymentSearchFieldsBlank());
    }
});
DCT.Util.reg('dctbillingsearchpaymentfloatfield', 'billingSearchPaymentFloatField', 'DCT.BillingSearchPaymentFloatField');

/**
*
*/
Ext.define('DCT.WriteOffProcessAmountFloatField', {
    extend: 'DCT.ProcessAmountFloatField',

    inputXType: 'dctwriteoffprocessamountfloatfield',
    xtype: 'dctwriteoffprocessamountfloatfield',

    /**
 	*
 	*/
    calculateAvailableandTotals: function (field) {
        var me = this;
        var availableFieldId, availableElement;
        var waivedFieldId, waivedElement;

        if (!field.isValid())
            field.setValue('');
        var currentAmount = me.formatting.formatNumberToUS(field.getValue());
        var linkedFieldId = me.dctLinkedField + me.dctRowNumber;
        var linkedFieldElement = Ext.getDom(linkedFieldId);
        linkedFieldElement.value = parseFloat(currentAmount);

        var numOfRows = Ext.getDom('tblItemListDetail').rows.length - 1;

        var totalAvailElement = Ext.getDom(me.dctTotalAvailableField);
        var totalRemainElement = Ext.getDom(me.dctTotalRemainingField);
        me.totalAvailAmt = 0.00;
        me.totalRemainAmt = 0.00;
        for (var row = 1; row <= numOfRows; row++) {
            availableFieldId = me.dctAvailableField + row;
            availableElement = Ext.getDom(availableFieldId);
            waivedFieldId = me.dctLinkedField + row;
            waivedElement = Ext.getDom(waivedFieldId);
            if (waivedElement.value == '')
                waivedValue = 0;
            else {
                waivedValue = me.formatting.formatNumberToUS(waivedElement.value);
                waivedValue = waivedValue.replace(/[^\d-.]/g, "")
            }
            if (availableElement.value == '')
                availableValue = 0;
            else
                availableValue = availableElement.value;
            me.totalAvailAmt = Number(Number(me.totalAvailAmt) + Number(parseFloat(waivedValue))).toFixed(2);
            me.totalRemainAmt = Number(Number(me.totalRemainAmt) + (Number(parseFloat(availableValue)) - Number(parseFloat(waivedValue)))).toFixed(2);
        }

        totalAvailElement.value = Number(me.totalAvailAmt).toFixed(2);
        totalRemainElement.value = Number(me.totalRemainAmt).toFixed(2);
        var displayTotalAvailAmt = me.formatting.formatFloat(totalAvailElement.value);
        var displayTotalRemainAmt = me.formatting.formatFloat(totalRemainElement.value);
        Ext.get(me.dctTotalAvailableText).update(displayTotalAvailAmt);
        Ext.get(me.dctTotalRemainingText).update(displayTotalRemainAmt);

        return true;
    },
    /**
 	*
 	*/
    blankCheck: function () {
        var me = this;
        var reason = Ext.getCmp(me.dctReBillField).getValue();
        var hold = Ext.getCmp(me.dctReasonField).getValue();

        if (Ext.isEmpty(reason) || Ext.isEmpty(hold))
            return true;
        else
            return false;
    }

});
DCT.Util.reg('dctwriteoffprocessamountfloatfield', 'writeOffProcessAmountFloatField', 'DCT.WriteOffProcessAmountFloatField');           
