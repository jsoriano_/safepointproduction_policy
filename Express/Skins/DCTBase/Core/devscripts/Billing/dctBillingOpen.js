
/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
    /**
	*
	*/
    openAccountSummary: function (page, accountId, linkId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_accountId', accountId);
        me.submitPageForm(page, linkId);
    },
    /**
	*
	*/
    openReport: function (page, reportPath, historyId, appCode, linkId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_reportPath', reportPath);
        me.addToParmArray('_historyId', historyId);
        me.addToParmArray('_appCode', appCode);
        me.submitPageForm(page, linkId);
    },
    /**
	*
	*/
    exportReport: function (page, reportPath, historyId, exportFormat, linkId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_reportPath', reportPath);
        me.addToParmArray('_historyId', historyId);
        me.addToParmArray('_exportFormat', exportFormat);
        me.addToParmArray('_submitAction', 'reportExport');
        me.submitPageForm(page, linkId);
        Ext.getDom('_submitAction').value = '';
    },
    /**
	*
	*/
    openCandidateListAccount: function (page, gridId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_accountId', Ext.getCmp(gridId).getSelectionModel().getSelection()[0].id);
        me.submitPageForm(page, 'processContinue');
    },
    /**
	*
	*/
    openPolicySummary: function (page, policyId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_policyId', policyId);
        me.submitPageForm(page);
    },
    /**
	*
	*/
    viewManualInstallRedistribute: function (page, technicalId, detailTypeCode) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_technicalId', technicalId);
        me.addToParmArray('_detailTypeCode', detailTypeCode);
        me.submitPageForm(page);
    },
    /**
	*
	*/
    viewInstallmentLevelRedistribute: function (page, installmentDate) {
    	var me = this;
    	me.clearParmArray();
    	me.addToParmArray('_targetPage', page);
    	me.addToParmArray('_installmentDate', installmentDate);
    	me.submitPageForm(page);
   },
	/**
	*
	*/
	viewIndividualDueDateChange: function (page, installmentDate, originalDueDate) {
		var me = this;
		me.clearParmArray();
		me.addToParmArray('_targetPage', page);
		me.addToParmArray('_installmentDate', installmentDate);
		me.addToParmArray('_originalDueDate', originalDueDate);
		me.submitPageForm(page);
	},
	/**
	*
	*/
    viewWriteOffWaiveDetail: function (page, technicalId, detailTypeCode) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', page);
        me.addToParmArray('_technicalId', technicalId);
        me.addToParmArray('_detailTypeCode', detailTypeCode);
        me.submitPageForm(page);
    },
    /**
	*
	*/
    openTargetPageAfterSearch: function (targetPage, targetAction, gridId) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', targetPage);
        me.addToParmArray('_submitAction', targetAction);
        me.addToParmArray('_searchById', Ext.getCmp(gridId).getSelectionModel().getSelection()[0].id);
        me.addToParmArray('_searchByNbr', Ext.getCmp(gridId).getSelectionModel().getSelection()[0].data['AccountReference']);
        me.addToParmArray('_searchMode', 'accountWithId');
        me.submitPageForm("", 'processContinue');
    },
    /**
	*
	*/
    gotoScheduleItemDetail: function (itemId, itemTypeCode) {
        var me = this;
        me.clearParmArray();
        me.addToParmArray('_targetPage', 'scheduledItemDetail');
        me.addToParmArray('_itemId', itemId);
        me.addToParmArray('_itemTypeCode', itemTypeCode);
        me.submitPageForm('scheduledItemDetail');
    },
    /**
	*
	*/
    viewPaymentDetails: function (paymentId, paymentType, statusCode) {
        var me = this;
        me.clearParmArray();
        var targetPage = '';
        if (paymentType != 'UNID' || statusCode == 'REV') {
            me.addToParmArray('_technicalId', paymentId);
            me.addToParmArray('_targetPage', 'searchPaymentDetails');
            targetPage = 'searchPaymentDetails';
        } else {
            me.addToParmArray('_unidentifiedPaymentId', paymentId);
            me.addToParmArray('_targetPage', 'unidentifiedPaymentsDetail');
            targetPage = 'unidentifiedPaymentsDetail';
        }
        me.submitPageForm(targetPage);
    },
    /**
	*
	*/
    gotoBillingAccountPage: function (accountId) {
        var me = this;
        if (DCT.BillingSystemURL != '')
            document.forms[0].action = DCT.BillingSystemURL;
        me.clearParmArray();
        me.addToParmArray('_PortalName', 'Billing');
        me.addToParmArray('_targetPage', 'accountSummary');
        me.addToParmArray('_autoSaveState', "");
        me.addToParmArray('_accountId', accountId);
        DCT.FieldsChanged.clear();
        DCT.Submit.setActiveParent('billingDashBoard');
        me.addToParmArray('_submitAction', 'setPortal');
        me.addToParmArray('_SuppressAutoSave', '1');
        me.submitPageForm('accountSummary');
    },
    /**
	*
	*/
    gotoBillingPolicyTermSummaryPage: function (accountId, policyTermId) {
        var me = this;
        if (DCT.BillingSystemURL != '')
            document.forms[0].action = DCT.BillingSystemURL;
        me.clearParmArray();
        me.addToParmArray('_PortalName', 'Billing');
        me.addToParmArray('_targetPage', 'policySummary');
        me.addToParmArray('_autoSaveState', "");
        me.addToParmArray('_accountId', accountId);
        me.addToParmArray('_policyId', policyTermId);
        DCT.FieldsChanged.clear();
        DCT.Submit.setActiveParent('billingDashBoard');
        me.addToParmArray('_submitAction', 'setPortal');
        me.submitPageForm('policySummary');
    },
    /**
	*
	*/
    gotoPASPolicyPage: function (policyId) {
        var me = this;
        if (DCT.PolicySystemURL != '')
            document.forms[0].action = DCT.PolicySystemURL;

        me.clearParmArray();
        me.addToParmArray('_PortalName', 'AdminPAS');
        me.addToParmArray('_targetPage', 'policy');
        me.addToParmArray('_autoSaveState', "");
        me.addToParmArray('_QuoteId', policyId);
        DCT.FieldsChanged.clear();
        DCT.Submit.setActiveParent('dashboardHome');
        me.addToParmArray('_submitAction', 'setPortal');
        me.submitPageForm('policy');
    }
});