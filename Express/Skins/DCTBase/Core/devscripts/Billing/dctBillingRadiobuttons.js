/**
*
*/
Ext.define('DCT.BillingRadioField', {
    extend: 'DCT.SystemRadioField',

    inputXType: 'dctbillingradiofield',
    xtype: 'dctbillingradiofield',

    /**
	*
	*/
    setListeners: function (config) {
    },
    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
    },
    /**
	*
	*/
    enableDisableAction: function (radioButton, checked) {
    },
    /**
	*
	*/
    blankCheck: function (radioButton, checked) {
    },
    /**
	*
	*/
    otherCheckProcess: function (radioButton, checked) {
    },
    /**
	*
	*/
    setDisable: function (config) {
        config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
    }
});


/**
*
*/
Ext.define('DCT.BillingBasicRadioField', {
    extend: 'DCT.BillingRadioField',

    inputXType: 'dctbillingbasicradiofield',
    xtype: 'dctbillingbasicradiofield',

    /**
	*
	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            change: {
                fn: function (radiobutton, checked, oldValue, eOpts) {
                    me.showHideFields(radiobutton, checked);
                    me.enableDisableAction(radiobutton, checked);
                    me.otherCheckProcess(radiobutton, checked);
                },
                scope: me
            }
        });
    }
});


/**
*
*/
Ext.define('DCT.BillingPaymentRadioField', {
    extend: 'DCT.BillingBasicRadioField',

    inputXType: 'dctbillingpaymentradiofield',
    xtype: 'dctbillingpaymentradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        var removeData = Ext.isDefined(radioButton.dctRemoveData) ? radioButton.dctRemoveData : true;
        if (checked) {
            var fieldValue = radioButton.getSubmitValue();
            DCT.Util.hideShowGroup("accountPolicyNameSearchId", fieldValue == "accountPolicyName");
            DCT.Util.hideShowGroup("unidentifiedFieldsId", fieldValue == "unidentified");

            if (removeData) {
                if (fieldValue == "accountPolicyName") {
                    Ext.getCmp("unidentifedLastname").setValue("");
                    Ext.getCmp("unidentifedFirstname").setValue("");
                    Ext.getCmp("unidentifedMiddlename").setValue("");
                    Ext.getCmp("unidentifedAccount").setValue("");
                    Ext.getCmp("unidentifedPolicy").setValue("");
                    Ext.getCmp("unidentifedOtherInfo").setValue("");
                } else {
                    Ext.getCmp("searchByNbr").setValue("");
                }
            }
            if (radioButton.dctHidePaymentDetail) DCT.Util.showHidePaymentDetails(!DCT.Util.checkIfReferenceFieldsBlank());
            if (!Ext.isEmpty(Ext.get("_hiddenAccountId").getValue())) DCT.Util.removeReferenceFields(radioButton.dctHidePaymentDetail);
        }
    }
});


/**
*
*/
Ext.define('DCT.BillingEditPaymentRadioField', {
    extend: 'DCT.BillingPaymentRadioField',

    inputXType: 'dctbillingeditpaymentradiofield',
    xtype: 'dctbillingeditpaymentradiofield',

    /**
	*
	*/
    enableDisableAction: function (radioButton, checked) {
        var me = this;
        DCT.Util.enableDisableActionButton(me.blankCheck(radioButton, checked), me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function (radioButton, checked) {
        return (DCT.Util.checkIfPaymentFieldsBlank() || DCT.Util.checkIfReferenceFieldsBlank(radioButton.getSubmitValue()));
    }

});


/**
*
*/
Ext.define('DCT.BillingUnidentifiedPaymentRadioField', {
    extend: 'DCT.BillingEditPaymentRadioField',

    inputXType: 'dctbillingunidentifiedpaymentradiofield',
    xtype: 'dctbillingunidentifiedpaymentradiofield',

    /**
	*
	*/
    blankCheck: function (radioButton, checked) {
        var me = this;
        return (me.dctCheckFunction());
    },
    /**
	*
	*/
    otherCheckProcess: function (radioButton, checked) {
        var actionButton = Ext.get('paymentAction');
        var holdGroup = Ext.get('placeOnHoldGroup');
        if (actionButton) actionButton.remove(false);
        if (holdGroup) holdGroup.remove(false);
    }
});


/**
*
*/
Ext.define('DCT.BillingHoldRadioField', {
    extend: 'DCT.BillingBasicRadioField',

    inputXType: 'dctbillingholdradiofield',
    xtype: 'dctbillingholdradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked)
            DCT.Util.hideShowGroup("followUpOptionsRadiobuttons", radioButton.getSubmitValue() == "HB");
    }
});


/**
*
*/
Ext.define('DCT.BillingHoldAccountRadioField', {
    extend: 'DCT.BillingBasicRadioField',

    inputXType: 'dctbillingholdaccountradiofield',
    xtype: 'dctbillingholdaccountradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked) {
            var fieldValue = radioButton.getSubmitValue();			
			DCT.Util.enableDisableActionButton(true, 'holdAction');
            DCT.Util.hideShowGroup("holdIndividualAccountGroup", (fieldValue == 'I'));
            DCT.Util.hideShowGroup("holdEventsListId", (fieldValue == 'A'));
            DCT.Util.hideShowGroup("holdBillFollowUpTitle", (fieldValue == 'A'));
            DCT.Util.hideShowGroup("holdDisbursementEventsListId", (fieldValue == 'A'));
            DCT.Util.hideShowGroup("holdDisbursementsTitle", (fieldValue == 'A'));			
            if (fieldValue == 'A')
    		{
				var currentProcessingDate = new Date(Date.parse(DCT.Util.getSafeElement('_currentSystemDate').value));
    			Ext.getCmp('holdTypeCode').setValue("");
    			Ext.getCmp('holdDisbReasonCode').setValue("");
    			Ext.getCmp('holdReasonCode').setValue("");
    			Ext.getCmp('holdStartDate').setValue(currentProcessingDate);
    			Ext.getCmp('_noOption').setValue(true);
    			Ext.getCmp('holdBillingIndefinitely').setValue(true);
				DCT.Util.hideShowGroup("followUpOptionsRadiobuttons", false);
    		}
			if (fieldValue == 'I') {
				if (Ext.getCmp('holdEventsList')) {
					this.clearGridSelection("holdEventsList");
				}
				if (Ext.getCmp('holdDisbursementEventsList')) {
					this.clearGridSelection("holdDisbursementEventsList");
				}
			}
			Ext.getCmp('holdEndDate').setDisabled(true);			
		}
    },	
	clearGridSelection: function (gridId) {
		var grid = Ext.getCmp(gridId);
		if (grid) {
			var selectModel = grid.getSelectionModel(); 
			var selection = grid.getSelectionModel().getSelection()[0];
			if (selection) {
				var selectedRow = grid.store.indexOf(selection);				
				selectModel.deselect(selectedRow);
			}
        }
    }
});


/**
*
*/
Ext.define('DCT.BasicSearchPaymentRadioField', {
    extend: 'DCT.BillingBasicRadioField',

    inputXType: 'dctbasicsearchpaymentradiofield',
    xtype: 'dctbasicsearchpaymentradiofield',

    /**
	*
	*/
    enableDisableAction: function (radioButton, checked) {
        var me = this;
        DCT.Util.enableDisableActionButton(me.blankCheck(radioButton, checked), me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function (radioButton, checked) {
        var me = this;
        return (me.checkIfPaymentSearchFieldsBlank());
    }
});


/**
*
*/
Ext.define('DCT.BillingAmountRadioField', {
    extend: 'DCT.BasicSearchPaymentRadioField',

    inputXType: 'dctbillingamountradiofield',
    xtype: 'dctbillingamountradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked) {
            var fieldValue = radioButton.getSubmitValue();
            DCT.Util.hideShowGroup("exactAmt", fieldValue == 'exact');
            DCT.Util.hideShowGroup("amtRangeFieldGroup", fieldValue == 'range');
            if (fieldValue == 'range') {
                Ext.getCmp("startAmount").setValue("");
                if (Ext.get("_startAmountHidden"))
                    Ext.getDom("_startAmountHidden").value = "";
                Ext.getCmp("endAmount").setValue("");
                if (Ext.get("_endAmountHidden"))
                    Ext.getDom("_endAmountHidden").value = "";
            } else {
                Ext.getCmp("exactAmount").setValue("");
                if (Ext.get("_exactAmountHidden"))
                    Ext.getDom("_exactAmountHidden").value = "";
            }
        }
    }
});


/**
*
*/
Ext.define('DCT.BillingDateRadioField', {
    extend: 'DCT.BasicSearchPaymentRadioField',

    inputXType: 'dctbillingdateradiofield',
    xtype: 'dctbillingdateradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked) {
            var fieldValue = radioButton.getSubmitValue();
            DCT.Util.hideShowGroup("exactDt", fieldValue == 'exact');
            DCT.Util.hideShowGroup("dateRangeFieldGroup", fieldValue == 'range');

            if (fieldValue == 'range') {
                Ext.getCmp("startDate").setValue("");
                Ext.getCmp("endDate").setValue("");
            } else {
                Ext.getCmp("exactDate").setValue("");
            }
        }
    }
});


/**
*
*/
Ext.define('DCT.BillingMakePaymentRadioField', {
    extend: 'DCT.BillingBasicRadioField',

    inputXType: 'dctbillingmakepaymentradiofield',
    xtype: 'dctbillingmakepaymentradiofield',

    /**
	*
	*/
    enableDisableAction: function (radioButton, checked) {
        var me = this;
        DCT.Util.enableDisableActionButton(me.blankCheck(radioButton, checked), me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function (radioButton, checked) {
        var me = this;
        return (me.dctCheckFunction());
    },
    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        var me = this;
        if (checked) {
            var otherAmountCmp = Ext.getCmp('_otherAmountId');
            var fieldValue = radioButton.getSubmitValue();
            if (fieldValue != 'otherAmount') {
                var extComponent = Ext.getCmp("manuallyAllocatePaymentId");
                extComponent.setValue(false);
                DCT.Util.showHideManualAllocationGroup(false);
                me.setManualAllocationTotals(otherAmountCmp, me.dctAmount);
            } else {
                var otherAmount = otherAmountCmp.formatting.formatNumberToUS(otherAmountCmp.getValue());
                DCT.Util.getSafeElement('_hiddenPaymentAmount').value = otherAmount;
                if (parseFloat(otherAmount) > 0)
                    DCT.Util.showHideManualAllocationGroup(Ext.getCmp(me.dctPaymentTypeField) != 'A');
            }
        }
    }
});


/**
*
*/
Ext.define('DCT.BillingAgencyPaymentRadioField', {
    extend: 'DCT.BillingMakePaymentRadioField',

    inputXType: 'dctbillingagencypaymentradiofield',
    xtype: 'dctbillingagencypaymentradiofield',

    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked) {
            var fieldValue = radioButton.getSubmitValue();
            if (fieldValue == 'allPromiseToPay')
                DCT.Util.setPromiseToPayListTotals();
        }
        if (radioButton.id == 'allPromiseToPayId') {
            var promiseToPayList = Ext.get("promiseToPayListGroup");
            if (Ext.isDefined(promiseToPayList))
                DCT.Util.hideShowGroup(promiseToPayListGroup, checked);
        }
    }
});


Ext.define('DCT.BillingRedistributionRadioField', {
    extend: 'DCT.BillingRadioField',

    inputXType: 'dctbillingredistributionradiofield',
    xtype: 'dctbillingredistributionradiofield',

    /**
	*
	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            change: {
                fn: function (radiobutton, checked, oldValue, eOpts) {
                    me.showHideFields(radioButton, checked);


                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    showHideFields: function (radioButton, checked) {
        if (checked) {
            var value = radioButton.getSubmitValue();
            var suspendAnchor = Ext.get('_redistributeAction');
            suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');

            DCT.Util.hideShowGroup(((value == 'accountLevel') ? "InstallmentInputs" : "billingRedistributionListId"), true);
            DCT.Util.hideShowGroup(((value == 'accountLevel') ? "billingRedistributionListId" : "InstallmentInputs"), false);
            if (value == 'policyLevel') {
                Ext.getCmp("startDate").setValue("");
                Ext.getCmp("numberOfInstallments").setValue("");
            }
            else if (value == 'accountLevel') {

                Ext.getCmp("RedistributionList").getSelectionModel().clearSelections();

            }
        }
    }
});
