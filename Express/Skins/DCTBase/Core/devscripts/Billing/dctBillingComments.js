/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    displayBillingComment: function (gridId, extRecordId, commentText, cmtCreationDate, cmtOriginator) {
        var me = this;
        var displayComments = Ext.getCmp('cmtsDisplayPopup');
        if (displayComments) {
            return;
        }
        me.removeTabs(true);
        var commentsDisplayPanel = Ext.create('DCT.BillingDisplayComment', {
            dctExtRecordId: extRecordId,
            dctCommentText: commentText,
            dctCmtCreationDate: cmtCreationDate,
            dctCmtOriginator: cmtOriginator,
            dctDataStore: (gridId != null) ? Ext.getCmp(gridId).getStore() : null
        });
        var commentsWindow = Ext.create('DCT.BaseWindow', {
            title: DCT.T('Comments'),
            width: 400,
            height: 400,
            id: 'cmtsDisplayPopup',
            items: commentsDisplayPanel,
            dctFocusFieldId: '_cmtCancelButton'
        });
        commentsWindow.show();
    },
    /**
	*
	*/
    displayAddBillingCommentsPopUp: function (targetPage, gridData, itemId, itemTypeCode, hiddenCommentField, hiddenCmtTypeCode, viewAnchorField, hideOnStatementCheckbox) {
        var addComments = Ext.getCmp('commentsAddPopup');
        if (addComments) {
            return;
        }
        var commentsAddPanel = Ext.create('DCT.BillingAddComment', {
            dctTargetPage: targetPage,
            dctDataStore: gridData,
            dctItemId: itemId,
            dctItemTypeCode: itemTypeCode,
            dctHiddenCommentField: hiddenCommentField,
            dctHiddenCmtTypeCode: hiddenCmtTypeCode,
            dctViewAnchorField: viewAnchorField,
            dctHideOnStatementCheckbox: hideOnStatementCheckbox
        });
        var commentsWindow = Ext.create('DCT.BaseWindow', {
            title: DCT.T('AddItemComment'),
            width: 500,
            height: 300,
            id: 'commentsAddPopup',
            items: commentsAddPanel,
            dctFocusFieldId: '_commentEntry'
        });
        commentsWindow.show();
    }
});

/**
*
*/
Ext.define('DCT.BillingAddComment', {
    extend: 'DCT.AddComment',

    inputXType: 'dctbillingaddcomment',
    xtype: 'dctbillingaddcomment',

    /**
	*
	*/
    setPanelItems: function (config) {
        var me = this;
        config.items = [{
            style: 'margin-top:5px;',
            hideLabel: true,
            boxLabel: DCT.T("ShowCommentOnStatement"),
            xtype: 'checkbox',
            name: '_commentOnStatement',
            id: '_commentOnStatement',
            anchor: '100%',
            hidden: (Ext.isDefined(config.dctHideOnStatementCheckbox)) ? config.dctHideOnStatementCheckbox : false
        }, {
            hideLabel: true,
            xtype: 'textarea',
            name: '_commentEntry',
            id: '_commentEntry',
            anchor: '100% -45',
            listeners: {
                afterrender: {
                    fn: function (control) {
                        me.el.on('contextmenu', Ext.emptyFn, null, { preventDefault: true });
                    },
                    scope: me
                },
                keyup: {
                    fn: function (control) {
                        me.setAddButtonState(control.getValue());
                    },
                    scope: me
                }
            }
        }];
    },
    /**
	*
	*/
    saveAdd: function () {
        var me = this;
        DCT.Util.getSafeElement('_commentStatus').value = 'A';
        DCT.Util.getSafeElement('_commentCode').value = 'OTHR';
        var entry = Ext.getCmp('_commentEntry');
        var onStatement = Ext.getCmp('_commentOnStatement').getValue();

        if (!Ext.isEmpty(me.dctTargetPage)) {
            Ext.getDom('_targetPage').value = me.dctTargetPage;
            Ext.getDom('_submitAction').value = "addComment";
            DCT.Util.getSafeElement('_commentTypeCode').value = 'CI';
            DCT.Util.getSafeElement('_commentDescription').value = entry.getValue();
            if (onStatement)
                DCT.Util.getSafeElement('_commentTypeCode').value = 'CP';
            if (!Ext.isDefined(me.dctItemId)) {
                me.dctDataStore.grid.assignGridSelection(false, DCT.Grid.getItemIdAndItemTypeForGridProcessing);
                me.dctDataStore.loadPage(1, {
							    callback: function(records, operation, success) {
							    	Ext.getDom('_submitAction').value = "";
								  	}
								  });
            } else {
                DCT.Util.getSafeElement('_itemId').value = me.dctItemId;
                DCT.Util.getSafeElement('_itemTypeCode').value = me.dctItemTypeCode;
                me.dctDataStore.loadPage(1, {
							    callback: function(records, operation, success) {
							    	Ext.getDom('_submitAction').value = "";
							  	}
							  });
            }
            me.ownerCt.closeWindow();
        } else {
            DCT.Util.getSafeElement(me.dctHiddenCommentField).value = entry.getValue().replace(/,/g, "");
            if (onStatement)
                DCT.Util.getSafeElement(me.dctHiddenCmtTypeCode).value = 'CP';
            else
                DCT.Util.getSafeElement(me.dctHiddenCmtTypeCode).value = 'CI';
            DCT.Util.hideShowGroup(me.dctViewAnchorField, true);
            me.ownerCt.closeWindow();
        }
    }
});


/**
*
*/
Ext.define('DCT.BillingDisplayComment', {
    extend: 'DCT.DisplayComment',

    inputXType: 'dctbillingdisplaycomment',
    xtype: 'dctbillingdisplaycomment',
    formId: 'commentsForm',

    /**
	*
	*/
    setPanelItems: function (config) {
        var commentDate = '';
        var commentDescription = '';
        if (config.dctExtRecordId) {
            var extRecord = config.dctDataStore.getById(config.dctExtRecordId);
            if (extRecord) {
                var creationDate = extRecord.data['ItemCmtDate'];
                var creator = extRecord.data['ItemCmtOriginator'];
                var description = extRecord.data['ItemCmtDescription'];
                var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
                var creationDt = Ext.Date.format(creationDate, displayDateFormat + ", g:i a");
                var cmtSize = Number(extRecord.data['ItemCmtSize']);
                commentDate = creationDt + " : " + creator;
                commentDescription = description;
                if (cmtSize > 200)
                    commentDescription = commentDescription + '...';
            }
        } else {
            if ((config.dctCmtCreationDate) && (config.dctCmtOriginator)) {
                commentDate = config.dctCmtCreationDate + " : " + config.dctCmtOriginator;
            }
            commentDescription = config.dctCommentText;
        }
        config.items = [{
            style: 'margin-top:5px;',
            hideLabel: true,
            id: 'commentDate',
            anchor: '100%',
            value: commentDate
        }, {
            hideLabel: true,
            id: 'commentDescription',
            anchor: '100%',
            value: commentDescription
        }];
    },
    /**
	*
	*/
    setPanelButtons: function (config) {
        var me = this;
        var buttonsObject = [];
        if (config.dctExtRecordId) {
            var extRecord = config.dctDataStore.getById(config.dctExtRecordId);
            if (extRecord) {
                if (extRecord.store.proxy.reader.rawData.parentNode.baseName != "ClosedMatchList") {
                    var cmtCount = Number(extRecord.data['ItemCmtCount']);
                    var cmtSize = Number(extRecord.data['ItemCmtSize']);
                    var itemId = extRecord.data['ItemObjectId'];
                    var itemTypeCode = extRecord.data['ItemObjectType'];
                    buttonsObject.push({
                        text: DCT.T("More"),
                        id: '_cmtMoreButton',
                        hidden: (cmtCount < 2 && cmtSize < 200),
                        handler: function () { DCT.Submit.gotoScheduleItemDetail(itemId, itemTypeCode); me.ownerCt.closeWindow(); },
                        scope: me
                    });
                }
            }
        }
        buttonsObject.push({
            text: DCT.T('Cancel'),
            id: '_cmtCancelButton',
            handler: function () { me.ownerCt.closeWindow(); },
            scope: me
        });
        config.buttons = buttonsObject;
    }
});


