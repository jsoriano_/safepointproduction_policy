/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
		/**
		*
		*/
		policyTermColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var policyTermColumnHTML = '<a href="javascript:;" id="policyIdLink" onclick="DCT.Submit.openPolicySummary(\'policySummary\',\'' + extRecord.data['policyId'] + '\');">' + dataValue + '</a>';
				return policyTermColumnHTML;
		},
		/**
		*
		*/
		policyLinkToPASRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = (dataValue == '') ? '' : '<a href="javascript:;" onclick="DCT.Submit.gotoPASPolicyPage(' + extRecord.data['quoteId'] + ');"><img src="' + DCT.imageDir + 'icons\/application_go.png" alt="' + DCT.T('ViewinPolicyAdministrationSystem') + '" title="' + DCT.T('ViewinPolicyAdministrationSystem') + '"/></a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		removeAnchorsFromGroupHeader: function (dataValue, unused, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var headerHTML = dataValue;
				return headerHTML;
		},
		/**
		*
		*/
		activityAmountColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var activityAmountColumnHTML = '';
				if (extRecord.data['type'] == 'Write Off')
						activityAmountColumnHTML = '<div><a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'writeOffDetail\',' + extRecord.data['writeOffId'] + ')">' + dataValue + '</a></div>';
				else
						activityAmountColumnHTML = '<div>' + dataValue + '</div>';

				return activityAmountColumnHTML;
		},
		/**
		*
		*/
		activityDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowActionsHTML, linkHTML, replaceString, descriptionText, descriptionLink,
						dataRow = rowIndex + 1,
						xpathValue = 'CandidateList/ListItem:nth(' + dataRow + ')',
						rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
				
				descriptionText = Ext.DomQuery.selectValue('ActivityDescription/text/@value', rawRecord);
				descriptionText = Ext.util.Format.htmlEncode(descriptionText);
				descriptionLink = Ext.DomQuery.select('ActivityDescription/link', rawRecord);
				rowActionsHTML = ''
				if (Ext.isDefined(descriptionText))
						rowActionsHTML = rowActionsHTML + descriptionText;

				if (descriptionLink.length > 0) {
						for (i = 0; i < descriptionLink.length; i++) {
								id = Ext.DomQuery.selectValue('@id', descriptionLink[i]);
								linkText = Ext.DomQuery.selectValue('@linkText', descriptionLink[i]);
								linkIds = Ext.DomQuery.select('linkId', descriptionLink[i]);
								linkType = Ext.DomQuery.selectValue('@linkType', descriptionLink[i]);
								replaceString = '^' + id;

								linkParms = '';
								for (j = 0; j < linkIds.length; j++) {
										linkParms = linkParms + "\'" + Ext.DomQuery.selectValue('@value', linkIds[j]) + "\'";
										if (j + 1 < linkIds.length)
												linkParms = linkParms + ',';
								}

								var canAccessAccount = DCT.Util.isAuthorized('canAccessAccountSummary', extRecord);
								var canAccessPolicy = DCT.Util.isAuthorized('canAccessPolicySummary', extRecord);
								var canAccessPayment = DCT.Util.isAuthorized('canAccessPaymentDetail', extRecord);
								var canAccessDisbursement = DCT.Util.isAuthorized('canAccessDisbursementDetail', extRecord);
								var canAccessWriteoff = DCT.Util.isAuthorized('canWriteOffPayments', extRecord);
								var canAccessAllAreas = DCT.Util.isAuthorized('canAccessAllAreas', extRecord);

								switch (linkType) {
										case 'account':
												if (canAccessAccount || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('account',";
												else
														functionToCall = '';
												break;
										case 'policy':
												if (canAccessPolicy || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('policy',";
												else
														functionToCall = '';
												break;
										case 'payment':
												if (canAccessPayment || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('payment',";
												else
														functionToCall = '';
												break;
										case 'invoice':
												//functionToCall = "DCT.Submit.submitBillingActivity('invoice',";  add back when screen built
												functionToCall = '';
												break;
										case 'disbursement':
												if (canAccessDisbursement || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('disbursement',";
												else
														functionToCall = '';
												break;
										case 'writeoff':
												if (canAccessWriteoff || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('writeoff',";
												else
														functionToCall = '';
												break;
										case 'writeoffinstallment':
												if (canAccessWriteoff || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('writeoffinstallment',";
												else
														functionToCall = '';
												break;
                    	case 'paymentwriteoff':
							if (canAccessWriteoff || canAccessAllAreas)
								functionToCall = "DCT.Submit.submitBillingActivity('paymentwriteoff',";
							else
								functionToCall = '';
							break;
										case 'eftRequest':
												if (true || canAccessAllAreas)
														functionToCall = "DCT.Submit.submitBillingActivity('eftRequest',";
												else
														functionToCall = '';
												break;

										default:
												functionToCall = '';
												break;
								}

								if (functionToCall.length == 0)
										linkHTML = linkText;
								else
										linkHTML = '<a href="javascript:;" onclick="' + functionToCall + linkParms + ');">' + linkText + '</a>';

								rowActionsHTML = rowActionsHTML.replace(replaceString, linkHTML);
						}
				}
				var activityType = Ext.DomQuery.selectValue('ActivityTypeCode', rawRecord);
				if (activityType == "ANOT")
						rowActionsHTML = '<div class="notesDescription">' + rowActionsHTML + '</div>';
				return {
					rowBody: '<div id="activityDescription' + rowIndex + '">' + rowActionsHTML + '</div>'
				};
		},
		/**
		*
		*/
		viewErrorDetailsColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
        var errorDetailsColumnHTML = '<a href="javascript:;" id="errorDetailsId" onclick="DCT.Submit.displayErrorDetail(\'' + extRecord.id + '\');">' + DCT.T('UI_ViewDetails') + '</a>';
				return errorDetailsColumnHTML;
		},
    viewResubmitDetailsColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
    	var exceptionTypeCode = extRecord.data['ExceptionStatus'];
    	if (exceptionTypeCode == "Closed")
    		return null;

    	var resubmitDetailsColumnHTML = '<a href="javascript:;" id="resubmitDetailsId" onclick="DCT.Submit.displayResubmitDetail(\'' + extRecord.id + '\');">' + DCT.T('UI_Resubmit') + '</a>';
        return resubmitDetailsColumnHTML;
    },

		/**
		*
		*/
		viewUnidentifiedPaymentsDetailColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				return '<a href="javascript:;" id="UnidentifiedPaymentsDetailId" onclick="DCT.Submit.displayUnidentifiedPaymentsDetail(\'' + extRecord.id + '\');">Details</a>';
		},
		/**
		*
		*/
		paymentAmountRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var allocationTypeCode = extRecord.data['allocationTypeCode'];
				var destinationId = extRecord.data['destinationId'];
				var transactionDate = extRecord.data['transactionDate'];
				var rowActionsHTML;
				var authorizedInstallment = extRecord.data['authorizedInstallment'];
				var authorizedDisburse = extRecord.data['authorizedDisburse'];
				var installmentDate = extRecord.data['installmentDate'];

				// Just in case the installment date is NULL or empty, use the transaction date in place of it.
				var defaultDate = (Ext.isDefined(installmentDate) && installmentDate != "" ? installmentDate : transactionDate);

				switch (allocationTypeCode) {
						case 'IS':
						case 'CH':
								if (authorizedInstallment == 'true')
										rowActionsHTML = '<div><a href="javascript:;" onclick="DCT.Submit.submitBillingInstallment(\'installmentSchedule\',\'' + Ext.Date.format(defaultDate, 'Y-m-d\\TH:i:s') + '\', ' + destinationId + ', \'' + allocationTypeCode + '\')">' + dataValue + '</a></div>';
								else
										rowActionsHTML = '<div>' + dataValue + '</div>';
								break;
						case 'PD':
						case 'DB':
								if (authorizedDisburse == 'true')
										rowActionsHTML = '<div><a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'viewDisbursementDetails\',' + destinationId + ')">' + dataValue + '</a></div>';
								else
										rowActionsHTML = '<div>' + dataValue + '</div>';
								break;
						default:
								rowActionsHTML = '<div>' + dataValue + '</div>';
								break;
				}
				return rowActionsHTML;
		},
		/**
		*
		*/
		viewDetailsRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				var authorized = extRecord.data['authorized'];

				if (authorized == 'true')
						rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'viewDisbursementDetails\',' + extRecord.id + ')">View Details</a>';

				return rowActionsHTML;
		},
		/**
		*
		*/
		documentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var downloadActionsHTML;
				var rowActionsHTML = "";
				var canViewInvoice = DCT.Util.isAuthorized('canViewInvoice', extRecord);
				var canAccessAllAreas = DCT.Util.isAuthorized('canAccessAllAreas', extRecord);

				if (canViewInvoice || canAccessAllAreas) {
						if (dataValue == '') {
								rowActionsHTML = "";
						} else {
							var dataRow = rowIndex + 1,
									xpathValue = 'CandidateList/ListItem:nth(' + dataRow + ')',
									rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData),
									moniker = Ext.DomQuery.selectValue('Moniker', rawRecord),
									filename = Ext.DomQuery.selectValue('FileName', rawRecord);
							if (!Ext.isEmpty(moniker))
									moniker = moniker.replace("\\", "\\\\");
							downloadActionsHTML = '<a href="javascript:;" onclick="DCT.Util.displayPDFAttachment(\'viewPDF\', \'' + extRecord.data['Attachment'] + '\',\'' + moniker + '\',\'' + filename + '\');"><img src="' + DCT.imageDir + 'icons\/attach_small.png" alt="Download attachment"/></a>';
							rowActionsHTML = downloadActionsHTML;
						}
				}
				return rowActionsHTML;
		},
		checkIfPolicyHold: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var downloadActionsHTML;
				var rowActionsHTML = '';

				if (extRecord.data['holdTypeCode'] != 'N' && extRecord.data['holdTypeCode'] != '') {
						downloadActionsHTML = '<div align="left"><img src="' + DCT.imageDir + 'icons\/stop.png" alt="' + extRecord.data['PolicyTermHoldReason'] + '" title="' + extRecord.data['PolicyTermHoldReason'] + '"/></div>';
						rowActionsHTML = downloadActionsHTML;
				}
				return rowActionsHTML;
		},
		/**
		*
		*/
		reprintInvoiceColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var	reprintActionsHTML
						rowActionsHTML = "",
						dataRow = rowIndex + 1,
						xpathValue = 'CandidateList/ListItem:nth(' + dataRow + ')',
						rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData),
						subjectCode = Ext.DomQuery.selectValue('SubjectCode', rawRecord),
						canReprintInvoice = DCT.Util.isAuthorized('canReprintInvoice', extRecord),
						canAccessAllAreas = DCT.Util.isAuthorized('canAccessAllAreas', extRecord);

				if (canReprintInvoice || canAccessAllAreas) {
						if (subjectCode == 'INV') {
								reprintActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingInvoiceReprint(\'accountSummary\', \'reprintInvoice\', \'' + dataValue + '\');"><img src="' + DCT.imageDir + 'icons\/printer_small.png" alt="Reprint Invoice" title="Reprint Invoice"/></a>';
								rowActionsHTML = reprintActionsHTML;
						}
				}

				return rowActionsHTML;
		},
		/**
		*
		*/
		formatDateColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "", displayDateFormat;
				if (Ext.isDefined(dataValue)) {
						displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
						columnHTML = Ext.Date.format(dataValue, displayDateFormat);
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatAmountColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						columnHTML = numberFormat.formatFloat(dataValue);
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatInstallmentDateColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "", displayDateFormat, dt, sd = "";
				if (Ext.isDefined(dataValue)) {
						displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
						columnHTML = Ext.Date.format(dataValue, displayDateFormat);
				} else {
						if (Ext.isDefined(extRecord.get('SourceInstallmentDueDate'))) {
								displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
								dt = Ext.Date.parse(extRecord.get('SourceInstallmentDueDate'), "Y-m-d");
								sd = " - " + Ext.Date.format(dt, displayDateFormat);
							}
						if (Ext.isDefined(extRecord.get('InvoicedIndicator'))) {
								if (extRecord.get('InvoicedIndicator') == 'Y') {
										columnHTML = '<img src="' + DCT.imageDir + 'icons\/page.png" alt="Invoiced" title="Invoiced" unselectable="on"/>';
								}
						}
						if (Ext.isDefined(extRecord.get('StatementIndicator'))) {
								if (extRecord.get('StatementIndicator') == 'Y') {
										columnHTML = '<img src="' + DCT.imageDir + 'icons\/page.png" alt="Statement Generated" title="Statement Generated" unselectable="on"/>';
								}
						}
						if (Ext.isDefined(extRecord.get('InstallmentTypeCode'))) {
								switch (extRecord.get('InstallmentTypeCode')) {
										case 'M':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/icoDateAlert.png" alt="Missed Installment" title="Missed Installment" unselectable="on"/>';
												break;
										case 'C':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/icoDateWarn.png" alt="Catchup Installment' + sd + '" title="Catchup Installment' + sd + '" unselectable="on"/>';
												break;
										case 'E':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/money_dollar.png" alt="Earned Premium" title="Earned Premium" unselectable="on"/>';
												break;
										case 'R':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/arrow_divide.png" alt="Redistributed Installment " title="Redistributed Installment " unselectable="on"/>';
												break;
										case 'S':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/icoDateClock.png" alt="Spread Installment' + sd + '" title="Spread Installment' + sd + '" unselectable="on"/>';
												break;
										case 'MR':
												columnHTML = columnHTML + '<img src="' + DCT.imageDir + 'icons\/icoDateAlertYellow.png" alt="Missed Installment due to Reschedule" title="Missed Installment due to Reschedule" unselectable="on"/>';
												break;
										default:
												break;
								}
						}
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatItemAmtColumnWithoutWriteOffActionRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						columnHTML = numberFormat.formatFloat(dataValue);
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatItemAmtColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						columnHTML = numberFormat.formatFloat(dataValue);
				} else {
						if (extRecord.get('Type') == 'Current') {
							var installmentDate = extRecord.get('InstallmentDate');
							if ((Ext.isDefined(extRecord.get('Balance'))) && (Ext.isDefined(installmentDate))) {
									if (extRecord.get('Balance') > 0)
											columnHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'writeOffWaiveInstallment\',\'' + Ext.Date.format(installmentDate, 'Y-m-d') + '\');"><img src="' + DCT.imageDir + 'icons\/icoWriteOff.png" alt="Write-Off/Waive" title="Write-Off/Waive"/></a>';
							}
						}
				}
				return columnHTML;
		},
		/**
		*
		*/

		formatAttachmentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				var installmentDate = extRecord.get('InstallmentDate');
				if (!Ext.isDefined(dataValue)) {
						columnHTML = "";
				}
				else {
						var scheduleType = extRecord.get('Type');
						if (scheduleType == 'Current') {
								var moniker = Ext.getDom('_hiddenMoniker').value;
								if (!Ext.isEmpty(moniker))
										moniker = moniker.replace("\\", "\\\\");
								if (Ext.isDefined(installmentDate)) {
										columnHTML = '<a href="javascript:;" onclick="DCT.Util.displayPDFAttachment(\'viewPDF\', \'' + extRecord.get('AttachmentId') + '\',\'' + moniker + '\',\'' + extRecord.get('FileName') + '\');"><img src="' + DCT.imageDir + 'icons\/attach_small.png" alt="Download attachment"/></a>';
								}
						}
				}
				return columnHTML;
		},
		/**
		*
		*/

		formatAgencyItemAmtColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				var installmentDate = extRecord.get('InstallmentDate');
				if (Ext.isDefined(dataValue)) {
						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						columnHTML = numberFormat.formatFloat(dataValue);
				} else {
						var scheduleType = extRecord.get('Type');
						if (scheduleType == 'Current') {
								if ((Ext.isDefined(extRecord.get('Balance'))) && (Ext.isDefined(installmentDate))) {
										columnHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'writeOffWaiveInstallmentAgency\',\'' + Ext.Date.format(installmentDate, 'Y-m-d') + '\');"><img src="' + DCT.imageDir + 'icons\/icoWriteOff.png" alt="Write-Off/Waive" title="Write-Off/Waive"/></a>';
								}
						}
				}
				return columnHTML;
		},
		/**
		 *
		 */
		formatWriteOffAmtColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var detailId = extRecord.get('DetailId');
						var scheduleType = extRecord.get('Type');
						var detailTypeCode = extRecord.get('DetailTypeCode');
						var forceWriteoff = extRecord.get('WriteOffExists');
						var isHyperlink = ((((Ext.isDefined(forceWriteoff))
																&& forceWriteoff == 1)
															|| (Ext.isDefined(detailId))
															&& (dataValue != 0))
														 && (scheduleType == 'Current'));

						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						if (isHyperlink == true)
								columnHTML = '<a class="dataAnchor" href="javascript:;" onclick="DCT.Submit.viewWriteOffWaiveDetail(\'writeOffWaiveDetail\',\'' + detailId + '\',\'' + detailTypeCode + '\');">' + numberFormat.formatFloat(dataValue) + '</a>';
						else
								columnHTML = numberFormat.formatFloat(dataValue);
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatWriteOffAmtColumnSecuredRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
						columnHTML = numberFormat.formatFloat(dataValue);
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatManualInstallRedistributeAmtColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						if (dataValue == "0.0000") {
								columnHTML = DCT.Grid.formatAmountColumnRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
						} else {
								var subGroupDescription = extRecord.get('ScheduleSubGroupDescription');
								var eftStatusCode = extRecord.get('EFTStatusCode');
								var detailId = extRecord.get('DetailId');
								var scheduleType = extRecord.get('Type');
								var detailTypeCode = extRecord.get('DetailTypeCode');
								var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });

								if ((Ext.isDefined(detailId)) && (detailTypeCode != "CH") && (eftStatusCode != "T") && (scheduleType == "Current"))
										columnHTML = '<a class="dataAnchor" href="javascript:;" onclick="DCT.Submit.viewManualInstallRedistribute(\'manualInstallmentRedistribution\',\'' + detailId + '\',\'' + detailTypeCode + '\');">' + numberFormat.formatFloat(dataValue) + '</a>';
								else if ((!Ext.isDefined(subGroupDescription)) && (!Ext.isDefined(detailId)) && (eftStatusCode != "T") && (scheduleType == "Current")) {
										var installmentDate = extRecord.get('InstallmentDate');
										columnHTML = '<a class="dataAnchor" href="javascript:;" onclick="DCT.Submit.viewInstallmentLevelRedistribute(\'manualInstallmentRedistribution\',\'' + Ext.Date.format(installmentDate, 'Y-m-d') + '\');">' + numberFormat.formatFloat(dataValue) + '</a>';
								} else {
										columnHTML = numberFormat.formatFloat(dataValue);
								}
						}
				}
				return columnHTML;
		},
		/**
		*
		*/
		formatIndividualDueDateChangeDateColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				if (Ext.isDefined(dataValue)) {
						var installmentDate = extRecord.get('InstallmentDate');
						var originalDueDate = extRecord.get('OriginalDueDate');
						var eftStatusCode = extRecord.get('EFTStatusCode');
						var scheduleType = extRecord.get('Type');
						var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;

						if (Ext.isDefined(installmentDate) && (eftStatusCode != "T") && (scheduleType == "Current")) {
								columnHTML = '<a class="dataAnchor" href="javascript:;" onclick="DCT.Submit.viewIndividualDueDateChange(\'individualDueDateChange\',\'' + Ext.Date.format(installmentDate, 'Y-m-d') + '\',\'' + Ext.Date.format(originalDueDate, 'Y-m-d') + '\');">' + Ext.Date.format(dataValue, displayDateFormat) + '</a>';
					}
						else
								columnHTML = Ext.Date.format(dataValue, displayDateFormat);
				}
				return columnHTML;
		},
		/**
		*
		*/
		viewEditDetailsColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'batchPaymentList\',' + extRecord.id + ')">' + dataValue + '</a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		unidentifiedColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				rowActionsHTML = '';
				if (dataValue == '')
						rowActionsHTML = '<div class="required">*</div>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		editPaymentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				var statusCode = extRecord.data['StatusCode'];
				if (statusCode != 'P')
						rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.editBatchPayment(\'editBatchPayment\',' + dataValue + ')"><img alt="' + DCT.T('EditBatchPayment') + '" title="' + DCT.T('EditBatchPayment') + '" src="' + DCT.imageDir + 'icons\/pencil.png"/></a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		deletePaymentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				var statusCode = extRecord.data['StatusCode'];
				if (statusCode != 'P') {
						var	dataRow = rowIndex + 1,
								xpathValue = 'BatchPaymentList/BatchPaymentItem:nth(' + dataRow + ')',
								rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData),					
								paymentTS = Ext.DomQuery.selectValue('PaymentBatchEntryLockingTS', rawRecord);
						rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.deleteBatchPayment(\'batchPaymentList\',\'processDeleteBatchPayment\',\'' + dataValue + '\',\'' + paymentTS + '\')"><img alt="' + DCT.T('DeleteBatchPayment') + '" title="' + DCT.T('DeleteBatchPayment') + '" src="' + DCT.imageDir + 'icons\/decline.png"/></a>';
				}
				return rowActionsHTML;
		},
		/**
		*
		*/
		formatTotalsColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				var currencyMask = '-$??????????#.00!' + extRecord.data['currencySymbols'];
				var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: currencyMask });
				columnHTML = '<div>' + numberFormat.formatFloat(dataValue) + '</div>';
				return columnHTML;
		},
		/**
		*
		*/
		printDetailsColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				rowActionsHTML = '<a href="javascript:;" id="printLink" onclick="DCT.Submit.submitBillingPageAction(\'depositedBatch\',\'printBatch\',' + dataValue + ', false, \'printLink\')">Print</a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		viewPaymentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				var paymentId = extRecord.data['PaymentId'];
				var paymentType = extRecord.data['PaymentType'];
				var destinationId = extRecord.data['DestinationId'];
				var statusCode = extRecord.data['StatusCode'];
				var currencyMask = '-$??????????#.00!' + extRecord.data['currencySymbols'];
				if (paymentType == 'UNID' && statusCode == 'ACV')	//if payment has been reversed we will only look at detail
						paymentId = destinationId;
				var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: currencyMask });
				rowActionsHTML = '<div><a href="javascript:;" onclick="DCT.Submit.viewPaymentDetails(\'' + paymentId + '\', \'' + paymentType + '\', \'' + statusCode + '\')">' + numberFormat.formatFloat(dataValue) + '</a></div>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		paymentDetailsAccountRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				var accountId = extRecord.data['accountId'];
				rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.submitBillingActivity(\'account\',\'' + accountId + '\')">' + dataValue + '</a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		endBillingColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.processHoldEvent(\'' + extRecord.id + '\', \'manageHoldEvents\', \'endHoldEvent\', \'' + extRecord.data['EventTS'] + '\')">End</a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		reviewHoldEventColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.processHoldEvent(\'' + extRecord.id + '\', \'manageHoldEvents\', \'reviewHoldEvent\', \'\')">Review</a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		errorDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowActionsHTML, descriptionText, descriptionLink;

				descriptionText = extRecord.data['Description'];
				rowActionsHTML = ''
				if (Ext.isDefined(descriptionText))
						rowActionsHTML = rowActionsHTML + descriptionText;
				rowActionsHTML = '<div class="errorDescription">' + rowActionsHTML + '</div>';
				return {
					rowBody: rowActionsHTML
				};
		},
		/**
		*
		*/
		collectorsPayorRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var collectorsPayorColumnHTML = "";
				var partyName = "";
				var partyFirstName = "";
				var partyMiddleName = "";
				var fullName = "";
				var partyRecords = Ext.DomQuery.select("Parties/PartyRecord", extRecord.store.proxy.reader.rawData);

				if (partyRecords.length > 0) {
						for (i = 0; i < partyRecords.length; i++) {
								var partyId = Ext.DomQuery.selectValue('PartyRecordPartyId', partyRecords[i]);
								if (partyId == dataValue) {
										partyName = Ext.DomQuery.selectValue("Party/PartyName", partyRecords[i]);
										partyFirstName = Ext.DomQuery.selectValue("Party/PartyFirstName", partyRecords[i]);
										partyMiddleName = Ext.DomQuery.selectValue("Party/PartyMiddleName", partyRecords[i]);
										break;
								}
						}
				}

				if (!Ext.isDefined(partyFirstName) || partyFirstName == "")
						fullName = partyName;
				else {
						if (!Ext.isDefined(partyMiddleName) || partyFirstName == "")
								fullName = partyFirstName + " " + partyName;
						else
								fullName = partyFirstName + " " + partyMiddleName + " " + partyName;
				}
				policyTermColumnHTML = fullName;

				return policyTermColumnHTML;

		},
		/**
		*
		*/
		policyTermRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var policyTermColumnHTML = "", displayDateFormat;
				var policyEffDate = extRecord.data['PolicyTermEffectiveDate'];
				var policyExpDate = extRecord.data['PolicyTermExpirationDate'];
				var policyEffDateFormatted;
				var policyExpDateFormatted;
				var EffDate;
				var ExpDate;

				displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
				EffDate = Ext.Date.parse(policyEffDate, "Y-m-d");
				ExpDate = Ext.Date.parse(policyExpDate, "Y-m-d");
				policyEffDateFormatted = Ext.Date.format(EffDate, displayDateFormat);
				policyExpDateFormatted = Ext.Date.format(ExpDate, displayDateFormat);

				policyTermColumnHTML = '<a href="javascript:;" id="policyIdLink" onclick="DCT.Submit.openPolicySummary(\'policySummary\',\'' + extRecord.id + '\');">' + policyEffDateFormatted + ' - ' + policyExpDateFormatted + '</a>';

				return policyTermColumnHTML;
		},
		/**
		*
		*/
		batchDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowActionsHTML, descriptionText, descriptionLink;

				descriptionText = extRecord.data['Description'];
				rowActionsHTML = ''
				if (Ext.isDefined(descriptionText))
						rowActionsHTML = rowActionsHTML + descriptionText;
				rowActionsHTML = '<div class="batchDescription">' + rowActionsHTML + '</div>';
				return {
					rowBody: rowActionsHTML
				};
		},
		/**
		*
		*/
		billItemTermDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
    	var rowActionsHTML, linkHTML, replaceString, descriptionText, descriptionLink, miscTypeCodeDesc;

				descriptionText = extRecord.data['policyTerm'];
        	miscTypeCodeDesc = extRecord.data['miscTypeCodeDesc'];
				rowActionsHTML = 'Billing Item Effective Dates - '
				if (Ext.isDefined(descriptionText))
						rowActionsHTML = rowActionsHTML + descriptionText;
        	if (typeof (miscTypeCodeDesc) != "undefined" && miscTypeCodeDesc != null && miscTypeCodeDesc != '')
        		rowActionsHTML = rowActionsHTML + ' - ' + miscTypeCodeDesc;
				rowActionsHTML = '<div class="billItemTermDescription">' + rowActionsHTML + '</div>';
				return {
					rowBody: rowActionsHTML
				};
		},
		/**
		*
		*/
		scheduledItemsDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowTitleHTML = '<tr class="remittedTableTitle"><td colspan="5">Remitted Detail</td></tr><tr><td>Statement Date</td><td>Gross</td><td>Commission</td><td>Net</td><td>Remitted</td></tr>';
				var rowDataHTML = '';
				var rowHTML = '';

				agencyStatement = Ext.DomQuery.select('AgencyStatements/AgencyStatement', extRecord.store.proxy.reader.rawData);

				if (agencyStatement.length > 0) {
						var currencyMask = '-$??????????#.00!' + extRecord.data['currencySymbols'];
						var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
						for (i = 0; i < agencyStatement.length; i++) {
								statementDate = Ext.DomQuery.selectValue('StatementDate', agencyStatement[i]);
								dt = Ext.Date.parse(statementDate, "Y-m-d\\TH:i:s");
								grossAmount = Ext.DomQuery.selectValue('GrossAmount', agencyStatement[i]);
								commissionAmount = Ext.DomQuery.selectValue('CommissionAmount', agencyStatement[i]);
								netAmount = Ext.DomQuery.selectValue('NetAmount', agencyStatement[i]);
								remittedAmount = Ext.DomQuery.selectValue('RemittedAmount', agencyStatement[i]);
								var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: currencyMask });
								rowDataHTML = rowDataHTML + '<tr><td>' + Ext.Date.format(dt, displayDateFormat) + '</td><td>' + numberFormat.formatFloat(grossAmount) + '</td><td>' + numberFormat.formatFloat(commissionAmount) + '</td><td>' + numberFormat.formatFloat(netAmount) + '</td><td>' + numberFormat.formatFloat(remittedAmount) + '</td></tr>';
						}
						rowHTML = '<div id="remittedDetail"><table>' + rowTitleHTML + rowDataHTML + '</table></div>'
				}
				return {
					rowBody: rowHTML,
					rowBodyCls: 'scheduledItemsList'
				};
		},
		/**
		*
		*/
		closedMatchesDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowTitleHTML = '<tr class="matchedTableTitle"><td colspan="6">Matched Items</td></tr><tr><td>Statement Date</td><td>Type</td><td>Gross</td><td>Commission</td><td>Net</td><td>Matched Amount</td></tr>';
				var rowDataHTML = '';
				var rowHTML = '';

				agencyStatement = Ext.DomQuery.select('MatchedItem', extRecord.store.proxy.reader.rawData);

				if (agencyStatement.length > 0) {
						var currencyMask = '-$??????????#.00!' + extRecord.data['currencySymbols'];
						var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
						for (i = 0; i < agencyStatement.length; i++) {
								typeCode = Ext.DomQuery.selectValue('TypeCode', agencyStatement[i]);
								statementDate = Ext.DomQuery.selectValue('StatementDate', agencyStatement[i]);
								dt = Ext.Date.parse(statementDate, "Y-m-d");
								grossAmount = Ext.DomQuery.selectValue('GrossAmount', agencyStatement[i]);
								commissionAmount = Ext.DomQuery.selectValue('CommissionAmount', agencyStatement[i]);
								netAmount = Ext.DomQuery.selectValue('NetAmount', agencyStatement[i]);
								matchedAmount = Ext.DomQuery.selectValue('MatchedAmount', agencyStatement[i]);
								var extCmtRecord = Ext.DomQuery.selectNode('ItemComment', agencyStatement[i]);
								var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: currencyMask });
								if (extCmtRecord) {
										var creator = Ext.DomQuery.selectValue('@Originator', extCmtRecord);
										var description = Ext.DomQuery.selectValue('@Description', extCmtRecord);
										var creationDate = Ext.DomQuery.selectValue('@CreationDate', extCmtRecord);
										var cmtDate = Ext.Date.parse(creationDate, "Y-m-dTH:i:s");
										var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
										var creationDt = Ext.Date.format(cmtDate, displayDateFormat + ", g:i a");
										rowDataHTML = rowDataHTML + '<tr><td>' + Ext.Date.format(dt, displayDateFormat) + '</td><td>' + typeCode + '</td><td>' + numberFormat.formatFloat(grossAmount) + '</td><td>' + numberFormat.formatFloat(commissionAmount) + '</td><td>' + numberFormat.formatFloat(netAmount) + '</td><td>' + numberFormat.formatFloat(matchedAmount) + '</td><td><a href="javascript:;" onclick="displayComment(' + 'null' + '' + ',\'' + description + '\',\'' + creationDt + '\',\'' + creator + '\')"><img alt="Display Item Comment" title="Display Item Comment" src="' + DCT.imageDir + 'icons\/magnifier.png"/></a></td></tr>';

								}
								else {
										rowDataHTML = rowDataHTML + '<tr><td>' + Ext.Date.format(dt, displayDateFormat) + '</td><td>' + typeCode + '</td><td>' + numberFormat.formatFloat(grossAmount) + '</td><td>' + numberFormat.formatFloat(commissionAmount) + '</td><td>' + numberFormat.formatFloat(netAmount) + '</td><td>' + numberFormat.formatFloat(matchedAmount) + '</td><td></tr>';
								}
						}
						rowHTML = '<div id="matchedItemDetail"><table>' + rowTitleHTML + rowDataHTML + '</table></div>'
				}
				return {
					rowBody: rowHTML,
					rowBodyCls: 'closedMatchesList'
				};
		},
		/**
		*
		*/
		formatMatchedTotalColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "";
				var currencyMask = '-$??????????#.00!' + extRecord.data['currencySymbols'];
				var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: currencyMask });
				var itemAmount = numberFormat.formatFloat(dataValue);
				if (itemAmount.charAt(0) == '-')
						itemAmount = '(' + itemAmount.substr(1) + ')';
				columnHTML = '<div>' + itemAmount + '</div>';
				return columnHTML;
		},
		/**
		*
		*/
		formatPolicyReferenceRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var policyTermColumnHTML = "";
				var ObjectTypeCode = extRecord.data['ItemObjectType'];
				var PolicyTermId = extRecord.data['PolicyTermId'] == '0' ? '' : extRecord.data['PolicyTermId'];

				if ((ObjectTypeCode != 'CA') && (ObjectTypeCode != 'AA') && (PolicyTermId != ''))
						policyTermColumnHTML = '<a href="javascript:;" id="policyIdLink" onclick="DCT.Submit.openPolicySummary(\'policySummary\',\'' + PolicyTermId + '\');">' + dataValue + '</a>';
				else
						policyTermColumnHTML = dataValue;

				return policyTermColumnHTML;
		},
		/**
		*
		*/
		formatEffectiveDateRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "", displayDateFormat;
				var ObjectTypeCode = extRecord.data['ItemObjectType'];
				displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;

				columnHTML = '<a href="javascript:;" id="scheduleItemLink" onclick="DCT.Submit.gotoScheduleItemDetail(\'' + extRecord.data['ItemObjectId'] + '\',\'' + extRecord.data['ItemObjectType'] + '\');">' + Ext.Date.format(dataValue, displayDateFormat) + '</a>';

				return columnHTML;
		},
		/**
		*
		*/
		formatMatchedDateRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var columnHTML = "", displayDateFormat;

				displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
				columnHTML = '<a href="javascript:;" id="matchItemDetailLink" onclick="DCT.Submit.gotoMatchedItemDetail(\'' + extRecord.id + '\');">' + Ext.Date.format(dataValue, displayDateFormat) + '</a>';
				return columnHTML;
		},
		/**
		*
		*/
		formatItemCommentColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var rowActionsHTML = "";
				if (dataValue != '')
						rowActionsHTML = '<a href="javascript:;" onclick="DCT.Util.displayBillingComment(\'' + extDataStore.grid.id + '\', \'' + extRecord.id + '\')"><img alt="Display Item Comment" title="Display Item Comment" src="' + DCT.imageDir + 'icons\/magnifier.png"/></a>';
				return rowActionsHTML;
		},
		/**
		*
		*/
		commentsDescriptionRowRenderer: function (recordData, rowIndex, extRecord, orig) {
				var me = this;
				var rowHTML = '';

				rowHTML = '<div id="commentDetail">' + extRecord.data['description'] + '</div>'
				return {
					rowBody: rowHTML
				};
		},
		/**
		*
		*/
		formatCommentTypeActionRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var commentTypeHTML = "";

				if (extRecord.data['commentTypeCode'] == 'CP')
						commentTypeHTML = '<a href="javascript:;" onclick="DCT.Grid.updateCommentTypeCode(\'scheduledItemDetail\',\'updateCommentType\',\'' + extRecord.id + '\', \'CI\', \'' + extRecord.data['commentLockingTS'] + '\');"><img src="' + DCT.imageDir + 'icons\/decline.png" alt="Remove comment from statement" title="Remove item from statement"/></a>';
				else if (extRecord.data['commentTypeCode'] == 'CI')
						commentTypeHTML = '<a href="javascript:;" onclick="DCT.Grid.updateCommentTypeCode(\'scheduledItemDetail\',\'updateCommentType\',\'' + extRecord.id + '\', \'CP\', \'' + extRecord.data['commentLockingTS'] + '\');"><img src="' + DCT.imageDir + 'icons\/add.png" alt="Add comment to statement" title="Add comment to statement"/></a>';

				return commentTypeHTML;
		},
		/**
		*
		*/
		agencyPaymentAmountRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var allocationTypeCode = extRecord.data['allocationTypeCode'];
				var destinationId = extRecord.data['destinationId'];
				var transactionDate = extRecord.data['transactionDate'];
				var rowActionsHTML;
				var authorizedInstallment = extRecord.data['authorizedInstallment'];
				var authorizedDisburse = extRecord.data['authorizedDisburse'];

				switch (allocationTypeCode) {
						case 'PD':
						case 'DB':
								if (authorizedDisburse == 'true')
										rowActionsHTML = '<div><a href="javascript:;" onclick="DCT.Submit.submitBillingProcessAction(\'viewDisbursementDetails\',' + destinationId + ')">' + dataValue + '</a></div>';
								else
										rowActionsHTML = '<div>' + dataValue + '</div>';
								break;
						default:
								rowActionsHTML = '<div>' + dataValue + '</div>';
								break;
				}
				return rowActionsHTML;
		},
		/**
		*
		*/
		setPaymentDetailSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				var authorizedToSuspend = extRecord.data['authorizedToSuspend'];

				if ((extRecord.data['availableForSuspense'] == 'N') || (extRecord.data['statusCode'] == 'REV') || (authorizedToSuspend == 'false'))
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
				else
						return '<div class="x-grid-row-checker">&#160;</div>';
		},
		/**
		*
		*/
		paymentDetailCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							var authorizedToSuspend = extRecord.data['authorizedToSuspend'];
							if ((extRecord.data['availableForSuspense'] == 'N') || (extRecord.data['statusCode'] == 'REV') || (authorizedToSuspend == 'false')){
								return true;
							}else{
								return false;
							}
						}, me);
	
				return foundRecords.getCount();
		},
		/**
		*
		*/
		checkPaymentDetailSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				var authorizedToSuspend = recordData.data['authorizedToSuspend'];
				if ((recordData.data['availableForSuspense'] == 'N') || (recordData.data['statusCode'] == 'REV') || (authorizedToSuspend == 'false'))
						return false;
				else
						return true;
		},
		/**
		*
		*/
		candidateListItemDeselected: function (selectionModel, dataRecord, rowIndex) {
				var continueAnchor = Ext.get('processContinue');
				continueAnchor.replaceCls('btnEnabled', 'btnDisabled');
		},
		/**
		*
		*/
		candidateListItemSelected: function (selectionModel, dataRecord, rowIndex) {
				var continueAnchor = Ext.get('processContinue');
				continueAnchor.replaceCls('btnDisabled', 'btnEnabled');
		},
		/**
		*
		*/
		collectionAccountsListItemDeselected: function (selectionModel, dataRecord, rowIndex) {
				var continueAnchor = Ext.get('transferAction');
				continueAnchor.replaceCls('btnEnabled', 'btnDisabled');
		},
		/**
		*
		*/
		collectionAccountsListItemSelected: function (selectionModel, dataRecord, rowIndex) {
				var continueAnchor = Ext.get('transferAction');
				continueAnchor.replaceCls('btnDisabled', 'btnEnabled');
		},
		/**
		*
		*/
		suspenseItemDeselected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems == 0) {
						DCT.Util.enableDisableActionButton(true, 'suspenseActionA');
						DCT.Util.disableComponent('billingSuspenseActionField', true);
				}
		},

		suspenseItemDeselectedTest: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				var radioField = Ext.getCmp('paymentOptionsRadiobuttons').getValue();
				if (selectedItems == 0) {
						var suspendAnchor = Ext.get('_redistributeAction');
						suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
				}
		},
		/**
		*
		*/
		suspenseItemSelectedTest: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems > 0) {
						var suspendAnchor = Ext.get('_redistributeAction');
						suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');


				}
		},
		/**
		*
		*/
		suspenseItemSelected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems > 0) {
						var suspenseAction = Ext.getCmp('billingSuspenseActionField');
						DCT.Util.enableDisableActionButton(DCT.Util.checkIfSuspenseFieldBlank(suspenseAction.getValue()), 'suspenseActionA');
						DCT.Util.disableComponent('billingSuspenseActionField', false);
				}
		},
		/**
		*
		*/
		setSuspenseItemSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				if (extRecord.data['authorized'] == 'true')
						return '<div class="x-grid-row-checker">&#160;</div>';
				else
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
		},
		/**
		*
		*/
		suspenseItemCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							if (extRecord.data['authorized'] == 'true'){
								return false;
							}else{
								return true;
							}
						}, me);
	
				return foundRecords.getCount();
		},
		/**
		*
		*/
		setWrittenOffItemSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				if (extRecord.data['writeOffReasonCode'] == 'TCOL')
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
				else
						return '<div class="x-grid-row-checker">&#160;</div>';
		},
		/**
		*
		*/
		writtenOffItemCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							if (extRecord.data['writeOffReasonCode'] == 'TCOL'){
								return true;
							}else{
								return false;
							}
						}, me);
	
				return foundRecords.getCount();
		},
		/**
		*
		*/
		checkSuspenseItemSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				if (recordData.data['authorized'] == 'true')
						return true;
				else
						return false;
		},
		/**
		*
		*/
		checkWrittenOffItemSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				if (recordData.data['writeOffReasonCode'] == 'TCOL')
						return false;
				else
						return true;
		},
		/**
		*
		*/
		writtenOffItemDeselected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems == 0) {
						DCT.Util.enableDisableActionButton(true, '_reverseAction');
				}
		},
		/**
		*
		*/
		writtenOffItemSelected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems > 0) {
						DCT.Util.enableDisableActionButton(DCT.Util.checkIfReverseReasonFieldBlank(), '_reverseAction');
				}
		},
		/**
		*
		*/
		setBatchSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				if ((extRecord.data['StatusCode'] == 'E') || (extRecord.data['StatusCode'] == 'O'))
						return '<div class="x-grid-row-checker">&#160;</div>';
				else
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
		},
		/**
		*
		*/
		batchCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							if ((extRecord.data['StatusCode'] == 'E') || (extRecord.data['StatusCode'] == 'O')){
								return false;
							}else{
								return true;
							}
						}, me);
	
				return foundRecords.getCount();
		},
		/**
		*
		*/
		checkBatchSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				if ((recordData.data['StatusCode'] == 'E') || (recordData.data['StatusCode'] == 'O'))
						return true;
				else
						return false;
		},
		/**
		*
		*/
		setReverseBatchSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				if (extRecord.data['StatusCode'] == 'P')
						return '<div class="x-grid-row-checker">&#160;</div>';
				else
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
		},
		/**
		*
		*/
		reverseBatchCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							if (extRecord.data['StatusCode'] == 'P'){
								return false;
							}else{
								return true;
							}
						}, me);
	
				return foundRecords.getCount();
		},
		/**
		*
		*/
		checkReverseBatchSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				if (recordData.data['StatusCode'] == 'P')
						return true;
				else
						return false;
		},
		/**
		*
		*/
		scheduledItemForOpenItemsDeselected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems == 0) {
						DCT.Util.enableDisableActionButton(true, 'applyOpenItemsAction');
						DCT.Util.disableComponent('openItemsActionField', true);
				}
		},
		/**
		*
		*/
		scheduledItemForOpenItemsSelected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems > 0) {
						DCT.Util.enableDisableActionButton(DCT.Util.checkIfOpenItemsFieldBlank(), 'applyOpenItemsAction');
						DCT.Util.disableComponent('openItemsActionField', false);
				}
		},
		/**
		*
		*/
		scheduledItemForStatementDeselected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems == 0) {
						DCT.Util.enableDisableActionButton(true, 'applyStatementAction');
						DCT.Util.disableComponent('statementActionField', true);
				}
		},
		/**
		*
		*/
		scheduledItemForStatementSelected: function (selectionModel, dataRecord, rowIndex) {
				var selectedItems = selectionModel.getCount();
				if (selectedItems > 0) {
						DCT.Util.enableDisableActionButton(DCT.Util.checkIfStatementFieldBlank(), 'applyStatementAction');
						DCT.Util.disableComponent('statementActionField', false);
				}
		},
		/**
		*
		*/
		setScheduledItemSelectionCheckbox: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
				statementType = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/Type', extRecord.store.proxy.reader.rawData);
				statementStatus = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/StatementStatus', extRecord.store.proxy.reader.rawData);
				if (!Ext.isDefined(statementType))
						return '<div class="x-grid-row-checker">&#160;</div>';
				else if ((statementType == 'IP') && (statementStatus != 'A') && (statementStatus != 'D') && (statementStatus != 'IP'))
						return '<div class="x-grid-row-checker">&#160;</div>';
				else
						return '<div class="x-grid-row-checker-disable">&#160;</div>';
		},
		/**
		*
		*/
		scheduledItemCheckboxAdjustedCount: function () {
			var me = this,
					foundRecords = me.queryBy(function(extRecord, recordId){
							var statementType = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/Type', extRecord.store.proxy.reader.rawData),
									statementStatus = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/StatementStatus', extRecord.store.proxy.reader.rawData);
							if (Ext.isDefined(statementType) && (!((statementType == 'IP') && (statementStatus != 'A') && (statementStatus != 'D') && (statementStatus != 'IP')))){
								return true;
							}else{
								return false;
							}
						}, me);
	
				return foundRecords.getCount();						
		},
		/**
		*
		*/
		checkScheduledItemsSelectionCheckbox: function (selectionModel, recordData, rowIndex, eOpts) {
				statementType = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/Type', recordData.store.proxy.reader.rawData);
				statementStatus = Ext.DomQuery.selectValue('/page/content/OpenList/StatementHeader/StatementStatus', recordData.store.proxy.reader.rawData);
				if (!Ext.isDefined(statementType))
						return true;
				else if ((statementType == 'IP') && (statementStatus != 'A') && (statementStatus != 'D'))
						return true;
				else
						return false;
		},
		/**
		*
		*/
		holdEventItemSelectedDeselected: function (selectionModel, dataRecord, rowIndex) {
				var holdEventSelected=false;
				var holdDisbEventSelected=false;

			//Select Deselect from "Hold Bill/ Hold Follow-up" table
				var holdEventList=Ext.getCmp('holdEventsList');
				if (holdEventList) {
					var billingEventList = holdEventList.getSelectionModel();
					if (billingEventList.getCount() == 0) {
						DCT.Util.getSafeElement('holdEventId').value = "";
						holdEventSelected = false;
					}
					else {
						DCT.Util.getSafeElement('holdEventId').value = billingEventList.getSelection()[0].get('id');
						holdEventSelected = true;
					}
				}

			//Select Deselect from "Hold Disb" table
				var holdDisbursementEventList=Ext.getCmp('holdDisbursementEventsList');
				if (holdDisbursementEventList) {
					var disbEventList = holdDisbursementEventList.getSelectionModel();
					if (disbEventList.getCount() == 0) {
						DCT.Util.getSafeElement('holdDisbEventId').value = "";
						holdDisbEventSelected = false;
					}
					else {
						DCT.Util.getSafeElement('holdDisbEventId').value = disbEventList.getSelection()[0].get('id');
						holdDisbEventSelected = true;
					}
				}
				
				if (holdEventSelected || holdDisbEventSelected) {//Enable "Place Hold" button
					DCT.Util.enableDisableActionButton(false, 'holdAction');
				}
				else{//Disable "Place Hold" button
					DCT.Util.enableDisableActionButton(true, 'holdAction');
				}
		}
});
