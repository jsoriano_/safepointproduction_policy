
/**
*
*/
Ext.define('DCT.BillingDateField', {
    extend: 'DCT.SystemDateField',

    inputXType: 'dctbillingdatefield',
    xtype: 'dctbillingdatefield',

});
DCT.Util.reg('dctbillingdatefield', 'billingDateField', 'DCT.BillingDateField');

//==================================================                                         
// Billing Basic Change Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicChangeDateField', {
    extend: 'DCT.BillingDateField',

    inputXType: 'dctbasicchangedatefield',
    xtype: 'dctbasicchangedatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            change: {
                fn: function (date, newValue, oldValue) {
                    if (date.isValid())
                        DCT.Util.enableDisableActionButton(me.dctCheckFunction(date), me.dctActionButton);
                    else
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                },
                scope: me
            },
            select: {
                fn: function (date, selectedDate) {
                    DCT.Util.enableDisableActionButton(me.dctCheckFunction(date), me.dctActionButton);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbasicchangedatefield', 'basicChangeDateField', 'DCT.BasicChangeDateField');           

//==================================================                                         
// Billing Basic KeyUp date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicKeyUpDateField', {
    extend: 'DCT.BasicChangeDateField',

    inputXType: 'dctbasickeyupdatefield',
    xtype: 'dctbasickeyupdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (date, e) {
                    if (date.isValid())
                        DCT.Util.enableDisableActionButton(me.dctCheckFunction(date), me.dctActionButton);
                    else
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbasickeyupdatefield', 'basicKeyUpDateField', 'DCT.BasicKeyUpDateField');           

//==================================================                                         
// Billing Cash Management date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.CashManagementDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctcashmanagementdatefield',
    xtype: 'dctcashmanagementdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (datefield, e) {
                    me.submitPage(datefield, e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    submitPage: function (datefield, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            if (datefield.isValid()) {
                DCT.Submit.submitBillingPageActionWithCheck(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
            }
        }
    },
    /**
	*
	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkIfBatchSearchFieldsBlank());
    }

});
DCT.Util.reg('dctcashmanagementdatefield', 'cashManagementDateField', 'DCT.CashManagementDateField');         
//==================================================                                         
// Billing Closed Matches date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ApplyFilterDateField', {
    extend: 'DCT.BillingDateField',

    inputXType: 'dctapplyfilterdatefield',
    xtype: 'dctapplyfilterdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            change: {
                fn: function (date, newValue, oldValue) {
                    if (date.isValid())
                        DCT.Grid.applyScheduledItemsFiltersToList(Ext.getCmp(me.dctDataGridId).getStore());
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctapplyfilterdatefield', 'applyFilterDateField', 'DCT.ApplyFilterDateField');         

//==================================================                                         
// Billing Deposit Batch date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.DepositBatchDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctdepositbatchdatefield',
    xtype: 'dctdepositbatchdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (datefield, e) {
                    me.loadGrid(datefield, e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    loadGrid: function (datefield, e) {
        var me = this;

        if (e.getKey() == e.ENTER) {
            if (datefield.isValid()) {
                DCT.Grid.applySearchFieldsToGrid(Ext.getCmp(me.dctDataGridId).getStore());
            }
        }
    },
    /**
	*
	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkIfBatchSearchFieldsBlank());
    }
});
DCT.Util.reg('dctdepositbatchdatefield', 'depositBatchDateField', 'DCT.DepositBatchDateField');

//==================================================                                         
// Billing Hold Account date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.HoldAccountDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctholdaccountdatefield',
    xtype: 'dctholdaccountdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;

        me.callParent([config]);
        Ext.apply(config.listeners, {
            change: {
                fn: function (date, newValue, oldValue) {
                    me.showHideResumeOptions(newValue != '');
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctholdaccountdatefield', 'holdAccountDateField', 'DCT.HoldAccountDateField');
//==================================================                                         
// Billing EFT Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.EFTDateField', {
    extend: 'DCT.BillingDateField',

    inputXType: 'dcteftdatefield',
    xtype: 'dcteftdatefield',

    /**
    *
    */
    setListeners: function (config) {
        var me = this;

        Ext.apply(config.listeners, {
            change: {
                fn: function (date, newValue, oldValue) {
                    if (date.isValid()) {
                        DCT.Util.enableDisableActionButton(false, me.dctActionButton);
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }

                },
                scope: me
            },
            keyup: {
                fn: function (date, e) {
                    if (date.isValid()) {
                        DCT.Util.enableDisableActionButton(false, me.dctActionButton);
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            },
            select: {
                fn: function (date, e) {
                    if (date.isValid()) {
                        DCT.Util.enableDisableActionButton(false, me.dctActionButton);
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dcteftdatefield', 'eftDateField', 'DCT.EFTDateField');           
//==================================================                                         
// Billing Search Payment date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.SearchPaymentDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctsearchpaymentdatefield',
    xtype: 'dctsearchpaymentdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (datefield, e) {
                    me.submitPage(datefield, e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    submitPage: function (datefield, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            if (datefield.isValid()) {
                DCT.Submit.submitBillingPageAction(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
            }
        }
    },
    /**
	*
	*/
    dctCheckFunction: function () {
        var me = this;
        return (me.checkIfPaymentSearchFieldsBlank());
    }
});
DCT.Util.reg('dctsearchpaymentdatefield', 'searchPaymentDateField', 'DCT.SearchPaymentDateField');         
//==================================================                                         
// Billing Installment Date control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.InstallmentDateField', {
    extend: 'DCT.BillingDateField',

    inputXType: 'dctinstallmentdatefield',
    xtype: 'dctinstallmentdatefield',

    /**
    *
    */
    setDisable: function (config) {
    }

});
DCT.Util.reg('dctinstallmentdatefield', 'installmentDateField', 'DCT.InstallmentDateField');
//==================================================                                         
// Billing Hold Event Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingHoldEventDateField', {
    extend: 'DCT.BasicChangeDateField',

    inputXType: 'dctbillingholdeventdatefield',
    xtype: 'dctbillingholdeventdatefield',

    /**
 	*
 	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkIfHoldEventFieldsBlank());
    },


	/**
	*
	*/
	setListeners: function (config) {
	var me = this;

	me.callParent([config]);
	Ext.apply(config.listeners, {
		change: {
			fn: function (date) {
				var me = this;
				return (me.checkIfHoldEventFieldsBlank());
			},
			scope: me
		}
	});
}

});
DCT.Util.reg('dctbillingholdeventdatefield', 'billingHoldEventDateField', 'DCT.BillingHoldEventDateField');           

//==================================================                                         
// Billing Hold Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingHoldDateField', {
	extend: 'DCT.BasicChangeDateField',
	inputXType: 'dctbillingholddatefield',
	xtype: 'dctbillingholddatefield',

	/**
	*
	*/
	dctCheckFunction: function (date) {
		var me = this;
		return me.checkIfHoldFieldsBlank();
	},

	setListeners: function(config) {
		var me = this;

		me.callParent([config]);
		Ext.apply(config.listeners, {
			blur: {
				scope: me,
				fn: function(event, eventOptions) {
					me.checkIfHoldFieldsBlank();
				}
			}
		});
	},

	validateStartDate: function() {
		var currentProcessingDateValue = new Date(Date.parse(DCT.Util.getSafeElement('_currentSystemDate').value));
		var currentProcessingDate = new Date(currentProcessingDateValue.getFullYear(), currentProcessingDateValue.getMonth(), currentProcessingDateValue.getDate());

		var holdStartDateValue = Ext.getCmp('holdStartDate').getValue();
		var holdStartDate = new Date(holdStartDateValue.getFullYear(), holdStartDateValue.getMonth(), holdStartDateValue.getDate());

		return holdStartDate >= currentProcessingDate;
	},

	validateEndDate: function() {
		var holdIndefinitely = Ext.getCmp('holdBillingIndefinitely').getValue();
		if (holdIndefinitely)
			return true;

		var holdStartDateValue = Ext.getCmp('holdStartDate').getValue();
		var holdStartDate = new Date(holdStartDateValue.getFullYear(), holdStartDateValue.getMonth(), holdStartDateValue.getDate());

		var holdEndDateValue = Ext.getCmp('holdEndDate').getValue();
		var holdEndDate = new Date(holdEndDateValue.getFullYear(), holdEndDateValue.getMonth(), holdEndDateValue.getDate());

		return holdEndDate > holdStartDate;
	}
});
DCT.Util.reg('dctbillingholddatefield', 'billingHoldDateField', 'DCT.BillingHoldDateField');           
//==================================================                                         
// Billing Edit Hold Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingEditHoldDateField', {
    extend: 'DCT.BillingDateField',
    inputXType: 'dctbillingeditholddatefield',
    xtype: 'dctbillingeditholddatefield',

    /**
 	*
 	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkIfEditHoldFieldsBlank());
    }
});
DCT.Util.reg('dctbillingeditholddatefield', 'billingEditHoldDateField', 'DCT.BillingEditHoldDateField');           
//==================================================                                         
// Billing Add Statement Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingAddStatementDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctbillingaddstatementdatefield',
    xtype: 'dctbillingaddstatementdatefield',

    /**
 	*
 	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkIfAddItemFieldsBlank());
    }
});
DCT.Util.reg('dctbillingaddstatementdatefield', 'billingAddStatementDateField', 'DCT.BillingAddStatementDateField');           

//==================================================                                         
// Billing Agency Received Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.AgencyReceivedDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctagencyreceiveddatefield',
    xtype: 'dctagencyreceiveddatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            afterrender: {
                fn: function (datefield) {
                    var checkDateTip = DCT.T("DatePaymentReceivedByCarrier");
                    Ext.create('Ext.ToolTip', {
                        target: datefield.id,
                        html: Ext.String.trim(checkDateTip),
                        showDelay: 100,
                        dismissDelay: 0,
                        trackMouse: true
                    });
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctagencyreceiveddatefield', 'agencyReceivedDateField', 'DCT.AgencyReceivedDateField');
//==================================================                                         
// Billing Agency Post Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.AgencyPostDateField', {
    extend: 'DCT.BillingDateField',

    inputXType: 'dctagencypostdatefield',
    xtype: 'dctagencypostdatefield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            afterrender: {
                fn: function (datefield) {
                    var postDateTip = DCT.T("DatePaymentAdded");
                    Ext.create('Ext.ToolTip', {
                        target: datefield.id,
                        html: Ext.String.trim(postDateTip),
                        showDelay: 100,
                        dismissDelay: 0,
                        trackMouse: true
                    });
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctagencypostdatefield', 'agencyPostDateField', 'DCT.AgencyPostDateField');
//==================================================                                         
// Billing Manual Start Date control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingManualStartDateField', {
    extend: 'DCT.BasicKeyUpDateField',

    inputXType: 'dctbillingmanualstartdatefield',
    xtype: 'dctbillingmanualstartdatefield',

    /**
 	*
 	*/
    dctCheckFunction: function (date) {
        var me = this;
        return (me.checkRedistributeInput(date));
    }
});
DCT.Util.reg('dctbillingmanualstartdatefield', 'billingManualStartDateField', 'DCT.BillingManualStartDateField');           

