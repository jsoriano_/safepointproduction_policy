
/**
*
*/
Ext.define('DCT.BillingTextAreaField', {
    extend: 'DCT.SystemTextAreaField',

    inputXType: 'dctbillingtextarea',
    xtype: 'dctbillingtextarea',

    /**
 	*
 	*/
    blankCheck: function (value) {
        return false;
    }
});
DCT.Util.reg('dctbillingtextarea', 'billingTextAreaField', 'DCT.BillingTextAreaField');

//==================================================
// Enabling text area control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnableTextAreaField', {
    extend: 'DCT.BillingTextAreaField',

    inputXType: 'dctbillingenabletextareafield',
    xtype: 'dctbillingenabletextareafield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textarea, e) {
                    DCT.Util.enableDisableActionButton(me.blankCheck(textarea.getValue()), me.dctActionButton);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    }
});
DCT.Util.reg('dctbillingenabletextareafield', 'billingEnableTextAreaField', 'DCT.BillingEnableTextAreaField');

//==================================================
// Item Matched Comment text area control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingItemMatchedTextAreaField', {
    extend: 'DCT.BillingEnableTextAreaField',

    inputXType: 'dctbillingitemmatchedtextareafield',
    xtype: 'dctbillingitemmatchedtextareafield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.checkIfReverseItemFieldsBlank())
    }
});
DCT.Util.reg('dctbillingitemmatchedtextareafield', 'billingItemMatchedTextAreaField', 'DCT.BillingItemMatchedTextAreaField');