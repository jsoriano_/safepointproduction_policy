/**
* @class DCT.Grid
*/
Ext.define('DCT.InstallmentTreeGridPanel', {
	extend: 'DCT.TreeGridPanel',

	inputXType: 'dctinstallmenttreegridpanel',
	xtype: 'dctinstallmenttreegridpanel',
	emptyText: DCT.T('InstallmentScheduleNotFound'),
	
	/**
	*
	*/
	setModel: function (config) {
		Ext.define('DCT.Installment', {
				extend: 'Ext.data.TreeModel',
				childType: 'DCT.ScheduleSubGroup',
				fields: [
					{ name: 'Balance', type: 'string' },
					{ name: 'ClosedRedistributed', type: 'string' },
					{ name: 'ClosedToCash', type: 'string' },
					{ name: 'ClosedToCredit', type: 'string' },
					{ name: 'ClosedWriteOff', type: 'string' },
					{ name: 'DueDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'EFTStatusCode', type: 'string' },
					{ name: 'InstallmentAmount', type: 'string' },
					{ name: 'InstallmentDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'OriginalDueDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'Type', type: 'string' }
				]
		});
		Ext.define('DCT.ScheduleSubGroup', {
				extend: 'Ext.data.TreeModel',
				childType: 'DCT.ScheduleSubGroupItem',
				fields: [
					{ name: 'Balance', type: 'string' },
					{ name: 'ClosedRedistributed', type: 'string' },
					{ name: 'ClosedToCash', type: 'string' },
					{ name: 'ClosedToCredit', type: 'string' },
					{ name: 'ClosedWriteOff', type: 'string' },
					{ name: 'InstallmentAmount', type: 'string' },
					{ name: 'ScheduleSubGroupDescription', type: 'string' }
				]
		});
		Ext.define('DCT.ScheduleSubGroupItem', {
				extend: 'Ext.data.TreeModel',
				fields: [
					{ name: 'Balance', type: 'string' },
					{ name: 'BillItemId', type: 'string' },
					{ name: 'ClosedRedistributed', type: 'string' },
					{ name: 'ClosedToCash', type: 'string' },
					{ name: 'ClosedToCredit', type: 'string' },
					{ name: 'ClosedWriteOff', type: 'string' },
					{ name: 'CoverageReference', type: 'string' },
					{ name: 'CurrencyCulture', type: 'string' },
					{ name: 'DetailId', type: 'string' },
					{ name: 'DetailTypeCode', type: 'string' },
					{ name: 'DueDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'EFTStatusCode', type: 'string' },
					{ name: 'InstallmentAmount', type: 'string' },
					{ name: 'InstallmentTypeCode', type: 'string' },
					{ name: 'ItemAmount', type: 'string' },
					{ name: 'ItemEffectiveDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'PolicyTermId', type: 'string' },
					{ name: 'OriginalDueDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'ReceivableTypeCode', type: 'string' },
					{ name: 'TransactionDate', type: 'date', dateFormat: 'Y-m-d' },
					{ name: 'TransactionTypeCode', type: 'string' },
					{ name: 'Type', type: 'string' }
				]
		});
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			viewready: {
				fn: function (treeGrid, eOpts) {
					var me = this;
	        var defaultDetailId = Ext.get("_detailId").getValue();
	        var defaultAllocationCode = Ext.get("_allocationCode").getValue();
	        var defaultStartDate = Ext.get('_defaultStartDt').getValue();
	        if (!Ext.isEmpty(defaultStartDate)){
	        	var installmentDate = new Date(defaultStartDate);
	        	var record = treeGrid.getStore().findRecord('InstallmentDate', installmentDate);
	        	if (record){
	        		treeGrid.expandNode(record, true, function(treePanel){
	        			var me = this;
	        			if (!Ext.isEmpty(defaultDetailId)){
		        			var recordNode = treeGrid.getStore().findRecord('DetailId', defaultDetailId, record.data.index);
		        			if (recordNode){
			        			var allocationCd = recordNode.get('DetailTypeCode');
		   			        if (!Ext.isEmpty(defaultAllocationCode) && !Ext.isEmpty(allocationCd) && defaultAllocationCode == allocationCd){
											me.view.getSelectionModel().select(recordNode);
										}
									}
		        		}
	        		});
	        	}
	        }
				},
				scope: me
			}
		};		
	}

});
DCT.Util.reg('dctinstallmenttreegridpanel', 'installmentTreeGridPanel', 'DCT.InstallmentTreeGridPanel');

/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
		/**
	*
	*/
		createRowHelp: function (rowValues) {
				var record = rowValues.record,
						view = rowValues.view,
						quickHelpTxt = "",
						effectiveDate = record.get('ItemEffectiveDate');

				if (Ext.isDefined(effectiveDate)) {
						var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value,
								displayEffDate = "Effective Date: " + Ext.Date.format(effectiveDate, displayDateFormat),
								transDate = record.get('TransactionDate'),
								displayTransDate = "Transaction Date: " + Ext.Date.format(transDate, displayDateFormat),
								description = record.get('Description'),
								receivableSubType = record.get('ReceivableSubTypeCode');
						if (Ext.isDefined(description))
								description = "Description: " + description;
						else
								description = "";
						if (Ext.isDefined(receivableSubType))
								receivableSubType = DCT.T("ReceivableSubTypeCode") + ": " + receivableSubType;
						else
								receivableSubType = "";
								
						quickHelpTxt = displayEffDate + ' / ' + displayTransDate + (description == "" ? description : ' / ' + description) + (receivableSubType == "" ? receivableSubType : ' / ' + receivableSubType);
						
						rowValues.rowAttr['data-qtip'] = quickHelpTxt || '';
						rowValues.rowAttr['data-qtitle'] = '';
				}
		},
		/**
	*
	*/
		createRowClass: function (record, rowIdx, rowParams, store) {
				var className = "";
						installmentTypeCode = record.get('InstallmentTypeCode');

				if (Ext.isDefined(installmentTypeCode) && installmentTypeCode == "MR") {
					className = "italic-text";
				}
				return className;
		}
		
});


/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
		/**
	*
	*/
		loadScheduleHistory: function (historyId, historyText) {
				DCT.Util.getSafeElement('_historyId').value = historyId;
				var tree = Ext.getCmp('installmentScheduleTreePanel');
        tree.getStore().load({
			    callback: function(records, operation, success) {
						DCT.Util.getSafeElement('_historyId').value = "";
			  	}
			  });
				Ext.getCmp('installmentStartDate').disable();
				Ext.getCmp('installmentEndDate').disable();
				Ext.getCmp('refresh').disable();
				var treeToolbar = Ext.getCmp('treeToolbar');
				treeToolbar.items.items[0].setText(DCT.T("ScheduleHistory", { history: historyText }));
		}
});


