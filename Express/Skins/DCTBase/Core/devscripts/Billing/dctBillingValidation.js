/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    validateAvailableForSuspense: function (gridData) {
        var selectedItems = gridData.grid.getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].data['availableForSuspense'] == 'N')
                return false;
        }
        return true;
    },
    /**
	*
	*/
    /*
    Validation function for evaluating if eChecks are authorized for a particular action.
    For eChecks in pending status, Disburse and Write offs are not allowed.
    */
	validateECheckAuthorizedAction: function (suspenseAction, gridListData) {
		//If action is not disburse or Write off or Transfer, then they are authorized.
		if (suspenseAction != 'DB' && suspenseAction != 'WO' && suspenseAction != 'TR')
	        return true;

        //For disburse, Write off and transfer check if atleast one echeck is selected with pending status.
	    var selectedItems = gridListData.grid.getSelectionModel().getSelected();
	    var bECheckSelected = false;
	    for (i = 0; i < selectedItems.items.length; i++) {
	        if (selectedItems.items[i].data['paymentMethod'].toUpperCase() == '1EFT' && selectedItems.items[i].data['StatusCode'].toUpperCase() == 'P') {
	            bECheckSelected = true;
	            break;
	        }
	    }
	    return !bECheckSelected;
	},
    /**
    *
    */
    /*
    Validation function for evaluating if Card are Captured for a particular action.
    For not Captured Cards, Disburse and Write offs are not allowed.
    */
	validateCardCapturedAction: function (suspenseAction, gridListData) {
	    //If action is not disburse or Write off, then they are authorized.
	    if (suspenseAction != 'DB' && suspenseAction != 'WO')
	        return true;

	    //For disburse and Write off, check if atleast one card selected is not captured.
	    var selectedItems = gridListData.grid.getSelectionModel().getSelected();
	    var bECheckSelected = false;
	    for (i = 0; i < selectedItems.length; i++) {
			var selectedItem = selectedItems.items[i];
	        if ((selectedItem.data['paymentMethod'].toUpperCase() == '1XCC' || selectedItem.data['paymentMethod'].toUpperCase() == '1XDC' ||
                selectedItem.data['paymentMethod'].toUpperCase() == 'RCC' || selectedItem.data['paymentMethod'].toUpperCase() == 'RDC')
                && selectedItem.data['cardPaymentStatus'].toUpperCase() != 'C') {
	            bECheckSelected = true;
	            break;
	        }
	    }
	    return !bECheckSelected;
	},
    /**
    *
    */
    validateAvailableForDisbursement: function (gridListData) {
	    var refundMethod, accountNumber, token;
        var valid = true;
        var selectedItems = gridListData.grid.getSelectionModel().getSelection();
        //if more than one refund method is in the list for disburements return false
        for (i = 0; i < selectedItems.length; i++) {
            if (i == 0) {
                refundMethod = selectedItems[i].data['refundMethod'];
	            accountNumber = selectedItems[i].data['accountNumber'];
	            token = selectedItems[i].data['token']
            }
            else if (refundMethod != selectedItems[i].data['refundMethod']) {
                valid = false;
                break;
            }
	        else if ((refundMethod == 'CC' || refundMethod == 'DC'|| refundMethod == 'EFT' || refundMethod == '1EFT')
                     && (accountNumber != selectedItems[i].data['accountNumber'])) {
                valid = false;
                break;
            }
	        else if ((refundMethod == '1XDC' || refundMethod == '1XCC' || refundMethod == 'RDC' || refundMethod == 'RCC')
                     && ((token != selectedItems[i].data['token']) || (accountNumber != selectedItems[i].data['accountNumber']))) {
	            valid = false;
	            break;
	        }
	    }
        //override if refunding to check
        var refundToCheck = false;
        var refundToEFT = false;
        var refundToCheckControl = Ext.getCmp('refundToCheck');
        if (refundToCheckControl) {
            refundToCheck = refundToCheckControl.getValue();
        }
        var refundToEFTControl = Ext.getCmp('refundToEFT');
        if (refundToEFTControl) {
            refundToEFT = refundToEFTControl.getValue();
        }

        if (refundToCheck || refundToEFT) {
            valid = true;
        }
        return valid;
    },
    /**
	*
	*/
    validateAvailableForHoldorRelease: function (gridListData) {
        var suspenseAction = Ext.getCmp('billingSuspenseActionField').getValue();
        var selectedItems = gridListData.grid.getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            if (suspenseAction == selectedItems[i].data['statusCd'])
                return false;
        }
        return true;
    },
    /**
	*
	*/
    validateBillingScreen: function (processScreen, fieldId) {
        var me = this;
        var msgText = "";
        var throwAlert = false;
        var focusField;
        var fieldValue;
        switch (processScreen) {
            case "formPost":
                var accountPolicyName = Ext.getCmp('accountPolicyName');
                if (accountPolicyName) {
                    focusField = accountPolicyName;
                    fieldValue = focusField.getValue();
                    if (fieldValue != true) {
                        msgText = DCT.T("PaymentReferenceRequired");
                        throwAlert = true;
                    }
                }
                break;
            case 'manualInstallmentRedistribution':
                var radioField = Ext.getCmp('paymentOptionsRadiobuttons').getValue();
                if (radioField.getRawValue() == 'accountLevel') {
                    focusField = Ext.getCmp('startDate');
                    fieldValue = focusField.getValue();
                    if (Ext.isEmpty(fieldValue)) {
                        msgText = DCT.T("PaymentStartDateRequired");
                        throwAlert = true;
                    }
                    else
                        throwAlert = false;

                    if (!throwAlert) {
                        focusField = Ext.getCmp('numberOfInstallments');
                        fieldValue = focusField.getValue();
                        if (Ext.isEmpty(fieldValue)) {
                            msgText = DCT.T("PaymentCountRequired");
                            throwAlert = true;
                        }
                        else
                            throwAlert = false;
                    }
                }

                else {
                    throwAlert = false;
                }
                break;
            case 'processWriteOffPayment':
                focusField = Ext.getCmp('billingWriteOffSuspenseActionField');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("WriteOffReasonRequired");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'reversePayment':
                focusField = Ext.getCmp('billingReverseReasonField');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("ReasonRequiredReversal");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'processSuspendPayment':
                focusField = Ext.getCmp('billingSuspendReasonField');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("ReasonRequiredSuspension");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'suspenseList':
                focusField = Ext.getCmp('billingSuspenseActionField');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("RequiredSuspenseAction");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'accountSearch':
                focusField = Ext.getCmp('quickSearchTextId');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("RequiredSearchKey");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'reverseMatch':
                focusField = Ext.getCmp('reversalReasonCodeId');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("ReasonRequiredReversal");
                    throwAlert = true;
                }
                else
                    throwAlert = false;

                if (!throwAlert) {
                    focusField = Ext.getCmp('comment');
                    fieldValue = focusField.getValue();
                    if (Ext.isEmpty(fieldValue)) {
                        msgText = DCT.T("RequiredComment");
                        throwAlert = true;
                    }
                    else
                        throwAlert = false;
                }
                break;
            case 'accountDashBoardSearch':
                focusField = Ext.getCmp('searchByNbr');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("RequiredSearchKey");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'transferToAccountSearch':
            case 'referenceSearch':
                focusField = Ext.getCmp('searchByNbr');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("RequiredReferenceNumber");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'processEftRequestDetails':
                throwAlert = true;
                focusField = Ext.getCmp('drawDate');
                var drawDate = focusField.getValue();
                if (Ext.isEmpty(drawDate)) {
                    DCT.Util.enableDisableActionButton(true, 'eftRequestAction');
                    msgText = DCT.T("RequiredDrawDate");
                } else {
                    var dateMask = Ext.get('_hiddenDateDisplayFormat').getValue();
                    var vDate = Ext.Date.format(drawDate, 'Y-m-d');
                    if (vDate != '') {
                    	var dateField = Ext.getDom('_hiddenDrawDate');
                    	dateField.value = vDate;

                    	var drawAmtField = Ext.getCmp('DrawAmt');
                    	if (drawAmtField != undefined && drawAmtField != null) {
                    		var drawAmt = drawAmtField.getValue();
                    		var hidDrawAmtField = Ext.getDom('_hiddenDrawAmount');
                    		hidDrawAmtField.value = drawAmt;

                    	}
                        throwAlert = false;
                    }
                }
                break;
            case 'processEftRequestCancel':
                throwAlert = false;
                break;
            case 'processAddBatchPayment':
                focusField = Ext.getCmp('paymentAmount');
                var batchTotal = Ext.get('_hiddenBatchTotalAmt').getValue();
                var controlTotal = Ext.get('_hiddenControlTotalAmt').getValue();
                fieldValue = focusField.getValue();
                var totalAmt = Number(parseFloat(batchTotal) + parseFloat(fieldValue)).toFixed(2);
                if (parseFloat(controlTotal) > 0) {
                    if (parseFloat(totalAmt) > parseFloat(controlTotal)) {
                        msgText = DCT.T("BatchTotalExceedsControlTotal");
                        throwAlert = true;
                    } else
                        throwAlert = false;
                } else
                    throwAlert = false;

                if (!throwAlert) {
                    focusField = Ext.getCmp('expMonth');
                    fieldValue = focusField.getValue();
                    var paymentMethod = Ext.getCmp('paymentMethod').getValue();
                    if (paymentMethod == 'DC' || paymentMethod == 'PAC' || paymentMethod == 'PAD') {
                        var today = new Date();
                        var currentMonth = today.getMonth() + 1;
                        var currentYear = today.getYear();
                        var expMonth = Ext.getCmp('expMonth').getValue();
                        var expYear = Ext.getCmp('expYear').getValue();
                        if ((expYear == currentYear) && (expMonth < currentMonth)) {
                            msgText = DCT.T("ExpirationMonthAndYearRangeError");
                            throwAlert = true;
                        }
                        else
                            throwAlert = false;

                        if (!throwAlert) {
                            var cardField = Ext.getCmp('_cardNumberId');
                            if (!cardField.isValid()) {
                                msgText = DCT.T("InvalidCardNumber");
                                focusField = cardField;
                                throwAlert = true;
                            }
                            else
                                throwAlert = false;
                        }
                    } else
                        throwAlert = false;
                }
                break;
            case 'processUpdateBatchPayment':
                focusField = Ext.getCmp('expMonth');
                fieldValue = focusField.getValue();
                var paymentMethod = Ext.getCmp('paymentMethod').getValue();
                if (paymentMethod == 'DC') {
                    var today = new Date();
                    var currentMonth = today.getMonth() + 1;
                    var currentYear = today.getYear();
                    var expMonth = Ext.getCmp('expMonth').getValue();
                    var expYear = Ext.getCmp('expYear').getValue();
                    if ((expYear == currentYear) && (expMonth < currentMonth)) {
                        msgText = DCT.T("ExpirationMonthAndYearRangeError");
                        throwAlert = true;
                    }
                    else
                        throwAlert = false;

                    if (!throwAlert) {
                        var cardField = Ext.getCmp('_cardNumberId');
                        if (!cardField.isValid()) {
                            msgText = DCT.T("InvalidCardNumber");
                            focusField = cardField;
                            throwAlert = true;
                        }
                        else
                            throwAlert = false;
                    }
                }
                else
                    throwAlert = false;
                break;
            case 'processMakeAPayment':
                var isAgencyLite = (Ext.get('_hiddenAgencyLiteInd').getValue() == 'true');
                var suspenseAmount = Ext.get('totalSuspenseAmtId').getValue();
                throwAlert = false;
                if (isAgencyLite && Ext.getCmp("manuallyAllocatePaymentId").getValue() && parseFloat(Ext.get('amountAllocatedTotalId').getValue()) > parseFloat(suspenseAmount)) {
                    var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: Ext.getDom('_hiddenDisplayAmtMask').value });
                    msgText = DCT.T("AllocatedAmountExceedsSuspenseAmount", { amount: numberFormat.formatFloat(suspenseAmount) });
                    throwAlert = true;
                    focusField = Ext.getCmp('_otherAmountId');     //Payment amount field.
                    break;
                }
            case 'processAddPayment':
                focusField = Ext.getCmp('expMonth');
                fieldValue = focusField.getValue();
                var paymentMethod = Ext.getCmp('paymentMethod').getValue();
                if (paymentMethod == 'DC') {
                    var today = new Date();
                    var currentMonth = today.getMonth() + 1;
                    var currentYear = today.getYear();
                    var expMonth = Ext.getCmp('expMonth').getValue();
                    var expYear = Ext.getCmp('expYear').getValue();
                    if ((expYear == currentYear) && (expMonth < currentMonth)) {
                        msgText = DCT.T("ExpirationMonthAndYearRangeError");
                        throwAlert = true;
                    }
                    else
                        throwAlert = false;

                    if (!throwAlert) {
                        var cardField = Ext.getCmp('_cardNumberId');
                        if (!cardField.isValid()) {
                            msgText = DCT.T("InvalidCardNumber");
                            focusField = cardField;
                            throwAlert = true;
                        }
                        else
                            throwAlert = false;
                    }
                } else
                    throwAlert = false;
                break;
            case 'processSingleBatch':
                focusField = Ext.get('addPayment');
                var batchTotal = Ext.get('_hiddenBatchTotalAmt').getValue();
                var controlTotal = Ext.get('_hiddenControlTotalAmt').getValue();
                if (parseFloat(controlTotal) > 0) {
                    if (parseFloat(batchTotal) != parseFloat(controlTotal)) {
                        msgText = DCT.T("BatchTotalMismatch");
                        throwAlert = true;
                    } else
                        throwAlert = false;
                } else
                    throwAlert = false;

                if (!throwAlert) {
                    if (parseFloat(controlTotal) == 0 && parseFloat(batchTotal) == 0) {
                        msgText = DCT.T("BatchTotalZero");
                        throwAlert = true;
                    } else
                        throwAlert = false;
                }
                break;
            case 'processBatch':
                focusField = Ext.getCmp('_processAction');
                fieldValue = focusField.getValue();
                if (Ext.isEmpty(fieldValue)) {
                    msgText = DCT.T("RequiredProcessingAction");
                    throwAlert = true;
                } else if ((fieldValue == 'deposit') && (!DCT.Util.batchTotalsValid())) {
                    msgText = DCT.T("BatchTotalError");
                    throwAlert = true;
                } else if ((fieldValue == 'reverse') && (!DCT.Util.batchItemsValid())) {
                    msgText = DCT.T("BatchTotalOpenStatus");
                    throwAlert = true;
                }
                else
                    throwAlert = false;
                break;
            case 'paymentSearch':
                if (fieldId != 'searchPayment') {
                    focusField = Ext.getCmp(fieldId);
                    switch (fieldId) {
                        case 'exactAmount':
                        case 'startAmount':
                        case 'endAmount':
                            if (!focusField.isValid())
                                return false;
                            break;
                        case 'exactDate':
                        case 'startDate':
                        case 'endDate':
                            if (!focusField.isValid()) {
                                return false;
                            }
                            break;
                    }
                }
                var isDateRangeOption = Ext.getCmp('_dateRange').getValue();
                var isAmtRangeOption = Ext.getCmp('_amtRange').getValue();
                if (isDateRangeOption) {
                    var currentSystemDate = Ext.get('_currentSystemDate');
                    var today = Ext.isEmpty(currentSystemDate) ? new Date() : new Date(currentSystemDate.getValue());

                    var dateFieldCmp = Ext.getCmp('startDate');
                    var startDate = dateFieldCmp.getValue();
                    var endDate = Ext.getCmp('endDate').getValue();
                    if (((Ext.isEmpty(startDate)) && (!Ext.isEmpty(endDate))) || ((!Ext.isEmpty(startDate)) && (Ext.isEmpty(endDate)))) {
                        if (Ext.isEmpty(startDate))
                            focusField = Ext.getCmp('startDate');
                        else
                            focusField = Ext.getCmp('endDate');
                        msgText = DCT.T("RequiredSearchDates");
                        throwAlert = true;
                    }
                    else if (startDate > endDate) {
                        focusField = Ext.getCmp('startDate');
                        msgText = DCT.T("DateStartGreaterThanEnd");
                        throwAlert = true;
                    }
                    else if (endDate > today || startDate > today) {
                        if (startDate > today)
                            focusField = Ext.getCmp('startDate');
                        else
                            focusField = Ext.getCmp('endDate');
                        msgText = DCT.T("DatePastToday");
                        throwAlert = true;
                    } else if ((!Ext.isEmpty(startDate)) && (!Ext.isEmpty(endDate))) {
                        if (startDate.getTime() == endDate.getTime()) {
                            focusField = Ext.getCmp('startDate');
                            msgText = DCT.T("DatesEqual");
                            throwAlert = true;
                        } else
                            throwAlert = false;
                    } else
                        throwAlert = false;
                } else
                    throwAlert = false;

                if (!throwAlert) {
                    if (isAmtRangeOption) {
                        var startAmt = Ext.getCmp('startAmount').getValue();
                        var endAmt = Ext.getCmp('endAmount').getValue();
                        var startAmtHidden = Ext.isEmpty(startAmt) ? 0 : Ext.get('_startAmountHidden').getValue();
                        var endAmtHidden = Ext.isEmpty(endAmt) ? 0 : Ext.get('_endAmountHidden').getValue();
                        if (((Ext.isEmpty(startAmt)) && (!Ext.isEmpty(endAmt))) || ((!Ext.isEmpty(startAmt)) && (Ext.isEmpty(endAmt)))) {
                            if (Ext.isEmpty(startAmt))
                                focusField = Ext.getCmp('startAmount');
                            else
                                focusField = Ext.getCmp('endAmount');
                            msgText = DCT.T("AmountsRequired");
                            throwAlert = true;
                        } else if (parseFloat(startAmtHidden) > parseFloat(endAmtHidden)) {
                            focusField = Ext.getCmp('startAmount');
                            msgText = DCT.T("AmountStartPastEnd");
                            throwAlert = true;
                        } else if (parseFloat(startAmtHidden) == parseFloat(endAmtHidden) && startAmtHidden > 0 && endAmtHidden > 0) {
                            focusField = Ext.getCmp('startAmount');
                            msgText = DCT.T("AmountsCannotBeEqual");
                            throwAlert = true;
                        } else
                            throwAlert = false;
                    }
                }
                break;
        }
        if (throwAlert) {
            me.removeTabs(true);
            Ext.Msg.alert(DCT.T('Alert'), msgText, function (btn) {
                me.removeTabs(false);
                focusField.focus();
            }, me);
        }

        return (!throwAlert);
    },
    /**
	*
	*/
    verifyAllocationAmounts: function (linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        totalSuspenseElement = Ext.get('totalSuspenseAmtId');
        totalRemainElement = Ext.get('amountRemainingTotalId');

        if (parseFloat(totalSuspenseElement.getValue()) == parseFloat(totalRemainElement.getValue())) {
            me.removeTabs(true);
            Ext.Msg.confirm(DCT.T('Confirm'), DCT.T("AmountsNotAllocated"), function (btn, text) {
                me.removeTabs(false);
                if (btn == 'yes') {
                    DCT.Submit.submitBillingPageAction('suspenseList', 'processAllocationPayment', true);
                } else
                    Ext.getCmp('processAllocation').focus();
            }, me);
        } else {
            DCT.Submit.submitBillingPageAction('suspenseList', 'processAllocationPayment', true);
        }
    },
    /**
	*
	*/
    batchTotalsValid: function () {
        var selectedItems = Ext.getCmp('waitingDepositBatchList').getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            var controlTotal = selectedItems[i].data['ControlTotal'];
            var batchTotal = selectedItems[i].data['BatchTotal']
            if (parseFloat(controlTotal) > 0) {
                if (parseFloat(batchTotal) != parseFloat(controlTotal))
                    return false;
            } else {
                if (parseFloat(batchTotal) == 0)
                    return false;
            }
        }
        return true;
    },
    /**
	*
	*/
    batchItemsValid: function () {
        var selectedItems = Ext.getCmp('depositedBatchList').getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            var statusCd = selectedItems[i].data['StatusCode'];
            if (statusCd == 'O')
                return false;
        }
        return true;
    },
    /**
	*
	*/
    validateAvailableForHold: function (gridListData) {
        var selectedItems = gridListData.grid.getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].data['ItemStatusCode'] != 'BILL')
                return false;
        }
        return true;
    },
    /**
	*
	*/
    validateAvailableForRelease: function (gridListData) {
        var selectedItems = gridListData.grid.getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].data['ItemStatusCode'] == 'BILL')
                return false;
        }
        return true;
    },
    /**
	*
	*/
    validateAvailableForDeletion: function (gridListData) {
        var selectedItems = gridListData.grid.getSelectionModel().getSelection();
        for (i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].data['ItemObjectType'] != 'CA')
                return false;
        }
        return true;
    },
    /**
	*
	*/
    validateCommentsCreated: function () {
        var globalReason = Ext.getCmp('holdReasonField').getValue();
        var globalComment = Ext.get('hiddenGlobalComment').getValue();

        if (globalReason != '') {
            if ((globalReason == 'OTHR') && (globalComment == ''))
                return false;
        } else {
            var numOfRows = Ext.get('tblItemListDetail').dom.rows.length - 1;
            for (i = 1; i <= numOfRows; i++) {
                var reason = Ext.getCmp("holdItemReasonField" + i).getValue();
                var comment = Ext.get("hiddenComment" + i).getValue();
                if ((reason == 'OTHR') && (comment == ''))
                    return false;
            }
        }
        return true;
    },
    /**
	*
	*/
    validateRemainingBalanceOption: function () {
        var balance = Ext.get('hiddenBalance').getValue();
        if (balance != '0.0000') {
            var leaveOpenBalanceOption = Ext.getCmp('leaveOpenBalanceOption').getValue();
            var writeOffOption = Ext.getCmp('writeOffOption').getValue();
            var writeOffReason = Ext.getCmp('writeOffReasonCode').getValue();

            if (!leaveOpenBalanceOption && !writeOffOption)
                return 'balanceOption';

            if (writeOffOption && writeOffReason == '')
                return 'writeOffReasonBlank';
        }
        return '';
    },
    /**
	*
	*/
    cancelEftRequest: function (requestId) {
        var me = this;
        DCT.Submit.actionVerify(DCT.T("CancelTransaction"), me.completeEftRequestCancel);
    },
    /**
	*
	*/
    completeEftRequestCancel: function (btn) {
        if (btn != 'no') {
            DCT.Submit.submitBillingPageAction('accountSummary', 'processEftRequestCancel', true, 'eftRequestCancel');
        }
    },

    validateEditHoldEndDate: function () {
        var holdEndDate = Ext.getCmp('holdEndDate').getValue();
        var holdStartDate = Ext.getCmp("holdStartDate").getValue();
        var holdIndefinitely = Ext.getCmp("holdBillingIndefinitely").getValue();
        var validDate = false;

        if (holdIndefinitely) {
            validDate = true;
        }

        else if (!Ext.isEmpty(holdEndDate)) {

            if (Date.parse(holdEndDate) <= Date.parse(holdStartDate)) {
                validDate = false;
                Ext.create('DCT.AlertMessage', {
                    msg: DCT.T("StartDateGreaterThanEndDateError"),
                    title: DCT.T("Alert")
                }).show();
            } else {
                validDate = true;
            }

        }
        else {
            validDate = false;
        }


        return validDate;
    }



});
