/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	*
	*/
	cancelApproval: function(focusField, buttonArray, checkFieldArray){
		DCT.Util.getSafeElement('_statementStatus').value = '';
		DCT.Util.hideShowGroup("approveStatement",false);
		var buttons = buttonArray.split("|");
		for (var i=0; i<buttons.length; i++){
			var field = buttons[i].split("!");
			if (field[1] == 'button')
				DCT.Util.enableDisableActionButton(false, field[0]);
			else
				Ext.getCmp(field[0]).setDisabled(false);
		}
		var fieldGroup = checkFieldArray.split("!");
		if (Ext.getCmp(fieldGroup[0]).getValue() != ''){
				DCT.Util.enableDisableActionButton(false, fieldGroup[1]);
				Ext.getCmp(fieldGroup[0]).setDisabled(false);
		}
		Ext.getCmp(focusField).focus();
	},
	/**
	*
	*/
	cancelItemRemoval: function(focusField, buttonsString, dataGridStore){
		DCT.Util.hideShowGroup("removeItems",false);
		var buttons = buttonsString.split("|");
		for (var i=0; i<buttons.length; i++) {
			var field = buttons[i].split("!");
			if (field[1] == 'button')
				DCT.Util.enableDisableActionButton(false, field[0]);
			else
				Ext.getCmp(field[0]).setDisabled(false);
		}
	
	//release the locked grid
	if (dataGridStore.grid.getSelectionModel().isLocked())
	    dataGridStore.grid.getSelectionModel().unlock();
	   
	Ext.getCmp(focusField).focus();
	}
});