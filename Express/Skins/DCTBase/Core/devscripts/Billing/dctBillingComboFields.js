//==================================================                                         
// Billing Basic Combo control                                                                
//==================================================

/**
*
*/
Ext.define('DCT.BasicComboField', {
		extend: 'DCT.SystemComboField',

		inputXType: 'dctbasiccombofield',
		xtype: 'dctbasiccombofield',

	/**
	*
	*/
		blankCheck: function (value) {
				return false;
		},
		/**
	*
	*/
		dctChangeFunction: function (value) {
		}
});
DCT.Util.reg('dctbasiccombofield', 'basicComboField', 'DCT.BasicComboField');           

//==================================================                                         
// Billing Basic Enabling Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.EnablingComboField', {
		extend: 'DCT.BasicComboField',

		inputXType: 'dctenablingcombofield',
		xtype: 'dctenablingcombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						select: {
								fn: function (combo, record, eOpts) {
										var me = this;
										DCT.Util.enableDisableActionButton(me.blankCheck(combo.getValue()), me.dctActionButton);
										if (Ext.isDefined(me.dctChangeFunction)) {
												me.dctChangeFunction(combo.getValue());
										}
								},
								scope: me
						}
				});
		}
});
DCT.Util.reg('dctenablingcombofield', 'enablingComboField', 'DCT.EnablingComboField');           

//==================================================                                         
// Billing Processing Org Unit Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BatchProcessingOrgUnitComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbatchprocessingorgunitcombofield',
		xtype: 'batchprocessingorgunitcombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						keypress: {
								fn: function (combo, e) {
										var me = this;
										me.onEnter(e);
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var processingOrgUnit = Ext.getCmp('processingOrgUnit').getValue();
				if (Ext.isEmpty(value) || Ext.isEmpty(batchNbr))
						return true;
				else
						return false;
		},
		/**
	*
	*/
		onEnter: function (e) {
				var me = this;
				if (e.getKey() == e.ENTER) {
						DCT.Submit.createBatch('processingOrgUnit', me.dctActionButton);
						return false;
				}
		}
});
DCT.Util.reg('batchprocessingorgunitcombofield', 'batchProcessingOrgUnitComboField', 'DCT.BatchProcessingOrgUnitComboField');

//==================================================                                         
// Billing Batch Payment Method Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BatchPaymentMethodComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbatchpaymentmethodcombofield',
		xtype: 'dctbatchpaymentmethodcombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						keypress: {
								fn: function (combo, e) {
										var me = this;
										me.onEnter(e);
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var batchNbr = Ext.getCmp('batchNbr').getValue();
				if (Ext.isEmpty(value) || Ext.isEmpty(batchNbr))
						return true;
				else
						return false;
		},
		/**
	*
	*/
		onEnter: function (e) {
				var me = this;
				if (e.getKey() == e.ENTER) {
						DCT.Submit.createBatch('batchPayment', me.dctActionButton);
						return false;
				}
		}
});
DCT.Util.reg('dctbatchpaymentmethodcombofield', 'batchPaymentMethodComboField', 'DCT.BatchPaymentMethodComboField');       

//==================================================                                         
// Billing Basic Blank Check Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicBlankCheckComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbasicblankcheckcombofield',
		xtype: 'dctbasicblankcheckcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				if (Ext.isEmpty(value))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctbasicblankcheckcombofield', 'basicBlankCheckComboField', 'DCT.BasicBlankCheckComboField');           
//==================================================                                         
// Billing Payment Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicEnablingComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbasicenablingcombofield',
		xtype: 'dctbasicenablingcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.dctCheckFunction());
		}
});
DCT.Util.reg('dctbasicenablingcombofield', 'basicEnablingComboField', 'DCT.BasicEnablingComboField');           

//==================================================                                         
// Billing Hold Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ShowHideComboField', {
		extend: 'DCT.BasicComboField',

		inputXType: 'dctshowhidecombofield',
		xtype: 'dctshowhidecombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						select: {
								fn: function (combo, record, eOpts) {
										var me = this;
										DCT.Util.enableDisableActionButton(me.blankCheck(combo.getValue()), me.dctActionButton);
										if (Ext.isDefined(me.dctShowHideFunction)) {
												me.dctShowHideFunction(combo);
										}
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.dctCheckFunction());
		}
});
DCT.Util.reg('dctshowhidecombofield', 'showhideComboField', 'DCT.ShowHideComboField');           

//==================================================                                         
// Billing Hold Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ShowHideCmtsComboField', {
		extend: 'DCT.BasicComboField',

		inputXType: 'dctshowhidecmtscombofield',
		xtype: 'dctshowhidecmtscombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						select: {
								fn: function (combo, record, eOpts) {
										var me = this;
										DCT.Util.enableDisableActionButton(me.blankCheck(combo.getValue()), me.dctActionButton);
										if (Ext.isDefined(me.dctShowHideFunction)) {
												me.dctShowHideFunction(combo, me.dctItemsList, me.dctItemListRow, me.dctCmtDescription, me.dctCmtTypeCode);
										}
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfHoldItemActionFieldBlank());
		}
});
DCT.Util.reg('dctshowhidecmtscombofield', 'showHideCmtsComboField', 'DCT.ShowHideCmtsComboField');           

//==================================================                                         
// Billing Basic Details Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicDetailsComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbasicdetailscombofield',
		xtype: 'dctbasicdetailscombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				switch (me.dctPaymentListCheck) {
						case 'equal':
								if (Ext.isEmpty(value) || (Ext.getCmp(me.dctPaymentListId).getSelectionModel().getCount() == 0))
										return true;
								else
										return false;
								break;
						case 'greater':
								if (Ext.isEmpty(value) || (Ext.getCmp(me.dctPaymentListId).getSelectionModel().getCount() < 0))
										return true;
								else
										return false;
								break;
				}
				return false;
		}
});
DCT.Util.reg('dctbasicdetailscombofield', 'basicDetailsComboField', 'DCT.BasicDetailsComboField');     

//==================================================                                         
// Billing Basic Report Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicProcessComboField', {
		extend: 'DCT.BasicComboField',

		inputXType: 'dctbasicprocesscombofield',
		xtype: 'dctbasicprocesscombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						select: {
								fn: function (combo, record, eOpts) {
										var me = this;
										me.processFunction(combo, record);
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		processFunction: function (combo, record) {
		}
});
DCT.Util.reg('dctbasicprocesscombofield', 'basicProcessComboField', 'DCT.BasicProcessComboField');           

//==================================================                                         
// Billing Report History Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.HistoryReportComboField', {
		extend: 'DCT.BasicProcessComboField',

		inputXType: 'dcthistoryreportcombofield',
		xtype: 'dcthistoryreportcombofield',

		/**
	*
	*/
		processFunction: function (combo, record) {
				var me = this;
				DCT.Submit.openReport(me.dctTargetPage, me.dctReportPath, combo.getValue(), me.dctActionButton);
		}
});
DCT.Util.reg('dcthistoryreportcombofield', 'historyReportComboField', 'DCT.HistoryReportComboField');           
//==================================================                                         
// Billing Report Name Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ReportNameComboField', {
		extend: 'DCT.BasicProcessComboField',

		inputXType: 'dctreportnamecombofield',
		xtype: 'dctreportnamecombofield',

		/**
	*
	*/
		processFunction: function (combo, record) {
				var me = this;
				DCT.Submit.openReport(me.dctTargetPage, combo.getValue(), '', me.dctActionButton);
		}
});
DCT.Util.reg('dctreportnamecombofield', 'reportNameComboField', 'DCT.ReportNameComboField');
//==================================================                                         
// Billing Export Report Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ExportReportComboField', {
		extend: 'DCT.BasicProcessComboField',

		inputXType: 'dctexportreportcombofield',
		xtype: 'dctexportreportcombofield',

		/**
	*
	*/
		processFunction: function (combo, record) {
				var me = this;
				DCT.Submit.exportReport(me.dctTargetPage, me.dctReportPath, me.dctHistoryId, combo.getValue(), me.dctActionButton)
				combo.setValue('');
		}
});
DCT.Util.reg('dctexportreportcombofield', 'exportReportComboField', 'DCT.ExportReportComboField');           
//==================================================                                         
// Billing Payment Search Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.PaymentSearchComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctpaymentsearchcombofield',
		xtype: 'dctpaymentsearchcombofield',

		/**
	*
	*/
		setListeners: function (config) {
				var me = this;
				me.callParent([config]);
				Ext.apply(config.listeners, {
						keypress: {
								fn: function (combo, e) {
										var me = this;
										me.onEnter(e);
								},
								scope: me
						}
				});
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfPaymentSearchFieldsBlank());
		},
		/**
	*
	*/
		onEnter: function (e) {
				var me = this;
				if (e.getKey() == e.ENTER) {
						DCT.Submit.submitBillingPageAction(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
						return false;
				}
		}
});
DCT.Util.reg('dctpaymentsearchcombofield', 'paymentSearchComboField', 'DCT.PaymentSearchComboField');       
//==================================================                                         
// Billing Suspend Payment Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.SuspendPaymentComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctsuspendpaymentcombofield',
		xtype: 'dctsuspendpaymentcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				if (Ext.isEmpty(value) || (Ext.get('actionAmountTotalId').getValue() == 0))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctsuspendpaymentcombofield', 'suspendPaymentComboField', 'DCT.SuspendPaymentComboField');           
//==================================================                                         
// Billing Suspense Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.SuspenseComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctsuspensecombofield',
		xtype: 'dctsuspensecombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.dctCheckFunction(value));
		},
		/**
	*
	*/
		dctChangeFunction: function (value) {
				var me = this;
				me.setRefundToCheck_Checkbox(value);
				me.setRefundToEFT_Checkbox(value);
		},
		/**
	*
	*/
		setRefundToCheck_Checkbox: function (value) {
				var me = this;
				var refundToCheck = Ext.getCmp(me.dctRefundCheckField);
				if (refundToCheck) {
						if (value == 'DB') {
								refundToCheck.enable();
						} else {
								refundToCheck.disable();
								refundToCheck.setValue(false);
						}
				}
		},
		/**
	*
	*/
		setRefundToEFT_Checkbox: function (value) {
				var me = this;
				var refundToEFT = Ext.getCmp(me.dctRefundEftField);
				if (refundToEFT) {
						if (value == 'DB') {
								refundToEFT.enable();
						} else {
								refundToEFT.disable();
								refundToEFT.setValue(false);
						}
				}
		}
});
DCT.Util.reg('dctsuspensecombofield', 'suspenseComboField', 'DCT.SuspenseComboField');               
//==================================================                                         
// Billing Account Search Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.AccountSearchComboField', {
		extend: 'DCT.BasicProcessComboField',

		inputXType: 'dctaccountsearchcombofield',
		xtype: 'dctaccountsearchcombofield',

		/**
	*
	*/
		processFunction: function (combo, record) {
				DCT.Util.hideShowGroup("searchNumberGroup", combo.getValue() != 'party');
		}
});
DCT.Util.reg('dctaccountsearchcombofield', 'accountSearchComboField', 'DCT.AccountSearchComboField');           
//==================================================                                         
// Billing Basic Reason Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BasicReasonComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbasicreasoncombofield',
		xtype: 'dctbasicreasoncombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				if (Ext.isEmpty(value) || (Ext.get('actionAmountTotalId').getValue() == 0))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctbasicreasoncombofield', 'basicReasonComboField', 'DCT.BasicReasonComboField');
//==================================================                                         
// Billing Transfer Reason Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TransferReasonComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dcttransferreasoncombofield',
		xtype: 'dcttransferreasoncombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				if (Ext.isEmpty(value))
						return true;
				else
						return false;
		},
		/**
	*
	*/
		dctChangeFunction: function (value) {
				DCT.Util.hideShowGroup("transferOptionForPayorChange", value == 'COPR');
		}
});
DCT.Util.reg('dcttransferreasoncombofield', 'transferReasonComboField', 'DCT.TransferReasonComboField');
//==================================================                                         
// Billing Hold Option Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ViewDisburseComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctviewdisbursecombofield',
		xtype: 'dctviewdisbursecombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				var reason = Ext.getCmp('billingreSuspendActionField').getValue();
				var hold = 'NA';
				var holdCmp = Ext.getCmp('holdOptionField');

				if (Ext.isDefined(holdCmp)) {
						if (!me.dctIsDisbursed)
								hold = holdCmp.getValue();
						else
								hold = '';
				}
				if ((Ext.isEmpty(reason)) || (Ext.isEmpty(hold)))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctviewdisbursecombofield', 'viewDisburseComboField', 'DCT.ViewDisburseComboField');                      
//==================================================                                         
// Billing Write Off Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.WriteOffComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctwriteoffcombofield',
		xtype: 'dctwriteoffcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var reason = Ext.getCmp('reverseReasonField').getValue();
				var hold = Ext.getCmp('holdOptionField').getValue();
				var actionAnchor = Ext.get('resuspendWriteoff');

				if ((Ext.isEmpty(reason)) || (Ext.isEmpty(hold)))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctwriteoffcombofield', 'writeOffComboField', 'DCT.WriteOffComboField');                      
//==================================================                                         
// Billing WriteOff Reason Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.WriteOffReasonComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctwriteoffreasoncombofield',
		xtype: 'dctwriteoffreasoncombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var reason = Ext.getCmp('billingreBillField').getValue();
				var action = Ext.getCmp('writeOffWaiveReasonId').getValue();
				if ((Ext.isEmpty(reason)) || (Ext.isEmpty(action)))
						return true;
				else
						return false;
		}
});
DCT.Util.reg('dctwriteoffreasoncombofield', 'writeOffReasonComboField', 'DCT.WriteOffReasonComboField');               
//==================================================                                         
// Billing WriteOff Action Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.WriteOffActionComboField', {
		extend: 'DCT.WriteOffReasonComboField',

		inputXType: 'dctwriteoffactioncombofield',
		xtype: 'dctwriteoffactioncombofield',

		/**
	*
	*/
		dctChangeFunction: function (value) {
				if (value == 'WO' || Ext.isEmpty(value))
						Ext.get('_writeoffWaiveAction').query('span[class=g-btn-text]')[0].innerHTML = DCT.T('WriteOff');
				else
						Ext.get('_writeoffWaiveAction').query('span[class=g-btn-text]')[0].innerHTML = DCT.T('Waive');
		}
});
DCT.Util.reg('dctwriteoffactioncombofield', 'writeOffActionComboField', 'DCT.WriteOffActionComboField');               

//==================================================                                         
// Billing Card Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingCardTypeComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbillingcardtypecombofield',
		xtype: 'dctbillingcardtypecombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.dctCheckFunction());
		},
		/**
	*
	*/
		dctChangeFunction: function (value) {
				var me = this;
				Ext.getCmp(me.dctCardNumberField).validate();
		}
});
DCT.Util.reg('dctbillingcardtypecombofield', 'billingCardTypeComboField', 'DCT.BillingCardTypeComboField');           

//==================================================                                         
// Billing Payment Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.PaymentTypeComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctpaymenttypecombofield',
		xtype: 'dctpaymenttypecombofield',

		/**
	*
	*/
		dctChangeFunction: function (value) {
				var me = this;
				(me.dctAgency) ? me.showHideApplyPaymentRadiobutton(value) : me.showHideApplyToRadiobutton(value);
		},
		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.dctCheckFunction());
		},
		/**
	*
	*/
		showHideApplyPaymentRadiobutton: function (paymentType) {
				var currentPromiseToPayGrp = Ext.get("currentPromiseToPayGroup");

				if (currentPromiseToPayGrp) {
						DCT.Util.hideShowGroup("currentPromiseToPayGroup", paymentType != "A");
						Ext.getCmp("currentPromiseToPayId").setValue((paymentType != "A"));
				}
				if (Ext.get("suspenseGroup")) {
						DCT.Util.currentPromiseToPayGroup("suspenseGroup", paymentType != "A");
						Ext.getCmp("suspenseId").setValue((paymentType != "A" && Ext.isEmpty(currentPromiseToPayGrp)));
				}
				DCT.Util.showHidePromisesToPay();
		},
		/**
	*
	*/
		showHideApplyToRadiobutton: function (paymentType) {
				var currentInvoiceGrp = Ext.get("currentInvoiceGroup");
				var currentDueGrp = Ext.get("currentDueGroup");
				var accountBalanceGrp = Ext.get("accountBalanceGroup");
				var otherAmountField = Ext.getCmp("otherAmountId");

				if (currentInvoiceGrp) {
						DCT.Util.hideShowGroup('currentInvoiceGroup', (paymentType != "A"));
						Ext.getCmp("currentInvoiceId").setValue(paymentType != "A");
				}
				if (currentDueGrp) {
						DCT.Util.hideShowGroup('currentDueGroup', paymentType != "A");
						Ext.getCmp("currentDueId").setValue(paymentType != "A" && currentInvoiceGrp == null);
				}
				if (accountBalanceGrp) {
						DCT.Util.hideShowGroup('accountBalanceGroup', paymentType != "A");
						Ext.getCmp("accountBalanceId").setValue(paymentType != "A" && currentInvoiceGrp == null && currentDueGrp == null);
				}
				
				if (otherAmountField){
					otherAmountField.setValue(paymentType == "A" || (currentInvoiceGrp == null && currentDueGrp == null && accountBalanceGrp == null));
	
					var otherAmount = Ext.getCmp('_otherAmountId');
					var otherAmountValue = otherAmount.getValue();
					var otherAmt = parseFloat(otherAmount.formatting.formatNumberToUS(otherAmountValue));
	
					DCT.Util.showHideManualAllocationGroup(paymentType != "A" && (otherAmt > 0) && (currentInvoiceGrp == null && currentDueGrp == null && accountBalanceGrp == null));
				}
		}

});
DCT.Util.reg('dctpaymenttypecombofield', 'paymentTypeComboField', 'DCT.PaymentTypeComboField');           

//==================================================                                         
// Billing Hold Event Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingHoldEventComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbillingholdeventcombofield',
		xtype: 'dctbillingholdeventcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfHoldEventFieldsBlank());
		}
});
DCT.Util.reg('dctbillingholdeventcombofield', 'billingHoldEventComboField', 'DCT.BillingHoldEventComboField');           

//==================================================                                         
// Billing Hold Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingHoldComboField', {
		extend: 'DCT.ShowHideComboField',

		inputXType: 'dctbillingholdcombofield',
		xtype: 'dctbillingholdcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfHoldFieldsBlank());
		},
		/**
	*
	*/
		dctShowHideFunction: function (combo) {
			var me = this;
			if (combo.getValue() == "HD") {
				DCT.Util.hideShowGroup("HoldDisbursementReason", true);
				DCT.Util.hideShowGroup("HoldBillFollowupReason", false);
			}
			else {
				DCT.Util.hideShowGroup("HoldBillFollowupReason", true);
				DCT.Util.hideShowGroup("HoldDisbursementReason", false);
			}
				me.showHideHoldOptions(combo);
		}
});
DCT.Util.reg('dctbillingholdcombofield', 'billingHoldComboField', 'DCT.BillingHoldComboField');
//==================================================                                         
// Billing Hold Reason Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingHoldReasonComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbillingholdreasoncombofield',
		xtype: 'dctbillingholdreasoncombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfHoldFieldsBlank());
		}

	


});
DCT.Util.reg('dctbillingholdreasoncombofield', 'billingHoldReasonComboField', 'DCT.BillingHoldReasonComboField');           
//==================================================                                         
// Billing Item Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingItemTypeComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbillingitemtypecombofield',
		xtype: 'dctbillingitemtypecombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfAddItemFieldsBlank());
		}
});
DCT.Util.reg('dctbillingitemtypecombofield', 'billingItemTypeComboField', 'DCT.BillingItemTypeComboField');
//==================================================                                         
// Billing Item Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BillingItemMatchComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctbillingitemmatchcombofield',
		xtype: 'dctbillingitemmatchcombofield',

		/**
	*
	*/
		blankCheck: function (value) {
				var me = this;
				return (me.checkIfReverseItemFieldsBlank());
		}
});
DCT.Util.reg('dctbillingitemmatchcombofield', 'billingItemMatchComboField', 'DCT.BillingItemMatchComboField');           
//==================================================                                         
// Billing Payment Method Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.PaymentMethodComboField', {
		extend: 'DCT.EnablingComboField',

		inputXType: 'dctpaymentmethodcombofield',
		xtype: 'dctpaymentmethodcombofield',

		/**
	*
	*/
	blankCheck: function (value) {
		return(this.dctCheckFunction());
	}
	
});                                                                                          
DCT.Util.reg('dctpaymentmethodcombofield', 'paymentMethodComboField', DCT.PaymentMethodComboField);

//==================================================                                         
// Billing Basic reversal Reason Combo control                                                                
//==================================================                                         

/**
*
*/
DCT.RevReasonCombofield = Ext.extend(DCT.EnablingComboField, {

    inputXType: 'dctrevreasoncombofield',

    /**
 	*
 	*/
    setListeners: function (config) {
        DCT.RevReasonCombofield.superclass.setListeners.call(this, config);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (combo, e) {
                    this.onEnter(e);
                },
                scope: this
            }
        });
    },
    /**
	*
	*/

    /**
 	*
 	*/
    blankCheck: function (value) {
        var reversalReason = (Ext.getCmp(this.dctCombo)) ? Ext.getCmp(this.dctCombo).getValue() : "";
       
        if (Ext.isEmpty(reversalReason)) {
            return true;
        }
        return false;
    }
});
DCT.Util.reg('dctrevreasoncombofield', 'RevReasonCombofield', DCT.RevReasonCombofield);

