
/**
*
*/
Ext.define('DCT.BillingTextField', {
    extend: 'DCT.SystemTextField',

    inputXType: 'dctbillingtextfield',
    xtype: 'dctbillingtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        return false;
    },
    /**
	*
	*/
    onEnter: function (value, e) {
    },
    /**
	*
	*/
    enableDisable: function (value, e) {
        var me = this;
        DCT.Util.enableDisableActionButton(me.blankCheck(value), me.dctActionButton);
    }

});
DCT.Util.reg('dctbillingtextfield', 'billingTextField', 'DCT.BillingTextField');

//==================================================
// Basic Enabling text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnablingTextField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingenablingtextfield',
    xtype: 'dctbillingenablingtextfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.enableDisable(textfield.getValue(), e);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingenablingtextfield', 'billingEnablingTextField', 'DCT.BillingEnablingTextField');

//==================================================
// Basic Enter Key text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnterTextField', {
    extend: 'DCT.BillingEnablingTextField',

    inputXType: 'dctbillingentertextfield',
    xtype: 'dctbillingentertextfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keypress: {
                fn: function (textfield, e) {
                    me.onEnter(textfield.getValue(), e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    onEnter: function (value, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            DCT.Submit.submitBillingPageAction(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
        }
    }
});
DCT.Util.reg('dctbillingentertextfield', 'billingEnterTextField', 'DCT.BillingEnterTextField');

//==================================================
// Billing Batch Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingBatchNbrTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingbatchnbrtextfield',
    xtype: 'dctbillingbatchnbrtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        if (Ext.isEmpty(Ext.getCmp(me.dctCheckField)) || (Ext.isEmpty(value)))
            return true;
        else
            return false;
    }
});
DCT.Util.reg('dctbillingbatchnbrtextfield', 'billingBatchNbrTextField', 'DCT.BillingBatchNbrTextField');

//==================================================
// Batch Description text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingDescriptionTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingdescriptiontextfield',
    xtype: 'dctbillingdescriptiontextfield',

    /**
 	*
 	*/
    onEnter: function (value, e) {
    }
});
DCT.Util.reg('dctbillingdescriptiontextfield', 'billingDescriptionTextField', 'DCT.BillingDescriptionTextField');
//==================================================
// Billing Search text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingSearchByTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingsearchbytextfield',
    xtype: 'dctbillingsearchbytextfield',

    /**
 	*
 	*/
    onEnter: function (value, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            DCT.Submit.submitReferenceSearch(Ext.getCmp(me.dctSearchTypeField).getValue(), me.dctTargetPage);
        }
    }
});
DCT.Util.reg('dctbillingsearchbytextfield', 'billingSearchByTextField', 'DCT.BillingSearchByTextField');

//==================================================
// Billing Search text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingPaymentShowTextField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingpaymentshowtextfield',
    xtype: 'dctbillingpaymentshowtextfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.showHideFields(textfield.getValue(), e);
                    me.validateFields(textfield.getValue(), e);
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    showHideFields: function (value, e) {
        var me = this;
        DCT.Util.showHidePaymentDetails(!me.dctCheckFunction());
    },
    /**
	*
	*/
    validateFields: function (value, e) {
        var me = this;
        var lastname = Ext.getCmp('unidentifedLastname');
        var account = Ext.getCmp('unidentifedAccount');
        var policy = Ext.getCmp('unidentifedPolicy');
        var other = Ext.getCmp('unidentifedOtherInfo');
        if (!me.dctCheckFunction()) {
            lastname.clearInvalid();
            account.clearInvalid();
            policy.clearInvalid();
            other.clearInvalid();
            lastname.allowBlank = true;
            account.allowBlank = true;
            policy.allowBlank = true;
            other.allowBlank = true;
        } else {
            lastname.allowBlank = false;
            account.allowBlank = false;
            policy.allowBlank = false;
            other.allowBlank = false;
            lastname.validate();
            account.validate();
            policy.validate();
            other.validate();
        }
    }

});
DCT.Util.reg('dctbillingpaymentshowtextfield', 'billingPaymentShowTextField', 'DCT.BillingPaymentShowTextField');
//==================================================
// Billing Payment Enable text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingCheckFunctionEnableTextField', {
    extend: 'DCT.BillingEnablingTextField',

    inputXType: 'dctbillingcheckfunctionenabletextfield',
    xtype: 'dctbillingcheckfunctionenabletextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    }
});
DCT.Util.reg('dctbillingcheckfunctionenabletextfield', 'billingCheckFunctionEnableTextField', 'DCT.BillingCheckFunctionEnableTextField');

//==================================================
// Billing Batch Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingBatchSearchTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingbatchsearchtextfield',
    xtype: 'dctbillingbatchsearchtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        return (me.checkIfBatchSearchFieldsBlank());
    },
    /**
	*
	*/
    onEnter: function (value, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            DCT.Submit.submitBillingPageActionWithCheck(me.dctTargetPage, me.dctAction, me.dctCheckEvent, me.dctActionButton);
        }
    }
});
DCT.Util.reg('dctbillingbatchsearchtextfield', 'billingBatchSearchTextField', 'DCT.BillingBatchSearchTextField');

//==================================================
// Billing Batch Number text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingDepositBatchTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingdepositbatchtextfield',
    xtype: 'dctbillingdepositbatchtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    },
    /**
	*
	*/
    onEnter: function (value, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            DCT.Grid.applySearchFieldsToGrid(Ext.getCmp(me.dctDataGridId).getStore());
        }
    }
});
DCT.Util.reg('dctbillingdepositbatchtextfield', 'billingDepositBatchTextField', 'DCT.BillingDepositBatchTextField');

//==================================================
// Billing Search Payment text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingSearchPaymentTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingsearchpaymenttextfield',
    xtype: 'dctbillingsearchpaymenttextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.checkIfPaymentSearchFieldsBlank())
    }
});
DCT.Util.reg('dctbillingsearchpaymenttextfield', 'billingSearchPaymentTextField', 'DCT.BillingSearchPaymentTextField');

//==================================================
// Billing Trasnfer Search text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingTransferSearchByTextField', {
    extend: 'DCT.BillingEnterTextField',

    inputXType: 'dctbillingtransfersearchbytextfield',
    xtype: 'dctbillingtransfersearchbytextfield',

    /**
 	*
 	*/
    onEnter: function (value, e) {
        var me = this;
        if (e.getKey() == e.ENTER) {
            DCT.Submit.submitTransferToSearch(value, Ext.getCmp(me.dctSearchTypeField).getValue(), me.dctTargetPage);
        }
    }
});
DCT.Util.reg('dctbillingtransfersearchbytextfield', 'billingTransferSearchByTextField', 'DCT.BillingTransferSearchByTextField');

//==================================================
// Billing Hold Event text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingHoldEventTextField', {
    extend: 'DCT.BillingEnablingTextField',

    inputXType: 'dctbillingholdeventtextfield',
    xtype: 'dctbillingholdeventtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.checkIfHoldEventFieldsBlank())
    }
});
DCT.Util.reg('dctbillingholdeventtextfield', 'billingHoldEventTextField', 'DCT.BillingHoldEventTextField');
//==================================================
// Billing Add Statement text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingAddStatementTextField', {
    extend: 'DCT.BillingEnablingTextField',

    inputXType: 'dctbillingaddstatementtextfield',
    xtype: 'dctbillingaddstatementtextfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.checkIfAddItemFieldsBlank())
    }
});
DCT.Util.reg('dctbillingaddstatementtextfield', 'billingAddStatementTextField', 'DCT.BillingAddStatementTextField');
