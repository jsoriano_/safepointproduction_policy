
/**
*
*/
Ext.define('DCT.BillingIntField', {
    extend: 'DCT.BillingInputNumberField',

    inputXType: 'dctbillingintfield',
    xtype: 'dctbillingintfield',

    /**
    *
    */
    setValidator: function (config) {
        var me = this;
        config.validator = me.checkInt;
    },
    /**
    *
    */
    setFormatter: function (config) {
        config.formatter = config.formatting.formatInt;
    },
    /**
    *
    */
    setCultureSymbols: function (config) {
        var me = this;
        config.getCultureSymbols = me.getIntSymbols;
    },
    /**
    *
    */
    setStripSymbols: function (config) {
        var me = this;
        config.getStripSymbols = me.getIntStripSymbols;
    }

});
DCT.Util.reg('dctbillingintfield', 'billingIntField', 'DCT.BillingIntField');

/**
*
*/
Ext.define('DCT.BillingBlurIntField', {
    extend: 'DCT.BillingIntField',

    inputXType: 'dctbillingblurintfield',
    xtype: 'dctbillingblurintfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            blur: {
                fn: function (field) {
                    if (!Ext.isEmpty(field.getValue())) {
                        if (field.isValid() && field.isDirty()) {
                            me.formatField();
                            me.processValidField(field);
                        } else {
                            if (!field.isValid())
                                me.processInValidField(field);
                        }
                    }
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingblurintfield', 'billingBlurIntField', 'DCT.BillingBlurIntField');           
//==================================================
// Billing AddStatement int control 
//==================================================

/**
*
*/
Ext.define('DCT.AddStatementIntField', {
    extend: 'DCT.BillingBlurIntField',

    inputXType: 'dctaddstatementintfield',
    xtype: 'dctaddstatementintfield',

    /**
 	*
 	*/
    processValidField: function (field) {
        var me = this;
        me.setItemAmounts(field);
    }
});
DCT.Util.reg('dctaddstatementintfield', 'addStatementIntField', 'DCT.AddStatementIntField');

/**
*
*/
Ext.define('DCT.ManualInstallmentsIntField', {
    extend: 'DCT.BillingBlurIntField',

    inputXType: 'dctmanualinstallmentsintfield',
    xtype: 'dctmanualinstallmentsintfield',

    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        me.callParent([config]);
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (floatfield, e) {
                    if (floatfield.isValid()) {
                        DCT.Util.enableDisableActionButton(me.blankCheck(), me.dctActionButton);
                    } else {
                        DCT.Util.enableDisableActionButton(true, me.dctActionButton);
                    }
                },
                scope: me
            }
        });
    },
    /**
	*
	*/
    processInValidField: function (field) {
        var me = this;
        if (!field.isValid())
            DCT.Util.enableDisableActionButton(true, me.dctActionButton);
    },
    /**
	*
	*/
    blankCheck: function () {
        var me = this;
        return (me.checkRedistributeInput());
    }

});
DCT.Util.reg('dctmanualinstallmentsintfield', 'manualInstallmentsIntField', 'DCT.ManualInstallmentsIntField');           