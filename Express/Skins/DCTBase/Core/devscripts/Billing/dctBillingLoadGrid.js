/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
    /**
	*
	*/
    applyErrorFiltersToList: function (action, dataStore) {
        var me = this;
        Ext.getDom('_submitAction').value = action;
        dataStore.grid.assignGridSelection(false, "");
        dataStore.grid.getSelectionModel().clearSelections();
        dataStore.loadPage(1);
    },
	/**
	*
	*/
    applyErrorFiltersAndSubmit: function (action, target, dataStore) {
    	Ext.getDom('_submitAction').value = action;
    	Ext.getDom('_targetPage').value = target;
    	dataStore.grid.assignGridSelection(false, "");
    	document.forms[0].submit();
    },
    /**
	*
	*/
    applySearchFieldsToGrid: function (gridData) {
        var me = this;
        if (DCT.Util.isButtonDisabled('searchBatch'))
            return;
        Ext.getDom("_processDefaultDate").value = "";
        Ext.getDom("_entryDefaultDate").value = "";
        gridData.loadPage(1);
    },
    /**
	*
	*/
    applyScheduledItemsFiltersToList: function (dataStore) {
        var me = this;
        dataStore.grid.getSelectionModel().clearSelections();
        dataStore.loadPage(1);
    },
    /**
	*
	*/
    clearScheduledItemsFiltersToList: function (dataStore, linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        Ext.getCmp('moreOptionsGroupPanelCmp').collapse(true);

        if (Ext.get("aggregationField") != null)
            Ext.getDom("aggregationField").selectedIndex = 0;

        if (Ext.get("subAgentsField") != null)
            Ext.getDom("subAgentsField").selectedIndex = 0;

        Ext.getDom("commissionPercent").value = "";
        Ext.getDom("hiddenCommissionPercent").value = "";
        Ext.getDom("_insuredId").value = "";
        if (Ext.get("insuredGroup") != null) {
            Ext.get("insuredGroup").remove(false);
            Ext.getDom('searchForInsuredAction').innerHTML = DCT.T('SearchForInsured');
        }
        DCT.Util.enableDisableActionButton(true, 'clearFilterAction');
        me.applyScheduledItemsFiltersToList(dataStore);
    },
    /**
	*
	*/
    resetSearchFieldsToGrid: function (gridData) {
        var me = this;
        Ext.getCmp("userId").setValue("");
        Ext.getCmp("batchNbr").setValue("");

        me.applySearchFieldsToGrid(gridData);

        DCT.Util.enableDisableActionButton(true, 'searchBatch');
    },
    /**
	*
	*/
    updateCommentTypeCode: function (page, action, commentId, commentTypeCode, commentLockingTS) {
        DCT.Util.getSafeElement('_commentId').value = commentId;
        DCT.Util.getSafeElement('_commentTypeCode').value = commentTypeCode;
        DCT.Util.getSafeElement('_commentLockingTS').value = commentLockingTS;
        DCT.Util.getSafeElement('_submitAction').value = action;
	      Ext.getCmp('CommentsList').getStore().loadPage(1, {
			    callback: function(records, operation, success) {
			    	Ext.getDom('_submitAction').value = "";
			  	}
			  });
    }
});