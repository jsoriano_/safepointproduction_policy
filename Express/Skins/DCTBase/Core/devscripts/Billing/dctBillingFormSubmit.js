/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
    /**
	*
	*/
    billingQuickSearch: function (searchNumber, searchType, validationAction) {
        var me = this;
        DCT.Util.getSafeElement('_accountId').value = "";
        DCT.Util.getSafeElement('_searchNumber').value = searchNumber;
        DCT.Util.getSafeElement('_searchType').value = searchType;

        switch (searchType) {
            case "party":
                Ext.getDom('_submitAction').value = 'partyStart:partySearch';
                DCT.Util.getSafeElement('__submitAction').value = 'accountSearch';
                DCT.Util.getSafeElement('__overrideTarget').value = 'accountSummary';
                break;
            default:
                if (!DCT.Util.validateBillingScreen(validationAction))
                    return;
                Ext.getDom('_submitAction').value = 'accountSearch';
                Ext.getDom('_targetPage').value = 'accountSummary';
                DCT.Submit.setActiveParent('billingDashBoard');
                break;
        }

        me.secureAction("");
        me.submitForm();
        Ext.event.Event.stopEvent;
    },
    /**
    *
    */
    submitBillingPageActionWithCheck: function (page, action, eventChecked, actionButton) {
        var me = this;
        if (typeof (eventChecked) != 'undefined') {
            if (!eventChecked) {
                if (DCT.Util.isButtonDisabled(actionButton))
                    return;
            }
        }
        if (DCT.Util.validateBillingScreen(page, actionButton)) {
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = action;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
    *
    */
    submitBillingPageAction: function (page, action, eventChecked, actionButton) {
        var me = this;
        if (typeof (eventChecked) != 'undefined') {
            if (!eventChecked) {
                if (DCT.Util.isButtonDisabled(actionButton))
                    return;
            }
        }
        if (DCT.Util.validateBillingScreen(action, actionButton)) {
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = action;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
     *
     */
    submitBillingAccountSummaryPage: function (page, accountid) {
        DCT.Util.getSafeElement('_accountId').value = accountid;
        DCT.Submit.submitBillingPage(page);
    },
    /**
	 *
	 */
    submitBillingPageTransferPolicyAction: function (page, transferOption) {
        DCT.Util.getSafeElement('_transferOption').value = transferOption;
        DCT.Submit.submitBillingPage(page);
    },

    /**
	 *
	 */
    submitBillingPageEditHoldBillingForAccountAction: function (page, action, eventChecked, actionButton) {
        if (DCT.Util.validateEditHoldEndDate()) {
            DCT.Util.getSafeElement('_isResumeAction').value = "0";
            DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton);
        }
    },


	
    submitBillingPageHoldForAccountAction: function (page, action, eventChecked, actionButton) {
    	if (DCT.Util.validateEditHoldEndDate()) {
    		DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton);
    	}
    },


    submitEditHoldDisbBillingPage: function (page, action, eventChecked, actionButton) {
		DCT.Util.getSafeElement('EventHoldEdit').value = eventChecked;
    	DCT.Util.getSafeElement('_editBillingHoldMode').value = 'HD';
    	DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton);
    },
	
	submitEditHoldBillingPage: function (page, action, eventChecked, actionButton) {
		DCT.Util.getSafeElement('EventHoldEdit').value = eventChecked;
		DCT.Submit.submitBillingPage(page);
    },
    /**
    *
    */
    submitBillingPageTransferToCollectionsAction: function (page, action, eventChecked, actionButton, gridId) {
        DCT.Util.getSafeElement('_collectionAccountTo').value = Ext.getCmp(gridId).getSelectionModel().getSelection()[0].id;
        DCT.Util.getSafeElement('_agencyReference').value = Ext.getCmp(gridId).getSelectionModel().getSelection()[0].data["AgencyReference"];
        DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton)
    },
    /**
    *
    */
    submitReferenceSearch: function (searchType, page) {
        var me = this;
        DCT.Util.getSafeElement('_accountId').value = "";
        Ext.getDom('_targetPage').value = page;

        switch (searchType) {
            case 'account':
            case 'policy':
                if (!DCT.Util.validateBillingScreen('referenceSearch'))
                    return;

                Ext.getDom('_submitAction').value = 'accountBatchSearch';
                DCT.Util.getSafeElement('_searchMode').value = searchType;
                break;
            case 'party':
                Ext.getDom('_submitAction').value = 'partyStart:partyInvolvementSearch';
                DCT.Util.getSafeElement('_searchMode').value = searchType;
                DCT.Util.getSafeElement('__submitAction').value = 'accountBatchSearch';
                break;

        }
        me.secureAction("");
        me.submitForm();
    },
    /**
    *
    */
    submitTransferToSearch: function (searchNumber, searchType, page) {
        var me = this;
        DCT.Util.getSafeElement('_accountId').value = "";
        Ext.getDom('_targetPage').value = page;
        DCT.Util.getSafeElement('_searchNumber').value = searchNumber;
        DCT.Util.getSafeElement('_searchType').value = searchType;


        switch (searchType) {
            case 'account':
            case 'policy':
                if (!DCT.Util.validateBillingScreen('transferToAccountSearch'))
                    return;
                Ext.getDom('_submitAction').value = 'accountSearch';
                break;
            case 'party':
                Ext.getDom('_submitAction').value = 'partyStart:partyInvolvementSearch';
                DCT.Util.getSafeElement('_searchByNbr').value = "";
                DCT.Util.getSafeElement('__submitAction').value = 'accountSearch';
                break;
        }
        me.secureAction("");
        me.submitForm();
    },
    /**
    *
    */
    submitBillingPage: function (page) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        if (page == 'policySummary')
            DCT.Util.getSafeElement('_policyId').value = "";
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    submitBillingSuspenseAction: function (gridListId, linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        var gridList = Ext.getCmp(gridListId);
        var gridListData = gridList.getStore();
        var suspenseAction = Ext.getCmp('billingSuspenseActionField').getValue();
        switch (suspenseAction) {
            case 'P':
            case 'M':
            case 'N':
                Ext.getDom('_targetPage').value = 'suspenseList';
                Ext.getDom('_submitAction').value = 'suspenseHold';
                break;
            case 'DB':
                Ext.getDom('_targetPage').value = 'refundToSourcePayment';
                break;
            case 'MA':
                Ext.getDom('_targetPage').value = 'allocateSuspensePayment';
                break;
            case 'TR':
                Ext.getDom('_targetPage').value = 'transferPayment';
                break;
	 		case 'WO':
                Ext.getDom('_targetPage').value = 'writeOffPayment';
                break;
        }

        if (DCT.Util.validateBillingScreen('suspenseList', linkId)) {
            if (suspenseAction == 'DB') {
                if (!DCT.Util.validateAvailableForDisbursement(gridListData)) {
                    Ext.getDom('_submitAction').value = '';
                    DCT.Util.removeTabs(true);
		            Ext.Msg.alert(DCT.T('Alert'), DCT.T("Only one Refund Method, Account Number or Card allowed per Disbursement. Please uncheck the invalid item/items."), function (btn) {
                        DCT.Util.removeTabs(false);
                    }, me);
                    return;
                }
            }
		    //If at least one echeck in pending status is selected, prevent disburse and write-off.
		    if (!DCT.Util.validateECheckAuthorizedAction(suspenseAction, gridListData))
		    {
		    	var invalidActionMsg;
		    	if(suspenseAction == 'DB')
		    		invalidActionMsg = "Disbursement ";
		    	else if(suspenseAction == 'WO')
		    		invalidActionMsg = "Write-off ";
		    	else if(suspenseAction == 'TR')
		    		invalidActionMsg = "Transfer ";

				invalidActionMsg = invalidActionMsg	+ "cannot be processed when the payment status is pending.";
		        Ext.Msg.alert(DCT.T('Alert'), DCT.T(invalidActionMsg), function (btn) { });
		        return;
		    }

		    //Do not disburse or write-off in case at least one selected card item is not "Captured".
		    if (!DCT.Util.validateCardCapturedAction(suspenseAction, gridListData)) {
		        var invalidActionMsg = "You cannot " + (suspenseAction == 'DB' ? "disburse " : "write-off ") + "Card payments that have been not Settled.";
		        Ext.Msg.alert(DCT.T('Alert'), DCT.T(invalidActionMsg), function (btn) { });
		        return;
		    }
		    
            if (suspenseAction == 'P' || suspenseAction == 'M' || suspenseAction == 'N') {
                if (DCT.Util.validateAvailableForHoldorRelease(gridListData)) {
                    gridList.assignGridSelection(true, "");
                    me.secureAction(Ext.getDom('_targetPage').value);
                    me.submitForm();
                }
                else {
                    Ext.getDom('_submitAction').value = '';
                    DCT.Util.removeTabs(true);
		            Ext.Msg.alert(DCT.T('Alert'), DCT.T("One or more of the selected items is already set to the chosen action. Please uncheck the invalid item/items."), function (btn) {
                        DCT.Util.removeTabs(false);
                    }, me);
                }
            }
            else {
                gridList.assignGridSelection(false, "");
                me.secureAction(Ext.getDom('_targetPage').value);
                me.submitForm();
            }
        }

    },
    /**
	*
	*/
    processReversal: function (page, action, gridListId, linkId) {
        var me = this;
        if (!DCT.Util.isButtonDisabled(linkId)) {
            Ext.getCmp(gridListId).assignGridSelection(true, "");
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = action;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
    *
    */
    submitBillingActivity: function (page, technicalId, otherId) {
        var me = this;
        if (DCT.Util.getSafeElement('_activeParentPage').value == "")
            DCT.Submit.setActiveParent('billingDashBoard');
        switch (page) {
            case 'account':
                Ext.getDom('_targetPage').value = 'accountSummary';
                DCT.Util.getSafeElement('_accountId').value = technicalId;
                break;
            case 'policy':
                Ext.getDom('_targetPage').value = 'policySummary';
                DCT.Util.getSafeElement('_policyId').value = technicalId;
                break;
            case 'payment':
                Ext.getDom('_targetPage').value = 'paymentDetails';
                DCT.Util.getSafeElement('_technicalId').value = technicalId;
                break;
            case 'invoice':
                Ext.getDom('_targetPage').value = 'invoiceDetails';
                DCT.Util.getSafeElement('_technicalId').value = technicalId;
                break;
            case 'disbursement':
                Ext.getDom('_targetPage').value = 'viewDisbursementDetails';
                DCT.Util.getSafeElement('_technicalId').value = technicalId;
                break;
            case 'writeoff':
                Ext.getDom('_targetPage').value = 'writeoffDetails';
                DCT.Util.getSafeElement('_technicalId').value = technicalId;
                break;
            case 'writeoffinstallment':
                Ext.getDom('_targetPage').value = 'installmentSchedule';
                DCT.Util.getSafeElement('_technicalStartDate').value = technicalId;
                break;
		  	case 'paymentwriteoff':
		  		Ext.getDom('_targetPage').value = 'paymentWriteOffDetails';
		  		DCT.Util.getSafeElement('_technicalId').value = technicalId;
		  		break;
            case 'eftRequest':
                Ext.getDom('_targetPage').value = 'eftRequestDetails';
                DCT.Util.getSafeElement('_technicalId').value = technicalId;
                break;
        }

        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    submitPaymentSuspenseAction: function (gridId) {
        var me = this;
        if (DCT.Util.isButtonDisabled('suspendPaymentA'))
            return;

        var grid = Ext.getCmp(gridId);
        var gridData = grid.getStore();
        Ext.getDom('_targetPage').value = 'suspendPayment';
        grid.assignGridSelection(false, "");
        if (DCT.Util.validateAvailableForSuspense(gridData)) {
            me.secureAction('suspendPayment');
            me.submitForm();
        } else {
            DCT.Util.removeTabs(true);
            Ext.Msg.alert(DCT.T('Alert'), DCT.T("SelectedItemNotAvailableToSuspend"), function (btn) {
                DCT.Util.removeTabs(false);
            }, me);
        }
    },
    /**
	*
	*/
    submitBillingProcessAction: function (page, technicalId) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        DCT.Util.getSafeElement('_technicalId').value = technicalId;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    submitBillingInstallment: function (page, technicalDate, technicalId, technicalCode) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        DCT.Util.getSafeElement('_technicalStartDate').value = technicalDate;
        DCT.Util.getSafeElement("_technicalId").value = technicalId;
        DCT.Util.getSafeElement("_technicalCode").value = technicalCode;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    submitBillingInvoiceReprint: function (page, action, invoiceId) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        Ext.getDom('_submitAction').value = action;
        DCT.Util.getSafeElement('_invoiceId').value = invoiceId;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    deleteBatchPayment: function (page, action, paymentId, paymentTS) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        Ext.getDom('_submitAction').value = action;
        DCT.Util.getSafeElement('_paymentId').value = paymentId;
        DCT.Util.getSafeElement('_paymentTS').value = paymentTS;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    editBatchPayment: function (page, paymentId) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        DCT.Util.getSafeElement('_batchPaymentId').value = paymentId;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    processBatchItems: function (page, action, linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        if (DCT.Util.validateBillingScreen(action)) {
            var processAction = Ext.getCmp('_processAction').getValue();
            switch (processAction) {
                case 'delete':
                    var maxPageSize;
                    var dataGrid = Ext.getCmp('waitingDepositBatchList');
                    var dataGridStore = dataGrid.getStore();
                    var dataSelection = dataGrid.getSelectionModel();
                    Ext.getDom("_submitAction").value = action;
                    dataGrid.assignGridSelection(true, "");
                    dataGridStore.loadPage(1, {
									    callback: function(records, operation, success) {
									    	Ext.getDom('_submitAction').value = "";
										  	}
										});
                    dataSelection.clearSelections();
                    dataSelection.allSelected = false;
                    dataSelection.setPageSelections();
                    break;
                case 'deposit':
                case 'print':
                case 'reverse':
                    Ext.getDom('_targetPage').value = page;
                    Ext.getDom('_submitAction').value = action;
                    Ext.getCmp('waitingDepositBatchList').assignGridSelection(false, "");
                    me.secureAction(page);
                    me.submitForm();
                    break;
            }
        }
    },
    /**
	*
	*/
    processBatchItem: function (page, action, linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        if (DCT.Util.validateBillingScreen(action, linkId)) {
            DCT.Util.getSafeElement('_processAction').value = "deposit";
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = 'processBatch';
            var selectedItemsArray = new Array();
            selectedItemsArray[selectedItemsArray.length] = Ext.get("_technicalId").getValue();
            DCT.Util.getSafeElement('_gridSelectionArray').value = selectedItemsArray;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
	*
	*/
    processResume: function (page, action, eventId, linkId) {
        if (eventId == '') {
            var holdIndefinitelyComponent = Ext.getCmp("holdBillingIndefinitely");
            if (!Ext.isEmpty(holdIndefinitelyComponent)) {
                Ext.getCmp("holdBillingIndefinitely").setValue(false);
                var endDate = Ext.getCmp('holdEndDate');
                endDate.enable();
                endDate.adjustDate(0);
            }
        }

        DCT.Util.getSafeElement('_isResumeAction').value = "1";
        DCT.Submit.submitBillingPageAction(page, action, false, linkId);
    },
    /**
	*
	*/
    submitStopDisbursementInterfaceRequest: function (page, action, linkId) {
        DCT.Util.getSafeElement('_actionType').value = 'stop';
        DCT.Submit.submitBillingPageAction(page, action, false, linkId);
    },
    /**
	*
	*/
    submitResetSystemDate: function (page, action, linkId) {
        DCT.Util.getSafeElement('_resetSytemDate').value = '1';
        DCT.Submit.submitBillingPageAction(page, action, false, linkId);
    },
    /**
	*
	*/
    reverseBatchItems: function (page, action, linkId) {
        var me = this;
        if (DCT.Util.isButtonDisabled(linkId))
            return;

        DCT.Util.getSafeElement('_processAction').value = 'reverse';
        Ext.getDom('_targetPage').value = page;
        Ext.getDom('_submitAction').value = action;
        Ext.getCmp('depositedBatchList').assignGridSelection(false, "");
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    searchPartyForInsured: function () {
        var me = this;
        DCT.Util.getSafeElement('_submitAction').value = 'partyStart:partySearch';
        me.secureAction("");
        me.submitForm();
    },
    /**
	*
	*/
    submitBillingStatement: function (page, technicalDate) {
        var me = this;
        Ext.getDom('_targetPage').value = page;
        DCT.Util.getSafeElement('_defaultStatementDate').value = technicalDate;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    setStatementStatus: function (statusCode, fieldArray, linkId) {
        DCT.Util.getSafeElement('_statementStatus').value = statusCode;
        if (statusCode == 'A') {
            DCT.Util.hideShowGroup("approveStatement", true);
            var buttons = fieldArray.split("|");
            for (var i = 0; i < buttons.length; i++) {
                var field = buttons[i].split("!");
                if (field[1] == 'button')
                    DCT.Util.enableDisableActionButton(true, field[0]);
                else
                    Ext.getDom(field[0]).disabled = true;
            }
        }
        else
            DCT.Submit.submitBillingPageAction('agencyStatement', 'setStatementStatus', false, linkId);
    },
    /**
	*
	*/
    applyAgencyAction: function (actionField, targetPage, dataStore, buttonsString) {
        var me = this;
        var processAction = Ext.getCmp(actionField).getValue();
        switch (processAction) {
            case 'DI':
                if (DCT.Util.validateAvailableForDeletion(dataStore)) {
                    Ext.getDom('_targetPage').value = targetPage;
                    Ext.getDom('_submitAction').value = "deleteManualItem";
                    dataStore.grid.assignGridSelection(false, DCT.Grid.getItemIdForGridProcessing);
                    me.secureAction(targetPage);
                    me.submitForm();
                } else {
                    DCT.Util.removeTabs(true);
                    Ext.Msg.alert(DCT.T('Alert'), DCT.T("OneOrMoreNotCompanyItem"), function (btn) {
                        DCT.Util.removeTabs(false);
                        Ext.get(actionField).focus();
                    }, me);
                }
                break;
            case 'HI':
                if (DCT.Util.validateAvailableForHold(dataStore)) {
                    DCT.Util.hideShowGroup("removeItems", true);
                    var buttons = buttonsString.split("|");
                    for (var i = 0; i < buttons.length; i++) {
                        var field = buttons[i].split("!");
                        if (field[1] == 'button')
                            DCT.Util.enableDisableActionButton(true, field[0]);
                        else
                            Ext.getDom(field[0]).disabled = true;
                    }
                    //lock the grid so no changes are made before the action occurs
                    dataStore.grid.getSelectionModel().lock();
                } else {
                    DCT.Util.removeTabs(true);
                    Ext.Msg.alert(DCT.T('Alert'), DCT.T("OneOrMoreAlreadyHolding"), function (btn) {
                        DCT.Util.removeTabs(false);
                        Ext.get(actionField).focus();
                    }, me);
                }
                break;
            case 'AC':
                var gridTargetPage = (targetPage == "openItems") ? "openItemsList" : "scheduledItemsList";
                DCT.Util.displayAddBillingCommentsPopUp(gridTargetPage, dataStore);
                break;
            case 'RH':
                if (DCT.Util.validateAvailableForRelease(dataStore)) {
                    Ext.getDom('_targetPage').value = targetPage;
                    Ext.getDom('_submitAction').value = "setScheduledItemStatus";
                    DCT.Util.getSafeElement('_itemStatusCode').value = 'BILL';
                    dataStore.grid.assignGridSelection(true, DCT.Grid.getItemIdAndItemTypeForGridProcessing);
                    me.secureAction(targetPage);
                    me.submitForm();
                } else {
                    DCT.Util.removeTabs(true);
                    Ext.Msg.alert(DCT.T('Alert'), DCT.T("OneOrMoreAlreadyReleased"), function (btn) {
                        DCT.Util.removeTabs(false);
                        Ext.get(actionField).focus();
                    }, me);
                }
                break;
            case 'MI':
                var selectedItems = dataStore.grid.getTotalSelectionCount();

                switch (selectedItems) {
                    case 0:
                        DCT.Util.removeTabs(true);
                        Ext.Msg.alert(DCT.T('Alert'), DCT.T("PleaseSelectTwoItems"), function (btn) {
                            DCT.Util.removeTabs(false);
                            Ext.get(actionField).focus();
                        }, me);
                        break;
                    case 1:
                        DCT.Util.removeTabs(true);
                        Ext.Msg.alert(DCT.T('Alert'), DCT.T("PleaseSelectMoreThanOneItem"), function (btn) {
                            DCT.Util.removeTabs(false);
                            Ext.get(actionField).focus();
                        }, me);
                        break;
                    case 2:
                        Ext.getDom('_targetPage').value = "matchScheduledItems";
                        dataStore.grid.assignGridSelection(true, DCT.Grid.getItemIdAndItemTypeForGridProcessing);
                        me.secureAction("matchScheduledItems");
                        me.submitForm();
                        break;
                    default:
                        DCT.Util.removeTabs(true);
                        Ext.Msg.alert(DCT.T('Alert'), DCT.T("OnlyTwoItemsCanBeSelected"), function (btn) {
                            DCT.Util.removeTabs(false);
                            Ext.get(actionField).focus();
                        }, me);
                        break;
                }
                break;
        }
    },
    /**
	*
	*/
    processScheduledItems: function (actionField, dataStore, itemId) {
        var me = this;
        var processAction = Ext.getCmp(actionField).getValue();
        switch (processAction) {
            case 'HI':
                Ext.getDom('_targetPage').value = "holdScheduledItems";
                if (dataStore != null)
                    dataStore.grid.assignGridSelection(false, DCT.Grid.getItemIdAndItemTypeForGridProcessing);
                else
                    DCT.Util.getSafeElement('_itemId').value = itemId;
                me.secureAction("holdScheduledItems");
                me.submitForm();
                break;
        }
    },
    /**
	*
	*/
    applyScheduledItemAction: function (actionField, targetPage, itemId, itemLockingTS) {
        var me = this;
        var processAction = Ext.getCmp(actionField).getValue();
        switch (processAction) {
            case 'HI':
                DCT.Util.hideShowGroup("removeItems", true);
                break;
            case 'AC':
                DCT.Util.displayAddBillingCommentsPopUp(targetPage, dataStore);
                break;
            case 'RH':
                Ext.getDom('_targetPage').value = targetPage;
                Ext.getDom('_submitAction').value = "setScheduledItemStatus";
                DCT.Util.getSafeElement('_itemAction').value = processAction;
                DCT.Util.getSafeElement('_itemStatusCode').value = 'BILL';
                DCT.Util.getSafeElement('_itemId').value = itemId;
                DCT.Util.getSafeElement('_itemLockingTS').value = itemLockingTS;
                me.secureAction(targetPage);
                me.submitForm();
                break;
        }
    },
    /**
	*
	*/
    processStatementImport: function (page, action, gridListId) {
        var me = this;
        if (DCT.Util.isButtonDisabled('importStatementAction'))
            return;

        Ext.getCmp(gridListId).assignGridSelection(false, "");
        Ext.getDom('_targetPage').value = page;
        Ext.getDom('_submitAction').value = action;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    removeSelectedItem: function (removedItemId, page) {
        var me = this;
        var selectedItemsArray = new Array();
        var numOfRows = Ext.getDom('tblItemListDetail').rows.length - 1;
        for (i = 1; i <= numOfRows; i++) {
            itemElement = Ext.getDom('hiddenItemId' + i);
            itemTypeCodeElement = Ext.getDom('hiddenItemTypeCd' + i);
            if (removedItemId != itemElement.value)
                selectedItemsArray[selectedItemsArray.length] = itemElement.value + '|' + itemTypeCodeElement.value;
        }
        DCT.Util.getSafeElement('_gridSelectionArray').value = selectedItemsArray;
        Ext.getDom('_targetPage').value = page;
        me.secureAction(page);
        me.submitForm();
    },
    /**
	*
	*/
    processMatchItems: function (page, action, eventChecked, actionButton) {
        var me = this;
        switch (DCT.Util.validateRemainingBalanceOption()) {
            case 'balanceOption':
                DCT.Util.removeTabs(true);
                Ext.Msg.alert(DCT.T('Alert'), DCT.T("PleaseSelectRemainingBalance"), function (btn) {
                    DCT.Util.removeTabs(false);
                    Ext.get('leaveOpenBalanceOption').focus();
                }, me);
                break;
            case 'writeOffReasonBlank':
                DCT.Util.removeTabs(true);
                Ext.Msg.alert(DCT.T('Alert'), DCT.T("WriteOffReasonRequired"), function (btn) {
                    DCT.Util.removeTabs(false);
                    Ext.get('writeOffReasonCode').focus();
                }, me);
                break;
            default:
                DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton);
                break;
        }
    },
    /**
	*
	*/
    processFormPost: function () {
        var billingInterfaces = Ext.get('_billingInterfaces');
        if (billingInterfaces) {
            var data = billingInterfaces.getValue();
            var xDoc = DCT.Util.loadXMLString(data);
            var method = Ext.DomQuery.selectValue("ExternalPaymentInterface/PaymentSiteURL/@method ", xDoc);
            var url = Ext.DomQuery.selectValue("ExternalPaymentInterface/PaymentSiteURL/@url ", xDoc);
            if (method == "GET") {
                var queryString = Ext.DomQuery.select('ExternalPaymentInterface/PaymentSiteQueryString/param', xDoc);
                if (queryString.length > 0) {
                    url += "?";
                    for (i = 0; i < queryString.length; i++) {
                        var externalName = Ext.DomQuery.selectValue('@externalName', queryString[i]);
                        var value = Ext.DomQuery.selectValue('@value', queryString[i]);
                        var useExpressValue = Ext.DomQuery.selectValue('@useExpressValue', queryString[i]);
                        if (useExpressValue == "False") {
                            url += externalName + '=' + value + "&";
                        }
                        else if (useExpressValue == "True") {
                            var qvalue = Ext.get(value).getValue();
                            if (qvalue != null) {
                                url += externalName + '=' + qvalue + "&";
                            }
                        }

                    }
                }
                window.location.href = url;
            }
            if (method == "POST") {
                alert("POST");
                var postForm = document.createElement("Form");
                document.body.appendChild(postForm);
                postForm.method = "POST";
                postForm.action = url;
                var queryString = Ext.DomQuery.select('ExternalPaymentInterface/PaymentSiteQueryString/param', xDoc);
                if (queryString.length > 0) {
                    for (i = 0; i < queryString.length; i++) {
                        var externalName = Ext.DomQuery.selectValue('@externalName', queryString[i]);
                        var value = Ext.DomQuery.selectValue('@value', queryString[i]);
                        var useExpressValue = Ext.DomQuery.selectValue('@useExpressValue', queryString[i]);
                        var newInput = document.createElement("<input name='" + externalName + "' type='hidden'>");
                        postForm.appendChild(newInput);
                        if (useExpressValue == "False") {
                            postForm.value = value;
                        }
                        else if (useExpressValue == "True") {
                            var qvalue = Ext.get(value).getValue();
                            if (qvalue != null) {
                                postForm.value = qvalue;
                            }
                        }
                    }
                }
                postForm.submit();
            }
        }
    },
    /**
	*
	*/
    submitAgencySuspense: function (page, action, eventChecked, actionButton, gridId) {
        var me = this;
        if (typeof (eventChecked) != 'undefined') {
            if (!eventChecked) {
                if (DCT.Util.isButtonDisabled(actionButton))
                    return;
            }
        }
        if (DCT.Util.validateBillingScreen(action)) {
            Ext.getCmp(gridId).assignGridSelection(true, DCT.Grid.getAgencyAgreementKeysForGridProcessing);
            Ext.getDom('_targetPage').value = page;
            Ext.getDom('_submitAction').value = action;
            me.secureAction(page);
            me.submitForm();
        }
    },
    /**
	*
	*/
    verifyAgencyAllocationAmounts: function (actionButton) {
        if (DCT.Util.isButtonDisabled(actionButton))
            return;

        totalSuspenseElement = Ext.getDom('totalSuspenseAmtId');
        totalRemainElement = Ext.getDom('amountRemainingTotalId');

        if (parseFloat(totalSuspenseElement.value) == parseFloat(totalRemainElement.value)) {
            DCT.Util.removeTabs(true);
            Ext.Msg.alert(DCT.T('Alert'), DCT.T("NoFundsAllocated"), function (btn) {
                DCT.Util.removeTabs(false);
                Ext.get('processAllocation').focus();
            }, me);
        } else {
            DCT.Submit.submitBillingPageAction('suspenseList', 'processAgencyAllocationPayment', true);
        }
    },
    /**
	*
	*/
    displayErrorDetail: function (errorId) {
        var me = this;
        DCT.Util.getSafeElement('_popUp').value = '1';
        DCT.Util.getSafeElement('_exceptionId').value = errorId;
        var errorPopup = Ext.create('DCT.PopupWindow', {
            width: 600,
            height: 500,
            modal: true,
            title: DCT.T("ErrorDetails"),
            dctItemClass: 'DCT.ErrorDetailsPanel',
            defaultButton: 'closeButton',
            buttons: [{
                text: DCT.T('Close'),
                id: 'closeButton',
                handler: function () {
                		var me = this;
                    var popup = me.findParentByType('dctpopupwindow');
                    popup.closePopupWindow();
                }
            }]
        });
        errorPopup.show();
    },

	displayResubmitDetail: function (errorId) {
	    DCT.Util.getSafeElement('_popUp').value = '1';
	    DCT.Util.getSafeElement('_exceptionId').value = errorId;
	    var errorPopup = Ext.create('DCT.PopupWindow', {
	        width: 500,
	        height: 500,
	        modal: true,
	        title: "Resubmission Details",
	        dctItemClass: DCT.ResubmitDetailsPanel,
	    });
	    errorPopup.show();
	},

    /**
	*
	*/
    displayUnidentifiedPaymentsDetail: function (unidentifiedPaymentId) {
        DCT.Util.getSafeElement('_unidentifiedPaymentId').value = unidentifiedPaymentId;
        DCT.Submit.submitPage('unidentifiedPaymentsDetail');
    },
    /**
	*
	*/
    createBatch: function (page, action) {
        if (Ext.get('createBatch').hasCls('btnDisabled'))
            return;
        DCT.Submit.submitBillingPageAction(page, action, true);
    },
    /**
	*
	*/
    processHoldItems: function (page, action, eventChecked, actionButton, focusField) {
        var me = this;
        if (DCT.Util.validateCommentsCreated())
            DCT.Submit.submitBillingPageAction(page, action, eventChecked, actionButton);
        else {
            DCT.Util.removeTabs(true);
            Ext.Msg.alert(DCT.T('Alert'), DCT.T("OneOrMoreNoReasonForHold"), function (btn) {
                DCT.Util.removeTabs(false);
                Ext.get(focusField).focus();
            }, me);
        }
    },
    /**
	*
	*/
    processHoldEvent: function (eventId, page, action, eventTS) {
        DCT.Util.getSafeElement('_eventId').value = eventId;
        DCT.Util.getSafeElement('_eventLockingTS').value = eventTS;
        DCT.Submit.submitBillingPageAction(page, action, true);
    },
    /**
	*
	*/
    gotoMatchedItemDetail: function (itemId) {
        var me = this;
        Ext.getDom('_targetPage').value = "itemMatchedDetails";
        DCT.Util.getSafeElement('_itemId').value = itemId;
        me.secureAction("itemMatchedDetails");
        me.submitForm();
    }
});