
/**
*
*/
Ext.define('DCT.BillingEmailField', {
    extend: 'DCT.BillingTextField',

    inputXType: 'dctbillingemailfield',
    xtype: 'dctbillingemailfield',

    /**
    *
    */
    setVType: function (config) {
        config.vtype = 'email';
    }

});
DCT.Util.reg('dctbillingemailfield', 'billingEmailField', 'DCT.BillingEmailField');

//==================================================
// Basic Enabling Email control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingEnablingEmailField', {
    extend: 'DCT.BillingEmailField',

    inputXType: 'dctbillingenablingemailfield',
    xtype: 'dctbillingenablingemailfield',

    /**
 	*
 	*/
    blankCheck: function (value) {
        var me = this;
        return (me.dctCheckFunction())
    },
    /**
 	*
 	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            keyup: {
                fn: function (textfield, e) {
                    me.enableDisable(textfield.getValue(), e);
                },
                scope: me
            }
        });
    }
});
DCT.Util.reg('dctbillingenablingemailfield', 'billingEnablingEmailField', 'DCT.BillingEnablingEmailField');