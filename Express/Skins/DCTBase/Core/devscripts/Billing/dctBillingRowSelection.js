/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid,{
	/**
	*
	*/
	paymentAllocationItemDeselected: function (selectionModel, dataRecord, rowIndex) {
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems == 0){
	  	if (Ext.get('reversePaymentA')) {
	  		var reverseAnchor = Ext.get('reversePaymentA');
	  		var reasonField = Ext.getCmp('billingReverseReasonField');
	  		if (!Ext.isEmpty(reasonField.getValue()))
	  			reverseAnchor.replaceCls('btnDisabled', 'btnEnabled');
	  		reasonField.setDisabled(false);
	  	}
	  	if (Ext.get('suspendPaymentA')) {
	  		var suspendAnchor = Ext.get('suspendPaymentA');
	  		suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
	  	}
	  }
	},
	/**
	*
	*/
	paymentAllocationItemSelected: function(selectionModel, dataRecord, rowIndex){
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems > 0){
	  	if (Ext.get('reversePaymentA')) {
	  		var reverseAnchor = Ext.get('reversePaymentA');
	  		reverseAnchor.replaceCls('btnEnabled', 'btnDisabled');
	  		var reasonField = Ext.getCmp('billingReverseReasonField');
	  		reasonField.setDisabled(true);
	  	}
	  	if (Ext.get('suspendPaymentA')) {
	  		var suspendAnchor = Ext.get('suspendPaymentA');
	  		suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');
	  	}
	  }
	},
	/**
	*
	*/
	batchItemDeselected: function(selectionModel, dataRecord, rowIndex){
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems == 0){
	  	var suspendAnchor = Ext.get('processBatch');
	    suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
	    DCT.Util.disableComponent('_processAction', true);
	  }
	},
	/**
	*
	*/
	batchItemSelected: function (selectionModel, dataRecord, rowIndex) {
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems > 0){
	  	var suspendAnchor = Ext.get('processBatch');
	    suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');
	    DCT.Util.disableComponent('_processAction', false);
	  }
	},
	/**
	*
	*/
	highLightRow: function(record, rowIndex, rowParams, dataStore){
		var className = DCT.Grid.checkIfAddPolicyHoldClass(record, rowIndex, rowParams, dataStore);
		if(className == "")
		{
			className = DCT.Grid.checkIfAddPolicyCollectionsClass(record, rowIndex, rowParams, dataStore);
		}
		return className;
	},
	/**
	*
	*/
	checkIfAddPolicyHoldClass: function(record, rowIndex, rowParams, dataStore){
		var className = "";
		if (record.data['holdTypeCode'] != 'N' && record.data['holdTypeCode'] != '')
			className = "policyOnHold";
		return className;
	},
	/**
	*
	*/
	checkIfAddPolicyCollectionsClass: function (record, rowIndex, rowParams, dataStore) {
		var className = "";
		if (record.data['collectionStatusCode'] == 'STRT')
			className = "policyInInternalCollections";
		return className;
	},
	/**
	*
	*/
	reverseBatchItemDeselected: function(selectionModel, rowIndex, dataRecord){
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems == 0){
	  	var buttonAnchor = Ext.get('reverseBatch');
	    buttonAnchor.replaceCls('btnEnabled', 'btnDisabled');
	  }
	},
	/**
	*
	*/
	reverseBatchItemSelected: function (selectionModel, dataRecord, rowIndex) {
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems > 0)
	  {
	  	var buttonAnchor = Ext.get('reverseBatch');
	    buttonAnchor.replaceCls('btnDisabled', 'btnEnabled');
	  }
	},
	/**
	*
	*/
	statementImportItemDeselected: function (selectionModel, dataRecord, rowIndex) {
	  if (selectionModel.getCount() == 0){
	  	DCT.Util.enableDisableActionButton(true, 'importStatementAction');
	 	}
	},
	/**
	*
	*/
	statementImportItemSelected: function(selectionModel, dataRecord, rowIndex){
	  if (selectionModel.getCount() > 0){
	  	DCT.Util.enableDisableActionButton(false, 'importStatementAction');
	  }
	},
	/**
	*
	*/
	agencyPaymentAllocationItemDeselected: function(selectionModel, dataRecord, rowIndex){
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems == 0){
	  	var reverseAnchor = Ext.get('reversePaymentA');
	  	var reasonField = Ext.getCmp('billingReverseReasonField');
	  	var suspendField = Ext.getCmp('billingSuspendReasonField');
	  	var suspendAnchor = Ext.get('suspendPaymentA');
	  	if (!Ext.isEmpty(reasonField.getValue()))
	      reverseAnchor.replaceCls('btnDisabled', 'btnEnabled');
	    suspendAnchor.replaceCls('btnEnabled', 'btnDisabled');
	    reasonField.setDisabled(false);
	    suspendField.setDisabled(true);
	  }
	},
	/**
	*
	*/
	agencyPaymentAllocationItemSelected: function(selectionModel, dataRecord, rowIndex){
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems > 0){
	  	var reverseAnchor = Ext.get('reversePaymentA');
	  	var suspendAnchor = Ext.get('suspendPaymentA');
	  	var reasonField = Ext.getCmp('billingReverseReasonField');
	  	var suspendField = Ext.getCmp('billingSuspendReasonField');
	  	if (!Ext.isEmpty(suspendField.getValue()))
	    	suspendAnchor.replaceCls('btnDisabled', 'btnEnabled');
	    reverseAnchor.replaceCls('btnEnabled', 'btnDisabled');
	    reasonField.setDisabled(true);
	    suspendField.setDisabled(false);
	  }
	}
});
