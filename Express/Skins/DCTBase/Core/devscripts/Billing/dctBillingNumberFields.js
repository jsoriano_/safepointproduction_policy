
/**
*
*/
Ext.define('DCT.BillingInputNumberField', {
    extend: 'DCT.InputNumberField',

    inputXType: 'dctbillinginputnumberfield',
    xtype: 'dctbillinginputnumberfield',

    /**
	*
	*/
    setDefaultListeners: function (config) {
        config.listeners = {
        };
    },
    /**
    *
    */
    setListeners: function (config) {
    },
    /**
	*
	*/
    processValidField: function (field) {
    },
    /**
	*
	*/
    processInValidField: function (field) {
    },
    /**
	*
	*/
    setDisable: function (config) {
        config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
    },
    /**
	*
	*/
    setItemAmounts: function (field) {
        //retrieve inputted values, if blank make 0 otherwise use value from input fields 
        var grossAmtCmp = Ext.getCmp('grossAmount');
        var grossAmt = grossAmtCmp.getValue();
        grossAmt = (Ext.isEmpty(grossAmt)) ? "0" : grossAmt;
        var grossAmtValue = grossAmtCmp.formatting.formatNumberToUS(grossAmt);
        var grossAmount = Number(parseFloat(grossAmtValue)).toFixed(2);
        var commissionPctCmp = Ext.getCmp('commissionPercent');
        var commissionPct = commissionPctCmp.getValue();
        commissionPct = (Ext.isEmpty(commissionPct)) ? "0" : commissionPct / 100;
        var commissionPercent = Number(parseFloat(commissionPct)).toFixed(2);

        //calculate values and prepare for display
        var commissionAmt = Number(grossAmount * commissionPercent);
        var netAmt = Number(grossAmtValue - commissionAmt).toFixed(2);
        var commissionAmount = commissionAmt.toFixed(2);

        //update hidden fields (values that will go to database)
        DCT.Util.getSafeElement('_hiddenGrossAmt').value = grossAmtValue;
        DCT.Util.getSafeElement('_hiddenCommissionPct').value = commissionPct;
        DCT.Util.getSafeElement('_hiddenCommissionAmt').value = commissionAmt;
        DCT.Util.getSafeElement('_hiddenNetAmt').value = netAmt;

        //format display fields
        Ext.getDom('commissionAmount').innerHTML = commissionPctCmp.formatting.formatFloat(commissionAmount);
        Ext.getDom('netAmount').innerHTML = grossAmtCmp.formatting.formatFloat(netAmt);
    }
});

