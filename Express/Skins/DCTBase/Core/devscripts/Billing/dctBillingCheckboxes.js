/**
*
*/
Ext.define('DCT.BillingCheckBoxField', {
    extend: 'DCT.SystemCheckBoxField',

    inputXType: 'dctbillingcheckboxfield',
    xtype: 'dctbillingcheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
    },
    /**
	*
	*/
    enableDisableAction: function (checkbox, checked) {
    },
    /**
	*
	*/
    blankCheck: function (checkbox, checked) {
    },
    /**
	*
	*/
    otherCheckProcess: function (checkbox, checked) {
    }
});


/**
*
*/
Ext.define('DCT.BillingBasicCheckBoxField', {
    extend: 'DCT.BillingCheckBoxField',

    inputXType: 'dctbillingbasiccheckboxfield',

    /**
	*
	*/
    setListeners: function (config) {
        var me = this;
        Ext.apply(config.listeners, {
            change: {
                fn: function (checkbox, checked, oldValue, eOpts) {
                    me.showHideFields(checkbox, checked);
                    me.enableDisableAction(checkbox, checked);
                    me.otherCheckProcess(checkbox, checked);
                },
                scope: me
            }
        });
    }
});


/**
*
*/
Ext.define('DCT.BillingEnableCheckBoxField', {
    extend: 'DCT.BillingBasicCheckBoxField',

    inputXType: 'dctbillingenablecheckboxfield',
    xtype: 'dctbillingenablecheckboxfield',

    /**
	*
	*/
    enableDisableAction: function (checkbox, checked) {
        var me = this;
        DCT.Util.enableDisableActionButton(me.blankCheck(checkbox, checked), me.dctActionButton);
    },
    /**
    *
    */
    blankCheck: function (checkbox, checked) {
        var me = this;
        return (me.dctCheckFunction());
    }
});


/**
*
*/
Ext.define('DCT.BillingHoldCheckBoxField', {
    extend: 'DCT.BillingBasicCheckBoxField',

    inputXType: 'dctbillingholdcheckboxfield',
    xtype: 'dctbillingholdcheckboxfield',

    /**
	*
	*/
    otherCheckProcess: function (radioButton, checked) {
        var me = this;
        field = Ext.getDom(me.dctHiddenField);
        field.value = checked ? 1 : 0;
    }
});


/**
*
*/
Ext.define('DCT.AddStatementCheckBoxField', {
    extend: 'DCT.BillingEnableCheckBoxField',

    inputXType: 'dctaddstatementcheckboxfield',
    xtype: 'dctaddstatementcheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
        var me = this;
        var field = Ext.get(me.dctShowHideField);
        DCT.Util.hideShowGroup(me.dctShowHideField, field.hasCls('hideOffset'));
    },
    /**
	*
	*/
    otherCheckProcess: function (checkbox, checked) {
        var me = this;
        if (!checked) {
            Ext.getDom(me.dctAmtField + 'Hidden').value = "0.00";
            Ext.getDom(me.dctAmtField).value = "";
        }
    },
    /**
	*
	*/
    blankCheck: function (checkbox, checked) {
        var me = this;
        return (me.checkIfAddItemFieldsBlank());
    }
});


/**
*
*/
Ext.define('DCT.HoldBillingCheckBoxField', {
    extend: 'DCT.BillingEnableCheckBoxField',

    inputXType: 'dctholdbillingcheckboxfield',
    xtype: 'dctholdbillingcheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
        var me = this;
        var dateField = Ext.getCmp('holdEndDate');

        if (checked) {
            dateField.allowBlank = true;
            dateField.setValue("");
            dateField.disable();
            if (me.dctHideShowResumeGroup)
                me.showHideResumeOptions(false);
        } else {
        	var holdStartDate = Ext.getCmp("holdStartDate").getValue();
        	var startDate = Ext.Date.parse(holdStartDate, dateField.format)
        	dateField.addDate(holdStartDate, 1);
            dateField.enable();
            dateField.allowBlank = false;
            if (me.dctHideShowResumeGroup)
                me.showHideResumeOptions(true);
        }
    }
});


/**
*
*/
Ext.define('DCT.HoldReasonCheckBoxField', {
    extend: 'DCT.BillingEnableCheckBoxField',

    inputXType: 'dctholdreasoncheckboxfield',
    xtype: 'dctholdreasoncheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
        var holdItemColData, holdItemCmtColData;
        var numOfRows = Ext.get('tblItemListDetail').dom.rows.length - 1;
        DCT.Util.hideShowGroup('holdReasonSelection', checked);
        for (i = 1; i <= numOfRows; i++) {
            DCT.Util.hideShowGroup('holdItemReason' + i, !checked);
            if (checked) {
                DCT.Util.hideShowGroup('itemCommentActionGroup' + i, false);
                DCT.Util.hideShowGroup('itemViewCommentGroup' + i, false);
                Ext.getDom("hiddenComment" + i).value = "";
                Ext.getDom("hiddenCmtTypeCode" + i).value = "";
                Ext.getDom("holdItemReasonField" + i).value = "";
            }
        }

        if (!checked) {
            DCT.Util.hideShowGroup('commentActionGroup', false);
            DCT.Util.hideShowGroup('viewCommentGroup', false);
            Ext.getCmp("holdReasonField").setValue("");
            Ext.getDom("hiddenGlobalComment").value = "";
            Ext.getDom("hiddenGlobalCmtTypeCode").value = "";
        }
    },
    /**
	*
	*/
    blankCheck: function (checkbox, checked) {
        var me = this;
        return (me.checkIfHoldItemActionFieldBlank());
    }

});


/**
*
*/
Ext.define('DCT.MakePaymentCheckBoxField', {
    extend: 'DCT.BillingBasicCheckBoxField',

    inputXType: 'dctmakepaymentcheckboxfield',
    xtype: 'dctmakepaymentcheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
        DCT.Util.showHideManualAllocationFields(checked);
    }
});


/**
*
*/
Ext.define('DCT.AgencyLeaveOpenCheckBoxField', {
    extend: 'DCT.BillingBasicCheckBoxField',

    inputXType: 'dctagencyleaveopencheckboxfield',
    xtype: 'dctagencyleaveopencheckboxfield',

    /**
	*
	*/
    otherCheckProcess: function (checkbox, checked) {
        var me = this;
        if (checked) {
            var otherCheckbox = Ext.getCmp(me.dctOtherField);
            if (otherCheckbox.getValue())
                otherCheckbox.setValue(!otherCheckbox.getValue());
        }
    }
});


/**
*
*/
Ext.define('DCT.AgencyWriteOffCheckBoxField', {
    extend: 'DCT.AgencyLeaveOpenCheckBoxField',

    inputXType: 'dctagencywriteOffcheckboxfield',
    xtype: 'dctagencywriteOffcheckboxfield',

    /**
	*
	*/
    showHideFields: function (checkbox, checked) {
        var me = this;
        var field = Ext.get(me.dctShowHideField);
        DCT.Util.hideShowGroup(me.dctShowHideField, field.hasCls('hideOffset'));
    }
});


/**
*
*/
Ext.define('DCT.SuspenseListCheckBoxField', {
    extend: 'DCT.BillingBasicCheckBoxField',

    inputXType: 'dctsuspenselistcheckboxfield',
    xtype: 'dctsuspenselistcheckboxfield',

    /**
	*
	*/
    otherCheckProcess: function (checkbox, checked) {
        var me = this;
        var otherCheckbox = Ext.getCmp(me.dctOtherField);
        if (Ext.isDefined(otherCheckbox)) {
            if (checked) {
                otherCheckbox.setValue(false);
                otherCheckbox.disable();
            } else {
                otherCheckbox.enable();
            }
        }
    }
});


/**
*
*/
Ext.define('DCT.CreateHoldCheckBoxField', {
    extend: 'DCT.HoldBillingCheckBoxField',

    inputXType: 'dctcreateholdcheckboxfield',
    xtype: 'dctcreateholdcheckboxfield',

    /**
	*
	*/
    blankCheck: function (checkbox, checked) {
        var me = this;
        return (me.checkIfHoldFieldsBlank());
    }
});


/**
*
*/
Ext.define('DCT.EditHoldCheckBoxField', {
    extend: 'DCT.HoldBillingCheckBoxField',

    inputXType: 'dcteditholdcheckboxfield',
    xtype: 'dcteditholdcheckboxfield',

    /**
	*
	*/
    blankCheck: function (checkbox, checked) {
        var me = this;
        return (me.checkIfEditHoldFieldsBlank());
    }
});

