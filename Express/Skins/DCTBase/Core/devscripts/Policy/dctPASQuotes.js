/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	*
	*/
	gotoPageForQuote: function (page, quoteID) {
		var me = this;
		me.removeKeyNames();
		DCT.Util.getSafeElement('_QuoteID').value = quoteID;
		DCT.Util.getSafeElement('_UseClientID').value = '1';
		me.submitPage(page);
	},
	/**
	*
	*/
	newQuote: function (sAction, sDetails) {
		var me = this;
		if (Ext.isEmpty(Ext.getCmp('ProducerName').getValue())) {
			DCT.Util.removeTabs(true);
			Ext.create('DCT.AlertMessage', {
				title: DCT.T('Alert'),
				msg: DCT.T("QuoteWithoutProducer"),
				fn: function () { DCT.Util.removeTabs(false); }
			}).show();
		} else {
			DCT.globalPage = "new";
			DCT.globalSaveAction = "save," + sAction;
			DCT.globalAction = sAction;
			DCT.globalDetails = sDetails;
			me.saveVerify(Ext.Function.createDelayed(me.completeNewQuote, 0, me));
		}
	},
	/**
	*
	*/
	completeNewQuote: function (btn) {
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn == 'no') {
			Ext.getDom('_manuscriptLOB').value = DCT.globalDetails;
			me.submitAction(DCT.globalAction);
		} else
			if (btn == 'yes')
				me.completeSaveProcess();
	},
	/**
	*
	*/
	actOnQuote: function (sAction, iQuoteID, sLOB, iLiteLoad) {
		var me = this;
		var waiting = DCT.ajaxProcess.waitForRequests(me);
		if (waiting)
		{
			return;
		}
		DCT.globalAction = sAction;
		DCT.globalQuoteID = iQuoteID;
		DCT.Util.getSafeElement('_liteLoad').value = iLiteLoad;
		DCT.globalLOB = (sLOB == null) ? "" : sLOB;
		if (sAction == "deleteQuote") {
			DCT.globalAction = "delete";
			msg = DCT.T("AreYouSureDelete");
			me.actionVerify(msg, Ext.Function.createDelayed(me.completeQuoteProcessing, 0, me));
		} else if ((sAction == "delete") || (sAction == "deleteClient")) {
			var msg;
			if ((Ext.getCmp('showDeleted') != null) && (Ext.getCmp('showDeleted').getValue()))
				msg = DCT.T("AreYouSureUndelete");
			else
				msg = DCT.T("AreYouSureDelete");
			me.actionVerify(msg, Ext.Function.createDelayed(me.completeQuoteProcessing, 0, me));
		} else
			me.completeQuoteProcessing('yes');
	},
	/**
	*
	*/
	completeQuoteProcessing: function (btn) {
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn != 'no') {
			var CurrentSearch = Ext.getCmp('lastSearchItem');
			if ((Ext.isDefined(CurrentSearch)) && (DCT.globalAction != 'filter') && DCT.Util.getSafeElement('_gridPage').value != 'open')
				CurrentSearch.setValue('(all)');
			DCT.Util.getSafeElement('_QuoteID').value = DCT.globalQuoteID;
			if (DCT.globalAction.indexOf("load") >= 0) {
				DCT.Util.getSafeElement('_manuscriptLOB').value = DCT.globalLOB;
			}
			if (!Ext.isEmpty(DCT.clientName)) {
				if (DCT.globalAction.indexOf("load") >= 0) {
					DCT.globalPage = "";
					DCT.globalSaveAction = "save," + DCT.globalAction;
					me.saveVerify(Ext.Function.createDelayed(me.completeQuoteSave, 0, me));
				} else
					me.completeQuoteSave('no');
			} else
				me.completeQuoteSave('no');
		}
	},
	/**
	*
	*/
	completeQuoteSave: function (btn) {
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn == 'no') {
			var gridPage = DCT.Util.getSafeElement('_gridPage').value;
			if (gridPage != 'open') {
				DCT.Util.getSafeElement('_targetPage').value = "";
				me.submitAction(DCT.globalAction);
				DCT.Util.getSafeElement('_QuoteID').value = 0;
			} else
				DCT.Grid.applyFilterToDataStore(Ext.getCmp('quoteList').getStore(), true, DCT.Submit.resetPolicyVariables);
		} else if (btn == 'yes') {
			me.completeSaveProcess();
			DCT.Util.getSafeElement('_QuoteID').value = 0;
		}
	},
	/**
	*
	*/
	resetPolicyVariables: function () {
		DCT.Util.getSafeElement('_QuoteID').value = 0;
	},
    /**
*
*/
	downloadSubmissionPolicy: function (sAction, iQuoteID) {
		var me = this;
		DCT.globalQuoteID = iQuoteID;
		DCT.globalAction = sAction;
		DCT.Util.getSafeElement('_QuoteID').value = DCT.globalQuoteID;
		var msg = DCT.T( 'VerifyPolicyDownload');
		me.actionVerify(msg, Ext.Function.createDelayed(me.downloadPolicy, 0, me));
	},
	downloadPolicy: function (btn) {
		if (btn == 'yes') {
			Ext.getDom('_submitAction').value = DCT.globalAction;
			DCT.Submit.applySearchFilter(DCT.Submit.activeSearchMode);
		}
	},

	downloadSubmissionClientPolicy: function (sAction, iQuoteID) {
		var me = this;
		DCT.globalQuoteID = iQuoteID;
		DCT.globalAction = sAction;
		DCT.globalPage = 'clientInfo';
		DCT.Util.getSafeElement('_QuoteID').value = DCT.globalQuoteID;
		var msg = DCT.T('VerifyPolicyDownload');
		me.actionVerify(msg, Ext.Function.createDelayed(me.downloadClientPolicy, 0, me));
	},
	downloadClientPolicy: function (btn) {
		var me = this;
		if (btn == 'yes') {
			Ext.getDom('_submitAction').value = DCT.globalAction;
			me.removeKeyNames();
			me.setListItems();
			Ext.getDom('_targetPage').value = DCT.globalPage;
			me.secureAction(DCT.globalPage);
			me.submitForm();
		}
	},
	/**
	*
	*/
	actionVerify: function (sMessage, sFunction, bDisableForm) {
		var me = this;
		if (typeof (bDisableForm) == "undefined" || bDisableForm == true)
			DCT.Util.removeTabs(true);
		DCT.msgBox = Ext.MessageBox.confirm('Verify', sMessage, sFunction);
		msgToolbar = DCT.msgBox.getDockedItems('toolbar')[0];
		DCT.msgActionBtn = msgToolbar.getComponent('yes');
		Ext.Function.defer(me.setDlgActionBtnFocus, 5);
	},
	/**
	*
	*/
	setDlgActionBtnFocus: function () {
		if (DCT.msgBox.isVisible())
			DCT.msgActionBtn.el.dom.focus();
	},
	/**
	*
	*/
	getSubmissionXML: function (submissionId) {
		var me = this;
		DCT.Util.getSafeElement('_submissionId').value = submissionId;
		DCT.Util.getSafeElement('_targetPage').value = "";
		me.submitAction('submissionDownload');
		Ext.getDom('_submitAction').value = '';
	}
});
