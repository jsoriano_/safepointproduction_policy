/// <reference path="dctPASRenderers.js" />
/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
	/**
	 *
	 */
	selectAgencyRenderer: function(returnPage, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var agencyCaption;

			if (extRecord.get('name') != null && extRecord.get('name') != ''){
			agencyCaption = extRecord.get('name');
		}
		if (extRecord.get('city') != null && extRecord.get('city') != ''){
			agencyCaption += ", " + extRecord.get('city');
		}
		if (extRecord.get('state') != null && extRecord.get('state') != ''){
			agencyCaption += ", " + extRecord.get('state');
		}

		var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Util.selectAgency(' + extRecord.id + ',\'' + agencyCaption + '\');"><div class="passIcon" title="' + DCT.T('Select') + '"></div></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	portfolioAttachmentRowActionColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var deleteActionHTML = '<a href="javascript:;" onclick="DCT.Submit.deletePortfolioAttachment(\'' + extRecord.id + '\');"><img src="' + DCT.imageDir + 'delete.gif" alt="Delete portfolio attachment" title="Delete portfolio attachment"/></a>';
			var downloadActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.downloadPortfolioAttachment(\'' + extRecord.id + '\');"><img src="' + DCT.imageDir + 'icons\/disk_download.png" alt="Download portfolio attachment" title="Download portfolio attachment"/></a>';
		var rowActionsHTML = deleteActionHTML + downloadActionsHTML;
		return rowActionsHTML;
	},
	/**
	 *
	 */
	portfolioAttachmentNameColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var fileName = dataValue;
		return '<a href="javascript:;" id="aFileName" onclick="DCT.Submit.downloadPortfolioAttachment(\'' + extRecord.id + '\')">' + Ext.util.Format.htmlEncode(fileName) + '</a>';
	},
	/**
	 *
	 */
	printJobRowActionColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var name = extRecord.data['name'];
			var removed = extRecord.data['remove'];
			var selected = extRecord.data['selected'];
			var removeHiddenFieldsHTML = '<input id="_removeFormNames' + name + '" type="hidden" name="_removeFormNames" value="' + name + '=' + ((!removed) ? '0' : '1') + '"/>';
			var selectHiddenFieldsHTML = '<input id="_printJobNames' + name + '" type="hidden" name="_printJobNames" value="' + name + '=' + selected + '"/>';

			var previewActionsHTML = DCT.Grid.previewRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			var editFormPropsActionsHTML = DCT.Grid.editFormPropsRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			var removeActionsHTML = DCT.Grid.removeRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			var undoRemoveActionsHTML = DCT.Grid.undoRemoveRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			var downloadActionsHTML = DCT.Grid.downloadRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			var rowActionsHTML = previewActionsHTML + downloadActionsHTML + removeActionsHTML + undoRemoveActionsHTML + editFormPropsActionsHTML;

			return rowActionsHTML + selectHiddenFieldsHTML + removeHiddenFieldsHTML;
	},
	/**
	*
	*/
	printJobRowActionReviewColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		
		var reviewActionsReviewedColumnHTML = DCT.Grid.reviewRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
		return reviewActionsReviewedColumnHTML;
	},
		/**
	 *
	 */
	editFormPropsRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var name = extRecord.data['name'];
		var order = extRecord.data['order'];
		var showEditorIcon = extRecord.data['type'] == 'adhoc' || DCT.Util.getSafeElement('_canReplace').value == "1"
			|| DCT.Util.getSafeElement('_canAdd').value == "1" || DCT.Util.getSafeElement('_canReorder').value == "1";
		var editHTML = '';;

		//Make sure it is an Ad Hoc Form and that they have access - right now all they can do is reorder, so make sure they have access.
		if (showEditorIcon) {
			editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'adHocFormEditor\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');"><img src="' + DCT.imageDir + 'icons\/wrench.png" alt="' + DCT.T('EditFormProperties') + '" title="' + DCT.T('EditFormProperties') + '"/></a>';
		}
		return editHTML;
	},
	/**
	 *
	 */
	printJobRowActionEditColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
			var rowActionsEditColumnHTML = DCT.Grid.editRenderer(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView);
			return rowActionsEditColumnHTML;
	},
	/**
	 *
	 */
	editRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
			var name = extRecord.data['name'];
			var docManuscript = extRecord.data['docManuscript'];
			var pageRef = extRecord.data['pageRef'];
			var topicRef = extRecord.data['topicRef'];
			var hasMerge = extRecord.data['hasMerge'];
			var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
			var removed = extRecord.data['remove'];
			var order = extRecord.data['order'];
			var review = extRecord.data['review'];
			var editStatus = extRecord.data['editStatus'];
			var editHTML = '<div class="collapsed"><a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + topicRef + ':' + pageRef + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editIcon" title="' + DCT.T('EditFormField') + '"></div>' + editStatus + '</a></div>';
			if ((hasMerge) && (readOnly != '1')) {
					if (topicRef != '') {
							if (editStatus == "1") {
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + topicRef + ':' + pageRef + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editIcon" title="' + DCT.T('EditRequiredFormField') + '"></div></a><span class="required">*</span>';
							}else if(editStatus=="2"){
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + topicRef + ':' + pageRef + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editIcon" title="' + DCT.T('EditField') + '"></div></a>';
							} else {
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + topicRef + ':' + pageRef + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editdoneIcon" title="' + DCT.T('FormFieldEdited') + '"></div></a>';
							}
					} else {
							if (editStatus == "1") {
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + name + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editIcon" title="' + DCT.T('EditRequiredFormField') + '"></div></a><span class="required">*</span>';
							} else if (editStatus == "2") {
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + name + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editIcon" title="' + DCT.T('EditField') + '"></div></a>';
							} else {
								editHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJobWithReturnToList(\'editPrintJob\',\'' + name + '\',\'' + docManuscript + '\',\'' + review + '\',\'' + name + '\',\'' + extDataStore.currentStartIndex + '\',\'' + rowIndex + '\');" class="iconLink"><div class="editdoneIcon" title="' + DCT.T('FormFieldEdited') + '"></div></a>';
							}
					}
			}
			return editHTML;
	},
	/**
	 *
	 */
	previewRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var name = extRecord.data['name'];
			var docManuscript = extRecord.data['docManuscript'];
			var printJob = extRecord.data['printjob'];
			var isAdHoc = extRecord.data['type'] == 'adhoc';
		var isOverride = extRecord.data['type'] == 'override';
			var previewHTML = '';

			if (isAdHoc) {
				previewHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJob(\'previewAdHoc\',\'ADHOCFORM:' + name + '|' + docManuscript + '\');"><img src="' + DCT.imageDir + 'icons\/magnifier.png" alt="' + DCT.T('RenderedFormPreview') + '" title="' + DCT.T('RenderedFormPreview') + '"></a>';
			}
			else if (isOverride) {
				previewHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJob(\'previewAdHoc\',\'FORMOVERRIDE:' + name + '|' + docManuscript + '\');"><img src="' + DCT.imageDir + 'icons\/magnifier.png" alt="' + DCT.T('RenderedFormPreview') + '" title="' + DCT.T('RenderedFormPreview') + '"></a>';
			}
			else if(printJob){
				previewHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnPrintJob(\'preview\',\'FORM:' + name + '|' + docManuscript + '\');"><img src="' + DCT.imageDir + 'icons\/magnifier.png" alt="' + DCT.T('RenderedFormPreview') + '" title="' + DCT.T('RenderedFormPreview') + '"></a>';
			}

			return previewHTML;
	},
	/**
	 *
	 */
	undoRemoveRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var name = extRecord.data['name'];
			var removed = extRecord.data['remove'];
			var undoRendererHTML = '<a href="javascript:;" name="UndoRemoveForm' + name + '" id="UndoRemoveForm' + name + '" onclick="DCT.Grid.toggleRemoveForm(\'' + name + '\', \'' + rowIndex + '\', \'' + extDataStore.grid.id + '\');"><img src="' + DCT.imageDir + 'icons\/undelete.png" alt="' + DCT.T('UndoRemove') + '" title="' + DCT.T('UndoRemove') + '"/></a>';
			var onPolicy = extRecord.data['onpolicy'];
			var printDefault = extRecord.data['printDefault'];
			var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
			var printJob = extRecord.data['printjob'];
			var removeable = onPolicy && !((printDefault == 'Mandatory') || readOnly == '1') && printJob;

			if (!removed || !removeable)  //hide
					undoRendererHTML = '<a href="javascript:;"  name="UndoRemoveForm' + name + '" class="collapsed" id="UndoRemoveForm' + name + '" onclick="DCT.Grid.toggleRemoveForm(\'' + name + '\', \'' + rowIndex + '\', \'' + extDataStore.grid.id + '\');"><img src="' + DCT.imageDir + 'icons\/undelete.png" alt="' + DCT.T('UndoRemove') + '" title="' + DCT.T('UndoRemove') + '"/></a>';

			return undoRendererHTML;
	},
	/**
	 *
	 */
	removeRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var name = extRecord.data['name'];
			var removed = extRecord.data['remove'];
			var isAdHoc = extRecord.data['type'] == 'adhoc';
			var printJob = extRecord.data['printjob'];
			var printDefault = extRecord.data['printDefault'];
			var onPolicy = extRecord.data['onpolicy'];
			var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
			var canDelete = DCT.Util.getSafeElement('_canDelete').value == "1";
			var purgeable = isAdHoc && !onPolicy && canDelete;
			var removeHiddenHTML = '<input id="_removeForm' + name + '" type="hidden" name="__removeForm' + name + '" value="' + ((!removed) ? '0' : '1') + '"/>';
			var removeRendererHTML;
			var removeable = onPolicy && !((printDefault == 'Mandatory' && printJob) || readOnly == '1');

		if (!purgeable && (removed || !removeable)) {
			removeRendererHTML = '<a href="javascript:;" name="RemoveForm' + name + '" class="collapsed" id="RemoveForm' + name + '" onclick="DCT.Grid.toggleRemoveForm(\'' + name + '\', \'' + rowIndex + '\', \'' + extDataStore.grid.id + '\');"><img src="' + DCT.imageDir + 'icons\/decline.png" alt="' + DCT.T('RemoveForm') + '" title="' + DCT.T('RemoveForm') + '"/></a>';
		} else if (purgeable) {
			var purgeClass = extRecord.data['staysWithPolicy'] == 1 ? "collapsed" : "";
			removeRendererHTML = '<a href="javascript:;" name="PurgeForm' + name + '" class="' + purgeClass + '" id="PurgeForm' + name + '" onclick="DCT.Grid.purgeForm(\'' + name + '\', \'' + rowIndex + '\', \'' + extDataStore.grid.id + '\');"><img src="' + DCT.imageDir + 'icons\/trashcan.png" alt="' + DCT.T('Delete') + '" title="' + DCT.T('Delete') + '"/></a>';
		} else {
			removeRendererHTML = '<a href="javascript:;" name="RemoveForm' + name + '" id="RemoveForm' + name + '" onclick="DCT.Grid.toggleRemoveForm(\'' + name + '\', \'' + rowIndex + '\', \'' + extDataStore.grid.id + '\');"><img src="' + DCT.imageDir + 'icons\/decline.png" alt="' + DCT.T('RemoveForm') + '" title="' + DCT.T('RemoveForm') + '"/></a>';
		}

		return removeRendererHTML + removeHiddenHTML;
	},
	/**
	*
	*/
	downloadRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var canDownload = DCT.Util.getSafeElement('_canDownload').value == "1";
		if (!canDownload) {
			return "";
		}
		
		var name = encodeURI(extRecord.data['name']);
		var manuscript = extRecord.data['docManuscript'];
		var isAdHoc = extRecord.data['type'] == 'adhoc';
		
		if (isAdHoc) {
			manuscript = 'adhoc';
		}

		var downloadRendererHTML = Ext.String.format('<a href="javascript:;" name="DownloadForm{0}" id="DownloadForm{0}" onclick="DCT.Grid.downloadForm(\'{0}\');"><img src="{1}icons\/disk_download.png" alt="{2}" title="{2}"/></a>',
			Ext.String.escape(name), DCT.imageDir, DCT.T('Download'));

		return downloadRendererHTML;
	},
	/**
	*
	*/
	reviewRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var hasMerge = extRecord.data['hasMerge'];
		var review = extRecord.data['review'];
		var editHTML = '';
		if (review == '1' && hasMerge=='1') {
			editHTML = '<div class="passIcon"></div>';
		}
		return editHTML;
	},
	/**
	 *
	 */
	titleColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var titleHTML = '<div class="normalWhiteSpace">' + Ext.util.Format.htmlEncode(dataValue) + '</div>';
			return titleHTML;
	},
	/**
	 *
	 */
	printSetRowSelection: function(extRecord){
		var printJob = extRecord.data['printjob'];
		var removed = extRecord.data['remove'];
		var selected = extRecord.data['selected'];
		var printDefault = extRecord.data['printDefault'];
		var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
		var isSelected = ((selected == '1' || selected == '2') && !removed);
		var isGrayedOut = (printDefault == "Mandatory" || readOnly == '1');
		var isHidden = (removed);
		return (printJob && isSelected && !(isGrayedOut || isHidden));
	},
	/**
	 *
	 */
	printCheckBoxColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var printJob = extRecord.data['printjob'];
			var removed = extRecord.data['remove'];
			var selected = extRecord.data['selected'];
			var printDefault = extRecord.data['printDefault'];
			var name = extRecord.data['name'];
			var onPolicy = extRecord.data['onpolicy'];
			var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
			var checkBoxColumnHTML = '<div class="x-grid-row-checker" id="_printJob' + name + '" name="__printJob' + name + '">&#160;</div>';
			var cbHiddenHTML = '<input id="_printJobCB' + name + '" type="hidden" name="__printJobCB' + name + '" value="' + selected + '"/>';

			var isSelected = ((selected == '1' || selected == '2') && !removed);
			var isGrayedOut = (printDefault == "Mandatory" || readOnly == '1');
			var isHidden = (removed);

			if (printJob) {
					if (isSelected) {
							extDataStore.grid.getSelectionModel().select(rowIndex, true, false);
							extDataStore.grid.getSelectionModel().setPageSelections();
					}
					if (isGrayedOut || isHidden){
						if (isSelected)
							checkBoxColumnHTML = '<div class="x-grid-row-checker-on-disable" id="_printJob' + name + '" name="__printJob' + name + '">&#160;</div>';
						else{
							if (isHidden)
								checkBoxColumnHTML = '<div class="x-grid-row-checker-hidden" id="_printJob' + name + '" name="__printJob' + name + '">&#160;</div>';
							else
								checkBoxColumnHTML = '<div class="x-grid-row-checker-disable" id="_printJob' + name + '" name="__printJob' + name + '">&#160;</div>';
						}
					}
			}else
				checkBoxColumnHTML = '<div class="x-grid-row-checker-hidden" id="_printJob' + name + '" name="__printJob' + name + '">&#160;</div>';

			return checkBoxColumnHTML + cbHiddenHTML;
	},
	/**
	 *
	 */
	printCheckBoxAdjustedCount: function(){
		var me = this,
				foundRecords = me.queryBy(function(extRecord, recordId){
						var removed = extRecord.data['remove'],
								printDefault = extRecord.data['printDefault'],
								readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData),
								isGrayedOut = (printDefault == "Mandatory" || readOnly == '1'),
								isHidden = (removed);
						
						if (isGrayedOut || isHidden){
							return true;
						}else{
							return false;
						}
					}, me);

			return foundRecords.getCount();
	},
	/**
	 *
	 */
	messageDescriptionColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var messageDescriptionColumnHTML = '<a href="javascript:;" id="messagesListLoadQuoteA" onclick="DCT.Submit.actOnMessage(\'' + extRecord.id + '\',\'readMessage\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return messageDescriptionColumnHTML;
	},
	/**
	 *
	 */
	openRowActionsRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var lob = extRecord.data['LOB'];
		var showDeleted = extRecord.data['deleted'];
		var duplicateQuotePriveledges = Ext.DomQuery.selectNode('state/user/privileges/privilege[@name="DuplicatePolicy"]', extRecord.store.proxy.reader.rawData);
		var deleteQuotePriveledges = Ext.DomQuery.selectNode('state/user/privileges/privilege[@name="DeleteQuotes"]', extRecord.store.proxy.reader.rawData);
		//var status = extRecord.get('status');
		var security = Ext.DomQuery.selectValue('state/@securityLevel', extRecord.store.proxy.reader.rawData);
		var IsReadOnly = DCT.IsReadOnly;
		var rowActionsHTML = '<a href="javascript:;" id="quoteListLoadQuoteA" onclick="DCT.Submit.actOnQuote(\'load\',\'' + extRecord.id + '\',\'' + lob + '\');"><img width="16" alt="' + DCT.T('ViewPolicy') + '" title="' + DCT.T('ViewPolicy') + '" src="' + DCT.imageDir + 'icons\/magnifier.png"/></a>';
		rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.gotoPageForQuote(\'policy\',\'' + extRecord.id + '\');"><img width="16" alt="' + DCT.T('ViewPolicyDetails') + '" title="' + DCT.T('ViewPolicyDetails') + '" src="' + DCT.imageDir + 'icons\/application_view_list.png"/></a>';
		if (!IsReadOnly) {
			if (deleteQuotePriveledges ) {
				if (showDeleted != '1')
					rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.setGridAction(\'open\', \'delete\');DCT.Submit.actOnQuote(\'delete\',\'' + extRecord.id + '\');"><img width="16" alt="' + DCT.T('Delete') + '" title="' + DCT.T('Delete') + '" src="' + DCT.imageDir + 'icons\/delete.png"/></a>';
				else
					rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.setGridAction(\'open\', \'delete\');DCT.Submit.actOnQuote(\'delete\',\'' + extRecord.id + '\');"><img width="16" alt="' + DCT.T('Undelete') + '" title="' + DCT.T('Undelete') + '" src="' + DCT.imageDir + 'icons\/undelete.png"/></a>';
			}
			if (showDeleted != '1' ) {
				if (duplicateQuotePriveledges || (security >= 8))
				{
					rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.setGridAction(\'open\', \'duplicate\');DCT.Submit.actOnQuote(\'duplicate\',\'' + extRecord.id + '\');"><img width="16" alt="' + DCT.T('Duplicate') + '" title="' + DCT.T('Duplicate') + '" src="' + DCT.imageDir + 'icons\/page_white_copy.png"/></a>';
				}
				rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.actOnQuote(\'download\',\'' + extRecord.id + '\');"><img width="16" alt="' + DCT.T('DownloadXML') + '" title="' + DCT.T('DownloadXML') + '" src="' + DCT.imageDir + 'icons\/disk_download.png"/></a>';
				rowActionsHTML = rowActionsHTML + '<a href="javascript:;" onclick="DCT.Submit.downloadSubmissionPolicy(\'submissionDownloadPolicy\',\'' + extRecord.id + '\' );"><img width="16" alt="' + DCT.T('DownloadPolicy') + '" title="' + DCT.T('DownloadPolicy') + '" src="' + DCT.imageDir + 'icons\/download_policy.png"/></a>';
			}
		}
		return rowActionsHTML;
	},
	/**
	 *
	 */
	onPolicyColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var selected = extRecord.data['selected'];
			var onPolicy = extRecord.data['onpolicy'];
			var removed = extRecord.data['remove'];
			var name = extRecord.data['name'];
			var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', extRecord.store.proxy.reader.rawData);
			var on = '<div id="policyForm' + name + '_On" style="display: none;"><div class="passIcon"></div></div>';
			var removeHtml = '<div id="policyForm' + name + '_Remove" style="display: none;">' + DCT.T('Remove') + '</div>';
			var add = '<div id="policyForm' + name + '_Add" style="display: none;">'+ DCT.T('Add') +'</div>';

			if (onPolicy && !removed) //display the tick when its on the policy and not removed
					on = '<div id="policyForm' + name + '_On"><div class="passIcon"></div></div>';

			else if ((selected == '1' || selected == '2') && !onPolicy) //show add if its getting added
					add = '<div id="policyForm' + name + '_Add">'+ DCT.T('Add') +'</div>';

			else if (removed && onPolicy) //show remove if its getting removed and on the policy
					removeHtml = '<div id="policyForm' + name + '_Remove">'+ DCT.T('Remove') +'</div>';

			var onPolicyTotalHTML = on + add + removeHtml;

			return onPolicyTotalHTML;
	},
	/**
	 *
	 */
	nameColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var clientId = extRecord.id;
		var displayCaption;

		if (dataValue=="")
			displayCaption = "(none)";
		else
			displayCaption = dataValue;
		nameColumnHTML = '<a href="javascript:;" id="clientPageLoadA" onclick="DCT.Submit.gotoPageForClient(\'clientInfo\',\'' + clientId + '\');">' + Ext.util.Format.htmlEncode(displayCaption) + '</a>';;
		return nameColumnHTML;
	},
	/**
	 *
	 */
	namePolicyColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var nameColumnHTML = '<a href="javascript:;" id="clientPageLoadA" onclick="DCT.Submit.gotoPageForQuote(\'clientInfo\',\'' + extRecord.id + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return nameColumnHTML;
	},
	/**
	 *
	 */
	clientRowActionsRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var showDeleted = extRecord.data['deleted'],
				rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.setGridAction(\'open\', \'deleteClient\');DCT.Submit.actOnClient(\'deleteClient\',\'' + extRecord.id + '\');"><img ',
				imageHTML;

		if (showDeleted == '1')
				imageHTML = ' src="' + DCT.imageDir + 'icons\/undelete.png" alt="' + DCT.T('UndeleteClient') + '" title="' + DCT.T('UndeleteClient') + '"/></a>';
		else
				imageHTML = ' src="' + DCT.imageDir + 'icons\/delete.png" alt="' + DCT.T('DeleteClient') + '" title="' + DCT.T('DeleteClient') + '"/></a>';
		return rowActionsHTML + imageHTML;
	},
	/**
	 *
	 */
	batchClientColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var lineColumnHTML = '<a href="javascript:;" id="quoteListLoadQuoteA" onclick="DCT.Submit.actOnQuote(\'load\',\'' + extRecord.id + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return lineColumnHTML;
	},
	/**
	 *
	 */
	newNameColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var nameColumnHTML;
		if ((extRecord.data['family'] == "portfolio") || (extRecord.data['family'] == "Portfolio"))
			nameColumnHTML = '<a href="javascript:;" id="productListStartNewQuote" onclick="DCT.Submit.newQuote(\'newPortfolio\',\'' + extRecord.id + '|' + extRecord.data['lob'] + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		else
			nameColumnHTML = '<a href="javascript:;" id="productListStartNewQuote" onclick="DCT.Submit.newQuote(\'newQuote\',\'' + extRecord.id + '|' + extRecord.data['lob'] + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return nameColumnHTML;
	},
	/**
	 *
	 */
	rowActionsRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.editManuScript(\'' + extRecord.id + '\');"><img src="' + DCT.imageDir + 'icons\/application_view_list.png" alt="' + DCT.T('ViewDetailsOfCompany') + '" title="' + DCT.T('ViewDetailsOfCompany') + '"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	jobRowActionColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
			var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.batchDeleteJob(\'' + extRecord.id + '\');"><img src="' + DCT.imageDir + 'delete.gif" alt="' + DCT.T('DeleteJob') + '" title="' + DCT.T('DeleteJob') + '"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	logNameColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var lineColumnHTML = '<a href="javascript:;" id="batchProcessSelectJobA" onclick="DCT.Submit.batchSelectJob(\'' + dataValue + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return lineColumnHTML;
	},
	/**
	 *
	 */
	messageBodyPreview: function(recordData, rowIndex, extRecord, rowValues){
		var me = this;
		var rowActionsHTML, descriptionText;
		descriptionText = extRecord.data['body'].length > 150 ? extRecord.data['body'].substr(0, 147) + '...' : extRecord.data['body'];
		descriptionText = Ext.util.Format.htmlEncode(descriptionText);
		read = extRecord.data['read'];
		if (read=='0')
			rowActionsHTML = '<div class="messageBody unreadMessage">' + descriptionText + '</div>';
		else
			rowActionsHTML = '<div class="messageBody">' + descriptionText + '</div>';
		return {
			rowBody: rowActionsHTML
		};
	},
	/**
	 *
	 */
	messageEditColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var rowActionsHTML = '<a href="javascript:;" id="messagesListLoadQuoteA" onclick="DCT.Submit.actOnMessage(\'' + extRecord.id + '\',\'readMessage\');"><img alt="' + DCT.T('View') + '" title="' + DCT.T('View') + '" src="' + DCT.imageDir + 'icons\/magnifier.png"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	messageDeleteColumnRenderer: function (returnPage, canDeleteUnowned, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var rowActionsHTML = '';
		if (canDeleteUnowned && DCT.IsReadOnly == false)
				rowActionsHTML = '<a href="javascript:;" id="messagesDeleteA" onclick="DCT.Grid.deleteNotification(\'' + extRecord.id + '\',\'' + returnPage + '\');"><img alt="' + DCT.T('Delete') + '" title="' + DCT.T('Delete') + '" src="' + DCT.imageDir + 'icons\/decline.png"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	messagePolicyColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var policyId = extRecord.data['policyID'];
		if (policyId=="")
			rowActionsHTML = "(none)";
		else
			rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.gotoPageForQuote(\'policy\',\'' + policyId + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	messageTypeColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var rowActionsHTML = "";
		var xpathValue = "/root/content/CodeLists/CodeList/ListEntry[@Code='" + dataValue + "']/@Description";
		var listEntry = Ext.DomQuery.select(xpathValue,extRecord.store.proxy.reader.rawData);

		if (listEntry.length > 0)
			rowActionsHTML = listEntry[0].firstChild.nodeValue;
		else
			rowActionsHTML = "(none)";

		return rowActionsHTML;
	},
	/**
	 *
	 */
	deletePASCommentColumnRenderer: function (returnPage, canDelete, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var rowActionsHTML = '';
		var checkDelete = Ext.DomQuery.selectValue('/page/content/comments/@deleteAccess', extRecord.store.proxy.reader.rawData)
		if(checkDelete == '1' && DCT.IsReadOnly==false)
				canDelete = true;
		else
				canDelete = false;

		if (canDelete)
				rowActionsHTML = '<a href="javascript:;" id="commentsDelete" onclick="DCT.Grid.deleteComment(\'' + extRecord.id + '\',\'' + extRecord.get('commentLockingTS')  + '\',\'' + returnPage + '\', DCT.Grid.completeDeletePASComment);"><img alt="' + DCT.T('Delete') + '" title="' + DCT.T('Delete') + '" src="' + DCT.imageDir + '\/icons\/decline.png"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	viewPASCommentColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var rowActionsHTML = '<a href="javascript:;" id="viewPASComment" onclick="DCT.Util.displayPASViewCommentPopUp(\'' + extRecord.id + '\',\'' + extRecord.get('commentTypeCode') + '\');"><img alt="' + DCT.T('View') + '" title="' + DCT.T('View') + '" src="' + DCT.imageDir + '\/icons\/magnifier.png"/></a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	batchExecuteColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var executeText = "";
		switch(dataValue){
			case '-1':
				executeText = DCT.T("ExecutePre");
				break;
			case '-2':
				executeText = DCT.T("ExecutePost");
				break;
			default:
				executeText = DCT.T("Execute");
				break;
		}
		return executeText;
	},
	/**
	 *
	 */
	batchSuccessColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var successCount = 0,
				dataRow = rowIndex + 1,
				xpathValue = 'batchLog/batchLogItem:nth(' + dataRow + ')',
				rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		for (i=0;i<rawRecord.attributes.length;i++){
				if ((rawRecord.attributes[i].nodeName.indexOf('.') > -1) && (rawRecord.attributes[i].nodeValue == 'success'))
					successCount+=1;
		}
		return successCount;
	},
	/**
	 *
	 */
	batchFailureColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var failureCount = 0,
				dataRow = rowIndex + 1,
				xpathValue = 'batchLog/batchLogItem:nth(' + dataRow + ')',
				rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		for (i=0;i<rawRecord.attributes.length;i++){
				if ((rawRecord.attributes[i].nodeName.indexOf('.') > -1) && (rawRecord.attributes[i].nodeValue == 'failure'))
					failureCount+=1;
		}
		return failureCount;
	},
	/**
	 *
	 */
	batchTotalColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var totalCount = 0,
				dataRow = rowIndex + 1,
				xpathValue = 'batchLog/batchLogItem:nth(' + dataRow + ')',
				rawRecord = Ext.dom.Query.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		for (i=0;i<rawRecord.attributes.length;i++){
				if ((rawRecord.attributes[i].nodeName.indexOf('.') > -1))
					totalCount+=1;
		}
		return totalCount;
	},
	/**
	 *
	 */
	submissionPolicyColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		var quoteId = extRecord.data['QuoteID'];
		var lob = extRecord.data['LOB'];

		if (quoteId == "")
			rowActionsHTML = '';
		else
			rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.actOnQuote(\'load\', \'' + quoteId + '\',\'' + lob + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return rowActionsHTML;
	},
	/**
	 *
	 */
	submissionActionsRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {	   
		var submissionId = extRecord.data['SubmissionId'];
		var rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.getSubmissionXML(\'' + submissionId + '\');"><img width="16" alt="' + DCT.T('Download') + '" title="' + DCT.T('Download') + '" src="' + DCT.imageDir + 'icons\/disk_download.png"/></a>';
		return rowActionsHTML;
	},
	/**
		*
		*/
	submissionLogRowRenderer: function (extRecord, rowIndex, rowParms, extDataStore) {
		var rowActionsHTML, replaceString, descriptionText, logs, status, subject, details;

		logs = Ext.DomQuery.select('SubmissionLogs/SubmissionLog', rowParms.store.proxy.reader.rawData)
		rowActionsHTML = '';

		if (logs.length > 0) {

			if (extRecord.Status !== 'CMP' && extRecord.Status !== 'ERR') {
				var imageHtml = Ext.String.format('<img src="{0}"/>', DCT.skinsDir + '/images/loadingInd.gif');
				rowActionsHTML += '<h3>Log Entries ' + imageHtml + '</h3>';
			} else {
				rowActionsHTML += '<h3>Log Entries</h3>';
			}

			rowActionsHTML += '<table style="width:100%" border="0" class="x_ZebraStripe">';
			rowActionsHTML += '<th align="left" width="50" class="x_columnB">Status</th>';
			rowActionsHTML += '<th align="left" width="75" class="x_columnB">Subject</th>';
			rowActionsHTML += '<th align="left" class="x_columnB">Details</th>';

			for (i = 0; i < logs.length; i++) {
				if (Ext.DomQuery.selectValue('/SubmissionId', logs[i]) == extRecord.SubmissionId) {
					rowActionsHTML += '<tr>';
					status = Ext.DomQuery.selectValue('/Status', logs[i]);
					subject = Ext.DomQuery.selectValue('/Subject', logs[i]);
					details = Ext.DomQuery.selectValue('/Details', logs[i]);
					rowActionsHTML += '<td>' + status + '</td>';
					rowActionsHTML += '<td>' + subject + '</td>';
					rowActionsHTML += '<td>' + details + '</td>';
					rowActionsHTML += '</tr>';
				}
			}
			rowActionsHTML += "</table>"
		}

		rowActionsHTML = '<div class="submissionLog">' + rowActionsHTML + '</div>';
		return {
			rowBody: '<div id="submissionLog' + rowIndex + '">' + rowActionsHTML + '</div>'
		};
	},
	/**
	*
	*/
	highlightRowRenderer: function (extRecord, rowIndex, rowParams, extDataStore) {
		var me = this;
		var rowClass = '';
		if (rowIndex == me.grid.dctClickedRowIndex) {
			return me.grid.getView().rowOverCls;
		}
		return rowClass;
	}
});
