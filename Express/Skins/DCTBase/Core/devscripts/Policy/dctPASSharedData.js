/**
* @class DCT.Window
*/
 Ext.define('DCT.SharedDataDialog', {
	extend: 'Ext.Window',
	dctControl: true,
	inputXType: 'dctshareddatadialog',
	title: DCT.T("AcceptChanges"),
	plain:true,
	modal:true,
	layout:'fit',
	width:350,
	height:250,
	shadow:true,
	minWidth:175,
	minHeight:175,
	stateful: false,
	xtype: 'dctshareddatadialog',

	/**
	 *
	 */
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	 *
	 */
	setItems: function(config){
		var me = this;
		var sharedData = Ext.getDom(config.dctDivFieldId);
		config.items = Ext.create('Ext.form.FormPanel', {
        defaultType: 'displayfield',
        bodyBorder: false,
        id : 'sharedFormId',
        formId : 'sharedForm',
        items: [{
					style: 'margin-top:5px;',
					hideLabel : true,
					anchor:'100%',
					html: (sharedData) ? sharedData.innerHTML : ''
				}],
        buttons: [{
					text: 'Yes',
					handler: function(){
						me.acceptSharedDataChanges();
						me.closeDialog();
						},
					scope: me
				},{
					text: 'Cancel',
					id: 'closeButton',
					handler: function(){me.closeDialog();},
					scope: me
				}]
    });
	},
	/**
	 *
	 */
	setListeners: function(config){
    var me = this;
    config.listeners = {
    	show : {
				fn: function(control) {
					Ext.getCmp('closeButton').focus(false, 5);
				},
				scope:me
			},
    	close : {
				fn: function() {
					var me = this;
					me.closeDialog();
				},
				scope:me
			},
    	hide : {
				fn: function() {
					var me = this;
					me.closeDialog();
				},
				scope:me
			}
		};
	},
	/**
	 *
	 */
  closeDialog: function(){
    var me = this;
  	DCT.Util.removeTabs(false);
    me.destroy();
  },
	/**
	 *
	 */
	acceptSharedDataChanges: function(){
		var me = this;
		var sharedDataValue = Ext.DomQuery.selectNode('div[class=sharedDataValue]',me.body.dom).innerHTML;
		var sharedDataElement = Ext.getDom(me.dctElementName);
		if (sharedDataElement.type == 'checkbox')
			sharedDataElement.checked = (!Ext.isEmpty(sharedDataValue) && sharedDataValue == '1');
		else
			sharedDataElement.value = sharedDataValue;
		DCT.FieldsChanged.set();
		Ext.get(me.dctElement).setStyle("visibility", 'hidden');
	}
});
DCT.Util.reg('dctshareddatadialog', 'sharedDataDialog', 'DCT.SharedDataDialog');
/**
* @class DCT.Window
*/
Ext.define('DCT.MessageDialog', {
	extend: 'Ext.Window',
	dctControl: true,
	inputXType: 'dctmessagedialog',
	width:450,
	height:250,
	minWidth:175,
	minHeight:175,
  maximizable: true,
	modal:true,
	layout:'fit',
  plain: true,
	shadow:true,
	stateful: false,
  iconImage: 'warning.gif',
  xtype: 'dctmessagedialog',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setItems: function(config){
		var me = this;
		var icon = Ext.DomHelper.createDom({tag:'img', name: 'imgPopupHelpIcon', id: 'imgPopupHelpIcon', src: DCT.imageDir + me.iconImage}).outerHTML;
		config.items = Ext.create('Ext.form.FormPanel', {
        defaultType: 'displayfield',
        bodyBorder: false,
        id : 'messageFormId',
        formId : 'messageForm',
        items: [{
					style: 'margin-top:5px;',
					hideLabel : true,
					anchor:'100%',
					value: 'Information in policy differs from the portfolio\'s shared data.  What do I do?'
				},{
					hideLabel : true,
					anchor: '100%',
					html: "- Hover mouse over " + icon + " to view shared data value for this field."
				},{
					hideLabel : true,
					anchor: '100%',
					html: "- Click " + icon + " to update policy information with the shared data value."
        }],
        buttons: [{
					text: DCT.T('Close'),
					id: 'closeButton',
					handler: function(){me.closeDialog();},
					scope: me
				}]
    });
	},
	/**
	*
	*/
	setListeners: function(config){
    var me = this;
    config.listeners = {
    	show : {
				fn: function(control) {
					Ext.getCmp('closeButton').focus(false, 5);
				},
				scope:me
			},
    	close : {
				fn: function() {
					me.closeDialog();
				},
				scope:me
			},
    	hide : {
				fn: function() {
					me.closeDialog();
				},
				scope:me
			}
		};
	},
  closeDialog: function(){
    var me = this;
  	DCT.Util.removeTabs(false);
    me.destroy();
  }
});
DCT.Util.reg('dctmessagedialog', 'messageDialog', 'DCT.MessageDialog');
/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	*
	*/
	showPopupMessage: function(title){
		var me = this;
		me.removeTabs(true);
		var messagePopup = Ext.getCmp('messageDialog');
		if (messagePopup){
			return;
		}
		var messageDialog = Ext.create('DCT.MessageDialog', {
			title: title,
			id: 'messageDialog'
			});
		messageDialog.show();
	},
	/**
	*
	*/
	showShareDataChanges: function(element, elementName, div){
		var me = this;
		var sharedDiv = Ext.getDom(div);
		if (Ext.isEmpty(sharedDiv))
			return;

		me.removeTabs(true);
		var sharedDataPopup = Ext.getCmp('sharedDataDialog');
		if (sharedDataPopup){
			return;
		}
		var sharedDataDialog = Ext.create('DCT.SharedDataDialog', {
			id: 'sharedDataDialog',
			dctElement: element,
			dctElementName: elementName,
			dctDivFieldId: div
			});
		sharedDataDialog.show();
	},
	/**
	*
	*/
	previewSharedDataChange: function(element, divID){
		var sharedDataDiv = Ext.getDom(divID);
		if (sharedDataDiv == null)
			return;
		var sharedDataDivs = Ext.DomQuery.selectNode('div[class=sharedDataValue]', sharedDataDiv.document);
		var sharedDataDiv = null;
		if (Ext.isDefined(sharedDataDivs))
			sharedDataDiv = sharedDataDivs;
		var value = '-- no value --';
		if (!Ext.isEmpty(sharedDataDiv) && !Ext.isEmpty(sharedDataDiv.innerHTML))
			value = sharedDataDiv.innerHTML;

		Ext.create('Ext.ToolTip', {
			target: element,
			html: value,
			title: DCT.T('SharedDataValue'),
			trackMouse: true});
	}
});

