/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	 *
	 */
	displayPASViewCommentPopUp: function(extRecordId, commentTypeCode){
		var me = this;
		var displayComments = Ext.getCmp('cmtsDisplayPopup');
		if (displayComments){
				return;
		}
	  	me.removeTabs(true);
		var commentsDisplayPanel = Ext.create('DCT.DisplayComment', {
			dctExtRecordId: extRecordId,
			dctCommentTypeCode: commentTypeCode,
			dctDataStore : Ext.getCmp('policyComments').getStore()
			});
		var commentsWindow = Ext.create('DCT.BaseWindow', {
	    title: DCT.T('Note'),
	    width: 500,
	    height:450,
	    id: 'cmtsDisplayPopup',
			items: commentsDisplayPanel,
			dctFocusFieldId: '_cmtCancelButton'
			});
		commentsWindow.show();
	},
	/**
	 *
	 */
	displayPASAddCommentsPopUp: function(targetPage, dataStore, itemId, itemTypeCode, viewAnchorField, internalOnlyCheckbox){
		var addComments = Ext.getCmp('commentsAddPopup');
		if (addComments){
				return;
		}
		var commentsAddPanel = Ext.create('DCT.PolicyAddComment', {
			dctTargetPage: targetPage,
			dctDataStore: dataStore,
			dctItemId: itemId,
			dctItemTypeCode: itemTypeCode,
			dctViewAnchorField: viewAnchorField,
			dctInternalOnlyCheckbox: internalOnlyCheckbox
			});
		var commentsWindow = Ext.create('DCT.BaseWindow', {
			title: DCT.T('Note'),
			width: 500,
			height:350,
			id: 'commentsAddPopup',
			items: commentsAddPanel,
			dctFocusFieldId: '_commentEntry'
			});
		commentsWindow.show();
	}

});

/**
* @class DCT.AddComment
*/
Ext.define('DCT.PolicyAddComment', {
	extend: 'DCT.AddComment',
	inputXType: 'dctpolicyaddcomment',
	xtype: 'dctpolicyaddcomment',
	/**
	 *
	 */
	setPanelItems: function(config){
		var me = this;
		config.items = [{
			hideLabel : true,
			xtype: 'textarea',
			name: '_commentEntry',
			id: '_commentEntry',
			maxLength: 1000,
			anchor: '100% -45',
			stripCharsRe: /(^\s+)/g,
			listeners: {
				afterrender:{
					fn: function(control) {
						var me = this;
						me.el.on('contextmenu', Ext.emptyFn, null, {preventDefault: true});
						},
					scope:me
				},
				keyup:{
					fn: function(control) {
						var me = this;
						me.setAddButtonState(control.getValue());
						},
					scope:me
				}
			}
		},{
			style: 'margin-top:5px;',
			hideLabel : true,
			boxLabel: DCT.T("InternalOnly"),
			xtype: 'checkbox',
			name: '_internalOnly',
			id: '_internalOnly',
			anchor:'100%',
			hidden: (Ext.isDefined(config.dctInternalOnlyCheckbox)) ? config.dctInternalOnlyCheckbox : true
		}];
	},
	/**
	 *
	 */
  saveAdd: function(){
    var me = this;
    DCT.Util.getSafeElement('_commentStatus').value = 'A';
    DCT.Util.getSafeElement('_commentCode').value = 'PASC';
    var entry = Ext.getCmp('_commentEntry');
    var isInternal = Ext.getCmp('_internalOnly').getValue();
    if (!Ext.isEmpty(me.dctTargetPage)){
      Ext.getDom('_targetPage').value = me.dctTargetPage;
      Ext.getDom('_submitAction').value = "addPolicyComment";
      DCT.Util.getSafeElement('_commentTypeCode').value = 'CP';
      DCT.Util.getSafeElement('_commentDescription').value = entry.getValue();
      if (isInternal)
         DCT.Util.getSafeElement('_commentTypeCode').value = 'CI';
      DCT.Util.getSafeElement('_itemId').value = me.dctItemId;
      DCT.Util.getSafeElement('_itemTypeCode').value = me.dctItemTypeCode;
      me.dctDataStore.loadPage(1, {
		    callback: function(records, operation, success) {
		    	Ext.getDom('_submitAction').value = "";
		  	}
		  });
      me.ownerCt.closeWindow();
     }
   }
});

/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid,{
	/**
	 *
	 */
	commentTypeColumnRenderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView){
		var altText = DCT.T('CommentIsInternal');
		var typeDisplay = "";
		if (dataValue == 'CI' )
			typeDisplay = '<img alt="' + altText + '" title="' + altText + '" src="' + DCT.imageDir + 'icons\/page_key.png"/>';
		return typeDisplay;
	},
	/**
	 *
	 */
	completeDeletePASComment: function(btn, commentId, commentLockingTS, returnPage){
		if (btn != 'no')	{
			DCT.Util.getSafeElement('_gridAction').value = 'deletePolicyComment';
			DCT.Util.getSafeElement('_returnPage').value = returnPage;
			DCT.Util.getSafeElement('_submitAction').value = 'deletePolicyComment';
			DCT.Util.getSafeElement('_commentId').value = commentId;
			DCT.Util.getSafeElement('_commentLockingTS').value = commentLockingTS;
			DCT.Grid.applyFilterToDataStore(Ext.getCmp('policyComments').getStore(), true, DCT.Grid.resetPASCommentsVariables);
		}
	},
	/**
	 *
	 */
	resetPASCommentsVariables: function(){
		DCT.Util.getSafeElement('_commentId').value = 0; //reset
		DCT.Util.getSafeElement('_submitAction').value = '';
	},
	/**
	*
	*/
	viewPASCommentDescriptionColumnRender: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		return Ext.util.Format.htmlEncode(dataValue);
	}
});
