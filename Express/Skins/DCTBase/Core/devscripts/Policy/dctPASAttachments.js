/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit,{
	/**
	 *
	 */
	processAttachment: function(ID, returnPage, action){
		var me = this;
		DCT.Util.getSafeElement('_attachmentID').value = ID;
		if (Ext.isEmpty(returnPage))
			returnPage = 'policy';

		var prevSubmitAction = Ext.getDom('_submitAction').value;

		me.submitPageAction(returnPage, action);

		//Set the submit action back to its previous value which
		//ensures the download is not triggered on the next action.
		Ext.getDom('_submitAction').value = prevSubmitAction;
	},
	/**
	 *
	 */
	ajaxDeleteAttachment: function(ID){
		DCT.Submit.actionVerify(DCT.T('AreYouSureDeleteAttachment'), function (btn) { DCT.Submit.completeDeleteAttachment(btn, ID);}, false);
	},
	/**
	*
	*/
	completeDeleteAttachment: function (btn, ID) {
		if (btn != 'no'){ 
			Ext.Ajax.request({
				url: DCT.Util.getPostUrl(),
				method: 'POST',
				params: { 
					_attachmentID: ID,
					_QuoteID: DCT.Util.getSafeElement('_QuoteID').value,
					_ClientID: DCT.Util.getSafeElement('_ClientID').value,
					_submitAction: 'deleteAttachment',
					_targetPage: 'noContent'
				},
				success: DCT.hubEvents.fireEvent('attachmentdeleted')
			});
		}
	},
	/**
	 *
	 */
	ajaxUpdateAttachmentCaption: function(ID, moniker, caption, callback, scope){
		Ext.Ajax.request({
			url: DCT.Util.getPostUrl(),
			method: 'POST',
			params: { 
				_attachmentID: ID,
				_moniker: moniker,
				_caption: caption,
				_submitAction:'updateAttachmentCaption'
			},
			callback: callback,
			scope: scope
		});
	},
	/**
	 *
	 */
	addAttachment: function(screenName){
		var me = this;
		if (Ext.isEmpty(Ext.getCmp('caption').getValue()) || Ext.isEmpty(Ext.getCmp('uploadFile').getValue())) {
			Ext.Msg.alert(DCT.T('Alert'), DCT.T('EnterFilenameCaptionUpload'), function(){Ext.getCmp('caption').focus();});
		}else
			me.submitPageAction(screenName,'upload');
	}
});