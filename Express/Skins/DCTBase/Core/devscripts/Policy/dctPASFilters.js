/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	 *
	 */
	delFilter: function(itemName){
		var filter = Ext.getDom('__keyname' + itemName);
		filter.value = '(all)';
	},
	/**
	 *
	 */
	resetNoQuery: function(page){
		var me = this;
		Ext.getDom('_startIndex').value=0;
		var CurrentSearch = Ext.getCmp('lastSearchItem');
		if(Ext.isDefined(CurrentSearch))
			CurrentSearch.setValue('(all)');
		Ext.getDom('_targetPage').value = page;
		me.submitAction('query');
	},
	/**
	 *
	 */
	addFilterAndQuery: function () {
		var me = this;
		Ext.getDom('_startIndex').value = 0;
		Ext.getDom('_targetPage').value = 'batchProcess';
		me.submitAction('query');
	}
});