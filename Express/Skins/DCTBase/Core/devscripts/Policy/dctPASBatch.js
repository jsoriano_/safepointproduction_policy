/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit,{
	/**
	 *
	 */
	batchSubmit: function(pageType, jobName, startIndex, displayCount, jobItemID){
		var me = this;
		DCT.Util.getSafeElement('_pageType').value = pageType;
		DCT.Util.getSafeElement('_jobName').value = jobName;
		DCT.Util.getSafeElement('_startIndex').value = startIndex;
		DCT.Util.getSafeElement('_displayCount').value = displayCount;
		DCT.Util.getSafeElement('_jobItemID').value = jobItemID;
		me.submitPage("batchProcess");
	},
	/**
	 *
	 */
	batchSelectTab: function(pageType){
    var me = this;
    me.batchSubmit(pageType, "", 0, 10, -1);
	},
	/**
	 *
	 */
	batchSelectJob: function(jobName){
		var me = this;
		me.batchSubmit(Ext.getDom('_pageType').value, jobName, 0, 10, -1);
	},
	/**
	 *
	 */
    batchDeleteJob: function (jobItemID) {
        Ext.getDom('_action').value = "deleteJobByLogID";
        Ext.MessageBox.show({
            title: DCT.T("BatchReports"),
            msg: DCT.T("BatchReportDelete"),
            buttons: Ext.MessageBox.YESNO,
            icon: Ext.MessageBox.QUESTION,
            fn: function (btn) {
                if (btn == "yes") {
                    DCT.Submit.batchSubmit(Ext.getDom('_pageType').value, "", 0, 10, jobItemID);
                }
            },
            value: jobItemID
        });
	}
});
