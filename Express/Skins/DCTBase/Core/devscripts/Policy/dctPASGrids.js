/**
*
*/
Ext.define('DCT.AttachmentEditing', {
		alias: 'plugin.dctattachementediting',
		extend: 'DCT.CellEditing',
		
	pluginId: 'dctattachementediting',
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
				edit: {
					fn: me.processAfterEdit,
					scope: me
				}
			};
	},
	/**
	*
	*/
	processAfterEdit: function (editor, editEvent, eOpts) {
		var me = this;
		DCT.Submit.ajaxUpdateAttachmentCaption(editEvent.record.data.attachmentID, editEvent.record.data.moniker, editEvent.value, editor.grid.reloadData, editor.grid);
	}

});
/**
*
*/
Ext.define('DCT.AttachmentGridPanel', {
	extend: 'DCT.EditorGridPanel',

	inputXType: 'dctattachmentgridpanel',
	xtype: 'dctattachmentgridpanel',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.callParent([config]);
		DCT.hubEvents.addListener('attachmentdeleted', me.reloadData, me);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: me.addDropZone,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setPlugins: function (config) {
		config.plugins = [{
			ptype: 'dctattachementediting'
		}];		
	},
	/**
	*
	*/

	setBottomToolbar: function (config) {
	},
	/**
	*
	*/
	
	setTopToolbar: function (config) {
	},
	/**
	*
	*/
	
	setDataStore: function (config) {
		var me = this;
		config.dctStoreConfig.fields = [
			{ name: 'id', mapping: config.dctStoreConfig.idProperty},
			{ name: 'attachmentID', type: 'string', mapping: '@attachmentID' },
			{ name: 'caption', type: 'string', mapping: '@caption' },
			{ name: 'filename', type: 'string', mapping: '@filename' },
			{ name: 'attachDate', type: 'date', dateFormat:'c', mapping: '@attachDate' },
			{ name: 'moniker', type: 'string', mapping: '@moniker' }
		];
		config.store = Ext.create('DCT.EditPagingStore', config.dctStoreConfig);
		config.store.on('load', me.afterReloadData);
	},
	/**
	*
	*/
	setColumnModel: function (config) {
		var me = this;
		config.dctColumnModel = [
			{ text: DCT.T("Caption"), dataIndex: 'caption', width: 40, sortable: true, editable: true, editor: Ext.create('Ext.form.TextField', {}) },
			{ text: DCT.T("Date"), dataIndex: 'attachDate', width: 15, sortable: true, renderer: Ext.util.Format.dateRenderer(Ext.getDom('_hiddenDisplayDtMask').value) },
			{ text: DCT.T("Filename"), dataIndex: 'filename', width: 40, sortable: true },
			{ text: '', dataIndex: 'attachmentID', width: 5, sortable: false, renderer: me.deleteColumnRenderer },
			{ text: '', dataIndex: 'attachmentID', width: 5, sortable: false, renderer: me.downloadColumnRenderer }
		];
		config.columns = {
			items: config.dctColumnModel,
			sortAscText: DCT.T('SortAscending'), 
			sortDescText: DCT.T('SortDescending'), 
			columnsText: DCT.T('Columns')
		};  	
		
	},
	/**
	*
	*/
	createFileUploadArea: function (grid) {
		var dialog = grid.el.wrap().createChild({
			id: 'attachmentsDialogSectionPanel',
			tag: 'div',
			html: '<div id="uploadFileCmp"></div>'
		});
		grid.fileUpload = Ext.create('DCT.SystemFileField', { renderTo: Ext.get('uploadFileCmp'), buttonText: DCT.T('AddAnAttachment'), buttonOnly: true });
	},
	/**
	 *
	 */
	getFileData: function (grid, file, options) {
		if (file) {
			var formData = new FormData();
			formData.append('_QuoteID', DCT.Util.getSafeElement('_QuoteID').value);
			formData.append('_ClientID', DCT.Util.getSafeElement('_ClientID').value);
			formData.append('_caption', file.name);
			formData.append('_submitAction', 'ajaxAttach');
			formData.append(file.name, file);
			options.params = formData;
		}
		else {
			DCT.Util.getSafeElement('_submitAction').value = 'ajaxAttach';
			options.params = {
				_caption: grid.fileUpload.fileInput.dom.value.split(/(\\|\/)/g).pop()
			};
			options.form = document.forms[0];
		}
	},
	/**
	 *
	 */
	deleteColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		var rowActionsHTML = '';
		rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.ajaxDeleteAttachment(\'' + dataValue + '\');"><img alt="' + DCT.T('Delete') + '" title="' + DCT.T('Delete') + '" src="' + DCT.imageDir + 'icons\/delete.png"/></a>';
		return rowActionsHTML;
	},
	/**
	*
	*/
	downloadColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		var rowActionsHTML = '';
		rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.processAttachment(\'' + dataValue + '\', \'\', \'downloadAttachment\');"><img src="' + DCT.imageDir + 'icons\/disk_download.png" alt="' + DCT.T('DownloadAttachment') + '" title="' + DCT.T('DownloadAttachment') + '"/></a>';
		return rowActionsHTML;
	}
});

DCT.Util.reg('dctattachmentgridpanel', 'attachmentGridPanel', 'DCT.AttachmentGridPanel');

/**
*
*/
Ext.define('DCT.SubmissionGridPanel', {
	extend: 'DCT.PagingGridPanel',

	inputXType: 'dctsubmissiongridpanel',
	xtype: 'dctsubmissiongridpanel',
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: me.addDropZone,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setDataStore: function (config) {
		var me = this;
		me.superclass.setDataStore.call(me, config);
		config.store.on('refresh', function () { me.pollingInitialization(config.store); });
	},
	/**
	*
	*/
	reloadData: function () {
		var me = this;
		me.store.oldItems = me.store.data.items;
		DCT.Grid.applyFilterToDataStore(me.store, null, function () { me.pollingInitialization(me.store); });
	},
	/**
	*
	*/
	pollingInitialization: function (store) {
		var me = this;

		Ext.each(store.grid.store.data.items, function (item, index, records) {
			if (item.data.Status != 'CMP' && item.data.Status != 'ERR') {
				Ext.defer(function () { me.checkStatus(item); }, 5000, store);
			}
		});
	},
	/**
	*
	*/
	checkStatus: function (item) {
		var me = this;

		Ext.Ajax.request({
			url: DCT.Util.getPostUrl(),
			method: 'POST',
			scope: item,
			params: {
				_submissionId: item.data.id,
				_submissionMode: 'get',
				_submitAction: 'submission'
			},
			success: function (response, opts) {
				var record = this;
				if (!record.store) {
					return;
				}

				var submissionElement = DCT.Util.loadXMLString(response.responseText);

				var submissionStatus = Ext.DomQuery.selectValue("Status", submissionElement);

				if (submissionStatus != 'CMP' && submissionStatus != 'ERR') {
					Ext.defer(function () { me.checkStatus(record); }, 5000, this.store);

					var submissionNode = Ext.DomQuery.selectNode("page/content/Submissions/Submission:has(SubmissionId:nodeValue(" + record.id + "))", record.store.proxy.reader.rawData);

					if (submissionNode) {
						Ext.DomQuery.selectNode("page/content/Submissions/Submission:has(SubmissionId:nodeValue(" + record.id + "))", record.store.proxy.reader.rawData).innerHTML = submissionElement.documentElement.innerHTML;
					}

					record.store.grid.getView().refresh(record);
				}
				else {
					record.beginEdit();
					record.set('Status', Ext.DomQuery.selectValue("Status", submissionElement));
					record.set('InsuredName', Ext.DomQuery.selectValue("InsuredName", submissionElement));
					record.set('PolicyNumber', Ext.DomQuery.selectValue("PolicyNumber", submissionElement));
					record.set('QuoteID', Ext.DomQuery.selectValue("QuoteID", submissionElement));
					record.set('EffectiveDate', Ext.DomQuery.selectValue("EffectiveDate", submissionElement));
					record.endEdit();

					record.commit();

					var submissionNode = Ext.DomQuery.selectNode("page/content/Submissions/Submission:has(SubmissionId:nodeValue(" + record.id + "))", record.store.proxy.reader.rawData);
					if (submissionNode) {
						Ext.DomQuery.selectNode("page/content/Submissions/Submission:has(SubmissionId:nodeValue(" + record.id + "))", record.store.proxy.reader.rawData).innerHTML = submissionElement.documentElement.innerHTML;
					}

					record.store.grid.getView().refresh(record);
				}

			},
			failure: function (response, opts) {

			}
		});
	},
	/**
	*
	*/
	downloadColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		var rowActionsHTML = '';
		rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.processSubmission(\'' + dataValue + '\', \'\', \'downloadSubmission\');"><img src="' + DCT.imageDir + 'icons\/disk_download.png" alt="' + DCT.T('DownloadSubmission') + '" title="' + DCT.T('DownloadSubmission') + '"/></a>';
		return rowActionsHTML;
	},
	/**
	*
	*/
	createFileUploadArea: function (grid) {
		var dialog = grid.el.wrap().createChild({
			id: 'submissionsDialogSectionPanel',
			tag: 'div',
			html: '<div id="uploadFileCmp"></div>'
		});
		grid.fileUpload = Ext.create('DCT.SystemFileField', { renderTo: Ext.get('uploadFileCmp'), buttonText: DCT.T('UploadSubmission'), buttonOnly: true });
	},
	/**
	 *
	 */
	getFileData: function (grid, file, options) {
		if (file) {
			var formData = new FormData();
			formData.append('_QuoteID', DCT.Util.getSafeElement('_QuoteID').value);
			formData.append('_submitAction', 'submissionUpload');
			formData.append(file.name, file);
			options.params = formData;
		}	else {
			DCT.Util.getSafeElement('_submitAction').value = 'submissionUpload';
			options.params = {
				_caption: grid.fileUpload.fileInput.dom.value.split(/(\\|\/)/g).pop()
			};
			options.form = document.forms[0];
		}
	}
});

DCT.Util.reg('dctsubmissiongridpanel', 'submissionGridPanel', 'DCT.SubmissionGridPanel');

/**
*
*/

Ext.define('DCT.SubmissionDownloadGridPanel', {
	extend: 'DCT.PagingGridPanel',

	inputXType: 'dctsubmissiondownloadgridpanel',
	xtype: 'dctsubmissiondownloadgridpanel',
	
	downloadColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		var rowActionsHTML = '';
		rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.processSubmission(\'' + dataValue + '\', \'\', \'downloadSubmission\');"><img src="' + DCT.imageDir + 'icons\/disk_download.png" alt="' + DCT.T('DownloadSubmission') + '" title="' + DCT.T('DownloadSubmission') + '"/></a>';
		return rowActionsHTML;
	}
});
DCT.Util.reg('dctsubmissiondownloadgridpanel', 'submissionDownloadGridPanel', 'DCT.SubmissionDownloadGridPanel');

/**
*
*/

Ext.define('DCT.PrintJobGridPanel', {
	extend: 'DCT.PagingGridPanel',

	inputXType: 'dctprintjobgridpanel',
	xtype: 'dctprintjobgridpanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		me.callParent([config]);
		Ext.apply(config.listeners, {
			columnhide: {
				fn: function ( ct, column, eOpts) {
					var me = this;
					if (column.dataIndex == "selected") {
						var checkColumn = ct.getHeaderAtIndex(0);
						checkColumn.hide();
					}
				},
				scope: me
			},
			columnshow: {
				fn: function ( ct, column, eOpts) {
					var me = this;
					if (column.dataIndex == "selected") {
						var checkColumn = ct.getHeaderAtIndex(0);
						checkColumn.show();
					}
				},
				scope: me
			}
		});
	}  
});
DCT.Util.reg('dctprintjobgridpanel', 'printJobGridPanel', 'DCT.PrintJobGridPanel');

/**
*
*/
Ext.define('DCT.PolicySearchPagingStore', {
	extend: 'DCT.PagingStore',

  inputXType: 'dctpolicysearchpagingstore',
  xtype: 'dctpolicysearchpagingstore',
   

	setListeners: function (config) {
		var me = this;
	  DCT.PolicySearchPagingStore.superclass.setListeners.call(me, config);
    Ext.apply(config.listeners, {
        load: {
            fn: function (store, records, options) {
            	var me = this;
              me.processLoad(store, records, options);
            },
            scope: me
        }
    });
	},
	processLoad: function (store, records, options) {
		var me = this;
    if (Ext.DomQuery.selectNode("/session[@timedOut]", store.proxy.reader.rawData) != null) {
        Ext.getDom('_targetPage').value = 'login';
        DCT.Submit.submitForm();
    }
    var errorNode = Ext.DomQuery.selectNode("/page/content/errors", store.proxy.reader.rawData);
    if (errorNode) {
        DCT.Util.getSafeElement("_errorXml").value = errorNode.xml;
        DCT.Submit.submitPage("errors");
    }
    var downloadStatusNode = Ext.DomQuery.selectNode("/root/downloadPolicyStatus[@id='downloadStatus']", store.proxy.reader.rawData);
    if (downloadStatusNode) {
        var message = downloadStatusNode.getAttribute('status');
        new DCT.AlertMessage({ title: DCT.T('DownloadStatus'), msg: message }).show();
    }
    if (me.dctLoadMethod)
        me.dctLoadMethod(store, records, options);
	}
});

DCT.Util.reg('dctpolicysearchpagingstore', 'policySearchPagingStore', 'DCT.PolicySearchPagingStore');