/**
* @class DCT.ValidationLogs
*/
Ext.ns('DCT.ValidationLogs');

Ext.apply(DCT.ValidationLogs, {
	validationLogData: null,
	validationLogWindow: null,
	requestInProgress: false,
	minimized: true,
	hasValidationsElem: true,

	showValidationLogDialog: function() {
		var me = this;

		var window = me.validationLogWindow || Ext.get('validationLogWindow') || Ext.getCmp('validationLogWindow');

		if (window == null && !me.requestInProgress && me.hasValidationsElem) {
			me.requestInProgress = true;
			if (me.validationLogData == null) {
				Ext.Ajax.request({
					url: DCT.Util.getPostUrl(),
					method: "POST",
					success: function(response, opts) {
						var data = response.responseText;
						if (!Ext.isEmpty(data)) {
							if (Ext.String.startsWith(data, '<validationLogData', true)) {
								me.validationLogData = DCT.Util.loadXMLString(data);
								DCT.ValidationLogs.initValidationLogDialog(me.validationLogData);
							} else {
								me.hasValidationsElem = false;
							}
						}
						me.requestInProgress = false;
					},
					scope: DCT.LoadingMessage,
					params: {
						_submitAction: 'validationLogs',
						_SuppressAutoSave: '1',
						_quoteId: DCT.Util.getSafeElement("_QuoteID").value
					}
				});
			} else {
				me.initValidationLogDialog(me.validationLogData);
			}
		}
	},

	initValidationLogDialog: function(validationLogData) {
		var me = this;
		var logHtml = '';
		logs = Ext.DomQuery.select("validationLogData/validation[@message]", validationLogData);
		if (logs == null || logs.length < 1) {
			me.validationLogData = 'No validation log data';
			logHtml = '<h2>No field corrections for the current quote.</h2>';
		} else {
			for (var i = 0; i < logs.length; i++) {
				var validationHtml = '<h2><b>Field: </b>';
				var fieldName = Ext.DomQuery.selectValue("@fieldName", logs[i]);
				var message = Ext.DomQuery.selectValue("@message", logs[i]);
				var oldValue = Ext.DomQuery.selectValue("@oldValue", logs[i]);
				var newValue = Ext.DomQuery.selectValue("@newValue", logs[i]);

				if (fieldName != null && fieldName != "") {
					validationHtml += fieldName;
				}
				validationHtml += '\n</h2>';

				if (oldValue != null && oldValue != "") {
					validationHtml += '<p><b>Was</b>: ' + oldValue + '\n</p>';
				}

				if (newValue != null && newValue != "") {
					validationHtml += '<p><b>Is Now: </b>: ' + newValue + '\n</p>';
				}

				if (message != null && message != "") {
					validationHtml += '<p><b>Error: </b>: ' + message + '\n\n</p>';
				}

				logHtml += validationHtml;
			}
		}
		me.validationLogWindow = Ext.create('Ext.Window', {
			id: 'validationLogWindow',
			width: 320,
			minWidth: 276,
			minHeight: 320,
			height: 420,
			zoomIncrement: .05,
			hideCollapseTool: true,
			maximizable: false,
			closable: false,
			frame: false,
			html: '<div class="container" background:>' + logHtml + '</div>',
			header: {
				xtype: 'panel',
				frame: false,
				border: false,
				height: 30,
				id: 'validationLogHeader',
				style: {
					background: 'transparent',
					'margin-top': 5,
					border: 'none',
					'box-shadow': 'none !important',
					'float': 'right'
				},
				items: [
				{
					xtype: 'button',
					id: 'validationMinimize',
					iconCls: 'spotlightMinusIcon',
					style: {
						border: 'none',
						background: '#006699',
						height: '25px',
						width: '25px',
						'border-radius': '5px',
						'margin-right': '5px',
						'float': 'right'
					},
					listeners: {
						afterrender: function(me) {
							// Register the new tip with an element's ID
							Ext.tip.QuickTipManager.register({
								target: me.getId(), // Target button's ID
								cls: 'x-tip-validationLog',
								text: 'Hide', // Tip content
								listeners: {
									beforerender: function(x, y, z) {
										alert('hiiihh');
									}
								}
							});
						}
					},
					handler: Ext.bind(function() {
						Ext.getCmp('validationLogWindow').collapseValidations();
					}, this)
				},
				{
					xtype: 'panel',
					id: 'validationTitleText',
					html: '<div class="validationTitleText">Automatic Corrections</div>'
				}]
			},
			Style: {
				background: 'transparent',
				position: 'fixed !important',
				'box-shadow': 'none'
			},
			collapseValidations: function () {
				var me = this;

				Ext.DomHelper.append('relatedActions', {
					tag: 'b',
					onclick: "Ext.getCmp('validationLogWindow').restoreValidations(this);",
					cls: 'fieldValidationsExpandIcon',
					id: 'validationsRestoreHelper',
					style: 'float:right;cursor: pointer;'
				});

				DCT.ValidationLogs.minimized = true;

				me.hide();
			},
			restoreValidations : function (btn) {
				var me = this;
				DCT.ValidationLogs.minimized = false;
				me.show();
				if (btn.remove) {
					btn.remove();
				} else {
					btn.removeNode();
				}
			},
			closeToolText: '',
			resizable: true,
			liveDrag: true,
			guid: DCT.Util.getWindowGuid(),
			plain: true,
			scrollable: true,
			shadow: false,
			modal: false,
			focusOnToFront: true,
			toFrontOnShow: true,
			alwaysOnTop: true,
			tbar: [
				'->'
			],
			listeners: {
				show: {
					fn: function (win, eOpts) {
						var me = this;

						win.setStyle({
							left: 'auto',
							right: '200px',
							top: '15%'
						});

					},
					scope: me
				}
			}
		});
		if (me.minimized) {
			me.validationLogWindow.collapseValidations();
		} else {
			me.validationLogWindow.show();
		}
	}
});

