/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	 *
	 */
	updatePASSearchMode: function(value, checked){
		if (checked){
			DCT.Submit.activeSearchMode = value;
			DCT.Util.getSafeElement('_clearFilterCache').value = '1';
			DCT.Util.getSafeElement('_cacheDataXml').value = value;
			DCT.ajaxProcess.processPage('search');
			if (value == 'policy' || value == 'client') {
				DCT.Util.hideShowGroup("searchConstraintsWrapper", true);
			} else {
				DCT.Util.hideShowGroup("searchConstraintsWrapper", false);
			}
		}
	},
	/**
	 *
	 */
	disableCheck: function(){
		var wrapper = Ext.get('searchConstraintsWrapper');
		if (wrapper.hasCls('hideOffset')){
			wrapper.select("input,textarea,a", true).each(function (fieldEl) {
					DCT.Util.disableComponent(fieldEl.id,true);
				});
			}
	},
	/**
	 *
	 */
	policyRecordCountCheck: function(dataStore, newRecords, dataOptions){
		Ext.get("clientGridEmpty").setDisplayed(false);
		Ext.getCmp("clientListData").hide();
		Ext.get("createClientButton").setDisplayed(false);

		if (dataStore.getTotalCount() == 0){
			Ext.get("policyGridEmpty").setDisplayed(true);
			Ext.getCmp("quoteList").hide();
		}else{
			Ext.get("policyGridEmpty").setDisplayed(false);
			Ext.getCmp("quoteList").show();
		}
        DCT.LoadingMessage.hide();
	},
	/**
	 *
	 */
	clientRecordCountCheck: function(dataStore, newRecords, dataOptions ){
		Ext.get("policyGridEmpty").setDisplayed(false);
		Ext.getCmp("quoteList").hide();
		Ext.get("createClientButton").setDisplayed(true);

		if (dataStore.getTotalCount() == 0){
			Ext.get("clientGridEmpty").setDisplayed(true);
			Ext.getCmp("clientListData").hide();
		}else{
			Ext.get("clientGridEmpty").setDisplayed(false);
			Ext.getCmp("clientListData").show();
		}
        DCT.LoadingMessage.hide();
	},
    /**
    *
    */
    setSearchField: function () {
        var table = document.getElementById("searchFilterTable").rows.length;
        if (table >= 1) {
            for (var i = 1; i <= table; i++) {
                var name = '_keynameAdvSearch' + i + 'Combo';
                var value = document.getElementById(name).value;
                var dateField = Ext.getCmp('_keyvalueDateSearch' + i);
                var textField = Ext.getCmp('_keyvalueTextSearch' + i);
                if (value == 'EffectiveDate' || value == 'Quote.ExpirationDate' || value == 'Quote.DateAccessed') {
                    dateField.setVisible(true);
                    dateField.setDisabled(false);
                    textField.setVisible(false);
                    textField.setDisabled(true);
                }
            }
        }
    }
});
/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	SUPPORT_PARTY: true, //setting this to true is not yet fully supported
	activeSearchMode: null,
	policyGridDataStore: null,
	clientGridDataStore: null,
	/**
	 *
	 */
	runQuickSearch: function(){
		var me = this;
		var quickSearchText = Ext.getCmp('quickSearchTextId').getValue();
		var newSearchMode = Ext.getCmp('quickSearchModeId').getValue();
		me.activeSearchMode = newSearchMode;
		DCT.Util.getSafeElement('_cacheDataXml').value = newSearchMode;
		DCT.Util.getSafeElement('_clearFilterCache').value = '0';
		DCT.Util.getSafeElement('_filterSubset').value = 'Search';

		switch (newSearchMode){
			case 'policy':
				DCT.Util.getSafeElement('_keynameSearch').value = 'PolicyNumber';
				DCT.Util.getSafeElement('_typeOfSearchQuick').value = 'policy';
				break;
			case 'clientname':
				DCT.Util.getSafeElement('_keynameSearch').value = 'Name';
				DCT.Util.getSafeElement('_typeOfSearchQuick').value = 'client';
				break;
			case 'phone':
				DCT.Util.getSafeElement('_keynameSearch').value = 'Phone.PhoneNumber';
				DCT.Util.getSafeElement('_typeOfSearchQuick').value = 'client';
				break;
			default:
				DCT.Util.getSafeElement('_keynameSearch').value = 'PolicyNumber';
				DCT.Util.getSafeElement('_typeOfSearchQuick').value = 'policy';
				break;
		}
		DCT.Util.getSafeElement('_keyvalueSearch').value = quickSearchText;
		DCT.Util.getSafeElement('_keyopSearch').value = 'contains';
		DCT.Submit.submitPage('search');
	},
	/**
	 *
	 */
	applySearchFilter: function (searchMode) {
		var me = this;
		if (!DCT.Util.validateFields()) { return false; }
		if (!Ext.getCmp("quoteList").isVisible()){
			DCT.LoadingMessage.show();
		}
		if (Ext.isEmpty(searchMode)){
			var selectedRadio = Ext.getCmp('searchRadioGroup').getValue();
			searchMode = (selectedRadio) ? Ext.Object.getValues(selectedRadio)[0] : null;
		}
		me.policyGridDataStore = Ext.getCmp('quoteList').getStore();
		me.clientGridDataStore = Ext.getCmp('clientListData').getStore();
		switch (searchMode){
			case 'allPolicies':
			case 'policy':
				DCT.Grid.applyFilterToDataStore(me.policyGridDataStore);
				break;
			case 'allClient':
			case 'client':
				DCT.Grid.applyFilterToDataStore(me.clientGridDataStore);
				break;
			case 'clientname':
				DCT.Grid.applyFilterToDataStore(me.clientGridDataStore);
				searchMode = 'client';
				break;
			case 'phone':
				DCT.Grid.applyFilterToDataStore(me.clientGridDataStore);
				searchMode = 'client';
				break;
			default:
				DCT.Grid.applyFilterToDataStore(me.policyGridDataStore);
				searchMode = 'policy';
				break;
		}
		me.activeSearchMode = searchMode;
	},
	/**
	 *
	 */
	addSearchFilter: function(){
		DCT.Util.getSafeElement('_clearFilterCache').value = '0';
		DCT.Util.getSafeElement('_searchConstraintsAction').value = 'add';
		DCT.ajaxProcess.processPage('search');
		DCT.Util.getSafeElement('_searchConstraintsAction').value = '';
	},
	/**
	 *
	 */
	removeSearchFilter: function(indexOfFilterRow){
		var filterTable = Ext.getDom('searchFilterTable');
		DCT.Util.getSafeElement('_clearFilterCache').value = '0';
		DCT.Util.getSafeElement('_searchConstraintsAction').value = 'remove';
		DCT.Util.getSafeElement('_actionKeyIndex').value = indexOfFilterRow - 1;
		DCT.ajaxProcess.processPage('search');
		DCT.Util.getSafeElement('_searchConstraintsAction').value = '';
		DCT.Util.getSafeElement('_actionKeyIndex').value = 0;
	},
	/**
	 *
	 */
	createNewClient: function(){
		var me = this;
		if (me.SUPPORT_PARTY){
			me.submitPartyAction('partyStart:partyAdd','["_AddPageTitle:Add New Client"]');
		}else{
			me.newClient('clientDetail');
		}
	},
	/**
	 *
	 */
	editClient: function(clientId){
		var me = this;
		if (me.SUPPORT_PARTY){
			me.submitPartyAction('partyStart:partyUpdate','["_partyIndex:' + clientId + '","_UpdatePageTitle:Client Information","_partyReturnStructure:none"]');
		}else{
			me.gotoPageForClient('clientDetail', clientId);
		}
	}
});