/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	 *
	 */
	submitClientAction: function(page, iClientID, action){
		var me = this;
		Ext.getDom('_targetPage').value = page;
		Ext.getDom('_submitAction').value = action;
		Ext.getDom('_ClientID').value = iClientID;
		me.secureAction(page);
		// in order to format phone numbers, etc, when updating client details.
	  if (!DCT.Util.validateFields()){return false;}
		me.submitForm();
	},
	/**
	 *
	 */
	newClient: function(page){
		var me = this;
		Ext.getDom('_targetPage').value = page;
		DCT.Util.getSafeElement('_ClientID').value = '0';
		me.secureAction(page);
		me.submitForm();
	},
	/**
	 *
	 */
	actOnClient: function(sAction, iClientID){
		var me = this;
		DCT.globalClientID = iClientID;
		DCT.globalAction = sAction;
		var msg;
		if ((Ext.getCmp('showDeleted') != null) && (Ext.getCmp('showDeleted').getValue()))
			msg = DCT.T("AreYouSureUndelete");
		else
			msg = DCT.T("AreYouSureDelete");
		me.actionVerify(msg, Ext.Function.createDelayed(me.completeClientProcessing, 0, me));
	},
	/**
	 *
	 */
	completeClientProcessing: function(btn){
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn != 'no'){
			var gridPage = DCT.Util.getSafeElement('_gridPage').value;
			DCT.Util.getSafeElement('_ClientID').value = DCT.globalClientID;
			if (gridPage != 'open'){
				DCT.Util.getSafeElement('_targetPage').value = "";
				me.submitAction(DCT.globalAction);
				DCT.Util.getSafeElement('_ClientID').value = '0';
			}
			else
				DCT.Grid.applyFilterToDataStore(Ext.getCmp('clientListData').getStore(), true, DCT.Submit.resetClientVariables);
		}
	},
	/**
	 *
	 */
	resetClientVariables: function(btn){
		DCT.Util.getSafeElement('_ClientID').value = '0';
	},
	/**
	 *
	 */
	gotoPageForClient: function(page, clientID){
		var me = this;
		me.removeKeyNames();
		DCT.Util.getSafeElement('_ClientID').value = clientID;
		me.submitPage(page);
	}
});