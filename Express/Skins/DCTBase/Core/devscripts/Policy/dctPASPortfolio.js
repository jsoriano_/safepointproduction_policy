/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	 *
	 */
	loadPortfolio: function(portfolioID){
		var me = this;
		DCT.Util.getSafeElement('_usingPortfolio').value = 1;
		DCT.Util.getSafeElement('_PortfolioID').value = portfolioID;
		me.submitAction('loadPortfolio');
	},
	/**
	 *
	 */
	addNewPortfolio: function(clientID, page){
		var me = this;
		if (page == null)
			page = 'new';
		me.removeKeyNames();
		DCT.Util.getSafeElement('_ClientID').value = clientID;
		DCT.Util.getSafeElement('_PortfolioID').value = 0;
		DCT.Util.getSafeElement('_usingPortfolio').value = 1;
		DCT.Util.getSafeElement('_UseClientID').value = 1;
		me.submitPage(page);
	},
	/**
	 *
	 */
	deletePortfolio: function(portfolioID){
		var me = this;
		DCT.globalPortfolioID = portfolioID;
		DCT.globalAction = 'deletePortfolio';
		me.actionVerify(DCT.T("AreYouSureDeletePortfolio"), Ext.Function.createDelayed(me.completePortfolioProcessing, 0, me));
	},
	/**
	 *
	 */
	completePortfolioProcessing: function(btn){
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn != 'no'){
			DCT.Util.getSafeElement('_PortfolioID').value = DCT.globalPortfolioID;
			me.submitAction(DCT.globalAction);
		}
	},
	/**
	 *
	 */
	deletePortfolioAttachment: function(attachmentID){
		var me = this;
		DCT.Util.getSafeElement('_attachmentID').value = attachmentID;
		me.submitPageAction('portfolioAttachment','deletePortfolioAttachment');
	},
	/**
	 *
	 */
	downloadPortfolioAttachment: function(attachmentID){
		var me = this;
		DCT.Util.getSafeElement('_attachmentID').value = attachmentID;
		me.submitPageAction('portfolioAttachment','downloadPortfolioAttachment');
	}
});
