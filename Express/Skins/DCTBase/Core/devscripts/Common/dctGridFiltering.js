 /**
 * Overrride the date filter class to configure which filter option to appear.
 */

Ext.define('DCT.grid.filters.filter.Date', {
    override: 'Ext.grid.filters.filter.Date',

     /**
     * @cfg {Object} date menu options
     * Configuration options for which menu option will appear.
     */
		dateBefore: true,
		dateAfter: true,
		dateOn: true,
   
    createMenu: function(config) {
        var me = this;
        me.getFilterMenuItems();
        me.callParent(arguments);
    },
    getFilterMenuItems: function(){
    	var me = this,
    			menuItems = [];
      if (me.dateBefore)
         menuItems.push('lt');
      if (me.dateAfter)
         menuItems.push('gt');
      if (me.dateBefore && me.dateAfter && me.dateOn)
         menuItems.push('-');
      if (me.dateOn)
         menuItems.push('eq');
      me.menuItems = menuItems;    	
    },
    onMenuSelect: function(picker, date) {
        var fields = this.fields,
            field = fields[picker.itemId],
            gt = fields.gt,
            lt = fields.lt,
            eq = fields.eq,
            v = {};
        field.up('menuitem').setChecked(true, 
        true);
        if (field === eq) {
        	if (lt)
            lt.up('menuitem').setChecked(false, true);
          if (gt)  
            gt.up('menuitem').setChecked(false, true);
        } else {
        	if (eq)
            eq.up('menuitem').setChecked(false, true);
            if (field === gt && lt && (+lt.value < +date)) {
                lt.up('menuitem').setChecked(false, true);
                
                v.lt = null;
            } else if (field === lt && gt && (+gt.value > +date)) {
                gt.up('menuitem').setChecked(false, true);
                
                v.gt = null;
            }
        }
        v[field.filterKey] = date;
        this.setValue(v);
        picker.up('menu').hide();
    }
});

Ext.define('DCT.grid.filters.filter.List', {
    override: 'Ext.grid.filters.filter.List',

    createListStore: function (options) {
        var me = this,
            store = me.store,
            isStore = options.isStore,
            idField = me.idField,
            labelField = me.labelField,
            optionsStore = false,
            storeData, o, i, len, value;

        if (isStore) {
            if (options !== me.getGridStore()) {
                optionsStore = true;
                store = me.store = options;
            } else {
                me.autoStore = true;
                storeData = me.getOptionsFromStore(options);
            }
        } else {
            storeData = [];

            for (i = 0, len = options.length; i < len; i++) {
                value = options[i];

                switch (Ext.typeOf(value)) {
                    case 'array':
                        storeData.push(value);
                        break;
                    case 'object':
                        storeData.push(value);
                        break;
                    default:
                        if (value != null) {
                            o = {};
                            o[idField] = value;
                            o[labelField] = value;
                            storeData.push(o);
                        }
                }
            }
        }

        if (!optionsStore) {
            if (store) {
                store.destroy();
            }

            store = me.store = new Ext.data.Store({
                fields: [idField, labelField],
                data: storeData
            });

            // Note that the grid store listeners may have been bound in the constructor if it was determined
            // that the grid filter was active and defined with a value.
            
            // Note: this should only be actived if the items are getting created from the store and not supplied
            //  if (!me.gridStoreListeners) {
            //      me.getGridStore().on(me.getGridStoreListeners());
            //  }

            me.loaded = true;
        }

        me.setStoreFilter(store);
    }
});