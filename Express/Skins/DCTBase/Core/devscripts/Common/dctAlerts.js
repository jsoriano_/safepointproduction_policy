
/**
 * @class DCT.AlertMessage
 * This class wraps the Ext.MessageBox functionality into an OO model.
 */
Ext.define('DCT.AlertMessage', {
	title:   DCT.T('Alert'),
	xtype: 'dctalertmessage',
	cls: 'cMsg',

	/**
	 * @param {Object} config The source of the properties to pass to Ext.MessageBox.
	 */
	constructor: function (config) {
		var me = this;
		me.setButton(config);
		me.setIcon(config);
		me.callParent([config]);
		Ext.apply(me, config);
	},

	/**
	 * Show a message dialog. This is a non-blocking dialog.
	 */
	show: function () {
		var me = this;
		Ext.MessageBox.show(me);
	},
	/**
	 * 
	 */
	setButton: function (config) {
		config.buttons = Ext.MessageBox.OK;
	},
	/**
	 *
	 */
	setIcon: function (config) {
		config.icon = Ext.MessageBox.WARNING;
	}
});

/**
*
*/
Ext.define('DCT.RatingMessageToolBar', {
	extend: 'Ext.Toolbar',
	dctControl: true,
	cls: 'ratingToolBar',
	inputXType: 'dctratingmessagetoolbar',
	xtype: 'dctratingmessagetoolbar',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setListeners(config);
		me.setRenderTo(config);
		me.setToolbarItems(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setRenderTo: function (config) {
		config.renderTo = (config.renderTo) ? config.renderTo : Ext.getBody();
	},
	/**
	* 
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function() { 
							var me = this;
							DCT.hubEvents.addListener('ajaxcomplete', me.cleanUpToolbar, me);
						},
						scope: me
			}
		};
	},
	/**
	* 
	*/
	cleanUpToolbar: function () { 
		var me = this,
				rateMsgItems = Ext.getDom('justMessages');
		if (!rateMsgItems){
			me.destroy();
		}
	},
	/**
	*
	*/
	setToolbarItems: function (config) {
		var me = this;
		me.items = [{
			xtype: 'button',
			text: DCT.T('ClickToViewRatingMessages'),
			icon: DCT.imageDir + 'icons\/error.png',
			handler: function (btn) {
				var rateMsgItems = Ext.getDom('justMessages').innerHTML;
				var win = Ext.create('Ext.Window', {
					width: 550,
					title: DCT.T('RatingMessages'),
					dctControl: true,
					scrollable: true,
					cls: 'rateMsgWindow',
					modal: true,
					bodyCls:'rateMsgBody',
					html: "<ul id='rateMsgWinList' class='rateMsgList'>" + rateMsgItems + "</ul>"
				});
				win.show();
			}
		}];
	}		  
});
DCT.Util.reg('dctratingmessagetoolbar', 'ratingMessages', 'DCT.RatingMessageToolBar');