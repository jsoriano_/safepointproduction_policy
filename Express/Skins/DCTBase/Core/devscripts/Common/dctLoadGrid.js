/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
	/**
	*
	*/
	applySearch: function (dataStore, exactMatchFilter) {
		var me = this;
		var filter = me.getFilterKeys(exactMatchFilter);
		var previousPage = Ext.getDom('_previousPage').value;
		Ext.getDom('_filterXML').value = filter;
		dataStore.loadPage(1, {params:{_previousPage: previousPage, _filterXML: filter}});
	},
	/**
	*
	*/
	applyFilterToDataStore: function (dataStore, refreshStore, resetFunction) {
		var startIndex = 0;
		var maxPageSize = 0;
		var localRefreshStore = true;
		
		if(!Ext.isEmpty(refreshStore)){
			localRefreshStore = refreshStore;
		}
		if (!Ext.isEmpty(dataStore)){
			if (localRefreshStore){
				dataStore.loadPage(dataStore.currentPage, {
					callback: function(records, operation, success) {
						var me = this;
						if (Ext.isEmpty(records)){
							DCT.Util.getSafeElement('_gridAction').value = "";
							DCT.Util.getSafeElement('_gridPage').value = "";
							DCT.Util.getSafeElement('_submitAction').value = "";
							if (Ext.isDefined(resetFunction)){
								resetFunction();
							}
							if ((me.currentPage-1) > 0){
								me.loadPage(me.currentPage-1, {
									callback: function(records, operation, success) {
										DCT.LoadingMessage.hide()
									}
								});
							}
						}else{
							DCT.LoadingMessage.hide()
							DCT.Util.getSafeElement('_gridAction').value = "";
							DCT.Util.getSafeElement('_gridPage').value = "";
							DCT.Util.getSafeElement('_submitAction').value = "";
							if (Ext.isDefined(resetFunction)){
								resetFunction();
							}
						}
					}
				});
			}
		}
	},
	/**
	*
	*/
	getFilterKeys: function (exactMatchFilter) {
		var searchFields = Ext.dom.Query.select("INPUT[type!=hidden]");
		var filter = '<keys>';
		for (i=0;i<searchFields.length;i++)
	   {
	    var elSib = searchFields[i];
	    var dctComponent = Ext.getCmp(elSib.getAttribute('data-componentid'));
			if (elSib.value.length > 0 && (elSib.id.indexOf('Agency') == 0) && !Ext.isEmpty(dctComponent)){
	    	var componentName = (dctComponent.hiddenName) ? dctComponent.hiddenName : dctComponent.name;
				if(Ext.isEmpty(dctComponent.getValue()) || !exactMatchFilter)
					filter = filter + '<key name="' + componentName.replace(/_/, ".") + '" value="' + dctComponent.getValue() + '" connector="and" op="contains" />';
				else
					filter = filter + '<key name="' + componentName.replace(/_/, ".") + '" value="' + dctComponent.getValue() + '" connector="and" op="equal" />';
			}
		 }
		filter = filter + '</keys>';
		return filter;		
	}
});
