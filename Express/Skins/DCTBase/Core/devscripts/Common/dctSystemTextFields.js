
/**
*
*/
Ext.define('DCT.SystemTextField', {
  extend: 'DCT.InputField',
  
 	inputXType: 'dctsystemtextfield',
 	xtype: 'dctsystemtextfield',

 	/**
 	*
 	*/
 	setDefaultListeners: function (config) {
 		if (!Ext.isDefined(config.listeners)){
		    config.listeners = {
			};
		}
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setDisable: function (config) {
		if (Ext.isDefined(config.renderTo))
	    config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
	}	
});
DCT.Util.reg('dctsystemtextfield', 'systemTextField', 'DCT.SystemTextField');

//==================================================
// Login text control 
//==================================================

/**
*
*/
Ext.define('DCT.LoginTextField', {
  extend: 'DCT.SystemTextField',
  
 	inputXType: 'dctlogintextfield',
 	xtype: 'dctlogintextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					me.loginOnEnter(e, textfield.dctSecure);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	loginOnEnter: function (e, secure) {
	  if (e.getKey() == e.ENTER){
	      if (!secure)
	          DCT.Submit.submitAction('login');
	      else
	          DCT.Submit.submitPageAction('welcome','login');
	      return false;
	  }
	}
	
});
DCT.Util.reg('dctlogintextfield', 'loginTextField', 'DCT.LoginTextField');

//==================================================
// Quick Search Policy text control 
//==================================================

/**
*
*/
Ext.define('DCT.PolicyQuickSearchTextField', {
  extend: 'DCT.SystemTextField',
  

 	inputXType: 'dctpolicyquicksearchtextfield',
 	xtype: 'dctpolicyquicksearchtextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					DCT.Submit.setActiveParent('search');
					me.searchOnEnter(e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	searchOnEnter: function (e) {
	    if (e.getKey() == e.ENTER){
	        DCT.Submit.runQuickSearch();
	    }
	}
	
	
});
DCT.Util.reg('dctpolicyquicksearchtextfield', 'policyQuickSearchTextField', 'DCT.PolicyQuickSearchTextField');

//==================================================
// Quick Search Billing text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingQuickSearchTextField', {
  extend: 'DCT.SystemTextField',
  
 	inputXType: 'dctbillingquicksearchtextfield',
 	xtype: 'dctbillingquicksearchtextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					me.onEnter(textfield.getValue(), e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	onEnter: function (value, e) {
	    if (e.getKey() == e.ENTER){
	        DCT.Submit.billingQuickSearch(value, Ext.getCmp('quickSearchModeId').getValue(), 'accountSearch');
	    }
	}
	
	
});
DCT.Util.reg('dctbillingquicksearchtextfield', 'billingQuickSearchTextField', 'DCT.BillingQuickSearchTextField');
//==================================================
// Quick Search Billing Dashboard text control 
//==================================================

/**
*
*/
Ext.define('DCT.BillingDashboardSearchTextField', {
  extend: 'DCT.SystemTextField',
  
 	inputXType: 'dctbillingdashboardsearchtextfield',
 	xtype: 'dctbillingdashboardsearchtextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					me.onEnter(textfield.getValue(), e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	onEnter: function (value, e) {
	    if (e.getKey() == e.ENTER){
	    		DCT.Submit.billingQuickSearch(value, Ext.getCmp('searchMode').getValue(), 'accountDashBoardSearch')
	    }
	}
	
	
});
DCT.Util.reg('dctbillingdashboardsearchtextfield', 'billingDashboardSearchTextField', 'DCT.BillingDashboardSearchTextField');
//==================================================
// Batch Search text control 
//==================================================

/**
*
*/
Ext.define('DCT.BatchSearchTextField', {
  extend: 'DCT.SystemTextField',
  
 	inputXType: 'dctbatchsearchtextfield',
 	xtype: 'dctbatchsearchtextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					me.batchQuery(textfield.getValue(), e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	batchQuery: function (value, e) {
		var me = this; 
		if(e.getKey() == e.ENTER){
		 	me.resetOpenQuery('batchProcess'); 
		}
	}
});
DCT.Util.reg('dctbatchsearchtextfield', 'batchSearchTextField', 'DCT.BatchSearchTextField');

//==================================================
// Email text control 
//==================================================

/**
*
*/
Ext.define('DCT.SystemEmailField', {
  extend: 'DCT.InputEmailField',
  
	inputXType: 'dctsystememailfield',
	xtype: 'dctsystememailfield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
	    config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	
   
});
DCT.Util.reg('dctsystememailfield', 'systemEmailField', 'DCT.SystemEmailField');
//==================================================
// Phone text control 
//==================================================

/**
*
*/
Ext.define('DCT.SystemPhoneField', {
  extend: 'DCT.InputPhoneField',
  
	inputXType: 'dctsystemphonefield',
	xtype: 'dctsystemphonefield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
    config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	

});
DCT.Util.reg('dctsystemphonefield', 'systemPhoneField', 'DCT.SystemPhoneField');
//==================================================
// ZipCode text control 
//==================================================

/**
*
*/
Ext.define('DCT.SystemZipCodeField', {
  extend: 'DCT.InputZipCodeField',
  
	inputXType: 'dctsystemzipcodefield',
	xtype: 'dctsystemzipcodefield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
    config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	

});
DCT.Util.reg('dctsystemzipcodefield', 'systemZipCodeField', 'DCT.SystemZipCodeField');

//==================================================
// Task Search text control 
//==================================================

/**
*
*/
Ext.define('DCT.TaskSearchTextField', {
  extend: 'DCT.SystemTextField',
  
 	inputXType: 'dctbatchsearchtextfield',
 	xtype: 'dcttasksearchtextfield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(textfield, e) {
					var me = this;
					DCT.Util.updateForSearch(me.dctTaskFilter);
				},
				scope: me
			}
		});
	}		
});
DCT.Util.reg('dcttasksearchtextfield', 'taskSearchTextField', 'DCT.TaskSearchTextField');
