
/****/
Ext.define('DCT.InputFloatField', {
  extend: 'DCT.InputNumberField',

	inputXType: 'dctinputfloatfield',
	xtype: 'dctinputfloatfield',

  /**  *  */
  setValidator: function(config){
  	var me = this;
   	config.validator = me.checkFloat;
  },
  /**  *  */
  setFormatter: function(config){
  	config.formatter = config.formatting.formatFloat;
  },
  /**  *  */
  setCultureSymbols: function(config){
  	var me = this;
  	config.getCultureSymbols = me.getFloatSymbols;
  },
  /**  *  */
  setStripSymbols: function(config){
  	var me = this;
  	config.getStripSymbols = me.getFloatStripSymbols;
  }	  
});
DCT.Util.reg('dctinputfloatfield', 'interviewInputFloatField', 'DCT.InputFloatField');
