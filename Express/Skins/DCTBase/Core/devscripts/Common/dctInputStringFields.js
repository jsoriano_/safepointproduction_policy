/**
* 
*/
Ext.define('DCT.InputStringField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputstringfield',
	dctFormatBeforePost: true,
	xtype: 'dctinputstringfield',

	/**
	* 
	*/
	setValidator: function (config) {
		var me = this;
		config.validator = me.checkString;
	},

	/**
	* 
	*/
	setFormatter: function (config) {
		var me = this;
		switch (config.dctJSFormatter)
		{
			case 'upper':
				config.formatter = me.formatUpper;
			break;
			case 'lower':
				config.formatter = me.formatLower;
			break;
			case 'string':
				config.formatter = me.formatString;
			break;
		}
	}		  
   
});
DCT.Util.reg('dctinputstringfield', 'interviewInputStringField', 'DCT.InputStringField');
