/****/
DCT.CustomEvents = {

	// Custom events are ones that can be specified in the XSL on an element, but
	// do not represent real Ext events. The definition should bind to an event
	// that actually exists.

	/**	*	*/
	enter: function (obj, fn) {
		obj.on("keypress", function (f, e) {
			if (parseInt(e.keyCode, 10) === 13) 
			{
				if (Ext.isString(fn)) eval(fn);
				if (Ext.isFunction(fn)) fn(f, e);
			}
		});
	}

};
