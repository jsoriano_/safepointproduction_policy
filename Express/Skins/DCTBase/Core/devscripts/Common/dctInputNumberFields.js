/**
* 
*/
Ext.define('DCT.InputNumberField', {
	extend: 'Ext.form.TextField',
	
	inputXType: 'dctinputnumberfield',
	formatter: undefined,
	maskChars: '[0-9{0}]',
	cultureSymbols: '\\$-.,',
	highFloat: 99999999999.9999,
	lowFloat: -99999999999.9999,
	highInteger: 2147483647,
	lowInteger: -2147483648,
	stripChars: '[\\$,]',
	dctControl: true,	
	xtype: 'dctinputnumberfield',

	/**
	* 
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.setFormatting(config);
		me.setFormatter(config);
		me.setRegEx(config);
		me.setCultureSymbols(config);
		me.setMaskRe(config);
		me.setStripSymbols(config);
		me.setStripCharRe(config);
		me.setValidator(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	* 
	*/
	setFormatter: function(config){
	},
	/**
	* 
	*/
	formatField: function(){
		var me = this;
		if (Ext.isDefined(me.formatter)){
			var fieldValue = me.getValue();
			if (me.isValid() && me.isDirty()){
				if (!Ext.isEmpty(fieldValue)){
					me.setRawValue(me.formatter(fieldValue));
				}
			}
		}
	},
	/**
	* 
	*/
	setStripSymbols: function(config){
	},
	/**
	* 
	*/
	setCultureSymbols: function(config){
	},
	/**
	* 
	*/
	setMaskRe: function(config){
		var me = this;
		var regChars = Ext.String.format(me.maskChars, (Ext.isDefined(config.getCultureSymbols) ? config.getCultureSymbols(config) : me.cultureSymbols));
		config.maskRe = new RegExp(regChars,'i');
	},
	/**
	* 
	*/
	setStripCharRe: function(config){
		var me = this;
		var regChars = Ext.isDefined(config.getStripSymbols) ? config.getStripSymbols(config) : me.stripChars;
		config.stripCharsRe = new RegExp(regChars,'gi');
	},
	/**
	* 
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
			 specialkey: function(field, e){
						if (e.getKey() == e.ENTER) {
								return false;
						}
				},
			blur: {
				fn: function(field) {
					var me = this;
					me.fieldValidation();
					me.formatField();
				},
				scope:this
			},
			change: {
				fn: me.fieldChanged,
				scope:this
			},
			focus: {
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope:this
			},
			keydown: {
				fn: function (f, e) {
					var me = this;
					switch (e.getKey()){
						case e.TAB:
							DCT.Util.setKeysUsed(true, e.shiftKey);
						break;
					}
				},
				scope: me
			}
		};
	},
	/**
	* 
	*/
	setAutoCreate: function(config){
		var controlSize = (Ext.isDefined(config.size)) ? config.size : 20;
		config.width = controlSize * 6.5 + 20;
		config.size = 0;
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		if (Ext.isDefined(config.maxLength)) {
			config.enforceMaxLength = true; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**
	* 
	*/
	setRegEx: function(config){
		config.regex = (Ext.isDefined(config.dctRegEx)) ? config.dctRegEx : null;
		config.regexText = (Ext.isDefined(config.dctRegExMsg)) ? config.dctRegExMsg : '';
	},
	/**
	* 
	*/
	setValidator: function(config){
	},
	/**
	* 
	*/
	setFormatting: function(config){
		config.numberMask = (Ext.isDefined(config.dctNumberFormat)) ? config.dctNumberFormat : '';
		config.numberType = (Ext.isDefined(config.dctType)) ? config.dctType : '';
		config.formatting = Ext.create('DCT.Formatting', config);
	},  
	/**
	* 
	*/
	getFloatSymbols: function(field){
		var currency = (Ext.isEmpty(field.formatting.currencySymbol)) ? "" : ((field.formatting.currencySymbol == '$') ? '\\' + field.formatting.currencySymbol : field.formatting.currencySymbol);
		var thousands = (Ext.isEmpty(field.formatting.thousandsSymbol)) ? "" : ((field.formatting.thousandsSymbol == '.') ? '\\' + field.formatting.thousandsSymbol : field.formatting.thousandsSymbol);
		var cents = (Ext.isEmpty(field.formatting.centsSymbol)) ? "" : ((field.formatting.centsSymbol == '.') ? '\\' + field.formatting.centsSymbol : field.formatting.centsSymbol);
		var floatSymbols = currency + cents + thousands + field.formatting.negativeSign;
		return(floatSymbols);
	},
	/**
	* 
	*/
	getFloatStripSymbols: function(field){
		var currency = (Ext.isEmpty(field.formatting.currencySymbol)) ? "" : ((field.formatting.currencySymbol == '$') ? '\\' + field.formatting.currencySymbol : field.formatting.currencySymbol);
		var thousands = (Ext.isEmpty(field.formatting.thousandsSymbol)) ? "" : ((field.formatting.thousandsSymbol == '.') ? '\\' + field.formatting.thousandsSymbol : field.formatting.thousandsSymbol);
		var floatStripArray = (Ext.isEmpty(currency + thousands)) ? "" : '[' + currency + thousands + ']';
		return(floatStripArray);
	},
	/**
	* 
	*/
	getIntStripSymbols: function(field){
		var currency = (Ext.isEmpty(field.formatting.currencySymbol)) ? "" : ((field.formatting.currencySymbol == '$') ? '\\' + field.formatting.currencySymbol : field.formatting.currencySymbol);
		var thousands = (Ext.isEmpty(field.formatting.thousandsSymbol)) ? "" : ((field.formatting.thousandsSymbol == '.') ? '\\' + field.formatting.thousandsSymbol : field.formatting.thousandsSymbol);
		var intStripArray = (Ext.isEmpty(thousands)) ? "" : '[' + currency + thousands + ']';
		return(intStripArray);
	},
	/**
	* 
	*/
	getIntSymbols: function(field){
		var currency = (Ext.isEmpty(field.formatting.currencySymbol)) ? "" : ((field.formatting.currencySymbol == '$') ? '\\' + field.formatting.currencySymbol : field.formatting.currencySymbol);
		var thousands = (Ext.isEmpty(field.formatting.thousandsSymbol)) ? "" : ((field.formatting.thousandsSymbol == '.') ? '\\' + field.formatting.thousandsSymbol : field.formatting.thousandsSymbol);
		var intSymbols = currency + thousands + field.formatting.negativeSign;
		return(intSymbols);
	},
	/**
	* 
	*/
	buildNumberExp: function(numberFormat, type, numberMax, numberMin) {
		var me = this;
		var leftReq = 0;
		var leftTot = 0;
		var rightMask = "";
		var leftMask = "";
		var rightReq = 0;
		var rightTot = 0;
		var decimal = -1;
		var negative = -1;
		var leftStart = 0;
		var leftDif = 0;
		var rightDif = 0;
		var intPart = "";
		var decPart = "";
		var highFloat = "";
		var lowFloat = "";

		matchStr = "^";
		negative = numberFormat.indexOf('-');
		if (negative > -1)
				matchStr = matchStr + "(-)?";
		var currency = numberFormat.indexOf('$');
		if (currency > -1)
			matchStr = matchStr + "(\\$)?";
		decimal = numberFormat.indexOf('.');
		if (decimal > -1) {
				rightMask = numberFormat.substr(decimal + 1, numberFormat.length - (decimal + 1));
				if (negative > -1)
						leftStart = 1;
				else
						leftStart = 0;
				leftMask = numberFormat.substr(leftStart, decimal - leftStart);
		} else {
				rightMask = "";
				if (negative > -1)
						leftStart = 1;
				else
						leftStart = 0;
				leftMask = numberFormat.substr(leftStart, numberFormat.length - leftStart);
		}
		if (rightMask != "") {
				rightTot = rightMask.length;
				rightReq = rightTot;
				for (i = 0; i < rightMask.length; i++) {
						if ((rightMask.charAt(i) == "?") || (rightMask.charAt(i) == "0"))
								rightReq--;
				}
		}
		leftTot = leftMask.length;
		leftReq = leftTot;
		for (i = 0; i < leftMask.length; i++) {
				if ((leftMask.charAt(i) == "?") || (leftMask.charAt(i) == "0"))
						leftReq--;
		}
		matchStr = matchStr + "(\\d{" + leftReq + "," + leftTot + "})";
		if (decimal > -1) {
				matchStr = matchStr + "\\.";
				if (rightReq == 0)
						matchStr = matchStr + "?";
				matchStr = matchStr + "(\\d{" + rightReq + "," + rightTot + "})";
		}
	
		if (type == "float") {
			if (Ext.isEmpty(numberMax)) {
					highFloat = me.highFloat.toString();
				decPart = highFloat.substr(highFloat.indexOf('.'), highFloat.length - highFloat.indexOf('.'));
				intPart = highFloat.substr(0, highFloat.length - decPart.length);
				leftDif = leftTot - intPart.length;
				rightDif = rightTot - (decPart.length - 1);
				if (leftDif > 0) {
					for (i = 0; i < leftDif; i++) {
						intPart = intPart + "9";
					}
				}
				if (rightDif > 0) {
					for (i = 0; i < rightDif; i++) {
						decPart = decPart + "9";
					}
				}
				highFloat = intPart + decPart;
				me.highFloat = highFloat;
			}
			if (Ext.isEmpty(numberMin)) {
				lowFloat = me.lowFloat.toString();
				decPart = lowFloat.substr(lowFloat.indexOf('.'), lowFloat.length - lowFloat.indexOf('.'));
				intPart = lowFloat.substr(0, lowFloat.length - decPart.length);
				leftDif = leftTot - (intPart.length - 1);
				rightDif = rightTot - (decPart.length - 1);
				if (leftDif > 0) {
					for (i = 0; i < leftDif; i++) {
						intPart = intPart + "9";
					}
				}
				if (rightDif > 0) {
					for (i = 0; i < rightDif; i++) {
						decPart = decPart + "9";
					}
				}
				lowFloat = intPart + decPart;
				me.lowFloat = lowFloat;
			}
	}
	return matchStr;
	},
	/**
	* 
	*/
	checkFloat: function(fieldValue){
		var me = this;
		if (!Ext.isEmpty(fieldValue)){
  		
  		if (me.formatting.isMultiCurrency(me.dctNumberFormat, me.dctCurrencyCode)) {
			return true;
		}

			fieldValue = fieldValue.toString();
			var numDecMask = 0;
			fieldValue = me.formatting.formatNumberToUS(fieldValue);
			if (!Ext.isEmpty(me.formatting.centsSymbol) && me.formatting.numberMask.indexOf(me.formatting.centsSymbol) > -1) {
					if (me.formatting.numberMask.indexOf(me.formatting.centsSymbol) < me.formatting.numberMask.length - 1) {
							for (var i = me.formatting.numberMask.indexOf(me.formatting.centsSymbol) + 1; i < me.formatting.numberMask.length; i++) {
									if ((me.formatting.numberMask.charAt(i) == "#") || (me.formatting.numberMask.charAt(i) == "0") || (me.formatting.numberMask.charAt(i) == "?"))
											numDecMask++;
							}
					}
			} else
					numDecMask = 0;
			var numberFormat = me.formatting.numberMask.replace(/0/g, "?");
			var internalCentsSymbol = '';
			if (!Ext.isEmpty(me.formatting.centsSymbol) && (me.formatting.centsSymbol != '.')) {
				internalCentsSymbol = '.';
			}
			else {
				internalCentsSymbol = me.formatting.centsSymbol;
			}
			matchExp = new RegExp(me.buildNumberExp(me.formatting.internalFormat, me.dctType, me.dctNumberMax, me.dctNumberMin));
			var matchArr = fieldValue.match(matchExp);
			var highVal = (Ext.isEmpty(me.dctNumberMax)) ? me.highFloat : parseFloat(me.dctNumberMax);
			var lowVal = (Ext.isEmpty(me.dctNumberMin)) ? me.lowFloat : parseFloat(me.dctNumberMin);
			if (numDecMask > 0) {
					if (fieldValue.indexOf(internalCentsSymbol) > -1) {
							var numDecUser = fieldValue.length - fieldValue.indexOf(internalCentsSymbol) - 1;
							if (numDecMask < numDecUser)
									return DCT.T("FloatInvalidFormat", { format: numberFormat });
					}
			}
			fieldValue = fieldValue.replace(/,/g,'');
			var objFloat = (fieldValue == "") ? parseFloat(Number("0").toFixed(numDecMask)) : parseFloat(Number(fieldValue).toFixed(numDecMask));
	
			if (matchArr == null || isNaN(objFloat)) {
					return DCT.T("FloatInvalidFormat", { format: numberFormat });
			}
			if (objFloat > highVal || objFloat < lowVal) {
					var lowObj = new Number(lowVal).toFixed(2);
					var highObj = new Number(highVal).toFixed(2);
					return DCT.T("ValueInvalidRange", { min: lowObj, max: highObj });
			}
		}
		return true;
	},
	/**
	* 
	*/
	checkInt: function(fieldValue){
		var me = this;
		if (!Ext.isEmpty(fieldValue)){
			fieldValue = fieldValue.toString();
			fieldValue = me.formatting.formatNumberToUS(fieldValue);
			if (!Ext.isEmpty(me.formatting.centsSymbol)){
				var dpPosition = fieldValue.lastIndexOf(me.formatting.centsSymbol);
				if (dpPosition > -1) {
					return DCT.T("IntegerNoDecimalPlaces", { format: me.formatting.numberMask });
				}
			}
			while ((fieldValue.charAt(0) == "0") && (fieldValue.length > 1)) {
				fieldValue = fieldValue.substring(1, fieldValue.length);
			}
			if (isNaN(fieldValue)){
				return DCT.T("IntegerInvalidValue", { format: me.formatting.numberMask });
			}
			var matchExp = new RegExp(me.buildNumberExp(me.formatting.internalFormat, me.dctType, me.dctNumberMax, me.dctNumberMin));
			var matchArr = fieldValue.match(matchExp);
			var highVal = (Ext.isEmpty(me.dctNumberMax)) ? me.highInteger : parseInt(me.dctNumberMax);
			var lowVal = (Ext.isEmpty(me.dctNumberMin)) ? me.lowInteger : parseInt(me.dctNumberMin);
			if (matchArr == null) {
				return DCT.T("IntegerInvalidValue", { format: me.formatting.numberMask });
			}
			var objInt = parseInt(fieldValue);
			if (objInt > highVal || objInt < lowVal) {
				return DCT.T("ValueInvalidRange", { min: lowVal, max: highVal });
			}
			if (objInt == 0 && matchArr[1] == '-') {
				return DCT.T("IntegerNegativeZero");
			}
		}
		return true;
	},
	/**
	* 
	*/
  processRawValue: function(value) { // override the processRawValue so the stripped value doesnt get displayed and only used for validation.
      var me = this,
          stripRe = me.stripCharsRe,
          newValue;

      if (stripRe) {
          newValue = value.replace(stripRe, '');
          if (newValue !== value) {
              value = newValue;
          }
      }
      return value;
  },
	/**
	* 
	*/
	setDefaultListeners: function(config){
	}  	
});
