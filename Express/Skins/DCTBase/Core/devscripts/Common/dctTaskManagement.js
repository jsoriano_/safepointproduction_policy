/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	*
	*/
	openAddNewTaskDialog: function (returnPage) {
		var me = this;
		var waiting = DCT.ajaxProcess.waitForRequests(me);			
		if (waiting)
		{ 
			return;
		}
		DCT.Util.getSafeElement('_returnPage').value = returnPage;
		DCT.Util.getSafeElement('_detailMode').value = 'add';
		me.submitPage('taskDetail', '');
	},
	/**
	*
	*/
	openAddNewBillingTaskDialog: function (returnPage) {
		var me = this;
	  DCT.Util.getSafeElement('_returnPage').value = returnPage;
	  DCT.Util.getSafeElement('_detailMode').value = 'add';
	  me.submitPage('billingTaskDetail', '');
	},
	/**
	*
	*/
	viewTask: function (taskId, returnPage, postAction) {
		var me = this;
		var waiting = DCT.ajaxProcess.waitForRequests(me);
		if (waiting) {
			return;
		}
		DCT.Util.getSafeElement('_returnPage').value = returnPage;
		DCT.Util.getSafeElement('_taskId').value = taskId;
		DCT.Util.getSafeElement('_detailMode').value = 'view';
		if (postAction == null) postAction = '';
		me.submitPageAction('taskDetail', postAction);
	},
	/**
	*
	*/
	viewBillingTask: function (taskId, returnPage, postAction) {
		var me = this;
	  DCT.Util.getSafeElement('_returnPage').value = returnPage;
	  DCT.Util.getSafeElement('_taskId').value = taskId;
	  DCT.Util.getSafeElement('_detailMode').value = 'view';
	  if (postAction == null) postAction = '';
	  me.submitPageAction('billingTaskDetail', postAction);
	},
	/**
	*
	*/
	pickUpTask: function (taskId, taskLockingTS, returnPage) {
		var me = this;
		DCT.Util.getSafeElement('_taskId').value = taskId;
		DCT.Util.getSafeElement('_taskLockingTS').value = taskLockingTS;
		me.viewTask(taskId, returnPage, 'assignTask');
	},
	/**
	*
	*/
	reassignTask: function (taskId, taskLockingTS, returnPage) {
		var me = this;
		DCT.Util.getSafeElement('_taskId').value = taskId;
		DCT.Util.getSafeElement('_taskLockingTS').value = taskLockingTS;
		DCT.Util.getSafeElement('_detailMode').value = 'reassignDirect';
		me.submitPage('taskDetail', '');
	},
	/**
	*
	*/
	completeTask: function (taskId, taskLockingTS, returnPage) {
		Ext.Msg.confirm('Confirm', DCT.Localization.translate("AreYouSureTaskComplete"), function (btn, text) {
			if (btn == 'yes') {
				DCT.Util.getSafeElement('_taskId').value = taskId;
				DCT.Util.getSafeElement('_taskLockingTS').value = taskLockingTS;
				DCT.Submit.submitPageAction(returnPage, 'setTaskComplete');
			} else {
				return false;
			}
		});
	}
});
/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
	/**
	*
	*/
	taskRowRenderer: function (extRecord, rowIndex, rowParams, extDataStore) {
		var rowClass = '';
		var dueDate = extRecord.get('DueDate');
		if (dueDate && (dueDate) != '') {
			dueDate = Ext.Date.clearTime(dueDate, false);
			var todaysDate = new Date();
			todaysDate = Ext.Date.clearTime(todaysDate, false);
			if (todaysDate > dueDate) {
				rowClass = 'Overdue';
			}
		}
		return rowClass;
	},
	/**
	*
	*/
	taskTitleViewRenderer: function (returnPage, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView, onClickMethodCall) {
		var taskTitle = dataValue;
		var onClickMethod = 'DCT.Submit.viewTask';
		if (onClickMethodCall) {
			onClickMethod = onClickMethodCall;
		}
		return '<a href="javascript:;" id="aTaskTitle" onclick="' + onClickMethod + '(\'' + extRecord.id + '\', \'' + returnPage + '\')">' + Ext.util.Format.htmlEncode(taskTitle) + '</a>';
	},
	/**
	*
	*/
	generalTaskActionColumnRenderer: function (returnPage, isAdmin, currentUserID, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var taskLockingTS = extRecord.get('taskLockingTS');
		var taskStatusCode = extRecord.get('taskStatusCode');
		var isOpen = (taskStatusCode == 'OPN');
		var isViewOnly = ((extRecord.get('viewOnly')) == '1');
		var canPickup = ((extRecord.get('canPickup')) == '1');
		var canReassign = ((extRecord.get('canReassign')) == '1');
		var entityId = extRecord.get('entityId');
		var entityName = extRecord.get('entityName');
		var rowActionsHTML = '<ul class="gridAction"><li>';
		if (isOpen && DCT.IsReadOnly==false) {
			if (extRecord.get('assignedUserId') && extRecord.get('assignedUserId').length > 0) {
				if (!isAdmin) {
					if (extRecord.get('assignedUserId') == currentUserID || extRecord.get('createdBy') == currentUserID) {
						rowActionsHTML += '<a href="javascript:;" onclick="DCT.Submit.completeTask(\'' + extRecord.id + '\',\'' + taskLockingTS + '\', \'' + returnPage + '\');"><img src="' + DCT.imageDir + 'icons\/clipboard_tick.png" alt="Mark Complete" title="' + DCT.T('MarkComplete') +'"/></a>';
					}
				}
			} else {
				if (!isViewOnly && canPickup) {
					rowActionsHTML += '<a href="javascript:;" onclick="DCT.Submit.pickUpTask(\'' + extRecord.id + '\',\'' + taskLockingTS + '\', \'' + returnPage + '\');"><img src="' + DCT.imageDir + 'icons\/clipboard_link.png" alt="Pick Up" title="Pick Up"/></a>';
				} 
			}
			if (isAdmin && canReassign){
				rowActionsHTML += '<a href="javascript:;" onclick="DCT.Submit.reassignTask(\'' + extRecord.id + '\',\'' + taskLockingTS + '\', \'' + returnPage + '\');"><img src="' + DCT.imageDir + 'icons\/clipboard_pencil.png" alt="' + DCT.T('Reassign') + '" title="' + DCT.T('Reassign') + '"/></a>';
			}
		}
		rowActionsHTML += '</li></ul>';
		return rowActionsHTML;
	}
});

/**
* Task grids for Policy Details, My Tasks and Task Management pages
*/
Ext.define('DCT.TasksPagingGridPanel', {
	extend: 'DCT.PagingGridPanel',

	inputXType: 'dcttaskspaginggridpanel',
	xtype: 'dcttaskspaginggridpanel',

	setHubListeners: function (config) {
		DCT.hubEvents.addListener('taskpickedup', config.dctClassMethod);
		DCT.hubEvents.addListener('taskcompleted', config.dctClassMethod);
		DCT.hubEvents.addListener('taskadded', config.dctClassMethod);		
	}
});
DCT.Util.reg('dcttaskspaginggridpanel', 'tasksPagingGridPanel', 'DCT.TasksPagingGridPanel');

/**
* @class DCT.Util
*/
Ext.onReady(function() {
	DCT.hubEvents.addListener('taskpickedup', DCT.Util.refreshTaskDetails);
	DCT.hubEvents.addListener('taskcompleted', DCT.Util.refreshTaskDetails);
	DCT.hubEvents.addListener('taskadded', DCT.Util.refreshTaskDetails);
});

Ext.apply(DCT.Util, {
	/**
	*
	*/
	refreshTaskDetails: function (taskid) {
		var currentTaskId = DCT.Util.getSafeElement('_taskId').value;
		if (currentTaskId === taskid) {
			DCT.Submit.viewTask(taskid, 'taskManagement');
		}
	},
	/**
	* Refresh task queue grid on Policy Details page
	*/
	refreshPolicyTaskQueue: function () {
		DCT.Util.reloadTaskQueue(Ext.getCmp('policyQueueEntityId').getValue(), null, '', 'policy', 'TaskQueue', false, 'skipCheck');
	},
	/**
	* Refresh task queue grid on Task Management page
	*/
	refreshAdminTaskQueue: function () {
		DCT.Util.reloadTaskQueue(Ext.getCmp('adminQueueEntityId').getValue(), Ext.getCmp('adminAssignedUserId').getValue(), Ext.getCmp('adminFilter').getValue(), 'admin','QueueAdmin', false, 'skipCheck'); 
	},
	/**
	* Refresh assigned task queue grid on My Tasks page
	*/
	refreshAssignedTaskQueue: function () {
		DCT.Util.reloadTaskQueue('', null, Ext.getCmp('assignedFilterId').getValue(), 'assigned', 'QueueAssigned', false, 'skipCheck');
	},
	/**
	* Refresh unassigned task queue grid on My Tasks page
	*/
	refreshUnassignedTaskQueue: function () {
		DCT.Util.reloadTaskQueue(Ext.getCmp('unassignedQueueEntityId').getValue(), null, Ext.getCmp('unassignedFilterId').getValue(), 'unassigned', 'QueueUnassigned', false, 'skipCheck');
	},		
	/**
	*
	*/
	reloadTaskQueue: function (entityId, assignedUserId, showFilter, elemPrefix, gridId, resetFilters, linkId) {
		var me = this;
		if (linkId != '' && linkId != 'skipCheck'){
			liNode = Ext.get(linkId);
			if (liNode.hasCls('btnDisabled'))
				return;
		}
		if (resetFilters){
			assignedUserId = ''; //assigned user id is not just an 'additional' filter -- it gets passed in as well and needs to be cleared
			me.clearFilters(elemPrefix);
		}
		if (Ext.isEmpty(entityId))
			entityId = '';
		if (elemPrefix == 'admin' || elemPrefix == 'policy'){
			Ext.getCmp(elemPrefix + 'QueueEntityId').setValue(entityId);
		}else{
			DCT.Util.getSafeElement("_entityId").value = entityId;
		}
		if (assignedUserId == null)
			assignedUserId = '';
		if (elemPrefix == 'admin'){
			Ext.getCmp(elemPrefix + 'AssignedUserId').setValue(assignedUserId);
		}else{
			DCT.Util.getSafeElement("_assignedUserId").value = assignedUserId;
		}
		if (Ext.isEmpty(showFilter)){
			showFilter = '';
		}
		if (elemPrefix == 'admin'){
			creationFilter = Ext.getCmp(elemPrefix + 'CreatedOnFilter').getValue();
		}else{
			creationFilter = '';
		}
		DCT.Util.getSafeElement(elemPrefix + "CreationFilter").value = creationFilter;
		DCT.Util.getSafeElement("_show" + elemPrefix + "Filter").value = showFilter;
		DCT.Util.getSafeElement("_showFilter").value = showFilter;
		me.updateAdditionalFilters(elemPrefix, gridId);
		var gridDataStore = me.getDataStore(gridId);
		DCT.Grid.applyFilterToDataStore(gridDataStore);
	},
	/**
	*
	*/
	clearFilters: function (elemPrefix) {
		var catagoryFilter = (elemPrefix == 'unassigned' || elemPrefix == 'admin') ? 'GEN' : '';
		if (elemPrefix == 'admin' || elemPrefix == 'policy'){
			Ext.getCmp(elemPrefix + 'QueueEntityId').setValue('');
			}
		if (elemPrefix == 'admin'){
			Ext.getCmp(elemPrefix + 'AssignedUserId').setValue('');
			Ext.getCmp(elemPrefix + 'Filter').setValue('');
			}
		if (elemPrefix != 'policy'){
			Ext.getCmp(elemPrefix + 'CategoryFilterId').setValue(catagoryFilter);
			Ext.getCmp(elemPrefix + 'StatusFilterId').setValue('OPN');
			Ext.getCmp(elemPrefix + 'SearchFilterId').setValue('policy');
			Ext.getCmp(elemPrefix + 'SearchFilterForId').setValue('');
		}
		var clearButton = Ext.get(elemPrefix + 'ClearButton');
		clearButton.addCls('btnDisabled');
		clearButton.dom.children[0].disabled = true;
	},
	/**
	*
	*/
	updateAdditionalFilters: function (elemPrefix, filterSubset) {
		var me = this;
		var categoryFilter = '';
		var statusFilter = '';
		var searchFilter = '';
		var searchFilterFor = '';
		var keynameValue = '';
		var keyopValue = '';
		var keyvalueValue = '';
		var keyconnectorValue = '';
		
		if (elemPrefix != 'policy'){
			categoryFilter = Ext.getCmp(elemPrefix + 'CategoryFilterId').getValue();
			statusFilter = Ext.getCmp(elemPrefix + 'StatusFilterId').getValue();
			searchFilter = Ext.getCmp(elemPrefix + 'SearchFilterId').getValue();
			searchFilterFor = Ext.String.trim(Ext.getCmp(elemPrefix + 'SearchFilterForId').getValue());
		}
		if (categoryFilter != ''){
			keynameValue = me.appendDelimValue(keynameValue, 'TaskTypeCode');
			keyopValue = me.appendDelimValue(keyopValue, 'eq');
			keyvalueValue = me.appendDelimValue(keyvalueValue, categoryFilter);
			keyconnectorValue = me.appendDelimValue(keyvalueValue, 'and');
		}
		if (statusFilter != ''){
			var arrStatus = new Array(); 
			arrStatus = statusFilter.split(",");
			for (i=0;i<arrStatus.length;i++) {
				keynameValue = me.appendDelimValue(keynameValue, 'TaskStatusCode');
				keyopValue = me.appendDelimValue(keyopValue, 'eq');
				keyvalueValue = me.appendDelimValue(keyvalueValue, arrStatus[i]);
				keyconnectorValue = me.appendDelimValue(keyvalueValue, 'and');				
			} 
		}
		if (searchFilter != '' && searchFilterFor != ''){
			if (searchFilter == 'policy'){
				keynameValue = me.appendDelimValue(keynameValue, 'ObjectReference');
				keyopValue = me.appendDelimValue(keyopValue, 'cn');
				keyvalueValue = me.appendDelimValue(keyvalueValue, searchFilterFor);
				keyconnectorValue = me.appendDelimValue(keyvalueValue, 'and');
			}
			else if (searchFilter == 'titledesc'){
				keynameValue = me.appendDelimValue(keynameValue, 'TaskText');
				keyopValue = me.appendDelimValue(keyopValue, 'cn');
				keyvalueValue = me.appendDelimValue(keyvalueValue, searchFilterFor);
				keyconnectorValue = me.appendDelimValue(keyvalueValue, 'and');
			}
		}
		DCT.Util.getSafeElement('_keyname' + filterSubset).value = keynameValue;
		DCT.Util.getSafeElement('_keyop' + filterSubset).value = keyopValue;
		DCT.Util.getSafeElement('_keyvalue' + filterSubset).value = keyvalueValue;
		DCT.Util.getSafeElement('_keyconnector' + filterSubset).value = keyconnectorValue;
	},
	/**
	*
	*/
	getDataStore: function (ownerId) {
		var gridDataStore = null;
		var gridObj = Ext.getCmp(ownerId);
		if (!Ext.isEmpty(gridObj))
			gridDataStore = gridObj.store;
		return gridDataStore;
	},
	/**
	*
	*/
	updateForSearch: function (elemPrefix) {
		var clearButton = Ext.get(elemPrefix + 'ClearButton');
		clearButton.removeCls('btnDisabled');
		clearButton.dom.children[0].disabled = false;
	},
	/**
	*
	*/
	appendDelimValue: function (appendTo, valueToAppend) {
		if (appendTo.length > 0)
			appendTo += ',';
		appendTo += valueToAppend;
		return appendTo;
	}
});