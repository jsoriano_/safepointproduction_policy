
/**
*
*/
Ext.define('DCT.EditorCheckBoxField', {
  extend: 'DCT.CheckBoxField',

	inputXType: 'editorcheckboxfield',
	xtype: 'editorcheckboxfield',

	/**
	*
	*/
	setAutoCreate: function (config) {
		if (Ext.isDefined(config.fieldRef)) { 
			config.inputAttrTpl = 'fieldRef="' + config.fieldRef + '-editorcheckboxfield"' + ' objectRef="' + config.objectRef + '-editorcheckboxfield"'; 
		}
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		if (Ext.isDefined(config.listeners)) {
			Ext.apply(config.listeners, {
				change: {
					fn: function (control) {
						var me = this;
						me.fieldChanged();
						var fieldName = me.gridEditor.record.store.fields.itemAt(me.gridEditor.col).name;
						me.gridEditor.record.set(fieldName, me.getValue());
					},
					scope: me
				}
			});
			Ext.destroyMembers(config.listeners, 'focus', 'afterrender');
			Ext.Array.forEach(['focus', 'afterrender'], function(item, index, arrayList){
				delete config.listeners[item];
			}, me)
		} else {
			config.listeners = {
				change: {
					fn: function (control) {
						var me = this;
						me.fieldChanged();
						var fieldName = me.gridEditor.record.store.fields.itemAt(me.gridEditor.col).name;
						me.gridEditor.record.set(fieldName, me.getValue());
					},
					scope: me
				}
			};
		}
	}
});
