/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	loadXMLString: function (txt) {
	  if (window.DOMParser){
	  	
	    xmlDoc = new DOMParser().parseFromString(txt, "text/xml");
	  }else{  // Internet Explorer 
	  	
	    xmlDoc = new ActiveXObject('Msxml2.DOMDocument.6.0');
	    xmlDoc.setProperty("ProhibitDTD", true);
	    xmlDoc.setProperty("ProhibitDTD", true);
	    xmlDoc.async = "false";
	    xmlDoc.loadXML(txt);
	  }
	  return xmlDoc;
	},
	/**
	*
	*/
	getSingleNode: function (xmlDoc, elementPath) {
	  if(Ext.isIE){
	    return xmlDoc.selectSingleNode(elementPath);
	  }else{
	    var xpe = new XPathEvaluator();
	    var nsResolver = xpe.createNSResolver( xmlDoc.ownerDocument == null ? xmlDoc.documentElement : xmlDoc.ownerDocument.documentElement);
	    var results = xpe.evaluate(elementPath,xmlDoc,nsResolver,XPathResult.FIRST_ORDERED_NODE_TYPE, null);
	    return results.singleNodeValue; 
	  }
	},
	/**
	*
	*/
	getNodes: function (xmlDoc, elementPath) {
	  if(Ext.isIE){
	    return xmlDoc.selectNodes(elementPath);
	  }else{
	    var xpe = new XPathEvaluator();
	    var nsResolver = xpe.createNSResolver( xmlDoc.ownerDocument == null ? xmlDoc.documentElement : xmlDoc.ownerDocument.documentElement);
	    var results = xpe.evaluate(elementPath,xmlDoc,nsResolver,XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	    var myNodeArray = [];
	
	    for (var i=0; i<results.snapshotLength; i++) {
	      myNodeArray[i] = results.snapshotItem(i);
	    };
	    return myNodeArray;
	  }
	},
	/**
	*
	*/
	getNodeText: function (tNode) {
	  if(Ext.isIE)
	     return tNode.text;
	  else
	     return tNode.textContent;
	}
});

Ext.apply(Ext, {
    /**
     * Clone simple variables including array, {}-like objects, DOM nodes and Date without keeping the old reference.
     * A reference for the object itself is returned if it's not a direct descendant of Object. For model cloning,
     * see {@link Ext.data.Model#copy Model.copy}.
     *
     * @param {Object} item The variable to clone
     * @return {Object} clone
     */
    clone: function(item) {
   	    var objectPrototype = Object.prototype,
        		toString = objectPrototype.toString;

        if (item === null || item === undefined) {
            return item;
        }
        // DOM nodes
        // TODO proxy this to Ext.Element.clone to handle automatic id attribute changing
        // recursively
        if (item.nodeType && "cloneNode" in item) {
            return item.cloneNode(true);
        }
        var type = toString.call(item),
            i, j, k, clone, key;
        // Date
        if (type === '[object Date]') {
            return new Date(item.getTime());
        }
        // Array
        if (type === '[object Array]') {
            i = item.length;
            clone = [];
            while (i--) {
                clone[i] = Ext.clone(item[i]);
            }
        }
        // Object
        else if (type === '[object Object]' && item.constructor === Object) {
            clone = {};
            for (key in item) {
                clone[key] = Ext.clone(item[key]);
            }
            if (Ext.enumerables) {
                for (j = Ext.enumerables.length; j--; ) {
                    k = Ext.enumerables[j];
                    if (item.hasOwnProperty(k)) {
                        clone[k] = item[k];
                    }
                }
            }
        }
        return clone || item;
    }
});
