
/****/
Ext.define('DCT.InputDateField', {
	extend: 'Ext.form.DateField',

	defaultDateFormat: 'm/d/yyyy',
	internalDateFormat: 'Y-m-d',
	invalidText: "{0} is not a valid date",
	formatText: 'Expected date format: {0}',
	inputXType: 'dctinputdatefield',
	dctControl: true,
	dctSystemDate: '_defaultSystemDate',
	xtype: 'dctinputdatefield',
	useStrict: true,

	/**	*	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setShowToday(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.setDateFormat(config);
		me.setMaxLength(config);
		me.setAltFormats(config);
		me.setMinMaxDates(config);
		me.setDateValue(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**	*	*/
	setShowToday: function (config) {
		var me = this;
		var today = Ext.Date.clearTime(new Date());
		var min = config.minValue ? Ext.Date.clearTime(new Date(Ext.Date.parse(config.minValue, me.internalDateFormat))) : Number.NEGATIVE_INFINITY;
		var max = config.maxValue ? Ext.Date.clearTime(new Date(Ext.Date.parse(config.maxValue, me.internalDateFormat))) : Number.POSITIVE_INFINITY;
		if (today < min || today > max) {
			config.showToday = false;
		}
	},
	/**	*	*/
	setDateValue: function (config) {
		// If a day-offset was supplied, apply it now.
		if (!Ext.isEmpty(config.value) && !Ext.isEmpty(config.addDaysToDefaultDate)) {
			var dt = Ext.Date.parse(config.value, config.format);
			dt = Ext.Date.add(dt, Ext.Date.DAY, config.addDaysToDefaultDate);
			config.value = Ext.Date.format(dt, config.format);
		}
	},
	/**	*	*/
	getInternalDate: function () {
		var me = this;
		if (!Ext.isEmpty(me.getValue())) {
			return (Ext.Date.format(me.getValue(), me.internalDateFormat));
		}
	},
	/**	*	*/
	adjustDate: function (days) {
		var me = this;
		var systemDate = Ext.get(me.dctSystemDate);
		var currentDate = (systemDate) ? Ext.Date.parse(systemDate.getValue(), me.format) : new Date();
		var adjustedDate = Ext.Date.add(currentDate, Ext.Date.DAY, days);
		me.setValue(adjustedDate);
	},
	/**	*	*/
	addDate: function (date,days) {
		var me = this;
		
		var currentDate = Ext.Date.parse(date, me.format);

		if (currentDate != null) {

			var adjustedDate = Ext.Date.add(currentDate, Ext.Date.DAY, days);
		}
		else {
			var adjustedDate = Ext.Date.add(date, Ext.Date.DAY, days);
		}


		//
		
		me.setValue(adjustedDate);
	},
	/**	*	*/
	setAutoCreate: function (config) {
		var controlSize = (Ext.isDefined(config.size)) ? config.size : 10;
		config.width = (Ext.isDefined(config.width)) ? config.width : controlSize * 7.5 + 20;
		config.size = 0;
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) {
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"';
		}
		if (Ext.isDefined(config.maxLength)) {
			config.enforceMaxLength = true;
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**	*	*/
	setMaxLength: function (config) {
		var me = this;
		var dateFormat = (Ext.isEmpty(config.dateFormat)) ? me.defaultDateFormat : config.dateFormat;
		config.maxLength = (Ext.isEmpty(config.maxLength)) ? Number.MAX_VALUE : ((dateFormat.length > config.maxLength) ? dateFormat.length : config.maxLength);
	},
	/**	*	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			specialkey: function (field, e) {
				if (e.getKey() == e.ENTER) {
					return false;
				}
			},
			blur: {
				fn: me.fieldValidation,
				scope: this
			},
			change: {
				fn: me.fieldChanged,
				scope: this
			},
			focus: {
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope: this
			},
			keydown: {
				fn: function (f, e) {
					switch (e.getKey()) {
						case e.TAB:
							DCT.Util.setKeysUsed(true, e.shiftKey);
							break;
					}
				},
				scope: me
			}
		};
	},
	/**	*	*/
	setDateFormat: function (config) {
		var me = this;
		if (Ext.isEmpty(config.format)) {
			var dateFormat = (Ext.isEmpty(config.dateFormat)) ? me.defaultDateFormat : config.dateFormat;

			if (dateFormat.search("yyyy") > -1)
				dateFormat = dateFormat.replace("yyyy", "Y");
			else
				dateFormat = dateFormat.replace("yy", "y");
			if (dateFormat.search("MMM") > -1)
				dateFormat = dateFormat.replace("MMM", "M");
			else if (dateFormat.search(/mm/i) > -1)
				dateFormat = dateFormat.replace(/mm/i, "m");
			else
				dateFormat = dateFormat.replace(/m/i, "n");
			if (dateFormat.search("dd") > -1)
				dateFormat = dateFormat.replace("dd", "d");
			else
				dateFormat = dateFormat.replace("d", "j");

			config.format = dateFormat;
			config.formatText = Ext.String.formatEncode(me.formatText, (Ext.isEmpty(config.dateFormat)) ? me.defaultDateFormat : config.dateFormat);
		}
	},
	/**	*	*/
	setAltFormats: function (config) {
		if (Ext.isEmpty(config.altFormats)) {
			switch (config.format) {
				case "d-m-Y":
				case "d/m/Y":
					config.altFormats = 'd/m/Y|j/n/Y|j/n/y|j/m/y|d/n/y|j/m/Y|d/n/Y|d-m-y|d-m-Y|d/m|d-m|dm|dmy|dmY|d|Y-d-m';
					break;
				case 'd-MMM-Y':
				case 'd/MMM/Y':
					config.altFormats = 'dMY|jMY|d-M-Y|j-M-Y|dMy|jMy|d-M-y|j-M-y|dmY|d-m-Y|d-n-Y|j-n-Y|d-n-y|j-n-y';
					break;
				default:
					config.altFormats = Ext.form.DateField.prototype.altFormats + "|n/j/Y|n/j/y|m/j/y|n/d/y|m/j/Y|n/d/Y|n-j|n/j|n-j-Y|n-j-y|m-j-y|n-d-y|m-j-Y|n-d-Y|m/d/y";
					break;
			}
		}
		config.altFormats = "c|" + config.altFormats;
	},
	/**	*	*/
	setMinMaxDates: function (config) {
		switch (config.format) {
			case "d-m-Y":
			case "d/m/Y":
				var separator = (config.format == 'd/m/Y') ? '/' : '-';
				if (!Ext.isEmpty(config.minValue))
					config.minValue = config.minValue.substr(8) + separator + config.minValue.substr(5, 2) + separator + config.minValue.substr(0, 4);
				if (!Ext.isEmpty(config.maxValue))
					config.maxValue = config.maxValue.substr(8) + separator + config.maxValue.substr(5, 2) + separator + config.maxValue.substr(0, 4);
				break;
			case 'd-MMM-Y':
			case 'd/MMM/Y':
				var separator = (config.format == 'd/MMM/Y') ? '/' : '-';
				if (!Ext.isEmpty(config.minValue))
					config.minValue = config.minValue.substr(8) + separator + config.minValue.substr(5, 2) + separator + config.minValue.substr(0, 4);
				if (!Ext.isEmpty(config.maxValue))
					config.maxValue = config.maxValue.substr(8) + separator + config.maxValue.substr(5, 2) + separator + config.maxValue.substr(0, 4);
				break;
			default:
				break;
		}
	},
	/**	*	*/
	setDefaultListeners: function (config) {
	}
});
DCT.Util.reg('dctinputdatefield', 'inputDateField', 'DCT.InputDateField');
