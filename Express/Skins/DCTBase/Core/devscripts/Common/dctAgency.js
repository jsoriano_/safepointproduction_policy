/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
    *
    */
    searchAgency: function (resetFilters, linkId) {
        if (this.isButtonDisabled(linkId))
            return;
        var clearButton = Ext.get('clearLink');
        if (resetFilters) {
            this.resetAgencySearchFilters();
            clearButton.addCls('btnDisabled');
        } else {
            clearButton.removeCls('btnDisabled');
        }
        DCT.Grid.applySearch(Ext.getCmp('agencyList').getStore(), false);
    },
    /**
    *
    */
    resetAgencySearchFilters: function () {
        Ext.getCmp('Agency_Name_Id').setValue('');
        Ext.getCmp('Agency_PhoneNumber_Id').setValue('');
        Ext.getCmp('Agency_Address1_Id').setValue('');
        Ext.getCmp('Agency_City_Id').setValue('');
        Ext.getCmp('Agency_State_Id').setValue('');
    },
    /**
    *
    */
    selectAgency: function (agencyId, agencyCaption) {
        var parentWindow = DCT.Util.getParent();
        parentWindow.Ext.getDom('_AgencyID').value = agencyId;
        parentWindow.Ext.getDom('agencyIDField').innerHTML = agencyCaption;
        var parentGrid = parentWindow.Ext.getCmp('agencyList');
        if (parentGrid)
            parentGrid.getStore().loadPage(1, { params: { _AgencyID: agencyId }, callback: parentWindow.DCT.Util.loadProducerList });
        this.closeWindow();
    },
    /**
    *
    */
    popUpAgencies: function () {
        DCT.Util.getSafeElement('_popUp').value = '1';
        var agencyPopup = Ext.create('DCT.PopupWindow', {
            width: 850,
            height: 500,
            modal: true,
            dctItemClass: 'DCT.AgencyPanel',
            dctPanelConfig: {
                dctTargetPage: 'selectAgency'
            }
        });
        agencyPopup.show();
    },
    /**
    *
    */
    cancelSelectAgency: function (returnPage) {
        if (returnPage == 'login') {
            DCT.Submit.submitPage('logout');
        } else {
            this.closeWindow();
        }
    },
    /**
    *
    */
    loadProducerList: function (gridRecord, optionsObject, loadSuccess) {
        var producers;
        var producerField = Ext.getCmp('ProducerName');
        var producerStore = producerField.getStore();
        var producerSelected = '';
        var comboList = [];

        if (gridRecord.length > 0) {
            producerField.setValue('');
            producers = Ext.DomQuery.select('/page/content/userList/user', gridRecord[0].store.proxy.reader.rawData);
            if (producers.length > 0) {
                for (i = 0, n = producers.length; i < n; i++) {
                    producerName = Ext.DomQuery.selectValue('@name', producers[i]);
                    producerFullName = Ext.DomQuery.selectValue('@fullName', producers[i]);
                    if (Ext.isEmpty(producerFullName))
                        comboList.push([producerName, producerName]);
                    else
                        comboList.push([producerName, producerFullName]);
                    if (i == 0)
                        producerSelected = producerName;
                }
            }
            producerStore.loadData(comboList);
            producerField.collapse();
        } else {
            if (loadSuccess)
                producerStore.removeAll();
        }
        producerField.setValue(producerSelected);
    }
});