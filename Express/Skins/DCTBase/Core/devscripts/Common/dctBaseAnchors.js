/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	 * 
	 */ 
	 setInterviewActionAsync: function() {
		 this.getSafeElement('runInterviewActionAsync').value = true;
	 },
	/**
	 * 
	 */
	 processInterviewActionAsync: function (objectTypeCode, objectId, actionAsyncAllowWait) {
		
		if (DCT.Util.getSafeElement('runInterviewActionAsync').value === 'true') {

			if (!objectTypeCode) {
				objectTypeCode = "POL";
			}

			var portal = "AdminPAS";
			var title = "Policy Dashboard";

			switch (objectTypeCode) {
				case "PTY":
					portal = "Party";
					title = "Party Dashboard";
					break;
				case "BIL":
					portal = "Billing";
					title = "Billing Dashboard";
					break;
			}

			//Show Async Queue Process Overlay
			DCT.ProcessQueue.displayAsyncProcessingPopUp(title, portal, objectTypeCode, objectId, actionAsyncAllowWait);
		}
	},
	/**
	*
	*/
	processInterviewGridAnchor: function (gridId, anchorKey) {
		var me = this;
		var grid = Ext.getCmp(gridId);
		var anchorObject = grid.getStore().anchorObjects.get(anchorKey);
		me.processInterviewAnchor(anchorObject);
	},
	/**
	*
	*/
	processInterviewAnchor: function (anchorObject) {
		var me = this;
		var work;

		switch (anchorObject.processType) {
		case 'newWindow':
			work = function () { me.processInterviewAnchor_NewWindow(anchorObject); };
			break;
		case 'alertMsg':
			work = function () { me.processInterviewAnchor_AlertMessage(anchorObject); };
			break;
		case 'actionUrl':
			work = function () { me.processInterviewAnchor_ActionUrl(anchorObject); };
			break;
		case 'download':
			work = function () { me.processInterviewAnchor_Download(anchorObject); };
			break;
		case 'pdfPage':
			work = function () { me.processInterviewAnchor_PdfPage(anchorObject); };
			break;
		case 'popUp':
			work = function () { me.processInterviewAnchor_PopUp(anchorObject); };
			break;
		case 'integrationReturn':
			work = function () { me.processInterviewAnchor_IntegrationReturn(anchorObject); };
			break;
		case 'partyReturn':
			work = function () { me.processInterviewAnchor_PartyReturn(anchorObject); };
			break;
		case 'billingReturn':
			work = function () { me.processInterviewAnchor_BillingReturn(anchorObject); };
			break;
		case 'runForPortal':
			work = function () { me.processInterviewAnchor_RunForPortal(anchorObject); };
			break;
		default:
			work = function () { me.processInterviewAnchor_Default(anchorObject); };
			break;
		}

		if (anchorObject.processType != 'alertMsg') {
			if (anchorObject.isDeleteCmd) {
				if (anchorObject.isList) {
					me.initSubmission('delete', true);
				} else {
					me.initSubmission('delete', false);
				}
			}
		}
		if (!Ext.isEmpty(anchorObject.preEventField) && Ext.isEmpty(anchorObject.preEventFieldAsync)) {
			if (anchorObject.preEventFieldConfirm == '1') {
				me.preEventFieldConfirmAndGo(
				anchorObject.manuscriptId,
				anchorObject.actionName,
				anchorObject.preEventField,
				anchorObject.skipValidation,
				anchorObject.preEventFieldConfirmText,
				anchorObject.preEventFieldAlertOnly,
				work);
			} else {
				me.preEventField(
				anchorObject.manuscriptId,
				anchorObject.actionName,
				anchorObject.preEventField,
				anchorObject.skipValidation,
				anchorObject.preEventFieldAlertOnly,
				true,
				work);
			}
		} else {
			work();
		}

		if (anchorObject.processType != 'alertMsg') {
			if (anchorObject.handleSkinCommand) {
				anchorObject.skinCommand;
			}
		}
	},
	/**
	*
	*/
	processInterviewButtonConfirm: function (anchorObject, callFunction) {
		var me = this;
		if (anchorObject.isConfirm == '1' || anchorObject.isDelete == '1') {
			var confirmString;
			if (!Ext.isEmpty(anchorObject.confirmText)) {
				confirmString = anchorObject.confirmText;
			}
			else {
				confirmString = anchorObject.isConfirm == '1' ? DCT.T("AreYouSure") : DCT.T("AreYouSureDelete");
			}

			DCT.Util.removeTabs(true);
			Ext.Msg.confirm(DCT.T('Confirm'), confirmString, function (btn, text) {
				DCT.Util.removeTabs(false);
				if (btn == 'yes') {
					if (anchorObject.skipValidation != '1') {
						if (!DCT.Util.validateFields()) { return false; }
					}

					if (anchorObject.cancelChanges == '1') {
						DCT.Util.resetFields();
					}
					callFunction();
				}
				else
					return false;
			}, me);
		}
		else {
			callFunction();
		}
	},
	/**
	*
	*/
	processInterviewAnchor_RunForPortal: function (anchorObject) {
		var portalName = anchorObject.parameters["_PortalName"];
		if ((portalName == 'Billing') && (DCT.BillingSystemURL != ''))
			document.forms[0].action = DCT.BillingSystemURL;
		else if ((portalName != "Party") && (DCT.PolicySystemURL != ''))
			document.forms[0].action = DCT.PolicySystemURL;

		var seperator = anchorObject.postAction.search(':') == 0 ? '' : ',';
		var action = 'setPortal' + seperator + anchorObject.postAction;
		DCT.Submit.addToParmArray('_submitAction', action);
		for (var p in anchorObject.parameters) {
			DCT.Submit.addToParmArray(p, anchorObject.parameters[p]);
		}
		DCT.FieldsChanged.clear();
		DCT.Submit.setActiveParent('dashboardHome');
		DCT.Submit.submitPageForm(anchorObject.parameters['_targetPage']);
    },
	/**
	*
	*/
	processInterviewAnchor_Default: function (anchorObject) {
		var me = this;

		if (anchorObject.postAction == 'upload') {
			me.postFile();
		}

		var pagingGroups = (Ext.getDom('_groupPaging')) ? Ext.getDom('_groupPaging').value : null;
		if (pagingGroups) {
				DCT.Util.getSafeElement('_pagingGroups').value = pagingGroups;
		}

		var submitType = (anchorObject.isConfirm == '1') ? 'Confirm' : ((anchorObject.isDelete == '1') ? 'Delete' : '');
		DCT.ajaxProcess.submitAjax(
					'PageContainer',
					anchorObject.actionName,
					anchorObject.skipValidation,
					anchorObject.cancelChanges,
					submitType,
					anchorObject.processingMsg,
					anchorObject.confirmText);
	},
	/**
	*
	*/
	processInterviewAnchor_BillingReturn: function (anchorObject) {
		var me = this;
		if (anchorObject.postAction == 'upload') {
			me.postFile();
		}

		me.processInterviewButtonConfirm(anchorObject, function() {
			DCT.ajaxProcess.submitInterfaceReturn(anchorObject.popUp, anchorObject.postAction, '', anchorObject.actionName);
		});
	},
	/**
	*
	*/
	processInterviewAnchor_AlertMessage: function (anchorObject) {
		var me = this;
		me.showCustomMessage(anchorObject.alertMsg, anchorObject.alertMsgTitle, anchorObject.alertMsgClass, me);
	},
	/**
	*
	*/
	processInterviewAnchor_PartyReturn: function (anchorObject) {
		var me = this;
		if (anchorObject.postAction == 'upload') {
			me.postFile();
		}
		me.processInterviewButtonConfirm(anchorObject, function () {
			DCT.ajaxProcess.submitPartyReturn(anchorObject.popUp, anchorObject.postAction, anchorObject.partyId, anchorObject.actionName);
		});

	},
	/**
	*
	*/
	processInterviewAnchor_IntegrationReturn: function (anchorObject) {
		var me = this;
		if (anchorObject.postAction == 'upload') {
			me.postFile();
		}
		me.processInterviewButtonConfirm(anchorObject, function () {
			DCT.ajaxProcess.submitIntegrationReturn(anchorObject.popUp, anchorObject.postAction, anchorObject.integrationImportRq, anchorObject.actionName);
		});
	},
	/**
	*
	*/
	processInterviewAnchor_PopUp: function (anchorObject) {
		DCT.Util.popUpPage(
			anchorObject.popUp,
			anchorObject.actionName,
			anchorObject.skipValidation,
			anchorObject.cancelChanges,
			anchorObject.hideCloseButton,
			anchorObject.popUpClass,
			anchorObject.popUpWidth,
			anchorObject.popUpHeight,
			anchorObject.popUpTitle,
			anchorObject.isConfirm,
			anchorObject.confirmText);
	},
	/**
	*
	*/
	processInterviewAnchor_PdfPage: function (anchorObject) {

		DCT.Util.printOutWithAction(
			'',
			anchorObject.pdfPage,
			anchorObject.actionName,
			anchorObject.suppressRefresh);
	},
	/**
	*
	*/
	processInterviewAnchor_ActionUrl: function (anchorObject) {
		if (!Ext.isEmpty(anchorObject.preEventField)) {
			DCT.LoadingMessage.hideAndCancel();
			eval(anchorObject.actionUrl);
		} else {
			if (!Ext.isEmpty(anchorObject.newWindow)) {
				eval(window.open(anchorObject.actionUrl));
			}
			else {
				eval(anchorObject.actionUrl);
			}
		}
	},
	/**
	*
	*/
	processInterviewAnchor_Download: function (anchorObject) {
		if (anchorObject.attachmentID > 0) {
			DCT.Submit.processAttachment(
				anchorObject.attachmentId || 0,
				anchorObject.returnPage || '',
				anchorObject.interViewAction
			);
		} else {
			DCT.Util.getSafeElement('_action').value = anchorObject.actionName;
			DCT.Util.getSafeElement('_targetPage').value = "";
			DCT.Submit.submitAction(anchorObject.interViewAction);
			DCT.LoadingMessage.hideAndCancel();
		}
	},
	/**
	*
	*/
	processInterviewAnchor_NewWindow: function (anchorObject) {
	    if (!Ext.isEmpty(anchorObject.actionName)) {
	        //Since newWindow opens a new tab, we need to put certain parts of the page
	        //state back like we found it for future actions on the existing tab.
	        DCT.LoadingMessage.cancel();
	        var actionPrevValue = Ext.getDom('_action').value;
	        var popupPrevValue = DCT.Util.getSafeElement('_popUp').value;
	        var submitActionPreValue = DCT.Util.getSafeElement("_submitAction").value;
	        DCT.Util.getSafeElement('_duplicateWindow').value = '1';
	        DCT.Util.getSafeElement('_popUp').value = 'false';
	        Ext.getDom('_action').value = anchorObject.actionName;
	        document.forms[0].target = "_blank";

	        if (!Ext.isEmpty(anchorObject.portalName)) {
	            DCT.Util.getSafeElement('_externalSite').value = '0';
	            var seperator = anchorObject.postAction.search(":") == 0 ? "" : ",";
	            var action = "setPortal" + seperator + anchorObject.postAction;
	            DCT.Util.getSafeElement("_submitAction").value = action;
	            DCT.Util.getSafeElement("_PortalName").value = anchorObject.portalName;
	            for (var parameter in anchorObject.parameters) {
					if (parameter == "objectID") {
						if (anchorObject.portalType == "PAS") {
							if (DCT.PolicySystemURL != '') {
								document.forms[0].action = DCT.PolicySystemURL;
							}
							DCT.Util.getSafeElement("_QuoteID").value = anchorObject.parameters[parameter];
						} else if (anchorObject.portalType == "BIL") {
							if (DCT.BillingSystemURL != '') {
								document.forms[0].action = DCT.BillingSystemURL;
							}
							DCT.Util.getSafeElement("_accountId").value = anchorObject.parameters[parameter];
						} else if (anchorObject.portalType == "PTY") {
							DCT.Util.getSafeElement("_partyIndex").value = anchorObject.parameters[parameter];
						}
					}
					if (parameter == "language") {
						DCT.Util.getSafeElement("_language").value = anchorObject.parameters[parameter];
						DCT.Util.getSafeElement("_submitAction").value = "setLanguage," + DCT.Util.getSafeElement("_submitAction").value;
					}
				}
	        }

			DCT.Submit.submitForm();
			DCT.LoadingMessage.hideAndCancel();

			//Put value(s) back
			Ext.getDom('_action').value = actionPrevValue;
			DCT.Util.getSafeElement('_duplicateWindow').value = '0';
			DCT.Util.getSafeElement('_popUp').value = popupPrevValue;
			DCT.Util.getSafeElement("_submitAction").value = submitActionPreValue;
			
			Ext.getDom('_targetPage').value = "";
			document.forms[0].target = "";
		} else {
			window.open(anchorObject.actionUrl);
		}
	},
	/**
	*
	*/
	processInterviewButtonAnchor: function (divId) {
		var me = this;
		if (DCT.ajaxProcess.processingBlur) {
			DCT.ajaxProcess.needsSubmitAjax = true;
			DCT.ajaxProcess.delayedSubmitAjax = function () { DCT.Util.processInterviewButtonAnchor(divId); };
		}
		else {
			var divElement = Ext.get(divId);
			if (divElement) {
				var acnhorConfig = divElement.getDataPrefixedAttributes();
				me.processInterviewAnchor(acnhorConfig);
			}
		}
	},
	/**
	*
	*/
	processFormSubmit: function (submitScreen, submitType, skipValidation, cancelChanges, confirmText) {
		var me = this;
		me.customOnSubmit();
		if ((submitType == 'Confirm') || (submitType == 'Delete')) {
			if (!Ext.isEmpty(confirmText)) {
				strConfirm = confirmText;
			}
			else {
				if (submitType == 'Confirm')
					strConfirm = DCT.T("AreYouSure");
				else
					strConfirm = DCT.T("AreYouSureDelete");
			}

			DCT.Util.removeTabs(true);
			DCT.LoadingMessage.hideAndCancel();
			Ext.Msg.confirm(DCT.T('Confirm'), strConfirm, function (btn, text) {
				DCT.Util.removeTabs(false);
				if (btn == 'yes') {
					if (skipValidation != '1') {
						if (!DCT.Util.validateFields()) { return false; }
					}

					Ext.getDom('_targetPage').value = "";
					Ext.getDom('_action').value = submitScreen;
					document.forms[0].action = DCT.postPage;
					document.forms[0].target = "";

					if (cancelChanges == '1') {
						DCT.Util.resetFields();
					}
					DCT.Submit.submitForm();
				}
				else
					return false;
			}, me);
		}
		else {
			if (skipValidation != '1') {
				if (!DCT.Util.validateFields()) { 
					DCT.LoadingMessage.hideAndCancel();
					return false; }
			}
			Ext.getDom('_targetPage').value = "";
			Ext.getDom('_action').value = submitScreen;
			document.forms[0].action = DCT.postPage;
			document.forms[0].target = "";

			if (cancelChanges == '1') {
				DCT.Util.resetFields();
			}
			DCT.Submit.submitForm();
		}
	},
	/**
	*
	*/
	initSubmission: function (modeType, paging) {
		if (modeType == 'delete') {
			if (paging)
				Ext.getDom('_parentGroupIsPaged').value = '1';
			else
				Ext.getDom('_parentGroupIsPaged').value = '0';
		}
	},
	/**
	*
	*/
	preEventFieldConfirmAndGo: function (manuScriptID, actionUid, fieldName, skipValidation, preEventFieldConfirmText, preEventFieldAlertOnly, functionToBeCalled) {
		var me = this;
		if (Ext.isEmpty(preEventFieldConfirmText)) {
			preEventFieldConfirmText = DCT.T("AreYouSure");
		}
		me.removeTabs(true);
		Ext.Msg.confirm(DCT.T('Confirm'), preEventFieldConfirmText, function (btn) {
			me.removeTabs(false);
			if (btn == 'yes') {
				DCT.Util.preEventField(manuScriptID, actionUid, fieldName, skipValidation, preEventFieldAlertOnly, true, functionToBeCalled);
			}
		}, me);
	},
	/**
	*
	*/
	processPreEventField: function (responseDoc) {
		var me = this;
		DCT.FieldsChanged.clear();
		// Enable the buttons so they can continue to submit.
		DCT.Util.toggleButtons(false);
		if (!Ext.isEmpty(responseDoc) && me.displayMessage) {
			var message = '';
			// Keith Kelly - selectNodes() isn't supported in Firefox, use custom function
			var HTTPError = DCT.Util.getNodes(responseDoc, '/HTTPError');
			var errors = DCT.Util.getNodes(responseDoc, '//ManuScript.getValueRs[@status="failure"]/errors/error');
			if (HTTPError.length > 0) {
				Ext.each(HTTPError, function (errorNode) {
					message += (errorNode.text) ? '\r\n' + errorNode.text : '\r\n' + errorNode.textContent;
				});
			} else if (errors.length > 0) {
				message = DCT.T("FieldProcessingError");
				Ext.each(errors, function (errorNode) {
					message += (errorNode.text) ? '\r\n' + errorNode.text : '\r\n' + errorNode.textContent;
				});
			} else {
				//Keith Kelly - selectSingleNode() isn't supported in Firefox, use custom function
				var valueNode = DCT.Util.getSingleNode(responseDoc, '//ManuScript.getValueRs[@status="success"]/@value');
				if (valueNode == null)
					message = DCT.T("FieldNoValue", { name: me.fieldName, manuScriptId: me.manuScriptID });
				else {
					message = DCT.Util.getNodeText(valueNode);
					if (Ext.isEmpty(Ext.String.trim(message))) {
						message = "";
					}
				}
			}
			if (message.length > 0) {
				DCT.LoadingMessage.hideAndCancel();
				DCT.Util.removeTabs(true);
				Ext.create('DCT.AlertMessage', {
					title: DCT.T('Alert'),
					msg: message,
					fn: function (btn) {
						DCT.Util.removeTabs(false);
						if (btn == 'ok' && me.doFunctionToBeCalled && me.preEventFieldAlertOnly == '1') me.functionToBeCalled();
						if (me.doFunctionToBeCalled) return false;
						return me.preEventFieldAlertOnly == '1';
					},
					scope: me
				}).show();
			} else
				if (me.doFunctionToBeCalled) {
					me.functionToBeCalled();
					return true;
				}
		} else if (!Ext.isEmpty(responseDoc)) {
			DCT.LoadingMessage.hideAndCancel();
			DCT.Util.removeTabs(true);
			var asyncNode = DCT.Util.getSingleNode(responseDoc, '//ManuScript.getValueRs[@status="success"]/@asyncID');
			if (asyncNode != null) {
				var asyncValue = DCT.Util.getNodeText(asyncNode);
				if (asyncValue.length > 0) {
					DCT.Submit.setPortal('AdminPAS', '');
					return true;
				}
			}
		}
		else if (me.doFunctionToBeCalled) {
			me.functionToBeCalled();
		}
		return true;
	},
	/**
	*
	*/
	preEventField: function (manuScriptID, actionUid, fieldName, skipValidation, preEventFieldAlertOnly, displayMessage, functionToBeCalled) {
		var me = this;
		var xml;
		me.doFunctionToBeCalled = false;
		me.manuScriptID = manuScriptID;
		me.fieldName = fieldName;
		me.preEventFieldAlertOnly = preEventFieldAlertOnly;
		me.displayMessage = displayMessage;
		me.functionToBeCalled = functionToBeCalled;
		if (!Ext.isDefined(displayMessage))
			me.displayMessage = true;
		if (!Ext.isEmpty(functionToBeCalled))
			me.doFunctionToBeCalled = true;
		// Disable the buttons so they cannot continue to push the buttons (ClearHREF will not work, because it is destructive to the form).
		me.toggleButtons(true);
		if (skipValidation != '1')
			if (!DCT.Util.validateFields()) {
				me.toggleButtons(false);
				return false;
			}
		DCT.LoadingMessage.delayedShow();
		xml = DCT.ajaxProcess.createStoreXML(actionUid);
		DCT.ajaxProcess.postServerMessage(xml, "PreEvent/xml", Ext.Function.createDelayed(me.processPreEventField, 0, me), null, true);
	},
	/**
	*
	*/
	toggleButtons: function (disabled) {
		if ((document.forms[0].target == "") || (document.forms[0].target == "_self")) {
			var anchors = Ext.query('A');
			if (anchors) {
				for (i = 0, n = anchors.length; i < n; i++) {
					if (anchors[i].onclick) {
						if (disabled) {
							var onclick = anchors[i].getAttribute('onclick');
							anchors[i].setAttribute('data-onclick', onclick);
							anchors[i].setAttribute('onclick', "");
						} else {
							var data_onclick = anchors[i].getAttribute('data-onclick');
							if (!Ext.isEmpty(data_onclick)) {
								anchors[i].setAttribute('onclick', data_onclick);
								anchors[i].setAttribute('data-onclick', "");
							}
						}
					}
				}
			}
		}
	},
	/**
	*
	*/
	showCustomMessage: function (msg, title, cls, elem, callback) {
		var CustomMsg = "<div class='cMsg'>" + msg + "</div>";
		Ext.Msg.show({
			title: title || DCT.T('Alert'),
			cls: cls || 'cMsgWin',
			msg: CustomMsg,
			buttons: Ext.Msg.OK,
			width: 400,
			fn: callback || null,
			animEl: (elem && elem.id) ? elem.id : null
		});
	},
	/**
	*
	*/
	postFile: function () {
		document.forms[0].encoding = 'multipart/form-data';
		DCT.FieldsChanged.set();
	},
	/**
	*
	*/
	customOnSubmit: function () {
	}
});
