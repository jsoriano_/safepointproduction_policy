
/** 
*
*/
Ext.define('DCT.ExtendedComboField', {
  extend: 'DCT.ComboField',

	typeAhead: false, 
  editable: false,
  selectOnFocus: false,
  inputXType: 'dctextendedcombofield',
  xtype: 'dctextendedcombofield',

	/** 
	*
	*/
	setDataStore: function (config) {
		config.store = Ext.create('Ext.data.ArrayStore', {
        fields: ['code', 'display', 'caption', 'comparisonCaption', 'special'],
        data : config.dctStore
    });
	},
    /** 
	*
	*/
    setTemplate: function (config) {
    var extendedTemplate = Ext.create('Ext.XTemplate', 
            '<tpl for=".">',
                '<div data-qtip="{display:htmlEncode}" role="option" unselectable="on" class="x-boundlist-item ' + config.dctComparisonStyle + '">',
                    '<tpl if="!special">',
                        '<div class="widgetComboCaption">{caption:htmlEncode}</div><div class="widgetComboAffectedVal">{comparisonCaption:htmlEncode}</div>',
                    '</tpl>',
                    '<tpl if="special">',
                        '<div class="extSpecial"><div class="widgetComboCaption">{caption:htmlEncode}</div><div class="widgetComboAffectedVal">{comparisonCaption:htmlEncode}</div></div>',
                    '</tpl>',
                '</div>',
            '</tpl>'
    );
		config.tpl = extendedTemplate;
	}		  		  
   
});
DCT.Util.reg('dctextendedcombofield', 'interviewExtendedComboField', 'DCT.ExtendedComboField');
