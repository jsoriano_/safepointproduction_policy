
/**
*
*/
Ext.define('DCT.SystemNumberField', {
  extend: 'DCT.InputNumberField',
  
 	inputXType: 'dctsystemnumberfield',
 	xtype: 'dctsystemnumberfield',

 	/**
 	*
 	*/
 	setDefaultListeners: function (config) {
 		if (!Ext.isDefined(config.listeners)){
		    config.listeners = {
			};
		}
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setDisable: function (config) {
		if (Ext.isDefined(config.renderTo))
	    config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
	}	
});
DCT.Util.reg('dctsystemnumberfield', 'systemNumberField', 'DCT.SystemNumberField');

