/*** @class DCT.Grid*/
Ext.apply(DCT.Grid, {
	/**	*	*/
	senderItemDeselected: function (selectionModel, dataRecord, rowIndex) {
		DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
	},
	/**	*	*/
	senderItemSelected: function (selectionModel, dataRecord, rowIndex) {
		DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
	},
	/**	*	*/
	deleteNotification: function (messageID, returnPage) {
		DCT.Submit.actionVerify(
			DCT.T("AreYouSureDeleteNotification"),
			function(btn) { DCT.Grid.completeDeleteNotification(btn, messageID, returnPage); },
			false
		);
	},
	/**	*	*/
	completeDeleteNotification: function (btn, messageID, returnPage) {
		var me = this;
		if (btn != 'no'){ 
		  DCT.Util.getSafeElement('_gridAction').value = 'deleteMessage';
			DCT.Util.getSafeElement('_returnPage').value = returnPage; //tell it where to return to after it executes @@md:todo -- we really need to have this not be necessary, post actions should NOT be responsible for controlling which content page is targetted
			DCT.Util.getSafeElement('_submitAction').value = 'deleteMessage'; //this is still needed to make the action happen
			DCT.Util.getSafeElement('_messageID').value = messageID;
			me.applyFilterToDataStore(Ext.getCmp('messagesList').getStore(), true, DCT.Grid.resetDeleteNotification);
		}
	},
	/**
	*
	*/
	resetDeleteNotification: function () {
		DCT.Util.getSafeElement('_messageID').value = 0; //reset
		DCT.Util.getSafeElement('_submitAction').value = ''; //reset
	}	
});

/*** @class DCT.Submit*/
Ext.apply(DCT.Submit, {
	/**	*	*/
	submitMessage: function (returnPage) {
		var me = this;
	  if (Ext.get('newMessageAnchor').hasCls('btnDisabled'))
	     return;
	  var sendToOption = DCT.Util.getSendToTypeValue();
	  switch (sendToOption){
			case "users":
				Ext.getCmp('userList').assignGridSelection(false, "");  
			break;
			case "entities":
				Ext.getCmp('entitiesList').assignGridSelection(false, "");  
			break;
			case "entityGroup":
				Ext.getCmp('entityGroupList').assignGridSelection(false, "");  
			break;
		}
		if (returnPage != null)
			DCT.Util.getSafeElement('_targetPage').value = returnPage;
		me.submitAction('saveNotification');
	},
	/**	*	*/
	actOnMessage: function (messageID, action) {
		var me = this;		
		var waiting = DCT.ajaxProcess.waitForRequests(me);					
		if (waiting){
			return;
		}
		DCT.Util.getSafeElement('_messageID').value = messageID;
		if (action == 'newNotification') {
			DCT.Util.getSafeElement('_detailMode').value = 'AddNotification';
			me.submitPageAction('messageNew', action);
		}
		else {
			DCT.Util.getSafeElement('_detailMode').value = 'ViewNotification';
			me.submitPageAction('message', action);
		}
	}
});

/*** @class DCT.Util*/
Ext.apply(DCT.Util, {
	/**	*	*/
	checkIfMessageFieldsBlank: function () {
	  var messageType = Ext.getCmp('messageType').getValue();
	  var body = Ext.getCmp('body').getValue();
	  var sendToOption = this.getSendToTypeValue();
	  var gridItemsNotSelected = false;
		if (sendToOption != 'all'){
		    switch (sendToOption){
					case "users":
					  if (Ext.getCmp('userList').getSelectionModel().getCount() == 0)
							gridItemsNotSelected = true;
					break;
					case "entities":
					  if (Ext.getCmp('entitiesList').getSelectionModel().getCount() == 0)
							gridItemsNotSelected = true;
					break;
					case "entityGroup":
					  if (Ext.getCmp('entityGroupList').getSelectionModel().getCount() == 0)
							gridItemsNotSelected = true;
					break;
					default:
						return true;
			}
	  	if ((messageType=='') || (body=='') || gridItemsNotSelected)
	    	return true;
		}else{
	  	if (((messageType == '') || (body == '')) && Ext.getCmp("_allUsersOption").getValue())
	  		return true;
	  }
	  return false;
  },
	/**	* 	*/
	getSendToTypeValue: function () {
	  var selected = Ext.getCmp("entitiesRadioGroup").getValue();
	  if (!Ext.Object.isEmpty(selected))
	     return Ext.Object.getValues(selected)[0];
	}    
});
