/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	*
	*/
	validateSignup: function () {
		var me = this;
		if(!Ext.getCmp('userNameId').isValid() || !Ext.getCmp('emailId').isValid()){
			return(false);
		}
		me.submitPageAction('login','signup');
	}
});