/* 	MixedCollection for all tab groups to store their active tab in.
Each tab group and it's active tab is then added to this collection.
	key: 		tab group uid
	value: 	selected tab index (not id b/c contentEl changes that)
*/
/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	collectionId : 'activeTabs',
	/**
	*
	*/
	getTabsMixedCollection: function (collectionId) { 
		var activeTabsCollection;
		if(!Ext.StoreMgr.containsKey(collectionId)){ 
			activeTabsCollection = Ext.create('Ext.util.MixedCollection');
			Ext.StoreMgr.add(collectionId, activeTabsCollection);
		}
		else {
			activeTabsCollection = Ext.StoreMgr.get(collectionId);
		}
		return activeTabsCollection;
	},
	/**
	*
	*/
	setActiveTab: function (id, currentTab, tabCount) {
		var me = this;
		var coll = me.getTabsMixedCollection(me.collectionId);
		if (coll.containsKey(id)) {
			coll.replace(id, { activeIndex: currentTab, tabCount: tabCount });
		}else{ 
			coll.add(id, { activeIndex: currentTab, tabCount: tabCount });
		} 
	},
	/**
	*
	*/
	getActiveTab: function (id) { 
		var me = this;
		var coll = me.getTabsMixedCollection(me.collectionId); 
		var tabInfo = coll.get(id);
		var index = 0;
		if (!Ext.isEmpty(tabInfo) && !Ext.isEmpty(tabInfo.activeIndex)) {
			index = tabInfo.activeIndex;
		}
		return index;
	},
	/**
	*
	*/
	getTabCount: function (config,ActiveTab) {
		var id=config.renderTo;
		var me = this;
		var coll = me.getTabsMixedCollection(me.collectionId);
		if (!coll.containsKey(id)) {
			coll.add(id,{ activeIndex: ActiveTab, tabCount: config.itemCount-1});
		}
		var tabInfo = coll.get(id);
		var count = 0;
		if (!Ext.isEmpty(tabInfo) && !Ext.isEmpty(tabInfo.tabCount)) {
			count = tabInfo.tabCount;
		}
		return count;
	},
	/**
	*
	*/
	destroyTabs: function () {
		var me = this;
		me.tabComponents.each(function (item) {
			if (!item.isDestroyed)
				item.destroy();
		}, me);
	},
	/**
	*
	*/
	destroyTabHistory: function () {
		var me = this;
		var coll = me.getTabsMixedCollection(me.collectionId);
		if (coll.keys.length === 0) {
			Ext.StoreMgr.removeAtKey(me.collectionId);
		} else {
			coll.eachKey(function (key, item) {
				if (Ext.isEmpty(Ext.get(key))) {
					coll.removeAtKey(key);
				}
			}, me);
		}
	}
});

/**
*
*/
Ext.define('DCT.TabPanel', {
  extend: 'Ext.TabPanel',
  
	resizeTabs : true,
	minTabWidth : 115, 
	tabWidth : 135, 
	enableTabScroll : true, 
	plain : true, //render tab strip w/o background container image
	border : false,
	inputXType: 'dcttabpanel',
	listening: false,
	hideMode: 'offsets',
	xtype: 'dcttabpanel',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
    me.checkControlExists(config);
    me.setListeners(config);
    config.items = [];
    config.itemCount = 0;
    Ext.get(config.renderTo).select(".x-tab", true).each(function (fieldEl) {
    	var me = this;
      config.items.push({
          title: fieldEl.dom.title,
          frame: true,
          listeners: {
              activate: {
                  fn: me.handleActivate,
                  scope: me
              },
              afterrender: {
                  fn: me.assignFieldListener,
                  scope: me
              }
          },
          itemId: 'tab_' + config.itemCount,
          contentEl: fieldEl.id,
          listening: true,
          tabsGroupId: config.renderTo,
          hideMode: 'offsets',
          fields: null
      });
      config.itemCount++;
    }, me);
    me.setActiveItem(config);
    me.callParent([config]);
    me.listening = true;
    DCT.Util.tabComponents.add(me.id, me);  //identifies the tab panels to a global object for other processes to use during Ajax processing
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
        beforetabchange: {
            fn: me.handleBeforeTabChange,
            scope: me
        },
        afterrender: {
            fn: me.setTabClass,
            scope: me
        },
        tabchange: {
            fn: me.setTabClass,
            scope: me
        }
    };
	},
	/**
	*
	*/
	handleBeforeTabChange: function (tabPanel, newTab, currentTab) {
		var me = this;
		if( !me.listening ) { return true; } 
		
		var fields = Ext.DomQuery.jsSelect( "INPUT", currentTab.contentEl.dom ); 
		for( var i = 0; i < fields.length; i ++ ) {
			var fieldCmp = Ext.getCmp(fields[i].getAttribute('data-componentid'));
			if (Ext.isDefined(fieldCmp)){
				if (!fieldCmp.validate()){
					Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: DCT.T("InvalidDataOnPage") }).show();
					return false; 
				}
			}
		} 
		return true; 
	},
	/**
	*
	*/
	handleActivate: function (tab) {
		var me = this; 
		if( !me.listening ) { return; } 
		DCT.Util.setActiveTab( tab.tabsGroupId, tab.getTabIndex(), me.itemCount); 
	},
	/**
	*
	*/
	setActiveItem: function (config) {
		var me = this;
		var activeTab = DCT.Util.getActiveTab(config.renderTo);
		var lastCount = DCT.Util.getTabCount(config,activeTab);
		// check if the current tab was deleted or a new one was added
		if (activeTab > config.itemCount - 1 || (lastCount != 0 && lastCount < config.itemCount)) {
			//it was, reset it to the last tab
			activeTab = ((config.itemCount - 1) < 0) ? 0 : config.itemCount - 1;
		}
		DCT.Util.setActiveTab(config.renderTo, activeTab, config.itemCount);
		config.activeTab = activeTab;
	},
  setTabClass: function (mainTabPanel) {
  	var me = this;
    mainTabPanel.items.each(function (item, index, length) {
     	var me = this;
      me.setInvalidTabIcon(item);
    }, me);
  },
  setInvalidTabIcon: function (tabPanel) {
  	var me = this;
    if (!me.validateTabFields(tabPanel)) {
        tabPanel.setIconCls("x-tab-invalid");
    }
    else {
        tabPanel.setIconCls("");
    }
  },
  assignFieldListener: function (tabPanel) {
  	var me = this;
    var fields = (tabPanel.fields) ? tabPanel.fields : Ext.DomQuery.jsSelect("INPUT", tabPanel.contentEl.dom);
    Ext.each(fields, function (field, index, allItems) {
        var fieldCmp = Ext.getCmp(field.getAttribute('data-componentid'));
        if (Ext.isDefined(fieldCmp)) {
            fieldCmp.addListener('blur', me.tabFieldListener, me);
        }
    }, me);
  },
  validateTabFields: function (tabPanel) {
  	var me = this;
    var fields = (tabPanel.fields) ? tabPanel.fields : Ext.DomQuery.jsSelect("INPUT", tabPanel.contentEl.dom);
    var valid = true;
    Ext.each(fields, function (field, index, allItems) {
        var fieldCmp = Ext.getCmp(field.getAttribute('data-componentid'));
        if (Ext.isDefined(fieldCmp)) {
            if (!fieldCmp.validate()) {
                valid = false;
                return false;
            }
        }
    }, me);
    return valid;
  },

  tabFieldListener: function (fieldToValidate) {
  	var me = this;
    if (!fieldToValidate.isValid()) {
        me.getActiveTab().setIconCls("x-tab-invalid");
    } else {
        me.setInvalidTabIcon(me.getActiveTab());
    }
  }

});
DCT.Util.reg('dcttabpanel', 'tabs', 'DCT.TabPanel');

