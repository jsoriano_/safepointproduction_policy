/**
*
*/
Ext.define('DCT.TreeColumn', {
		extend: 'Ext.tree.Column',
		alias: 'widget.dcttreecolumn',
		cellTpl: [
				'<tpl for="lines">',
				'<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
				'{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>" role="presentation"/>',
				'</tpl>',
				'<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
				'<tpl if="isLast">-end</tpl><tpl if="expandable">-plus {expanderCls}</tpl>" role="presentation"/>',
				'<tpl if="checked !== null">',
				'<input type="button" {ariaCellCheckboxAttr}',
				' class="{childCls} {checkboxCls}<tpl if="checked"> {checkboxCls}-checked</tpl>"/>',
				'</tpl>',
				'<span class="{textCls} {childCls}">{value}</span>'
		]
});
/**
*
*/
Ext.define('DCT.TreeGridPanel', {
	extend: 'Ext.tree.Panel',

	inputXType: 'dcttreegridpanel',
	dctControl: true,
	xtype: 'dcttreegridpanel',
	forceFit: true,
	reserveScrollbar: true,
	lines:false,
	rootVisible:false,
	sortableColumns: false,
	enableColumnMove: false,
	enableColumnHide: false,
	height: 600,
	emptyText: DCT.T('NoResultsToDisplay'),
	

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setModel(config);
		me.setDataStore(config);
		me.setColumnModel(config);
		me.setGridView(config);
		me.setToolbar(config);
		me.setListeners(config);
		config.store.grid = me;
		me.callParent([config]);
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setModel: function (config) {
	},
	
	/**
	*
	*/
	setDataStore: function (config) {
		var me = this;
		config.store = Ext.create(config.dctStoreClass, config.dctStoreConfig);
	},
	/**
	*
	*/
	setColumnModel: function (config) {
	},
	/**
	*
	*/
	setGridView: function (config) {
  	var viewConfig =	{ 
  		cellTpl: [
        '<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} style="width:{column.cellWidth}px;<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
        '<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
				'<tpl if="record.getTreeStore().grid.view.debugModeOn">',
				'columnRef="{[values.record.getTreeStore().grid.view.debugId]}_{column.dataIndex}" ',
				'</tpl>',
        'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>{value}</div>',
        '</td>',
        {
            priority: 0
        }
    	],
	    treeRowTpl: [
	        '{%',
	        'this.processRowValues(values);',
	        'this.nextTpl.applyOut(values, out, parent);',
	        '%}',
	        {
	            priority: 10,
	            processRowValues: function(rowValues) {
	                var record = rowValues.record,
	                    view = rowValues.view;
	                
	                
	                if (view.getRowTips){
	                	view.getRowTips(rowValues);
	                }else{
		                rowValues.rowAttr['data-qtip'] = record.get('qtip') || '';
		                rowValues.rowAttr['data-qtitle'] = record.get('qtitle') || '';
		              }
	                if (record.isExpanded()) {
	                    rowValues.rowClasses.push(view.expandedCls);
	                }
	                if (record.isLeaf()) {
	                    rowValues.rowClasses.push(view.leafCls);
	                }
	                if (record.isLoading()) {
	                    rowValues.rowClasses.push(view.loadingCls);
	                }
	            }
	        }
	    ]
    };
    if (Ext.isDefined(config.dctViewConfig)){
			Ext.apply(config.dctViewConfig, viewConfig);
    }else{
    	config.dctViewConfig = viewConfig;
    }
		config.viewConfig = config.dctViewConfig;
	},
	/**
	*
	*/
	setToolbar: function (config) {
		if (config.dctTreeToolbarClass){
			var toolbarConfig = {
				dctTitle: config.dctTitle,
				dctTreeId: config.id,
				dctDateFormat: config.dctDateFormat,
				dctDefaultStartDate: config.dctDefaultStartDate
			};
			if (config.dctToolbarConfig){
				Ext.apply(toolbarConfig, config.dctToolbarConfig);
			}
			config.tbar = Ext.create(config.dctTreeToolbarClass, toolbarConfig);
		}
	}

});
DCT.Util.reg('dcttreegridpanel', 'treeGridPanel', 'DCT.TreeGridPanel');

/**
*
*/
Ext.define('DCT.TreeStore', {
	extend: 'Ext.data.TreeStore',

	inputXType: 'dcttreetore',
	remoteSort : false,
	remoteFilter: false,
	autoLoad: true,
	autoDestroy: true,
	xtype: 'dcttreetore',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setProxyItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setProxyItems: function(config){
		config.proxy = {
			type: 'ajax',
			actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
			url: DCT.Util.getProxyUrl(),
			reader: {
				type: 'json'
			}  		
		};
	},
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
				beforeload: {
					fn: function(store, operation, eOpts){
						var me = this;
						me.setBeforeLoadParams(store, operation, me.getLocalParams());
					},
					scope: me
				},
				load: {
					fn: function(store, records, successful, operation){
						var me = this;
						if (!successful){
							if (operation.error.response.timedout){
								Ext.getDom('_targetPage').value = 'login';
								DCT.Submit.submitForm();
							}
						}
					},
					scope: me
				}
			};
	},
	/**
	*
	*/
	getLocalParams: function () {
		var me = this;
		var params = {
			_targetPage: me.dctTargetPage,
			_gridRequest: '1'
		}
		return params;
	}
});

/**
*
*/
Ext.define('DCT.BaseTreeToolbar', {
	extend: 'Ext.Toolbar',

		inputXType: 'dctbasetreetoolbar',
		xtype: 'dctbasetreetoolbar',
		id: 'treeToolbar',

		/**
		*
		*/
		constructor: function (config) {
			var me = this;
			me.setToolbarItems(config);
			me.callParent([config]);
		},
		/**
		*
		*/
		setToolbarItems: function (config) {
		},
		/**
		*
		*/
		refreshPage: function (btn, pressed) {
				DCT.Submit.submitBillingPage('installmentSchedule', '');
		},
		/**
		*
		*/
		toggleTree: function (btn, pressed) {
			var me = this;
			if (pressed)
					Ext.getCmp(me.dctTreeId).collapseAll();
			else
					Ext.getCmp(me.dctTreeId).expandAll();
		},
		/**
		*
		*/
		refreshClicked: function (button, eventObj) {
			var me = this;
			Ext.getCmp('expandCollapse').setPressed();
			Ext.getCmp(me.dctTreeId).getStore().load();
		}
});

/**
*
*/
Ext.define('DCT.AgencyInstallmentTreeToolbar', {
	extend: 'DCT.BaseTreeToolbar',

		inputXType: 'dctagencyinstallmenttreetoolbar',
		xtype: 'dctagencyinstallmenttreetoolbar',

	/**
	*
	*/
		setToolbarItems: function (config) {
			var me = this;
			config.items = [
			config.dctTitle,
			'->',
			{
					icon: DCT.imageDir + 'icons\/arrow_left.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('Show Policy Term Summary'),
					id: 'policytermsummary',
					disabled: false,
					handler: me.policyTermSummary,
					scope: me
			},
			' ',
			{
					icon: DCT.imageDir + 'icons\/arrow_redo.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('Refresh Page'),
					id: 'refreshpage',
					disabled: false,
					handler: me.refreshPage,
					scope: me
			},
			' ',
			{
					icon: DCT.imageDir + 'icons\/table_sort.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('ExpandCollapse'),
					id: 'expandCollapse',
					template: Ext.create('Ext.Template', 
										'<table id="{4}" cellspacing="0" class="x-btn {3}"><tbody class="{1}">',
										'<tr><td class="x-btn-tl"><i>&#160;</i></td><td class="x-btn-tc"></td><td class="x-btn-tr"><i>&#160;</i></td></tr>',
										'<tr><td class="x-btn-ml"><i>&#160;</i></td><td class="x-btn-mc"><em class="{2}" unselectable="on"><button id="{4}Button" type="{0}"></button></em></td><td class="x-btn-mr"><i>&#160;</i></td></tr>',
										'<tr><td class="x-btn-bl"><i>&#160;</i></td><td class="x-btn-bc"></td><td class="x-btn-br"><i>&#160;</i></td></tr>',
										'</tbody></table>', { compiled: true }),
					pressed: true,
					enableToggle: true,
					allowDepress: true,
					toggleHandler: me.toggleTree,
					scope: me
			},
			' ',
			{
					xtype: 'dctinstallmentdatefield',
					format: config.dctDateFormat,
					emptyText: DCT.T('StartDate'),
					name: '_startDate',
					id: 'installmentStartDate',
					value: config.dctDefaultStartDate
			},
			' ',
			{
					xtype: 'dctinstallmentdatefield',
					format: config.dctDateFormat,
					emptyText: DCT.T('EndDate'),
					name: '_endDate',
					id: 'installmentEndDate'
			},
			'-',
			{
					icon: DCT.imageDir + 'icons\/arrow_rotate_clockwise.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('Refresh'),
					disabled: false,
					id: 'refresh',
					handler: me.refreshClicked,
					scope: me
			}
		];
		},
		/**
		*
		*/
		policyTermSummary: function (btn, pressed) {
				DCT.Submit.submitBillingActivity('policy', DCT.Util.getSafeElement('_policyId').value);
		}
});

/**
*
*/
Ext.define('DCT.InstallmentTreeToolbar', {
	extend: 'DCT.BaseTreeToolbar',

		inputXType: 'dctinstallmenttreetoolbar',
		xtype: 'dctinstallmenttreetoolbar',

		/**
		*
		*/
		setToolbarItems: function (config) {
			var me = this;
			config.items = [
			config.dctTitle,
			'->',
			{
					icon: DCT.imageDir + 'icons\/table_sort.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('ExpandCollapse'),
					id: 'expandCollapse',
					template: Ext.create('Ext.Template', 
										'<table id="{4}" cellspacing="0" class="x-btn {3}"><tbody class="{1}">',
										'<tr><td class="x-btn-tl"><i>&#160;</i></td><td class="x-btn-tc"></td><td class="x-btn-tr"><i>&#160;</i></td></tr>',
										'<tr><td class="x-btn-ml"><i>&#160;</i></td><td class="x-btn-mc"><em class="{2}" unselectable="on"><button id="{4}Button" type="{0}"></button></em></td><td class="x-btn-mr"><i>&#160;</i></td></tr>',
										'<tr><td class="x-btn-bl"><i>&#160;</i></td><td class="x-btn-bc"></td><td class="x-btn-br"><i>&#160;</i></td></tr>',
										'</tbody></table>', { compiled: true }),
					pressed: true,
					enableToggle: true,
					allowDepress: true,
					toggleHandler: me.toggleTree,
					scope: me
			},
			' ',
			{
					xtype: 'dctinstallmentdatefield',
					format: config.dctDateFormat,
					emptyText: DCT.T('StartDate'),
					name: '_startDate',
					id: 'installmentStartDate',
					value: config.dctDefaultStartDate
			},
			' ',
			{
					xtype: 'dctinstallmentdatefield',
					format: config.dctDateFormat,
					emptyText: DCT.T('EndDate'),
					name: '_endDate',
					id: 'installmentEndDate'
			},
			'-',
			{
					icon: DCT.imageDir + 'icons\/arrow_rotate_clockwise.png',
					iconCls: 'x-btn-icon',
					tooltip: DCT.T('Refresh'),
					disabled: false,
					id: 'refresh',
					handler: me.refreshClicked,
					scope: me
			}];
		}
});