/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	displayPDFAttachment: function (targetPage, attachmentId, moniker, fileName) {
		window.open(DCT.Util.getPostUrl() + "&_submitAction=&_targetPage=" + targetPage + "&_attachmentID=" + attachmentId + "&_moniker=" + moniker + "&_filename=" + fileName + "&mode=popup&_action=", "", "dependent=yes,hotkeys=no,location=no,width=800,height=600,status=no,resizable=yes,toolbar=no,scrollbars=no");
	}
});

/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	*
	*/
	deleteMsgAttachment: function (ID) {
		DCT.Util.getSafeElement('_attachmentID').value = ID;
		this.submitPageAction('message','deleteAttachment');
	},
	/**
	*
	*/
	downloadAttachment: function (ID, returnPage) {
		DCT.Util.getSafeElement('_attachmentID').value = ID;
		if (Ext.isEmpty(returnPage))
			returnPage = 'policy';
		this.submitPageAction(returnPage,'downloadAttachment');
	}	
});