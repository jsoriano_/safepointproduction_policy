/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	*
	*/
	displayCommentPopUp: function (extRecordId, commentTypeCode) {
		var me = this;
		var displayComments = Ext.getCmp('cmtsDisplayPopup');
		if (displayComments){
				return;
		}
		me.removeTabs(true);
		var commentsDisplayPanel = Ext.create('DCT.DisplayComment', {
			dctExtRecordId: extRecordId, 
			dctCommentTypeCode: commentTypeCode,
			dctDataStore : Ext.getCmp('comments').getStore()
			});
		var commentsWindow = Ext.create('DCT.BaseWindow',{
			title: DCT.T('Note'),
			width: 500,
			height:450,
			id: 'cmtsDisplayPopup',
			items: commentsDisplayPanel,
			dctFocusFieldId: '_cmtCancelButton'		
			});
		commentsWindow.show();
	},
	refreshNotes: function () {
			var policyCommentsGrid = Ext.getCmp('policyComments');
			var commentsGrid = Ext.getCmp('comments');
			if (Ext.isDefined(policyCommentsGrid)) {
					DCT.Grid.applyFilterToDataStore(policyCommentsGrid.getStore(), true);
			}
			else if (Ext.isDefined(commentsGrid)) {
					DCT.Grid.applyFilterToDataStore(commentsGrid.getStore(), true);
			}
	}
});

/**
*
*/
Ext.define('DCT.DisplayComment', {
	extend: 'Ext.form.FormPanel',

	inputXType: 'dctdisplaycomment',
	defaultType: 'displayfield',
	bodyBorder: false,
	formId : 'commentsViewForm',
	xtype: 'dctdisplaycomment',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setPanelItems(config);
		me.setPanelButtons(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setPanelItems: function (config) {
		var description = '';
		var isInternalViewText = '';
		var creationInfo = '';
		var objectStatus = '';
		if (config.dctExtRecordId){
			var extRecord = config.dctDataStore.getById(config.dctExtRecordId);
			if (extRecord){
				description = extRecord.data['description'];
			//	description = description.replace(/\n/g, "<br/>");
				var createdBy = extRecord.data['originator'];
				//Comment creation info	
				var creationDateTime = extRecord.data['creationDateTime'];	
				var displayDateFormat = Ext.getDom('_hiddenDisplayDtMask').value;
				var createdOn = Ext.Date.format(creationDateTime, displayDateFormat + ", g:i a");
				creationInfo = DCT.T('Created_by') +  " " + createdBy + " " + DCT.T('On_lowercase') +  " "  + createdOn;
				//Comment object status info
				var policyStatus = extRecord.get('policyStatus');
				var type = extRecord.get('type');
				var transactionStatus = extRecord.get('transactionStatus');	
				objectStatus = DCT.T('TransactionStatus') + ": " + policyStatus + "-" + type +  "-"  + transactionStatus;
				isInternalViewText = DCT.T('CommentIsPublic');
				if (config.dctCommentTypeCode){
					if (config.dctCommentTypeCode == 'CI') {
							isInternalViewText = DCT.T('CommentIsInternal');
					}
				}
			}
		}
		config.items = [{
			anchor: '100% -85',
			hideLabel: true,
			htmlEncode: true,
			value: description,
			name: '_commentView',
			id: '_commentView',
			autoScroll: true
		}, {
			hideLabel: true,
			value: isInternalViewText,
			name: '_viewInternalOnly',
			id: '_viewInternalOnly',
			anchor: '100%',
			autoHeight: true
		},{
			hideLabel: true,
			value: creationInfo,
			name: '_creationInfo',
			id: '_creationInfo',
			anchor: '100%',
			autoHeight: true
		},{
			hideLabel: true,
			value: objectStatus,
			name: '_objectStatus',
			id: '_objectStatus',
			anchor: '100%',
			autoHeight: true
		}];
	},
	/**
	*
	*/
	setPanelButtons: function (config) {
		var me = this;
		config.buttons = [{
			text: DCT.T('Cancel'),
			id: '_cmtCancelButton',
			handler: function(){me.ownerCt.closeWindow();},
			scope: this
		}];
	}
});

/**
*
*/
Ext.define('DCT.AddComment', {
	extend: 'Ext.form.FormPanel',

	inputXType: 'dctaddcomment',
	defaultType: 'textfield',
	labelWidth: 55,
	bodyBorder: false,
	formId : 'commentsForm',
	saveButton: '_saveAddButton',
	cancelButton: '_cancelAddButton',
	xtype: 'dctaddcomment',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setPanelItems(config);
		me.setPanelButtons(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setPanelItems: function (config) {
	},
	/**
	*
	*/
	setPanelButtons: function (config) {
		var me = this;
		config.buttons = [{
			text: DCT.T('Cancel'),
			id: me.cancelButton,
			handler: function(){me.ownerCt.closeWindow();},
			scope: this
		},{
			text: DCT.T('Save'),
			id: me.saveButton,
			disabled: true,
			handler: function(){me.saveAdd();},
			scope: this
		}];
	},
	/** 
	*
	*/
	setAddButtonState: function(value){
		var me = this;
		if (Ext.isEmpty(value))
			me.disableAddSave();
		else
			me.enableAddSave();
	},
	/**
	*
	*/
	enableAddSave: function(){
		var me = this;
		Ext.getCmp(me.saveButton).enable();
	},
	/**
	*
	*/
	disableAddSave: function(){
		var me = this;
		Ext.getCmp(me.saveButton).disable();
	},
	/**
	*
	*/
	saveAdd: function(){
	}  
});

/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
	/**
	*
	*/
	commentDeleteColumnRenderer: function (returnPage, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var rowActionsHTML = '';
		var canDelete = extRecord.get('deleteAccess');
		var objectTypeCode = extRecord.get('objectTypeCode');
		if (canDelete == '1' && objectTypeCode == 'HIST' && DCT.IsReadOnly == false)
			rowActionsHTML = '<a href="javascript:;" id="commentsDelete" onclick="DCT.Grid.deleteComment(\'' + extRecord.id + '\',\'' + extRecord.get('commentLockingTS')  + '\',\'' + returnPage + '\', DCT.Grid.completeDeleteComment);"><img alt="Delete" title="'+ DCT.T('Delete')+'" src="' + DCT.imageDir + '\/icons\/decline.png"/></a>';
		return rowActionsHTML;
	},
	/**
	*
	*/
	commentViewColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var rowActionsHTML = '<a href="javascript:;" id="viewPASComment" onclick="DCT.Util.displayCommentPopUp(\'' + extRecord.id + '\',\'' + extRecord.get('commentTypeCode') + '\');"><img alt="View" title="'+ DCT.T('View')+'" src="' + DCT.imageDir + '\/icons\/magnifier.png"/></a>';
		return rowActionsHTML;
	},
	/**	
	*
	*/
	commentObjectColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var objectId = extRecord.get('objectId');
		if (Ext.isEmpty(objectId))
			rowActionsHTML = "(none)";
		else
			rowActionsHTML = '<a href="javascript:;" onclick="DCT.Submit.gotoPageForQuote(\'policy\',\'' + objectId + '\');">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return rowActionsHTML;
	},
	/**
	*
	*/
	objectStatusColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) {
		var statusDisplay = '';
		var commentCode =  extRecord.get('commentCode');
		var policyStatus = extRecord.get('policyStatus');
		var type = extRecord.get('type');
		var transactionStatus = extRecord.get('transactionStatus');
		var displayStatus = "";
		var qtipText = "";
	
		if(commentCode =="PASC"){
			qtipText = DCT.T('PolicyStatus') + ": " + policyStatus + ", " + DCT.T('Type') + ": " + type + ", " + DCT.T('Status') + ": " + transactionStatus;
			displayStatus = policyStatus + "-" + type +  "-"  + transactionStatus;	
					statusDisplay =  Ext.String.format('<span data-qtip="{0}">{1}</span>', qtipText, displayStatus);
		}else
			statusDisplay = DCT.T('None_parentheses');
		return statusDisplay;
	},
	/**
	*
	*/
	deleteComment: function (commentId, commentLockingTS, returnPage, completeFunction) {
		DCT.Submit.actionVerify(
			DCT.T("AreYouSureDeleteComment"),
			function(btn) { completeFunction(btn, commentId, commentLockingTS, returnPage);},
			false);
	},
	/**
	*
	*/
	completeDeleteComment: function (btn, commentId, commentLockingTS, returnPage) {
		if (btn != 'no'){ 
			var deleteAction;
			DCT.Util.getSafeElement('_gridAction').value = 'deletePolicyComment';
			DCT.Util.getSafeElement('_returnPage').value = returnPage; 
			DCT.Util.getSafeElement('_submitAction').value = 'deletePolicyComment'; 
			DCT.Util.getSafeElement('_commentId').value = commentId;
			DCT.Util.getSafeElement('_commentLockingTS').value = commentLockingTS;
			DCT.Grid.applyFilterToDataStore(Ext.getCmp('comments').getStore(), true, DCT.Grid.resetCommentsVariables);
		}
	},
	/**
	*
	*/
	resetCommentsVariables: function () {		
		DCT.Util.getSafeElement('_commentId').value = 0; //reset
	}
});


/**
* Notes grids for Policy Details, Notes pages
*/
Ext.define('DCT.NotesPagingGridPanel', {
	extend: 'DCT.PagingGridPanel',

	inputXType: 'dctnotespaginggridpanel',
	xtype: 'dctnotespaginggridpanel',

/**
* 
*/
	setHubListeners: function (config) {
		DCT.hubEvents.addListener('refreshnotes', 'DCT.Util.refreshNotes');
		config.store.on('load', function () {
				DCT.hubEvents.fireEvent('refreshnotestoolbar');
		}); 
	}

});
DCT.Util.reg('dctnotespaginggridpanel', 'notesPagingGridPanel', 'DCT.NotesPagingGridPanel');


