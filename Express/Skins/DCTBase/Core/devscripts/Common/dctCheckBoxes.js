
/****/
Ext.define('DCT.CheckBoxGroup', {
  extend: 'Ext.form.CheckboxGroup',

	hideLabel: true,
	blankText: DCT.T("FieldRequired"),
	inputXType: 'dctcheckboxgroup',
	dctControl: true,
	xtype: 'dctcheckboxgroup',

	/**	*	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setInitialValue(config);
		me.setCheckboxItems(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
		if (DCT.ajaxProcess.chkboxChanged)
			me.validate();
	},
	/**
	*
	*/
	setInitialValue: function (config) {
		var me = this;
		config.value = null;
	},
	/**	*	*/
	setCheckboxItems: function (config) {
		var me = this;
		config.items = [];
		Ext.each(config.dctCheckBoxes,function (itemConfig) {
			var xtypeName = (Ext.isEmpty(itemConfig.dctClassName)) ? 'dctcheckboxfield' : itemConfig.dctClassName;
			Ext.apply(itemConfig, {xtype: xtypeName});
			config.items.push(itemConfig);
		}, me);
	}		  
});
DCT.Util.reg('dctcheckboxgroup', 'interviewCheckBoxGroup', 'DCT.CheckBoxGroup');


/****/
Ext.define('DCT.CheckBoxField', {
  extend: 'Ext.form.Checkbox',

	enableKeyEvents: true,
	inputXType: 'dctcheckboxfield',
	uncheckedValue: '0',
	xtype: 'dctcheckboxfield',

	/**	*	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**	*	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
			change:{
				fn: function(control, newValue, oldValue, eOpts) {
					me.fieldChanged();
					me.fieldValidation();
				},
				scope:me
			},
			focus:{
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope:me
			},
			boxready: {
				fn: me.setUpChangeEvent,
				scope: me
			},
			specialkey: {
				fn: function (f, e) {
					var me = this;
					switch (e.getKey()){
						case e.TAB:
							DCT.Util.setKeysUsed(true, e.shiftKey);
						break;
					}
				},
				scope: me
			}
		};
	},
	/**	*	*/
	setAutoCreate: function (config) {
		var attributeTemplate = 'name="' + config.name + '" ';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.inputAttrTpl = attributeTemplate;
		var checkedValue = (config.checked ? "1" : "0");
		config.afterBodyEl = '<input hidden id="' + config.id + '-hiddenCheckBoxEl" data-ref="hiddenCheckBoxEl" role="checkbox" data-cmpid="' + config.id + '" ' + attributeTemplate + ' autocomplete="off" hidefocus="true" value="' + checkedValue + '" />';		
	},
	/**	*	*/
	setDefaultListeners: function (config) {
	},
	/**
	*
	*/
	setHiddenElement: function () {
		var me = this;
		var hiddenField = Ext.getDom(me.id + '-hiddenCheckBoxEl');
		if (!Ext.isEmpty(hiddenField)){
			hiddenField.value = (me.getValue() ? "1" : "0");
		}
	},
	/**
	*
	*/
	setUpChangeEvent: function () {
		var me = this;
		me.on("change", me.setHiddenElement, me);
	}
});
