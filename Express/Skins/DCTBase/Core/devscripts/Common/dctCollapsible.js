/**
*
*/
Ext.define('DCT.CollapsibleGroupPanel', {
  extend: 'Ext.Panel',

	layout: 'auto',
	border: false,
	dctControl: true,
	inputXType: 'dctcollapsiblegrouppanel',
	xtype: 'dctcollapsiblegrouppanel',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setPanelItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setPanelItems: function (config) {
		var me = this;
		me.items = [];
		Ext.get(config.renderTo).select(".collapsibleGroupItem", true).each(function (fieldEl, compEl, itemIndex) {
			var panelItem = {xtype: 'dctcollapsiblegroupitem'};
			var itemConfig = fieldEl.getDataPrefixedAttributes();
			Ext.apply(panelItem, itemConfig);
			me.items.push(panelItem);
		}, me);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender:{
				fn: function(panel) {
					Ext.get(me.renderTo).removeCls('x-hidden');
				},
				scope:me
			}
		};
	}		  
});
DCT.Util.reg('dctcollapsiblegrouppanel', 'collapsibleGroupPanel', 'DCT.CollapsibleGroupPanel');


/**
*
*/
Ext.define('DCT.CollapsibleGroupItem', {
  extend: 'Ext.Panel',

	margins:'0 0 0 0', 
	animCollapse: true,
	collapsible: true,
	titleCollapse: true,
	hideMode:'offsets', 
	inputXType: 'dctcollapsiblegroupitem',
	xtype: 'dctcollapsiblegroupitem',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setListeners: function (config) {
	}		  
});

/**
*
*/
Ext.define('DCT.CollapsiblePanel', {
  extend: 'Ext.Panel',

	header: false,
	layout:'fit',
	animCollapse: true,
	collapsible: false,
	border: false,
	collapsed:true,
	hideMode:'offsets', 
	dctControl: true,
	inputXType: 'dctcollapsiblepanel',
	xtype: 'dctcollapsiblepanel',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.callParent([config]);
	}		  
});
DCT.Util.reg('dctcollapsiblepanel', 'collapsiblePanel', 'DCT.CollapsiblePanel');

/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	toggleSection: function(sectionId) {
		var me = this;
		var section = Ext.getCmp(sectionId);
		Ext.get(section.contentEl).select('div[data-cmpid]').each(function(field) {
			var dctComponentId = field.getAttribute('data-cmpid');
			dctComponentId = (dctComponentId) ? dctComponentId : field.dom.getAttribute('data-cmpid');
			var dctCmp = Ext.getCmp(dctComponentId);
			dctCmp.setDisabled(!section.collapsed);
			dctCmp.validate();
		}, me);
		section.toggleCollapse(false);
	},
	/**
	*
	*/
	toggleInterviewSection: function(sectionId) {
		var section = Ext.get(sectionId);
		if (!Ext.isEmpty(section)) {
			var sectionHeader = section.prev('[id*=_toggleHeader]') || section.parent().prev('[id*=_toggleHeader]');
			if (section.isVisible()) {

				if (sectionHeader) {
					sectionHeader.addCls('toggle-collapsed');
				}
				section.slideOut('t', {
					useDisplay: false,
					callback: function () {
						DCT.Sections.process(sectionId, true);
						DCT.Sections.collapseChildren(section);
						section.addCls('x-hidden');
					}
				});
				
			} else {
				
				section.removeCls('x-hidden');
				if (sectionHeader) {
					sectionHeader.removeCls('toggle-collapsed');
				}
				section.slideIn('t', {
					callback: function () {
						DCT.Sections.process(sectionId, false);
						DCT.Sections.expandChildren(section);
					}
				});
				
			}
		}
	}
});

/**
 * @class DCT.Sections
 * This class handles collapsible sections for interview pages.  Makes sure they are collapsed or expanded if entire page is redisplayed
 */
Ext.apply(DCT.Sections, {
	items: Ext.create('Ext.util.MixedCollection', {}),
	collapsed: false,
	savedState: Ext.create('Ext.util.MixedCollection', {}),

	/**
	*
	*/
	process: function (sectionId, collapse) {
		var me = this;
		var section = me.get(sectionId);
		me.setCollapsed(section, collapse);
	},
	/**
	*
	*/
	add: function (config) {
		var me = this;
 		if (!(me.items.containsKey(config.id)))
 			me.items.add(config.id, Ext.create('DCT.Section', config));
	},
  	/**
	*
	*/
  	get: function (id) {
  		var me = this;
		return me.items.get(id);
	},
	/**
	*
	*/
	set: function (id, section) {
		var me = this;
		if (me.items.containsKey(id))
    		me.items.replace(id, section);
	},
	/**
	*
	*/
	setCollapsed: function (section, collapse) {
		var me = this;
		section.collapsed = collapse;
		me.set(section.id, section);
	},
	/**
	*
	*/
	findAllSections: function () {
		var me = this;
		Ext.select(".collapsibleSection", true).each(function(section){
			var config, inputField;
			config = section.getDataPrefixedAttributes();
			me.add(config);
		}, me);
	},
	/**
	* 
	*/
	backupCollapsedStates: function() {
		var me = this;
		me.items.each(function (item) {
			if (DCT.Sections.savedState.containsKey(item.id)) {
				DCT.Sections.savedState.replace(item.id, item.collapsed);
			} else {
				DCT.Sections.savedState.add(item.id, item.collapsed);
			}
		});
	},
	/**
	* 
	*/
	setSectionStates: function() {
		var arrStates = new Array();
		var states;

		this.items.each(function (item) {
			arrStates.push('"' + item.id + ':' + item.collapsed + '"');
		}); 
		states = '[' + arrStates.toString() + ']';
		DCT.Util.getSafeElement('_sectionStates').value = states;
	},
	/**
	* 
	*/
	restoreCollapsedStates: function () {
		var me = this;
		var needsRefresh = false;
		me.items.each(function (item) {
			if (DCT.Sections.savedState.containsKey(item.id)) {
				var oldCollapsed = DCT.Sections.savedState.get(item.id);
				var newCollapsed = item.collapsed;
				if (oldCollapsed != newCollapsed) {
					item.collapsed = oldCollapsed;
					needsRefresh = true;
				}
			}
		});
		if (needsRefresh) {
			me.setSections();
		}
	},
	/**
	*
	*/
	restoreToSavedStates: function (sectionStates) {
	    var needsRefresh = false;
	    var array = "";
	    if (sectionStates != null && sectionStates.length > 2) {
	        array = eval("(" + sectionStates + ")");
	    }
	    for (var i = 0; i < array.length; i++) {
	        var pair = array[i].split(":");
	        var item = this.items.get(pair[0]);
	        if (item) {
	            var savedState = (pair[1] === 'true');
	            var currentState = item.collapsed ? true : false;
	            if (currentState != savedState) {
	                item.collapsed = savedState;
	                needsRefresh = true;
	            }
	        }
	    }
	    if (needsRefresh) {
	        this.setSections();
	    }
	},
	/**
	*
	*/
	setSections: function () {
		var me = this;
		me.items.each(function(item){
			var sectionElement = Ext.get(item.id);
			if (sectionElement) {
				var sectionHeader = sectionElement.prev('[id*=_toggleHeader]');
				if (sectionElement.isVisible() && item.collapsed){
					sectionElement.addCls('x-hidden');
					if (sectionHeader)
						sectionHeader.addCls('toggle-collapsed');
				}
				else if (!sectionElement.isVisible() && !item.collapsed){
					sectionElement.removeCls('x-hidden');
					if (sectionHeader)
						sectionHeader.removeCls('toggle-collapsed');
						sectionElement.setDisplayed(true);
					}
				}
		});
	},
  /**
  *
  */
  initialize: function(){
	var sectionStates = DCT.Util.getSafeElement('_sectionStates').value;
  	this.findAllSections();
  	this.setSections();
	if (this.savedState.getCount()>0) {
		this.restoreCollapsedStates();
		}
	else if (sectionStates != "") {
		this.restoreToSavedStates(sectionStates);
	}
  },
    /**
	*
	*/
	toggle: function () {
		var me = this;
		var itemsCollapsed = me.items.filter('collapsed', ((me.collapsed)? 'true' : 'false'), true, true);
		itemsCollapsed.each(function(item){
			me.process(item.id, !me.collapsed);
			DCT.Util.toggleInterviewSection(item.id);
		}, me);
		me.collapsed = (me.collapsed) ? false : true;
	},
	/**
	*
	*/
	toggleAll: function (collapse) {
		var itemsCollapsed = this.items.filter('collapsed', !collapse, true, true);
		itemsCollapsed.each(function(item){
			this.process(item.id, collapse);
		}, this);
		this.collapsed = collapse;
	},	
	/**
	*
	*/
	setToggleButton: function () {
		var collapsibleSections = Ext.query('div.toggleHeader').length;
		if (collapsibleSections > 0)
			Ext.get('togglePanelButton').setDisplayed(true);
		else
			Ext.get('togglePanelButton').setDisplayed(false);
	},
	/**
	*
	*/
	expandForInvalidFields: function () {
		var me = this;
		if (me.items.getCount() > 0){
			me.items.each(function (item){
				var me = this;
				if (item.collapsed){
					var fieldArray = Ext.query(".controlContainer", true, Ext.getDom(item.id));
					Ext.each(fieldArray, function (field, index) {
						var compId = field.getAttribute("data-cmpid");
						var extComp = Ext.getCmp(compId);
						if (extComp) {
							if (!extComp.isValid()){
								DCT.Util.toggleInterviewSection(item.id);
								return false;
							}
						}
					}, me);
				}				
			}, me);
		}
	},

	expandChildren: function (section) {

		var expChildren = section.query('[id*=toggleItem]');
		for (var childIndex = 0; childIndex < expChildren.length; childIndex++) {
			var curExpand = Ext.get(expChildren[childIndex].id);

			// only expand directly nested children
			var toggleParent = curExpand.up('[id*=toggleItem]');
			if (toggleParent.id === section.id) {

				var sectionHeader = curExpand.prev('[id*=_toggleHeader]');
				if (sectionHeader && !sectionHeader.hasCls('toggle-collapsed')) {
					curExpand.removeCls('x-hidden');
					curExpand.slideIn('t', {
						callback: function (childId, expansionChild) {
							return function () {
								
								DCT.Sections.process(childId, false);
								DCT.Sections.expandChildren(expansionChild);

							};
						}(expChildren[childIndex].id, curExpand)
					});
				}
			}
		}
	},

	collapseChildren: function (section) {

		var collChildren = section.query('[id*=toggleItem]');
		for (var childIndex = 0; childIndex < collChildren.length; childIndex++) {
			var curCollapse = Ext.get(collChildren[childIndex].id);

			// only collapse directly nested children
			var toggleParent = curCollapse.up('[id*=toggleItem]');
			if (toggleParent.id === section.id) {

				var sectionHeader = curCollapse.prev('[id*=_toggleHeader]');
				if (sectionHeader) {

					curCollapse.slideOut('t', {
						useDisplay: false,
						callback: function(collapse, collapseChildIndex) {
							return function() {
								collapse.addCls('x-hidden');
								DCT.Sections.process(collChildren[collapseChildIndex].id, true);
								DCT.Sections.collapseChildren(collapse);
							}
						}(curCollapse, childIndex)
					});
				}
			}
		}
	}
});
/**
 * @class DCT.Section
 * This class is a html section on a interview page.
 */
Ext.define('DCT.Section', {
	collapsed: false,
	id: '',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.collapsed = config.collapsed;
		me.id = config.id;
		me.callParent([config])
	}
});

