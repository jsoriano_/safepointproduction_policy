/**
* 
*/
Ext.define('DCT.InputField', {
  extend: 'Ext.form.TextField',
  
    inputXType: 'dctinputfield',
    formatter: undefined,
    dctControl: true,
    dctFormatBeforePost: false,
    xtype: 'dctinputfield',

	/**
	* 
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.setFormatter(config);
		me.setVType(config);
		me.setRegEx(config);
		me.setFormatMask(config);
		me.setValidator(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},

	/**
	* 
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			specialkey: function (field, e) {
				if (e.getKey() == e.ENTER) {
					return false;
				}
			},
			blur: {
				fn: function (field) {
					me.fieldValidation();
					me.formatField();
				},
				scope: me
			},
			change: {
				fn: me.fieldChanged,
				scope: me
			},
			focus: {
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope: me
			},
			keydown: {
				fn: function (f, e) {
					switch (e.getKey()) {
						case e.TAB:
							DCT.Util.setKeysUsed(true, e.shiftKey);
							break;
					}
				},
				scope: me
			}
		};
	},

	/**
	* 
	*/
	setAutoCreate: function (config) {
		var controlSize = (Ext.isDefined(config.size)) ? config.size : 20;
		config.width = controlSize * 6.5 + 20;
		config.size = 0;
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		if (Ext.isDefined(config.maxLength)) {
			config.enforceMaxLength = true; 
		}
		config.inputAttrTpl = attributeTemplate;
	},

    /**
    * 
    */
    setFormatMask: function (config) {
    },

    /**
    * 
    */
    setValidator: function (config) {
    },

    /**
    * 
    */
    setRegEx: function (config) {
        config.regex = (Ext.isDefined(config.dctRegEx)) ? (Ext.isEmpty(config.dctRegExAttribs) ? new RegExp(config.dctRegEx) : new RegExp(config.dctRegEx, config.dctRegExAttribs)) : null;
        config.regexText = (Ext.isDefined(config.dctRegExMsg)) ? config.dctRegExMsg : '';
    },

    /**
    * 
    */
    setVType: function (config) {
    },

    /**
    * 
    */
    setFormatter: function (config) {
    },

	/**
	* 
	*/
	formatField: function (skipValidate) {
		var me = this;
		if (Ext.isDefined(me.formatter)) {
			var fieldValue = me.getValue();
			if ((skipValidate || (me.isValid()) && me.isDirty())) {
				if (!Ext.isEmpty(fieldValue)) {
					me.setRawValue(me.formatter(fieldValue));
				}
			}
		}
	},

	/**
	* 
	*/
	formatPhone: function (fieldValue) {
		if (!Ext.isEmpty(fieldValue)) {
			var output = new String("");
			var i = 0;
			for (i = 0; (i < fieldValue.length); i++) {
				if (fieldValue.charCodeAt(i) > 47 && fieldValue.charCodeAt(i) < 58) {
					output = output + fieldValue.charAt(i);
				}
			}
			return (output.substr(0, 3) + "-" + output.substr(3, 3) + "-" + output.substr(6, 4));
		}
	},
	/**
	* 
	*/
	formatZipUk: function (fieldValue) {
			if (!Ext.isEmpty(fieldValue)) {
			    if (!(/\s/.test(fieldValue))) {
			        if (fieldValue.length == 5)
			            return (fieldValue.substr(0, 2) + " " + fieldValue.substr(2, 3));
					if (fieldValue.length == 6)
						return (fieldValue.substr(0, 3) + " " + fieldValue.substr(3, 3));
					else if (fieldValue.length == 7)
						return (fieldValue.substr(0, 4) + " " + fieldValue.substr(4, 3));
				}
				else {
					return (fieldValue.replace(/\s+/g, ' '));
				}
			}
	},

    /**
    * 
    */
    formatSSN: function (fieldValue) {
        if (!Ext.isEmpty(fieldValue)) {
            var output = "";
            for (i = 0; (i < fieldValue.length); i++) {
                if (fieldValue.charCodeAt(i) > 47 && fieldValue.charCodeAt(i) < 58) {
                    output = output + fieldValue.charAt(i);
                }
            }
            return (output.substr(0, 3) + "-" + output.substr(3, 2) + "-" + output.substr(5, 4));
        }
    },

    /**
    * 
    */
    formatUpper: function (fieldValue) {
        if (!Ext.isEmpty(fieldValue)) {
            return (fieldValue.toUpperCase());
        }
    },

    /**
    * 
    */
    formatLower: function (fieldValue) {
        if (!Ext.isEmpty(fieldValue)) {
            return (fieldValue.toLowerCase());
        }
    },

    /**
    * 
    */
    formatString: function (fieldValue) {
		var me = this;
        if (!Ext.isEmpty(fieldValue)) {
            var missingCharacter = '';
            var addLiterals = false;
            var upperMode = 0;
            var lowerMode = 0;

			if ((Ext.isEmpty(me.dctFormatMask)) || (Ext.isDefined(me.dctFormatMask) && me.dctFormatMask.charAt(0) != "@"))
                return fieldValue;

			var mask = me.dctFormatMask;
            var formatMaskParts = mask.split(';');
            mask = formatMaskParts[0].substring(1);

            if (formatMaskParts.length > 1) {
                addLiterals = formatMaskParts[1] == '0';
            }
            if (formatMaskParts.length > 2) {
                missingCharacter = formatMaskParts[2];
            }

			var regex = me.createStringFormatRegex(mask);
            var regexp = new RegExp(regex);
            var test = fieldValue;

            test = test.split(missingCharacter).join('');
            if (!regexp.test(test)) {
                return fieldValue;
            }
            fieldValue = test;
            var loopLength = mask.length;
            var counter = 0;
            for (var i2 = 0; i2 < loopLength; i2++) {
                if (((mask.charAt(i2)) != ">") && ((mask.charAt(i2)) != "<") && ((mask.charAt(i2)) != "^")) {
                    counter++;
                }
            }
            var matches = fieldValue.match(regexp).slice(1);

            var matchIndex = 0;
            var result = '';

            for (var i = 0; i < mask.length; i++) {
                switch (mask.charAt(i)) {
                    case ">":
                        {
                            upperMode = 1;
                            lowerMode = 0;
                        }
                        break;
                    case "<":
                        {
                            if (mask.charAt(i + 1) == ">") {
                                lowerMode = 0;
                                upperMode = 0;
                                i++;
                            }
                            else {
                                lowerMode = 1;
                                upperMode = 0;
                            }
                        }
                        break;
                    case "C":
                    case "c":
                        if (matchIndex < matches.length && !Ext.isEmpty(matches[matchIndex])) {
                            var value = matches[matchIndex++];
                            if (mask.charAt(i) == 'c') {
                                value = value.toLowerCase();
                            }
                            else {
                                value = value.toUpperCase();
                            }
                            result += value;
                        }
                        else {
                            if (Ext.isEmpty(matches[matchIndex])) {
								result = me.addOneChar(result, matchIndex, missingCharacter);
                            }
                        }
                        break;
                    case "^":
                        {
                            if (mask.charAt(++i) != matches[matchIndex]) {
								result = me.addOneChar(result, matchIndex++, addLiterals ? mask.charAt(i) : missingCharacter);
                            }
                            else if (matchIndex < counter) {
                                result += matches[matchIndex++];
                            }
                            break;
                        }
                    default:
                        {
                            if (Ext.isEmpty(matches[matchIndex])) {
								result = me.addOneChar(result, matchIndex++, missingCharacter);
                            }
                            else if (matchIndex < counter) {
                                if (upperMode == 1) {
                                    result += matches[matchIndex++].toUpperCase();
                                }
                                else if (lowerMode == 1) {
                                    result += matches[matchIndex++].toLowerCase();
                                }
                                else {
                                    result += matches[matchIndex++];
                                }
                            }
                        }
                        break;
                }
            }
        }
        return result;
    },

    /**
    * 
    */
    createStringFormatRegex: function (mask) {
		var me = this;
        var result = "^";
        for (var i = 0; i < mask.length; i++) {
            switch (mask.charAt(i)) {
                case "<":
                    if (mask.charAt(i + 1) == ">") {
                        i++;
                    }
                    break;
                case "^":
                    //literal character properly escaped BY regex FOR regex :)
                    result += '([' + mask.charAt(++i).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") + ']?)';
                    break;
                default:
                    if (mask.charAt(i) != '>') {
						result += me.createRegexPart(mask.charAt(i), !/\^-|\^+/.test(mask));
                    }
                    break;
            }

        }
        return result + '$';
    },

    /**
    * 
    */
    createRegexPart: function (maskType, numbersIncludeMathChars) {
        var alphabetic = '([A-Za-z]?)';
        var numeric = '([\\d]?)';
        var alphaNumeric = '([A-Za-z0-9]?)';
        var numberChar = numbersIncludeMathChars ? '([\\d+-]?)' : '([\\d]?)';
        var space = '(\\s?)';
        var anything = '(.?)';

        switch (maskType) {
            case "A":
                {
                    return alphaNumeric;
                }
                break;
            case "L":
            case "C":
            case "c":
            case "a":
                {
                    return alphabetic;
                }
                break;
            case "0":
                {
                    return numeric;
                }
                break;
            case "9":
            case "#":
                {
                    return numberChar;
                }
                break;
            case "-":
                {
                    return space;
                }
                break;
            case "*":
                {
                    return anything;
                }
                break;
            default:
                return "";
        }
    },

    /**
    * 
    */
    checkString: function (fieldValue) {
		var me = this;
		if (Ext.isDefined(me.formatter) && !me.hasFocus) {
			me.formatField(true);
			fieldValue = me.getRawValue();
        }

		if (!Ext.isEmpty(fieldValue) && !Ext.isEmpty(me.dctFormatMask) && (me.dctFormatMask.charAt(0) == "@")) {
            var missingCharacter = '';
			var formatMaskParts = me.dctFormatMask.split(';');
            if (formatMaskParts.length > 2) {
                missingCharacter = formatMaskParts[2];
            }
            var newString = '';
            var valueIndex = 0;
            var upperMode = 0;
            var lowerMode = 0;
            var loopLength;
            var counter = 0;
            var specificCharacter = '';
            var characterPosition = 0

            //net 0 = 1 less for @, One added back for index 0"
			loopLength = me.dctFormatMask.split(';')[0].length;

            for (var i2 = 1; i2 < loopLength; i2++) {
				if (((me.dctFormatMask.charAt(i2)) != ">") && ((me.dctFormatMask.charAt(i2)) != "<") && ((me.dctFormatMask.charAt(i2)) != "^")) {
                    counter++;
                }
            }

            for (var i = 1; i < loopLength; i++) {
				switch (me.dctFormatMask.charAt(i)) {
                    case ">":
                        {
                            upperMode = 1;
                            lowerMode = 0;
                            i++;
                        }
                        break;
                    case "<":
                        {
							if (me.dctFormatMask.charAt(i + 1) == ">") {
                                lowerMode = 0;
                                upperMode = 0;
                                i++;
                            }
                            else {
                                lowerMode = 1;
                                upperMode = 0;
                            }
                            i++;
                        }
                        break;
                    case "^":
                        {
                            i++;
							specificCharacter = me.dctFormatMask.charAt(i);
                            if (specificCharacter != (fieldValue.charAt(valueIndex))) {
                                characterPosition = valueIndex + 1;
                                return DCT.T("SpecificCharacterInvalidEntry", { character: specificCharacter, position: characterPosition });
                            }
                        }
                        break;
                    case ";":
                        {
                            return true;
                        }
                        break;
                }
				if (me.dctFormatMask.charAt(i) != '^' && (me.dctFormatMask.charAt(i - 1) != '^' || me.dctFormatMask.charAt(i) != fieldValue.charAt(valueIndex))) {
					newString = me.validateChar(fieldValue, valueIndex, me.dctFormatMask, i, upperMode, lowerMode);
                    if (newString != '') {
                        fieldValue = newString;
                        if (valueIndex > counter && i < loopLength) {
                            return DCT.T("EntryTooShort");
                        }
                    }
                    else if (fieldValue.length < counter) {
                        return DCT.T("EntryTooShort");
                    }
                    else {
                        return DCT.T("EntryInvalidFormat", {
							mask: me.dctFormatMask.charAt(i),
                            character: fieldValue.charAt(valueIndex)
                        });
                    }
                }
                valueIndex++;
            }

            fieldValue = newString;

            if (fieldValue.length != counter) {
                return DCT.T("EntryInvalidLength", { expected: counter.toString(), actual: fieldValue.length });
            }
        }
        return true;
    },

    /**
    * 
    */
    validateChar: function (entryString, entryIndex, mask, maskIndex, upperMode, lowerMode) {
		var me = this;
        var ValidUpperChar = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var ValidLowerChar = 'abcdefghijklmnopqrstuvwxyz';
        var ValidNumChar = '0123456789';
        var ValidNumTypeChar = '+-';
        var ValidSpaceChar = ' ';

        switch (mask.charAt(maskIndex)) {
            case "L":
                {
                    if ((ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0)
					|| (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0))
                        return entryString;
                    else
                        return "";
                }
                break;
            case "A":
                {
                    if ((ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0)
					|| (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0)
					|| (ValidNumChar.indexOf(entryString.charAt(entryIndex)) >= 0))
                        return entryString;
                    else
                        return "";
                }
                break;
            case "a":
                {
                    if (upperMode)
                        if (ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0)
                            return entryString;
                        else
                            if (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0) {
								entryString = me.replaceOneCharCase(entryString, entryIndex, 1);
                                return entryString;
                            }

                    if (lowerMode)
                        if (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0)
                            return entryString;
                        else
                            if (ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0) {
								entryString = me.replaceOneCharCase(entryString, entryIndex, 0);
                                return entryString;
                            }

                    if ((ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0)
				   || (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0))
                        return entryString;
                    else
                        return "";
                }
                break;
            case "C":
                {
                    if (ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0)
                        return entryString;
                    else
                        if (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0) {
							entryString = me.replaceOneCharCase(entryString, entryIndex, 1);
                            return entryString;
                        }

                    return "";
                }
                break;
            case "c":
                {
                    if (ValidLowerChar.indexOf(entryString.charAt(entryIndex)) >= 0)
                        return entryString;
                    else
                        if (ValidUpperChar.indexOf(entryString.charAt(entryIndex)) >= 0) {
							entryString = me.replaceOneCharCase(entryString, entryIndex, 0);
                            return entryString;
                        }

                    return "";
                }
                break;
            case "0":
                {
                    if (ValidNumChar.indexOf(entryString.charAt(entryIndex)) >= 0)
                        return entryString;
                    else
                        return "";
                }
                break;
            case "9":
            case "#":
                {
                    if ((ValidNumChar.indexOf(entryString.charAt(entryIndex)) >= 0)
					|| (ValidNumTypeChar.indexOf(entryString.charAt(entryIndex)) >= 0))
                        return entryString;
                    else
                        return "";
                }
                break;
            case "-":
                {
                    if (ValidSpaceChar.indexOf(entryString.charAt(entryIndex)) >= 0
						|| mask.charAt(maskIndex - 1) == '^')
                        return entryString;
                    else {
						entryString = me.addOneChar(entryString, entryIndex, " ");
                        return entryString;
                    }
                }
                break;
            case "*":
                {
                    return entryString;
                }
                break;
            default:
                return "";
        }
    },

    /**
    * 
    */
    replaceOneCharCase: function (string, index, upperFlag) {
        var startString, middleString, endString, fullString;

        startString = string.slice(0, index);
        middleString = string.slice(index, index + 1);
        endString = string.slice(index + 1);

        if (upperFlag)
            middleString = middleString.toUpperCase();
        else
            middleString = middleString.toLowerCase();

        fullString = startString + middleString + endString;

        return fullString;
    },

    /**
    * 
    */
    addOneChar: function (string, index, addCharString) {
        var startString, endString, fullString;

        startString = string.slice(0, index);
        endString = string.slice(index);

        fullString = startString + addCharString + endString;

        return fullString;
    },

    /**
    * 
    */
    setDefaultListeners: function (config) {
    },

	/**
	* 
	*/
	checkCreditCard: function (cardnumber) {
		var me = this;
		var cardname = Ext.getCmp(me.dctCardTypeField).getValue();
		// Array to hold the permitted card characteristics
		var cards = new Array();

        // Define the cards we support. You may add addtional card types.
        //  Name:      As in the selection box of the form - must be same as user's
        //  Length:    List of possible valid lengths of the card number for the card
        //  prefixes:  List of possible prefixes for the card
        //  checkdigit Boolean to say whether there is a check digit

        cards[0] = { name: "VS",
            length: "13,16",
            prefixes: "4",
            checkdigit: true
        };
        cards[1] = { name: "MC",
            length: "16",
            prefixes: "51,52,53,54,55",
            checkdigit: true
        };
        cards[2] = { name: "DC",
            length: "14,16",
            prefixes: "300,301,302,303,304,305,36,38,55",
            checkdigit: true
        };
        cards[3] = { name: "CB",
            length: "14",
            prefixes: "300,301,302,303,304,305,36,38",
            checkdigit: true
        };
        cards[4] = { name: "AE",
            length: "15",
            prefixes: "34,37",
            checkdigit: true
        };
        cards[5] = { name: "DS",
            length: "16",
            prefixes: "6011,650",
            checkdigit: true
        };
        cards[6] = { name: "JC",
            length: "15,16",
            prefixes: "3,1800,2131",
            checkdigit: true
        };
        cards[7] = { name: "enRoute",
            length: "15",
            prefixes: "2014,2149",
            checkdigit: true
        };
        cards[8] = { name: "Solo",
            length: "16,18,19",
            prefixes: "6334, 6767",
            checkdigit: true
        };
        cards[9] = { name: "Switch",
            length: "16,18,19",
            prefixes: "4903,4905,4911,4936,564182,633110,6333,6759",
            checkdigit: true
        };
        cards[10] = { name: "Maestro",
            length: "16,18",
            prefixes: "5020,6",
            checkdigit: true
        };
        cards[11] = { name: "VisaElectron",
            length: "16",
            prefixes: "417500,4917,4913",
            checkdigit: true
        };

        // Ensure that the user has provided a credit card number
        if (Ext.isEmpty(cardnumber) && Ext.isEmpty(cardname)) {
            return (true);
        }
        // Establish card type
        var cardType = -1;
        for (var i = 0; i < cards.length; i++) {
            // See if it is this card (ignoring the case of the string)
            if (cardname.toLowerCase() == cards[i].name.toLowerCase()) {
                cardType = i;
                break;
            }
        }
        // If card type not found, report an error
        if (cardType == -1) {
            return (DCT.T("InvalidCardType"));
        }
        // Now remove any spaces from the credit card number
        cardnumber = cardnumber.replace(/\s/g, "");

        // Check that the number is numeric
        var cardNo = cardnumber
        var cardexp = /^[0-9]{13,19}$/;
        if (!cardexp.exec(cardNo)) {
            return (DCT.T("InvalidCardNumber"));
        }
        // Now check the modulus 10 check digit - if required
        if (cards[cardType].checkdigit) {
            var checksum = 0;                                  // running checksum total
            var mychar = "";                                   // next char to process
            var j = 1;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            var calc;
            for (i = cardNo.length - 1; i >= 0; i--) {
                // Extract the next digit and multiply by 1 or 2 on alternative digits.
                calc = Number(cardNo.charAt(i)) * j;
                // If the result is in two digits add 1 to the checksum total
                if (calc > 9) {
                    checksum = checksum + 1;
                    calc = calc - 10;
                }
                // Add the units element to the checksum total
                checksum = checksum + calc;
                // Switch the value of j
                if (j == 1) { j = 2 } else { j = 1 };
            }
            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.
            if (checksum % 10 != 0) {
                return (DCT.T("InvalidCardNumber"));
            }
        }

        // The following are the card-specific checks we undertake.
        var LengthValid = false;
        var PrefixValid = false;
        var undefined;

        // We use these for holding the valid lengths and prefixes of a card type
        var prefix = new Array();
        var lengths = new Array();

        // Load an array with the valid prefixes for this card
        prefix = cards[cardType].prefixes.split(",");

        // Now see if any of them match what we have in the card number
        for (i = 0; i < prefix.length; i++) {
            var exp = new RegExp("^" + prefix[i]);
            if (exp.test(cardNo)) PrefixValid = true;
        }

        // If it isn't a valid prefix there's no point at looking at the length
        if (!PrefixValid) {
            return (DCT.T("InvalidCardNumber"));
        }

        // See if the length is valid for this card
        lengths = cards[cardType].length.split(",");
        for (j = 0; j < lengths.length; j++) {
            if (cardNo.length == lengths[j]) LengthValid = true;
        }
        // See if all is OK by seeing if the length was valid. We only check the 
        // length if all else was hunky dory.
        if (!LengthValid) {
            return (DCT.T("InvalidCardDigitCount"));
        }
        // The credit card is in the required format.
        return (true);
    }
});


// Add the additional 'advanced' VTypes

/**
* @class Ext.form.VTypes
*/
Ext.apply(Ext.form.VTypes, {

	/**
	* 
	*/
	ssn: function (val, field) {
		var ssnTest = /^(?!000)(?!666)([0-8]\d{2})([\-]?)(?!00)(\d\d)([\-]?)(?!0000)(\d{4})$/i;
		return ssnTest.test(val);
	},
	ssnText: DCT.T("SSNInvalidCharacters"),
	ssnMask: /[0-9-]/i,

	/**
	* 
	*/
	phone: function (val, field) {
		var phoneTest = /^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/i;
		return phoneTest.test(val);
	},
	phoneText: DCT.T("PhoneNumberInvalidCharacters"),
	phoneMask: /[0-9-\(\)]/i,

	/**
	* 
	*/
	phoneuk: function (val, field) {
		var phoneukTest = /^\D?(\d{4})\D?\D?(\d{3})\D?(\d{4})$/i;
		return phoneukTest.test(val);
	},
	phoneukText: DCT.T("PhoneNumberInvalidCharactersUK"),
	phoneukMask: /[0-9 ]/i,
	
	/**
	* 
	*/
	phoneit: function (val, field) {
		var phoneitTest = /^\D?(\d{8,11})$/i;
		return phoneitTest.test(val);
	},
	phoneitText: DCT.T("PhoneNumberInvalidCharactersIT"),
	phoneitMask: /[0-9 ]/i,
	/**
	* 
	*/
	ip: function (val, field) {
		var ipTest = /^((0|1[0-9]{0,2}|2[0-9]{0,1}|2[0-4][0-9]|25[0-5]|[3-9][0-9]{0,1})\.){3}(0|1[0-9]{0,2}|2[0-9]{0,1}|2[0-4][0-9]|25[0-5]|[3-9][0-9]{0,1})$/i;
		return ipTest.test(val);
	},
	ipText: DCT.T("IPInvalidAddress"),
	ipMask: /[0-9\.]/i,

	/**
	* 
	*/
	zipcode: function (val, field) {
		var zipcodeTest = /^(\d{5})(-\d{4})?$/i;
		return zipcodeTest.test(val);
	},
	zipcodeText: DCT.T("ZipCodeInvalid"),
	zipcodeMask: /[0-9-]/i,

	/**
	* 
	*/
	zipcodecn: function (val, field) {
	var zipcodecnTest = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[ABCEGHJKLMNPRSTVXY]{1} \d{1}[ABCEGHJKLMNPRSTVXY]{1}\d{1}$/;
		return zipcodecnTest.test(val);
	},
	zipcodecnText: DCT.T("PostalCodeInvalid_NoFormat"),
	zipcodecnMask: /[0-9 a-z]/i,

	/**
	* 
	*/
	zipcodeuk: function (val, field) {
	    var zipcodeukTest = /^(GIR 0AA|[A-PR-UWYZa-pr-uwyz]([0-9]{1,2}|([A-HK-Ya-hk-y][0-9]|[A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRV-Yabehmnprv-y]))|[0-9][A-HJKPS-UWa-hjkps-uw])\s*[0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2})$/i;
		return zipcodeukTest.test(val);
	},
	zipcodeukText: DCT.T("PostalCodeInvalid_NoFormat"),
	zipcodeukMask: /[A-Z-0-9 ]/i,
	
	/**
	* 
	*/
	zipcodede: function (val, field) {
		var zipcodedeTest = /^(\d{5})$/i;
		return zipcodedeTest.test(val);
	},
	zipcodedeText: DCT.T("PostalCodeInvalid_NoFormat"),
	zipcodedeMask: /[0-9]/i,
	
	/**
	* 
	*/
	time: function (val, field) {
		var timeTest = /^ *(1[0-2]|[1-9]):[0-5][0-9] *(a|p|A|P)(m|M) *$/;
		return timeTest.test(val);
	},
	timeText: DCT.T("InvalidTimeFormat"),
	timeMask: /[ 0-9ampAPM:]/i,

	/**
	* 
	*/
	militarytime: function (val, field) {
		var timeTest = /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/;
		return timeTest.test(val);
	},
	militarytimeText: DCT.T("InvalidTimeFormat"),
	militarytimeMask: /[ 0-9:]/i
});
