/**
*
*/
Ext.define('DCT.SystemRadioGroup', {
  extend: 'DCT.RadioGroup',
  
	inputXType: 'dctsystemradiogroup',
	xtype: 'dctsystemradiogroup',

	/**
	*
	*/
	setListeners: function (config) {
	}	
});
DCT.Util.reg('dctsystemradiogroup', 'systemRadioGroup', 'DCT.SystemRadioGroup');

/**
*
*/
Ext.define('DCT.SystemRadioField', {
  extend: 'DCT.RadioField',
  
	inputXType: 'dctsystemradiofield',
	xtype: 'dctsystemradiofield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
		var me = this;
		config.listeners = {
			boxready: {
				fn: me.setUpChangeEvent,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}
});

/**
*
*/
Ext.define('DCT.SystemSearchRadioGroup', {
  extend: 'DCT.SystemRadioGroup',
  
	inputXType: 'dctsystemsearchradiogroup',
	xtype: 'dctsystemsearchradiogroup',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			change:{
				fn: function(control, newValue, oldValue, eOpts) {
					var me = this;
					DCT.Util.updatePASSearchMode(newValue._typeOfSearch, true);
					me.showHidePolicyItems(newValue._typeOfSearch, true);
				},
				scope: me
			}
		};
	},
	/**
	*
	*/
	showHidePolicyItems: function (value, checked) {
		if (checked){
			DCT.Util.hideShowGroup("searchHistoryGroup", (value == 'policy' || value == 'allPolicies'));
		}
	}	
});
DCT.Util.reg('dctsystemsearchradiogroup', 'systemSearchRadioGroup', 'DCT.SystemSearchRadioGroup');
/**
*
*/
Ext.define('DCT.SystemMessageRadioField', {
  extend: 'DCT.SystemRadioField',
  
	inputXType: 'dctsystemmessageradiofield',
	xtype: 'dctsystemmessageradiofield',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners,{
			change: {
				fn: function(checkbox, checked) {
					var me = this;
					me.loadUserGrid(checkbox.getSubmitValue(), checked);
					me.showHideUserType(checkbox.getSubmitValue(), checked);
					DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	showHideUserType: function (value, checked) {
		if ((checked) && (value=='all')){
			Ext.get("users").setDisplayed(false);
			Ext.get("entities").setDisplayed(false);
			Ext.get("entityGroup").setDisplayed(false);
		  Ext.getCmp('userList').getStore().removeAll();
		  Ext.getCmp('entitiesList').getStore().removeAll();
		  Ext.getCmp('entityGroupList').getStore().removeAll();
		}
	},
	/**
	*
	*/
	loadUserGrid: function (value, checked) {
	  var maxPageSize;
	
		if (checked) {
			var radioValue = value;
			Ext.get("users").setDisplayed(radioValue=='users');
			Ext.get("entities").setDisplayed(radioValue=='entities');
			Ext.get("entityGroup").setDisplayed(radioValue=='entityGroup');
			Ext.getDom("_pageType").value = radioValue;
			switch (radioValue){
				case "users":
					var userListStore = Ext.getCmp('userList').getStore();
		      userListStore.loadPage(1);
				break;
				case "entities":
					var entitiesDataStore = Ext.getCmp('entitiesList').getStore();
		    	entitiesDataStore.loadPage(1);
				break;
		    case "entityGroup":
		    	var entityGroupDataStore = Ext.getCmp('entityGroupList').getStore();
		      entityGroupDataStore.loadPage(1);
		    break;
			}
	  }
	}
	
});

/**
*
*/
Ext.define('DCT.SendToRadioField', {
  extend: 'DCT.SystemMessageRadioField',
  
	inputXType: 'dctsendtoradiofield',
	xtype: 'dctsendtoradiofield',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners,{
			change: {
				fn: function(checkbox, checked) {
					var me = this;
					me.showHideUserType(checkbox.getSubmitValue(), checked);
					DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
				},
				scope: me
			}
		});
	}   
});
