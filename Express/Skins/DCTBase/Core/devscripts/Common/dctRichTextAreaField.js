/**
*extends Ext.form.HtmlEditor to provide extensibility
*/
Ext.define('DCT.RichTextAreaField', {
  extend: 'Ext.form.HtmlEditor',
  requires: [
      'Ext.form.field.Base'
  ],
	
	defaultWidth: '500',
	defaultHeight: '200',
	inputXType: 'dctrichtextareafield',
	dctControl: true,
	defaultshowToolbar: true,
	enableColors: false,
	enableAlignments: false,
	enableSourceEdit: false,
	enableLinks: false,
	xtype: 'dctrichtextareafield',
	
	blankText: DCT.T('ThisFieldIsRequired'),
	maxLengthText: DCT.T("RichFieldMaxLengthError"),
	invalidWarningIconCls: Ext.baseCSSPrefix + 'form-invalid-warning-icon',
	invalidCls: Ext.baseCSSPrefix + 'form-invalid',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
	  me.value = Ext.util.Format.htmlEncode(me.value);
	  me.checkControlExists(config);
	  me.setCls(config);
	  me.setDisable(config);
	  me.setAllowBlank(config);
	  me.setAutoCreate(config);
	  me.setListeners(config);
	  me.setEditorToolbar(config);
	  me.callParent([config]);
	  me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
	  config.listeners = {
	      sync: {
	          fn: function (htmlEditor, htmlText) {
	          	var me = this;
	          	if (me.initialized) {
		            DCT.FieldsChanged.set();
		            me.validateValue(me.cleanRichTextHtml(htmlText));
		          }
	          }
	      }
	  };
	},
	/**
	*
	*/
	cleanRichTextHtml: function (html) {
	  if (html != null) {
	  	  var htmlInput = html;
			  htmlInput = htmlInput.replace(/&(lt|gt);/g, function (strMatch, p1) {
			      return (p1 == "lt") ? "<" : ">";
			  });
			  htmlInput = htmlInput.replace(/&(nbsp);/g, "");
			  htmlInput = htmlInput.replace(/<\/?[^>]+(>|$)/g, "");
        return htmlInput;
	  }
	},
	/**
	*
	*/
	setAutoCreate: function (config) {
		var me = this;
		var styleWidth = (Ext.isDefined(config.dctWidth)) ? config.dctWidth : me.defaultWidth;
		var isPercent = styleWidth.indexOf('%');
		if (isPercent > -1){
			var percentWidth = styleWidth.substring(0, isPercent);
			var widthNumber = new Number(percentWidth);
			if (widthNumber > 100)
				styleWidth = '100%';
		}
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.width = styleWidth; 
		if (Ext.isDefined(config.rows)) {
			config.height = config.rows * 10 + 50;
			config.rows = 0;
		} else {
			config.height = me.defaultHeight; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**
	* 
	*/
	setEditorToolbar: function (config) {
		var me = this;
    var showToolbar = (Ext.isDefined(config.dctShowToolbar)) ? config.dctShowToolbar : me.defaultshowToolbar;
    config.dctToolbarCfg = {
    	hidden: !showToolbar
    };
	},
	/**
	*
	*/
	getErrors: function (value) {
		var me = this,
  			format = Ext.String.format;
		value = arguments.length > 0 ? value : me.getValue();
	  var errors = me.callParent([value]);
	  if (!me.allowBlank && (value.length < 1 || value === me.emptyText)) {
	      errors.push(me.blankText);
	  }
    if (value.length > me.maxLength) {
        errors.push(format(me.maxLengthText, me.maxLength));
    }	  
	  return errors;
	},
  /**
   * Called when the editor creates its toolbar. Override this method if you need to
   * add custom toolbar buttons.
   * @param {Ext.form.field.HtmlEditor} editor
   * @protected
   */
  createToolbar: function() {
  	var me = this,
  			toolbarCfg = me.getToolbarCfg();
  	if (Ext.isDefined(me.dctToolbarCfg)){
  		Ext.apply(toolbarCfg, me.dctToolbarCfg);
  	}
  	me.toolbar = Ext.widget(toolbarCfg);
    return me.toolbar;
  },
	/**
	* Fixed the field container class for not having a class defined.  This was creating a warning when the html editor was created.
	*/
  getSubTplData: function(fieldData) {
    var me = this,
        data = Ext.apply(me.callParent([
        fieldData
    ]), {
        containerElCls: me.containerElCls
    });
    return data;
  },
  initEditor: function() {
      var me = this,
          dbody, ss, doc, docEl, fn;
      //Destroying the component during/before initEditor can cause issues.
      if (me.destroying || me.destroyed) {
          return;
      }
      dbody = me.getEditorBody();
      // IE has a null reference when it first comes online.
      if (!dbody) {
          setTimeout(function() {
              me.initEditor();
          }, 10);
          return;
      }
      ss = me.textareaEl.getStyle([
          'font-size',
          'font-family',
          'background-image',
          'background-repeat',
          'background-color',
          'color'
      ]);
      ss['background-attachment'] = 'fixed';
      // w3c
      dbody.bgProperties = 'fixed';
      // ie
      Ext.DomHelper.applyStyles(dbody, ss);
      doc = me.getDoc();
      docEl = Ext.get(doc);
      if (docEl) {
          try {
              docEl.clearListeners();
          } catch (e) {}
          /*
           * We need to use createDelegate here, because when using buffer, the delayed task is added
           * as a property to the function. When the listener is removed, the task is deleted from the function.
           * Since onEditorEvent is shared on the prototype, if we have multiple html editors, the first time one of the editors
           * is destroyed, it causes the fn to be deleted from the prototype, which causes errors. Essentially, we're just anonymizing the function.
           */
          fn = me.onEditorEvent.bind(me);
          docEl.on({
              mousedown: fn,
              dblclick: fn,
              click: fn,
              keyup: fn,
              delegated: false,
              buffer: 100
          });
          // These events need to be relayed from the inner document (where they stop
          // bubbling) up to the outer document. This has to be done at the DOM level so
          // the event reaches listeners on elements like the document body. The effected
          // mechanisms that depend on this bubbling behavior are listed to the right
          // of the event.
          fn = me.onRelayedEvent;
          docEl.on({
              mousedown: fn,
              // menu dismisal (MenuManager) and Window onMouseDown (toFront)
              mousemove: fn,
              // window resize drag detection
              mouseup: fn,
              // window resize termination
              click: fn,
              // not sure, but just to be safe
              dblclick: fn,
              // not sure again
              delegated: false,
              scope: me
          });
          if (Ext.isGecko) {
              docEl.on('keypress', me.applyCommand, me);
          }
          if (me.fixKeys) {
              docEl.on('keydown', me.fixKeys, me, {
                  delegated: false
              });
          }
          if (me.fixKeysAfter) {
              docEl.on('keyup', me.fixKeysAfter, me, {
                  delegated: false
              });
          }
          if (Ext.isIE9) {
              Ext.get(doc.documentElement).on('focus', me.focus, me);
          }
          // In old IEs, clicking on a toolbar button shifts focus from iframe
          // and it loses selection. To avoid this, we save current selection
          // and restore it.
          if (Ext.isIE8) {
              docEl.on('focusout', function() {
                  me.savedSelection = doc.selection.type !== 'None' ? doc.selection.createRange() : null;
              }, me);
              docEl.on('focusin', function() {
                  if (me.savedSelection) {
                      me.savedSelection.select();
                  }
              }, me);
          }
          // We need to be sure we remove all our events from the iframe on unload or we're going to LEAK!
          if (!Ext.isIE10m)
          	Ext.getWin().on('beforeunload', me.beforeDestroy, me);
          doc.editorInitialized = true;
          me.initialized = true;
          me.pushValue();
          me.setReadOnly(me.readOnly);
          me.fireEvent('initialize', me);
      }
  }  
  
}, function() {
	var me = this;
  me.borrow(Ext.form.field.Base, [
  		'validateValue',
  		'hasActiveWarning',
  		'getActiveWarning',
  		'setActiveWarning',
  		'unsetActiveWarning',
  		'setActiveWarnings',
      'markInvalid',
      'clearInvalid',
      'setError',
			'getActiveWarnings',
			'setActiveErrors',
			'unsetActiveError',
			'getWarnings',
			'onDisable',
			'renderActiveError',
			'parentRenderActiveError'
  ]);
});
DCT.Util.reg('dctrichtextareafield', 'interviewRichTextAreaField', 'DCT.RichTextAreaField');