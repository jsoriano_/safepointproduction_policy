/**
*
*/
Ext.define('DCT.PopupWindow', {
  extend: 'Ext.Window',
  
	dctControl: true,
	inputXType: 'dctpopupwindow',
	plain: true,
	layout: 'border',
	closable: true,
	cls: 'genericPopUp',
	closeAction: 'dctCloseAction',
	bodyBorder: false,
	dctPopupId: 'dctPopupWindow',
	dctPopupPrefix: 'dctPopup_',
	xtype: 'dctpopupwindow',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setId(config);
		me.checkControlExists(config);
		me.setPanelFrameId(config);
		me.setWindowTitle(config);
		me.setModalItems(config);
		me.setSizeItems(config);
		me.setItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			show: {
				fn: function (win) {
					DCT.Util.removeTabs(true);
				},
				scope: me
			},
			hide: {
				fn: function (win) {
					var me = this;
					me.hideWindow();
				},
				scope: me,
				delay: 10
			}
		};
		if (config.modal) {
			Ext.apply(config.listeners, {
				minimize: {
					fn: function (win) {
						win.minimized = true;
						win.setSize(win.minWidth, win.minWidth / 2);
					},
					scope: me
				},
				restore: {
					fn: function (win) {
						if (win.minimized) {
							win.minimized = false;
							win.setSize(config.width, config.height);
						}
					},
					scope: me
				}
			});
		}
	},
	/**
	*
	*/
	hideWindow: function () {
		var me = this;
		DCT.Util.removeTabs(false);

		if (me.dctParentIsPopup == 'false' || me.dctParentIsPopup == '0' || !me.dctParentIsPopup) {
			DCT.Util.getSafeElement('_popUp').value = '0';
		}
		if (me.dctInterview) {
			var parentWindow = DCT.Util.getParent(true);
			var currentAction = parentWindow.Ext.getDom('_currentAction');
			if (currentAction) {
				parentWindow.Ext.getDom('_targetPage').value = parentWindow.DCT.currentPage;
				parentWindow.DCT.Util.getSafeElement('_action').value = currentAction.value;
				parentWindow.DCT.Submit.submitForm();
				//have to submit here because ajax would diff the page between this and the popup we are closing
			}
		}
		if (this.dctPartyReturn) {
			DCT.ajaxProcess.submitPartyReturn(2, 'partyStop:partyReturn', '', '');
		}
		this.destroy();
	},
	/**
	*
	*/
	setId: function (config) {
		var me = this;
		if (config.modal) {
			config.id = me.dctPopupId;
		}
	},
	/**
	*
	*/
	randomId: function () {
		var id = "";
		for (var i = 0; i < 32; i++) {
			id += "ABCDEF1234567890".charAt(Math.floor(Math.random() * 16));
		}
		return id;
	},
	/**
	*
	*/
	setWindowTitle: function (config) {
		config.title = (config.title) ? config.title : document.title;
	},
	/**
	*
	*/
	setPanelFrameId: function (config) {
		var me = this;
		config.dctPanelFrameId = me.dctPopupPrefix + config.id + me.randomId();
	},
	/**
	*
	*/
	setModalItems: function (config) {
		if (!config.modal) {
			config.shadow = false;
			config.maximizable = true;
			config.minimizable = true;

		} else {
			config.shadow = false;
			config.style = { position: 'fixed' };
		}

	},
	/**
	*
	*/
	setSizeItems: function (config) {
		// Do not use size of #wrapper - it may be much taller than the viewport (i.e. on any page that scrolls).
		var maxWidth = document.documentElement.clientWidth * .80;
		var maxHeight = document.documentElement.clientHeight * .80;
		if (!(config.width > 0)) { //Width not set in the XSL
			config.width = maxWidth;
			config.height = maxHeight;
		}
		config.width = Ext.Number.constrain(config.width, 50, maxWidth);
		config.height = Ext.Number.constrain(config.height, 50, maxHeight);
	},
	/**
	*
	*/
	setItems: function (config) {
		var itemConfig = {
			dctScrollbars: config.dctScrollbars,
			dctFrameID: config.dctPanelFrameId,
			width: config.width,
			height: config.height
		};
		if (config.dctPanelConfig) {
			Ext.apply(itemConfig, config.dctPanelConfig);
		}
		config.items = Ext.create(config.dctItemClass, itemConfig);
	},
	/**
	*
	*/
	closePopupWindow: function () {
		var me = this;
		if (me.dctParentIsPopup == 'false' || me.dctParentIsPopup == '0' || !me.dctParentIsPopup) {
			DCT.Util.getSafeElement('_popUp').value = '0';
		}
		if (me.dctAssignFocus) {
			var focusField = DCT.Util.getFocusField();
			if (DCT.Util.isInterviewControl(focusField)) {
				if (Ext.getCmp(focusField))
					Ext.getCmp(focusField).setFieldFocus();
			} else {
				if (Ext.get(focusField))
					Ext.get(focusField).focus(5);
			}
		}
		DCT.Util.removeTabs(false);
		Ext.Function.defer(me.destroy, 200, me);
	},
  unghost: function(show, matchPosition, focus) {
      var me = this,
          ghostPanel = me.ghostPanel;
      if (!ghostPanel) {
          return;
      }
      if (show !== false) {
          
          
          me.el.show();
          if (matchPosition !== false) {
							// Adjust for scroll
							// Subtract the scroll position from the coordinates, so that the window won't jump if you are scrolled when you try to move it.
							var amountToOffset = Ext.get(document.body).getScroll();
				
							// In IE8 and IE9, if you move the popup by rapidly flicking the mouse downward, it vanishes off the screen completely because the offset is extraneous.
							if(me.x == ghostPanel.getX() && me.y == ghostPanel.getY()){
								amountToOffset = {left:0, top:0}; // cancel the offset
							}
							me.setPagePosition(ghostPanel.getX() - amountToOffset.left, ghostPanel.getY() - amountToOffset.top);
              if (me.hideMode === 'offsets') {
                  
                  delete me.el.hideModeStyles;
              }
          }
          if (focus) {
              me.focus(false, 10);
          }
      }
			// If user moves popup off the screen in any direction which makes popup disappeared, so setting popup at position x:50,y:50.
			if (document.documentElement.clientWidth - 10 <= me.x || document.documentElement.clientHeight - 10 <= me.y || (me.x + ghostPanel.getWidth()) <= 10 || (me.y + ghostPanel.getHeight()) <= 10) {
				me.setPosition(50, 50);
			}
			// Checks whether browser window is maximized or not.
			if ((window.window.screen.availWidth - 20) < document.documentElement.clientWidth && (window.window.screen.availHeight - 120) < document.documentElement.clientHeight) {
				// when popup dropped at bottom of window at particular cordinates it became non draggable so sending this back to x:50, y:50 position so that it becomes draggable.
				if (me.y >= (document.documentElement.clientHeight - 150)) {
					me.setPosition(50, 50);
				}
			}
      ghostPanel.el.hide();
      ghostPanel.setHiddenState(true);
  },
	/**
	*
	*/
	dctCloseAction: function() {
		var me = this;
		var me = eval(encodeURIComponent(me.dctPanelFrameId));
		var elements = me.Ext.select('div[data-config*=isCancel]').elements;
		if (elements.length > 0) {
			var elem = elements[0];
			var divElement = me.Ext.get(elem.id);
			var a = divElement.parent().select("a").elements[0];
			if (a) {
				if (typeof a.onclick == "function") {
					a.onclick.apply(a);
				}
			}
		}
		else {
			me.hide();
		}
	}
});
DCT.Util.reg('dctpopupwindow', 'popupWindow', 'DCT.PopupWindow');

/**
*
*/
Ext.define('DCT.DisplayPanel', {
  extend: 'Ext.Panel',
  
	inputXType: 'dctdisplaypanel',
	margins: '3 3 3 0',
	dctControl: true,
	region: 'center',
	dctScrollbars: null,
	bodyBorder: false,
	xtype: 'dctdisplaypanel',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setHtmlItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setHtmlItems: function (config) {
		var scrollbarsSetting = '';
		if (config.dctScrollbars != null) {
			if (config.dctScrollbars)
				scrollbarsSetting = 'scrolling="yes"';
			else
				scrollbarsSetting = 'scrolling="no"';
		}
		config.html = '<IFRAME id="' + config.dctFrameID + '" style="WIDTH: 100%; HEIGHT: 100%; BACKGROUND-COLOR: transparent" name="' + config.dctFrameID + '" frameBorder="0" width="100%" height="100%"' + scrollbarsSetting + '></IFRAME>';
	}
});

/**
*
*/
Ext.define('DCT.AnnotationsPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctannotationspanel',
	xtype: 'dctannotationspanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0].target = me.dctFrameID;
					var xmlString = '<helpItems><fieldId>' + me.dctField + '</fieldId><cacheId>' + me.dctCacheId + '</cacheId><actionId>' + me.dctActionId + '</actionId><currentPageId>' + me.dctCurrentPageId + '</currentPageId></helpItems>';
					Ext.getDom('_action').value = xmlString;
					Ext.getDom('_targetPage').value = "documentation";
					DCT.Util.getSafeElement('_popUp').value = '1';
					DCT.Submit.customBeforeSubmit();
					document.forms[0].submit();
					Ext.getDom('_targetPage').value = "";
					document.forms[0].target = "_self";
					Ext.getDom('_action').value = "";
					if (me.dctParentIsPopup == 'false' || me.dctParentIsPopup == '0' || !me.dctParentIsPopup) {
						DCT.Util.getSafeElement('_popUp').value = '0';
					}
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.DocumentationPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctdocumentationpanel',
	xtype: 'dctdocumentationpanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0].target = me.dctFrameID;
					Ext.getDom('_action').value = "";
					Ext.getDom('_targetPage').value = "documentation";
					DCT.Submit.submitForm();
					Ext.getDom('_targetPage').value = "";
					Ext.getDom('_action').value = "";
					document.forms[0].target = "_self";
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.PartyPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctpartypanel',
	xtype: 'dctpartypanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function(win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0]._targetPage.value = me.dctTargetPage;
					Ext.getDom('_action').value = ''; 
					document.forms[0].action = DCT.postPage;
					document.forms[0].target = me.dctFrameID;
					DCT.Submit.customBeforeSubmit();
					document.forms[0].submit();
					document.forms[0].target = "";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.InterviewPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctinterviewpanel',
	xtype: 'dctinterviewpanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					Ext.getDom('_targetPage').value = "";
					Ext.getDom('_action').value = me.dctActionName; 
					document.forms[0].action = DCT.postPage;
					document.forms[0].target = me.dctFrameID;
					DCT.Submit.customBeforeSubmit();
					document.forms[0].submit();
					document.forms[0].target = "";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.AgencyPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctagencypanel',
	xtype: 'dctagencypanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function(win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					Ext.getDom('_targetPage').value = me.dctTargetPage;
					document.forms[0].action = DCT.postPage;
					document.forms[0].target = me.dctFrameID;
					DCT.Submit.submitForm();
					DCT.LoadingMessage.cancel();
					document.forms[0].target="";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			},
			beforedestroy: {
				fn: function (win) {
					Ext.getDom('_targetPage').value = DCT.Util.getParent().DCT.currentPage;
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.PopupPanel', {
  extend: 'DCT.DisplayPanel',
  
	inputXType: 'dctpopuppanel',
	xtype: 'dctpopuppanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0].target = me.frameID;
					DCT.Submit.submitPageAction(DCT.globalPage, DCT.globalAction);
					document.forms[0].target = "_self";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.ErrorDetailsPanel', {
  extend: 'DCT.DisplayPanel',
  
    inputXType: 'dcterrordetailspanel',
    xtype: 'dcterrordetailspanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0].target = me.dctFrameID;
					Ext.getDom('_targetPage').value = "errorDetails";
					DCT.Submit.submitForm();
					DCT.LoadingMessage.cancel();
					Ext.getDom('_targetPage').value = "";
					document.forms[0].target = DCT.Util.getParent(true).name;
					Ext.getDom('_action').value = "";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});

/**
*
*/
Ext.define('DCT.ResubmitDetailsPanel', {
  extend: 'DCT.DisplayPanel',
  
    inputXType: 'dctresubmitdetailspanel',
    xtype: 'dctresubmitdetailspanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					document.forms[0].target = me.dctFrameID;
					Ext.getDom('_targetPage').value = "resubmissionDetails";
					DCT.Submit.submitForm();
					DCT.LoadingMessage.cancel();
					Ext.getDom('_targetPage').value = "";
					document.forms[0].target = DCT.Util.getParent(true).name;
					Ext.getDom('_action').value = "";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});

/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	doAnnotationPopup: function (field, cacheId, actionId, currentPageId) {
		var parentIsPopup = DCT.Util.getSafeElement('_popUp').value;
		var displayPopup = Ext.create('DCT.PopupWindow', {
			width: 600,
			height: 300,
			modal: false,
			dctParentIsPopup: parentIsPopup,
			dctItemClass: 'DCT.AnnotationsPanel',
			dctPanelConfig: {
				dctField: field,
				dctCacheId: cacheId,
				dctActionId: actionId,
				dctParentIsPopup: parentIsPopup,
				dctCurrentPageId: currentPageId
			},
			buttons: [{
				text: DCT.T('Close'),
				handler: function () {
					var me = this;
					var popup = me.findParentByType('dctpopupwindow');
					popup.closePopupWindow();
				}
			}]
		});
		displayPopup.show();
	},
	/**
	*
	*/
	openPopUp: function (width, height, title, actionName, popupClass, popupClosable) {
		var parentIsPopup = DCT.Util.getSafeElement('_popUp').value;
		DCT.Util.getSafeElement('_popUp').value = '1';
		var interviewPopup = Ext.create('DCT.PopupWindow', {
			width: width,
			height: height,
			modal: true,
			title: title,
			cls: (Ext.isEmpty(popupClass)) ? null : popupClass,
			closable: popupClosable,
			dctAssignFocus: true,
			dctInterview: true,
			dctItemClass: 'DCT.InterviewPanel',
			dctParentIsPopup: parentIsPopup,
			dctPanelConfig: {
				dctParentIsPopup: parentIsPopup,
				dctActionName: actionName
			}
		});
		interviewPopup.show();
		Ext.getCmp('dctPopupWindow').center();
	},
	/**
	*
	*/
	closePopUp: function (popUpMode, actionName, cancelChanges) {
		// popUpMode of 3 means post back to parent window to refresh data
		var me = this;
		var parentWindow = DCT.Util.getParent(true);
		Ext.getDom('_targetPage').value = (popUpMode == 3) ? parentWindow.DCT.currentPage : "";
		Ext.getDom('_action').value = actionName;
		document.forms[0].action = DCT.postPage;
		document.forms[0].target = parentWindow.name;
		if (cancelChanges == '1') {
			DCT.Util.resetFields();
		}
		var popUpWindow = parentWindow.Ext.getCmp('dctPopupWindow')
		if (!Ext.isEmpty(popUpWindow) && (popUpWindow.dctParentIsPopup == 'true' || popUpWindow.dctParentIsPopup == '1')) {
			DCT.Util.getSafeElement('_popUp').value = "1";
		}
		else {
			DCT.Util.getSafeElement('_popUp').value = '0';
		}
		DCT.LoadingMessage.show();
		parentWindow.DCT.LoadingMessage.show();
		DCT.Submit.customBeforeSubmit();

		var parentFocusId = parentWindow.DCT.Util.getFocusField();
		if (!Ext.isEmpty(parentFocusId)) {
			DCT.Util.setFocusField(parentFocusId);
		}
		document.forms[0].submit();
		me.closeWindow();
	},
	/**
	*
	*/
	popUpPage: function (popUpMode, locat, skipValidation, cancelChanges, hideCloseButton, popupClass,
						width, height, title, doConfirm, confirmText) {
		// popUpMode = 1 to OPEN popup
		// popUpMode = 2 to CLOSE
		// popUpMode = 3 to CLOSE and return to parent content page
		var popupClosable = (hideCloseButton == 1) ? false : true;
		DCT.LoadingMessage.hideAndCancel();
		if (skipValidation != '1')
			if (!DCT.Util.validateFields()) { return false; }

		/* in order to prompt the user, the 'Confirm' param must be present and true;
		* if the 'Confirm' param is true and 'confirmText' is not provided, we use
		* a default value.
		*/
		if (doConfirm == '1') {
			if (confirmText == null || confirmText.length == 0) {
				confirmText = DCT.T('AreYouSure');
			}
		}
		// potentially open popup
		if (popUpMode == 1) {
			if (doConfirm != '1') {
				DCT.Util.openPopUp(width, height, title, locat, popupClass, popupClosable);
				return;
			}
			// Confirm param was true, prompt before opening
			Ext.Msg.confirm(DCT.T('Confirm'), confirmText, function (btn) {
				if (btn == 'yes') {
					DCT.Util.openPopUp(width, height, title, locat, popupClass, popupClosable);
				}
			});
		}
		// potentially close popup
		else {
			if (doConfirm != '1') {
				DCT.Util.closePopUp(popUpMode, locat, cancelChanges);
				return;
			}
			// Confirm param for this action was true; can apply to OK, Cancel, etc
			Ext.Msg.confirm(DCT.T('Confirm'), confirmText, function (btn) {
				if (btn == 'yes') {
					DCT.Util.closePopUp(popUpMode, locat, cancelChanges);
				}
			});
		}
	},
	/**
	*
	*/
	closeWindow: function () {
		var popUpWindow = DCT.Util.getParent(true).Ext.getCmp('dctPopupWindow');
		if (popUpWindow)
			popUpWindow.closePopupWindow();
	}
});

/**
*
*/
Ext.define('DCT.PartyCommonPanel', {
  extend: 'DCT.DisplayPanel',
  
    inputXType: 'dctpartycommonpanel',
    xtype: 'dctpartycommonpanel',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function (win) {
					var me = this;
					DCT.Util.showLoadingMessageForPopup(win);
					Ext.getDom('_submitAction').value = me.dctTargetPage;
					document.forms[0].target = me.dctFrameID;
					DCT.Submit.submitForm();
					DCT.LoadingMessage.cancel();
					document.forms[0].target="";
					DCT.Util.getSafeElement('_popUp').value = '0';
				},
				scope: me
			}
		};
	}
});
