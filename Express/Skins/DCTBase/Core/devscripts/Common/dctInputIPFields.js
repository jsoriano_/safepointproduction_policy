
/****/
Ext.define('DCT.InputIPField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputipfield',
	xtype: 'dctinputipfield',

  /**  *
  */
  setVType: function(config){
  	config.vtype = 'ip';
  }		  
   
});
DCT.Util.reg('dctinputipfield', 'interviewInputIPField', 'DCT.InputIPField');
