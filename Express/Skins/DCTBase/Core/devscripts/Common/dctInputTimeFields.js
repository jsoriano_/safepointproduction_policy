
/****/
Ext.define('DCT.InputTimeField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputtimefield',
	xtype: 'dctinputtimefield',

  /**  *  */
  setVType: function(config){
  	config.vtype = (config.dctMilitaryTime == '0') ? 'time' : 'militarytime';
  }		  
});
DCT.Util.reg('dctinputtimefield', 'interviewInputTimeField', 'DCT.InputTimeField');
