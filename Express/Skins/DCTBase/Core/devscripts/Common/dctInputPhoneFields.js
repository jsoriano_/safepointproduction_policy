
/****/
Ext.define('DCT.InputPhoneField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputphonefield',
	dctFormatBeforePost: true,
	xtype: 'dctinputphonefield',

  /**  *  */
  setFormatter: function (config) {
  	var me = this;
  	config.formatter = me.formatPhone;
  },
  /**  *  */
  setVType: function (config) {
  	config.vtype = 'phone';
  }		  
});
DCT.Util.reg('dctinputphonefield', 'interviewInputPhoneField', 'DCT.InputPhoneField');
