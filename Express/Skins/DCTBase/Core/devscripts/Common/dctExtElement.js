
/** **/
Ext.define('DCT.Element', {
    override: 'Ext.Element',

	/** 	*	*/
	getDataPrefixedAttribute: function (name) {
		/// <summary>Get the given data prefixed attribute on this element and return its value</summary>
		/// <param name="name">The name of the data-prefixed attribute</param>
		/// <returns>Value of the data-prefixed attribute</returns>
		
		return this.dom.getAttribute("data-" + name);
	},

	/** 	*	*/
	getAllAttributes: function () {
		/// <summary>Get a list of attributes on this element, stripping away any data-prefixes.</summary>
		/// <returns>A hash of keys to values</returns>

		var attribs = {};

		var attributes = this.getAttributes();
		
		Ext.iterate(attributes, function (key, value, itself) {

			if (Ext.isString(key) && Ext.isString(value)) {
				if (key.indexOf("data-") === 0) {
					if (value === "true") value = true;
					if (value === "false") value = false;
					if (value === "null") value = null;

					attribs[key.substring(5)] = value;
				} else {
					attribs[key] = value;
				}
			}
		});

		return attribs;
	},

	/** 	*	*/
	getDataPrefixedAttributes: function () {
		/// <summary>Get a list of data-prefixed attributes on this element</summary>
		/// <returns>A hash of keys to values with the "data-" prefix removed</returns>

		var attribs = {};
		
		var attributes = this.getAttributes();

		Ext.iterate(attributes, function (key, value, itself) {

			if (key == 'data-config') {	
				value = value.replace(/\n/g, "\\n").replace(/\r/g, ""); 		
				Ext.apply(attribs, Ext.decode(value));
			}
			else if (key == 'data-value') { 
				var dataValue = {value:value}
				Ext.apply(attribs, dataValue);
			}			
			else {
				if (Ext.isString(key) && Ext.isString(value)) {
					if (key.indexOf("data-") === 0) {
						if (value === "true") value = true;
						if (value === "false") value = false;
						if (value === "null") value = null;
	
						attribs[key.substring(5)] = value;
					}
				}
			}
		});

		return attribs;
	},

	/** 	*	*/
	applyDataPrefixedAttributesTo: function (obj) {
		/// <summary>Assign all of this element's data-prefixed attributes to the given object</summary>
		/// <param name="obj">The object that will receive the attributes</param>

		Ext.apply(obj, this.getDataPrefixedAttributes());
	},

	/** 	*	*/
	bindConfiguredEvents: function () {
		/// <summary>Bind all events specified in data-prefixed attributes on the current element</summary>

		var element = this;
	
		Ext.iterate(element.getDataPrefixedAttributes(), function (k, v) {
			if (Ext.isEmpty(k) || Ext.isEmpty(v)) return;
			if (k.indexOf("event-") !== 0) return;

			var eventName = k.substr(6, k.length - 6);
			var customEvent = DCT.CustomEvents[eventName];

			var fn = function() {
				if (Ext.isFunction(element[v]))
					return element[v].apply(element, arguments);
				else if (Ext.isFunction(window[v]))
					return window[v].apply(element, arguments);
				else
					return eval(v);
			};

			if (Ext.isDefined(customEvent))
				customEvent(element, fn);
			else
				element.on(eventName, fn);
		});
	}
});

Ext.define('DCT.EventDom', {
	override: 'Ext.event.publisher.Dom',
	
	fire: function(element, eventName, e, direct, capture) {
	    var event;
	    if (element && element.hasListeners[eventName]) {
	        event = element.events[eventName];
	        if (event) {
	            if (capture && direct) {
	                event = event.directCaptures;
	            } else if (capture) {
	                event = event.captures;
	            } else if (direct) {
	                event = event.directs;
	            }
	            
	            
	            if (event) {
	                e.setCurrentTarget(element.dom);
	                event.fire(e, e.target);
	            }
	        }
	    }
	}
});
