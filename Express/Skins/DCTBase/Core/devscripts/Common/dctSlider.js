
/**
*
*/
Ext.define('DCT.ComparisonSlider', {
  extend: 'Ext.slider.Single',
  
	defaultWidthMultiple: 40,
	useTips: false,
	increment: 1,
	inputXType: 'dctcomparisonslider',
	storeFields: ['value', 'caption', 'comparisonValue', 'comparisonCaption'],
	valueField: 'value',
	captionField: 'caption',
	comparisonCaptionField: 'comparisonCaption',
	dctControl: true,
	xtype: 'dctcomparisonslider',
  childEls: [
      'endEl', 'innerEl', 'ticksEl', 'compValueEl'
  ],	
  fieldSubTpl: [
  		'<div id="{cmpId}-compValueEl" data-ref="compValueEl" class="sliderComparison" role="presentation">',
  		'{%this.renderCompValue(out, values)%}',
  		'</div>',
      '<div id="{id}" role="{role}" {inputAttrTpl} class="' + Ext.baseCSSPrefix + 'slider {fieldCls} {vertical} sliderRange',
      '{childElCls}"',
      '<tpl if="tabIdx != null"> tabindex="{tabIdx}"</tpl>',
      '<tpl if="isVertical"> aria-orientation="vertical"<tpl else> aria-orientation="horizontal"</tpl>',
      '>',
          '<div id="{cmpId}-endEl" data-ref="endEl" class="' + Ext.baseCSSPrefix + 'slider-end" role="presentation">',
              '<div id="{cmpId}-innerEl" data-ref="innerEl" class="' + Ext.baseCSSPrefix + 'slider-inner" role="presentation">',
                  '{%this.renderThumbs(out, values)%}',
              '</div>',
          '</div>',
          '<div id="{cmpId}-ticksEl" data-ref="ticksEl" class="sliderRangeTicks" role="presentation">',
                  '{%this.renderTicks(out, values)%}',
          '</div>',
      '</div>',
      {
          renderThumbs: function(out, values) {
              var me = values.$comp,
                  i = 0,
                  thumbs = me.thumbs,
                  len = thumbs.length,
                  thumb,
                  thumbConfig;

              for (; i < len; i++) {
                  thumb = thumbs[i];
                  thumbConfig = thumb.getElConfig();
                  thumbConfig.id = me.id + '-thumb-' + i;
                  Ext.DomHelper.generateMarkup(thumbConfig, out);
              }
          },
          renderTicks: function(out, values) {
              var me = values.$comp,
                  i = 0,
                  ticks = me.store.data.items,
                  len = ticks.length,
                  tick,
								  startPos = me.calculateThumbPosition(me.minValue),
								  nextPos = me.calculateThumbPosition((me.minValue + me.increment)),
								  diff = nextPos - startPos,
								  lastTick = 0,
                  tickConfig;

              for (; i < len; i++) {
                  tick = ticks[i];
                  tickPosition = (i==0) ? -6 : diff - 1 + lastTick;
                  lastTick = tickPosition;
                  tickConfig = {html: tick.data[me.captionField], cls: 'sliderTick', style: 'left: '+ tickPosition +'%; width: '+diff+'px;'};
                  Ext.DomHelper.generateMarkup(tickConfig, out);
              }
          },
          renderCompValue: function(out, values) {
              var me = values.$comp;
              out.push(me.getComparisonValue(me.value));
          },
          disableFormats: true
      }
  ],
	

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setListeners(config);
   	me.setWidth(config);
   	me.setMaxValue(config);
   	me.setDataStore(config);
   	me.setInitialValue(config);
   	me.setInputAttributes(config);
   	me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	* 
	*/
	setInputAttributes: function(config){
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
   /**
   *
   */
   onResize: function (w, h) {
		var me = this;
    DCT.ComparisonSlider.superclass.onResize.call(this, w, h);

		// need extra room for values
		me.el.addCls('sliderRange');
	
    // create comparison caption div above the slider in the dom
    if (me.compCap){me.compCap.remove();}
    me.compCap = me.el.parent().insertFirst({cls: 'sliderComparison'});

		// build a child div element to hold the tick mark/value div elements
	  if (me.ticks){me.ticks.remove();}
	  me.ticks = me.el.createChild({cls: 'sliderRangeTicks'});

	  // values for tick mark spacing
	  var startPos = me.translateValue(me.minValue);
	  var nextPos = me.translateValue((me.minValue + me.increment));
	  var diff = nextPos - startPos;
	  var counter = 0;
	  // create one child div element per option list value
		me.store.each(function (record) {
			me.ticks.createChild({html: record.get(me.captionField), cls: 'sliderTick', style: 'left: '+(-Math.round(diff/2) + (diff * counter)) +'px; width: '+diff+'px;'});
			counter++;
		}, me);

    // update the comparison caption div as the user changes the selection
		Ext.get(me.compCap.id).update(me.getComparisonValue(me.value));
		
	},
   /**
   *
   */
   setWidth: function (config) {
		var me = this;
    if (!Ext.isDefined(config.width)) {
        config.width = me.defaultWidthMultiple * config.dctOptions.length;
    } else {
        config.width = config.width * config.dctOptions.length;
    }
  },
  /**
  *
  */
  setMaxValue: function (config) {
    config.maxValue = config.dctOptions.length-1;
  },
   /**
   *
   */
   setListeners: function (config) {
		var me = this;
    config.listeners = {
			changecomplete: {
				fn: function(slider, newvalue, thumb) {
					var me = this;
					Ext.getDom(slider.hiddenId).value = slider.getSliderValue(newvalue);
					me.fieldChanged();
					me.fieldValidation();
				},
				scope: me
			},
			change: {
				fn: function(slider, newvalue, thumb) {
					var me = this;
					me.compValueEl.update(slider.getComparisonValue(newvalue));
				},
				scope: me
			}
		};
	},
	/**
	*
	*/
	setInitialValue: function (config) {
		var me = this;
		var initialValue = config.store.find(me.valueField, config.dctInitialValue);
    config.value = (initialValue == -1) ? 0 : initialValue;
	},
   /**
   *
   */
   getComparisonValue: function (valueIndex) {
		var me = this;
		record = me.store.getAt(valueIndex);
    return record.get(me.comparisonCaptionField);
	},
   /**
   *
   */
   getSliderValue: function (valueIndex) {
		var me = this;
		record = me.store.getAt(valueIndex);
    return record.get(me.valueField);
	},
   /**
   *
   */
   setDataStore: function (config) {
		var me = this;
		config.store = Ext.create('Ext.data.ArrayStore', {
        fields: me.storeFields,
        data : config.dctOptions
    });
	},
	fieldOnBlur: function () {
		var me = this;
    me.customOnBlur();
	},
	ajaxPost: function () {
		var me = this;
    me.customOnBlur();
		DCT.ajaxProcess.onBlurPost(me);
	}			  		  
});
DCT.Util.reg('dctcomparisonslider', 'interviewSlider', 'DCT.ComparisonSlider');
