/**
*
*/
Ext.define('DCT.selection.CheckboxModel', {
	alias: 'selection.dctcheckboxmodel',
	extend: 'Ext.selection.CheckboxModel',

	itemsSelectedPerPage: null,
	allSelected: false,
	selectAllItems: false,
	trackPageSelections: true,
	
	constructor: function() {
		var me = this;
		me.itemsSelectedPerPage = Ext.create('Ext.util.MixedCollection', {});
		me.pagingSelection = true;
		me.callParent(arguments);
		me.grid.store.on('load', function(){
			var me = this;
			if (me.trackPageSelections)
					me.assignPageSelections();
		}, me);
	},
	onSelectChange: function(record, isSelected, suppressEvent, commitFn) {
		var me = this;
		me.callParent(arguments);
		me.setPageSelections();
		if (!isSelected)
			me.allSelected = false;

		DCT.Util.getSafeElement('_gridAllSelected').value = me.allSelected;
	},
	onHeaderClick: function(headerCt, header, e) {
		if (header === this.column) {
				e.stopEvent();
				var me = this,
						isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');
				if (isChecked) {
						me.deselectAll();
						me.allSelected = false;
						if (me.selectAllItems) 
							me.clearPageSelections();
						else
							me.setPageSelections();
				} else {
						me.selectAll();
						me.allSelected = (me.selectAllItems) ? true : false;
						me.setPageSelections();            
				}

				DCT.Util.getSafeElement('_gridAllSelected').value = me.allSelected;
		}
	},
	updateHeaderState: function() {
			
			var me = this,
					store = me.store,
					storeCount = store.getCount(),
					views = me.views,
					hdSelectStatus = false,
					selectedCount = 0,
					selected, len, i;
			if (store.getAdjustedCount)
				storeCount = storeCount - store.getAdjustedCount(); 
			if (!store.isBufferedStore && storeCount > 0) {
					selected = me.selected;
					hdSelectStatus = true;
					for (i = 0 , len = selected.getCount(); i < len; ++i) {
							if (store.indexOfId(selected.getAt(i).id) === -1) {
									break;
							}
							++selectedCount;
					}
					hdSelectStatus = storeCount === selectedCount;
			}
			if (views && views.length) {
					me.toggleUiHeader(hdSelectStatus);
			}
	},
	/**
	*
	*/
	setPageSelections : function(){
		var me = this,
				grid = me.grid;
		if (Ext.isDefined(grid)){
			var recordArray = me.getSelection();
			var page = 1;
			var bBar = grid.getDockedItems('dctpagetoolbar');
			if (!Ext.isEmpty(bBar))
				page = bBar[0].getPageData().currentPage;
			me.itemsSelectedPerPage.add(page, recordArray);
		}
	},
	/**
	*
	*/
	assignPageSelections : function(){
		var me = this;
		if(me.locked) return;
		if (me.allSelected){
			me.selectAll();
			me.setPageSelections();
			return;
		}
		var grid = me.grid;
		me.selected.clear();
		var page = 1;
		var bBar = grid.getDockedItems('dctpagetoolbar');
		if (!Ext.isEmpty(bBar))
			page = bBar[0].getPageData().currentPage;
		var pageSelectionArray = me.itemsSelectedPerPage.get(page);
		if (Ext.isDefined(pageSelectionArray)){
			for(var i = 0, len = pageSelectionArray.length; i < len; i++){
					var rowIndex = grid.store.indexOfId(pageSelectionArray[i].id);
					me.select(rowIndex, true);
				}
		}
	},
	/**
	*
	*/
	clearPageSelections : function(){
		var me = this,
				grid = me.grid;
		if (Ext.isDefined(grid)){
			me.itemsSelectedPerPage.clear();
			me.allSelected = false;
		}
	},
	clearSelections: function() {
		var me = this,
				grid = me.grid;
		me.deselectAll();
		if (!me.silent)
			if (me.selectAllItems) 
				me.clearPageSelections();
			else{
				var bBar = grid.getDockedItems('dctpagetoolbar');
				var page = (!Ext.isEmpty(bBar)) ? bBar[0].getPageData().currentPage : 1;
				me.itemsSelectedPerPage.removeAtKey(page);
			}
	},
	isRowSelected: function(record) {
		var me = this;
		return (me.dctIsSelected) ? me.dctIsSelected(record) : me.isSelected(record);
	}	
});
/**
*
*/
Ext.define('DCT.CellEditing', {
		alias: 'plugin.dctcellediting',
		extend: 'Ext.grid.plugin.CellEditing',
		
	clicksToEdit: 1,
	pluginId: 'dctcellediting',

	 /**
	 *
	 */
	constructor: function (config) {
		var me = this;
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
				edit: {
					fn: me.processAfterEdit,
					scope: me
				},
				beforeedit: {
					fn: me.processBeforeEdit,
					scope: me
				}
			};
	},
	/**
	*
	*/
	processAfterEdit: function(editor, editEvent, eOpts) {
		var xpathValue = "fieldInstance[@fieldRef='" + editEvent.field + "']";
		var fieldItems = Ext.DomQuery.select(xpathValue, editEvent.store.proxy.reader.rawData);
		var fieldInstance = (Ext.isEmpty(fieldItems)) ? null : fieldItems[editEvent.rowIdx];
		var uid = Ext.DomQuery.selectValue('@uid', fieldInstance);
		var hiddenInput = Ext.getCmp(uid);
		var oldValue;
		var newValue;
		if (Ext.isDefined(hiddenInput)){
			switch (hiddenInput.dctType){
				case 'date':
					hiddenInput.setValue((!Ext.isEmpty(editEvent.value)) ? Ext.Date.format(editEvent.value, hiddenInput.format) : editEvent.value);
					oldValue = (!Ext.isEmpty(editEvent.originalValue)) ? Ext.Date.format(editEvent.originalValue, hiddenInput.format) : editEvent.originalValue;
					newValue = hiddenInput.getValue();
				break;
				case 'boolean':
					hiddenInput.setValue(editEvent.value);
					oldValue = editEvent.originalValue;
					newValue = editEvent.value;
				break;
				default:
					var formattedValue = (Ext.isDefined(hiddenInput.formatter)) ? hiddenInput.formatter(editEvent.value) : editEvent.value; 
					hiddenInput.setValue(formattedValue);
					oldValue = editEvent.originalValue;
					newValue = formattedValue;
				break;
			}
			if (oldValue != newValue) {
				editEvent.record.commit();
				if (hiddenInput.hasListener('change')){
					hiddenInput.fireEvent('change');
				}
				if (hiddenInput.hasListener('check')) {
					hiddenInput.fireEvent('check', hiddenInput);
				}
				if (hiddenInput.hasListener('select') && (hiddenInput.dctType != 'date')) {
					var dataStore = hiddenInput.getStore();
					var record = dataStore.getAt(dataStore.findExact(hiddenInput.valueField, newValue));
					hiddenInput.fireEvent('select', hiddenInput, record);
				}
				if (hiddenInput.hasListener('blur')) {
					hiddenInput.fireEvent('blur', hiddenInput);
				}
			}
		}
	},
	/**
	*
	*/
	processBeforeEdit: function(editor, editEvent, eOpts) {
		var xpathValue = "fieldInstance[@fieldRef='" + editEvent.field + "']";
		var fieldItems = Ext.DomQuery.select(xpathValue, editEvent.store.proxy.reader.rawData);
		var fieldInstance = (Ext.isEmpty(fieldItems)) ? null : fieldItems[editEvent.rowIdx];
		var uid = Ext.DomQuery.selectValue('@uid', fieldInstance);
		var hiddenInput = Ext.getCmp(uid);
		if (editEvent.column.dataReadOnly || ((Ext.isDefined(hiddenInput)) && hiddenInput.xtype == 'dctinputdisplayfield')){
			return false;
		}
		if (Ext.isDefined(hiddenInput)){
		    editEvent.value = editEvent.originalValue = hiddenInput.getValue();
			if (hiddenInput.hasListener('focus')){
				hiddenInput.fireEvent('focus');
			}
		}
		if (DCT.ajaxProcess.processingBlur){
			editEvent.cancel = true;
			return;
		}
		if (hiddenInput.dctType != 'boolean')
			editEvent.column.setEditor(DCT.Util.getEditorControl(hiddenInput));
	}

});
Ext.define('DCT.GridEditor', {
	override: 'Ext.Editor',
	alignment: "c-c"
});
/**
*
*/
Ext.define('DCT.BaseGridPanel', {
	extend: 'Ext.grid.GridPanel',

	inputXType: 'dctbasegridpanel',
	dctControl: true,
	loadMask: { msg: DCT.T("Loading") },
	originalGridConfig: null,
	dctGroupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? DCT.T("Items") : DCT.T("Item")]})',
	saveButton: 'saveButton',
	cancelButton: 'cancelButton',
	currentStartIndex: 0,
	dctClickedRowIndex: -1,
	xtype: 'dctbasegridpanel',
	forceFit: true,

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setDataStore(config);
		me.setPlugins(config);
		me.setFeatures(config);
		me.setColumnModel(config);
		me.setGridView(config);
		me.setTopToolbar(config);
		me.setBottomToolbar(config);
		me.setListeners(config);
		me.setHubListeners(config);
		me.setPanelButtons(config);
		me.setSelectionModel(config);
		config.store.grid = me;
		me.callParent([config]);

		if (config.dctPreviewGrid) {
			var previewPlugin = me.getPlugin('preview');
			var previewConfig = Ext.isEmpty(config.dctPreviewConfig) ? null : config.dctPreviewConfig;
			var expanded = Ext.isEmpty(previewConfig) || Ext.isEmpty(previewConfig.expanded) ? false : previewConfig.expanded;

			previewPlugin.toggleExpanded(expanded);
		}

		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setHubListeners: function (config) {
	},
	/**
	*
	*/
	setPlugins: function (config) {
		var me = this;
		var pluginItems = [];
		if (config.dctPreviewGrid){
			pluginItems.push(config.dctPreviewConfig); 
		}
		if (config.dctFilteringConfig){
			pluginItems.push(config.dctFilteringConfig); 
		}
		config.plugins = (Ext.isEmpty(pluginItems)) ? undefined : pluginItems;
	},
	/**
	*
	*/
	setFeatures: function (config) {
		var me = this;
		var featureItems = [];
		if (config.dctGroupingConfig){
			featureItems.push(config.dctGroupingConfig); 
		}
		config.features = (Ext.isEmpty(featureItems)) ? undefined : featureItems;
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			rowclick: {
				fn: me.rowClicked,
				scope: me
			},
			boxReady: {
				fn: function (grid, width, height, eOpts) {
					var me = this;
					me.originalGridConfig = me.pullGridState(grid);
					Ext.Function.defer(me.restoreGridState, 10, me, [grid]);
				},
				scope: me
			},
			afterrender: {
				fn: function (grid, eOpts) {
					var me = this;
					Ext.Function.defer(grid.updateLayout, 10, me);
				},
				scope: me
			},
			columnmove: {
				fn: function (ct, column, oldIndex, newIndex, eOpts ) {
					var me = this;
					var saveButton = Ext.getCmp('persistStateButton' + me.id);
					if (saveButton)
						saveButton.enable();
				},
				scope: me
			},
			columnresize: {
				fn: function ( ct, column, width, eOpts) {
					var me = this;
					var saveButton = Ext.getCmp('persistStateButton' + me.id);
					if (saveButton)
						saveButton.enable();
				},
				scope: me
			},
			headerclick: {
				fn: function (ct, column, direction, eOpts) {
					var me = this,
							saveButton = Ext.getCmp('persistStateButton' + me.id);
					if (column.isSortable()){
						if (saveButton)
							saveButton.enable();
							
						var sorters = ct.grid.getStore().getSorters(),
								sortDirection = Ext.getDom('_sortDirection'),
								sortColumn = Ext.getDom('_sortColumn');
						
						if (sorters.getCount() > 0){
							var columnInfo = sorters.getByKey(column.dataIndex);
							sortDirection.value = columnInfo.getDirection();
							sortColumn.value = columnInfo.getProperty();
						} else {
							sortDirection.value = "";
							sortColumn.value = "";							
						}
					}
				},
				scope: me
			}
		};
	},
	
	/**
	*
	*/
	setDataStore: function (config) {
		var me = this;
		var fields = config.dctStoreConfig.fields;
		if (Ext.isEmpty(Ext.Array.findBy(fields, function(item, index){
			if (item.hasOwnProperty('id'))
				return true;
			}, me))){
				config.dctStoreConfig.fields.push({name: 'id', mapping: config.dctStoreConfig.idProperty});
		}
		config.store = Ext.create(config.dctStoreClass, config.dctStoreConfig);
		config.store.on('beforeload', function (store, options) {
			var me = this;
			me.currentStartIndex = options.getStart();
		});		
	},
	/**
	*
	*/
	setColumnModel: function (config) {
		var me = this;
		config.columns = {
			items: config.dctColumnModel,
			sortAscText: DCT.T('SortAscending'), 
			sortDescText: DCT.T('SortDescending'), 
			columnsText: DCT.T('Columns'),
			listeners: {
				columnschanged: {
					fn: function (ct, eOpts) {
						var me = this;
						var saveButton = Ext.getCmp('persistStateButton' + me.id);
						if (saveButton)
							saveButton.enable();
					},
					scope: me
				}
			}
		};
	},
	/**
	*
	*/
	setSelectionModel: function (config) {
		var me = this;
		Ext.apply(config.dctSelectionConfig, {
			grid: me,
		});  	
		config.selModel = config.dctSelectionConfig;
	},
	/**
	*
	*/
	setGridView: function (config) {
		var cellTpl = { cellTpl: [
					'<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} style="width:{column.cellWidth}px;<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
					'<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
					'<tpl if="record.store.grid.view.debugModeOn">',
					'columnRef="{record.store.grid.view.debugId}_{column.dataIndex}" ',
					'</tpl>',
					'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>',
					'<tpl if="values.column.renderer">',
						'{value}',
					'<tpl else>',
						'{[this.checkForXSS(values.record.data[values.column.dataIndex])]}',
					'</tpl>',
					'</div>',
					'</td>',
					{
						priority: 0,
						// member function to check for XSS attack data in cell
						checkForXSS: function (value) {
							if (Ext.util.Format.htmlDecode(value) == value)
								return Ext.util.Format.htmlEncode(value);
							else
								return value;
						}
					}
			]
		};
		if (Ext.isDefined(config.dctViewConfig)) {
			Ext.apply(config.dctViewConfig, cellTpl);
		} else {
			config.dctViewConfig = cellTpl;
		}
		config.viewConfig = config.dctViewConfig;
	},
	/**
	*
	*/
	setTopToolbar: function (config) {
		if (config.dctShowToolBar) {
			config.tbar = Ext.create('DCT.GridToolbar', {
				dctTitle: config.dctTitle,
				dctPreviewGrid: config.dctPreviewGrid,
				dctShowPreviewToolTip: config.dctShowPreviewToolTip,
				dctInitialPreview: config.dctInitialPreview,
				dctCheckBoxSelection: config.dctCheckBoxSelection,
				dctGridCheckboxwithButtons: config.dctGridCheckboxwithButtons,
				dctGridId: config.id
			});
		}
	},
	/**
	*
	*/
	setBottomToolbar: function (config) {
	},
	/**
	*
	*/
	setPanelButtons: function (config) {
		var me = this;
		if (config.dctAddPanelButtons) {
			config.buttons = [{
				text: DCT.Localization.translate('Cancel'),
				id: me.cancelButton,
				handler: function () { 
					var me = this;
					me.ownerCt.closeWindow(); 
				},
				scope: me
			}, {
				text: DCT.Localization.translate('Save'),
				id: me.saveButton,
				handler: function () { 
					var me = this;
					me.saveItems(); 
				},
				scope: me
			}];
		}
	},
	/**
	*
	*/
	saveItems: function () {
		var me = this;
		var keys = new Array();
		var values = new Array();
		var totalSelections = me.getTotalSelectionCount();

		if ((totalSelections != 0) && (totalSelections != me.store.totalCount)) {
			me.assignGridSelection(false, DCT.Grid.buildItemsArray);
			var allItems = DCT.Util.getSafeElement('_gridSelectionArray').value;
			var items = allItems.split(',');
			Ext.each(items, function (item) {
				var itemSplit = item.split('|');
				keys.push(itemSplit[0]);
				values.push(itemSplit[1]);
			});
		}
		else {
			keys.push('');
			values.push(me.dctViewAllText);
		}
		var parentWindow = DCT.Util.getParent();
		parentWindow.Ext.getCmp(me.dctSelected).setValue(values.join(', '));
		parentWindow.Ext.getCmp(me.dctSelectedIds).setValue(keys.join(','));
		me.ownerCt.closeWindow();
	},
	/**
	*
	*/
	rowClicked: function (grid, record, tr, rowIndex, e) {
		if (Ext.isDefined(grid)) {
			if (!grid.dctCheckBoxSelection) {
				DCT.Util.getSafeElement('selectedID').value = record.id;
			}
		}
	},
	/**
	*
	*/
	pullGridState: function (grid) {
		var columns = grid.headerCt.getGridColumns();
		var columnArray = new Array();
		var columnItem;
		for (var i = 0; i < columns.length; i++) {
			columnItem = { id: columns[i].id,  dataIndex: columns[i].dataIndex, hidden: columns[i].isHidden(), width: columns[i].width, flex: columns[i].flex, sortState: columns[i].sortState };
			columnArray[columnArray.length] = columnItem;
		}
		//Paging
		var perPageControl = Ext.getCmp('perpage' + grid.id);
		if (perPageControl) {
			var perPageItem = { dataIndex: 'perpage' + grid.id, value: perPageControl.value };
			columnArray[columnArray.length] = perPageItem;
		}

		var State = Ext.encode(columnArray);
		return State;
	},
	/**
	*
	*/
	restoreGridState: function (grid) {
		var newConfig = Ext.util.Cookies.get(grid.id);
		if (!Ext.isEmpty(newConfig)) {
			newConfig = Ext.decode(newConfig);
			var oldCol, sort;
			var cm = grid.getColumnManager();
			for (var i = 0; i < newConfig.length; i++) {
				if ((grid.getSelectionModel().selType != 'dctcheckboxmodel') || (grid.getSelectionModel().selType == "dctcheckboxmodel" && i > 0)){
					column = cm.getHeaderByDataIndex(newConfig[i].dataIndex);
					if (column) {
						if (column.fullColumnIndex != i)
							cm.headerCt.move(column.fullColumnIndex, i);
						if (newConfig[i].hidden)
							column.hide();
						else
							column.show();
						if (newConfig[i].width)
							column.width = newConfig[i].width;
						if (newConfig[i].flex)
							column.setFlex(newConfig[i].flex);
						if (newConfig[i].sortState)
							sort = { direction: newConfig[i].sortState, dataIndex: newConfig[i].dataIndex };
					}
					
					if (newConfig[i].dataIndex == 'perpage' + grid.id) {
						var pagingValue = newConfig[i].value;
						var perPageControl = Ext.getCmp('perpage' + grid.id);
						var record = perPageControl.findRecord('id', pagingValue);
						var index = perPageControl.store.indexOf(record);
	
						if (index >= 0) {
							perPageControl.setValue(pagingValue);
							var store = grid.getStore();
							store.autoLoad.params.limit = pagingValue;
							if (!sort) {
								perPageControl.setValue(record.id);
								perPageControl.fireEvent('select', perPageControl, record);
							}
						}
					}
				}
			}
		}
		if (sort) {
			grid.store.sort(sort.dataIndex, sort.direction);
		}else{
			if (grid.store.sorters)
				grid.store.sorters.clear();
			grid.view.refresh();
		}
	},
	/**
	*
	*/
	buildSelectedArray: function (item) {
		var me = this;
		if (me.selectObject.appendTS) {
			if (typeof (me.selectObject.processingFunc) == 'function')
				me.selectObject.selectedItems.push(me.selectObject.processingFunc(item) + '|' + item.data['lockingTS']);
			else
				me.selectObject.selectedItems.push(item.id + '|' + item.data['lockingTS']);
		} else {
			if (typeof (me.selectObject.processingFunc) == 'function')
				me.selectObject.selectedItems.push(me.selectObject.processingFunc(item));
			else
				me.selectObject.selectedItems.push(item.id);
		}
	},
	/**
	*
	*/
	assignGridSelection: function (appendTS, functionToGetId) {
		var me = this;
		me.selectObject = {
			selectedItems: new Array(),
			appendTS: appendTS,
			processingFunc: functionToGetId
		};
		if (me.getStore()) {
			var selectModel = me.getSelectionModel();
			if (selectModel.pagingSelection) {
				selectModel.itemsSelectedPerPage.each(function (item, index, length) {
					Ext.each(item, function (item, index) {
						var me = this;
						me.buildSelectedArray(item);
					}, me);
				}, this);
			} else {
				var selectedItems = selectModel.getSelection();
				Ext.each(selectedItems, function (item, index) {
					var me = this;
					me.buildSelectedArray(item);
				}, me);
			}
		}
		DCT.Util.getSafeElement('_gridSelectionArray').value = me.selectObject.selectedItems;
		me.selectObject = null;
	},
	/**
	*
	*/
	getTotalSelectionCount: function (gridData) {
		var me = this;
		var totalSelectedItems = 0;
		if (me.getStore()) {
			var selectModel = me.getSelectionModel();
			if (selectModel.pagingSelection) {
				selectModel.itemsSelectedPerPage.each(function (item, index, length) {
					totalSelectedItems = totalSelectedItems + item.length;
				}, me);
			} else {
				totalSelectedItems = selectModel.getCount();
			}
		}
		return totalSelectedItems;
	},
	/**
	 *
	 */
	reloadData: function () {
		var me = this;
		me.store.oldItems = me.store.data.items;
		DCT.Grid.applyFilterToDataStore(me.store);
	},
	/**
	 *
	 */
	afterReloadData: function (e) {
		var me = this;
		var newItems = [];
		if (!Ext.isEmpty(me.oldItems) && me.oldItems.length < me.data.items.length) {
			Ext.each(me.data.items, function (newItem, newIndex, collection) {
				var found = false;
				Ext.each(me.oldItems, function (oldItem, oldIndex, collection) {
					if (newItem.id === oldItem.id) {
						found = true;
					}
				});
				if (!found) {
					newItems.push(newIndex);
				}
			});
			Ext.each(newItems, function (item, index, collection) {
				var row = me.grid.getView().getRow(item);
				Ext.get(row).highlight();
			});
		}
		if (!Ext.isEmpty(me.oldItems)) {
			DCT.hubEvents.fireEvent('ajaxcomplete');
		}
	},
	/**
	*
	*/
	addDropZone: function () {
		var grid = this;
		if (grid.supportsDragDrop()) {
			var wrap = grid.el.wrap(),
					dropZoneDiv = Ext.get('dropZone'),
					dropZone = null;
			if (dropZoneDiv){
				dropZone = dropZoneDiv.dom;
			}else{
				dropZone = wrap.createChild({
					id: 'dropZone',
					tag: 'div',
				});
			}
			grid.fileUpload = Ext.create('DCT.SystemDropFileField' ,{ renderTo: dropZone});
			var dropZone = Ext.get('dropZone').dom;
			grid.fileUpload.onDrop = Ext.emptyFn;
			dropZone.addEventListener("drop", function (e) { return grid.onDrop(e) }, false);
			dropZone.addEventListener("dragenter", grid.stopEvent, false);
			dropZone.addEventListener("dragleave", grid.stopEvent, false);
			dropZone.addEventListener("dragover", grid.stopEvent, false);
		}else{
			grid.createFileUploadArea(grid);
		}
		grid.bindListeners();
	},
	/**
	*
	*/
	createFileUploadArea: function (grid) {
	},
	/**
	*
	*/
	getFileData: function (grid, file, options) {
	},
	/**
	*
	*/
	onDrop: function (e) {
		var me = this;
		Ext.each(e.dataTransfer.files, function (file, index, collection) { 
			var grid = this;
			e.stopPropagation();
			e.preventDefault();
			grid.fileUpload.onDragLeave(e);
			grid.submitFile(file);
		}, me);
	},
	/**
	*
	*/
	bindListeners: function () {
		var grid = this;
		grid.fileUpload.on('fileselected', function () {
			grid.submitFile();
		}, grid);
	},
	/**
	*
	*/
	stopEvent: function (e) {
		e.stopPropagation();
		e.preventDefault();
	},
	/**
	 *
	 */
	submitFile: function (file) {
		var grid = this;
		var options = {
			url: DCT.Util.getPostUrl(),
			scope: grid,
			headers: {
				'Content-Type': null 
			},
			useDefaultXhrHeader: false,
			processFormData: true,
			success: function (data) {
				grid.reloadData();
			},
			callback: function (options, d, data) {
				grid.handleErrors(data);
				DCT.LoadingMessage.hide();
				grid.fileUpload.reset();
			}
		};
		grid.getFileData(grid, file, options);
		DCT.LoadingMessage.show();
		Ext.Ajax.request(options);
	},
	/**
	*
	*/
	handleErrors: function (data) {
		var errors;
		if (data.responseText.substring(0, 1) === '{') {
			var data = Ext.decode(data.responseText);
			if (!Ext.isEmpty(data) && !Ext.isEmpty(data.error)) {
				Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: data.error }).show();
			}
		}
		else if (data.responseText.search('Server Error') > -1) {
			var doc = data.responseXML || DCT.Util.loadXMLString(data.responseText);
			var title = doc.title;
			if (Ext.isEmpty(title)){
				var node = DCT.Util.getSingleNode(doc, '/html/head/title');
				if (!Ext.isEmpty(node)) {
					title = node.innerHTML;
				}
			}
			if (!Ext.isEmpty(title)) {
				Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: title }).show();
			}
		}
	},
	/**
	*
	*/
	supportsDragDrop: function () {
		var div = document.createElement('div');
		return ('ondragenter' in div && 'ondragleave' in div && 'ondragleave' in div && 'ondrop' in div) && div.addEventListener && window.FileList;
	}

});
DCT.Util.reg('dctbasegridpanel', 'baseGridPanel', 'DCT.BaseGridPanel');

/**
*
*/
Ext.define('DCT.EditorGridPanel', {
	extend: 'DCT.BaseGridPanel',

	inputXType: 'dcteditorgridpanel',
	hidden: false,
	enableColumnMove: true,
	xtype: 'dcteditorgridpanel',

	/**
	*
	*/
	setDataStore: function(config){
		var me = this,
				fields = config.dctStoreConfig.fields;
		if (Ext.isEmpty(Ext.Array.findBy(fields, function(item, index){
			if (item.hasOwnProperty('id'))
				return true;
			}, me))){
				config.dctStoreConfig.fields.push({name: 'id', mapping: config.dctStoreConfig.idProperty});
		}
		config.store = Ext.create('DCT.EditPagingStore', config.dctStoreConfig);
	},
	/**
	*
	*/
	setPlugins: function (config) {
		config.plugins = [{
			ptype: 'dctcellediting'
		}];		
	},
	/**
	*
	*/
	setColumnModel: function(config){
		config.columns = {
			items: config.dctColumnModel,
			sortAscText: DCT.T('SortAscending'), 
			sortDescText: DCT.T('SortDescending'), 
			columnsText: DCT.T('Columns')
		};  	
	},
	/**
	*
	*/
	setGridView: function(config){
		var cellTpl = { cellTpl: [
					'<td class="{tdCls}" {tdAttr} {[Ext.aria ? "id=\\"" + Ext.id() + "\\"" : ""]} style="width:{column.cellWidth}px;<tpl if="tdStyle">{tdStyle}</tpl>" tabindex="-1" {ariaCellAttr} data-columnid="{[values.column.getItemId()]}">',
					'<div {unselectableAttr} class="' + Ext.baseCSSPrefix + 'grid-cell-inner {innerCls}" ',
					'<tpl if="record.store.grid.view.debugModeOn">',
					'columnRef="InterviewGrid_{column.dataIndex}" ',
					'</tpl>',
					'style="text-align:{align};<tpl if="style">{style}</tpl>" {ariaCellInnerAttr}>',
					'<tpl if="values.column.renderer">',
						'{value}',
					'<tpl else>',
						'{[this.checkForXSS(values.record.data[values.column.dataIndex])]}',
					'</tpl>',
					'</div>',
					'</td>',
					{
						priority: 0,
						// member function to check for XSS attack data in cell
						checkForXSS: function (value) {
							if (Ext.util.Format.htmlDecode(value) == value)
								return Ext.util.Format.htmlEncode(value);
							else
								return value;
						}
					}
			]
		};
		if (Ext.isDefined(config.dctViewConfig)){
			Ext.apply(config.dctViewConfig, cellTpl);
		}else{
			config.dctViewConfig = cellTpl;
		}
		config.viewConfig = config.dctViewConfig;
	},
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
			boxready: {
				fn: function (grid, eOpts) {
					var me = this;
					Ext.Function.defer(grid.updateLayout, 10, me);
				},
				scope: me
			}
		};
	},
	/**
	*
	*/
	setBottomToolbar: function(config){
		var me = this;
		if (config.dctPagingBarConfig){
			Ext.apply(config.dctPagingBarConfig, {
				store: config.store
			});
			config.bbar = Ext.create('DCT.EditPageToolbar', config.dctPagingBarConfig);
		}
	}
});

DCT.Util.reg('dcteditorgridpanel', 'interviewEditorGridPanel', 'DCT.EditorGridPanel');

/**
*
*/
Ext.define('DCT.NonPagingGridPanel', {
	extend: 'DCT.BaseGridPanel',

	inputXType: 'dctnonpaginggridpanel',
	xtype: 'dctnonpaginggridpanel'

});
DCT.Util.reg('dctnonpaginggridpanel', 'nonPagingGridPanel', 'DCT.NonPagingGridPanel');

/**
*
*/
Ext.define('DCT.PagingGridPanel', {
	extend: 'DCT.BaseGridPanel',

	inputXType: 'dctpaginggridpanel',
	xtype: 'dctpaginggridpanel',

	/**
	*
	*/
	setBottomToolbar: function (config) {
		if (config.dctPagingBarConfig) {
			Ext.apply(config.dctPagingBarConfig, {
				store: config.store
			});
			config.bbar = Ext.create('DCT.PageToolbar', config.dctPagingBarConfig);
		}
	}
});
DCT.Util.reg('dctpaginggridpanel', 'pagingGridPanel', 'DCT.PagingGridPanel');

/**
*
*/
Ext.define('DCT.BaseXmlStore', {
	extend: 'Ext.data.XmlStore',

	inputXType: 'dctbasexmlstore',
	autoDestroy: true,
	xtype: 'dctbasexmlstore',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setStoreItems(config);
		me.setProxyItems(config);
		me.setListeners(config);
		me.setStartPage(config);
		me.setAdjustedCount(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setStoreItems: function(config){
	},
	/**
	*
	*/
	setAdjustedCount: function(config){
		var me = this;
		if (config.dctGetAdjustedCountMethod)
			config.getAdjustedCount = config.dctGetAdjustedCountMethod;
	},
	/**
	*
	*/
	setProxyItems: function(config){
		config.proxy = {
			type: 'ajax',
			actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
			url: DCT.Util.getProxyUrl(),
			simpleSortMode: true,
			reader: {
				type: 'xml',
				keepRawData: true,
				record: config.record
			}  		
		};
	},
	/**
	*
	*/
	setStartPage: function(config){
		config.currentPage = (config.dctStart>0) ? (config.dctStart/config.pageSize) + 1 : 1;		
	},
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		if (config.proxy.type !== 'memory'){
			config.listeners = {
					beforeload: {
						fn: function(store, operation, eOpts){
							var me = this;
							me.setBeforeLoadParams(store, operation, me.getLocalParams());
						},
						scope: me
					},
					load: {
						fn: function(store, records, successful, operation){
							var me = this,
									timeout = false,
									timeoutNode = null,
									errorNode = null;
							
							if (!successful){
								timeout = true;
							} else {
								if (!Ext.isEmpty(operation.getResponse())){
									timeoutNode = Ext.DomQuery.selectNode("/session[@timedOut]", operation.getResponse().responseXML);
									errorNode = Ext.DomQuery.selectNode("/page/content/errors", operation.getResponse().responseXML);
								}
							}
								
							if (timeout || (timeoutNode)){
								Ext.getDom('_targetPage').value = 'login';
								DCT.Submit.submitForm();
							} else {
								if (errorNode){
									DCT.Util.getSafeElement("_errorXml").value = errorNode.xml;
									DCT.Submit.submitPage("errors");
								}
								if (me.dctLoadMethod)
									me.dctLoadMethod(store, records, successful);
							}
							Ext.Function.defer(function(){
								var me = this;
								store.grid.updateLayout();
							}, 500, me);
						},
						scope: me
					}
				};
		}else{
			config.listeners = {
				refresh: {
					fn: function (store, eOpts ) {
						var me = this;
						var fieldFocus = DCT.Util.getFocusField();
						if (!Ext.isEmpty(fieldFocus)) {
							Ext.Function.defer(DCT.Util.activateField, 10, me, [fieldFocus]);
						}
					},
					scope: me
				}
			};					
		}
	},
	/**
	*
	*/
	getLocalParams: function () {
		var me = this;
		var params = {
			_targetPage: me.dctTargetPage,
			_gridRequest: '1',
			_queryFilterSubSet: me.dctFilterSubSet
		}
		if (DCT.manuscriptID && DCT.topic && DCT.page) {
			Ext.apply(params, {
				_manuScriptID: DCT.manuscriptID,
				_topic: DCT.topic,
				_page: DCT.page
			})
		}
		return params;
	}
});

/**
*
*/
Ext.define('DCT.NonPagingStore', {
	extend: 'DCT.BaseXmlStore',

	inputXType: 'dctnonpagingstore',
	remoteSort : false,
	remoteFilter: false,
	autoLoad: true,
	xtype: 'dctnonpagingstore'
	
});

/**
*
*/
Ext.define('DCT.PagingStore', {
	extend: 'DCT.BaseXmlStore',

	inputXType: 'dctpagingstore',
	remoteSort : true,
	remoteFilter: true,
	xtype: 'dctpagingstore',
	/**
	*
	*/
	setProxyItems: function(config){
		var me = this;
		var gridConfig = Ext.get(config.dctGridId + '_gridHiddenInputs');
		if (!Ext.isEmpty(gridConfig)) {
			config.proxy = {
				type: 'memory',
				data: me.getGridXml(gridConfig.getAttribute('data-grid-xml')),
				reader: {
					type: 'xml',
					keepRawData: true,
					record: config.record,
					totalProperty: config.totalProperty
				}
			};
		}else{
			config.proxy = {
				type: 'ajax',
				actionMethods: {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
				url: DCT.Util.getProxyUrl(),
				simpleSortMode: true,
				reader: {
					type: 'xml',
					keepRawData: true,
					record: config.record,
					totalProperty: config.totalProperty
				}  		
			};			
		}
	},
	/**
	*
	*/
	getGridXml: function (xml) {
		var gridDoc;
		//load XML string code for IE
		if (window.ActiveXObject) {
			gridDoc = new ActiveXObject("Microsoft.XMLDOM");
			gridDoc.setProperty("ProhibitDTD", true);
			gridDoc.async = "false";
			gridDoc.loadXML(xml);
		} else {
			var parser = new DOMParser();
			gridDoc = parser.parseFromString(xml, "text/xml");
		}
		delete xml;
		return (gridDoc.documentElement);
	}	
});

/**
*
*/
Ext.define('DCT.EditPagingStore', {
	extend: 'DCT.PagingStore',

	inputXType: 'dcteditpagingstore',
	anchorObjects: Ext.create('Ext.util.MixedCollection', {}),
	autoLoad: true,
	xtype: 'dcteditpagingstore',
	/**
	*
	*/
	setStartPage: function(config){
		config.currentPage = (config.dctStart) ? (config.dctStart/config.pageSize) + 1 : 1;
	}
	
});

/**
*
*/
Ext.define('DCT.ArrayStore', {
	extend: 'Ext.data.ArrayStore',

	inputXType: 'dctarraystore',
	autoDestroy: true,
	idIndex: 0,
	xtype: 'dctarraystore',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setStoreItems(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setStoreItems: function(config){
		var me = this;
		config.idIndex = me.idIndex;
	}	
});

/**
*
*/
Ext.define('DCT.DataStore', {
	override: 'Ext.data.Store',
/**
*
*/
	setBeforeLoadParams: function (store, operation, extraParams) {
		var me = this;
		var parmsObject = (store.lastOptions.params) ? store.lastOptions.params : {};
		var resetLastOptions = Ext.copyTo({}, parmsObject, [store.proxy.getStartParam(), store.proxy.getLimitParam(), store.proxy.getSortParam(), store.proxy.getDirectionParam()]);
		store.lastOptions.params = resetLastOptions;
		var baseParams = Ext.apply(DCT.Util.buildBaseParams(), extraParams);
		operation.setParams(baseParams);
		if (me.anchorObjects)
			me.anchorObjects.clear();
	}
});

/**
*
*/
Ext.define('DCT.GridToolbar', {
	extend: 'Ext.Toolbar',

	inputXType: 'dctgridtoolbar',
	xtype: 'dctgridtoolbar',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setToolbarItems(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setToolbarItems: function(config){
		var me = this;
		config.items = [config.dctTitle,'->'];
		if (config.dctPreviewGrid)
			config.items.push({
				icon: DCT.imageDir + 'icons\/table_sort.png',
				iconCls: 'x-btn-icon',
				tooltip: config.dctShowPreviewToolTip,
				tooltipType: 'title',
				pressed: config.dctInitialPreview,
				enableToggle:true,
				allowDepress:true,
				id: 'gridPreviewButton' + config.dctGridId,
				toggleHandler: me.togglePreview,
				scope: me
			});
		if (!config.dctCheckBoxSelection || config.dctGridCheckboxwithButtons) {
			config.items.push({
				icon: DCT.imageDir + 'icons\/disk.png',
				iconCls: 'x-btn-icon',
				tooltip: DCT.T('SaveTableSettings'),
				tooltipType: 'title',
				disabled: false,
				id: 'persistStateButton' + config.dctGridId,
				handler: me.persistStateClick,
				scope: me
			});
			config.items.push({
				icon: DCT.imageDir + 'icons\/arrow_rotate_clockwise.png',
				iconCls: 'x-btn-icon',
				tooltip: DCT.T('RestoreDefaults'),
				tooltipType: 'title',
				disabled: false,
				id: 'clearStateButton' + config.dctGridId,
				handler: me.clearStateClick,
				scope: me
			});
		}
		config.items.push({
			icon: DCT.imageDir + 'icons\/help.png',
			iconCls: 'x-btn-icon',
			tooltip: DCT.T("HowToConfigureTableSettings"),
			tooltipType: 'title',
			disabled: false,
			id: 'gridHelpButton' + config.dctGridId,
			handler: me.displayGridHelpPopUp,
			scope: me
		});
	},
	/**
	*
	*/
	togglePreview: function(btn, pressed){
		var me = this;
		var grid = Ext.getCmp(me.dctGridId);
		grid.getPlugin('preview').toggleExpanded(pressed);
	},
	/**
	*
	*/
	persistStateClick: function(button, eventObj){
		var me = this;
		var grid = Ext.getCmp(me.dctGridId);
		me.persistGridState(grid);
		button.disable();
		var tBar = grid.getDockedItems('dctgridtoolbar');
		if (!Ext.isEmpty(tBar)){
			tBar[0].getComponent('clearStateButton' + me.dctGridId).enable();
		}
	},
	/**
	*
	*/
	clearStateClick: function(button, eventObj){
		var me = this;
		var grid = Ext.getCmp(me.dctGridId);
		var dt = Ext.Date.add(new Date(), Ext.Date.DAY, 3650);
		Ext.util.Cookies.clear(me.dctGridId);
		me.setOriginalGridState(grid);
	},
	/**
	*
	*/
	persistGridState: function(){
		var me = this;
		var grid = Ext.getCmp(me.dctGridId);
		var gridState = grid.pullGridState(grid);
		try{
			var dt = Ext.Date.add(new Date(), Ext.Date.DAY, 3650);
			Ext.util.Cookies.set(me.dctGridId,gridState,dt);
		}
		catch(e){
			// Error
		}	
	},
	/**
	*
	*/
	setOriginalGridState: function(grid){
		var originalConfig = Ext.decode(grid.originalGridConfig);
		var column, sort;
		var cm = grid.getColumnManager();
		for (var i = 0; i < originalConfig.length; i++) {
			column = cm.getHeaderByDataIndex(originalConfig[i].dataIndex);
			if (column) {
				if (originalConfig[i].flex && (originalConfig[i].flex != column.flex))
					column.setFlex(originalConfig[i].flex);
				if (column.fullColumnIndex != i) {
					cm.headerCt.move(column.fullColumnIndex, i);
				}
				if (originalConfig[i].hidden && !column.isHidden())
					column.hide();
				if (!originalConfig[i].hidden && column.isHidden())
					column.show();
				if (originalConfig[i].width && (originalConfig[i].width != column.width))
					column.setWidth(originalConfig[i].width);
				if (originalConfig[i].sortState) {
					sort = { direction: originalConfig[i].sortState, dataIndex: originalConfig[i].dataIndex };
				}
			}
			
			if (originalConfig[i].dataIndex == 'perpage' + grid.id) {
				var pagingValue = originalConfig[i].value;
				var perPageControl = Ext.getCmp('perpage' + grid.id);
				var record = perPageControl.findRecord('id', pagingValue);
				var index = perPageControl.store.indexOf(record);

				if (index >= 0) {
					perPageControl.setValue(record.id);
					perPageControl.fireEvent('select', perPageControl, record);
				}
			}
		}
		if (sort){
				grid.store.sort(sort.dataIndex, sort.direction);
		}else{
			grid.store.sorters.clear();
			grid.view.refresh();
		}
	},
	/**
	*
	*/
	displayGridHelpPopUp: function(){
		DCT.Util.removeTabs(true);
		var gridHelpPopup = Ext.getCmp('gridHelpDialog');
		if (gridHelpPopup){
			return;
		}
		var gridHelpDialog = Ext.create('DCT.GridHelpDialog', {
			id: 'gridHelpDialog'
			});
		gridHelpDialog.show();
	}		
});

/**
*
*/
Ext.define('DCT.PageToolbar', {
	extend: 'Ext.PagingToolbar',

	inputXType: 'dctpagetoolbar',
	displayMsg: DCT.T('DynamicResultsFound'),
	emptyMsg: DCT.T('NoResultsToDisplay'),
	beforePageText: DCT.T('Grid_BeforePageText'),
	afterPageText: DCT.T('Grid_AfterPageText'),
	firstText: DCT.T('FirstPage'),
	prevText: DCT.T('PreviousPage'),
	nextText: DCT.T('NextPage'),
	lastText: DCT.T('LastPage'),
	refreshText: DCT.T('Refresh'), 
	displayInfo: true,
	xtype: 'dctpagetoolbar',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setPlugins(config);
		me.setItems(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setPlugins: function(config){
		var pluginItems = [];
		switch (config.dctPagingPluginType){
			case "slider":
				pluginItems.push(Ext.create('Ext.ux.SlidingPager', {})); 
			break;
			case "progress":
				pluginItems.push(Ext.create('Ext.ux.ProgressBarPager', {}));
			break;
			default: 
				pluginItems = undefined;
			break;
		}
		config.plugins = pluginItems;
	},
	/**
	*
	*/
	setItems: function(config){
		var me = this;
		if (config.showItemsPerPageCombo) {
			config.items = [{
				xtype: 'tbseparator'
			},{
				xtype: 'tbtext',
				html: DCT.T('ItemsPerPage')
			},{
				xtype: 'combobox',
				id: 'perpage' + config.dctGridId,
				name: 'perpage' + config.dctGridId,
				width: 45,
				grow: true,
				store: {
					xtype: 'array',
					fields: ['id'],
					data: [
						['10'],
						['25'],
						['50'],
						['100']
					]					
				},
		    listeners: {
		        select: {
		        	fn: function (pagingCombo, record, eOpts) {
								me.pageSize = me.store.pageSize = parseInt(record.get('id'), 10);
								me.store.currentPage = 1;
								me.doRefresh();
								var saveButton = Ext.getCmp('persistStateButton' + me.dctGridId);
								if (saveButton) {
									saveButton.enable();
								}
							},
							acope: me
		        }
		    },				
				mode: 'local',
				value: '10',
				displayField: 'id',
				valueField: 'id',
				editable: false,
				forceSelection: true,
				selectOnFocus: false				
			}];
		}
	}		
});

/**
*
*/
Ext.define('DCT.EditPageToolbar', {
	extend: 'DCT.PageToolbar',

	inputXType: 'dcteditpagetoolbar',
	displayMsg: DCT.T('DynamicResultsFound'),
	emptyMsg: DCT.T("NoResultsToDisplay"),
	displayInfo: true,
	xtype: 'dcteditpagetoolbar',

	/**
	*
	*/
	setPlugins: function(config){
	},
	/**
	*
	*/
	moveFirst: function() {
		var me = this,
				store = me.store;
		store.currentPage = 1;
		me.ajaxPaging(1, 'interview', store.dctGroupIndex);
	},
	/**
	*
	*/
	movePrevious: function() {
		var me = this,
				store = me.store,
				size = store.getPageSize(),
				prev = store.currentPage - 1,
				page = ((prev - 1) * size) + 1;
		me.ajaxPaging(page, 'interview', store.dctGroupIndex);
	},
	/**
	*
	*/
	moveNext: function() {
		var me = this,
				store = me.store,
				size = store.getPageSize(),
				next = store.currentPage + 1,
				page = ((next - 1) * size) + 1;
		me.ajaxPaging(page, 'interview', store.dctGroupIndex);
	},
	/**
	*
	*/
	moveLast: function() {
		var me = this,
				store = me.store,
				size = store.getPageSize(),
				last = me.getPageData().pageCount,
				page = ((last - 1) * size) + 1;
		me.ajaxPaging(page, 'interview', store.dctGroupIndex);
	},
	/**
	*
	*/
	doRefresh: function() {
		var me = this,
				store = me.store,
				size = store.getPageSize(),
				current = store.currentPage,
				page = ((current - 1) * size) + 1;
		me.ajaxPaging(page, 'interview', store.dctGroupIndex);
	},
	 /**
	 *
	 */
	ajaxPaging: function(nextIndex, page, groupIndex){
		if (DCT.Util.validateFields()){
			var pagingGroups = (Ext.getDom('_groupPaging')) ? Ext.getDom('_groupPaging').value : null;
			if (pagingGroups) {
				DCT.Util.getSafeElement('_groupIndex').value = groupIndex;
				DCT.Util.getSafeElement('_groupStart').value = nextIndex;
				DCT.Util.getSafeElement('_pagingGroups').value = pagingGroups;
			}
			Ext.getDom('_iterationStartIndex').value = nextIndex;
			Ext.getDom('_action').value = '';
			Ext.getDom('_pageChange').value = '1';
			Ext.getDom('_targetPage').value = page;
			document.forms[0].action = DCT.postPage;
			document.forms[0].target = "";
			DCT.ajaxProcess.completeContentAjax();
		}
	}			
});

/**
*
*/
Ext.define('DCT.GridHelpDialog', {
	extend: 'Ext.Window',

	dctControl: true,
	inputXType: 'dctgridhelpdialog',
	modal:true,
	plain:true, 
	width:650,
	height:200,
	title:DCT.T('Help'),
	layout:'fit',
	collapsible:false,
	shadow:true,
	stateful: false,
	xtype: 'dctgridhelpdialog',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setItems(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setItems: function(config){
		var me = this;
		config.items = Ext.create('Ext.form.FormPanel', {
		defaultType: 'displayfield',
		bodyBorder: false,
		id : 'gridHelpFormId',
		formId : 'gridHelpForm',
		items: [{
					style: 'margin-top:5px;',
					hideLabel : true,
					anchor:'100%',
					value: 'To configure table settings:'
				},{
					hideLabel : true,
					anchor: '100%',
					value: 'To sort entries by column heading, click the appropriate heading.'
				},{
					hideLabel : true,
					anchor: '100%',
					value: 'To rearrange column order, click the column heading and drag it to the desired position.'
		},{
					hideLabel : true,
					anchor: '100%',
					value: 'To configure other table settings, right-click in the column heading and select the desired option.'
		}],
		buttons: [{
					text: DCT.T('Close'),
					id: 'closeButton',
					handler: function(){
						var me = this;
						me.closeDialog();
					},
					scope: me
				}]        
	});		
	},
	/**
	*
	*/
	setListeners: function(config){
		var me = this;
		config.listeners = {
			show : {
					fn: function(control) {
						Ext.getCmp('closeButton').focus(false, 5);
					},
					scope: me
				},
			close : {
					fn: function() {
						var me = this;
						me.closeDialog();
					},
					scope: me
				},
			hide : {
					fn: function() {
						var me = this;
						me.closeDialog();
					},
					scope: me
				}			
			};
	},
	/**
	*
	*/
	closeDialog: function(){
		var me = this;
		DCT.Util.removeTabs(false);
		me.destroy();
	}
});
DCT.Util.reg('dctgridhelpdialog', 'gridHelpDialog', 'DCT.GridHelpDialog');

