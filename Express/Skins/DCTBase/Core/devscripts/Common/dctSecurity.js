/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	isAuthorized: function (privilege, extRecord) {
		var authorized = false;
		var privilegeNode;
		
		switch(privilege){
			case 'canSecLevel_EntityUser':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='SecLevel_EntityUser']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canSecLevel_Underwriter':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='SecLevel_Underwriter']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canSecLevel_EntityAdmin':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='SecLevel_EntityAdmin']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canSecLevel_CarrierAdmin':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='SecLevel_CarrierAdmin']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAcceptEntitySignupRequests':
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AcceptEntitySignupRequests']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessAsyncQueue':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessAsyncQueue']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessDataTester':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessDataTester']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessDeployment':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessDeployment']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessFields':            		
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessFields']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessForms':            		
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessForms']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessPages':           			
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessPages']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessTables':            		
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AccessTables']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddAttachments':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddAttachments']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddEntityGroups':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddEntityGroups']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddEntityLinesOfBusiness':  
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddEntityLinesOfBusiness']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddEntityRelationships':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddEntityRelationships']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddEntityRoles':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddEntityRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddEntityUsers':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddEntityUsers']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddUserEntities':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddUserEntities']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddUserRoles':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='AddUserRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteUserRoles':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteUserRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAssignTasks':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateAndAssignTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCloseAllTasks':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CloseAllTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCloseOwnedTasks':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CloseOwnedTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canConsumerAccess':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ConsumerAccess']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateClients':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateClients']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateEntities':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateEntities']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateEntityGroupTypes':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateEntityGroupTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateEntityTypes':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateEntityTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreatePortfolios':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreatePortfolios']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreatePrivileges':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreatePrivileges']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateQuotes':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateQuotes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateRoles':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateTasks':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canCreateUsers':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='CreateUsers']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteAllTasks':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteAllTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteAttachments':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteAttachments']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteClients':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteClients']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteEntities':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteEntities']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteEntityGroupTypes':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteEntityGroupTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteEntityRoles':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteEntityRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteEntityTypes':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteEntityTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteNotifications':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteNotifications']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteOwnedTasks':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteOwnedTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeletePortfolios':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeletePortfolios']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeletePrivileges':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeletePrivileges']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteQuotes':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteQuotes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteRoles':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteUsers':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteUsers']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDeleteUserState':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DeleteUserState']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canDuplicatePolicy':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='DuplicatePolicy']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canGetPrintJobHistory':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='GetPrintJobHistory']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canImportPolicyData':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ImportPolicyData']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canLockUserAccounts':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='LockUserAccounts']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canMassUpdate':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='MassUpdate']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canMovePolicies':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='MovePolicies']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canPurgePolicies':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='PurgePolicies']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canPurgeClients':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='PurgeClients']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canReassignPolicyToClient':
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ReassignPolicyToClient']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canReassignTasks':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ReassignTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canSetPrintJobHistory':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='SetPrintJobHistory']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateClient':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateClient']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntities':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntities']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityGroups':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityGroups']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityGroupTypes':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityGroupTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityLinesOfBusiness':
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityLinesOfBusiness']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityRelationships': 
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityRelationships']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityRoles':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntitySignupList':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntitySignupList']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityTypes':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateEntityUsers':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateEntityUsers']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdatePortfolios':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdatePortfolios']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdatePrivileges':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdatePrivileges']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateQuotes':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateQuotes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateRoles':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateUserEntities':  
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateUserEntities']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateUserRoles':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateUserRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateUsers':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateUsers']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canUpdateUserState':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='UpdateUserState']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewClients':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewClients']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewEntity':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewEntity']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewEntityGroups':    
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewEntityGroups']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewEntityList':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewEntityList']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewEntityTypes':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewEntityTypes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewNotifications':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewNotifications']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewPortfolios':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewPortfolios']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewPrivileges':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewPrivileges']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewQuotes':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewQuotes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewRoles':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewTasks':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewTasks']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewUser':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewUser']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewUserList':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='ViewUserList']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessAccountSummary':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_AccountSummary']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessDisbursementDetail':  
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_DisbursementDetail']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessInstallmentSchedule': 
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_InstallmentSchedule']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessItemsInSuspense':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_ItemsInSuspense']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessPaymentDetail':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_PaymentDetail']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessPolicySummary':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_PolicySummary']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessRecWriteOffDetail':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_RecWriteOffDetail']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessUnidentifiedPayments':
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Access_UnidentifiedPayments']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessAllAreas':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_AccessAllAreas']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAccessBatchCashMgmt':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_AccessBatchCashMgmt']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canAddNotes':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_AddNotes']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangeAccountSettings':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangeAccountSettings']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangeDueDay':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangeDueDay']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangePayorOrPayeeRoles':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangePayorOrPayeeRoles']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangeSystemDate':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangeSystemDate']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canErrorQueue':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ErrorQueue']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canHoldBilling':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_HoldBilling']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;   
			case 'canHoldDisbursements':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_HoldDisbursements']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;			
		    case 'CanChangeEftRequests':
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_EftMaintenance']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canMakePaymentInAccount':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_MakePaymentInAccount']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canMakePaymentOutsideAccount': 
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_MakePaymentOutsideAccount']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canManageHoldEvents':          
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ManageHoldEvents']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canManuallyAllocateSuspense':  
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ManuallyAllocateSuspense']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canManuallyRescind':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ManuallyRescind']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canPaymentBatchAdmin':         
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_PaymentBatchAdmin']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canPaymentSearch':     
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_PaymentSearch']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canRebill':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_Rebill']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canRequestDisbursement':       
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_RequestDisbursement']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canReversePayment':            
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ReversePayment']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canSuspendPayment':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_SuspendPayment']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canTransferPayments':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_TransferPayments']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canTransferPolicy':        
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_TransferPolicy']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canTransferToCollections': 
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_TransferToCollections']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewReports':           
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ViewReports']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canWriteOffPayments':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_WriteOffPayments']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canWriteOffReceivables':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_WriteOffReceivables']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canViewInvoice':      
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ViewInvoiceImage']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canReprintInvoice':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ReprintInvoice']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangePolicySettings':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangePolicySettings']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canChangeRenewalSettings':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ChangeRenewalSettings']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;      
			case 'canResuspendDisbursement':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_ResuspendDisbursement']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;   
			case 'canSubmitMPR':   
				privilegeNode = Ext.DomQuery.select("state/user/privileges/privilege[@name='BIL_SubmitMPR']",extRecord.store.proxy.reader.rawData);
				if (privilegeNode.length > 0)
					authorized = true;
			break;  		
		}
		return authorized;
	}  
});
	 