
//PORTAL PANEL================================================
/**
*
*/
Ext.define('Ext.DCTPortalPanel', {
  extend: 'Ext.Panel',
  
  border: false,
  borderBody: false,
	parent: null,
	/**
	*
	*/
	initComponent: function () {
      Ext.DCTPortalPanel.superclass.initComponent.call(this);
			this.setParent = function(parentItem) {
			this.parent = parentItem;
		}
	},
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	* 
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			afterrender: {
				fn: function() { 
							var me = this;
							DCT.hubEvents.addListener('ajaxcomplete', me.reSyncLayout, me);
						},
						scope: me
			}
		};
	},
	/**
	* 
	*/
	reSyncLayout: function () { 
		var me = this;
		me.updateLayout();
	}
});
//============================================================



//PORTAL COLUMN===============================================
/**
*
*/
Ext.define('Ext.DCTPortalColumn', {
  extend: 'Ext.DCTPortalPanel',
  
    layout: 'anchor',
    autoHeight: true,
    defaultType: 'portlet',
    cls:'x-portal-column',
    xtype: 'portalcolumn'
});
//============================================================


//PORTAL ZONE=================================================
/**
*
*/
Ext.define('Ext.DCTPortalZone', {
  extend: 'Ext.DCTPortalPanel',
  
    layout: 'column',
    autoScroll:true,
    autoHeight:true,
    border: false,
    borderBody: false,
    cls:'x-portal',
    defaultType: 'portalcolumn',
    xtype: 'portal',

    /**
	*
	*/
    initComponent: function () {
        Ext.DCTPortalZone.superclass.initComponent.call(this);
//        this.addEvents({
//            validatedrop:true,
//            beforedragover:true,
//            dragover:true,
//            beforedrop:true,
//            drop:true
//        });
    },

    /**
	*
	*/
    initEvents: function () {
        Ext.DCTPortalZone.superclass.initEvents.call(this);
        this.dd = Ext.create('Ext.DCTPortalZone.DropZone', this, this.dropConfig);
    },

    /**
	*
	*/
    beforeDestroy: function () {
        if(this.dd){
            this.dd.unreg();
        }
        Ext.DCTPortalZone.superclass.beforeDestroy.call(this);
    }
});
//============================================================


//PORTAL MODULE===============================================
/**
*
*/
Ext.define('Ext.DCTPortalModule', {
  extend: 'Ext.DCTPortalPanel',
  
    anchor: '100%',
		autoHeight:true,
    frame:true,
    collapsible:true,
    draggable:false,
    style:'margin:5px 5px 10px 5px',
    cls: 'x-portlet'
});
//============================================================


//PORTAL MANAGER==============================================
DCTPortalManager = function (config) {
	var _rootContainer = null;
	var _currentContainer = null;
	var _renderTo = config.renderTo;
	var _lastChildItem = null;
	
	this.add = function(item) {
		if (_currentContainer == null)
		{
			_rootContainer = Ext.create('Ext.DCTPortalPanel', {
										renderTo: _renderTo,
										layout: 'fit'
									});
			_currentContainer = _rootContainer;
		}
		if (item.type == 'module' && !Ext.get(item.contentEl))
		{
			_currentContainer = _lastChildItem;
			return; //exit if the content div doesn't exist
		}
		/*KDK - added ability to have tabPanels inside modules, and 
		  then modules inside those tab panels*/
		if (_currentContainer.type == "tabpanel"){
			var ctID = _currentContainer.items.items[0].id;
			Ext.getCmp(ctID).add(item);
			item.setParent(_currentContainer);
			_currentContainer = item;
		}
		else{
			_currentContainer.add(item);
			item.setParent(_currentContainer);
			_currentContainer = item;
		}
		_lastChildItem = item;
	}
	
	this.activeContainer = function() {
		return _currentContainer;
	}
	
	this.setActiveContainer = function(cn) {
		_currentContainer = cn;
	}
	
	this.popActiveContainer = function() {
		_currentContainer = _currentContainer.parent;
	}
	
	this.render = function() {
		_rootContainer.updateLayout();
	}
}
//============================================================


//PORTAL DROPZONE=============================================
/**
*
*/
Ext.define('Ext.DCTPortalZone.DropZone', {
  extend: 'Ext.dd.DropTarget',
  
	/**
	*
	*/
	constructor: function (portal, cfg) {
        this.portal = portal;
        Ext.dd.ScrollManager.register(portal.body);
        this.callParent([portal.body, cfg]);
        portal.body.ddScrollConfig = this.ddScrollConfig;
    },
    
    ddScrollConfig : {
        vthresh: 50,
        hthresh: -1,
        animate: true,
        increment: 200
    },

    /**
	*
	*/
    createEvent: function (dd, e, data, col, c, pos) {
        return {
            portal: this.portal,
            panel: data.panel,
            columnIndex: col,
            column: c,
            position: pos,
            data: data,
            source: dd,
            rawEvent: e,
            status: this.dropAllowed
        };
    },

    /**
	*
	*/
    notifyOver: function (dd, e, data) {
        var xy = e.getXY(), portal = this.portal, px = dd.proxy;

        // case column widths
        if(!this.grid){
            this.grid = this.getGrid();
        }

        // handle case scroll where scrollbars appear during drag
        var cw = portal.body.dom.clientWidth;
        if(!this.lastCW){
            this.lastCW = cw;
        }else if(this.lastCW != cw){
            this.lastCW = cw;
            portal.updateLayout();
            this.grid = this.getGrid();
        }

        // determine column
        var col = 0, xs = this.grid.columnX, cmatch = false;
        for(var len = xs.length; col < len; col++){
            if(xy[0] < (xs[col].x + xs[col].w)){
                cmatch = true;
                break;
            }
        }
        // no match, fix last index
        if(!cmatch){
            col--;
        }

        // find insert position
        var p, match = false, pos = 0,
            c = portal.items.itemAt(col),
            items = c.items.items, overSelf = false;

        for(var len = items.length; pos < len; pos++){
            p = items[pos];
            var h = p.el.getHeight();
            if(h === 0){
                overSelf = true;
            }
            else if((p.el.getY()+(h/2)) > xy[1]){
                match = true;
                break;
            }
        }

        pos = (match && p ? pos : c.items.getCount()) + (overSelf ? -1 : 0);
        var overEvent = this.createEvent(dd, e, data, col, c, pos);

        if(portal.fireEvent('validatedrop', overEvent) !== false &&
           portal.fireEvent('beforedragover', overEvent) !== false){

            // make sure proxy width is fluid
            px.getProxy().setWidth('auto');

            if(p){
                px.moveProxy(p.el.dom.parentNode, match ? p.el.dom : null);
            }else{
                px.moveProxy(c.el.dom, null);
            }

            this.lastPos = {c: c, col: col, p: overSelf || (match && p) ? pos : false};
            this.scrollPos = portal.body.getScroll();

            portal.fireEvent('dragover', overEvent);

            return overEvent.status;
        }else{
            return overEvent.status;
        }

    },

    /**
	*
	*/
    notifyOut: function () {
        delete this.grid;
    },

    /**
	*
	*/
    notifyDrop: function (dd, e, data) {
        delete this.grid;
        if(!this.lastPos){
            return;
        }
        var c = this.lastPos.c, 
            col = this.lastPos.col, 
            pos = this.lastPos.p,
            panel = dd.panel,
            dropEvent = this.createEvent(dd, e, data, col, c,
                pos !== false ? pos : c.items.getCount());

        if(this.portal.fireEvent('validatedrop', dropEvent) !== false &&
           this.portal.fireEvent('beforedrop', dropEvent) !== false){

            dd.proxy.getProxy().remove();
            panel.el.dom.parentNode.removeChild(dd.panel.el.dom);
            
            if(pos !== false){
                c.insert(pos, panel);
            }else{
                c.add(panel);
            }
            
            c.updateLayout();

            this.portal.fireEvent('drop', dropEvent);

            // scroll position is lost on drop, fix it
            var st = this.scrollPos.top;
            if(st){
                var d = this.portal.body.dom;
                setTimeout(function(){
                    d.scrollTop = st;
                }, 10);
            }

        }
        delete this.lastPos;
    },

    // internal cache of body and column coords
    /**
	*
	*/
    getGrid: function () {
        var box = this.portal.bwrap.getBox();
        box.columnX = [];
        this.portal.items.each(function(c){
             box.columnX.push({x: c.el.getX(), w: c.el.getWidth()});
        });
        return box;
    },

    // unregister the dropzone from ScrollManager
    /**
	*
	*/
    unreg: function () {
        Ext.dd.ScrollManager.unregister(this.portal.body);
        Ext.DCTPortalZone.DropZone.superclass.unreg.call(this);
    }
});

