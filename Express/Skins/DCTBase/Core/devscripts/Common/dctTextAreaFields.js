
/**
*
*/
Ext.define('DCT.TextAreaField', {
  extend: 'Ext.form.TextArea',
  
	defaultWidth: 300,
	defaultHeight: 60,
	inputXType: 'dcttextareafield',
	dctControl: true,
	xtype: 'dcttextareafield',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.value = Ext.util.Format.htmlEncode(me.value);
		me.checkControlExists(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.setRegEx(config);
		if (Ext.isDefined(config.maxWarningLength)) {
			config.maxWarningLengthText = DCT.T("FieldMaxLengthError", { max: config.maxWarningLength });
		}
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			blur: {
				fn: function (field) {
					var me = this;
					me.handleMaxLength(field.getValue());
					me.fieldValidation();
				},
				scope: me
			},
			change: {
				fn: me.fieldChanged,
				scope: me
			},
			focus: {
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			keypress: {
				fn: me.customOnKeyPress,
				scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setAutoCreate: function (config) {
		var me = this;
		var styleWidth = (Ext.isDefined(config.dctWidth)) ? config.dctWidth : me.defaultWidth;
		var isPercent = styleWidth.toString().indexOf('%');
		if (isPercent > -1){
			var percentWidth = styleWidth.substring(0, isPercent);
			var widthNumber = new Number(percentWidth);
			if (widthNumber > 100)
				styleWidth = '100%';
		}
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.width = styleWidth; 
		if (Ext.isDefined(config.rows)) {
			config.height = config.rows * 7 + 20;
			config.rows = 0;
		} else {
			config.height = me.defaultHeight; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**
	*
	*/
	setRegEx: function (config) {
		config.regex = (Ext.isDefined(config.dctRegEx)) ? config.dctRegEx : null;
		config.regexText = (Ext.isDefined(config.dctRegExMsg)) ? config.dctRegExMsg : '';
	},
	/**
	*
	*/
	customOnKeyPress: function () {
	},
	/**
	*
	*/
	setDefaultListeners: function (config) {
	},
	/**
	*
	*/
	handleMaxLength: function (value) {
		var me = this;
		if (value.length > me.maxWarningLength)
			me.setValue(value.substring(0, me.maxWarningLength));
	}
});
DCT.Util.reg('dcttextareafield', 'interviewTextAreaField', 'DCT.TextAreaField');
