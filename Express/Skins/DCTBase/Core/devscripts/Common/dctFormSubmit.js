/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	parmArray: [],

	/**
	*
	*/
	clearParmArray: function () {
		var me = this;
		me.parmArray = [];
	},
	/**
	*
	*/
	addToParmArray: function (arrayId, arrayValue) {
		var me = this;
		var itemArray = new Array(arrayId, arrayValue);
		me.parmArray.push(itemArray);
	},
	/**
	*
	*/
	convertParmArray: function () {
		var me = this;
		if (Ext.isArray(me.parmArray)) {
			for (var i = 0; i < me.parmArray.length; i++) {
				DCT.Util.getSafeElement(me.parmArray[i][0]).value = me.parmArray[i][1];
			}
		}
	},
	/**
	*
	*/
	submitPageForm: function (securePage, linkIdCheck) {
		var me = this;
		if (Ext.isDefined(linkIdCheck)) {
			if (DCT.Util.isButtonDisabled(linkIdCheck))
				return;
		}
		me.convertParmArray();
		me.secureAction(securePage);
		me.submitForm();
	},
	/**
	*
	*/
	submitForm: function (isPopUp) {
		var me = this;
    var _submitAction = Ext.getDom('_submitAction');
    var downloadAction = -1;
    var reportExportAction = -1;

    if ((_submitAction)) {
        downloadAction = _submitAction.value.search('download');
        reportExportAction = _submitAction.value.search('reportExport');
    }
    
    if ((_submitAction) && downloadAction == -1 && reportExportAction == -1
		&& _submitAction.value != "submissionDownload") {
	    DCT.LoadingMessage.delayedShow(true);
	}

	me.customBeforeSubmit();
	document.forms[0].submit();
	me.clearHrefs();
	},
	/**
	*
	*/
	secureAction: function (page) {
		var me = this;
		var action = document.forms[0].action;
		var secure = Ext.getDom('_secure');
		if (secure != null && secure.value == '1')
			if (action.indexOf("https:") == 0) {
				if ((page != "login") && (page != "admin")) {
					var re = /https/g;             //Create regular expression pattern.
					document.forms[0].action = action.replace(re, "http");
				}
			} else if ((page == "admin") && (action.indexOf("http:") == 0)) {
				var re = /http/g;             //Create regular expression pattern.
				document.forms[0].action = action.replace(re, "https");
			}
	},
	/**
	*
	*/
	clearHrefs: function () {
		var submitAction = Ext.getDom('_submitAction');
		if (!Ext.isEmpty(submitAction) && submitAction.value.indexOf('download') > -1)
			return;
		if (!Ext.isEmpty(submitAction) && submitAction.value.indexOf('reportExport') > -1)
			return;
		if (!Ext.isEmpty(submitAction) && submitAction.value.indexOf('submissionDownload') > -1)
			return;
		if (Ext.getDom('_targetPage').value == "preview")
			return;
		DCT.Util.toggleButtons(true);
	},
	/**
	*
	*/
	submitPage: function (page) {
		if (DCT.Util.validateFields(true, true, function() { DCT.Submit.submitPage(page); })) {
			var me = this;
			var waiting = DCT.ajaxProcess.waitForRequests(me);
			if (waiting) {
				return;
			}
			DCT.globalPage = page;
			if (page == "new") {
				DCT.globalSaveAction = "save";
				me.saveVerify(Ext.Function.createDelayed(me.completeSubmitPage, 0, me));
			} else {
				DCT.LoadingMessage.delayedShow();
				me.completeSubmitPage('no');
			}
		}
	},
	/**
	*
	*/
	completeSubmitPage: function (btn) {
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn == 'no') {
			me.setListItems();
			Ext.getDom('_submitAction').value = "";
			Ext.getDom('_targetPage').value = DCT.globalPage;
			me.secureAction(DCT.globalPage);
			me.submitForm();
		}
		else
			if (btn == 'yes')
				me.completeSaveProcess();
	},
	/**
	*
	*/
	submitPagePopup: function (sPage, action, width, height, scrollbars) {
		var me = this;
		DCT.globalPage = sPage;
		DCT.globalAction = action;
		DCT.globalPopupScrollbars = scrollbars;
		DCT.globalPopupWidth = width;
		DCT.globalPopupHeight = height;
		if ((sPage != "save") && (action != "messageDetail")) {
			DCT.globalSaveAction = "save";
			me.saveVerify(Ext.Function.createDelayed(me.completeSubmitPagePopup, 0, me));
		} else
			me.completeSubmitPagePopup('no');
	},
	/**
	*
	*/
	completeSubmitPagePopup: function (btn) {
		var me = this;
		DCT.Util.removeTabs(false);
		if (btn == 'no') {
			var defaultWidth = 800;
			var defaultHeight = 600;
			if (Ext.isEmpty(DCT.globalPopupWidth))
				width = defaultWidth;
			if (Ext.isEmpty(DCT.globalPopupHeight))
				height = defaultHeight;
			DCT.Util.getSafeElement('_popUp').value = '1';
			var pagePopup = Ext.create('DCT.PopupWindow', {
				width: width,
				height: height,
				modal: true,
				dctInterview: true,
				dctScrollbars: DCT.globalPopupScrollbars,
				dctItemClass: 'DCT.PopupPanel'
			});
			pagePopup.show();
		} else
			if (btn == 'yes')
				me.completeSaveProcess();
	},
	/**
	*	
	*/
	completeSaveProcess: function () {
		var me = this;
		if (DCT.savePageSet == '1')
			me.submitPagePopup(DCT.globalSaveAction, "");
		else
			me.submitPageAction(DCT.globalPage, DCT.globalSaveAction);
	},
	/**
	*
	*/
	submitPageAction: function (page, action) {
		if (DCT.Util.validateFields(true, true, function () { DCT.Submit.submitPageAction(page, action); })) {
			var me = this;
			if (action == "upload") {
				document.forms[0].enctype = 'multipart/form-data';
				var CurrentSearch = Ext.getCmp('lastSearchItem');
				if (Ext.isDefined(CurrentSearch))
					CurrentSearch.setValue('(all)');
			}
			Ext.getDom('_targetPage').value = page;
			Ext.getDom('_submitAction').value = action;
			me.secureAction(page);
			me.submitForm();
		}
	},
	/**
	*
	*/
	saveVerify: function (callBackFunction) {
		var me = this;
		if (!Ext.isEmpty(DCT.clientName)) {
			if ((DCT.FieldsChanged.fieldHasChanged()) && (Ext.getDom('_autoSaveState').value != "1")) {
				DCT.Util.removeTabs(true);
				Ext.MessageBox.show({
					title: DCT.T("SaveVerify"),
					msg: DCT.T("SaveConfirmQuote"),
					buttons: Ext.MessageBox.YESNOCANCEL,
					icon: Ext.MessageBox.QUESTION,
					fn: callBackFunction,
					scope: me
				});
				return;
			}
		}
		callBackFunction('no');
		return;
	},
	/**
	*
	*/
	setListItems: function () {
		var me = this;
		if (DCT.currentPage != DCT.globalPage) {
			me.removeKeyNames();
			var oVar = Ext.getDom('_startIndex');
			if (oVar != null)
				oVar.value = 1;
			var oVar = Ext.getDom('_displayCount');
			if (oVar != null)
				oVar.value = 10;
		}
		var CurrentSearch = Ext.getCmp('lastSearchItem');
		if (Ext.isDefined(CurrentSearch))
			CurrentSearch.setValue('(all)');
	},
	/**
	*
	*/
	customBeforeSubmit: function () {
		var me = this;
		me.removeEmptyText();
	},
	/**
	*
	*/
	resetEmptyText: function () {
		var emptyTextFields = Ext.dom.Query.select("input[class*=x-form-empty-field]", Ext.getDom('body'));  
		for (i = 0; i < emptyTextFields.length; i++) {
			var extCmp = Ext.getCmp(emptyTextFields[i].getAttribute('data-componentid'));
			if (extCmp) { extCmp.reset() }
		}
	},
	/**
	*
	*/
	removeEmptyText: function () {
		var emptyTextFields = Ext.dom.Query.select("input[class*=x-form-empty-field]", Ext.getDom('body'));  
		for (i = 0; i < emptyTextFields.length; i++) {
			var extCmp = Ext.getCmp(emptyTextFields[i].getAttribute('data-componentid'));
			if (extCmp) { extCmp.setRawValue("") }
		}
	},
	/**
	*
	*/
	setActiveParent: function (page) {
		DCT.Util.getSafeElement('_activeParentPage').value = page;
	},
	/**
	*
	*/
	adminAction: function (pageType, action) {
		var me = this;
		DCT.Util.getSafeElement('_adminAction').value = action;
		DCT.Util.getSafeElement('_pageType').value = pageType;
		me.submitPageAction("admin", "admin");
	},
	/**
	*
	*/
	submitSaveAction: function (action) {
		var me = this;
		Ext.getDom('_targetPage').value = "";
		me.submitAction(action);
	},
	/**
	*
	*/
	submitAction: function (action) {
		if (DCT.Util.validateFields(true, true, function () { DCT.Submit.submitAction(action); })) {
			var me = this;
			if (action != 'download' && action != "submissionDownload") {
				DCT.LoadingMessage.delayedShow();
			}
			Ext.getDom('_submitAction').value = action;
			me.submitForm();
		}
	},
	/**
	*
	*/
	submitPartyAction: function (action, PartyContextJson) {
		var me = this;
		// PartyContextJson is used to  dynamically add
		// a HTML hidden variable(s) for each pair in the PartyContextJson array. Example: ["_partyRole:PAYEE","_partyIndex:7"]
		if (PartyContextJson) {
			var array = eval("(" + PartyContextJson + ")");
			for (var i = 0; i < array.length; i++) {
				var pair = array[i].split(":");
				DCT.Util.appendHiddenElement(pair[0], pair[1]);
			}
		}
		Ext.getDom('_submitAction').value = action;
		me.submitForm();
	},
	/**
	*
	*/
	setPortal: function (portalName, xsltDir) {
		var me = this;
		var waiting = DCT.ajaxProcess.waitForRequests(me);
		if (waiting) {
			return;
		}

		if ((DCT.BillingSystemURL != DCT.PolicySystemURL) && (portalName != "Policy")) {
			DCT.Util.getSafeElement('_SuppressAutoSave').value = "1";
		}

		if ((portalName == 'Billing') && (DCT.BillingSystemURL != ''))
			document.forms[0].action = DCT.BillingSystemURL;
		else if ((portalName != "Party") && (DCT.PolicySystemURL != ''))
			document.forms[0].action = DCT.PolicySystemURL;

		if (xsltDir != '')
			DCT.Util.getSafeElement('xslt').value = xsltDir;
		DCT.Util.getSafeElement('_PortalName').value = portalName;
		DCT.Util.getSafeElement('_targetPage').value = "";
		DCT.Util.getSafeElement('_autoSaveState').value = "";
		me.submitAction('setPortal');
	},
	/**
	*
	*/
	createConsumerQuote: function () {
		var me = this;
		me.submitPageAction('', 'consumerNewQuote');
	},
	/**
	*
	*/
	viewConsumerQuote: function (quoteID) {
		var me = this;
		DCT.Util.getSafeElement('_QuoteID').value = quoteID;
		me.submitPageAction('', 'consumerLoadQuote');
	},
	/**
	*
	*/
	purchaseConsumerQuote: function (quoteID) {
		var me = this;
		DCT.Util.getSafeElement('_QuoteID').value = quoteID;
		me.submitPageAction('consumerDashboard', 'consumerConvertToPolicy');
	},
	/**
	*
	*/
	printConsumerPolicy: function (policyID, manuscriptLOB) {
		var me = this;
		DCT.Util.getSafeElement('_QuoteID').value = policyID;
		DCT.Util.getSafeElement('_manuscriptLOB').value = manuscriptLOB;
		me.submitPageAction('print', 'load');
	},
	/**
	*
	*/
	removeKeyNames: function () {
		// remove each _keyname (if any) from previous page (admin, etc.)
		var KeyNames = document.getElementsByName('_keyname');
		if (KeyNames) {
			for (var i = 0; i < KeyNames.length; i++) {
				if (KeyNames[i].type == "hidden")
					Ext.removeNode(KeyNames[i]);
				else
					Ext.getCmp('lastSearchItem').setValue('(all)');
			}
		}
		// clear each _orderBy (if any) from previous page (admin, etc.)
		var OrderBy = document.getElementsByName('_orderBy');
		if (OrderBy) {
			for (var i = 0; i < OrderBy.length; i++) {
				Ext.removeNode(OrderBy[i]);
			}
		}
	},
	/**
	*
	*/
	interviewPaging: function (nextIndex, page, groupIndex) {
		var me = this;
		if (DCT.Util.validateFields()) {
			var pagingGroups = (Ext.getDom('_groupPaging')) ? Ext.getDom('_groupPaging').value : null;
			if (pagingGroups) {
				DCT.Util.getSafeElement('_groupIndex').value = groupIndex;
				DCT.Util.getSafeElement('_groupStart').value = nextIndex;
				DCT.Util.getSafeElement('_pagingGroups').value = pagingGroups;
				DCT.Sections.setSectionStates();
			}
			DCT.Util.getSafeElement('_iterationStartIndex').value = nextIndex;
			DCT.Util.getSafeElement('_action').value = '';
			DCT.Util.getSafeElement('_pageChange').value = '1';
			me.submitPage(page);
		}
	},

	reloadCurrentPage: function () {
		DCT.Util.getSafeElement("_action").value = "";
		DCT.Util.getSafeElement("_topic").value = DCT.topic;
		DCT.Util.getSafeElement("_page").value = DCT.page;
		var isInterview = DCT.currentPage == "interview";
		DCT.Util.getSafeElement("_submitAction").value = isInterview ? "jump" : "load";
		DCT.Util.getSafeElement("_targetPage").value = DCT.currentPage;
		DCT.ajaxProcess.completeContentAjax();
		DCT.Util.getSafeElement("_topic").value = "";
		DCT.Util.getSafeElement("_page").value = "";
		DCT.Util.getSafeElement("_submitAction").value = "";
	}
});
