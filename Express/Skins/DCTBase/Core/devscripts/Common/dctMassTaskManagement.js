/**
*
*/
Ext.define('DCT.TaskDateRangePanel', {
  extend: 'Ext.FormPanel',
  
	inputXType: 'dcttaskdaterangepanel',
	bodyBorder: false,
	formId : 'taskDateItemsForm',
	saveButton: 'task_saveButton',
	cancelButton: 'task_cancelButton',
	xtype: 'dcttaskdaterangepanel',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setPanelItems(config);
		me.setPanelButtons(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setPanelItems: function (config) {
		config.items = [{ 			
			layout:'table',
			layoutConfig:{columns: 3},
			defaults:{border: false, width  : 100 },
			items:[{
				id: 'startingDate',
				name: 'startingDate',	
				xtype: 'dctsystemdatefield',
				value: config.dctDisplayDate,
				allowBlank:false,
				hideLabel : true,
				cellCls:'td-startingDate',
				disabled: false
			},{
				id: 'dateSeperator',
				border: false,
				xtype: 'label',
				text: 'and',
				cellCls: 'td-dateSeperator'
			},{
				id: 'endingDate',
				name: 'endingDate',
				xtype: 'dctsystemdatefield',		
				value: config.dctDisplayDate,
				allowBlank:false, 
				cellCls:'td-endingDate', 
				disabled: false	
			},{
				hideLabel : true,
				boxLabel: config.dctAnyDateText,
				xtype: 'checkbox',
				name: '_allDates',
				id: '_allDates',
				anchor:'-18 -80',
				columns: 3
			}]
		}]; 
	},
	/**
	*
	*/
	setPanelButtons: function (config) {
		var me = this;
		config.buttons = [{
			text: DCT.T('Cancel'),
			id: me.cancelButton,
			handler: function(){me.ownerCt.closeWindow();},
			scope: me
		},{
			text: DCT.T('Save'),
			id: me.saveButton,
			disabled: false,
			handler: function(){me.saveItems();},
			scope: me
		}];
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners,{
			keyup: {
				fn: function(textfield, e) {
					me.validateFields(textfield.getValue(), e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	saveItems: function () {	
		var me = this;
		var allDates = Ext.getCmp('_allDates').getValue();	
		if(!allDates && !me.validateDates())
			return;	
		var parentWindow = DCT.Util.getParent();		
		if(allDates) {	
			parentWindow.Ext.getCmp(me.dctDateRangeDisplay).setValue(DCT.T(me.dctAnyDateText));
			parentWindow.Ext.getCmp(me.dctFilter).setValue('');			
		}
		else {		
			parentWindow.Ext.getCmp(me.dctStartDate).setValue( Ext.getCmp('startingDate').getRawValue());
			parentWindow.Ext.getCmp(me.dctEndDate).setValue( Ext.getCmp('endingDate').getRawValue());
			if(Ext.getCmp('startingDate').getRawValue() == Ext.getCmp('endingDate').getRawValue())
				parentWindow.Ext.getCmp(me.dctDateRangeDisplay).setValue(DCT.T(me.dctDateText) + ' ' + Ext.getCmp('startingDate').getRawValue());
			else
				parentWindow.Ext.getCmp(me.dctDateRangeDisplay).setValue(DCT.T(me.dctDateRangeText) + ' ' + Ext.getCmp('startingDate').getRawValue() + ' ' + DCT.T('And') + ' ' + Ext.getCmp('endingDate').getRawValue());			
			parentWindow.Ext.getCmp(me.dctFilter).setValue('dateRange');
		}
		me.ownerCt.closeWindow();	
	},
	/**
	*
	*/
	validateDates: function () {
		var me = this;
		var startDate = Ext.getCmp('startingDate');
		var endDate = Ext.getCmp('endingDate');
		if(!startDate.validate()){	  		
			me.alertInvalidDate('DateUseValidFormat', 'startingDate');
			return false;
		}
		if(!endDate.validate()){	  		
			me.alertInvalidDate('DateUseValidFormat', 'endingDate');
			return false;
		}			
		if(Ext.getCmp('startingDate').getValue() > Ext.getCmp('endingDate').getValue()){	 
			me.alertInvalidDate('DateStartGreaterThanEnd', 'endingDate');
			return false;
		}		
		return true;
	},
	/**
	*
	*/
	alertInvalidDate: function (alertText, focusItem) {
		var me = this;
		Ext.Msg.alert(DCT.T('Alert'), DCT.T(alertText), function(btn){	  			
			Ext.getCmp(focusItem).focus();
		}, me);		
	}	
});

/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	displayTaskQueuePopUp: function (pageSize) {
		var taskQueuePopUp = Ext.getCmp('taskQueuePopUp');
		if (taskQueuePopUp){ 
			return; 
		}
		
		var selectTaskQueuePanel = Ext.create('DCT.PagingGridPanel', { 
			dctColumnModel: [{dataIndex: 'name', width: 85, sortable: false}],
			dctCheckBoxSelection: true,
			dctStoreClass: 'DCT.PagingStore',
			dctStoreConfig: {
				fields: [{name: 'name', type: 'string', mapping: 'Name'}],
				record: 'Queue',
				idProperty: 'EntityId',
				totalProperty: 'QueueList/SearchControl/Paging/TotalCount',
				dctTargetPage: 'taskQueueList',
				pageSize: pageSize,
				dctStart: 0,
				autoLoad: {params:{start: 0,limit: pageSize}}
			},
			dctSelectionConfig: {
				type: 'dctcheckboxmodel',
				checkOnly: true,
				mode: 'MULTI'
			},
			dctPagingBarConfig: { dctPagingPluginType: '', pageSize: pageSize},
			dctSelected : 'adminQueueEntityIdDisplay',
			dctSelectedIds : 'adminQueueEntityId',
			dctViewAllText: DCT.T('AllQueues'),
			dctAddPanelButtons: true
		});			
		var selectTaskQueueWindow = Ext.create('DCT.BaseWindow', {
			title: DCT.T('Queues'),
			width: 500,
			height:400,
			id: 'taskQueuePopUp',
			items: selectTaskQueuePanel,
			dctFocusFieldId: 'cancelButton'
		});
		selectTaskQueueWindow.show();
	},
	/**
	*
	*/
	displayTaskDuePopUp: function () {
		var taskDueDatesPopUp = Ext.getCmp('taskDueDatesPopUp');
		if (taskDueDatesPopUp){ 
			return; 
		}
		
		var displayDateFormat = Ext.get('_hiddenExtJsDateformat').getValue();
		var displayDate = Ext.Date.format(new Date(), displayDateFormat);		
		
		var selectDueDatesPanel = Ext.create('DCT.TaskDateRangePanel', {	
			title: DCT.T('DueOnOrBetween'),
			dctStartDate :  'dueStartDate',
			dctEndDate : 'dueEndDate',
			dctDateRangeDisplay : 'adminFilterDisplay',
			dctAnyDateText : DCT.T('AnyDueDate'),	
			dctFilter: 'adminFilter',
			dctDateRangeText : DCT.T('DueOnOrBetween_lower'),
			dctDateText : DCT.T('Due'),
			dctDisplayDate : displayDate
		});
		var selectTaskDueDatesWindow = Ext.create('DCT.BaseWindow', {
			title: '',
			width:380, 
			height:200, 
			layout:'fit', 
			id: 'taskDueDatesPopUp',
			items: selectDueDatesPanel,
			dctFocusFieldId: 'task_cancelButton'
		});
		selectTaskDueDatesWindow.show();
	},
	/**
	*
	*/
	displayTaskUserPopUp: function (pageSize) {
		var taskSearchUsersPopUp = Ext.getCmp('taskSearchUsersPopUp');
		if (taskSearchUsersPopUp){ 
			return; 
		}
		
		var selectTaskUsersPanel = Ext.create('DCT.PagingGridPanel', { 
			dctColumnModel: [{dataIndex: 'name', width: 85, sortable: false}],
			dctCheckBoxSelection: true,
			dctStoreClass: 'DCT.PagingStore',
			dctStoreConfig: {
				fields: [{name: 'name', type: 'string', mapping: 'FullName'}],
				record: 'QueueUser',
				idProperty: 'UserId',
				totalProperty: 'QueueUserList/Paging/TotalCount',
				dctTargetPage: 'taskQueueUserList',
				pageSize: pageSize,
				dctStart: 0,
				autoLoad: {params:{start: 0,limit: pageSize}}
			},
			dctSelectionConfig: {
				type: 'dctcheckboxmodel',
				checkOnly: true,
				mode: 'MULTI'
			},
			dctPagingBarConfig: { dctPagingPluginType: '', pageSize: pageSize},
			dctSelected : 'adminUserIdsDisplay',
			dctSelectedIds : 'adminAssignedUserId',
			dctViewAllText: DCT.T('AnyUserUnassigned'),
			dctAddPanelButtons: true			
		});	
		var selectTaskUsersWindow = Ext.create('DCT.BaseWindow', {
			title: DCT.T('AssignedTo'),
			width: 500,
			height:400,
			id: 'taskSearchUsersPopUp',
			items: selectTaskUsersPanel,
			dctFocusFieldId: 'cancelButton'
		});
		selectTaskUsersWindow.show();
	},
	/**
	*
	*/
	displayTaskStatusPopUp: function (statusData) {
		var taskSearchStatusPopUp = Ext.getCmp('taskSearchStatusPopUp');
		if (taskSearchStatusPopUp){ 
			return; 
		}

		var selectTaskStatusPanel = Ext.create('DCT.NonPagingGridPanel', { 
			id: "taskStatusList",
			dctColumnModel: [{dataIndex: 'name', width: 85, sortable: false}],
			dctCheckBoxSelection: true,
			dctTitle: '',
			dctStoreClass: 'DCT.ArrayStore',	
			dctStoreConfig: {
				fields: [{name: 'id', type: 'string', mapping: 0},{name: 'name', type: 'string', mapping: 1}],
				data: statusData
			},
			dctSelectionConfig: {
				type: 'dctcheckboxmodel',
				checkOnly: true,
				mode: 'MULTI'
			},	
			dctSelected : 'adminStatusDisplay',
			dctSelectedIds : 'adminStatusFilterId',
			dctViewAllText: DCT.T('AllTasks'),
			dctAddPanelButtons: true			
		});
		var selectTaskStatusWindow = Ext.create('DCT.BaseWindow', {
			title: DCT.T('TaskState'),
			width: 300,
			height:300,
			id: 'taskSearchStatusPopUp',
			items: selectTaskStatusPanel,
			dctFocusFieldId: 'cancelButton'
		});
		selectTaskStatusWindow.show();
	},
	/**
	*
	*/
	displayTaskCategoryPopUp: function (categoryData) {
		var taskSearchCategoryPopUp = Ext.getCmp('taskSearchCategoryPopUp');
		if (taskSearchCategoryPopUp){ 
			return; 
		}
		
		var selectTaskCategoryPanel = Ext.create('DCT.BaseGridPanel', {
			id: "taskCategoryList",
			dctColumnModel: [{dataIndex: 'name', width: 85, sortable: false}],
			dctCheckBoxSelection: true,
			dctTitle: '',
			dctStoreClass: 'DCT.ArrayStore',	
			dctStoreConfig: {
				fields: [{name: 'id', type: 'string', mapping: 0},{name: 'name', type: 'string', mapping: 1}],
				data: categoryData
			},
			dctSelectionConfig: {
				type: 'dctcheckboxmodel',
				checkOnly: true,
				mode: 'MULTI'
			},	
			dctSelected : 'adminCategoryDisplay',
			dctSelectedIds : 'adminCategoryFilterId',
			dctViewAllText: DCT.T('AllCategories'),
			dctAddPanelButtons: true						
		});
		var selectTaskCategoryWindow = Ext.create('DCT.BaseWindow', {
			title: DCT.T('Category'),
			width: 300,
			height:300,
			id: 'taskSearchCategoryPopUp',
			items: selectTaskCategoryPanel,
			dctFocusFieldId: 'cancelButton'
		});
		selectTaskCategoryWindow.show();
	},
	/**
	*
	*/
	displayTaskCreatedOnPopUp: function () {
		var taskCreationDatePopUp = Ext.getCmp('taskCreationDatePopUp');
		if (taskCreationDatePopUp){ 
			return; 
		}
		
		var displayDateFormat = Ext.get('_hiddenExtJsDateformat').getValue();
		var displayDate = Ext.Date.format(new Date(), displayDateFormat);		
		
		var selectCreationDatePanel = Ext.create('DCT.TaskDateRangePanel', {	
			title: DCT.T('CreatedOnOrBetween'),
			dctStartDate :  'createdOnStartDate',
			dctEndDate : 'createdOnEndDate',
			dctDateRangeDisplay : 'adminCreatedOnDisplay',
			dctFilter: 'adminCreatedOnFilter',
			dctDateRangeText : DCT.T('CreatedOnOrBetween_lower'),
			dctDateText : DCT.T('Created'),
			dctAnyDateText : DCT.T('AnyCreationDate'),
			dctDisplayDate : displayDate			
		});
		var selectTaskCreationDatesWindow = Ext.create('DCT.BaseWindow', {
			title: '',
			width:380, 
			height:200, 
			layout:'fit', 
			id: 'taskCreationDatePopUp',
			items: selectCreationDatePanel,
			dctFocusFieldId: 'task_cancelButton'
		});
		selectTaskCreationDatesWindow.show();
	}		
});

/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
	/**
	*
	*/
	massReassignTasks: function (gridId) {
		var gridObj = Ext.getCmp(gridId);
		var selectedTasksQueueAndLockingTS = [];
		var selectedGridRows = gridObj.getSelectionModel().itemsSelectedPerPage.items;

		Ext.each(selectedGridRows, function (pageRecord) {
			Ext.each(pageRecord, function (rowRecord) {
				selectedTasksQueueAndLockingTS.push(rowRecord.id + '|' + rowRecord.get('taskLockingTS'));
			})
		});

		if (selectedTasksQueueAndLockingTS.length == 0) {
			var me = this;
			Ext.Msg.alert(DCT.T('Alert'), DCT.T('NoTasksSelectedForReassignment'), function(btn){
				Ext.getCmp('QueueAdmin').focus();
			}, me);
			return false;
		}

		DCT.Util.getSafeElement('_taskSelectionArray').value = selectedTasksQueueAndLockingTS.join(',');
		DCT.Util.getSafeElement('_detailMode').value = 'reassignDirect';
		DCT.Submit.submitPage('taskMassReassign', '');
	}
});
/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {
	/**
	*
	*/
	buildItemsArray: function (dataRecord) {
		return (DCT.Grid.getDataKeyString(dataRecord, true, ['name']));
	}	
});
