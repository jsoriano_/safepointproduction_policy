/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	getPopUpWidth: function (requestNode) {
		var x = Ext.DomQuery.selectValue('@popUpWidth', requestNode);
		return (Ext.isDefined(x)) ? x : '750';
	},
	/**
	*
	*/
	getPopUpHeight: function (requestNode) {
		var x = Ext.DomQuery.selectValue('@popUpHeight', requestNode);
		return (Ext.isDefined(x)) ? x : '500';
	},
	/**
	*
	*/
	applyCellStyles: function (cellMetaData, dataValue, uid, dataEliminate) {

		//Space issue with IE - Add space if needed
		if (cellMetaData.tdCls.charAt(cellMetaData.tdCls.length - 1) != " ") {
			cellMetaData.tdCls += " ";
		}
		
		var cmp = Ext.getCmp(uid);
		if (Ext.isDefined(cmp)){
			if (!cellMetaData.column.dataReadOnly && !dataEliminate && cmp.xtype != 'dctinputdisplayfield') {
				if (cmp.dctRequired == "1" && Ext.isEmpty(dataValue)) {
					// No value so highlight this cell as in an error state
					cellMetaData.tdCls += ' x-form-invalid ';
					cellMetaData.attr = 'data-qtip="' + DCT.T('FieldRequired') + '"; data-qclass="x-form-invalid-tip"';
				} else {
					cellMetaData.tdCls += "editable ";
				}
			}
			else
				cellMetaData.tdCls += "uneditable ";
		}else{
			if (!cellMetaData.column.dataReadOnly && !dataEliminate)
				cellMetaData.tdCls += "editable ";
			else
				cellMetaData.tdCls += "uneditable ";
		}

		if (cellMetaData.column.dataFieldCls) {
			cellMetaData.tdCls += cellMetaData.column.dataFieldCls;
			cellMetaData.tdCls += ' ';
		}

	},
	/**
	*
	*/
	dateInterviewColumnRender: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, bEditableGrid) {
		var me = this;
		var dataEliminate = me.getEliminate(cellMetaData, extRecord);
		var fieldType = me.getType(cellMetaData, extRecord);
		var uidName = "uid_" + cellMetaData.column.dataIndex;
		var uid = extRecord.get(uidName);
		var field = Ext.getCmp(uid);
		var returnData = (Ext.isEmpty(dataValue) || dataEliminate) ? '' : dataValue;

		if (!dataEliminate){
			if (Ext.isDefined(field)){
				if (!bEditableGrid || cellMetaData.column.dataReadOnly || field.xtype == 'dctinputdisplayfield') {
					returnData = field.getValue();
				} else {
					if (!Ext.isEmpty(dataValue)){
						returnData = field.getRawValue();
					}
				}
			}else{
				if (!Ext.isEmpty(dataValue)){
					var displayDateFormat;
					if (fieldType == 'datetime'){
					    displayDateFormat = Ext.getDom('_hiddenExtJsDateTimeformat').value;
					    if (dataValue.indexOf(" ") > -1) {
					        dataValue = dataValue.replace(" ", "T");
					    }
					    if (dataValue.indexOf("T") > -1) {
					        dataValue = dataValue.substring(0, 19) + "Z";
					    }
						dataValue = new Date(dataValue);
						dataValue = new Date(dataValue.getTime() + (dataValue.getTimezoneOffset() * 60000));
						}else{
						displayDateFormat = Ext.getDom('_hiddenExtJsDateformat').value;
					}
					returnData = Ext.Date.format(dataValue, displayDateFormat);
				}
			}
			DCT.Util.applyCellStyles(cellMetaData, dataValue, uid, dataEliminate);
		}
		return returnData;
	},
	/**
	*
	*/
	textInterviewColumnRender: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, bEditableGrid) {
		var me = this;
		var dataEliminate = me.getEliminate(cellMetaData, extRecord);
		var returnData = (Ext.isEmpty(dataValue) || dataEliminate) ? '' : dataValue;
		if (!dataEliminate){
			var fieldType = me.getType(cellMetaData, extRecord);
			var uidName = "uid_" + cellMetaData.column.dataIndex;
			var uid = extRecord.get(uidName);
			var xpathValue = "fieldInstance[@uid='" + uid + "']";
			var fieldInstance = Ext.DomQuery.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
			var field = Ext.getCmp(uid);
			var quickTip = Ext.DomQuery.selectValue("@qtip", fieldInstance);
			if (Ext.isDefined(quickTip)) {
				cellMetaData.attr += 'title="' + quickTip + '"';
			}
			if (Ext.isDefined(field)){
				if (!bEditableGrid || cellMetaData.column.dataReadOnly){
					returnData = field.getValue();
				} else {
					if (Ext.isDefined(field.formatter) && !Ext.isEmpty(dataValue)){
						returnData = field.formatter(dataValue);
					}
				}
			}else{
				if (fieldType == 'float' || fieldType == 'int') {
					var formatMask = Ext.DomQuery.selectValue("@formatMask", fieldInstance);
					var hiddenFormatMask = Ext.getDom('_hiddenFormatNumMask').value
					if (!Ext.isEmpty(hiddenFormatMask)){
						if (hiddenFormatMask.charAt(0) == '!'){
							formatMask = formatMask + hiddenFormatMask;
						}else{
							formatMask = hiddenFormatMask;
						}
					} else {
						formatMask = null;
					}
					var numberFormat = Ext.create('DCT.Formatting', { numberType: 'float', numberMask: formatMask});
					returnData = numberFormat.formatFloat(dataValue);
				}
				else if (fieldType == 'string') {
					var formatMask = Ext.DomQuery.selectValue("@formatMask", fieldInstance);
					var stringFormatter = Ext.create('DCT.StringFormatting', { stringFormatMask: formatMask });
					returnData = stringFormatter.formatString(dataValue);
				}
			}
			DCT.Util.applyCellStyles(cellMetaData, dataValue, uid, dataEliminate);
		}
		return Ext.util.Format.htmlEncode(returnData);
	},
	/**
	*
	*/
	booleanInterviewBeforeChange: function (column, rowIndex, checked, eOpts) {
		if (column.dataReadOnly) {
				return false;
		}
		return true;
	},
	/**
	*
	*/
	booleanInterviewAfterChange: function (column, rowIndex, checked, eOpts) {
		var grid = column.getView().grid;
		var extRecord = grid.store.getAt(rowIndex);
		var uidName = "uid_" + column.dataIndex;
		var uid = extRecord.get(uidName);
		var hiddenInput = Ext.getCmp(uid);
		if (Ext.isDefined(hiddenInput)){
			hiddenInput.setValue(checked);
		}
	},
	/**
	*
	*/
	booleanInterviewColumnRender: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, bEditableGrid) {
		var me = this;
		var dataEliminate = me.getEliminate(cellMetaData, extRecord);
		var cssPrefix = Ext.baseCSSPrefix,
				cls = cssPrefix + 'grid-checkcolumn';

		if (cellMetaData.column.dataReadOnly || dataEliminate) {
				cellMetaData.tdCls += ' ' + view.disabledCls;
				cellMetaData.tdCls += " uneditable ";
		} else {
				cellMetaData.tdCls += " editable ";
		}
			
		var checked = (typeof(dataValue) == 'boolean') ? dataValue : ((dataValue == '0') ? false : true);
		if (checked) {
				cls += ' ' + cssPrefix + 'grid-checkcolumn-checked';
		}
		return '<img class="' + cls + '" src="' + Ext.BLANK_IMAGE_URL + '"/>';
	},
	/**
	*
	*/
	listInterviewColumnRender: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, bEditableGrid) {
		var me = this;
		var dataEliminate = me.getEliminate(cellMetaData, extRecord);
		var returnData = (Ext.isEmpty(dataValue) || dataEliminate) ? '' : dataValue;
		if (!dataEliminate){
			var uidName = "uid_" + cellMetaData.column.dataIndex;
			var uid = extRecord.get(uidName);
			var xpathValue = "fieldInstance[@uid='" + uid + "']";
			var fieldInstance = Ext.DomQuery.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
	
			if (!bEditableGrid || cellMetaData.column.dataReadOnly) {
				var field = Ext.getCmp(uid);
				if (Ext.isDefined(field)) {
					return field.getValue();
				}
			}
			xpathValue = "list/option[@value='" + dataValue + "']/@caption";
			returnData = Ext.DomQuery.selectValue(xpathValue, fieldInstance);
			DCT.Util.applyCellStyles(cellMetaData, dataValue, uid, dataEliminate);
		}
		return returnData;
	},
	/**
	*
	*/
	getEliminate: function (cellMetaData, extRecord) {
		var uidName = "uid_" + cellMetaData.column.dataIndex;
		var uid = extRecord.get(uidName);
		var xpathValue = "fieldInstance[@uid='" + uid + "']";
		var fieldInstance = Ext.DomQuery.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		xpathValue = "@eliminate";
		var eliminate = Ext.DomQuery.selectValue(xpathValue, fieldInstance);
		return ((!Ext.isEmpty(eliminate) && (eliminate == '1' || eliminate == '-1')) ? true : false);
	},
	/**
	*
	*/
	getReadOnly: function (cellMetaData, extRecord) {
		var uidName = "uid_" + cellMetaData.column.dataIndex;
		var uid = extRecord.get(uidName);
		var xpathValue = "fieldInstance[@uid='" + uid + "']";
		var fieldInstance = Ext.DomQuery.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		xpathValue = "@readOnly";
		var readOnly = Ext.DomQuery.selectValue(xpathValue, fieldInstance);
		return ((!Ext.isEmpty(readOnly) && readOnly == '1') ? true : false);
	},
	/**
	*
	*/
	getType: function (cellMetaData, extRecord) {
		var uidName = "uid_" + cellMetaData.column.dataIndex;
		var uid = extRecord.get(uidName);
		var xpathValue = "fieldInstance[@uid='" + uid + "']";
		var fieldInstance = Ext.DomQuery.selectNode(xpathValue, extRecord.store.proxy.reader.rawData);
		xpathValue = "@type";
		var fieldType = Ext.DomQuery.selectValue(xpathValue, fieldInstance);
		return (fieldType);
	},
	/**
	*
	*/
	getSessionId: function (xmlDoc, resumeRqNode) {
		var sessionId = Ext.DomQuery.selectValue('/page/state/sessionID', xmlDoc);
		return (sessionId != undefined) ? sessionId : Ext.DomQuery.selectValue('@sessionID', resumeRqNode);
	},
	/**
	*
	*/
	getManuscriptId: function (xmlDoc, requestNode) {
		var manuscriptId = Ext.DomQuery.selectValue('/page/content/getPage/properties/@manuscriptID', xmlDoc);
		return (manuscriptId != undefined) ? manuscriptId : Ext.DomQuery.selectValue('@manuscript', requestNode);
	},
	/**
	*
	*/
	checkForDeleteConfirmation: function (requestNode) {
		/* helper to check the possible ways of confirming deletion; 
		early escapes as soon as one evaluates to true, so put the 
		most likely option to the top 
		*/
		return (Ext.DomQuery.selectValue('@AreYouSure', requestNode) == '1'
						|| Ext.DomQuery.selectValue('@areyousure', requestNode) == '1'
						|| Ext.DomQuery.selectValue('@checkDelete', requestNode) == '1'
						|| Ext.DomQuery.selectValue('@checkdelete', requestNode) == '1'
						) ? '1' : '0';
	},
	/**
	*
	*/
	anchorInterviewColumnRenderer: function (dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore) {
		dataValue = (Ext.isEmpty(dataValue)) ? "" : dataValue;
		var anchorObject = {};
		var actionIdName = "actionId_" + cellMetaData.column.dataIndex;
		var actionId = extRecord.get(actionIdName);
		if (actionId==null) { return ""; }
		var xpathValue = "action[@id='" + actionId + "']";
		var fieldItems = Ext.DomQuery.select(xpathValue, extRecord.store.proxy.reader.rawData);
		var action = (Ext.isEmpty(fieldItems)) ? null : fieldItems[0];
		if (Ext.isEmpty(action)) { return ""; }
		var requestsNode = Ext.DomQuery.select('server/requests', action);
		var getPageRqNode, resumeRqNode;
		for (var i = 0; i < requestsNode[0].childNodes.length; i++) {
			if (Ext.isDefined(getPageRqNode) && Ext.isDefined(resumeRqNode)) {
				break;
			}
			if (!Ext.isDefined(getPageRqNode) && requestsNode[0].childNodes[i].nodeName === 'ManuScript.getPageRq') {
				getPageRqNode = requestsNode[0].childNodes[i];
				continue;
			}
			if (!Ext.isDefined(resumeRqNode) && requestsNode[0].childNodes[i].nodeName === 'Session.resumeRq') {
				resumeRqNode = requestsNode[0].childNodes[i];
			}
		}
		anchorObject.actionName = Ext.DomQuery.selectValue('@uid', action);
		anchorObject.actionUrl = Ext.DomQuery.selectValue('@actionUrl', getPageRqNode);
		anchorObject.attachmentId = Ext.DomQuery.selectValue('@attachmentId', getPageRqNode);
		anchorObject.returnPage = Ext.DomQuery.selectValue('@returnPage', getPageRqNode);
		anchorObject.pdfPage = Ext.DomQuery.selectValue('@PDFPage', getPageRqNode);
		anchorObject.preEventField = Ext.DomQuery.selectValue('@preEventField', getPageRqNode);
		anchorObject.preEventFieldAlertOnly = Ext.DomQuery.selectValue('@preEventFieldAlertOnly', getPageRqNode);
		anchorObject.preEventFieldConfirm = Ext.DomQuery.selectValue('@preEventFieldConfirm', getPageRqNode);
		anchorObject.preEventFieldConfirmText = Ext.DomQuery.selectValue('@preEventFieldConfirmText', getPageRqNode);
		anchorObject.isConfirm = Ext.DomQuery.selectValue('@Confirm', getPageRqNode);
		anchorObject.confirmText = (anchorObject.isConfirm == '1') ? Ext.DomQuery.selectValue('@confirmText', getPageRqNode) : undefined;
		anchorObject.popUp = Ext.DomQuery.selectValue('@popUp', getPageRqNode);
		anchorObject.newWindow = Ext.DomQuery.selectValue('@newWindow', getPageRqNode);
		anchorObject.popUpWidth = Number(DCT.Util.getPopUpWidth(getPageRqNode));
		anchorObject.popUpHeight = Number(DCT.Util.getPopUpHeight(getPageRqNode));
		anchorObject.popUpTitle = Ext.DomQuery.selectValue('@popUpTitle', getPageRqNode);
		anchorObject.hideCloseButton = Ext.valueFrom(Ext.DomQuery.selectValue('@hideCloseButton', getPageRqNode), false);
		anchorObject.popUpClass = Ext.valueFrom(Ext.DomQuery.selectValue('@popUpClass', getPageRqNode), '');
		anchorObject.cancelChanges = Ext.DomQuery.selectValue('@cancelChanges', getPageRqNode);

		var actionPage = Ext.DomQuery.selectValue('@page', getPageRqNode);
		var actionTopic = Ext.DomQuery.selectValue('@topic', getPageRqNode);
		var samePage = ((actionPage == DCT.page) && (actionTopic == DCT.topic)) ? '1' : '0';

		anchorObject.ignoreValidation = Ext.DomQuery.selectValue('@ignoreValidation', getPageRqNode);
		anchorObject.skipValidation = (anchorObject.ignoreValidationignoreValidation == '1' || samePage == '1') ? '1' : '0';
		anchorObject.manuscriptId = DCT.Util.getManuscriptId(extRecord.store.proxy.reader.rawData, getPageRqNode);
		anchorObject.sessionId = DCT.Util.getSessionId(extRecord.store.proxy.reader.rawData, resumeRqNode);
		anchorObject.suppressRefresh = Ext.DomQuery.selectValue('@suppressRefresh', getPageRqNode);
		anchorObject.isDelete = DCT.Util.checkForDeleteConfirmation(getPageRqNode);
		anchorObject.postAction = Ext.DomQuery.selectValue('@postAction', getPageRqNode);
		anchorObject.partyId = Ext.DomQuery.selectValue('PartyId', action);
		anchorObject.integrationImportRq = Ext.DomQuery.selectValue('@IntegrationImportRq', getPageRqNode);
		anchorObject.interViewAction = (Ext.isEmpty(anchorObject.postAction)) ? '' : anchorObject.postAction.substring(anchorObject.postAction.indexOf(":") + 1);
		anchorObject.alertMsg = Ext.DomQuery.selectValue('@alertMsg', getPageRqNode);
		anchorObject.alertMsgTitle = Ext.DomQuery.selectValue('@alertMsgTitle', getPageRqNode);
		anchorObject.alertMsgClass = Ext.DomQuery.selectValue('@alertMsgClass', getPageRqNode);
		anchorObject.qtip = Ext.DomQuery.selectValue('@qtip', getPageRqNode);
		anchorObject.isDeleteCmd = (Ext.DomQuery.selectValue('@command', getPageRqNode) == 'delete' || Ext.DomQuery.selectValue('@command', getPageRqNode) == 'markForDelete');
		anchorObject.isList = (extDataStore.dctPageSize > 0);
		anchorObject.runForPortal = Ext.DomQuery.selectValue('@runForPortal', getPageRqNode);

		if (!Ext.isEmpty(anchorObject.alertMsg)) {
			anchorObject.processType = 'alertMsg';
		}
		else if (!Ext.isEmpty(anchorObject.runForPortal)) {
			var portalName = '';
			var me = this;
			if (!Ext.isEmpty(DCT.portals)){
				Ext.each(DCT.portals, function (portal) {
					// By default, Portal types are "PAS", "BIL", and "PTY". Customize this if needed.
					if (portal.portalType.toLowerCase() == anchorObject.runForPortal.toLowerCase()) {
						portalName = portal.name;
					}
				}, me);
			}
			anchorObject.processType = 'runForPortal';
			anchorObject.parameters = {_PortalName: portalName};
			Ext.each(getPageRqNode.attributes, function (attr) {
				if (attr && attr.name) {
					anchorObject.parameters[attr.name] = attr.value;
				}
			});
		}
		else if (Ext.isDefined(anchorObject.actionUrl)) {
			anchorObject.processType = "actionUrl";
		}
		else if (Ext.isDefined(anchorObject.pdfPage)) {
			anchorObject.processType = 'pdfPage';
		}
		else if(anchorObject.interViewAction.search("download") != -1) {
			anchorObject.processType = "download"; 
		}
		else if (anchorObject.interViewAction == 'integrationReturn') {
			anchorObject.processType = 'integrationReturn';
		}
		else if (anchorObject.interViewAction == 'partyReturn') {
			anchorObject.processType = 'partyReturn';
		}
		else if (Ext.isDefined(anchorObject.popUp)) {
			anchorObject.processType = 'popUp';
		}
		else if (anchorObject.interViewAction == 'billingReturn') {
			anchorObject.processType = 'billingReturn';
		}
		else if (Ext.isDefined(anchorObject.newWindow)) {
			anchorObject.processType = 'newWindow';
		}
		else {
			anchorObject.processType = 'default';
		}

		var anchorKey = rowIndex + '_' + colIndex;
		extDataStore.anchorObjects.add(anchorKey, anchorObject);
		var actionId = 'id="' + anchorObject.actionName + '" ';
		var onClickJS = 'DCT.Util.customOnClick(this);DCT.Util.processInterviewGridAnchor(\'' + extDataStore.grid.id + '\',\'' + anchorKey + '\')';
		var anchorClass = Ext.DomQuery.selectValue('@class', getPageRqNode);
		if (!Ext.isDefined(anchorClass))
			anchorClass = "";
		if (!Ext.isDefined(anchorObject.qtip)) {
			anchorObject.qtip = "";
		}
		anchorColumnHTML = '<a id="dctGridLink" href="javascript:;" title="' + anchorObject.qtip + '" onmousemove="window.status=\'\';" onmouseout="window.status=\'\';" ' + actionId + 'onclick="' + onClickJS + '" class="' + anchorClass + '">' + Ext.util.Format.htmlEncode(dataValue) + '</a>';
		return anchorColumnHTML;
	}
});
