/*  Extend the ExtendedComboField combo to include a flag image in the list items */
Ext.define('DCT.LanguageComboField', {
  extend: 'DCT.ExtendedComboField',
  inputXType: 'dctlanguagecombofield',
  inputType: 'text',
  valueNotFoundText: 'English',
  xtype: 'dctlanguagecombofield',

  setListeners: function (config) {
  	var me = this;
  	me.callParent([config]);
    Ext.apply(config.listeners, {
        select: {
            fn: function (combo, record, index) {
				var language = combo.getValue();
				if (DCT.Util.validateFields(true, true, function () { DCT.Submit.selectLanguage(language); })) {
					DCT.Submit.selectLanguage(language);
				} else {
					combo.reset();
				}
            },
            scope: me
        }
    });
  },
  setDataStore: function(config) {
      config.store = Ext.create('Ext.data.ArrayStore', {
          fields: ['display','code','file'],
          data: config.dctStore
      });
  },
  setTemplate: function (config) {
      config.tpl = Ext.create('Ext.XTemplate', 
          '<tpl for=".">',
              '<li role="option" unselectable="on" class="x-boundlist-item languageItem" data-qclass="listcombotip" data-qtip="{display:htmlEncode}">',
                  '<div class="languageComboCaption">{display}</div><div class="languageComboImage" style="background-image: url({[DCT.imageDir]}\icons\/{file});"></div>',
              '</li>',
           '</tpl>'
      );
  }
});
DCT.Util.reg('dctlanguagecombofield', 'languageComboField', 'DCT.LanguageComboField');

