
/**
*
*/
Ext.define('DCT.RadioGroup', {
  extend: 'Ext.form.RadioGroup',
  
	hideLabel: true,
	inputXType: 'dctradiogroup',
	dctControl: true,
	xtype: 'dctradiogroup',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setRadioItems(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setListeners(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
		if (me.getWidth() == 0) {
			me.newWidth = 0;
			me.items.each(function (item) {
				me.newWidth += item.getWidth();
			}, me);
			me.setWidth(me.newWidth);
		}
	},
	/**
	*
	*/
	setRadioItems: function (config) {
		var me = this;
		me.items = [];
		Ext.each(config.dctRadioButtons,function (itemConfig) {
			var me = this;
			var xtypeName = (Ext.isEmpty(itemConfig.dctClassName)) ? 'dctradiofield' : itemConfig.dctClassName;
			Ext.apply(itemConfig, {xtype: xtypeName});
			me.items.push(itemConfig);
		}, me);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			change:{
				fn: function(control, newValue, oldValue, eOpts) {
					var me = this;
					var radioItemName = me.getRadioName(newValue);
					var radioNewItem = me.getRadioItemByObject(newValue, control);
					var radioOldItem = (Ext.isObject(oldValue)) ? me.getRadioItemByObject(oldValue, control) : me.getRadioItemNoObject(radioItemName, oldValue, control);
					if (!Ext.isEmpty(radioNewItem)){
						radioNewItem[0].setHiddenElement();
						control.dctActionId = radioNewItem[0].dctActionId;
						control.dctPartyMappingField = radioNewItem[0].dctPartyMappingField;
					}
					if (!Ext.isEmpty(radioOldItem)){
						radioOldItem[0].setHiddenElement();
					}
					me.fieldValidation();
				},
				scope: me
			}
		};
	},
	/**
	*
	*/
	getRadioItemByObject: function (radioValue, control, radioName) {
		var me = this;
		var radioName = Ext.Object.getKeys(radioValue)[0];
		var radioValue = radioValue[radioName];
		var searchString = '[name=' + radioName + '][inputValue=' + radioValue + ']'
		return(Ext.ComponentQuery.query(searchString, control));
	},
	/**
	*
	*/
	getRadioItemNoObject: function (radioName, radioValue, control) {
		var me = this;
		var searchString = '[name=' + radioName + '][inputValue=' + radioValue + ']'
		return(Ext.ComponentQuery.query(searchString, control));
	},
	/**
	*
	*/
	getRadioName: function (radioValue) {
		var me = this;
		var radioName = (Ext.isObject(radioValue)) ? Ext.Object.getKeys(radioValue)[0] : "";
		return(radioName);
	}	
});
DCT.Util.reg('dctradiogroup', 'interviewRadioGroup', 'DCT.RadioGroup');

/**
*
*/
Ext.define('DCT.RadioField', {
  extend: 'Ext.form.Radio',

	widgetCaptionCls: 'widgetCaption',
	widgetAffectedValueCls: 'widgetAffectedVal',
	defaultExtendedLabel : '<SPAN class="{0}">{1}</SPAN><SPAN class="{2}">{3}</SPAN>',
	inputXType: 'dctradiofield',
	xtype: 'dctradiofield',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		if (config.dctExtendedWidget) {
			me.buildWidgetLabel(config);
		}
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			change:{
				fn: function(control, newValue, oldValue, eOpts) {
					var me = this;
					me.fieldChanged();
				},
				scope: me
			},
			focus:{
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope: me
			},
			boxready: {
				fn: me.setUpChangeEvent,
				scope: me
			},
			specialkey: {
				fn: function (f, e) {
					var me = this;
					switch (e.getKey()){
						case e.TAB:
							DCT.Util.setKeysUsed(true, e.shiftKey);
						break;
					}
				},
				scope: me
			}
		};
	},
	/**
	*
	*/
	setAutoCreate: function (config) {
		var attributeTemplate = 'name="' + config.name + '" ';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.inputAttrTpl = attributeTemplate;
		var checkedString = 'checked';
		if (!config.checked)
			checkedString = '';
		config.afterBodyEl = '<input type="radio" hidden ' + checkedString + ' id="' + config.id + '-hiddenRadioEl" data-ref="hiddenRadioEl" role="radio" data-cmpid="' + config.id + '" ' + attributeTemplate + ' autocomplete="off" hidefocus="true" value="' + config.inputValue + '" />';
	},
	/**
	*
	*/
	buildWidgetLabel: function (config) {
		var me = this;
		config.boxLabel = Ext.String.format(me.defaultExtendedLabel, me.widgetCaptionCls, config.dctCaption, me.widgetAffectedValueCls, config.dctAffectedValue);
  },
	/**
	*
	*/
	setDefaultListeners: function (config) {
	},
	/**
	*
	*/
	setHiddenElement: function () {
		var me = this;
		var hiddenField = Ext.query('input[type=radio]', true, me.bodyEl);
		if (!Ext.isEmpty(hiddenField))
			hiddenField[0].checked = me.getValue();
	},
	/**
	*
	*/
	setUpChangeEvent: function () {
		var me = this;
		me.on("change", me.setHiddenElement, me);
	}		  
   
});
