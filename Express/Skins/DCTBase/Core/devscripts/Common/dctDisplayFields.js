
/**
*
*/
Ext.define('DCT.DisplayField', {
  extend: 'Ext.form.DisplayField',

	inputXType: 'dctinputdisplayfield',
	dctControl: true,
	xtype: 'dctinputdisplayfield',

	htmlEncode: true,

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setAutoCreate(config);
		me.callParent([config]);
	},
	setAutoCreate: function(config) {
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.inputAttrTpl = attributeTemplate;
	}
});
DCT.Util.reg('dctinputdisplayfield', 'interviewDisplayField', 'DCT.DisplayField');

/**
*
*/
Ext.define('DCT.SystemDisplayField', {
  extend: 'DCT.DisplayField',


	inputXType: 'dctsystemdisplayfield',
	dctControl: true,
	xtype: 'dctsystemdisplayfield',

	/**
	*
	*/
	setAutoCreate: function(config){
	}		  
   
});
DCT.Util.reg('dctsystemdisplayfield', 'systemDisplayField', 'DCT.SystemDisplayField');

