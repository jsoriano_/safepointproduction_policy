
/**
*
*/
Ext.define('DCT.SystemComboField', {
  extend: 'DCT.ComboField',
  
 	inputXType: 'dctsystemcombofield',
 	xtype: 'dctsystemcombofield',

 	/**
 	*
 	*/
 	setDefaultListeners: function (config) {
 		var me = this;
    config.listeners = {
			specialkey: function(f,e){
				var me = this;
				switch(e.getKey())
					{
						case e.RETURN:
						case e.ENTER:
						break;
						case e.TAB:
							var fieldSelection = f.getPicker().getSelectionModel();
              if (fieldSelection.getCount() == 1){
                var selRecord = fieldSelection.getSelection()[0];
                var selectedVal = selRecord.get(f.valueField);
                f.setValue(selectedVal);
                f.fireEvent('select', me, selRecord);
              }
						break;
					}
			},
			afterrender: {
				fn: function(control) {
					var me = this;
					me.initialHover(control);
				},
				scope: me
			},
			render: function(combo) {
				combo.inputEl.on('click', Ext.Function.createDelayed(combo.onTriggerClick, 0, combo));
			}
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setDisable: function (config) {
	    config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
	}	
});
DCT.Util.reg('dctsystemcombofield', 'systemComboField', 'DCT.SystemComboField');

//==================================================
// Quick Search Combo control 
//==================================================

/**
*
*/
Ext.define('DCT.QuickSearchComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctquicksearchcombofield',
 	xtype: 'dctquicksearchcombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.showHideSearchField(combo.getValue());
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	showHideSearchField: function (searchType) {
		DCT.Util.hideShowGroup("searchUsingLabel",searchType != "party");
		DCT.Util.hideShowGroup("quickSearchTextId",searchType != "party");
	}
});
DCT.Util.reg('dctquicksearchcombofield', 'quicksearchComboField', 'DCT.QuickSearchComboField');
//==================================================                                         
// Dashboard Search Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.DashboardComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctdashboardsearchcombofield',
 	xtype: 'dctdashboardsearchcombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.showHideDashBoardSearchField(combo.getValue());
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	showHideDashBoardSearchField: function (searchType) {                                                 
		DCT.Util.hideShowGroup("searchNumberGroup", searchType != "party");
	}                                                                                          
});                                                                                          
DCT.Util.reg('dctdashboardsearchcombofield', 'dashboardsearchComboField', 'DCT.DashboardComboField');
//==================================================                                         
// Agency Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.AgencyComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctagencycombofield',
 	xtype: 'dctagencycombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.applyAgencySelection(Ext.getCmp('newList').getStore(), combo.getValue());
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	applyAgencySelection: function (dataStore, fieldValue) {                                                 
		dataStore.loadPage(1, { params:{_AgencyID: fieldValue}, callback:DCT.Util.loadProducerList});
	}                                                                                          
});                                                                                          
DCT.Util.reg('dctagencycombofield', 'agencyComboField', 'DCT.AgencyComboField');
//==================================================                                         
// Message Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.MessageTypeComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctmessagetypecombofield',
 	xtype: 'dctmessagetypecombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
				},
				scope: me
			}
		});
	}                                                                                          
});                                                                                          
DCT.Util.reg('dctmessagetypecombofield', 'messagetypeComboField', 'DCT.MessageTypeComboField');
//==================================================                                         
// Message Query Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.MessageQueryComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctmessagequerycombofield',
 	xtype: 'dctmessagequerycombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.filterMessageList(combo.getValue(), Ext.getCmp('messagesList').getStore());
				},
				scope: me
			}
		});
	}                                                                                          
});                                                                                          
DCT.Util.reg('dctmessagequerycombofield', 'messageQueryComboField', 'DCT.MessageQueryComboField');

//==================================================                                         
// Task Entities Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskEntityComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dcttaskentitycombofield',
 	xtype: 'dcttaskentitycombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					DCT.Util.reloadTaskQueue(combo.getValue(), null, '', 'policy', 'TaskQueue', false, 'skipCheck');
				},
				scope: me
			}
		});
	}                                                                                          
});                                                                                          
DCT.Util.reg('dcttaskentitycombofield', 'taskEntityComboField', 'DCT.TaskEntityComboField');

//==================================================                                         
// Print List Combo control                                                                
//==================================================

/**
*
*/
Ext.define('DCT.PrintListComboField', {
  extend: 'DCT.SystemComboField',
  
    inputXType: 'dctprintlistcombofield',
    xtype: 'dctprintlistcombofield',

    /**
    *
    */
    setListeners: function (config) {
    	var me = this;
        Ext.apply(config.listeners, {
            select: {
                fn: function (combo, record, index) {
                		var me = this;
                    me.reloadPrintJobGrid();
                },
                scope: me
            }
        });
    },
    /**
    *
    */
    reloadPrintJobGrid: function () {
        var showFilter = DCT.Util.getSafeElement("formsShow").value;
        DCT.Util.getSafeElement("_printShowFilter").value = showFilter;
        DCT.Grid.applyFilterToDataStore(Ext.getCmp('printJobList').getStore());
    }
});                                                                                          
DCT.Util.reg('dctprintlistcombofield', 'printListComboField', 'DCT.PrintListComboField');
//==================================================                                         
// Select Agency Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.SelectAgencyComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctselectagencycombofield',
 	xtype: 'dctselectagencycombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					DCT.Util.searchAgency(false, combo.id);
				},
				scope: me
			}
		});
	}	                                                                                          
});                                                                                          
DCT.Util.reg('dctselectagencycombofield', 'selectagencyComboField', 'DCT.SelectAgencyComboField');

//==================================================                                         
// Task Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dcttaskcombofield',
 	xtype: 'dcttaskcombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.reloadTasks(combo);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	reloadTasks: function (combo) {
	}
});                                                                                          
DCT.Util.reg('dcttaskcombofield', 'taskComboField', 'DCT.TaskComboField');

//==================================================                                         
// Task Assigned Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskAssignedComboField', {
  extend: 'DCT.TaskComboField',
  
 	inputXType: 'dcttaskassignedcombofield',
 	xtype: 'dcttaskassignedcombofield',

 	/**
 	*
 	*/
 	reloadTasks: function (combo) {
		DCT.Util.reloadTaskQueue('', null, combo.getValue(), 'assigned', 'QueueAssigned', false, 'skipCheck');
	}                                                             
});                                                                                          
DCT.Util.reg('dcttaskassignedcombofield', 'taskassignedComboField', 'DCT.TaskAssignedComboField');

//==================================================                                         
// Task Filter Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskFilterComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dcttaskfiltercombofield',
 	taskType: 'assigned',
 	xtype: 'dcttaskfiltercombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					DCT.Util.updateForSearch(me.taskType);
				},
				scope: me
			}
		});
	}                                                                                          
});                                                                                          
DCT.Util.reg('dcttaskfiltercombofield', 'taskfilterComboField', 'DCT.TaskFilterComboField');

//==================================================                                         
// Task Unassigned Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskUnassignedComboField', {
  extend: 'DCT.TaskComboField',
  
 	inputXType: 'dcttaskunassignedcombofield',
 	xtype: 'dcttaskunassignedcombofield',

 	/**
 	*
 	*/
 	reloadTasks: function (combo) {
		DCT.Util.reloadTaskQueue(Ext.getCmp('unassignedQueueEntityId').getValue(), null, combo.getValue(), 'unassigned', 'QueueUnassigned', false, 'skipCheck');
	}                                                             
	                                                                                          
});                                                                                          
DCT.Util.reg('dcttaskunassignedcombofield', 'taskunassignedComboField', 'DCT.TaskUnassignedComboField');

//==================================================                                         
// Task Unassigned Queue Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskUnassignedQueueComboField', {
  extend: 'DCT.TaskComboField',
  
 	inputXType: 'dcttaskunassignedqueuecombofield',
 	xtype: 'dcttaskunassignedqueuecombofield',

 	/**
 	*
 	*/
 	reloadTasks: function (combo) {
		DCT.Util.reloadTaskQueue(combo.getValue(), null, Ext.getCmp('unassignedFilterId').getValue(), 'unassigned', 'QueueUnassigned', false, 'skipCheck');
	}                                                             
});                                                                                          
DCT.Util.reg('dcttaskunassignedqueuecombofield', 'taskunassignedqueueComboField', 'DCT.TaskUnassignedQueueComboField');

//==================================================                                         
// Task Unassigned Filter Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskUnassignedFilterComboField', {
  extend: 'DCT.TaskFilterComboField',
  
 	inputXType: 'dcttaskunassignedfiltercombofield',
 	taskType: 'unassigned',
 	xtype: 'dcttaskunassignedfiltercombofield'
});                                                                                          
DCT.Util.reg('dcttaskunassignedfiltercombofield', 'taskunassignedfilterComboField', 'DCT.TaskUnassignedFilterComboField');
//==================================================                                         
// Task Admin Queue Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskAdminQueueComboField', {
  extend: 'DCT.TaskComboField',
  
 	inputXType: 'dcttaskadminqueueComboField',
 	xtype: 'dcttaskadminqueueComboField',

 	/**
 	*
 	*/
 	reloadTasks: function (combo) {
		DCT.Util.reloadTaskQueue(combo.getValue(), Ext.getCmp('adminAssignedUserId').getValue(), Ext.getCmp('adminFilterId').getValue(), 'admin', 'QueueAdmin', false, 'skipCheck');
	}                                                             
});                                                                                          
DCT.Util.reg('dcttaskadminqueueComboField', 'taskadminqueueComboField', 'DCT.TaskAdminQueueComboField');
//==================================================                                         
// Task Admin Queue Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskAdminFilterComboField', {
  extend: 'DCT.TaskComboField',
  
 	inputXType: 'dcttaskadminFilterComboField',
 	xtype: 'dcttaskadminFilterComboField',

 	/**
 	*
 	*/
 	reloadTasks: function (combo) {
		DCT.Util.reloadTaskQueue(Ext.getCmp('adminQueueEntityId').getValue(), Ext.getCmp('adminAssignedUserId').getValue(), combo.getValue(), 'admin', 'QueueAdmin', false, 'skipCheck');
	}                                                             
});                                                                                          
DCT.Util.reg('dcttaskadminFilterComboField', 'taskadminFilterComboField', 'DCT.TaskAdminFilterComboField');
//==================================================                                         
// Task Unassigned Filter Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.TaskAdminComboField', {
  extend: 'DCT.TaskFilterComboField',
  
 	inputXType: 'dcttaskadmincombofield',
 	taskType: 'admin',
 	xtype: 'dcttaskadmincombofield'
});
DCT.Util.reg('dcttaskadmincombofield', 'taskadminComboField', 'DCT.TaskAdminComboField');

//==================================================                                         
// Batch Search Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.BatchSearchTypeComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctbatchsearchtypecombofield',
 	xtype: 'dctbatchsearchtypecombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.displaySearchField(record);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	displaySearchField: function (record) {
		var value = record.data.code;
		var	dateField = Ext.getCmp('lastSearchDateVal')
		var	textField = Ext.getCmp('lastSearchVal')
		if (value == 'Quote.EffectiveDate' || value =='SentDate'){
			dateField.setVisible(true);
			dateField.setDisabled(false);
			textField.setVisible(false);			
			textField.setDisabled(true);			
		}else{
			dateField.setVisible(false);
			dateField.setDisabled(true);
			textField.setVisible(true);			
			textField.setDisabled(false);			
		}
	}
});
DCT.Util.reg('dctbatchsearchtypecombofield', 'batchSearchTypeComboField', 'DCT.BatchSearchTypeComboField');
//==================================================                                         
// Policy Search Type Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.PolicySearchTypeComboField', {
  extend: 'DCT.SystemComboField',
  
    inputXType: 'dctpolicysearchtypecombofield',
    xtype: 'dctpolicysearchtypecombofield',

    /**
    *
    */
    setListeners: function (config) {
    		var me = this;
        Ext.apply(config.listeners, {
            select: {
                fn: function (combo, record, index) {
                		var me = this;
                    me.displaySearchField(record, index);
                },
                scope: me
            }
        });
    },
    /**
    *
    */
    displaySearchField: function (record, index) {
    		var me = this;
        var value = record.data.code;

        var id = me.id.substring(me.id.length - 1, me.id.length);
        var dateField = Ext.getCmp('_keyvalueDateSearch' + id);
        var textField = Ext.getCmp('_keyvalueTextSearch' + id);

        if (value == 'EffectiveDate' || value == 'Quote.ExpirationDate' || value == 'Quote.DateAccessed') {
            dateField.setVisible(true);
            dateField.setDisabled(false);
            textField.setVisible(false);
            textField.setDisabled(true);
        } else {
            dateField.setVisible(false);
            dateField.setDisabled(true);
            textField.setVisible(true);
            textField.setDisabled(false);
        }
    }
});
DCT.Util.reg('dctpolicysearchtypecombofield', 'policySearchTypeComboField', 'DCT.PolicySearchTypeComboField');         
//==================================================                                         
// Comments Filter Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.CommentsFilterComboField', {
  extend: 'DCT.SystemComboField',
  
 	inputXType: 'dctcommentsfiltercombofield',
 	taskType: 'assigned',
 	xtype: 'dctcommentsfiltercombofield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			select: {
				fn: function(combo, record, index) {
					var me = this;
					me.reloadCommentQueue(combo.getValue());
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	reloadCommentQueue: function (value) {
		var me = this;
    DCT.Util.getSafeElement("_filterType").value = value;
    var gridDataStore = DCT.Util.getDataStore(me.dctGridId);
    DCT.Grid.applyFilterToDataStore(gridDataStore);
	}                                                                                          
});                                                                                          
DCT.Util.reg('dctcommentsfiltercombofield', 'commentsFilterComboField', 'DCT.CommentsFilterComboField');
//==================================================                                         
// New Agency Signup Country Combo control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.CountryComboField', {
  extend: 'DCT.SystemComboField',

  inputXType: 'dctcountrycombofield',
  xtype: 'dctcountrycombofield',
  taskType: 'assigned',

  /**
 	*
 	*/
  setListeners: function (config) {
      config.listeners = {
          select: {
              fn: function (combo, record, index) {
              	var me = this;                    
                me.changeFieldVType();
              },
              scope: me
          }
      };
  },
  /**
	*
	*/
  changeFieldVType: function () {
  	var me = this;
    Ext.getCmp('zipId').setRawValue('');
    Ext.getCmp('phoneId').setRawValue('');
    if (Ext.isEmpty(me.value) || me.value == 'US') {
      me.applyVType('zipId', 'zipcode');
      me.applyVType('phoneId', 'phone');            
    } else {
      me.applyVType('zipId', 'zipcode' + me.value.toLowerCase());
      me.applyVType('phoneId', 'phone' + me.value.toLowerCase());
    }
    me.addRemoveFilterKeys('zipId');
    me.addRemoveFilterKeys('phoneId');
  },
    /**
   *
   */
  applyVType: function (fieldId, vType) {
    var field = Ext.getCmp(fieldId);
    if (Ext.isDefined(Ext.form.VTypes[vType])) {
        field.vtype = vType;
        field.maskRe = Ext.form.VTypes[vType + 'Mask'];
    } else {
        field.vtype = null;
        field.maskRe = null;
    }
  },
    /**
   *
   */
  addRemoveFilterKeys: function (fieldId) {
    var field = Ext.getCmp(fieldId);
    if (Ext.isEmpty(field.vtype)) {
        field.mun(field.el, 'keypress', field.filterKeys, field);
    } else {
	    var found, mon;
	    for (var i = 0, len = field.mons.length; i < len; ++i) {
	      mon = field.mons[i];
	      if (field.el === mon.item && 'keypress' == mon.ename && field.filterKeys === mon.fn && field === mon.scope) {                    
	         found = true;
	         break;
	      }
	    }
	    if (!found)
	        field.mon(field.el, 'keypress', field.filterKeys, field);
    }
  }
});
DCT.Util.reg('dctcountrycombofield', 'countryComboField', 'DCT.CountryComboField');
//==================================================                                         
// Product Combo control                                                                
//==================================================                                         

/**
*
*/
Ext.define('DCT.ProductComboField', {
	extend: 'DCT.SystemComboField',

	inputXType: 'dctproductcombofield',
	xtype: 'dctproductcombofield',
	/**
 	*
 	*/
	setDataStore: function (config) {
		config.store = Ext.create('Ext.data.ArrayStore', {
			fields: ['code', 'display', 'xsltDir'],
			data: config.dctPortalStore
		});
	},
	/**
 	*
 	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners, {
			select: {
				fn: function (combo) {
					DCT.Submit.setPortal(combo.value, combo.selection.data.xsltDir);
				},
				scope: me
			}
		});
	},
	listConfig: {
		cls: 'applicationName'
	}
});
DCT.Util.reg('dctproductcombofield', 'productComboField', 'DCT.ProductComboField');
