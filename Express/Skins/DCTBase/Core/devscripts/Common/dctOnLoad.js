/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
		/**
	*
	*/
	processOnLoad: function () {
		var me = this;
		if (!Ext.QuickTips.isEnabled()) { Ext.QuickTips.init() }
		me.setFormEncoding();
		me.initializePage();
		DCT.Sections.initialize();
		me.onLoadFromPopupCheck();
		me.resumeTabSession();
		me.customOnload();
		me.setGuidPropertiesForTab();
	},
		/**
	*
	*/
	customOnload: function () {
	},
		/**
	*
	*/
	customOnClick: function (field) {
		},
		/**
	*
	*/
	getApplication: function () {
		var me = this,
				appName = 'duckcreek';
		
		if (!Ext.isEmpty(DCT.portals)){
			Ext.each(DCT.portals, function (portal) {
				if (portal.active === '1'){
					switch (portal.portalType.toLowerCase()){
						case 'pas':
							appName = 'policy';
						return false;
						case 'bil':
							appName = 'billing';
						return false;
						case 'bilact':
							appName = 'billing';
						return false;
						case 'pty':
							appName = 'duckcreekparty';
						return false;
						default:
							appName = 'duckcreek';
						return false;
					}
				}
			}, me);
		}
		return appName;
	},
		/**
	*
	*/
	onLoadFromPopupCheck: function () {
			var parentWindow = DCT.Util.getParent();
			if ((DCT.currentPage == "login" || DCT.currentPage == "dashboardHome") && parentWindow != self) {
					if (parentWindow.DCT) {
							DCT.LoadingMessage.show();
							parentWindow.DCT.Submit.submitPage(DCT.currentPage);
							return;
					}
			}
	},
		/**
	*
	*/
	setFormEncoding: function () {
			var formField;
			var inputFileFields = Ext.query('input[type=file]');
			if (inputFileFields.length > 0) {
					Ext.getDom('mainForm').encoding = 'multipart/form-data';
			}
			else {
            Ext.getDom('mainForm').encoding = 'application/x-www-form-urlencoded';
        }
    },
    /**
	*
	*/
	initializePage: function () {
			var setDefaultFocus = true;
			DCT.Util.toggleButtons(false);
			try {
					if (DCT.notifyMessage) {
							DCT.Util.removeTabs(true);
							Ext.create('DCT.AlertMessage', {
									msg: DCT.notifyMessage,
									fn: function () { 
										DCT.Util.removeTabs(false); 
										if (DCT.redirectToFormsLogin){
											DCT.Submit.submitForm();
										}
									}
							}).show();
					}
					if (DCT.closePopup) {
							var popupWindow = Ext.getCmp('dctPopupWindow');
							if (popupWindow)
									popupWindow.closePopupWindow();
					}
					var focusField = DCT.Util.getFocusField();
					if (!Ext.isEmpty(focusField)) {
							var domElem = Ext.getDom(focusField);
							if (!Ext.isEmpty(domElem)) {
									if (!DCT.Util.isInterviewControl(focusField)) {
											domElem.focus(5);
											setDefaultFocus = false;
									} else {
											// field exists already and just needs to be assigned focus back 
											//to it since the page might have been redrawn during an ajax process
											var fieldCmp = Ext.getCmp(focusField);
											if (fieldCmp) {
													fieldCmp.focus(5);
													setDefaultFocus = false;
											}
									}
							}
					}
					// If focus was not set to a particular field set it to the first
					// element on the page that can receive focus.				
					if (setDefaultFocus) {
							var popup = DCT.Util.getSafeElement('_popUp').value;
							// Popups need a little time to render before setting the focus, otherwise you can get an invalid data error.
							if (popup == 'true' || popup == '1')
									DCT.Util.findAndSetFocus(500);
							else
									DCT.Util.findAndSetFocus();
					}
					// clear action element on load...
					Ext.getDom('_action').value = '';
			}
			catch (e) {
					// Error
			}
	},
	/**
	*
	*/
	resumeTabSession: function() {
		if (DCT.currentPage == "login" && window.name.search('TabSession-') > -1 && DCT.Util.getSafeElement("_newTabOpened").value === '1') {
			document.body.style.visibility = 'hidden';
			DCT.Util.setWindowGuid(window.name.replace('TabSession-',''));
			DCT.Submit.submitPage();
		}
	},
	/**
	*
	*/
	setGuidPropertiesForTab: function () {
		if (window.name === 'EndSession') {
			// Just showing error message in new tab or popup.
			// Next user action should start a new tabbed session.
			DCT.Util.setWindowGuid("");
			window.name = "";
		} else if (DCT.Util.getSafeElement("_popUp").value === 'false') {
			var windowGuid = DCT.Util.getWindowGuid();
			if (Ext.isEmpty(window.name) && Ext.isEmpty(window.opener) && !Ext.isEmpty(DCT.sessionID) && DCT.Util.getSafeElement('_newTabOpened').value === '0') {  
				// User duplicated a tab. Start new tabbed session.
				window.name = 'TabSession-' + windowGuid;
				DCT.LoadingMessage.show();
				DCT.Util.getSafeElement('_duplicateWindow').value = '1';
				DCT.Submit.submitPage(DCT.currentPage);
			}
			else if ((Ext.isEmpty(window.name) || window.name != windowGuid) && DCT.currentPage != "login") {
				// New tab. 
				window.name = 'TabSession-' + windowGuid;
			}
		}
	}
});

/**
*
*/
Ext.define('DCT.ExternalModules', {
  extend: 'Ext.Panel',
  
	dctControl: true,
	inputXType: 'dctexternalmodules',
	xtype: 'dctexternalmodules',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setBodyCfg(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setBodyCfg: function (config) {
		if (config.dctContentType == 'iframe'){
			config.items = {
				xtype: 'component',
				autoEl: {
					src: config.dctContentUrl,
					name: config.dctContentName || config.id + '-externalContentFrame',
					id: config.id + '-externalContentFrame',
					tag: 'iframe',
					cls: 'x-panel-body',
					frame: false,
					bodyBorder: false,
					width: '100%',
					scrolling: config.dctContentScrollable,
					frameBorder: 0,
					marginwidth: "0",
					marginheight: "0",
					height: (config.height) ? (config.height * 1) - 40 : null,
					style: {
						overflow: 'auto'
					}
				}
			};
		}
	}		  
});
DCT.Util.reg('dctexternalmodules', 'externalModules', 'DCT.ExternalModules');
