
/****/
Ext.define('DCT.InputEmailField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputemailfield',
	xtype: 'dctinputemailfield',

	/**	*	*/
	setVType: function (config) {
  		config.vtype = 'email';
	}		  
   
});
DCT.Util.reg('dctinputemailfield', 'interviewInputEmailField', 'DCT.InputEmailField');

