
/****/
Ext.define('DCT.EditorComboField', {
  extend: 'DCT.ComboField',

 	syncFont: true,
 	inputXType: 'editorcombofield',
 	xtype: 'editorcombofield',

  /**  *  */
  setAllowBlank: function(config){
  },
  /**  *  */
  setDataStore: function(config){
  },
  /**  *  */
  setAutoCreate: function(config){
		if (Ext.isDefined(config.fieldRef)) { 
			config.inputAttrTpl = 'fieldRef="' + config.fieldRef + '-editorcombofield"' + ' objectRef="' + config.objectRef + '-editorcombofield"'; 
		}
  },
  /**  *  */
  setHiddenInfo: function(config){
  },
  /**  *  */
  setCls: function(config){
  },
  /**  *  */
  setComboWidth: function(config){
  },
  /**  *  */
  setListParent: function(config){
  },
	/**	*	*/
	setListeners: function(config){
		var me = this;
		if (Ext.isDefined(config.listeners)){
			Ext.apply(config.listeners, {
				afterrender: {
					fn: function(control) {
						var me = this;
						me.initialHover(control);
					},
					scope:me
				}
			});
			Ext.destroyMembers(config.listeners, 'focus');
			delete config.listeners['focus'];
		} else {
	    config.listeners = {
				specialkey: function(f,e){
					var me = this;
					switch(e.getKey())
						{
							case e.RETURN:
							case e.ENTER:
							break;
							case e.TAB:
	                if (f.view.getSelectionCount() == 1){
	                  var selRecord = f.view.getSelectedRecords()[0];
	                  var selectedVal = selRecord.get(f.valueField);
	                  f.setValue(selectedVal);
	                  f.fireEvent('select', me, selRecord);
	                }
							break;
						}
				},
				afterrender: {
					fn: function(control) {
						var me = this;
						me.initialHover(control);
						me.setFieldFocus();
					},
					scope:me
				}
			};
	  }
	},
	/**	*	*/
	setTemplate: function (config) {
	}		  		  
   
});

