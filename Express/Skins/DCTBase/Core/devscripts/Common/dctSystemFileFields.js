/**
*
*/
Ext.define('DCT.SystemFileField', {
  extend: 'DCT.InputFileField',
  
	inputXType: 'dctsystemfilefield',
	xtype: 'dctsystemfilefield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
 		if (!Ext.isDefined(config.listeners)){
		    config.listeners = {
			};
		}
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	
   
});
DCT.Util.reg('dctsystemfilefield', 'systemFileField', 'DCT.SystemFileField');

/**
*
*/
Ext.define('DCT.SystemDropFileField', {
  extend: 'DCT.InputDropFileField',

	inputXType: 'dctsystemdropfilefield',
	xtype: 'dctsystemdropfilefield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
		config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	

});
DCT.Util.reg('dctsystemdropfilefield', 'systemDropFileField', 'DCT.SystemDropFileField');