/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
	*
	*/
    printOutWithAction: function (sPageSet, sXSL, action, suppressRefresh) {
        DCT.LoadingMessage.hideAndCancel();
        DCT.LoadingMessage.polling = true;
        DCT.LoadingMessage.show();
        var stateId = Ext.getDom('_stateID').value;
        Ext.Ajax.request({
            url: DCT.Util.getPostUrl() + "&_submitAction=&_targetPage=preview&_pageSet=" + sPageSet + "&_previewXSL=" + sXSL + "&mode=popup&_action=" + action,
            success: DCT.LoadingMessage.formsSuccess,
            failure: DCT.LoadingMessage.formsFailure,
            scope: DCT.LoadingMessage,
            params: { _stateID: stateId },
            method: "POST"
        });
        if (suppressRefresh != "1") {
            window.onfocus = Ext.Function.createDelayed(DCT.LoadingMessage.resumeSession, 0, DCT.LoadingMessage);
        }
    }
});

/**
* @class DCT.Grid
*/
Ext.apply(DCT.Grid, {

    /**
	*
	*/
	toggleRemoveForm: function (formName, rowIndex, gridId) {
	  var formDeleted = Ext.getDom('_removeForm' + formName).value;
	  if (formDeleted == '1') {
	    Ext.get('RemoveForm' + formName).removeCls("collapsed");
	    Ext.get('UndoRemoveForm' + formName).addCls("collapsed");
	    Ext.getDom('_removeForm' + formName).value = '0';
	    Ext.get('policyForm' + formName + '_Remove').setDisplayed("none");
	    Ext.get('policyForm' + formName + '_On').setDisplayed("inline");
	    //if its selected, show "Add"
			if (Ext.getDom('_printJobCB' + formName) != null && Ext.getDom('_printJobCB' + formName).value == '1') {
	        Ext.get('policyForm' + formName + '_Add').setDisplayed("inline");
			}
	    var printCheckBox = Ext.get('_printJob' + formName);
	    if (printCheckBox){
	    	printCheckBox.replaceCls('x-grid-row-checker-hidden', 'x-grid-row-checker');
	    	printCheckBox.setVisible(true);
	  	}
	  }else{ //REMOVING POLICY 
	    Ext.get('RemoveForm' + formName).addCls("collapsed");
	    Ext.get('UndoRemoveForm' + formName).removeCls("collapsed");
	    Ext.getDom('_removeForm' + formName).value = '1';
	    Ext.get('policyForm' + formName + '_Remove').setDisplayed("inline");
	    Ext.get('policyForm' + formName + '_Add').setDisplayed("none");
	    Ext.get('policyForm' + formName + '_On').setDisplayed("none");
	
	    var printCheckBox = Ext.get('_printJob' + formName);
	    if (printCheckBox){
   			var printJobGrid = Ext.getCmp(gridId);
   			var printJobSelectModel = printJobGrid.getSelectionModel();    
   			var printRecord = printJobSelectModel.store.getAt(rowIndex);
   			if (printJobSelectModel.isSelected(printRecord))
	    		printJobSelectModel.deselect(printRecord);
	    	printCheckBox.replaceCls('x-grid-row-checker', 'x-grid-row-checker-hidden');
	      printCheckBox.setVisible(false);
	    }
	  }  
	},
	/*
	*
	*/
	purgeForm: function (formName, rowIndex, gridId) {
		DCT.Util.getSafeElement('_formToRemove').value = formName;
		DCT.Util.getSafeElement('_submitAction').value = "adHocFormDelete,previewAsyncPrintJob";
		DCT.ajaxProcess.completeContentAjax("", function() {
			DCT.Util.getSafeElement('_submitAction').value = "";
			Ext.getCmp(gridId).getStore().reload();
		});
	},
	/**
	*
	*/
	downloadForm: function (documentName, subDocument) {
		DCT.Util.getSafeElement('_documentName').value = documentName;
		DCT.Util.getSafeElement('_subDocument').value = subDocument;
		var prevSubmitAction = Ext.getDom('_submitAction').value;
		DCT.Submit.submitPageAction('printJob', 'downloadPrintJob');
		//Set the submit action back to its previous value which 
		//ensures the download is not triggered on the next action.
		Ext.getDom('_submitAction').value = prevSubmitAction;
	},
    /**
	*
	*/
  setPrintJobBulk: function () {
      Ext.getDom('_printJobBulk').value = 'markDefault';
      var printJobGrid = Ext.getCmp('printJobList');
      printJobGrid.getSelectionModel().clearSelections();
      printJobGrid.getStore().reload({
          callback: function (records, operation, success) {
              Ext.getDom('_printJobBulk').value = '';
          }
      });
  },
    /**
	*
	*/
	setPrintJobRestrict: function (mark) {
		if (mark == 0) {
	      Ext.getCmp('printJobRestrict').setValue("");
		}
	  DCT.Grid.applyFilterToDataStore(Ext.getCmp('printJobList').getStore());
	},
	/**
	*
	*/
  submitPageRefresh: function () {
      var showFilter = Ext.getCmp("formsShow").getValue();
      DCT.Util.getSafeElement("_printShowFilter").value = showFilter;
      Ext.getDom('_submitAction').value = 'refreshAsyncPrintJob';
      DCT.Util.getSafeElement("_directProcess").value = '1';
      DCT.Grid.applyFilterToDataStore(Ext.getCmp('printJobList').getStore(), false, DCT.Grid.resetPageVariables);
  },
	/**
	*
	*/
  resetPageVariables: function () {
    Ext.getDom('_submitAction').value = '';
    DCT.Util.getSafeElement("_directProcess").value = '';
  },
    /**
	*
	*/
	deselectPrintJob: function (selectionModel, dataRecord, rowIndex) {
	  var name = dataRecord.data['name'];
	  var selected = dataRecord.data['selected'];
	  //keep track that its being unselected
	  var printJobCB = Ext.getDom('_printJobCB' + name);
	  if (printJobCB){
	      printJobCB.value = '0';
	  		var selectedItem = Ext.get('policyForm' + name + '_Selected');
			if (selectedItem) {
	    		selectedItem.setDisplayed(false);
			}
	  		var optionalItem = Ext.get('policyForm' + name + '_Optional');
			if (optionalItem) {
	    		optionalItem.setDisplayed(true);
	    }
		}
	  var selectedItems = selectionModel.getCount();
	  if (selectedItems >= 0) { //was ==
	  	var addMark = Ext.get('policyForm' + name + '_Add');
			if (addMark) {
	    	addMark.setDisplayed(false);
	  }
		}
	},
	/**
	*
	*/
	selectPrintJob: function (selectionModel, dataRecord, rowIndex) {
	  var name = dataRecord.data['name'];
	  var remove = dataRecord.data['remove'];
	  var selected = dataRecord.data['selected'];
	  //keep track that its being selected
	  var printJobCB = Ext.getDom('_printJobCB' + name);
	  if (printJobCB){
	      printJobCB.value = '1';
	  		var selectedItem = Ext.get('policyForm' + name + '_Selected');
			if (selectedItem) {
	    		selectedItem.setDisplayed(true);
			}
	  		var optionalItem = Ext.get('policyForm' + name + '_Optional');
			if (optionalItem) {
	    		optionalItem.setDisplayed(false);
	    }
		}
		  if (!remove) {
	      var selectedItems = selectionModel.getCount();
	      if (selectedItems > 0) {
	      	var tickMark = Ext.get('policyForm' + name + '_On');
	      	if (tickMark && !tickMark.isVisible()){
	      		var addMark = Ext.get('policyForm' + name + '_Add');
					if (addMark) {
	              addMark.setDisplayed(true);
	        }
	      }
	  }
		}
	},
	/**
	*
	*/
	checkPrintJob: function (selectionModel, recordData, rowIndex, eOpts) {
	   var printDefault = recordData.data['printDefault'];
	   var name = recordData.data['name'];
	   var remove = recordData.data['remove'];
	   var readOnly = Ext.DomQuery.selectValue('printDocs/@readOnly', recordData.store.proxy.reader.rawData);
		if (Ext.getDom('_removeForm' + name)) {
	    remove = (Ext.getDom('_removeForm' + name).value=='1');
		}
		if ((printDefault == "Mandatory" || readOnly == '1') || (remove)) {
	   	return false;
		}
		else {
	    return true;
		}
	},
	/*
	*
	*/
	displayAddFormPopUp: function (formName, order, rowIndex, gridId) {
		var isNewForm = (rowIndex == undefined || rowIndex == '');
		var recordData = (isNewForm) ? null : Ext.getCmp(gridId).getStore().getAt(rowIndex); 
		var addFormPopup = Ext.getCmp('addFormPopup');
		if (addFormPopup) {
			return;
		}
		var addFormWindow = Ext.create('Ext.Window',{
			title: (isNewForm) ? DCT.T('AddForm') : 'Edit Form',
			width: 350,
			height: 350,
			modal: true,
			id: 'addFormPopup',
			cls: 'x-addFormPopup',
			isNewForm: isNewForm,
			dctFocusFieldId: 'fileUpload',
			renderTo: 'mainForm',
			items: Ext.create('DCT.AddForm',{
				id: 'addFormPanel',
				isNewForm: isNewForm,
				formData: recordData
			}),
			buttons: [{
				text: (isNewForm) ? DCT.T('Add') : 'Update',
				id: '_saveAddButton',
				disabled: true,
				handler: function () { Ext.getCmp('addFormPanel').saveAdd(); },
				scope: this
			}, {
				text: DCT.T('Cancel'),
				id: '_cancelAddButton',
				disabled: false,
				handler: function () { Ext.getCmp('addFormPanel').closeForm(); },
				scope: this
			}]
		});
		addFormWindow.show();
	}
});
/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
    /**
	*
	*/
	actOnPrintJob: function (page, formName) {
		var me = this;
	  DCT.Util.getSafeElement('_printJobForm').value = formName;
		if (page == "preview" || page == "previewAdHoc")
			me.printOut("", "printjob:" + formName, page);
		else
			me.submitPage(page);
	},
	/**
 	*
 	*/
	printOut: function (sPageSet, sXSL, page) {
		if (!page) {
			page = "preview";
		}
		var stateId = Ext.getDom('_stateID').value;
	  Ext.Ajax.request({
	  	url: DCT.Util.getPostUrl() + "&_submitAction=&_targetPage=" + page + "&_pageSet=" + sPageSet + "&_previewXSL=" + sXSL + "&mode=popup",
	      success: DCT.LoadingMessage.formsSuccess,
	      failure: DCT.LoadingMessage.formsFailure,
	      params: {_stateID: stateId},
	      scope: DCT.LoadingMessage,
		  
	      method: "POST"
	  });
	},
  getReviewAction: function () {
      if ((Ext.getDom('reviewedValue').value) != Ext.getDom('reviewValueOld').value) {
          return 'refreshAsyncPrintJob';
      }
      return 'previewAsyncPrintJob';
  },
  submitPreviewAction: function (page, formName) {
      DCT.ajaxProcess.submitPreviewAjax('PageContainer', page, '0', '');
      DCT.Submit.actOnPrintJob('preview', 'FORM:' + formName);
  },
	/**
 	*
 	*/
    actOnPrintJobWithReturnToList: function (page, formName, tempManuScriptID, reviewValue, reviewFormName, currentStartIndex, dctClickedRowIndex) {
    	DCT.Util.getSafeElement('_tempManuScriptID').value = tempManuScriptID;
    	DCT.Util.getSafeElement('_reviewValue').value = reviewValue;
    	DCT.Util.getSafeElement('_reviewFormName').value = reviewFormName;
    	DCT.Util.getSafeElement('_currentStartIndex').value = currentStartIndex;
    	DCT.Util.getSafeElement('_clickedRowIndex').value = dctClickedRowIndex;
    	DCT.Submit.actOnPrintJob(page, formName);
    }
});
/**
*
*/
DCT.AddForm = Ext.extend(Ext.Panel, {

	inputXType: 'dctaddform',
	labelWidth: 100,
	bodyBorder: false,
	formId: 'addForm',
	saveButton: '_saveAddButton',
	cancelButton: '_cancelAddButton',
	layout: 'form',
	xtype: 'dctaddform',
	/**
	*
	*/
	constructor: function(config) {
		var me = this;
		me.checkControlExists(config);
		me.setDefaults(config);
		me.setPanelItems(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setDefaults: function(config) {
		var me = this;
		config.defaults = {
	    listeners: {
	      change: {
	          fn: me.checkChanges,
	          scope: me
	      }
	    }
		};
	},
	/**
	*
	*/
	setPanelItems: function(config) {
		var me = this;
		config.items = [{
			xtype: 'dctsystemfilefield',
			fieldLabel: DCT.T('FormFile'),
			id: 'fileUpload',
			name: '_fileUpload',
			disabled: (config.isNewForm) ? false : true,
			fileUpload: true,
			buttonText: DCT.T('Browse'),
			buttonOnly: false,
			validator: function (value) {
				if (value == '') {
					return DCT.T('NoFileSelected');
				}
				else if (!(value.toLowerCase().indexOf('.doc') >= 0 || value.toLowerCase().indexOf('.docx') >= 0 || value.toLowerCase().indexOf('.pdf') >= 0)) {
					return DCT.T('InvalidFormFileType');
				}
				return true;
			}
		},{
			xtype: 'dctsystemtextfield',
			fieldLabel: DCT.T('FormName'),
			id: 'formName',
			name: '_formName',
			readOnly: (config.isNewForm) ? false : true,
			dctRegEx: /^[a-zA-Z0-9-_]+$/,
			dctRegExMsg: DCT.T('InvalidFormName'),
			allowBlank: false,
			value: (config.isNewForm) ? '' : config.formData.get('name')
		},{
			xtype: 'dctsystemtextfield',
			fieldLabel: DCT.T('FormCaption'),
			id: 'caption',
			name: '_caption',
			value: (config.isNewForm) ? '' : config.formData.get('caption'),
			readOnly: (config.isNewForm) ? false : true
		},{
			xtype: 'dctsystemtextfield',
			fieldLabel: DCT.T('Category'),
			id: 'category',
			name: '_category',
			value: (config.isNewForm) ? '' : config.formData.get('category'),
			readOnly: (config.isNewForm) ? false : true
		},{
			xtype: 'dctsystemnumberfield',
			fieldLabel: DCT.T('FormPrintOrder'),
			id: 'printOrder',
			value: (config.isNewForm) ? parseInt(DCT.Util.getSafeElement('_printOrder').value) + 1 : config.formData.get('order'),
			name: '_printOrder',
			readOnly: DCT.Util.getSafeElement('_canReorder').value != "1",
			allowBlank: false
		},{
			xtype: 'dctsystemdisplayfield',
			fieldLabel: DCT.T('PrintJobName'),
			value: DCT.Util.getSafeElement('_printJobName').value,
			id: 'printJobName',
			name: '_printJobName',
			readOnly: true,
			allowBlank: false
		},{
			xtype: 'dctsystemnumberfield',
			fieldLabel: DCT.T('PaperBin'),
			id: 'paperBin',
			value: '0',
			name: '_paperBin',
			allowBlank: false
		},{
			xtype: 'dctsystemcheckboxfield',
			fieldLabel: '',
			boxLabel: DCT.T('PrintDuplex'),
			id: 'isDuplex',
			name: '_isDuplex',
			checked: false,
			inputValue: '0'
		}];
	},
	/**
	*
	*/
	checkChanges: function (field, newValue, oldValue, eOpts ) {
		var me = this;
		if (!me.isValidFormData() || (me.isValidFormData() && !me.isFormDirty())){
			me.disableAddSave();
		}else{
			me.enableAddSave();
		}
	},
	/**
	*
	*/
	enableAddSave: function () {
		var me = this;
		Ext.getCmp(me.saveButton).enable();
	},
	/**
	*
	*/
	disableAddSave: function () {
		var me = this;
		Ext.getCmp(me.saveButton).disable();
	},
	/**
	*
	*/
	saveAdd: function () {
		var me = this;
		if (me.isValidFormData() && me.isFormDirty()) {
			DCT.Util.getSafeElement('_submitAction').value = 'adHocFormAdd,previewAsyncPrintJob';
			DCT.LoadingMessage.show();
			if (me.isNewForm) {
				mainForm.enctype = 'multipart/form-data';
				mainForm.encoding = 'multipart/form-data';
				DCT.Util.getSafeElement('_isNewForm').value = 'true';
			}
			else {
				DCT.Util.getSafeElement('_isNewForm').value = 'false';
			}
			mainForm.submit();
		}
	},
	/**
	*
	*/
	closeForm: function () {
		Ext.getCmp('addFormPopup').close();
	},
	/**
	*
	*/
	isValidFormData: function () {
		var me = this,
				valid = true;
		me.items.each(function(item, index, len){
			if (!item.isDisabled() && !item.isValid()){
				valid = false;
				return false;
			}
			return true;
		}, me);
		
		return (valid)
	},
	/**
	*
	*/
	isFormDirty: function () {
		var me = this,
				dirty = false;
		me.items.each(function(item, index, len){
			if (!item.isDisabled() && item.isDirty()){
				dirty = true;
				return false;
			}
			return true;
		}, me);
		
		return (dirty)
	}
});
