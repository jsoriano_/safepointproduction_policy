Ext.ns('DCT', 'DCT.Util', 'DCT.Integration', 'DCT.Submit', 'DCT.Submit.Integration', 'DCT.Grid', 'DCT.CustomEvents', 'DCT.Strings', 'DCT.LoadingMessage', 'DCT.Localization', 'DCT.FieldsChanged', 'DCT.Sections');

Ext.require(['Ext.*', 'DCT.*']);

/**
* @class DCT
*/
Ext.apply(DCT, {
	// ASP file for Express
	postPage: "default.aspx",

	// Asychronous processing to check status of forms
	checkStatusUrl: "CheckSemaphoreStatus.aspx",

	// Current session Id from Server
	sessionID: '',

	// Current processing manuscript Id
	manuscriptID: '',

	// Current interview topic
	topic: '',

	// Current interview manuscript page
	page: '',

	// Indicates whether the interview pageset should be saved during page submission
	savePageSet: '',

	// Current Express content page
	currentPage: '',

	// Spotlight's minimum confidence level before highlighting fields
	spotlightConfidenceThreshold: '',

	// Notification message returned from Manuscript processing to display to the user
	notifyMessage: '',

	// Flag to redirect the user to Forms Authentication login by refreshing the page
	redirectToFormsLogin: '',

	// Indicates if the previous popup should be closed when the parent page is loading
	closePopup: '',

	// Current client name in Express state
	clientName: '',

	// Need to use Scope instead for these variables
	globalAction: '',
	globalPortfolioID: '',
	globalQuoteID: '',
	globalLOB: '',
	globalClientID: '',
	globalSaveAction: '',
	globalDetails: '',
	globalPage: '',
	globalPopupScrollbars: true,

	// Handle of the action button when verifying saving a policy
	msgActionBtn: '',

	// Handle of the message alert popup when verifying saving a policy
	msgBox: '',

	// Express proxy url used when process Ajax calls
	proxyUrl: null,

	// Directory path to the images for the current skin
	imageDir: '',

	// Developer Toolbar informational page items, retrieved from getPageRs properties
	versionID: '',
	versionDate: '',
	caption: '',
	inherited: '',
	inheritedPage: '',
	cultureCode: '',

	// Directory path to the current skin, only used for developer toolbar
	skinsDir: '',

	// Current logged on user, only used for developer toolbar
	currentUser: '',

	// Current logged on user's full name, only used for developer toolbar 
	currentFullName: '',

	// Version of Platform Server, only used for developer toolbar 
	versionServer: '',

	// Version of Platform Express, only used for developer toolbar 
	versionExpress: '',

	// System culture code used by system pages, only used for developer toolbar 
	systemCulture: '',

	// Current active agency, only used for developer toolbar 
	activeAgency: '',

	// Current logged on user's roles, only used for developer toolbar 
	userRoles: '',

	// Current logged on user's contexts, only used for developer toolbar 
	userContexts: '',

	// Help Collection for hover help text 
	hoverHelp: Ext.create('Ext.util.MixedCollection', {}),

	// Ajax object to handle interview processing 
	ajaxProcess: null,

	// URL of the Billing System to connect to (leave blank if on the same machine)
	BillingSystemURL: "",

	// URL of the Policy System to connect to (leave blank if on the same machine)
	PolicySystemURL: "",
	IsReadOnly: ''
});

//Define empty classes so they can be applied to later.

/**
*
*/
DCT.Util = {
};
/**
*
*/
DCT.Integration = {
};
/**
*
*/
DCT.Submit = {
};
/**
*
*/
DCT.Grid = {
};
/**
*
*/
DCT.LoadingMessage = {
};
/**
*
*/
DCT.ProcessQueue = {
};
/**
*
*/
DCT.FieldsChanged = {
};
/**
*
*/
DCT.Localization = {
};
/**
*
*/
DCT.Sections = {
};
// Generates all of the Express ExtJs controls once the page is loaded

/**
*
*/
Ext.onReady(function () {
	DCT.Util.generateControls();
});


