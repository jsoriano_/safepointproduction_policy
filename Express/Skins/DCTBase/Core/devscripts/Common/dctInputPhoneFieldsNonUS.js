

/**
*  Phone Field - United Kingdom 
*/
Ext.define('DCT.InputPhoneFieldUK', {
  extend: 'DCT.InputPhoneField',
  
	inputXType: 'dctinputphonefielduk',
	dctFormatBeforePost: true,
	xtype: 'dctinputphonefielduk',

	/**
	*
	*/
	setFormatter: function (config) {
		var me = this;
		config.formatter = me.formatPhoneUK;
	},
	
	/**
	*
	*/
	setVType: function (config) {
		config.vtype = 'phoneuk';
	},
	
	/**
	* 
	*/
	formatPhoneUK: function (fieldValue) {
		if (!Ext.isEmpty(fieldValue)) {
			var output = new String("");
			var i = 0;
			for (i = 0; (i < fieldValue.length); i++) {
				if (fieldValue.charCodeAt(i) > 47 && fieldValue.charCodeAt(i) < 58) {
					output = output + fieldValue.charAt(i);
				}
			}
			return (output.substr(0, 4) + " " + output.substr(4, 3) + " " + output.substr(7, 4));
		}
	}	
});
DCT.Util.reg('dctinputphonefielduk', 'interviewInputPhoneFieldUK', 'DCT.InputPhoneFieldUK');


/**
*  Phone Field - Italy
*/
Ext.define('DCT.InputPhoneFieldIT', {
  extend: 'DCT.InputPhoneField',

	inputXType: 'dctinputphonefieldit',
	dctFormatBeforePost: true,
	xtype: 'dctinputphonefieldit',

	/**
	*
	*/
	setFormatter: function (config) {
		var me = this;
		config.formatter = me.formatPhoneIT;
	},
	
	/**
	*
	*/
	setVType: function (config) {
		config.vtype = 'phoneit';
	}
	
	/**
	* 
	*/
	
});
DCT.Util.reg('dctinputphonefieldit', 'interviewInputPhoneFieldIT', 'DCT.InputPhoneFieldIT');