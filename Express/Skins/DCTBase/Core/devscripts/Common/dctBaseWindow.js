/****/
Ext.define('DCT.BaseWindow', {
  extend: 'Ext.Window',
	dctControl: true,
	inputXType: 'dctbasewindow',
	modal:true, 
	shadow:true,
	minWidth: 300,
	minHeight: 200,
	maximizable: true,
	layout: 'fit',
	plain:true,
	bodyBorder: false,
	bodyStyle:'padding:5px;',
	xtype: 'dctbasewindow',

	/**	*	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setListeners(config);
		me.callParent([config]);
	},
	/**	*	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
    	show : {
				fn: function(control) {
					var me = this;
					me.setFocus();
					DCT.Util.removeTabs(false);
				},
				scope:me
			},
    	close : {
				fn: function() {
					var me = this;
					me.destroy();
				},
				scope:me,
				delay: 10
			}
		};
	},
	/**	*	*/
	closeWindow: function () {
		var me = this;
    me.close();
  },
   /**	*	*/
   setFocus: function () {
   	var me = this;
   	var focusField = Ext.getCmp(me.dctFocusFieldId);
	  focusField.focus(false, 10);
  }
});
DCT.Util.reg('dctbasewindow', 'basewindow', 'DCT.BaseWindow');