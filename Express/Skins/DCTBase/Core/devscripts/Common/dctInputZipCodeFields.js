
/****/
Ext.define('DCT.InputZipCodeField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputzipcodefield',
	xtype: 'dctinputzipcodefield',

  /**  *  */
  setVType: function(config){
  	config.vtype = 'zipcode';
  }		  
});
DCT.Util.reg('dctinputzipcodefield', 'interviewInputZipCodeField', 'DCT.InputZipCodeField');
