/**
* 
*/
Ext.define('DCT.FieldBase', {
  override: 'Ext.Component',
  
	skipRequiredChecking: false,
	enableKeyEvents: true,
	selectOnFocus: true,
	dctPartyMappingField: false,
	dctSpotlight: false,

	/**
	* 
	*/
	customOnBlur: function () {

	},
	/**
	* 
	*/
	customOnChange: function () {
	},
	/**
	* 
	*/
	customFieldFormat: function () {
	},
	/**
	* 
	*/
	hidePartyButton: function () {
		DCT.Util.isPartyToolsVisible = false;

		Ext.Function.defer(function () {
			if (DCT.Util.isPartyToolsVisible) {
			} else {
				var tools = Ext.get('partyTools');
				if (tools != null) {
					tools.addCls('x-hidden-display');
				}
			}
		}, 150);
	},
	/**
	* 
	*/
	showPartyButton: function () {
		var me = this;
		/* Get all fields that match the partyRecordMappingId, only display the 1st one */
		var searchTxt = 'field[dctPartyRecordMappingId=' + me.dctPartyRecordMappingId + ']';
		var myGrpList = Ext.ComponentQuery.query(searchTxt);
		var partySlideOutItem = Ext.Array.findBy(myGrpList, function(item, index){
			if (item.dctPartyMappingSlideOut == '1')
				return true;
			});
		if (partySlideOutItem != null && me.id == partySlideOutItem.id) {
			var tools = Ext.get('partyTools');
			if (tools) {
				tools.set({ partyPopupTitle: me.dctPartyPopUpTitle });
				tools.set({ dctPartyRecordMappingId: me.dctPartyRecordMappingId });
				tools.set({ fieldObjectRef: me.id });
				DCT.Util.isPartyToolsVisible = true;
				DCT.Util.positionPartyTools(me.id);
			}
		}
	},
	/**
	* 
	*/
	fieldChanged: function () {
		var me = this;
		DCT.FieldsChanged.set();
		me.customOnChange();
	},
	/**
	* 
	*/
	fieldOnBlur: function () {
		var me = this;
		if (me.isDirty()) {
			if (me.validate()) {
				me.customFieldFormat();
				me.customOnBlur();
			}
		}
	},
	/**
	* 
	*/
	fieldValidation: function () {
		var me = this;
		if (Ext.isDefined(me.dctActionId) && me.dctActionId.length > 0)
			me.ajaxPost();
		else
			me.fieldOnBlur();
			
		if (me.dctPartyMappingField) {
			me.hidePartyButton();
		} 
	},
	/**
	* 
	*/
	ajaxPost: function () {
		var me = this;
		if (me.isDirty()) {
			if (me.validate()) {
				if (me.dctFormatBeforePost) { me.formatField() }
				me.customOnBlur();
				DCT.ajaxProcess.onBlurPost(me);
			}
		}
	},
	/**
	* 
	*/
	storeFocusField: function () {
		var me = this;
		DCT.Util.setFocusField(me.id);
		if (me.dctPartyMappingField) {
			me.showPartyButton();
		}

		if (DCT.spotlightEnabled && me.dctSpotlight) {
			DCT.Integration.Spotlight.mapInit(me, DCT.Integration.Spotlight.processInputField);
		}
	},
	/**
	* 
	*/
    scrollIntoViewOnFocus: function (field) {
        var tb_exists = Ext.get('workbenchToolbar');
        if (tb_exists != null) {
            var tb = tb_exists.dom;
            var fieldElement = field.el;
            if (fieldElement != null) {
                var fieldRect = fieldElement.dom.getBoundingClientRect();
                var toolbarRect = tb.getBoundingClientRect();

                if (toolbarRect.top > 0) {
                    var overlap = fieldRect.bottom > toolbarRect.top;
                    if (overlap) {
                        window.scrollBy(0, fieldRect.bottom - toolbarRect.top);
                    }
                }
            }
        }

    },
    /**
    * 
    */
	checkControlExists: function (config) {
		var me = this;
		var fieldCmp = Ext.getCmp(config.id);
		if (fieldCmp) {
			if (fieldCmp.getXType() == me.inputXType)
				fieldCmp.destroy();
		}
	},
	/**
	* 
	*/
	setDisable: function (config) {
	},
	/**
	* 
	*/
	setCls: function (config) {
		config.cls = (config.dctRequired == '1') ? 'dct-required-field' : ((Ext.isEmpty(config.cls)) ? 'dct-field' : config.cls + ' dct-field');
	},
	/**
	* 
	*/
	setAllowBlank: function (config) {
		var me = this;
		if (!me.skipRequiredChecking) {
			if (!Ext.isDefined(config.allowBlank)) {
				config.allowBlank = (config.dctRequired == '1') ? false : true;
			}
		}
	},
	/**
	* 
	*/
	setFieldFocus: function () {
		var me = this;
		if (DCT.Util.getFocusField() == me.id)
			me.focus(true, 5);
		if (DCT.fieldValidationLogsEnabled && DCT.currentPage == 'interview') {
			DCT.ValidationLogs.showValidationLogDialog();
		}
		if (DCT.spotlightEnabled && me.dctSpotlight) {
			DCT.Integration.Spotlight.mapInit(me, DCT.Integration.Spotlight.highlightForConfidence);
		}
	},
	/**
	* 
	*/
	filterMessageList: function (elementValue, dataStore) {
		var showRead = 1;
		var showUnread = 1;
		var showDeleted = 0;
		var customQuery = 0;
		DCT.Util.getSafeElement('_keyname').value = "";
		DCT.Util.getSafeElement('_keyop').value = "";
		DCT.Util.getSafeElement('_keyvalue').value = "";

		if (elementValue == 'admin') {
			showRead = 1;
			showUnread = 1;
			customQuery = Ext.getCmp('customQueryCheckbox').getValue(); //this is amazingly what is used to signify that we want an admin view (no limit on user ~ within valid entity hierarchy)
		} else if (elementValue == 'all') {
			showRead = 1;
			showUnread = 1;
		} else if (elementValue == 'read')
			showUnread = 0;
		else if (elementValue == 'unread')
			showRead = 0;
		else if (elementValue == 'deleted') {
			showDeleted = 1;
			var filterItem = 'Deleted';
			var filterValue = 1;
			var filterOp = 'equal';
			DCT.Util.getSafeElement('_keyname').value = filterItem;
			DCT.Util.getSafeElement('_keyop').value = filterOp;
			DCT.Util.getSafeElement('_keyvalue').value = filterValue;
		} else {
			var filterItem = 'Sender';
			var filterValue = elementValue;
			var filterOp = 'contains';
			DCT.Util.getSafeElement('_keyname').value = filterItem;
			DCT.Util.getSafeElement('_keyop').value = filterOp;
			DCT.Util.getSafeElement('_keyvalue').value = filterValue;
		}
		DCT.Util.getSafeElement('_customQuery').value = customQuery;
		DCT.Util.getSafeElement('_showRead').value = showRead;
		DCT.Util.getSafeElement('_showUnread').value = showUnread;
		DCT.Util.getSafeElement('_showDeleted').value = showDeleted;
		DCT.Grid.applyFilterToDataStore(dataStore);
	},
	/**
	* 
	*/
	resetOpenQuery: function (page) {
		Ext.getDom('_startIndex').value = 0;
		Ext.getDom('_targetPage').value = page;
		DCT.Submit.submitAction('query');
	},
	/**
	* 
	*/
	setManualAllocationTotals: function (otherAmt, amount) {
		var internalOtherAmt = otherAmt.formatting.formatNumberToUS(otherAmt.getValue());
		DCT.Util.getSafeElement('_hiddenPaymentAmount').value = otherAmt.formatting.formatNumberToUS(amount);

		var totalAllocationAmount = 0.0;
		for (i = 1; i < Ext.getDom('tblItemListDetail').rows.length; i++) {
			var defaultAmtObj = Ext.get('_hiddenProcessDefaultAmountId_' + i);
			var amt = (defaultAmtObj) ? defaultAmtObj.getValue() : 0.00;
			Ext.getDom('_hiddenProcessAmountId_' + i).value = Number(amt).toFixed(2);
			Ext.getDom('_processAmountId_' + i).value = otherAmt.formatting.formatFloat(Ext.getDom('_hiddenProcessAmountId_' + i).value);
			totalAllocationAmount += parseFloat(amt);
		}
		Ext.getDom('totalSuspenseAmtId').value = internalOtherAmt;
		Ext.getDom('amountRemainingTotalId').value = Number(internalOtherAmt - totalAllocationAmount).toFixed(2);
		Ext.get('displayTotalRemAmount').update(otherAmt.formatting.formatFloat(Ext.getDom('amountRemainingTotalId').value));
		Ext.getDom('amountAllocatedTotalId').value = totalAllocationAmount.toFixed(2); ;
		Ext.get('displayTotalAmount').update(otherAmt.formatting.formatFloat(Ext.getDom('amountAllocatedTotalId').value));
	},
	/**
	* 
	*/
	checkIfBatchSearchFieldsBlank: function () {
		var batchNbr = Ext.getCmp('batchNbr').getValue();
		var userId = Ext.getCmp('userId').getValue();
		var entryDate = Ext.getCmp('entryDate').getValue();
		var processDate = Ext.getCmp('processDate').getValue();

		if ((batchNbr != '') || (userId != '') || (entryDate != '') || (processDate != ''))
			return false;

		return true;
	},
	/**
	* 
	*/
	checkIfPaymentSearchFieldsBlank: function () {
		var accountRef = Ext.getCmp('accountReference').getValue();
		var policyRef = Ext.getCmp('policyReference').getValue();
		var exactDate = Ext.getCmp('exactDate').getValue();
		var startDate = Ext.getCmp('startDate').getValue();
		var endDate = Ext.getCmp('endDate').getValue();
		var exactAmt = Ext.getCmp('exactAmount');
		var startAmt = Ext.getCmp('startAmount');
		var endAmt = Ext.getCmp('endAmount');
		var paymentMethod = (Ext.getCmp('paymentMethod')) ? Ext.getCmp('paymentMethod').getValue() : "";
		var dateRange = Ext.getCmp('_dateRange').getValue();
		var amtRange = Ext.getCmp('_amtRange').getValue();
		var otherPaymentDetail = Ext.getCmp('otherPaymentDetail').getValue();

		var startAmount = (Ext.isEmpty(startAmt.getValue())) ? "0" : startAmt.getValue();
		var endAmount = (Ext.isEmpty(endAmt.getValue())) ? "0" : endAmt.getValue();
		var exactAmount = (Ext.isEmpty(exactAmt.getValue())) ? "0" : exactAmt.getValue();

		var startAmountValue = parseFloat(startAmt.formatting.formatNumberToUS(startAmount));
		var endAmountValue = parseFloat(endAmt.formatting.formatNumberToUS(endAmount));
		var exactAmountValue = parseFloat(exactAmt.formatting.formatNumberToUS(exactAmount));

		var disablePaymentAmtCheck = false;
		var disablePaymentDateCheck = false;

		if (dateRange) {
			if (Ext.isEmpty(startDate) && Ext.isEmpty(endDate))
				disablePaymentDateCheck = true;
			else
				disablePaymentDateCheck = false;
		} else {
			if (Ext.isEmpty(exactDate))
				disablePaymentDateCheck = true;
			else
				disablePaymentDateCheck = false;
		}

		if (amtRange) {
			if (startAmountValue == 0 && endAmountValue == 0)
				disablePaymentAmtCheck = true;
			else
				disablePaymentAmtCheck = false;
		} else {
			if (exactAmountValue == 0)
				disablePaymentAmtCheck = true;
			else
				disablePaymentAmtCheck = false;
		}

		if ((Ext.isEmpty(accountRef)) && (Ext.isEmpty(policyRef)) && (Ext.isEmpty(otherPaymentDetail)) && (Ext.isEmpty(paymentMethod)) && (disablePaymentDateCheck) && (disablePaymentAmtCheck))
			return true;

		return false;
	},
	/**
	* 
	*/
	checkIfHoldEventFieldsBlank: function () {
		var eventName = Ext.getCmp('eventName').getValue();
		var eventDescription = Ext.getCmp('eventDescription').getValue();
		var eventTypeCode = Ext.getCmp('eventTypeCode').getValue();
		var eventStartDate = Ext.getCmp('eventStartDate').getValue();

		if ((!Ext.isEmpty(eventName)) && (!Ext.isEmpty(eventDescription)) && (!Ext.isEmpty(eventTypeCode)) && (!Ext.isEmpty(eventStartDate)))
			return false;

		return true;
	},
	/**
	* 
	*/
	checkIfHoldFieldsBlank: function () {
		var holdType = Ext.getCmp('holdTypeCode').getValue();
		if (Ext.isEmpty(holdType))
			return true;

		var holdReasonComponentId = holdType == 'HD' ? 'holdDisbReasonCode' : 'holdReasonCode';
		var holdReason = Ext.getCmp(holdReasonComponentId).getValue();
		if (Ext.isEmpty(holdReason))
			return true;

		var holdStartDateComponent = Ext.getCmp('holdStartDate');
		var holdEndDateComponent = Ext.getCmp('holdEndDate');

		var currentProcessingDateValue = new Date(Date.parse(DCT.Util.getSafeElement('_currentSystemDate').value));
		var currentProcessingDate = new Date(currentProcessingDateValue.getFullYear(), currentProcessingDateValue.getMonth(), currentProcessingDateValue.getDate());

		var holdStartDateValue = holdStartDateComponent.getValue();
		var holdStartDate = new Date(holdStartDateValue.getFullYear(), holdStartDateValue.getMonth(), holdStartDateValue.getDate());

		var holdIndefinitely = Ext.getCmp('holdBillingIndefinitely').getValue();

		var holdEndDateValue = holdEndDateComponent.getValue();

		var minimumEndDate = new Date(holdStartDate.getFullYear(), holdStartDate.getMonth(), holdStartDate.getDate() + 1);

		Ext.apply(holdStartDateComponent, { minValue: currentProcessingDate, maxValue: null });
		Ext.apply(holdEndDateComponent, { minValue: minimumEndDate, maxValue: null });

		var holdStartDateValid = holdStartDateComponent.isValid();
		var holdEndDateValid = holdIndefinitely || holdEndDateComponent.isValid();

		return !holdStartDateValid || !holdEndDateValid;
	},



	/**
	* 
	*/
	checkIfEditHoldFieldsBlank: function () {
	    var holdEndDate = Ext.getCmp('holdEndDate').getValue();
		var holdStartDate = Ext.getCmp("holdStartDate").getValue();
		var holdIndefinitely = Ext.getCmp("holdBillingIndefinitely").getValue();
		var endDateNotBlank = false;
        
		if (holdIndefinitely){
			endDateNotBlank = true;
		}

		else if (!Ext.isEmpty(holdEndDate)) {

	        if (Date.parse(holdEndDate) <= Date.parse(holdStartDate)) {
	            endDateNotBlank = false;
	            Ext.create('DCT.AlertMessage', {
	                msg: DCT.T("StartDateGreaterThanEndDateError"),
	                title: DCT.T("Alert")
	            }).show();
	        } else {
	            endDateNotBlank = true;
	        }

		}
		else{
			endDateNotBlank = false;
		}
		
		if (endDateNotBlank)
			return false;

		return true;
	},
	
	
	
	/**
	* 
	*/
	checkIfHoldItemActionFieldBlank: function () {
		var holdReason = Ext.getCmp('holdReasonField').getValue();
		var holdCheckbox = Ext.getCmp('applyHoldReasonId').getValue();

		if (holdCheckbox) {
			if (Ext.isEmpty(holdReason))
				return true;
		} else {
			var holdItemReasons = Ext.DomQuery.select('[name=_holdItemReason]');
			for (i = 0; i < holdItemReasons.length; i++) {
				if (Ext.isEmpty(Ext.getCmp(holdItemReasons[i]).getValue()))
					return true;
			}
		}

		return false;
	},
	/**
	* 
	*/
	checkIfAddItemFieldsBlank: function () {
		var itemType = Ext.getCmp('itemTypeCode').getValue();
		var policyNumber = Ext.getCmp('policyNumber').getValue();
		var insuredName = Ext.getCmp('insuredName').getValue();
		var grossAmt = Ext.getCmp('grossAmount');
		var grossAmtValue = Ext.isEmpty(grossAmt.getValue()) ? "0" : grossAmt.getValue();
		grossAmtValue = grossAmt.formatting.formatNumberToUS(grossAmtValue);
		var grossAmount = Number(parseFloat(grossAmtValue)).toFixed(2);

		var surcharge = Ext.getCmp('addSurcharge').getValue();
		var fee = Ext.getCmp('addFee').getValue();
		var surchargeAmt = Ext.getCmp('surchargeAmount');
		var surchargeAmtValue = surchargeAmt.getValue();
		surchargeAmtValue = Ext.isEmpty(surchargeAmtValue) ? "0" : surchargeAmtValue;
		var feeAmt = Ext.getCmp('feeAmount');
		var feeAmtValue = feeAmt.getValue();
		feeAmtValue = Ext.isEmpty(feeAmtValue) ? "0" : feeAmtValue;

		var surchargeExists = true;
		var feeExists = true;
		var grossExists = true;

		if (surcharge) {
			surchargeAmtValue = surchargeAmt.formatting.formatNumberToUS(surchargeAmtValue);
			var surchargeAmount = Number(parseFloat(surchargeAmtValue)).toFixed(2);
			if (surchargeAmount != 0)
				surchargeExists = true;
			else
				surchargeExists = false;
		}

		if (fee) {
			feeAmtValue = feeAmt.formatting.formatNumberToUS(feeAmtValue);
			var feeAmount = Number(parseFloat(feeAmtValue)).toFixed(2);
			if (feeAmount != 0)
				feeExists = true;
			else
				feeExists = false;
		}

		if ((grossAmount != 0) && !surcharge && !fee)
			grossExists = true;
		else if (surcharge || fee)
			grossExists = true;
		else
			grossExists = false;

		if ((!Ext.isEmpty(itemType)) && (!Ext.isEmpty(policyNumber)) && (!Ext.isEmpty(insuredName)) && grossExists && surchargeExists && feeExists)
			return false;

		return true;
	},
	/**
	* 
	*/
	checkIfReverseItemFieldsBlank: function () {
		var reverseItemsReason = Ext.String.trim(Ext.getCmp('reversalReasonCodeId').getValue());
		var reverseItemComment = Ext.String.trim(Ext.getCmp('comment').getValue());

		if (Ext.isEmpty(reverseItemsReason) || Ext.isEmpty(reverseItemComment))
			return true;

		return false;
	},
	/**
	* 
	*/
	showHideHoldOptions: function (field) {
		DCT.Util.hideShowGroup("followUpOptionsRadiobuttons", field.getValue() == "HB");
		var holdindefinite = Ext.String.trim(Ext.getCmp('holdBillingIndefinitely').getValue());
		var holdType = field.getValue();

		if ((holdType == "HB" || holdType == "HF" || holdType == "") && holdindefinite == false) {
			DCT.Util.hideShowGroup("resumeOptionsRadiobuttons", true);
			
		}
		else {

			DCT.Util.hideShowGroup("resumeOptionsRadiobuttons", false);
			
		}
	},
	/**
	* 
	*/
	showHideResumeOptions: function (display) {
		var holdTypeCode = Ext.String.trim(Ext.getCmp('holdTypeCode').getValue());
		if (holdTypeCode != null && holdTypeCode == 'HD') {
			display = false;
		}

		DCT.Util.hideShowGroup("resumeOptionsRadiobuttons", display);
	},
	/**
	* 
	*/
	checkRedistributeInput: function (dateField) {
		var startDate = Ext.getCmp('startDate').getValue();
		var numberOfInstallments = Ext.getCmp('numberOfInstallments').getValue();
		if (Ext.isDefined(dateField)) {
			if (!dateField.isValid())
				startDate = '';
		}
		if ((startDate == '') || (numberOfInstallments == '') || numberOfInstallments < 1) {
			return true;
		} else {
			return false;
		}
	}

});

/* Added warning message logic to display a warning icon and allow validation to pass.  The only warning message implemented is for max lengths for textarea fields*/
/**
* 
*/
Ext.define('DCT.FieldFormBase', {
  override: 'Ext.form.field.Base',
  
  invalidWarningIconCls: Ext.baseCSSPrefix + 'form-invalid-warning-icon',

  getActiveWarning: function() {
      return this.activeWarning || '';
  },
  
  hasActiveWarning: function() {
      return !!this.getActiveWarning();
  },
  
  setActiveWarning: function(msg) {
      this.setActiveWarnings(msg);
  },
  
  unsetActiveWarning: function() {
      this.unsetActiveError(true);
  },
  
  setActiveWarnings: function(msg) {
      this.setActiveErrors(msg, true);
  },
  
  getActiveWarnings: function() {
      return this.activeWarnings || [];
  },
  
  setActiveErrors: function(errors, hasActiveWarnings) {
      var me = this,
          errorWrapEl = me.errorWrapEl,
          errorEl = me.errorEl,
          msgTarget = me.msgTarget,
          isSide = msgTarget === 'side',
          isQtip = msgTarget === 'qtip',
          activeError, tpl, targetEl;
      errors = Ext.Array.from(errors);
      Ext.Array.forEach(errors, function (item, index, allItems){
      		allItems[index] = Ext.util.Format.htmlEncode(item);
      	}, me
      );
      tpl = me.getTpl('activeErrorsTpl');
      if (hasActiveWarnings){
        me.activeWarnings = errors;
        activeError = me.activeWarning = tpl.apply({
            fieldLabel: me.fieldLabel,
            errors: errors,
            listCls: Ext.baseCSSPrefix + 'list-plain' + ' errorslist'
        });
      }else{
        me.activeErrors = errors;
        activeError = me.activeError = tpl.apply({
            fieldLabel: me.fieldLabel,
            errors: errors,
            listCls: Ext.baseCSSPrefix + 'list-plain' + ' errorslist'
        });
      }
      me.renderActiveError();
      if (me.rendered) {
          if (isSide) {
              me.errorEl.dom.setAttribute('data-errorqtip', activeError);
          } else if (isQtip) {
              me.getActionEl().dom.setAttribute('data-errorqtip', activeError);
          } else if (msgTarget === 'title') {
              me.getActionEl().dom.setAttribute('title', activeError);
          }
          if (isSide || isQtip) {
              Ext.form.Labelable.initTip();
          }
          if (!me.msgTargets[msgTarget]) {
              targetEl = Ext.get(msgTarget);
              if (targetEl) {
                  targetEl.dom.innerHTML = activeError;
              }
          }
      }
      if (errorWrapEl) {
      		if (hasActiveWarnings){
		        errorEl.addCls([
		            me.invalidWarningIconCls,
		            me.invalidWarningIconCls + '-' + me.ui
		        ]);
		        errorEl.removeCls([
		            me.invalidIconCls,
		            me.invalidIconCls + '-' + me.ui
		        ]);
      		}
          errorWrapEl.setVisible(errors.length > 0);
          if (isSide && me.autoFitErrors) {
              me.labelEl.addCls(me.topLabelSideErrorCls);
          }
          me.updateLayout();
      }
  },
  
  unsetActiveError: function(hasActiveWarnings) {
      var me = this,
          errorWrapEl = me.errorWrapEl,
          errorEl = me.errorEl,
          msgTarget = me.msgTarget,
          targetEl,
          restoreDisplay = me.restoreDisplay;
      if (me.hasActiveError() || me.hasActiveWarning()) {
      		if (hasActiveWarnings){
            delete me.activeWarning;
            delete me.activeWarnings;
      		}else{
            delete me.activeError;
            delete me.activeErrors;
          }
          me.renderActiveError();
          if (me.rendered) {
              if (msgTarget === 'qtip') {
                  me.getActionEl().dom.removeAttribute('data-errorqtip');
              } else if (msgTarget === 'title') {
                  me.getActionEl().dom.removeAttribute('title');
              }
              if (!me.msgTargets[msgTarget]) {
                  targetEl = Ext.get(msgTarget);
                  if (targetEl) {
                      targetEl.dom.innerHTML = '';
                  }
              }
              if (errorWrapEl) {
                  errorWrapEl.hide();
			        		if (hasActiveWarnings){
						        errorEl.addCls([
						            me.invalidIconCls,
						            me.invalidIconCls + '-' + me.ui
						        ]);
						        errorEl.removeCls([
						            me.invalidWarningIconCls,
						            me.invalidWarningIconCls + '-' + me.ui
						        ]);
			        		}                    
                  if (msgTarget === 'side' && me.autoFitErrors) {
                      me.labelEl.removeCls(me.topLabelSideErrorCls);
                  }
                  me.updateLayout();
                  
                  
                  
                  if (restoreDisplay) {
                      me.el.dom.style.display = 'block';
                      me.restoreDisplay();
                  }
              }
          }
      }
  },  
    
  getWarnings: function() {
    return [];
  },

  onDisable: function() {
      var me = this,
          inputEl = me.inputEl;
      me.callParent();
      if (inputEl) {
          inputEl.dom.disabled = true;
          if (me.hasActiveError() || me.hasActiveWarning()) {
              
              me.clearInvalid();
              me.hadErrorOnDisable = true;
          }
      }
      
      
      
      if (me.wasValid === false) {
          me.checkValidityChange(true);
      }
  },
  
  validateValue: function(value) {
      var me = this,
          errors = me.getErrors(value),
          warnings = me.getWarnings(value),
          hasWarnings = !Ext.isEmpty(warnings),
          isValid = Ext.isEmpty(errors);
      if (!me.preventMark) {
          if (isValid) {
          		if (hasWarnings){
          			me.markInvalid(warnings, true);
          		}else{
	              me.clearInvalid();
	            }
          } else {
              me.markInvalid(errors);
          }
      }
      return isValid;
  },
  
  markInvalid: function(errors, hasWarnings) {
      
      var me = this,
          oldMsg = me.getActiveError(),
          oldWarning = me.getActiveWarning(),
          activeWarning,
          active;
      if (hasWarnings){
	      me.setActiveWarnings(Ext.Array.from(errors));
	      activeWarning = me.getActiveWarning();
	      if (oldWarning !== activeWarning) {
	          me.setError(activeWarning);
	      }
      }else{
	      me.setActiveErrors(Ext.Array.from(errors));
	      active = me.getActiveError();
	      if (oldMsg !== active) {
	          me.setError(active);
	      }
	    }
  },
  
  clearInvalid: function() {
      
      var me = this,
          hadError = me.hasActiveError(),
          hadWarning = me.hasActiveWarning();
      delete me.hadErrorOnDisable;
      if (hadWarning){
      	me.unsetActiveWarning();
      }else{
	      me.unsetActiveError();
	    }
      if (hadError || hadWarning) {
          me.setError('');
      }
  },
  
  renderActiveError: function() {
      var me = this,
          hasError = me.hasActiveError(),
          hasWarning = me.hasActiveWarning(),
          invalidCls = me.invalidCls + '-field';
          inputEl = me.inputEl;
      
      if (inputEl) {
          inputEl[hasError || hasWarning ? 'addCls' : 'removeCls']([
              invalidCls,
              invalidCls + '-' + me.ui
          ]);
      }
      me.parentRenderActiveError();
  },
  
  parentRenderActiveError: function() {
      var me = this,
          activeError = me.getActiveError(),
          hasError = !!activeError,
          activeWarning = me.getActiveWarning(),
          hasWarning = !!activeWarning;
      if (activeError !== me.lastActiveError) {
          me.lastActiveError = activeError;
          me.fireEvent('errorchange', me, activeError);
      }
      if (me.rendered && !me.isDestroyed && !me.preventMark) {
          me.toggleInvalidCls(hasError || hasWarning);
          
          if (me.errorEl) {
              me.errorEl.dom.innerHTML = activeError || activeWarning;
          }
      }
  }
});

/* Override the checkboxgroup to handle escaping the html for hover help */
/**
* 
*/
Ext.define('DCT.CheckboxGroup', {
  override: 'Ext.form.CheckboxGroup',
  
  setActiveErrors: function(errors) {
      var me = this,
          errorWrapEl = me.errorWrapEl,
          msgTarget = me.msgTarget,
          isSide = msgTarget === 'side',
          isQtip = msgTarget === 'qtip',
          actionEl, activeError, tpl, targetEl;
      errors = Ext.Array.from(errors);
      Ext.Array.forEach(errors, function (item, index, allItems){
      		allItems[index] = Ext.util.Format.htmlEncode(item);
      	}, me
      );
      tpl = me.getTpl('activeErrorsTpl');
      me.activeErrors = errors;
      activeError = me.activeError = tpl.apply({
          fieldLabel: me.fieldLabel,
          errors: errors,
          listCls: Ext.baseCSSPrefix + 'list-plain' + ' errorslist'
      });
      me.renderActiveError();
      if (me.rendered) {
          actionEl = me.getActionEl();
          if (isSide) {
              me.errorEl.dom.setAttribute('data-errorqtip', activeError);
          } else if (isQtip) {
              actionEl.dom.setAttribute('data-errorqtip', activeError);
          } else if (msgTarget === 'title') {
              actionEl.dom.setAttribute('title', activeError);
          }
          // If msgTarget is title, setting an alert is redundant for ARIA purposes
          if (msgTarget !== 'title') {
              me.ariaErrorEl.dom.innerHTML = errors.join('. ');
              actionEl.dom.setAttribute('aria-describedby', me.ariaErrorEl.id);
          }
          if (isSide || isQtip) {
              Ext.form.Labelable.initTip();
          }
          if (!me.msgTargets[msgTarget]) {
              targetEl = Ext.get(msgTarget);
              if (targetEl) {
                  targetEl.dom.innerHTML = activeError;
              }
          }
      }
      if (errorWrapEl) {
          errorWrapEl.setVisible(errors.length > 0);
          if (isSide && me.autoFitErrors) {
              me.labelEl.addCls(me.topLabelSideErrorCls);
          }
          me.updateLayout();
      }
  }
});

/* Added warning message logic to display a warning icon and allow validation to pass.  The only warning message implemented is for max lengths for textarea fields*/
/**
* 
*/
Ext.define('DCT.FormFieldText', {
  override: 'Ext.form.field.Text',
  maxWarningLength: Number.MAX_VALUE,
  maxWarningLengthText: 'The maximum length for this field is {0}',
  blankText: DCT.T('ThisFieldIsRequired'),
	/**
	* 
	*/
  getWarnings: function(value) {
  	var me = this,
  			format = Ext.String.format,
  			warnings = me.callParent([value]);
      
    value = arguments.length ? (value == null ? '' : value) : me.processRawValue(me.getRawValue());
      
    if (value.length > me.maxWarningLength) {
     	warnings.push(format(me.maxWarningLengthText, me.maxWarningLength));
    }
    
    return warnings;
  }
});

/**
* 
*/
Ext.define('DCT.QuickTip', {
  override: 'Ext.QuickTip',
    /**
     * @private
     * Reads the tip text from the closest node to the event target which contains the
     * attribute we are configured to look for. Returns an object containing the text
     * from the attribute, and the target element from which the text was read.
     *
     * Added encoding to the title and text for security
     */
    getTipCfg: function(target, event) {
        var titleText = Ext.util.Format.htmlEncode(target.title),
            cfg = this.tagConfig,
            attr = cfg.attr || (cfg.attr = cfg.namespace + cfg.attribute),
            text;
        if (this.interceptTitles && titleText && Ext.isString(titleText)) {
            target.setAttribute(attr, titleText);
            target.removeAttribute('title');
            return {
                text: titleText
            };
        } else {
            target = Ext.fly(target).findParent(function(dom) {
                // Want to test the truthiness of the attribute and save it.
                return (text = dom.getAttribute(attr));
            });
            if (target) {
                return {
                    target: target,
                    text: (text.indexOf('errorslist') == -1) ? Ext.util.Format.htmlEncode(text) : text
                };
            }
        }
    }	
});
/**
*  Strict date parsing is used to prevent the date rollover effect
*/
Ext.apply(Date, {
       useStrict: true
});

Ext.define('DCT.dom.Shadow', {
	override: 'Ext.dom.Shadow',
	
  beforeShow: function() {
  		var me = this;
      if (!me.el.isDestroyed){
	      var style = me.el.dom.style,
	          shim = me.shim;
	
	      if (Ext.supports.CSS3BoxShadow) {
	          style[me.boxShadowProperty] = '0 0 ' + (me.offset + 2) + 'px #888';
	      } else {
	          style.filter = "progid:DXImageTransform.Microsoft.alpha(opacity=" + me.opacity + ") progid:DXImageTransform.Microsoft.Blur(pixelradius=" + (me.offset) + ")";
	      }
	
	      // if we are showing a shadow, and we already have a visible shim, we need to
	      // realign the shim to ensure that it includes the size of target and shadow els
	      if (shim) {
	          shim.realign();
	      }
	    }
  }
});