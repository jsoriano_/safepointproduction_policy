/*** @class DCT.FieldsChanged*/
Ext.apply(DCT.FieldsChanged, {
  /**  *  */
  set: function () {
      DCT.Util.getSafeElement('_fieldChanged').value = 1;
  },  /**  *  */
  clear: function() {
      DCT.Util.getSafeElement('_fieldChanged').value = 0;
  },
  /**  *  */
  fieldHasChanged: function () {
      return (DCT.Util.getSafeElement('_fieldChanged').value == 1);
  }
});