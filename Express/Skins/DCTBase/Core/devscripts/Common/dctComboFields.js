
/**
*
*/
Ext.define('DCT.ComboField', {
  extend: 'Ext.form.ComboBox',

	shadow: false,
	typeAhead: true,
	forceSelection: true,
 	queryMode: 'local',
 	valueField: 'code',
 	displayField: 'display',
 	valueNotFoundText: '',
 	inputXType: 'dctcombofield',
 	dctControl: true,
	keyDownValue: "",
	anyMatch: true,
	xtype: 'dctcombofield',
	defaultListConfig: {
		loadingHeight: 70, 
		minWidth: 30, 
		maxHeight: 300, 
		shadow: 'sides'
	},


 	/**
	*
	*/
 	constructor: function (config) {
 		var me = this;
		me.checkControlExists(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setListConfig(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.setDataStore(config);
		me.setTemplate(config);
		me.setComboWidth(config);
		me.setHiddenInfo(config);
		me.setValidator(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
		me.setInitialValue(config);
	},
	/**
	*
	*/
	setComboWidth: function (config) {
		config.size = 0;
	  if (!Ext.isDefined(config.ctCls) && !Ext.isDefined(config.width)) {
	  	config.grow = true;
			if (!Ext.isEmpty(config.dctMaxSize)) {
	    	config.growMax = Number(config.dctMaxSize);
	    }
	  }
  },
	/**
	*
	*/
	initialHover: function (combo) {
		var comboTip = Ext.getCmp(combo.id + 'tooltip');
		if (!Ext.isEmpty(comboTip)){comboTip.destroy();}
		var tip = Ext.create('Ext.tip.ToolTip', {
		  	id: combo.id + 'tooltip',
		  	dctControl: true,
		    target: combo.inputEl,
		    trackMouse: true,
		    renderTo: Ext.getBody(),
		    listeners: {
		        beforeshow: function updateTipBody(tip) {
		        	var selectedRecord = combo.getSelectedRecord();
		        	var displayText = '';
		        	if (selectedRecord){
		        		displayText = selectedRecord.get('display');
		        	}
		          tip.update(Ext.util.Format.htmlEncode(displayText));
		        }
		    }
		});		
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
			keydown: {
				fn: function (f, e) {
					var me = this;
					switch (e.getKey()){
						case e.TAB: {
							DCT.Util.setKeysUsed(true, e.shiftKey);
						}
						break;
					}
					Ext.defer(function () { 
						var me = this; 
						me.keyDownValue = me.getRawValue() 
					}, 1, me);
				},
				scope: me
			},
			afterrender: {
				fn: function (control) {
					var me = this;
					me.initialHover(control);
					me.setFieldFocus();
				},
				scope: me
			},
			select: {
				fn: function (combo, record, index, tabUsed) {
					var me = this;
					me.fieldChanged();
					me.fieldValidation();
				},
				scope: me
			},
			blur: {
				fn: function (f) {
					var me = this;
					var fieldSelection = f.getPicker().getSelectionModel();
					if (fieldSelection.getCount() == 1){
						var selRecord = fieldSelection.getSelection()[0];
						var selectedCaption = selRecord.get(f.displayField);
						var selectedVal = selRecord.get(f.valueField);
						if ((me.keyDownValue != "") && (selectedCaption.toUpperCase().indexOf(me.keyDownValue.toUpperCase()) != -1)) {
						f.setValue(selectedVal);
						f.fireEvent('select', me, selRecord, 0, true);
					}
						}
				},
				scope: me
			},
			focus: {
				fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			render: function (combo) {
				combo.inputEl.on('click', Ext.Function.createDelayed(combo.onTriggerClick, 0, combo));
			}
		};
	},
	/**
	*
	*/
	setAutoCreate: function (config) {
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**
	*
	*/
	setListConfig: function (config) {
		var objectInfo = '';
		if (Ext.isDefined(config.fieldRef)) { 
			objectInfo = ' fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '" '; 
		}
		
		config.listConfig = {
			emptyText: config.emptyText,
	    renderTpl: [
	        '<div id="{id}-listWrap"' + objectInfo + 'data-ref="listWrap" role="presentation" class="{baseCls}-list-ct ', Ext.dom.Element.unselectableCls, '">',
	            '<ul id="{id}-listEl" data-ref="listEl" class="' + Ext.baseCSSPrefix + 'list-plain">',
	            '</ul>',
	        '</div>',
	        '{%',
	            'var pagingToolbar=values.$comp.pagingToolbar;',
	            'if (pagingToolbar) {',
	                'Ext.DomHelper.generateMarkup(pagingToolbar.getRenderTree(), out);',
	            '}',
	        '%}',
	        {
	            disableFormats: true
	        }
	    ]			
		};
	},
	/**
	*
	*/
	setDataStore: function (config) {
		if (!Ext.isDefined(config.store)) {
			config.store = Ext.create('Ext.data.ArrayStore',{
	        fields: ['code', 'display'],
				data: config.dctStore
	    });
  	}
	},
	/**
	*
	*/
	setTemplate: function (config) {
		config.tpl = '<tpl for="."><li role="option" unselectable="on" class="x-boundlist-item" data-qtip="{display:htmlEncode}">{display:htmlEncode}</li></tpl>';
	},
	/**
	*
	*/
	setHiddenInfo: function (config) {
		config.hiddenId = config.id + "Combo";
	},
	/**
	*
	*/
	setInitialValue: function (config) {
		var me = this;
		if (Ext.isEmpty(config.value)) {
			var record = config.store.getAt(0);
			if (!Ext.isEmpty(record)){
				var restore = me.preventMark;
				me.preventMark = true;
				me.setValue(record.get(me.valueField));
				me.preventMark = restore;
			}
		}
	},
	/**
	*
	*/
	setDefaultListeners: function (config) {
	},
  /**
  *
  */
  setValidator: function(config){
  	var me = this;
   	config.validator = me.checkEmptyCode;
  },
  checkEmptyCode: function(value){
  	var me = this;
  	if (!me.allowBlank && Ext.isEmpty(me.getSubmitValue())){
  		return me.blankText;
  	}
  	return true;
  },
    /**
     * @private
     * Set the value of {@link #hiddenDataEl}
     * Dynamically adds and removes input[type=hidden] elements
     * Needed to override so add hidden Id to the hidden input for easier processing
     */
    setHiddenValue: function(values){
        var me = this,
            name = me.hiddenName,
            id = me.hiddenId,
            i,
            dom, childNodes, input, valueCount, childrenCount;

        if (!me.hiddenDataEl || !name) {
            return;
        }
        values = Ext.Array.from(values);
        dom = me.hiddenDataEl.dom;
        childNodes = dom.childNodes;
        input = childNodes[0];
        valueCount = values.length;
        childrenCount = childNodes.length;

        if (!input && valueCount > 0) {
            me.hiddenDataEl.setHtml(Ext.DomHelper.markup({
                tag: 'input',
                type: 'hidden',
                id: id,
                name: name
            }));
            childrenCount = 1;
            input = dom.firstChild;
        }
        while (childrenCount > valueCount) {
            dom.removeChild(childNodes[0]);
            -- childrenCount;
        }
        while (childrenCount < valueCount) {
            dom.appendChild(input.cloneNode(true));
            ++ childrenCount;
        }
        for (i = 0; i < valueCount; i++) {
            childNodes[i].value = values[i];
        }
    }		  		  
   
});
DCT.Util.reg('dctcombofield', 'interviewComboField', 'DCT.ComboField');
