
/**
*
*/
Ext.define('DCT.ProgressBar', {
  extend: 'Ext.ProgressBar',
  
	defaultWidth: 200,
	inputXType: 'dctprogressbar',
	dctControl: true,
	xtype: 'dctprogressbar',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
   	me.adjustValue(config);
   	me.setText(config);
   	me.setWidth(config);
   	me.callParent([config]);
		me.bindConfiguredEvents(config);
		me.setProgressBarCollection();
	},
  /**
  *
  */
  setWidth: function (config) {
		var me = this;
    if (!Ext.isDefined(config.width)) {
        config.width = me.defaultWidth;
    }
  },
  /**
  *
  */
  setText: function(config){
		var me = this;
    if (Ext.isEmpty(config.text)) {
    	config.text = me.getText(config.text, config.value);
    }
  },
  /**
  *
  */
  adjustValue: function (config) {
    if (!Ext.isEmpty(config.value)) {
        if (config.value > 1) config.value = 1;
    }
  },
  /**
  *
  */
  setProgressBarCollection: function () {
		var me = this;
  	if (!DCT.Util.progressBarsOnPage.containsKey(me.id)){
    	DCT.Util.progressBarsOnPage.add(me.id,this);
  	}
  },
  /**
  *
  */
  getText: function (text, value) {
    if (Ext.isEmpty(text)) {
        if (value == 1) text = DCT.T('Completed');
        else text = DCT.T("CompletedPercent", { percent: (value * 100).round() });
    }
    return text;
  }
});
DCT.Util.reg('dctprogressbar', 'interviewProgressBar', 'DCT.ProgressBar');
