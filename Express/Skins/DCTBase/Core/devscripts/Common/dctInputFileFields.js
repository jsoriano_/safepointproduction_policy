Ext.define('DCT.FileButton', {
    override: 'Ext.form.field.FileButton',

    onFileFocus: function(e) {
        var ownerCt = this.ownerCt;
        if (!this.hasFocus) {
            this.onFocus(e);
        }
        if (ownerCt && !ownerCt.hasFocus && !Ext.isIE) {
            ownerCt.onFocus(e);
        }
    }
});
/**
*
*/
Ext.define('DCT.InputFileField', {
  extend: 'Ext.form.field.File',

	inputXType: 'dctinputfilefield',
	defaultWidth: 300,
	dctControl: true,
	xtype: 'dctinputfilefield',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setCls(config);
		me.setDisable(config);
		me.setAllowBlank(config);
		me.setAutoCreate(config);
		me.setDefaultListeners(config);
		me.setListeners(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
			blur: {
				fn: me.fieldValidation,
				scope: me
			},
			change: {
				fn: me.fieldChanged,
				scope: me
			},
			focus: {
			    fn: function (field, e, eOpts) {
			        me.storeFocusField();
			        me.scrollIntoViewOnFocus(field);
			    },
			    scope: me
			},
			afterrender: {
				fn: me.setFieldFocus,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setAutoCreate: function (config) {
		var me = this;
		var controlSize = (Ext.isDefined(config.size)) ? config.size : 50;
		config.width = controlSize * 6.5 + 20;
		config.width = (config.width < me.defaultWidth) ? me.defaultWidth : config.width;
		config.size = 0;
		var attributeTemplate = '';
		if (Ext.isDefined(config.fieldRef)) { 
			attributeTemplate = attributeTemplate + 'fieldRef="' + config.fieldRef + '"' + ' objectRef="' + config.objectRef + '"'; 
		}
		if (Ext.isDefined(config.maxLength)) {
			config.enforceMaxLength = true; 
		}
		config.inputAttrTpl = attributeTemplate;
	},
	/**
	*
	*/
	setDefaultListeners: function (config) {
	}		  
   
});
DCT.Util.reg('dctinputfilefield', 'interviewFileField', 'DCT.InputFileField');

Ext.define('DCT.InputDropFileField', {
  extend: 'Ext.Container',
	
	inputXType: 'dctinputdropfilefield',
	xtype: 'dctinputdropfilefield',
	width: 'auto',
	dctControl: true,
	labelHtml: '<div class=droparea-instructions><div class=droparea-file-location>' + DCT.T('DragDropFilesHere') + '</div><div class=droparea-click-location>' + DCT.T('OrClickToBrowse') + '</div></div>',
	autoEl: 'div',
	file: {},
	
	/**
	*
	*/
	initComponent: function () {
		var me = this;
		me.callParent([arguments]);
	},

	// private
	/**
	*
	*/
	onRender: function (ct, position) {
		var me = this;
		me.callParent([arguments]);
		me.addCls('x-form-file-label');
		me.wrap = me.el.wrap({
			cls: 'x-form-field-wrap x-form-file-wrap dropArea'
		});
	
		me.wrap.dom.addEventListener("drop", function (e) { return me.onDrop(e); }, true);
		me.wrap.dom.addEventListener("dragenter", function (e) { return me.onDragEnter(e); }, false);
		me.wrap.dom.addEventListener("dragleave", function (e) { return me.onDragLeave(e); }, false);
		me.wrap.dom.addEventListener("dragover", function (e) { return me.onDragEnter(e); }, false);
		me.wrap.dom.addEventListener("click", function (e) { return me.onClick(e); }, false);
		
		me.setHtml(me.labelHtml);
		me.el.dom.removeAttribute('name');
		me.createFileInput();
		me.bindListeners();
		me.resizeEl = me.positionEl = me.wrap;
	},
	/**
	*
	*/
	setHtml: function (html) {
		var me = this;
		me.el.dom.innerHTML = html;
	},
	/**
	*
	*/
	onClick: function (e) {
		var doc = document;
		var target = this.fileInput.dom;
		if (target.click) {
			target.click();
		}
		else if (doc.createEvent) {
			e = new Event('click');
			target.dispatchEvent(e);
		} else {
			e = doc.createEventObject();
			target.fireEvent('onclick', e);
		}
	},
	/**
	*
	*/
	onDragEnter: function (e) {
		var me = this;
		me.stopEvent(e);
		me.wrap.addCls('file-hover');
	},
	/**
	*
	*/
	onDragLeave: function (e) {
		var me = this;
		me.stopEvent(e);
		me.wrap.removeCls('file-hover');
	},
	/**
	*
	*/
	onDrop: function (e) {
		var me = this;
		me.onDragLeave(e);
		me.file = e.dataTransfer.files[0];
		me.fileInput.remove();
		me.createFileInput();
		me.setHtml(me.file.name.split(/(\\|\/)/g).pop());
	},
	/**
	*
	*/
	stopEvent: function (e) {
		e.stopPropagation();
		e.preventDefault();
	},
	/**
	*
	*/
	bindListeners: function () {
		var me = this;
		me.fileInput.on({
			scope: this,
			change: function(){
				var v = this.fileInput.dom.value;
				this.file = {};
				var fileName = v.split(/(\\|\/)/g).pop();
				fileName = fileName === '' ? this.labelHtml : fileName;
				this.setHtml(fileName);
				this.fireEvent('fileselected', this, v);
				document.forms[0].encoding = "multipart/form-data";
			}
		});

		if (!Ext.isEmpty(me.button)) {
			me.button.on({
				scope: this.button,
				mouseenter: function () {
					this.addCls(['x-btn-over', 'x-btn-focus'])
				},
				mouseleave: function () {
					this.removeCls(['x-btn-over', 'x-btn-focus', 'x-btn-click'])
				},
				mousedown: function () {
				this.addCls('x-btn-click')
				},
				mouseup: function () {
					this.removeCls(['x-btn-over', 'x-btn-focus', 'x-btn-click'])
				}
			});
		}
	},

	/**
	*
	*/
	createFileInput: function () {
		var me = this;
		me.fileInput = me.wrap.createChild({
			id: me.getFileInputId(),
			name: me.name||me.getId(),
			cls: 'x-form-file',
			tag: 'input',
			type: 'file',
			size: 1
		});
	},

	/**
	*
	*/
	reset: function () {
		var me = this;
		me.fileInput.remove();
		me.createFileInput();
		me.bindListeners();
		me.setHtml(me.labelHtml);
	},

	// private
	/**
	*
	*/
	getFileInputId: function () {
		var me = this;
		return me.id + '-file';
	},

	// private
	/**
	*
	*/
	onResize: function (w, h) {
		var me = this;
		me.callParent([arguments]);
		me.wrap.setWidth(w);
	},

	// private
	/**
	*
	*/
	onDestroy: function () {
		var me = this;
		me.callParent([arguments]);
		Ext.destroy(me.fileInput, me.button, me.wrap);
	},

	/**
	*
	*/
	onDisable: function () {
		var me = this;
		me.callParent([arguments]);
		me.doDisable(true);
	},

	/**
	*
	*/
	onEnable: function () {
		var me = this;
		me.callParent([arguments]);
		me.doDisable(false);

	},
	
	// private
	/**
	*
	*/
	doDisable: function (disabled) {
		var me = this;
		me.fileInput.dom.disabled = disabled;
	},


	// private
	preFocus : Ext.emptyFn,

	// private
	/**
	*
	*/
	alignErrorIcon: function () {
		var me = this;
		me.errorIcon.alignTo(me.wrap, 'tl-tr', [2, 0]);
	}

});

DCT.Util.reg('dctinputdropfilefield', 'inputDropFileField', 'DCT.InputDropFileField');
