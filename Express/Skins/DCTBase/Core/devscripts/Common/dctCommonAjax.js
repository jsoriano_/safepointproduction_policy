/**
 * @class DCT.Ajax
 * This class defines the ajax processing for the interview pages.
 */
Ext.define('DCT.Ajax', {
	/**
	* When a second request has been executed before the first request is complete you can set 'needsSubmitAjax' 
	to true and store the second request into 'delayedSubmitAjax' which will only execute after the first request has concluded.
	*/
	needsSubmitAjax: false,
	/**
	* When a request has been executed, the processingBlur flag is set to true and only once the request has completed does this get set back to false.
	*/
	processingBlur: false,
	/** 
	* To Check if the Checkbox has been checked
	*/
	chkboxChanged: false,
	/**
	* Count of current Ajax Requests being processed.
	*/
	requestCount: 0,
	/**
	* See needsSubmitAjax
	*/
	delayedSubmitAjax: null,
	holdActionField: null,
	ajaxType: "",

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.callParent([config]);
	},
	/**
	*
	*/
	postServerMessage: function (xmlText, headerText, callback, loadingMessage, invisible) {
		var me = this;
		var parmsData = null;
		var xmlData = null;
		me.processingBlur = true;
		me.ajaxType = "";

		if (headerText != 'Ajax/help' && !invisible) {
			if (Ext.get('loadingWindow')) {
				var waitTime;
				if (!Ext.isEmpty(loadingMessage)) {
					DCT.LoadingMessage.hold();
					DCT.LoadingMessage.set(loadingMessage);
					waitTime = 1;
				} else
					waitTime = DCT.LoadingMessage.getWaitTime();
				DCT.LoadingMessage.delayedShow(waitTime);
			}
		}

		if (headerText.search("Ajax") != -1)
			xmlData = xmlText;
		else
			parmsData = xmlText;
		Ext.Ajax.request({
			url: DCT.Util.getProxyUrl(),
			headers: { 'Content-Type': headerText + '; charset=UTF-8' },
			useDefaultHeader: false,
			params: parmsData,
			xmlData: xmlData,
			//Note: Ext.Ajax.timeout seems to have a max timeout of roughly 105 minutes,
			//unless you pass the value by config.
			timeout: Ext.Ajax.timeout,
			scope: me,
			callback: function (options, success, response) {
				if (!invisible) {
					DCT.LoadingMessage.hideAndCancel();
				}
			},
			success: function (transport, options) {
				var me = this;
				try {
					if (headerText == 'PreEvent/xml') {
						headerText = 'text/xml';
					}

					var response = (headerText == 'text/xml') ? transport.responseXML : Ext.decode(transport.responseText, true);
					var isHTML = ((headerText != 'text/xml') && (response == null) && (!Ext.isEmpty(transport.responseText)) && (transport.responseText.match(/html/i) != null));
					if (!Ext.isEmpty(response)) {
						if (Ext.isIE && (headerText == 'text/xml')) {
							var doc = new ActiveXObject('Msxml2.DOMDocument.6.0');
							doc.loadXML(transport.responseText);
							response = doc;
						}
						if (response.sessionTimedOut == 1) {
							DCT.Submit.submitPage('home', '');
							me.processingBlur = false;
							return;
						} else {
							callback(response);
						}
					} else {
						if (isHTML) {
							DCT.LoadingMessage.hideAndCancel();
							document.open();
							document.clear();
							document.write(transport.responseText);
							document.close();
							return;
						}

						try
						{
							if (transport.responseXML != null) {
								response = transport.responseXML;
							}

							if (Ext.isIE && (headerText == 'application/x-www-form-urlencoded')) {
								var doc = new ActiveXObject('Msxml2.DOMDocument.6.0');
								doc.loadXML(transport.responseText);
								response = doc;
							}

							var asyncNode = DCT.Util.getSingleNode(response, '//*[@asyncID and @status="success"]/@asyncID');
							if (asyncNode != null) {
								var asyncValue = DCT.Util.getNodeText(asyncNode);
								if (asyncValue.length > 0) {
									DCT.ProcessQueue.init(asyncValue);
								}
							}
						}
						catch (err) {
							//Do Nothing. (Async).
						}
					}
				} 
				catch (err) {
					var errorMessage = (err.message.indexOf(":") > -1) ? err.message.substring(0, err.message.indexOf(":")) : err.message;
					errorMessage = (errorMessage.charAt(errorMessage.length - 1) != '.') ? (errorMessage + ".") : errorMessage;
					var errorXml = '<errors><error express="1">' + DCT.T("AjaxPageError") + '</error><error express="1">' + DCT.T("AjaxProcessingError", { message: errorMessage }) + '</error></errors>';
					me.handleErrorType({ value: errorXml });
					me.processingBlur = false;
				}
				me.processingBlur = false;
				Ext.Function.defer(DCT.Util.resyncTabPanels, 200, me);
				var blurPost = me.holdActionField != null && !Ext.isEmpty(Ext.get(me.holdActionField.id));
				if (blurPost) {
					me.onBlurPost(this.holdActionField);
				}
				else if (me.needsSubmitAjax) {
					me.needsSubmitAjax = false;
					Ext.Function.defer(me.delayedSubmitAjax, 100, me);
				}
				me.holdActionField = null;

			},
			failure: function (transport, options) {
				var me = this;
				var txt = DCT.T("AjaxTransportError", { text: transport.statusText, status: transport.status });
				Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: txt }).show();
				me.processingBlur = false;
				me.needsSubmitAjax = false;
			}
		});
	},
	/**
	*
	*/
	ajaxSetFocus: function () {
		var fieldFocus = DCT.Util.getFocusField();
		if (!Ext.isEmpty(fieldFocus)) {
			DCT.Util.activateField(fieldFocus)
		}
	},
	/**
	*
	*/
	createStoreXML: function (action) {
		var me = this;
		var htmlContentPath = '';
		var blurPostHTMLContentPath = Ext.getDom('_blurPostHTMLContentPath');

		if (blurPostHTMLContentPath != null)
			htmlContentPath = ' htmlContentPath="' + blurPostHTMLContentPath.value + '"';

		return '<AjaxRequest' + htmlContentPath + '><action>' + action + '</action><currentPageID>'
			+ Ext.getDom('_currentPageID').value + '</currentPageID><popUp>'
			+ Ext.getDom('_popUp').value + '</popUp>' + me.buildStoreData(document.forms[0]) + '</AjaxRequest>';
	},
	/**
	* onBlurPost is used to update a page as a control loses focus. For instance, refresh page on chg = true,
	* or the need to run a post-action field as part of a custom action, etc.
	*/
	onBlurPost: function (field) {
		var me = this;
		var skipRequiredFields = true;
		if (!DCT.Util.validateFields(skipRequiredFields)) { return false; }
		if (me.processingBlur) {
			me.holdActionField = field;
			return;
		}
		me.holdActionField = null;
		if (DCT.Util.checkFieldsChanged() || DCT.FieldsChanged.fieldHasChanged()) {
			me.processingBlur = true;
			var xml = me.createStoreXML(field.dctActionId);
			var callbackFunc = function (responseDoc) {
				me.processFieldsNew(responseDoc);
			};
			me.postServerMessage(xml, "Ajax/xml", Ext.Function.createDelayed(callbackFunc, 0, me), null);
		}
	},
	/**
	*
	*/
	replaceScript: function (oldScript, replaceScriptHTML) {
		var me = this;
		var regex = /<script([\s\S]*<\/)script>/;
		var scriptDiv = document.createElement("div");
		scriptDiv.innerHTML = replaceScriptHTML.replace(regex, '<div$1div>');
		var newScript = document.createElement('script');
		Ext.each(scriptDiv.firstChild.attributes, function (item, index) {
			newScript.setAttribute(item.nodeName, item.nodeValue);
		}, this);
		newScript.text = scriptDiv.firstChild.innerHTML;
		oldScript.parentNode.insertBefore(newScript, oldScript);
		oldScript.parentNode.removeChild(oldScript);
	},
	/**
	*
	*/
	getAttribute: function (fullPath) {
		var matches = fullPath.match(/@([a-zA-Z0-9_:-]+$)/);
		if (matches != null && matches.length > 1) {
			return matches[1];
		}
		else {
			return "";
		}
	},
	/**
	*
	*/
	getElementPath: function (fullPath) {
		var matches = fullPath.match(/(.*)(\/?@[a-zA-Z0-9_:-]+$)/);
		if (matches != null && matches.length > 1)
			return matches[1].replace(/\/$/, "");
		else
			return fullPath.replace(/\/$/, "");
	},
	/**
	*
	*/
	replaceComponent: function (oldComponent, dataConfig, newClass) {
		var me = this;
		var newXType = null;
		if (Ext.isEmpty(newClass)) {
			newXType = oldComponent.getXType();
		}
		var oldType = oldComponent.getXType();
		oldComponent.destroy();
		var comboTip = (oldType == 'dctcombofield') ? Ext.getCmp(dataConfig.id + 'tooltip') : null;
		if (!Ext.isEmpty(comboTip)) { comboTip.destroy(); }
		Ext.each(DCT.Util.classArray, function (item, index) {
			if ((newXType != null && item.xtype == newXType)
			|| (newClass != null && item.htmlclass == newClass)) {
				var attribs = {};
				Ext.apply(attribs, dataConfig);
				var newCmp = Ext.create(item.cls, attribs);
				return false;
			}
		}, me);
	},
	/**
	*
	*/
	setDomAttribute: function (element, attribute, value) {
		if (Ext.isIE) {
			// todo: first begin with on* events, then do the following switch
			switch (attribute) {
				case "class":
					element.className = value;
					break;
				case "className":
					element.className = value;
					break;
				case "style":
					element.style.cssText = value;
					break;
				case "href":
					element.href = value;
					break;
				case "src":
					element.src = value;
					break;
				case "acceptcharset":
					element.acceptCharset = value;
					break;
				case "accesskey":
					element.accessKey = value;
					break;
				case "allowtransparency":
					element.allowTransparency = value;
					break;
				case "bgcolor":
					element.bgColor = value;
					break;
				case "cellpadding":
					element.cellPadding = value;
					break;
				case "cellspacing":
					element.cellSpacing = value;
					break;
				case "colspan":
					element.colSpan = value;
					break;
				case "defaultchecked":
					element.defaultChecked = value;
					break;
				case "defaultselected":
					element.defaultSelected = value;
					break;
				case "defaultvalue":
					element.defaultValue = value;
					break;
				case "for":
					element.htmlFor = value;
					break;
				case "frameborder":
					element.frameBorder = value;
					break;
				case "hspace":
					element.hSpace = value;
					break;
				case "longdesc":
					element.longDesc = value;
					break;
				case "maxlength":
					element.maxLength = value;
					break;
				case "marginwidth":
					element.marginWidth = value;
					break;
				case "marginheight":
					element.marginHeight = value;
					break;
				case "noresize":
					element.noResize = value;
					break;
				case "noshade":
					element.noShade = value;
					break;
				case "readonly":
					element.readOnly = value;
					break;
				case "rowspan":
					element.className = value;
					break;
				case "tabindex":
					element.tabIndex = value;
					break;
				case "valign":
					element.vAlign = value;
					break;
				case "vspace":
					element.vSpace = value;
					break;
				default:
					element.setAttribute(attribute, value);
			}
		} else {
			if (element == document) {
				htmlParent = element.getElementById("htmlParent");
				if (htmlParent)
					htmlParent.setAttribute(attribute, value);
			}
			else
				element.setAttribute(attribute, value);
		}
	},
	/**
	*
	*/
	processFieldsNew: function (responseArray) {
		var me = this;
		var fieldArray = Ext.query(".controlContainer", true, Ext.getDom("body"));
		var fieldCollection = Ext.create('Ext.util.MixedCollection', {});
		(DCT.Util.shiftKeyUsed) ? fieldCollection.addAll(fieldArray.reverse()) : fieldCollection.addAll(fieldArray);
			
		var itemArray = responseArray.items;
		var pendingClassChanges = Ext.create('Ext.util.MixedCollection', {});
		DCT.Sections.backupCollapsedStates();
		Ext.each(itemArray, function (item, index) {
			var me = this;
			var idElem = me.getIdedElement(item.id);
			me.processResponseItem(item, idElem, pendingClassChanges);
		}, this);

		DCT.Sections.initialize();

		if (DCT.notifyMessage) {
			Ext.create('DCT.AlertMessage', {
				msg: DCT.notifyMessage
			}).show();
		}

		me.assignFieldFocus(fieldCollection);
		DCT.hubEvents.fireEvent('ajaxcomplete');
	},
	/**
	*
	*/
	assignFieldFocus: function (priorFieldCollection) {
		var me = this;
		if(!me.assignFocusToNewField(priorFieldCollection)) {
			me.assignFocusToField(priorFieldCollection);
		}
	},
	/**
	*
	*/
	assignFocusToNewField: function (priorFieldCollection) {
		var me = this;
		var elemFocusId = DCT.Util.getFocusField();
		var focusField = priorFieldCollection.get(elemFocusId + 'Cmp');
		var focusIndex = priorFieldCollection.indexOfKey(elemFocusId + 'Cmp');
		var priorFocusIndex = focusIndex - 1;
		var priorFocus = priorFieldCollection.getAt(priorFocusIndex);
		if (!Ext.isEmpty(Ext.get(elemFocusId)) && DCT.Util.tabKeyUsed && priorFocus) {
			//if the focus field exists on the new page
			var priorFocusId = priorFocus.getAttribute("data-cmpid");
			if (!Ext.isEmpty(Ext.get(elemFocusId)) && !Ext.isEmpty(Ext.get(priorFocusId))) {
				//and the prior field does as well
				var fieldArray = Ext.query(".controlContainer", true, Ext.getDom("body"));
				var fieldCollection = Ext.create('Ext.util.MixedCollection', {});
				(DCT.Util.shiftKeyUsed) ? fieldCollection.addAll(fieldArray.reverse()) : fieldCollection.addAll(fieldArray);
				//find any new fields that appeared in the direction that we tabbed to
				priorFocusIndex = fieldCollection.indexOfKey(priorFocusId + 'Cmp');
				var newFocus = fieldCollection.getAt(++priorFocusIndex);
				if (newFocus) {
					var currentFocusId = focusField.getAttribute("data-cmpid");
					var control = Ext.getCmp(currentFocusId);
					if (Ext.isEmpty(control) || !control.isDirty()) {
						//if the current focused field hasn't been modified, 
						//then change focus to the new field that appeared
						elemFocusId = newFocus.getAttribute("data-cmpid");
						me.setFocusToField(elemFocusId);
						return true;
					}
				}
			}
		}
		return false;
	},
	/**
	*
	*/
	assignFocusToField: function (priorFieldCollection) {
		var me = this;
		var elemFocusId = DCT.Util.getFocusField();
		var focusIndex = priorFieldCollection.indexOfKey(elemFocusId + 'Cmp');
		if (Ext.isEmpty(Ext.get(elemFocusId))) {
			var distance = 1;
			var focusNext;
			var focusPrior;
			var focusFields = [];
			var focus;
			if (DCT.Util.tabKeyUsed) {
				//get list of fields to attempt to focus by looking in the direction that was tabbed first
				while ((focus = priorFieldCollection.getAt(++focusIndex))) {
					focusFields.push(focus);
				}
				focusIndex = priorFieldCollection.indexOfKey(elemFocusId + 'Cmp');
				while ((focus = priorFieldCollection.getAt(--focusIndex))) {
					focusFields.push(focus);
				}
			}
			else {
				//get list of fields to attempt to focus by looking for the closest field
				focusNext = priorFieldCollection.getAt(focusIndex + distance);
				focusPrior = priorFieldCollection.getAt(focusIndex - distance);
				while (focusNext || focusPrior) {
					distance++;
					focusFields.push(focusNext);
					focusFields.push(focusPrior);
					focusNext = priorFieldCollection.getAt(focusIndex + distance);
					focusPrior = priorFieldCollection.getAt(focusIndex - distance);
				}
			}
			//attempt to focus a field if it can be found on the page
			for (var i = 0; i < focusFields.length; i++) {
				focus = focusFields[i];
				if (focus) {
					var focusId = focus.getAttribute("data-cmpid");
					if (!Ext.isEmpty(Ext.get(focusId))) {
						me.setFocusToField(focusId);
						DCT.Util.setKeysUsed(false, false);
						return;
					}
				}
			}
		}
		DCT.Util.setFocusField(elemFocusId);
		DCT.Util.setKeysUsed(false, false);
		me.ajaxSetFocus();
	},
	/**
	*
	*/
	findValidField: function (item, key) {
		var itemId = item.getAttribute("data-cmpid");
		var fieldComp = Ext.getCmp(itemId);
		if (fieldComp && (fieldComp.getXType() != 'dctinputdisplayfield'))
			return true;
	},
	/**
	*
	*/
	setFocusToField: function (fieldId) {
		var me = this;
		var extComp = Ext.getCmp(fieldId);
		if (extComp) {
			var xType = extComp.getXType();
			(xType != 'dctcheckboxgroup' && xType != 'dctradiogroup') ? extComp.focus(true, 5) : me.setGroupItemFocus(extComp);
			DCT.Util.setFocusField(fieldId);
		}
	},
	/**
	*
	*/
	setGroupItemFocus: function (field) {
		var fieldItem = null;
		field.items.each(function (item) {
			if (item.checked) {
				fieldItem = item;
				return false;
			}
		});
		fieldItem = (fieldItem) ? fieldItem : field.items.items[0];
		if (fieldItem.id != DCT.Util.elemFocus)
			fieldItem.focus(false, 5);
	},
	/**
	*
	*/
	getIdedElement: function (itemId) {
		var idElem = Ext.get(itemId);
		if (!idElem) {
			idElem = document;
		} else {
			idElem = idElem.dom;
		}
		return idElem;
	},
	/**
	*
	*/
	processResponseItem: function (item, idElem, pendingClassChanges) {
		var me = this;
		switch (item.type) {
			case "change":
				me.handleChangeType(item, idElem, pendingClassChanges);
				break;
			case "add":
				me.handleAddType(item, idElem);
				break;
			case "delete":
				me.handleDeleteType(item, idElem);
				break;
			case "error":
				me.handleErrorType(item);
				break;
		}
	},
	/**
	*
	*/
	handleBrowserClasses: function (item, idElem) {
		var me = this;
		var browserClasses = idElem.className.split(" ");
		Ext.each(browserClasses, function (browserClass, index) {
			if (browserClass.substring(0, 2) === "x-") {
				item.value += " " + browserClass;
			}
		}, me);
	},
	/**
	*
	*/
	handleChangeType: function (item, idElem, pendingItems) {
		var me = this;
		var selectPath = me.getElementPath(item.path);
		var oldElem = me.fixNode(selectPath, idElem);

		if (Ext.isDefined(oldElem)) {
			if (oldElem.id === "htmlBody" && item.path === "/@class") {
				me.handleBrowserClasses(item, idElem);
			}
		}
		if (/Cmp$/.test(item.id) && /\/div\//.test(item.path)) {
			var componentId = item.id.substring(0, item.id.length - 3);
			// We are having to make an assumption here: when a field changes component types, the class change will
			// come in first, then the the data-config will get updated afterwards.
			if (/@class$/.test(item.path)) {
				// Store the class so we can build the right type of object when new data-config comes in.
				pendingItems.add(componentId, item.value);
			} else if (/@data-config$/.test(item.path)) {
				item.value = item.value.replace(/\n/g, "\\n").replace(/\r/g, "");
				var config = Ext.decode(item.value);
				componentId = config.id;
				var oldCmp = Ext.getCmp(componentId);
				var newClass = null;
				componentId = componentId.replace("Grp", "");
				if (pendingItems.containsKey(componentId)) {
					newClass = pendingItems.get(componentId);
					pendingItems.removeAtKey(componentId);
				}
				if (Ext.isDefined(oldCmp.value) && oldCmp.getValue) {
					config.value = (oldCmp.isXType('datefield') || oldCmp.isXType('dctinputnumberfield')) ? oldCmp.getRawValue() : oldCmp.getValue();
				} else {
					config.value = "";
				}
				me.replaceComponent(oldCmp, config, newClass);
			}
			else if (/@data-value$/.test(item.path)) {
				var oldCmp = Ext.getCmp(componentId);
				oldCmp.setValue(item.value);
				oldCmp.originalValue = item.value;
			}
		} else if (/_gridHiddenInputs$/i.test(item.id) && /@data-grid-xml$/i.test(item.path)) {
			var itemId = item.id;
			var gridComponentId = itemId.replace("_gridHiddenInputs", "");

			var gridXml = DCT.Util.loadXMLString(item.value);
			var gridComponent = Ext.getCmp(gridComponentId);

			// if user tries to edit while page is refreshing, sometimes it can cause an error as this store reloads.
			var cellEditor = gridComponent.getPlugin('dctcellediting');
			if (cellEditor)
				cellEditor.cancelEdit();

			var gridStore = gridComponent.getStore();

			gridStore.proxy.setData(gridXml);
			gridStore.load();

			me.setDomAttribute(oldElem, 'data-grid-xml', item.value);
		} else {
			var attributeName = me.getAttribute(item.path);
			var isAttribute = (attributeName.length > 0);
			var isTextChange = (item.path.search('text()') > -1);
			if (oldElem) {
				if (isAttribute) {
					me.setDomAttribute(oldElem, attributeName, item.value);
				}
				else if (isTextChange) {
					oldElem.innerText = item.value;
				}
				else {
					if (oldElem.nodeName && oldElem.nodeName.toLowerCase() == "script") {
						me.replaceScript(oldElem, item.value);
					}
					else {
						if (oldElem.tagName == 'A' || oldElem.tagName == 'LI' || (item.value.indexOf("<div") == 0)) {
							oldElem.outerHTML = item.value;
						}
						else {
							oldElem.innerHTML = item.value;
						}
					}
				}
			}
		}
	},
	/**
	*
	*/
	createTempElement: function (item, existingParent) {
		var me = this;
		var elementType = existingParent.nodeName.toLowerCase();
		var returnElement = null;
		var returnParent = null;
		var tablePart = me.isTablePart(elementType);
		if (tablePart) {
			returnParent = document.createElement(elementType);
			returnElement = Ext.DomHelper.insertHtml("beforeEnd", returnParent, item.value);
			if (elementType == "table") {
				returnParent = returnParent.firstChild;
			}
		} else {
			returnParent = Ext.DomHelper.createDom({ tag: "div" });
			var matchItem = /(?:<script([^>]*)?>)/ig.test(item.value);
			if (matchItem) {
				var tempElement = Ext.get(returnParent).setHtml(item.value, true);
				returnParent = tempElement.dom;
			} else {
				returnElement = Ext.DomHelper.insertHtml("beforeEnd", returnParent, item.value);
			}
		}
		return returnParent;
	},
	/**
	*
	*/
	isTablePart: function (elementType) {
		return elementType == "table" || elementType == "tbody" || elementType == "thead" || elementType == "tfoot" ||
			elementType == "tr" || elementType == "td" || elementType == "th";
	},
	/**
	*
	*/
	handleAddType: function (item, idElem) {
		var me = this;
		var attributeName = me.getAttribute(item.path);
		var isAttribute = (attributeName.length > 0);
		var isTextChange = (item.path.search('text()') > -1);
		if (isAttribute) {
			var selectPath = me.getElementPath(item.path);
			var elem = me.fixNode(selectPath, idElem);
			me.setDomAttribute(elem, attributeName, item.value);
		} else if (isTextChange) {
			var parentElem = me.getParentNode(idElem, item);
			parentElem.innerText = item.value;
		} else {
			var parentElem = me.getParentNode(idElem, item);
			if (parentElem) {
				var childElem = me.getChildNode(parentElem, item);
				var newElement = me.createTempElement(item, parentElem);
				if (parentElem.childNodes.length > 0 && childElem) {
					Ext.DomHelper.insertHtml('beforeBegin', childElem, newElement.innerHTML);
				} else {
					Ext.DomHelper.insertHtml('beforeEnd', parentElem, newElement.innerHTML);
				}
				DCT.Util.generateControls(parentElem);
			}
		}
	},
	/**
	*
	*/
	handleDeleteType: function (item, idElem) {
		var me = this;
		var attributeName = me.getAttribute(item.path);
		var isAttribute = (attributeName.length > 0);
		if (isAttribute) {
			var selectPath = me.getElementPath(item.path);
			var elem = me.fixNode(selectPath, idElem);
			// todo: may need to work around IE bugs here, like we did for setDomAttribute()
			var success = elem.removeAttribute(attributeName);
		} else {
			// Get the elements from the DOM
			var parentElem = me.getParentNode(idElem, item);
			var childElem = me.getChildNode(parentElem, item);
			if (!Ext.isEmpty(childElem)) {
				// Clean-up ExtJS component manager
				var fieldArray = Ext.dom.Query.select('*[data-cmpid]', childElem);
				if (childElem.hasAttribute && childElem.hasAttribute('data-cmpid')) {
					fieldArray.push(childElem)
				}
				Ext.each(fieldArray, function (item, index) {
					var compId = item.getAttribute("data-cmpid");
					var extComp = Ext.getCmp(compId);
					if (extComp) {
						var oldType = extComp.getXType();
						extComp.destroy();
						var comboTip = (oldType == 'dctcombofield') ? Ext.getCmp(compId + 'tooltip') : null;
						if (!Ext.isEmpty(comboTip)) { comboTip.destroy(); }
					}
				}, this);
				childElem.parentNode.removeChild(childElem);
			}
		}
	},
	/**
	*
	*/
	handleErrorType: function (item) {
		DCT.Util.getSafeElement("_errorXml").value = item.value;
		DCT.Submit.submitPage("errors");
	},
	/**
	*
	*/
	fixpath: function (path) {
		// insert the assumed tbody into paths that are missing one of either tbody, thead, or tfoot
		var regex = new RegExp("((?:(?:tbody)|(?:thead)|(?:tfoot))/)?tr");
		return path.replace(regex, function ($0, $1) {
			return $1 ? $0 : "tbody/tr";
		});
	},
	/**
	*
	*/
	fixNode: function (path, root) {
		var me = this;
		var fixedPath = me.fixpath(path);
		return Ext.isEmpty(fixedPath) ? root : Ext.DomQuery.selectNode(fixedPath, root);
	},
	/**
	*
	*/
	getParentNode: function (idElem, item) {
		var me = this;
		var path = me.fixpath(item.path);
		var pathSize = path.lastIndexOf("/");
		pathSize = Math.max(0, pathSize);
		var parentPath = path.substring(0, pathSize);
		if ((idElem.tagName == 'TABLE') && (idElem.tBodies.length == 0)) parentPath = '';
		return me.fixNode(parentPath, idElem);
	},
	/**
	*
	*/
	getChildNode: function (parentNode, item, deletedRows) {
		var pathSize = (item.path.lastIndexOf("/") < 0) ? 0 : (item.path.lastIndexOf("/") + 1);
		var childPath = item.path.substring(pathSize, item.path.length);
		var elementName;
		var childNode = null;
		var index = 1;
		if (childPath.indexOf(":") > -1) {
			var parts = childPath.split(":");
			elementName = parts[0];
			var selector = parts[1];
			index = selector.split("(")[1].split(")")[0];
		}
		if (Ext.isEmpty(item.path)) {
			childNode = Ext.getDom(item.id);
		} else {
			var childElem = Ext.get(parentNode).first(childPath);
			if (!Ext.isEmpty(childElem)) {
				childNode = childElem.dom;
			}
		}
		childNode = (Ext.isEmpty(childNode)) ? ((parentNode.childNodes.length > 0) ? parentNode.childNodes[parseInt(index, 10) - 1] : undefined) : childNode;
		return childNode;
	},
	/**
	*
	*/
	doAjaxHelp: function (element, fieldId, cacheId, actionId, currentPageId, captionValue) {
		var me = this;
		var helpText = DCT.hoverHelp.get(fieldId);
		if (Ext.isEmpty(helpText)) {
			var xmlString = '<helpItems>' +
				'<fieldId>' + fieldId + '</fieldId>' +
				'<cacheId>' + cacheId + '</cacheId>' +
				'<actionId>' + actionId + '</actionId>' +
				'<currentPageId>' + currentPageId + '</currentPageId>' +
				'</helpItems>';
			var callbackFunc = function (responseDoc) {
				var me = this;
				me.processAjaxHelp(responseDoc, element, fieldId, captionValue);
			};
			me.postServerMessage(xmlString, "Ajax/help", Ext.Function.createDelayed(callbackFunc, 0, me), null);
		} else
			me.displayAjaxHelp(element, helpText, captionValue);
		return;
	},
	/**
	*
	*/
	processAjaxHelp: function (response, element, fieldId, captionValue) {
		var me = this;
		if (!Ext.isEmpty(response)) {
			var helpText = me.parseHelpText(response.actions);
			me.displayAjaxHelp(element, helpText, captionValue);
			if (helpText != 'Error')
				DCT.hoverHelp.add(fieldId, helpText);
		}
	},
	/**
	*
	*/
	displayAjaxHelp: function (element, helpText, caption) {
		var t = Ext.create('Ext.ToolTip', {
			target: element,
			dctControl: true,
			html: helpText,
			title: caption,
			trackMouse: true
		});
		var xy = Ext.get(element).getXY();
		xy[0] += 16; // x - empirically determined to be close
		xy[1] += 25; // y - empirically determined to be close
		t.showAt(xy);
	},
	/**
	*
	*/
	parseHelpText: function (objHelpText) {
		var me = this;
		var helpAnnotations = '';
		var arrayHelpText = objHelpText[0].items[0].helptext;
		var helpCaption = objHelpText[0].items[0].caption;
		if (helpCaption != 'Error') {
			Ext.each(arrayHelpText, function (item, index) {
				if (index == 0)
					helpAnnotations = item;
				else
					helpAnnotations = helpAnnotations + '.  ' + item;
			}, this);
		} else
			helpAnnotations = helpCaption;
		return (helpAnnotations);
	},
	/**
	*
	*/
	createURLString: function (theForm, type) {
		var strNameValuePair = DCT.Util.getFormNameValuePairs(theForm, true);
		if (type === 'interfaceJson') {
			strNameValuePair = strNameValuePair + '&interfaceJson=true';
		}
		else if (type === 'ajax') {
			strNameValuePair = strNameValuePair + '&ajax=true';
		}
		return strNameValuePair;
	},
	/**
	*
	*/
	getNewSubmitForm: function (popup) {
		var parentWindow = window;
		if (popup == 2) {
			parentWindow = DCT.Util.getParent(true);
		}
		var submitForm = parentWindow.document.createElement("FORM");
		parentWindow.document.body.appendChild(submitForm);
		submitForm.method = "POST";
		return submitForm;
	},
	/**
	*
	*/
	createNewFormElement: function (inputForm, elementName, elementValue) {
		var newElement = Ext.DomHelper.append(inputForm, { tag: 'input', type: 'hidden', name: elementName, id: elementName, value: elementValue });
		return newElement;
	},
	/**
	*
	*/
	convertToNamedPairs: function (querystring) {
		var me = this;
		var result = {};
		var kv = {};
		if (!querystring)
			return result;
		// substring(1) to remove the '?'   
		var pairs = querystring.substring(0).split("&");
		// Load the key/values of the return collection   
		Ext.each(pairs, function (item, index) {
			var idx = item.indexOf("=");
			if (idx > -1) {
				kv[0] = item.substring(0, idx);
				if (idx < item.length) {
					kv[1] = item.substring(idx + 1);
				}
			}
			result[kv[0]] = kv[1];
		}, this);
		return result;
	},
	/**
	*
	*/
	submitIntegrationReturn: function (popup, action, integrationImportRq, interviewAction) {
		var me = this;
		DCT.Util.getSafeElement('IntegrationImportRq').value = integrationImportRq;
		return me.submitInterfaceReturn(popup, action, "", interviewAction);
	},
	/**
	*
	*/
	submitPartyReturn: function (popup, action, partyId, interviewAction) {
		DCT.Util.getSafeElement('PartyID').value = partyId;
		return DCT.ajaxProcess.submitInterfaceReturn(popup, action, "", interviewAction);
	},
	/**
	*
	*/
	submitInterfaceReturn: function (popup, action, extraParameters, interviewAction) {
		var me = this;
		Ext.getDom('_submitAction').value = action;
		Ext.getDom('_action').value = interviewAction;
		if (popup == 2) {
			Ext.getDom('_popUp').value = 0;
		}
		var params = me.createURLString(document.forms[0], "interfaceJson");
		params = params + extraParameters;

		Ext.Ajax.request({
			url: DCT.Util.getProxyUrl(),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
			useDefaultHeader: false,
			params: params,
			scope: me,
			success: function (transport, options) {
				var me = this;
				try {
					if (popup == 2) {
						DCT.Util.closeWindow();
					}
					var json = Ext.util.JSON.decode(transport.responseText);
					var submitForm = me.getNewSubmitForm(popup);
					var kvPairs = me.convertToNamedPairs(json.postData);
					for (var i in kvPairs) {
						me.createNewFormElement(submitForm, i, kvPairs[i]);
					}

					if (json && json.items && json.items.length > 0 && json.items[0].type == 'error') {
						me.handleErrorType({ value: json.items[0].value });
						return (null);
					}

					submitForm.action = json.returnUrl;

					try {
						submitForm.submit();
					}
					catch (err) {
						var description = err.message.substring(0, err.message.indexOf(":"));
						var url = submitForm.action;
						var txt = DCT.T("AjaxCallReturnError", { message: description, url: url });
						Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: txt }).show();
						return (null);
					}
				}
				catch (err) {
					var errorMessage = (err.message.indexOf(":") > -1) ? err.message.substring(0, err.message.indexOf(":")) : err.message;
					errorMessage = (errorMessage.charAt(errorMessage.length - 1) != '.') ? (errorMessage + ".") : errorMessage;
					var errorXml = '<errors><error express="1">' + DCT.T("AjaxPageError") + '</error><error express="1">' + DCT.T("AjaxProcessingError", { message: errorMessage }) + '</error></errors>';
					me.handleErrorType({ value: errorXml });
					return (null);
				}
			},
			failure: function (transport, options) {
				var txt = DCT.T("AjaxTransportError", { text: transport.statusText, status: transport.status });
				Ext.create('DCT.AlertMessage', { title: DCT.T('Alert'), msg: txt }).show();
				return (null);
			}
		});
	},
	/**
	*
	*/
	submitAjax: function (ajaxType, submitScreen, skipValidation, cancelChanges, submitType, loadingMsg, confirmText) {
		var me = this;
		if (me.processingBlur) {
			me.needsSubmitAjax = true;
			me.delayedSubmitAjax = function () { me.submitAjax(ajaxType, submitScreen, skipValidation, cancelChanges, submitType, loadingMsg, confirmText); }
		} else {
			if (ajaxType == 'PageNavigation') {
				me.processPage(submitScreen);
			} else {
				DCT.Util.customOnSubmit();
				me.processDiv(submitScreen, submitType, skipValidation, cancelChanges, confirmText, loadingMsg);
			}
		}
		return;
	},
	/**
	*
	*/
	submitPreviewAjax: function (ajaxType, submitScreen, skipValidation, loadingMsg) {
		var me = this;
		DCT.Util.customOnSubmit();
		if (skipValidation != '1') {
			if (!DCT.Util.validateFields()) { return false; }
		}
		DCT.Util.getSafeElement("_targetPage").value = "editPrintJob";
		DCT.Util.getSafeElement("_submitAction").value = "";
		DCT.Util.getSafeElement("_action").value = submitScreen;
		document.forms[0].action = DCT.postPage;
		document.forms[0].target = "";

		if (document.forms[0].encoding == "multipart/form-data") {
			DCT.Submit.submitForm();
		} else {
			me.completeContentInterfaceJson(loadingMsg);
		}
		return;
	},
	/**
	*
	*/
	processDiv: function (submitScreen, submitType, skipValidation, cancelChanges, confirmText, loadingMsg) {
		var me = this;
		if (skipValidation != '1') {
			if (!DCT.Util.validateFields()) {
				return false;
			}
		}
		Ext.getDom('_targetPage').value = "";
		Ext.getDom('_submitAction').value = "";
		Ext.getDom('_action').value = submitScreen;
		document.forms[0].action = DCT.postPage;
		document.forms[0].target = "";
		if (cancelChanges == '1')
			DCT.Util.resetFields();

		if ((submitType == 'Confirm') || (submitType == 'Delete')) {
			if (confirmText == null || confirmText.length == 0) {
				if (submitType == 'Confirm')
					confirmText = DCT.T("AreYouSure");
				else
					confirmText = DCT.T("AreYouSureDelete");
			}

			Ext.Msg.confirm(DCT.T('Confirm'), confirmText, function (btn, text) {
				var me = this;
				if (btn == 'yes') {
					me.completeContentAjax(loadingMsg);
					Ext.getDom('_action').value = "";
				} else {
					Ext.getDom('_action').value = "";
					return false;
				}
			}, this);
		} else {
			if (DCT.Util.tabComponents.length > 0 || document.forms[0].encoding == "multipart/form-data") {
				DCT.Submit.submitForm();
			} else {
				me.completeContentAjax(loadingMsg);
			}
			Ext.getDom('_action').value = "";
		}
	},
	/**
	*
	*/
	completeContentAjax: function (loadingMsg, callback) {
		var me = this;
		var changes = DCT.ajaxProcess.createStoreXML("noAction", DCT.manuScriptID, null, null, DCT.sessionID, null, true);
		DCT.Util.getSafeElement('_clientChanges').value = changes;
		var urlString = me.createURLString(document.forms[0], 'ajax');
		var callbackFunc = function (responseDoc) {
			var me = this;
			me.processFieldsNew(responseDoc);
			if (callback) {
				callback();
			}
			DCT.Util.getSafeElement('runInterviewActionAsync').value = false;
		};

		me.postServerMessage(urlString, "application/x-www-form-urlencoded", Ext.Function.createDelayed(callbackFunc, 0, me), loadingMsg);
	},

	completeContentInterfaceJson: function (loadingMsg) {
		var me = this;
		var urlString = me.createURLString(document.forms[0], 'interfaceJson');
		var callbackFunc = function (responseDoc) {
			var me = this;
			me.processFieldsNew(responseDoc);
		};
		me.postServerMessage(urlString, "application/x-www-form-urlencoded", Ext.Function.createDelayed(callbackFunc, 0, me), loadingMsg);
	},
	/**
	*
	*/
	processPage: function (page) {
		var me = this;
		DCT.globalPage = page;
		if (page == "logout" || page == "new") {
			DCT.globalSaveAction = "save";
			DCT.Submit.saveVerify(Ext.Function.createDelayed(me.completeProcessPage, 0, me));
		} else
			me.completeProcessPage('no');
	},
	/**
	*
	*/
	completeProcessPage: function (btn) {
		var me = this;
		if (btn == 'no') {
			if (DCT.currentPage != DCT.globalPage) {
				DCT.Submit.removeKeyNames();
				var startIndex = Ext.getDom('_startIndex');
				var displayCount = Ext.getDom('_displayCount');
				if (startIndex != null)
					startIndex.value = 1;
				if (displayCount != null)
					displayCount.value = 10;
			}
			var CurrentSearch = Ext.getCmp('lastSearchItem');
			if (Ext.isDefined(CurrentSearch))
				CurrentSearch.setValue('(all)');
			Ext.getDom('_submitAction').value = "";
			Ext.getDom('_targetPage').value = DCT.globalPage;
			me.completeContentAjax();
		} else
			if (btn == 'yes')
				DCT.Submit.completeSaveProcess();
	},
	/**
	*
	*/
	buildStoreData: function (theForm) {
		var me = this;
		me.fieldChangesBuilder.start();
		me.clientChangesBuilder.start();
		var fields;
		// store the field values for every field on the page, fields are those that DON'T start with '_'
		fields = Ext.get(theForm).query("INPUT,TEXTAREA");
		Ext.each(fields, function (fld, index) {
			var me = this;
			var fldName;
			var fldID;
			var datatype;
			var fldValue;
			var fldRole;
			var processItem;
			if (!(Ext.isEmpty(fld.name))) {
				fldName = fld.name;
				var fieldname = fldName.split('|');
				fldName = fieldname[0];
				fldRole = fld.getAttribute('role');
				if ((!(fldName.indexOf('~') === 0)) && (fldName.indexOf("_") > 0) && (!(fldName.indexOf('_') === 0)) && (fldName.indexOf("_") != 1) && (!(fldName.indexOf('#') === 0))) {
					processItem = true;
					if (fldRole === 'radio' || fldRole === 'checkbox') {
						if ((fld.checked || fldRole === 'checkbox') && (fld.type !== 'button')) {
							var cmpId = fld.getAttribute("data-cmpid");
							var cmpValue = Ext.getCmp(cmpId).getSubmitValue();
							if (cmpValue != null)
								processItem = true;
							else
								processItem = false;
						}
						else
							processItem = false;
					}
					if ((fld.name != null) && (fld.name != '') && processItem) {
						fldID = (Ext.isEmpty(fld.getAttribute('data-componentid'))) ? fld.id : fld.getAttribute('data-componentid');
						if (fldID.indexOf('Combo') != -1) {
							fldID = fldID.replace(/Combo/g, '');
						}
						if (fld.type === 'file' && fldID.indexOf('-button') !== -1) {
							fldID = fldID.replace(/\-button/g, '');
						}
						params = fldName.split('_');
						if (fldName.search('_') !== -1) {
							fldValue = fld.value;
							if (Ext.get(fld).hasCls('x-form-empty-field')) {
								fldValue = "";
							}
							if (fldRole == 'radio' || fldRole == 'checkbox') {
								fldID = fld.getAttribute("data-cmpid");
								fldValue = Ext.getCmp(fldID).getSubmitValue();
								if (fldRole == 'radio') {
									fldID = fldID.substring(0, fldID.lastIndexOf("_"));
								}
							}
							var formattedValue = fldValue;
							datatype = params[0];
							if ((datatype == 'date') && (fldValue.length > 0 && !(fldValue.indexOf('__cacheINDEX') === 0))) {
								fldValue = Ext.getCmp(fldID).getInternalDate();
							}
							else if ((datatype == 'int' || datatype == 'float') && !(fldValue.indexOf('__cacheINDEX') === 0)) {
								var fldCmp = Ext.getCmp(fldID);
								if (fldCmp && Ext.isDefined(fldCmp.formatting))
									fldValue = fldCmp.formatting.formatNumberToUS(fldValue);
							}
							var item = { name: fldName, id: fldID, value: fldValue, formattedValue: formattedValue };
							var component = Ext.getCmp(fldID);
							var newValue = formattedValue;
							if (fldRole === 'checkbox') {
								newValue = component.getValue();
							}

							//Dates are reference types so equivalence test of two dates fail even if their date values are same because they
							//are two different objects. For equivalence the date values from these objects should be compared. 
							if (datatype == 'date') {
							    if (!Ext.isEmpty(component) && Ext.isDefined(component.getValue)
                                    && !Ext.isEmpty(component.getValue()) && Ext.isDefined(component.getValue().getTime)
                                    && !Ext.isEmpty(component.originalValue) && Ext.isDefined(component.originalValue.getTime)) {
	
									//The getTime() method returns the number of milliseconds between midnight of January 1, 1970 and the specified date 
									//and serves a better option to compare two date values.
									var newDateValue = component.getValue() != null && component.getValue() != '' ? component.getValue().getTime() : null;
									var originalDateValue = component.originalValue != null && component.originalValue != '' ? component.originalValue.getTime() : null;

									if (newDateValue === originalDateValue) {
										return;
									}
								}
							}

							if (!Ext.isEmpty(component) && (newValue === component.originalValue
								|| (Ext.isDefined(component.getValue) && component.getValue() === component.originalValue))) {
								return;
							}
							else if (!Ext.isEmpty(component) && component.resetOriginalValue) {
								component.resetOriginalValue();
							}

							me.fieldChangesBuilder.addItem(item);
							me.clientChangesBuilder.addItem(item);
						}
					}
				}
			}
		}, me);

		return me.clientChangesBuilder.getResult() + me.fieldChangesBuilder.getResult();
	},
	/**
	*
	*/
	fieldChangesBuilder: {
		xml: "",
		start: function () {
			xml = "";
		},
		addItem: function (item) {
			var params = item.name.split('_');
			var iterativeIndex = 0;
			if (params.length > 2)
				iterativeIndex = params[2];
			xml += '<field id="" index="' + params[1] + '" path="" fieldId="' + item.id + '" cacheId="' + params[1] + '" value="' + DCT.ajaxProcess.htmlize(item.value) + '" iterativeIndex="' + iterativeIndex + '" formattedValue="' + DCT.ajaxProcess.htmlize(item.formattedValue) + '"/>';
		},
		getResult: function () {
			return '<fieldChanges>' + xml + '</fieldChanges>';
		}
	},
	/**
	*
	*/
	clientChangesBuilder: {
		list: {},
		start: function () {
			list = { items: [] };
		},
		addItem: function (item) {
			list.items.push({ path: "/div/@data-value", id: item.id + "Cmp", value: item.formattedValue, type: 'change' });
		},
		getResult: function () {
			return '<clientChanges>' + DCT.ajaxProcess.htmlizeElement(Ext.encode(list)) + '</clientChanges>';
		}
	},
	/**
	*
	*/
	htmlize: function (str) {
		str = str.replace(/\&/g, "&amp;");
		str = str.replace(/\</g, "&lt;");
		str = str.replace(/\>/g, "&gt;");
		str = str.replace(/\"/g, "&quot;");
		return str;
	},
	/**
	*
	*/
	htmlizeElement: function (str) {
		str = str.replace(/\&/g, "&amp;");
		str = str.replace(/\</g, "&lt;");
		str = str.replace(/\>/g, "&gt;");
		return str;
	},
	/**
	*
	*/
	waitForRequests: function (scope) {
		var me = this;
		var callback = me.waitForRequests.caller;
		var args = me.waitForRequests.caller.arguments;
		if (DCT.ajaxProcess.requestCount > 0) {
			DCT.LoadingMessage.show();
			var process = function () {
				callback.apply(scope, args);
				Ext.Ajax.un('allrequestscomplete', process);
				DCT.LoadingMessage.hide();
			}
			Ext.Ajax.on('allrequestscomplete', process);
			return true;
		}
		return false;
	}
});

Ext.Ajax.on('beforerequest', function (conn, options) {
	var me = this;
	DCT.ajaxProcess.requestCount++;
	if (Ext.isEmpty(options.params) || Ext.isEmpty(options.params["_windowGuid"])) {
		if (Ext.isEmpty(options.method) && Ext.isEmpty(options.params) && Ext.isEmpty(options.xmlData) && Ext.isEmpty(options.jsonData)) {
			options.method = "GET";
		}
		if (typeof options.params === "string") {
			options.url += "?_windowGuid=" + DCT.Util.getWindowGuid();
		}
		else if (Ext.isEmpty(options.params)) {
			options.params = { _windowGuid: DCT.Util.getWindowGuid() };
		}
		else {
			var params = options.params;
			params = params || {};
			var p = { _windowGuid: DCT.Util.getWindowGuid() };
			Ext.apply(params, p);
		}
	}
}, this);

Ext.Ajax.on('requestcomplete', function (conn, response, options) {
	var me = this;
	DCT.ajaxProcess.requestCount--;
	if (DCT.ajaxProcess.requestCount == 0) {
		Ext.Ajax.fireEvent('allrequestscomplete');
	}
}, this);

DCT.ajaxProcess = Ext.create('DCT.Ajax', {});

/**
 * Override to allow form documents to be uploaded through AJAX calls
 */
Ext.define('DCT.data.Connection', {
	override: 'Ext.data.Connection',
	/**
	 * Sets various options such as the url, params for the request
	 * @param {Object} options The initial options
	 * @param {Object} scope The scope to execute in
	 * @return {Object} The params for the request
	 */
	setOptions: function (options, scope) {
		var me = this,
				params = options.params || {},
				extraParams = me.getExtraParams(),
				urlParams = options.urlParams,
				url = options.url || me.getUrl(),
				cors = options.cors,
				jsonData = options.jsonData,
				method, disableCache, data;
		if (cors !== undefined) {
			me.setCors(cors);
		}
		// allow params to be a method that returns the params object
		if (Ext.isFunction(params)) {
			params = params.call(scope, options);
		}
		// allow url to be a method that returns the actual url
		if (Ext.isFunction(url)) {
			url = url.call(scope, options);
		}
		url = this.setupUrl(options, url);
		if (!url) {
			Ext.raise({
				options: options,
				msg: 'No URL specified'
			});
		}
		// check for xml or json data, and make sure json data is encoded
		data = options.rawData || options.binaryData || options.xmlData || jsonData || null;
		if (jsonData && !Ext.isPrimitive(jsonData)) {
			data = Ext.encode(data);
		}
		// Check for binary data. Transform if needed
		if (options.binaryData) {
			if (!Ext.isArray(options.binaryData)) {
				Ext.log.warn("Binary submission data must be an array of byte values! Instead got " + typeof (options.binaryData));
			}
			if (me.nativeBinaryPostSupport()) {
				data = (new Uint8Array(options.binaryData));
				if ((Ext.isChrome && Ext.chromeVersion < 22) || Ext.isSafari || Ext.isGecko) {
					data = data.buffer;
				}
			}
		}
		//  send the underlying buffer, not the view, since that's not supported on versions of chrome older than 22
		// make sure params are a url encoded string and include any extraParams if specified
		if (Ext.isObject(params)) {
			params = Ext.Object.toQueryString(params);
		}
		if (Ext.isObject(extraParams)) {
			extraParams = Ext.Object.toQueryString(extraParams);
		}
		if (!options.processFormData) {
			params = params + ((extraParams) ? ((params) ? '&' : '') + extraParams : '');
		} else {
			delete params._windowGuid;
		}
		urlParams = Ext.isObject(urlParams) ? Ext.Object.toQueryString(urlParams) : urlParams;
		params = this.setupParams(options, params);
		// decide the proper method for this request
		method = (options.method || me.getMethod() || ((params || data) ? 'POST' : 'GET')).toUpperCase();
		this.setupMethod(options, method);
		disableCache = options.disableCaching !== false ? (options.disableCaching || me.getDisableCaching()) : false;
		// if the method is get append date to prevent caching
		if (method === 'GET' && disableCache) {
			url = Ext.urlAppend(url, (options.disableCachingParam || me.getDisableCachingParam()) + '=' + (new Date().getTime()));
		}
		// if the method is get or there is json/xml data append the params to the url
		if ((method == 'GET' || data) && params) {
			url = Ext.urlAppend(url, params);
			params = null;
		}
		// allow params to be forced into the url
		if (urlParams) {
			url = Ext.urlAppend(url, urlParams);
		}
		return {
			url: url,
			method: method,
			data: data || params || null
		};
	}
});
