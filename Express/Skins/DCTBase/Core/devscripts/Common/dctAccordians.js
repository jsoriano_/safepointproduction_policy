/**
*
*/

Ext.define('DCT.Accordion', {
  extend: 'Ext.panel.Panel',
  requires: [
  	'Ext.layout.container.Accordion'
  ],
	layout: {
		type: 'accordion',
		titleCollapse: true,
		animate: false,
		activeOnTop: false
	},
	dctControl: true,
	collapsible: false,
	autoScroll: false,
	border: false,
	inputXType: 'dctaccordion',
	xtype: 'dctaccordion',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setAccordianItems(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setAccordianItems: function (config) {
		var me = this;
		me.items = [];
		Ext.get(config.dctAccordion).select(".accordionItem", true).each(function (fieldEl, compEl, itemIndex) {
			var itemConfig = fieldEl.getDataPrefixedAttributes();
			if (itemIndex === 0) {
				Ext.apply(itemConfig, {collapsed: false});
			} else {
				Ext.apply(itemConfig, {collapsed: true});
			}
			me.items.push(Ext.create('DCT.AccordionItem', itemConfig));
		}, me);
	}		  
});
DCT.Util.reg('dctaccordion', 'accordion', 'DCT.Accordion');


/**
*
*/
Ext.define('DCT.AccordionItem', {
  extend: 'Ext.panel.Panel',
  requires: [
  	'Ext.layout.container.Accordion'
  ],
	margins: '0 0 0 0',
	animCollapse: false,
	collapsed: true,
	autoHeight: true,
	autoScroll: false,
	inputXType: 'dctaccordionitem',
	xtype: 'dctaccordionitem',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.checkControlExists(config);
		me.setListeners(config);
		me.callParent([config]);
		me.bindConfiguredEvents(config);
	},
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
    config.listeners = {
			beforeexpand:{
				fn: function(panel) {
					panel.onExpandEvent(panel);
				},
				scope:me
			}
		};
	},	
	/** 
	*
	*/
	onExpandEvent: function () {
	  // Execute the Express action.
	  var me = this;
		if (!Ext.isEmpty(me.expandEvent)) {
			DCT.Util.preEventField(DCT.manuscriptID, me.expandEventUid, me.expandEvent, '0', '1', false, Ext.Function.createDelayed(me.processExpansion, 0, me));
		}	  
	},
	/** 
	*
	*/
	processExpandEvent: function (responseDoc) {
		if (Ext.isDefined(responseDoc.actions[0])){
			var doc = Ext.DomHelper.createDom({tag:'div', html: responseDoc.actions[0].items[0].html});
			var domPath = 'div[id=' + this.contentEl + ']';
			var newSectionNode = Ext.DomQuery.selectNode(domPath,doc);
			var newSectionHTML = newSectionNode.innerHTML;
	
	    // The new shared data needs to be included in the updated document if available.
	    var divShareDataNew = Ext.DomQuery.selectNode('div[id=divSharedData]',doc.document);
			var divSharedDataOld = Ext.get('divSharedData');
			Ext.getDom('_currentPageID').value = Ext.DomQuery.selectNode('input[id=_currentPageID]',doc.document).value;
			Ext.getDom('_stateID').value = Ext.DomQuery.selectNode('input[id=_stateID]',doc.document).value;
	    if (!Ext.isEmpty(divShareDataNew)) {
	        if (Ext.isEmpty(divSharedDataOld))
	            document.forms[0].appendChild(divShareDataNew);
	        else
	            divSharedDataOld.setHtml(divShareDataNew.innerHTML, true);
	    }
	    var section = Ext.get(this.contentEl);
	    var sectionDom = (section) ? section.dom : null;
	    var childElem = section.first();
	    var childNode = (childElem) ? childElem.dom : undefined;
			if (!Ext.isEmpty(sectionDom)) {
				// Clean-up ExtJS component manager
				var fieldArray = Ext.query(".controlContainer", true, sectionDom);
				Ext.each(fieldArray, function (item, index) {
					var compId = item.getAttribute("data-cmpid");
					var extComp = Ext.getCmp(compId);
					if (extComp) {
						var oldType = extComp.getXType();
						extComp.destroy();
						var comboTip = (oldType == 'dctcombofield') ? Ext.getCmp(compId + 'tooltip') : null;
						if (!Ext.isEmpty(comboTip)) { comboTip.destroy(); }
					}
				}, this);
				sectionDom.removeChild(childNode);
			}
			var matchItem = /(?:<script([^>]*)?>)/ig.test(newSectionHTML);
			if (matchItem) {
				section.setHtml(newSectionHTML, true);
			} else {
				Ext.DomHelper.insertHtml('beforeEnd', sectionDom, newSectionHTML);
			}
	    DCT.Util.generateControls(sectionDom);
	    DCT.Submit.resetEmptyText();
	    DCT.Util.initializeFields();
 			DCT.LoadingMessage.hideAndCancel();
			DCT.Util.removeTabs(true);
		}
	  return;
	},
	/** 
	*
	*/
	processExpansion: function () {
		var me = this;
		DCT.LoadingMessage.hideAndCancel();
		DCT.ajaxProcess.postServerMessage(DCT.ajaxProcess.createURLString(document.forms[0], 'formContent'), "application/x-www-form-urlencoded", Ext.Function.createDelayed(me.processExpandEvent, 0, me), null);
	}		  
   
});
