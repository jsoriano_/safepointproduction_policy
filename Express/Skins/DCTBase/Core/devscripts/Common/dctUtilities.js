
/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	elemFocus: '',
	tabKeyUsed: false,
	shiftKeyUsed: false,
	sharedSizeItem: null,
	progressBarsOnPage: Ext.create('Ext.util.MixedCollection', {}),
	classArray: [],
	tabComponents: Ext.create('Ext.util.MixedCollection', {}),
	skipValidation: false,

	/**
	*
	*/
	getFileExtension: function (filename) {
		// returns the extension of the file if it exists
		// if it doesn't exist, returns false
		if (filename.indexOf('.') != -1) {
			var ext = filename.split('.').pop();
			return (ext.length > 0) ? ext : false;
		} else {
			return false;
		}

	},

	/**
	*
	*/
	hideShowGroup: function (elementId, displayCheck) {
		var visibleClassName = "hideOffset";
		var element = Ext.get(elementId);
		if (element) {
			(displayCheck) ? element.removeCls(visibleClassName) : element.addCls(visibleClassName);
			element.select(".controlContainer,a", true).each(function (fieldEl) {
				var compId = fieldEl.getAttribute("data-cmpid");
				if (fieldEl.findParent('div[class*=hideOffset]', 10)) {
					if (Ext.isEmpty(compId))
						fieldEl.dom.tabIndex = -1;
					else
						DCT.Util.disableComponent(compId, true);
				} else {
					if (Ext.isEmpty(compId))
						fieldEl.dom.tabIndex = 0;
					else
						DCT.Util.disableComponent(compId, false);
				}
			});
		}
	},

	/**
	*
	*/
	hideShowByClass: function (elementId, displayCheck, className) {
		var visibleClassName = className;
		var element = Ext.get(elementId);
		if (element) {
			(displayCheck) ? element.removeCls(visibleClassName) : element.addCls(visibleClassName);
		}
	},
	/**
	*
	*/
	disableComponent: function (controlId, disable) {
		var extComponent = Ext.getCmp(controlId);
		if (Ext.isDefined(extComponent)) {
			extComponent.setDisabled(disable);
			if (!disable)
				extComponent.validate();
		}
	},
	/**
	*
	*/
	checkFieldsChanged: function () {
		var me = this;
		return Ext.ComponentMgr.each(me.findDirtyItem, me) ? false : true;
	},
	/**
	*
	*/
	findDirtyItem: function (key, item, itself) {
		var result = true;
		if (Ext.isDefined(item.getXType()) && !Ext.isEmpty(item.dctControl)) {
			if (Ext.isDefined(item.isDirty)) {
				result = !item.isDirty();
			}
		}
		return result;
	},
	/**
	*
	*/
	initializeFields: function () {
		DCT.FieldsChanged.clear();
	},
	/**
	*
	*/
	resetFields: function () {
		var me = this;
		Ext.ComponentMgr.each(function (key, item, itself) {
			if (Ext.isDefined(item.getXType()) && !Ext.isEmpty(item.dctControl)) {
				if (Ext.isDefined(item.reset)) {
					item.reset();
				}
			}
		}, me);
		DCT.FieldsChanged.clear();
	},
	/**
	*
	*/
	setFocusField: function (controlId) {
		var me = this;
		me.elemFocus = controlId;
		var focusField = Ext.getDom('_focusField');
		if (focusField) { focusField.value = me.elemFocus; }
	},
	/**
	*
	*/
	setKeysUsed: function (tabKey, shiftKey) {
		var me = this
		me.shiftKeyUsed = shiftKey;
		me.tabKeyUsed = tabKey;
	},
	/**
	*
	*/
	getFocusField: function (controlId) {
		var me = this;
		return (me.elemFocus);
	},
	/**
	*
	*/
	isInterviewControl: function (controlId) {
		var retValue = false;
		var control = Ext.getCmp(controlId);
		if (control) {
			if (Ext.isDefined(control.getXType()) && !Ext.isEmpty(control.dctControl)) {
				retValue = true;
			}
		}
		return (retValue);
	},
	/**
	* validateFields checks all ExtJs controls on the current page for problems, returns true if all controls validate.
	*/
	validateFields: function (skipRequiredFields, allowCancelChanges, callback) {
		var me = this;
		var valid = true;
		var inValidItem = null;
		if (!me.skipValidation) {
			Ext.ComponentMgr.each(function (key, item, itself) {
				if (Ext.isDefined(!item.isDestroyed && item.getXType()) && !Ext.isEmpty(item.dctControl)) {
					var priorError = false;
					if (item.invalidClass) {
						priorError = item.el.hasCls(item.invalidClass)
					}
					if (Ext.isDefined(item.validate) && !item.validate()) {
						var itemIsEmpty = Ext.Array.contains(item.getActiveErrors(), item.blankText);
						if (!(skipRequiredFields && itemIsEmpty)) {
							valid = false;
							inValidItem = item;
						}
						else if (DCT.skipRequiredHighlightOnBlur && skipRequiredFields && (!priorError) && itemIsEmpty) {
							item.clearInvalid();
						}
					}
				}
			}, me);
			if (!valid) {
				DCT.Sections.expandForInvalidFields();
				var isTabs = (inValidItem) ? inValidItem.el.findParent('.x-tab') : null;
				var message = DCT.T("InvalidDataOnPage");
				if (isTabs) {
					message = DCT.T("InvalidDataOnTabs");
				}
				var buttons = {};
				if (allowCancelChanges) {
					buttons = { cancel: DCT.T("UndoChangesContinue") }
				}
				Ext.create('DCT.AlertMessage', {
					title: DCT.T('Alert'),
					msg: message,
					buttonText: buttons,
					fn: function (result) {
						if (result === 'cancel') {
							DCT.Util.resetFields();
							callback();
						}
					}
				}).show();
			}
		}
		return valid;
	},
	/**
	*
	*/
	setSkipRequiredFields: function () {
		var me = this;
		if (!Ext.form.field.Base.prototype.skipRequiredChecking) {
			Ext.form.field.Base.prototype.skipRequiredChecking = true;
			Ext.ComponentMgr.each(function (key, item, itself) {
				if (Ext.isDefined(item.getXType()) && !Ext.isEmpty(item.dctControl)) {
					if (Ext.isDefined(item.allowBlank)) {
						item.allowBlank = true;
					}
				}
			}, me);
		}
	},
	/**
	*
	*/
	destroyFields: function () {
		var me = this;
		Ext.ComponentMgr.each(function (key, item, itself) {
			if (!Ext.isEmpty(item.dctControl)) {
				if (!item.isDestroyed)
					item.destroy();
			}
		}, me);
	},
	/**
	*
	*/
	getEditorControl: function (hiddenInput) {
		var defaultEditorControl = Ext.create('Ext.form.TextField', {});
		if (Ext.isDefined(hiddenInput)) {
			var controlXType = hiddenInput.getXType();
			if (controlXType.indexOf('dct') > -1) {
				var newconfig = null;
				var editorClass = null;
				switch (controlXType) {
					case DCT.ComboField.xtype:
						newconfig = Ext.copyTo({}, hiddenInput.initialConfig, 'allowBlank,store,tpl,listeners,fieldRef,objectRef');
						editorClass = 'DCT.EditorComboField';
						break;
					case DCT.InputDateField.xtype:
						newconfig = Ext.copyTo({}, hiddenInput.initialConfig, 'allowBlank,altFormats,dateFormat,format,listeners,maxLength,maxValue,minValue,showToday,fieldRef,objectRef');
						editorClass = 'DCT.InputDateField';
						break;
					case DCT.InputIntField.xtype:
					case DCT.InputFloatField.xtype:
						newconfig = Ext.copyTo({}, hiddenInput.initialConfig, 'allowBlank,dctNumberFormat,dctNumberMax,dctNumberMin,listeners,maxLength,regex,regexText,validator,formatter,formatting,maskRe,fieldRef,objectRef');
						editorClass = 'DCT.EditorNumberField';
						break;
					case DCT.InputEmailField.xtype:
					case DCT.InputIPField.xtype:
					case DCT.InputZipCodeField.xtype:
					case DCT.InputTimeField.xtype:
					case DCT.InputStringField.xtype:
					case DCT.InputSSNField.xtype:
					case DCT.InputPhoneField.xtype:
						newconfig = Ext.copyTo({}, hiddenInput.initialConfig, 'allowBlank,dctFormatMask,listeners,maxLength,regex,regexText,vtype,formatter,validator,fieldRef,objectRef');
						editorClass = 'DCT.EditorInputField';
						break;
					case DCT.CheckBoxField.xtype:
						newconfig = Ext.copyTo({}, hiddenInput.initialConfig, 'boxLabel,checked,disabled,inputValue,listeners,fieldRef,objectRef');
						editorClass = 'DCT.EditorCheckBoxField';
						break;
					default:
						newconfig = {};
						editorClass = 'Ext.form.TextField';
						break;
				}
				editorControl = Ext.create(editorClass, newconfig);
			} else {
				editorControl = defaultEditorControl;
			}
		} else {
			editorControl = defaultEditorControl;
		}
		return (editorControl);
	},
	/**
	*
	*/
	reg: function (xtype, htmlClass, cls) {
		var me = this;
		var controlClass = {};
		Ext.apply(controlClass, {
			xtype: xtype,
			htmlclass: htmlClass,
			cls: cls
		});
		me.classArray.push(controlClass);
	},
	/**
	*
	*/
	generateControls: function (rootElement) {
		Ext.each(DCT.Util.classArray, function (control, index) {
			Ext.select("." + control.htmlclass, true, rootElement).each(function (fieldEl) {
				var config, inputField;
				config = fieldEl.getDataPrefixedAttributes();
				inputField = Ext.create(control.cls, config);
				Ext.removeNode(fieldEl.dom);
			});
		});
	},
	/**
	*
	*/
	resyncTabPanels: function () {
		var me = this;
		DCT.Util.tabComponents.each(function (tabGroup) {
			if (!tabGroup.isDestroyed) {
				//	var frameHeight = tabGroup.getFrameHeight();
				//		var activeTab = tabGroup.getActiveTab();
				tabGroup.updateLayout();
				//	var contentEl = Ext.get(activeTab.contentEl);
				//	if (contentEl) {
				//		var childHeight = contentEl.getHeight();
				//		activeTab.setHeight(childHeight + frameHeight);
				//	}
			}
		}, me);
	},
	/**
	*
	*/
	removeTabs: function (removeTab) {
		var me = this;
		if (typeof (removeTab) == 'object')
			removeTab = false;
		var oItem = Ext.query('A, INPUT');
		if (oItem) {
			for (i = 0; i < oItem.length; i++) {
				if (oItem[i].type != 'hidden') {
					if (removeTab) {
						if (oItem[i].getAttribute("tabProcessed") == "1")
							continue;
						if (oItem[i].tabIndex == -1) // If a tab is already disabled, mark it.
							oItem[i].tabIndex = -2;
						else
							oItem[i].tabIndex = -1;
						oItem[i].setAttribute("tabProcessed", "1");
					} else {
						if (oItem[i].tabIndex == -2) // If the tab wasn't previously enabled, don't enable it now.
							oItem[i].tabIndex = -1;
						else
							oItem[i].tabIndex = 0;
						oItem[i].setAttribute("tabProcessed", "0");
					}
				}
			}
		}
		Ext.ComponentMgr.each(function (key, item, itself) {
			if (item.startEditing != null) {
				item.setDisabled(removeTab);
			}
		}, me);
	},
	/**
	*
	*/
	includeHeaderFile: function (filename, filetype, oldfilename) {
		var fileref;
		if (filetype == "js") {
			fileref = document.createElement('script')
			fileref.setAttribute("type", "text/javascript")
			fileref.setAttribute("src", filename)
		} else if (filetype == "css") {
			fileref = document.createElement("link")
			fileref.setAttribute("rel", "stylesheet")
			fileref.setAttribute("type", "text/css")
			fileref.setAttribute("href", filename)
		}

		var replaced = false;
		var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist using
		var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for
		var allsuspects = document.getElementsByTagName(targetelement)

		// replace an old file with this one, if oldfilename was passed in
		if (typeof oldfilename != "undefined") {
			for (var i = allsuspects.length; i >= 0; i--) {
				if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(oldfilename) != -1) {
					allsuspects[i].parentNode.replaceChild(fileref, allsuspects[i]);
					replaced = true;
				}
			}
		}
		// we aren't replacing an old file, so a new one needs to be appended
		if (!replaced) {
			// no need to include this file if we already have it
			var alreadyExists = false;
			for (var i = allsuspects.length; i >= 0; i--) {
				if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1) {
					alreadyExists = true;
					break;
				}
			}
			if (!alreadyExists) {
				document.getElementsByTagName("head")[0].appendChild(fileref);
			}
		}
	},
	getParent: function (getImmediateParent) {
		var parentWindow = self;
		try {
			if (getImmediateParent) {
				parentWindow = (parentWindow.name.search('dctPopup_') > -1) ? parentWindow.parent : parentWindow;
			}
			else {
				while (parentWindow.parent.name === parentWindow.name && parentWindow.parent != parentWindow || (parentWindow.name.search('dctPopup_') > -1)) {
					parentWindow = parentWindow.parent;
				}
			}
		} catch (e) {
			parentWindow = window;
		}
		return parentWindow;
	},
	getPostUrl: function () {
		return DCT.postPage + "?_windowGuid=" + DCT.Util.getParent().DCT.Util.getWindowGuid();
	},
	getWindowGuid: function () {
		var windowGuid = DCT.Util.getParent().DCT.Util.getSafeElement("_windowGuid");
		return windowGuid.value;
	},
	setWindowGuid: function (newValue) {
		var windowGuid = DCT.Util.getParent().DCT.Util.getSafeElement("_windowGuid");
		windowGuid.value = newValue;
	},
	cloneElementWithComputedStyle: function (element, deep) {
		var me = this;
		var clone = element.cloneNode(false);
		if (element.style && window.getComputedStyle) {
			var cssText = window.getComputedStyle(element, null).cssText;
			if (cssText === "") {
				var completeStyle = window.getComputedStyle(element, null);
				for (var k in completeStyle) { if (typeof (completeStyle[k]) === "string") cssText += k + ":" + completeStyle[k] + ";"; }
			}
			clone.style.cssText = cssText;
			if (deep) {
				Ext.each(element.childNodes, function (child) {
					var clonedChild = DCT.Util.cloneElementWithComputedStyle(child, true);
					if (clonedChild) clone.appendChild(clonedChild);
				}, me)
			}
		}
		return clone;
	},
	showLoadingMessageForPopup: function (win) {
		DCT.LoadingMessage.show();
		var element = DCT.Util.getSafeElement('loadingMessage');
		var clonedElement = DCT.Util.cloneElementWithComputedStyle(element, true);
		var iFrame = win.body.query('iframe')[0];
		if (iFrame.contentWindow.document.body == null) {  // null in IE
			iFrame.contentWindow.document.write("<body></body>");
		}
		iFrame.contentWindow.document.body.appendChild(clonedElement);
		var viewSize = { height: iFrame.clientHeight, width: iFrame.clientWidth };
		DCT.Util.centerElement(clonedElement, win.body, viewSize);
		DCT.LoadingMessage.hide();
	},
	centerElement: function (element, body, viewSize) {
		var messageWidth = element.style.width;
		var messageHeight = element.style.height;

		messageWidth = messageWidth.substring(0, messageWidth.indexOf('px'));
		messageHeight = messageHeight.substring(0, messageHeight.indexOf('px'));
		var x = Math.round((viewSize.width / 2) - (messageWidth / 2));
		var h = body.getHeight(true);
		if (viewSize.height > 0) {
			h = viewSize.height;
		}
		var y = Math.round(h / 2 - (messageHeight / 2));


		element.style.left = x + 'px';
		element.style.top = y + 'px';

	}
});
