/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
	/**
	*
	*/
	enableDisableActionButton: function (fieldIsBlank, actionButton) {
	 	var actionAnchor = Ext.get(actionButton);
	 	if (actionAnchor){
		  if (fieldIsBlank)
		    actionAnchor.replaceCls('btnEnabled', 'btnDisabled');
		  else
		    actionAnchor.replaceCls('btnDisabled', 'btnEnabled');
	  }
	},
	/**
	*
	*/
	isButtonDisabled: function (linkId) {
	  liNode = Ext.get(linkId);
	  if (liNode){
	    if (liNode.hasCls('btnDisabled'))
	      return true;
	  }
	  return false;
	}
});