
/****/
Ext.override(String, {
	/**	*	*/
	replaceAll: function (pattern, replacement) {
		if (pattern === replacement) return this;

		var result = this;

		while (result.indexOf(pattern) !== -1)
		{
			result = result.replace(pattern, replacement);
		}

		return result;
	},

	/**	*	*/
	replaceAnchors: function (anchors) {
		var result = this;

		Ext.iterate(anchors, function (k, v) {
			var anchor = '#{' + k + '}';
			result = result.replaceAll(anchor, v);
		});

		return result;
	}
});


/*** @class DCT.Localization*/
Ext.apply(DCT.Localization, {
	/**	*	*/
	defaultString: function (stringName) {
		return function () {
			var string = DCT.Strings[stringName];
			return Ext.isDefined(string) ? string : stringName;
		};
	},
	/**	*	*/
	translate: function (stringName, anchors) {
		var result = DCT.Localization.defaultString(stringName)().replaceAnchors(anchors);
		return Ext.String.trim(result);
	}
});

//Shortcut for translation
DCT.T = DCT.Localization.translate;
