/**
 * @class DCT.StringFormatting
 * This class defines common formatting for string that can be used for controls or strings.
 */
Ext.define('DCT.StringFormatting', {

	stringFormatMask: '',
	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setFormatMask(config);
		me.callParent([config]);
	},

	/**
	*
	*/

	setFormatMask: function (config) {
		var me = this;
		if (!Ext.isEmpty(config.stringFormatMask)) {
			me.stringFormatMask = config.stringFormatMask;
		}
	},
	/**
	*
	*/

	formatReferenceNumber: function (fieldValue) {
		var cardNumberLenght = fieldValue.length;
		var startNumber, endNumber;

		if (cardNumberLenght == 0)
		{
			return String.Empty;
		}
		else if (cardNumberLenght < 2)
		{
			return cardNumber;
		}
		else
		{
			if (cardNumberLenght > 4)
			{
				fieldValue = fieldValue.replace(/.(?=.{4,}$)/g, "*");
			}
			else
			{
				fieldValue = fieldValue.replace(/.(?=.{1,}$)/g, "*");
			}
			return fieldValue;
		}
	},
	/**
	*
	*/
	formatString: function (fieldValue, fieldRef)
	{
		var me = this;
		switch (me.stringFormatMask) {
			case "referenceNumber":
				if (!Ext.isEmpty(fieldValue))
				{
					fieldValue = me.formatReferenceNumber(fieldValue);
				}
				break;
			default:

		}

		return fieldValue;
	}

});


/**
 * @class DCT.Formatting
 * This class defines common formatting for numbers that can be used for controls or strings.
 */
Ext.define('DCT.Formatting', {

	centsSymbol: '.',
	currencySymbol: '$',
	thousandsSymbol: ',',
	negativeSign: '-',
	floatMask: '-$??????????#.??',
	integerMask: '-?????????#',
	numberMask: '',
	internalFormat: '',
	numberType: '',

	/**
	*
	*/
	constructor: function (config) {
		var me = this;
		me.setFormatMask(config);
		me.callParent([config]);
	},
	/**
	*
	*/
	setFormatMask: function (config) {
		var me = this;
		if (!Ext.isEmpty(config.numberMask)) {
			var splitMask = config.numberMask.split("!");
			if (splitMask.length == 2) {
				config.numberMask = splitMask[0];
				symbols = splitMask[1];
			} else {
				config.numberMask = "";
				symbols = splitMask[0];
			}
			me.centsSymbol = symbols.charAt(0);
			me.thousandsSymbol = symbols.charAt(1);
			me.negativeSign = symbols.charAt(2);
			me.currencySymbol = symbols.charAt(3);
		}
		me.numberMask = (Ext.isEmpty(config.numberMask)) ? ((config.numberType == 'float') ? me.floatMask : me.integerMask) : config.numberMask;
		me.setSymbolsBasedonMask();
		me.setInternalNumberMask();
	},
	/**
	*
	*/
	setInternalNumberMask: function () {
		var me = this;
		if (!Ext.isEmpty(me.thousandsSymbol)) {
			var noCommaExp = new RegExp("\\" + me.thousandsSymbol, "g");
		}
		var noSpaceExp = new RegExp(" ", "g");
		var noPoundExp = new RegExp("#", "g");
		var noZerosExp = new RegExp("0", "g");
		var noCurrencyExp = new RegExp("\\$", "g");
		// Take out commas
		me.internalFormat = me.numberMask.replace(noCommaExp, "");
		// Take out spaces
		me.internalFormat = me.internalFormat.replace(noSpaceExp, "");
		// Replace # with ? (comment out the following line to force the user to enter
		//                   all digits with # in the format mask)
		//config.internalFormat = config.internalFormat.replace(noPoundExp,"?");
		// Replace 0 with ? (comment out the following line to force the user to enter
		//                   all digits with 0 in the format mask)
		//config.numberMask = config.numberMask.replace(noZerosExp,"?");
		me.internalFormat = me.internalFormat.replace(noCurrencyExp, "");
		if (!Ext.isEmpty(me.centsSymbol)) {
			me.internalFormat = me.internalFormat.replace(me.centsSymbol, ".");
		}
	},
	/**
	*
	*/
	setSymbolsBasedonMask: function () {
		var me = this;
		var cents = (me.centsSymbol == '.') ? /\./ : me.centsSymbol;
		var thousands = (me.thousandsSymbol == '.') ? /\./ : me.thousandsSymbol;
		var currency = /\$/;
		me.centsSymbol = (Ext.isEmpty(me.numberMask.match(cents))) ? "" : me.centsSymbol;
		me.thousandsSymbol = (Ext.isEmpty(me.numberMask.match(thousands))) ? "" : me.thousandsSymbol;
		me.negativeSign = (Ext.isEmpty(me.numberMask.match(me.negativeSign))) ? "" : me.negativeSign;
		me.currencySymbol = (Ext.isEmpty(me.numberMask.match(currency))) ? "" : me.currencySymbol;
	},
	/**
	*
	*/
	formatNumberToUS: function (fieldValue) {
		var me = this;
		if (!Ext.isEmpty(fieldValue)) {
			fieldValue = fieldValue.toString();
			var matchArr = fieldValue.match(/\d/);
			if (matchArr != null) {
				if (!Ext.isEmpty(me.centsSymbol)) {
					fieldValue = fieldValue.replace(me.centsSymbol, "D");
				}
				if (!Ext.isEmpty(me.thousandsSymbol)) {
					var thousandsRegEx = new RegExp("\\" + me.thousandsSymbol, 'g');
					fieldValue = fieldValue.replace(thousandsRegEx, "");
				}
				fieldValue = fieldValue.replace("D", ".");
				if (!Ext.isEmpty(me.negativeSign)) {
					fieldValue = fieldValue.replace(me.negativeSign, "-");
				}
				fieldValue = fieldValue.replace(me.currencySymbol, "");
				while ('' + fieldValue.charAt(0) == ' ')
					fieldValue = fieldValue.substring(1, number.length);
				while ('' + fieldValue.charAt(fieldValue.length - 1) == ' ')
					fieldValue = fieldValue.substring(0, fieldValue.length - 1);
			}
		}
		return fieldValue;
	},
	/**
	*
	*/
	formatNumberToCulture: function (fieldValue) {
		var me = this;
		if (!Ext.isEmpty(fieldValue)) {
			fieldValue = fieldValue.toString();
			var matchArr = fieldValue.match(/\d/);
			if (matchArr) {
				fieldValue = fieldValue.replace(".", "D");
				fieldValue = fieldValue.replace(/,/g, me.thousandsSymbol);
				fieldValue = fieldValue.replace("D", me.centsSymbol);
				fieldValue = fieldValue.replace("-", me.negativeSign);
				fieldValue = fieldValue.replace("$", me.currencySymbol);
			}
		}
		return fieldValue;
	},
	/**
	*
	*/
	isMultiCurrency: function (format, currencyCode) {
		var isMulti = false;
		var regex = new RegExp('^[C]{1}[0-9]{0,1}$');

		if (!Ext.isEmpty(currencyCode) && regex.test(format)) {

			isMulti = true;
		}

		return isMulti;
	},
	/**
	*
	*/
	currencyFormat: function (fieldValue, fieldRef, format, currencyCode) {

		var stateId = Ext.getDom('_stateID').value;

		Ext.Ajax.request({
			url: DCT.Util.getPostUrl(),
			method: "POST",
			success: function (response, opts) {
				var data = JSON.parse(response.responseText);
				if (!Ext.isEmpty(data) && !Ext.isEmpty(data.value)) {
					fieldRef.setRawValue(data.value);
				}
			},
			scope: DCT.LoadingMessage,
			params: {
				_stateID: stateId,
				_submitAction: 'currencyFormatter',
				_inputValue: fieldValue,
				_currencyCode: currencyCode,
				_format: format
			}
		});
	},
	/**
	*
	*/
	formatFloat: function (fieldValue, fieldRef) {
		var me = this;
		if (!Ext.isEmpty(fieldValue)) {
			var formatObject = (me.formatting) ? me.formatting : me;
			if (formatObject.isMultiCurrency(me.dctNumberFormat, me.dctCurrencyCode)) {
				fieldValue = formatObject.currencyFormat(fieldValue, fieldRef, me.dctNumberFormat, me.dctCurrencyCode);
			} else {

				var numDecMask = 0;
				var numOptionalDec = 0;
				fieldValue = formatObject.formatNumberToUS(fieldValue);
				if (!Ext.isEmpty(formatObject.centsSymbol)) {
					if (formatObject.numberMask.indexOf(formatObject.centsSymbol) > -1) {
						var decimal = formatObject.numberMask.substr(formatObject.numberMask.indexOf(formatObject.centsSymbol) + 1);
						var optionalChars = decimal.match(/\?/g);
						numOptionalDec = (Ext.isEmpty(optionalChars)) ? 0 : optionalChars.length;
						numDecMask = decimal.length;
					}
				}
				var decimalFormat = "";
				var format = '0,0';
				if (numDecMask > 0) {
					decimalFormat = Ext.String.leftPad(decimalFormat, numDecMask, '0');
					format = format + '.' + decimalFormat;
				}
				fieldValue = Ext.util.Format.number(fieldValue, format);
				fieldValue = formatObject.formatNumberToCulture(fieldValue);
				fieldValue = (formatObject.numberMask.indexOf(formatObject.centsSymbol) == 0) ? fieldValue.substr(fieldValue.indexOf(formatObject.centsSymbol)) : fieldValue;
				if (numOptionalDec > 0) {
					while (fieldValue.charAt(fieldValue.length - 1) == '0' && numOptionalDec > 0) {
						fieldValue = fieldValue.substring(0, fieldValue.length - 1);
						numOptionalDec--;
					}
					if (fieldValue.charAt(fieldValue.length - 1) == formatObject.centsSymbol) {
						fieldValue = fieldValue.substring(0, fieldValue.length - 1);
					}
				}
				if (!Ext.isEmpty(formatObject.currencySymbol)) {
					var currencyIndex = formatObject.numberMask.indexOf(formatObject.currencySymbol);
					if (parseFloat(fieldValue) < 0) {
						fieldValue = (currencyIndex < 2) ? fieldValue[0] + formatObject.currencySymbol + fieldValue.substr(1) : fieldValue + formatObject.currencySymbol;
					} else {
						fieldValue = (currencyIndex < 2) ? formatObject.currencySymbol + fieldValue : fieldValue + formatObject.currencySymbol;
					}
				}
				return fieldValue;
			}
		}
	},
	/**
	*
	*/
	formatInt: function (fieldValue) {
		var me = this;
		if (!Ext.isEmpty(fieldValue)) {
			var formatObject = (me.formatting) ? me.formatting : me;
			fieldValue = formatObject.formatNumberToUS(fieldValue);
			fieldValue = Ext.util.Format.number(fieldValue, '0,000');
			if (!Ext.isEmpty(formatObject.currencySymbol)) {
				var currencyIndex = formatObject.numberMask.indexOf(formatObject.currencySymbol);
				if (parseFloat(fieldValue) < 0) {
					fieldValue = (currencyIndex < 2) ? fieldValue[0] + formatObject.currencySymbol + fieldValue.substr(1) : fieldValue + formatObject.currencySymbol;
				} else {
					fieldValue = (currencyIndex < 2) ? formatObject.currencySymbol + fieldValue : fieldValue + formatObject.currencySymbol;
				}
			}
			return (formatObject.formatNumberToCulture(fieldValue));
		}
	}
});

/**
* @class Date
* Additional Date functions related to formatting.
*/
Ext.apply(Date, {
	/**
	*
	*/
	formatTimestamp: function (input, displayFormat) {
		return Ext.Date.format(Ext.Date.parse(input, "Y-m-d\\TH:i:s"), displayFormat);
	},
	/**
	*
	*/
	formatTimestampWithMilliseconds: function (input, displayFormat) {
		return Ext.Date.format(Ext.Date.parse(input, "Y-m-d\\TH:i:s.u"), displayFormat);
	}
});