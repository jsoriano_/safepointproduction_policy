
/** 
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	dctSelectorString: "INPUT[type!=hidden][disabled!=false],SELECT[type!=hidden][disabled!=false],TEXTAREA[type!=hidden][disabled!=false]",
	dctExcludedClass: ['autoFocusOff', 'hideOffset'],

	/** 
	*
	*/
	findAndSetFocus: function (focusDelay){
		var me = this;
		var focusItemsList = Ext.dom.Query.select(me.dctSelectorString, document.forms[0]);
		
		if (focusItemsList.length > 0)
		{
			var focusItem;
			var i = 0;
			while (Ext.isEmpty(focusItem) && i < focusItemsList.length - 1) {
				var isOK = true;
				for (var j = 0; j < me.dctExcludedClass.length; j++) {
					if (!Ext.isEmpty(Ext.fly(focusItemsList[i].id).findParent('div[class*=' + me.dctExcludedClass[j] + ']')) || Ext.fly(focusItemsList[i].id).hasCls(me.dctExcludedClass[j])) {
						isOK = false;
					}
				}
				if (isOK) {
					focusItem = focusItemsList[i];
				}
				i++;
			}
			if (!Ext.isEmpty(focusItem)) {
				if (focusDelay != undefined) {
					me.placeFocus(focusItem, focusDelay);
				} else {
					me.placeFocus(focusItem, 5);
				}
			}
		}
	},
	
	placeFocus: function(findFirstOne, focusDelay)
	{
		var fieldId = (Ext.isEmpty(findFirstOne.getAttribute('data-componentid'))) ? findFirstOne.id : findFirstOne.getAttribute('data-componentid');
		var fieldCmp = Ext.getCmp(fieldId);
		if (fieldCmp){
			var xType = fieldCmp.getXType();
			if (xType == 'dctcheckboxfield' || xType == 'dctradiofield'){
				var parentGroup = Ext.getCmp(fieldCmp.ownerCt.id);
				var checkedArray = parentGroup.getChecked();
				if (!Ext.isEmpty(checkedArray)){
					fieldCmp = Ext.getCmp(checkedArray[0].id)
				}
			}
			fieldCmp.focus(true, focusDelay);
		} else {
			Ext.get(findFirstOne).focus(focusDelay);
		}
	},
	/** 
	*
	*/
	overrideFocus: function (fielditem) {
		var field = Ext.getCmp(fielditem);
		if (field)
			field.focus(true,10);
	},
	/** 
	*
	*/
	activateField: function (fieldId) {
		var me = this;
		var control = Ext.getCmp(fieldId);
		if(control && control.hasFocus) return;
		if (control) {
			var domElement = Ext.get(fieldId);
			var elementParent = (Ext.isEmpty(domElement)) ? null : domElement.parent('div[class=gridHiddenInputs]');
			if (Ext.isEmpty(elementParent)) {	// not part of grid, just set focus
				try {
					if (control.disabled){
					  me.findAndSetFocus(5);
					}
					else {
						control.focus(true,5);
					}
				}
				catch (exc) {
				  // Just silently handle the error...
				}
			} else {	// hidden field for grid, find and select the correct cell
				var grid = Ext.getCmp(elementParent.id.substring(0, elementParent.id.indexOf('_gridHiddenInputs')));
				var row = -1;
				var column = -1;
				if (Ext.isDefined(grid) && (grid.store.getCount() > 0)){
					var xpathValue = "fieldInstance[@uid='" + fieldId + "']";
					var fieldInstance = Ext.DomQuery.selectNode(xpathValue, grid.store.proxy.reader.rawData);
					var fieldRef = Ext.DomQuery.selectValue('@fieldRef', fieldInstance);
					xpathValue = "fieldInstance[@fieldRef='" + fieldRef + "']";
					var fieldItems = Ext.DomQuery.select(xpathValue, grid.store.proxy.reader.rawData);
					var item = Ext.Array.findBy(fieldItems, function(item, index){
							if (Ext.DomQuery.selectValue('@uid', item) == fieldId){
								row = index;
								return true;
							}
					});
					var columns = grid.columns;
					for (var c = 0; c < columns.length; c++) {
						if (fieldRef == columns[c].dataIndex) {
							column = c;
							break;
						}
					}
					if (row > -1) {
						if (grid.view.getRow(row)){
							var cellEditing = grid.getPlugin('dctcellediting');
							if (cellEditing)
								cellEditing.startEditByPosition({
									row: row,
									column: column
								});
						}
					}
				}
			}
	  } else {
			try
			{
				if (Ext.getDom(fieldId)){
					Ext.get(fieldId).focus(5);
				} 
				else { 	
					me.findAndSetFocus(5);
				}
			}
			catch(e) {
				// Just silently handle the error...
			}
		}
	}
});