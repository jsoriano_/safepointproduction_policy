/**
* Event Hub Class Definition 
*/
Ext.define('DCT.EventHub', {
  extend: 'Ext.util.Observable',

	dctDefaultEvents: {
		'taskpickedup': true,
		'taskcompleted': true,
		'taskadded': true,
		'refreshnotes': true,
		'refreshnotestoolbar': true,
		'removetoolbar': true,
		'ajaxcomplete': true,
		'attachmentdeleted': true
	},
	constructor: function(config){
		var me = this;
		if (Ext.isDefined(config.dctEvents)){
			Ext.apply(me.dctDefaultEvents,config.dctEvents);
		}
		me.listeners = config.listeners;
		me.callParent(arguments);
	},
	fireEvent: function (eventName) {
		var me = this;
		Ext.defer(function() {
			var me = this;
			me.superclass.fireEvent.call(me, eventName);
		}, 1, me);
	}
});
Ext.apply(DCT, {
	hubEvents: Ext.create('DCT.EventHub', {})
});