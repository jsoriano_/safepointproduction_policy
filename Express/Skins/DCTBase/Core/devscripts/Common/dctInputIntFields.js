
/****/
Ext.define('DCT.InputIntField', {
  extend: 'DCT.InputNumberField',
  
	inputXType: 'dctinputintfield',
	xtype: 'dctinputintfield',

  /**  *  */
  setValidator: function(config){
  	var me = this;
 		config.validator = me.checkInt;
  },
  /**  *  */
  setFormatter: function(config){
  	config.formatter = config.formatting.formatInt;
  },
  /**  *  */
  setCultureSymbols: function(config){
  	var me = this;
  	config.getCultureSymbols = me.getIntSymbols;
  },
  /**  *  */
  setStripSymbols: function(config){
  	var me = this;
  	config.getStripSymbols = me.getIntStripSymbols;
  }
});
DCT.Util.reg('dctinputintfield', 'interviewInputIntField', 'DCT.InputIntField');
