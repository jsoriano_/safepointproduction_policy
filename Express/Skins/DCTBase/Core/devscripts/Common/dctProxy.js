/**
* @class DCT.Util
*/
Ext.apply(DCT.Util, {
    /**
    *
    */
    getProxyUrl: function () {
        var url = location.href;

        if (url.toLowerCase().search(DCT.postPage.toLowerCase()) == -1) 
        {
            url = url + DCT.postPage;
        }
        if (url.indexOf('#') == url.length - 1) 
        {
            url = url.replace("#", "");
        }
        return url;
    }
});

