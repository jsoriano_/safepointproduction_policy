
/****/
Ext.define('DCT.InputCreditCardField', {
  extend: 'DCT.InputField',

	inputXType: 'dctinputcreditcardfield',
	xtype: 'dctinputcreditcardfield',

  /**  *  */
  setValidator: function(config){
  	var me = this;
 		config.validator = me.checkCreditCard;
  },
  /**  *  */
  setMaskRe: function(config){
  	var regChars = "[0-9-]";
  	config.maskRe = new RegExp(regChars,'i');
  }
});
DCT.Util.reg('dctinputcreditcardfield', 'inputCreditCardField', 'DCT.InputCreditCardField');
