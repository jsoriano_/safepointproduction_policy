
/**
* Zip Code - Canada
*/
Ext.define('DCT.InputZipCodeFieldCN', {
  extend: 'DCT.InputField',
  

	inputXType: 'dctinputzipcodefieldcn',
	xtype: 'dctinputzipcodefieldcn',

	/**
	*
	*/
	setVType: function(config){
		config.vtype = 'zipcodecn';
	}	
});
DCT.Util.reg('dctinputzipcodefieldcn', 'interviewInputZipCodeFieldCN', 'DCT.InputZipCodeFieldCN');

/**

Zipcode - Germany
*
*/
Ext.define('DCT.InputZipCodeFieldDE', {
  extend: 'DCT.InputField',
  
    inputXType: 'dctinputzipcodefieldde',
    xtype: 'dctinputzipcodefieldde',

    /**
    *
    */
    setVType: function (config) {
        config.vtype = 'zipcodede';
    }
});
DCT.Util.reg('dctinputzipcodefieldde', 'interviewInputZipCodeFieldDE', 'DCT.InputZipCodeFieldDE');

/**

Zipcode - United Kingdom
*
*/
Ext.define('DCT.InputZipCodeFieldUK', {
  extend: 'DCT.InputField',
  
    inputXType: 'dctinputzipcodefielduk',
    xtype: 'dctinputzipcodefielduk',
    dctFormatBeforePost: true,
    /**
  *
  */
    setFormatter: function (config) {
        config.formatter = this.formatZipUk;
    },

    /**
    *
    */
    setVType: function (config) {
        config.vtype = 'zipcodeuk';
    }
});
DCT.Util.reg('dctinputzipcodefielduk', 'interviewInputZipCodeFieldUK', 'DCT.InputZipCodeFieldUK');
