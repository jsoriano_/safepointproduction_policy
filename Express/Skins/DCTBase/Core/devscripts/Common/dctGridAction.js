/*** @class DCT.Submit*/
Ext.apply(DCT.Submit, {
	setGridAction: function(page, processAction){
	  DCT.Util.getSafeElement('_gridAction').value = processAction;
	  DCT.Util.getSafeElement('_gridPage').value = page;
	}
});