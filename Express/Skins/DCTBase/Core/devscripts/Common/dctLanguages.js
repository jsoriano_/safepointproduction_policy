/**
* @class DCT.Submit
*/
Ext.apply(DCT.Submit, {
    selectLanguage: function (culture) {
    	DCT.Util.getSafeElement('_language').value = culture;
    	DCT.Submit.submitPageAction(DCT.currentPage, 'setLanguage');
    }
});
