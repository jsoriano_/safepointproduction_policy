/**
* @class DCT.LoadingMessage
*/
Ext.apply(DCT.LoadingMessage, {
	polling: false,
	waitTime: 4000,
	pollingInterval: 2000,
	loadingMsgText: '',
	timeoutHandle: null,

	/**
	*
	*/
	show: function () {
		// Bail out if there is no loading window or it is already visible
		var loadingWindow = Ext.get('loadingWindow');
		if (loadingWindow == null || loadingWindow.isVisible(false))
			return;

		loadingWindow.setDisplayed(true);
		var message = DCT.Util.getSafeElement('loadingMessage'); 
		var body = Ext.getBody();
		var viewSize = body.getViewSize();
		
		DCT.Util.centerElement(message, body, viewSize);
		
		Ext.get(message).setStyle({
			display: 'block',
			position: 'fixed'
		});
		
		var mask = Ext.get('loadingMask');
		mask.setDisplayed(true);
		mask.animate({ 
				from: {
					opacity: 0.0
				}, 
				to: {
					opacity: 0.25
				},
				duration: 0.2 
			});

		body.dom.style.cursor = 'wait';
	},
	/**
	*
	*/
	delayedShow: function (waitTime) {
		var me = this;
		// If we have an active timer, just let that one keep running instead of starting a second.
		if (me.timeoutHandle == null) {
			if (Ext.isEmpty(waitTime)) {
				waitTime = DCT.LoadingMessage.getWaitTime();
			}
			me.timeoutHandle = Ext.defer(DCT.LoadingMessage.show, waitTime);
		}
		return me.timeoutHandle;
	},
	/**
	*
	*/
	cancel: function ()  {
		var me = this;
		if (me.timeoutHandle != null) {
			clearTimeout(me.timeoutHandle);
			me.timeoutHandle = null;
		}
	},
	/**
	*
	*/
	hideAndCancel: function() {
		DCT.LoadingMessage.cancel();
		DCT.LoadingMessage.hide();
	},
	/**
	*
	*/
	hide: function () {
		if (document.body)
			document.body.style.cursor = 'auto';
		// Bail out if there is no loading window or it is already hidden
		var loadingWindow = Ext.get('loadingWindow');
		if (loadingWindow == null || !loadingWindow.isVisible(false))
			return;

		var animationComplete = function(){
			Ext.fly('loadingMessage').setDisplayed(false);
			Ext.fly('loadingMask').setDisplayed(false);
			Ext.fly('loadingWindow').setDisplayed(false);
		};
		Ext.get('loadingMask').animate({ 
			to: {
					opacity: 0.0
				}, 
			duration: 0.2,
			callback: animationComplete
		});
	},
	/**
	*
	*/
	hold: function () {
		var me = this;
		me.loadingMsgText = Ext.getDom('loadingText').innerHTML;
	},
	/**
	*
	*/
	reset: function () {
		var me = this;
		if (!Ext.isEmpty(me.loadingMsgText))
			Ext.getDom('loadingText').innerHTML = me.loadingMsgText;
		me.loadingMsgText = '';
	},
	/**
	*
	*/
	set: function (msg) {
		Ext.getDom('loadingText').innerHTML = msg;
	},
	/**
	*
	*/
	get: function () {
		return (Ext.getDom('loadingText').innerHTML);
	},
	/**
	*
	*/
	getWaitTime: function () {
		var me = this;
		return me.waitTime;
	},
	/**
	*
	*/
	getPollingInterval: function () {
		var me = this;
		return me.pollingInterval;
	},
	/**
	*
	*/
	drawDots: function () {
		var me = this;
		DCT.LoadingMessage.polling = true;
		DCT.LoadingMessage.show();
		Ext.Ajax.request({
			url: DCT.checkStatusUrl,
			success: me.formsSuccess,
			failure: me.formsFailure,
			scope: me
		});
	},
	/**
	*
	*/
	formsSuccess: function (response, objects) {
		var me = this;
		var xDoc = DCT.Util.loadXMLString(response.responseText);
		var status = Ext.DomQuery.selectValue("/semaphoreStatus", xDoc);
		var page = Ext.DomQuery.selectValue("/semaphoreStatus/@page", xDoc); //printJob|preview
		var mode = Ext.DomQuery.selectValue("/semaphoreStatus/@previewMode", xDoc); //0|1

		//if response has no semaphore status then check if the response is in HTML format, by checking the meta tag for content type.
		// if found then change the status to HTMLResponse.
		if (!Ext.isDefined(status)) {
			var pattern = /<meta.*?http-equiv="Content-Type".*?content="(.*?)".*?>|<meta.*?content="(.*?)".*?http-equiv="Content-Type".*?>/i;
			var arr = pattern.exec(response.responseText);

			if (arr != null && arr.length > 0 && arr.length >= 3) {
				if (arr[1].match("text/html") || arr[2].match("text/html")) {
					status = "HTMLResponse";
				}
			}
		}

				 switch (status) {
					case 'success':
						if (page == 'preview' && mode == '0') {
							window.open(DCT.Util.getPostUrl(), "Preview");
						} else {
							DCT.Submit.submitForm();
							DCT.Util.toggleButtons(false);
						}
						me.polling = false;
						me.hide();
						break;
					case 'failure':
						window.open(DCT.Util.getPostUrl(), "EndSession");
						me.polling = false;
						me.hide();
						break;
				 	case 'invalid':
				 		DCT.Submit.submitForm();
						me.polling = false;
						me.hide();
						break;
					case 'working':
						var sleepTime = me.getPollingInterval();
						Ext.defer(me.drawDots, sleepTime, me);
						break;
					case 'HTMLResponse':
						//The response is in HTML format.
						if (response.responseText != "") {
							var hWindow = window.open("", status, "dependent=yes,hotkeys=no,location=no,width=800,height=600,status=no,resizable=yes,toolbar=no,scrollbars=no");
							hWindow.document.write(response.responseText);
						}
						me.polling = false;
						me.hide();
						break;
					default:
				me.drawDots();
				break;
		}
	},
	/**
	*
	*/
	formsFailure: function (response, objects) {
		var me = this;
		document.location.replace(DCT.Util.getPostUrl());
		me.polling = false;
		me.hide();
	},
	/**
	*
	*/
	resumeSession: function () {
		var me = this;
		// This method may be called from the window.onfocus event.
		// Thus, it might be called one or more times before the ajax request is done,
		// but it shouldn't do anything unless the ajax request is done.
		if (!me.polling){
			// Remove the onfocus event so it won't fire again while the interview page renders
			window.onfocus = null;
			DCT.Submit.submitPage("interview");
			me.hide();
		}
	}
});
