/**
* @class DCT.Util
*/
Ext.apply(DCT.Util,{
	/**
	*
	*/
	showBubblePath: function (elementID, fieldText) {
		var value = 'No field text.';
	
		if (fieldText != undefined)
			value = fieldText;
		Ext.create('Ext.ToolTip', {
			target: elementID,
			html: value,
			width: 288,
			title: 'Field Info',
			trackMouse: true});
	},
	/**
	*
	*/
	getSafeElement: function (id) {
		var me = this;
		var element = Ext.getDom(id);
		if (element == null) element = me.appendHiddenElement(id);
		return element;
	},
	/**
	*
	*/
	appendHiddenElement: function (name, value) {
		var elementSpec;
		if (value != undefined)
			elementSpec = {tag: 'input', type: 'hidden', name: name, id: name, value: value};
		else
			elementSpec = {tag: 'input', type: 'hidden', name: name, id: name };
		var hiddenElement = Ext.DomHelper.append(document.forms[0], elementSpec);
		return hiddenElement;
	},
	/**
	*
	*/
	getFormNameValuePairs: function (theForm, asString) {
		DCT.Submit.removeEmptyText();
		var strNameValuePair = Ext.dom.Element.serializeForm(theForm);
		if (!asString)
			strNameValuePair = Ext.Object.fromQueryString(strNameValuePair);
		DCT.Submit.resetEmptyText();
		return strNameValuePair;
	},
	/**
	*
	*/
	buildBaseParams: function () {
		var me = this;
		if (Ext.getDom('__VIEWSTATE') != null)
			return;
		var strNameValuePair = me.getFormNameValuePairs(document.forms[0], false);
		return strNameValuePair;
	}
});