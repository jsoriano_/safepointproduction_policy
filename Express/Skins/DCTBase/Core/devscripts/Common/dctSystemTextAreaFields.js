
/**
*
*/
Ext.define('DCT.SystemTextAreaField', {
  extend: 'DCT.TextAreaField',
  
 	inputXType: 'dctsystemtextarea',
 	xtype: 'dctsystemtextarea',

 	/**
 	*
 	*/
 	setDefaultListeners: function (config) {
	    config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	}	
});
DCT.Util.reg('dctsystemtextarea', 'systemTextAreaField', 'DCT.SystemTextAreaField');

//==================================================
// Message Body Description text area control 
//==================================================

/**
*
*/
Ext.define('DCT.MessageBodyTextAreaField', {
  extend: 'DCT.SystemTextAreaField',
  
 	inputXType: 'dctmessagebodytextareafield',
 	xtype: 'dctmessagebodytextareafield',

 	/**
 	*
 	*/
 	setListeners: function (config) {
 		var me = this;
		Ext.apply(config.listeners,{
			keyup: {
				fn: function(textarea, e) {
					DCT.Util.enableDisableActionButton(DCT.Util.checkIfMessageFieldsBlank(), 'newMessageAnchor');
				},
				scope: me
			}
		});
	}
});
DCT.Util.reg('dctmessagebodytextareafield', 'messagebodyTextAreaField', 'DCT.MessageBodyTextAreaField');
