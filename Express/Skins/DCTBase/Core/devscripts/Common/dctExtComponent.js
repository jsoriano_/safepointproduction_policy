
/** **/
Ext.define('DCT.Component', {
    override: 'Ext.Component',

	/** 	*	*/
	bindConfiguredEvents: function (config) {
		/// <summary>Bind all events specified in data-prefixed attributes on the current element</summary>

		var element = this;
	
		Ext.iterate(config, function (k, v) {
			if (Ext.isEmpty(k) || Ext.isEmpty(v)) return;
			if (k.indexOf("event-") !== 0) return;

			var eventName = k.substr(6, k.length - 6);
			var customEvent = DCT.CustomEvents[eventName];

			var fn = function() {
				if (Ext.isFunction(element[v]))
					return element[v].apply(element, arguments);
				else if (Ext.isFunction(window[v]))
					return window[v].apply(element, arguments);
				else
					return eval(v);
			};

			if (Ext.isDefined(customEvent))
				customEvent(element, fn);
			else
				element.on(eventName, fn);
		});
	}

});
