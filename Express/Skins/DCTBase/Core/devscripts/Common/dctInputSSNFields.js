/**
* 
*/
Ext.define('DCT.InputSSNField', {
  extend: 'DCT.InputField',
  
	inputXType: 'dctinputssnfield',
	dctFormatBeforePost: true,
	xtype: 'dctinputssnfield',

	/**
	* 
	*/
	setFormatter: function(config){
		var me = this;
		config.formatter = me.formatSSN;
	},

	/**
	* 
	*/
	setVType: function (config) {
		config.vtype = 'ssn';
	}		  
});
DCT.Util.reg('dctinputssnfield', 'interviewInputSSNField', 'DCT.InputSSNField');
