/**
*
*/
Ext.define('DCT.SystemCheckBoxField', {
  extend: 'DCT.CheckBoxField',
  
	inputXType: 'dctsystemcheckboxfield',
	xtype: 'dctsystemcheckboxfield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
		var me = this;
		config.listeners = {
			boxready: {
				fn: me.setUpChangeEvent,
				scope: me
			}
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
  /**
  *
  */
  setDisable: function (config) {
    config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
  }
});
/**
*
*/
Ext.define('DCT.LoginRememberCheckBox', {
  extend: 'DCT.SystemCheckBoxField',
  

	inputXType: 'dctloginremembercheckbox',
	xtype: 'dctloginremembercheckbox',
	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		config.listeners = {
			change:{
				fn: function(control) {
					var me = this;
					if (me.checked) {
						var usernameInput = Ext.getCmp('username').getValue();
						var cookie = Ext.util.Cookies.get('_username');
						if(!Ext.isEmpty(usernameInput) && Ext.isEmpty(cookie)) {
							var dt = Ext.Date.add(new Date(), Ext.Date.DAY, 365);
							Ext.util.Cookies.set('_username', usernameInput, dt);
						}
						else if (Ext.isEmpty(cookie)) {
							me.setValue(false);
						}
					}
					else {
						Ext.util.Cookies.set('_username', null, new Date("January 1, 1970"));
						Ext.util.Cookies.clear('_username');
					}
				},
				scope: me
			},
			afterrender:{
				fn: function(control) {
					var me = this;
					var usernameCmp = Ext.getCmp('username');
					//delete the old cookies
					Ext.util.Cookies.set('username', null, new Date("January 1, 1970"));
					Ext.util.Cookies.clear('username');
					Ext.util.Cookies.set('password', null, new Date("January 1, 1970"));
					Ext.util.Cookies.clear('password');
					
					if (Ext.util.Cookies.get('_username')) {
						usernameCmp.setValue(Ext.util.Cookies.get('_username'));
						me.setValue(true);
					}
					else {
						me.setDisabled(true);
						usernameCmp.addListener('change', me.enableCheckbox, me);
					}
				}
			},
			scope: me
		};
	},
	/**
	*
	*/
	enableCheckbox: function (userName, newValue, oldValue, eOpts ) {
		var me = this;
		if (Ext.isEmpty(newValue)) {
			me.setDisabled(true);
		}
		else {
			me.setDisabled(false);
		}
	}
});
/**
*
*/
Ext.define('DCT.SystemMessageCheckBoxField', {
  extend: 'DCT.SystemCheckBoxField',
  

	inputXType: 'dctsystemmessagecheckboxfield',
	xtype: 'dctsystemmessagecheckboxfield',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners,{
			change: {
				fn: function(checkbox, checked) {
					var me = this;
					me.filterMessageList('admin', Ext.getCmp('messagesList').getStore());
				},
				scope: me
			}
		});
	}
});

