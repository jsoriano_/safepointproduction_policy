/*** @class DCT.Util*/
Ext.apply(DCT.Util, {
	/**	*	*/
	viewDocumentation: function () {
		DCT.Util.getSafeElement('_popUp').value = '1';
		var displayPopup = Ext.create('DCT.PopupWindow', {
			width: 800,
			height: 500,
			modal: false,
			dctItemClass: 'DCT.DocumentationPanel',
			defaultButton: 'closeButton',
			buttons: [{
		    text: DCT.T('Close'),
		    id: 'closeButton',
		    handler: function(){
		    	var me = this;
		    	var popup = me.findParentByType('dctpopupwindow');
		    	popup.closePopupWindow();}
		  }]
		});
		displayPopup.show();
	}	
});

