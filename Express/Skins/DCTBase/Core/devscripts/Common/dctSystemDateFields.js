
/**
*
*/
Ext.define('DCT.SystemDateField', {
	extend: 'DCT.InputDateField',

	inputXType: 'dctsystemdatefield',
	xtype: 'dctsystemdatefield',

	/**
	*
	*/
	setDefaultListeners: function (config) {
		config.listeners = {
		};
	},
	/**
	*
	*/
	setListeners: function (config) {
	},
	/**
	*
	*/
	setDisable: function (config) {
		config.disabled = (!Ext.isDefined(config.disabled)) ? ((Ext.get(config.renderTo).findParent('div[class*=hideOffset]', 10)) ? true : false) : config.disabled;
	}	
});
DCT.Util.reg('dctsystemdatefield', 'systemDateField', 'DCT.SystemDateField');
//==================================================                                         
// Batch Search Date control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.BatchSearchDateField', {
	extend: 'DCT.SystemDateField',

	inputXType: 'dctbatchsearchdatefield',
	xtype: 'dctbatchsearchdatefield',

	/**
	*
	*/
	setListeners: function (config) {
		var me = this;
		Ext.apply(config.listeners,{
			keypress: {
				fn: function(datefield, e) {
					var me = this;
					me.batchSearch(datefield, e);
				},
				scope: me
			}
		});
	},
	/**
	*
	*/
	batchSearch: function (datefield, e) { 
		var me = this;
		if(e.getKey() == e.ENTER){
			if (datefield.isValid()){
				me.resetOpenQuery('batchProcess'); 
			}
		}
	}		
});
DCT.Util.reg('dctbatchsearchdatefield', 'batchSearchDateField', 'DCT.BatchSearchDateField');

//==================================================                                         
// Policy Search Date control                                                                
//==================================================                                         
/**
*
*/
Ext.define('DCT.PolicySearchDateField', {
	extend: 'DCT.SystemDateField',

	inputXType: 'dctpolicysearchdatefield',
	hidden: true,
	xtype: 'dctpolicysearchdatefield',
    setListeners: function (config) {
    var me = this;
    Ext.apply(config.listeners, {
        keypress: {
            fn: function (datefield, e) {
                var me = this
                me.policySearch(datefield, e);
            },
            scope: me
        }
    });
},
/**
*
*/
policySearch: function (datefield, e) {
    var me = this;
    if (e.getKey() == e.ENTER) {
        if (datefield.isValid()) {
            DCT.Submit.applySearchFilter(DCT.Submit.activeSearchMode);
        }
    }
}
});
DCT.Util.reg('dctpolicysearchdatefield', 'policySearchDateField', 'DCT.PolicySearchDateField');
