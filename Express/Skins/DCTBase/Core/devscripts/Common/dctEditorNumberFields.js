
/****/
Ext.define('DCT.EditorNumberField', {
  extend: 'DCT.InputNumberField',

	inputXType: 'editornumberfield',
	xtype: 'editornumberfield',

	/**	*	*/
	setListeners: function (config) {
		var me = this;
		if (Ext.isDefined(config.listeners)){
			Ext.destroyMembers(config.listeners, 'focus', 'blur', 'afterrender');
			Ext.Array.forEach(['focus', 'blur', 'afterrender'], function(item, index, arrayList){
				delete config.listeners[item];
			}, me)
		} else {
		    config.listeners = {
					specialkey: function(field, e){
			          if (e.getKey() == e.ENTER) {
			              return false;
			          }
			    },
					change: {
						fn: me.fieldChanged,
						scope:me
					}
				};
		}
	},
	/**	*	*/
	setAllowBlank: function (config) {
  },
  /**   *  */
  setCls: function(config){
  },
  /**   *  */
  setAutoCreate: function(config){
		if (Ext.isDefined(config.fieldRef)) { 
			config.inputAttrTpl = 'fieldRef="' + config.fieldRef + '-editornumberfield"' + ' objectRef="' + config.objectRef + '-editornumberfield"'; 
		}
  },
  /**   *  */
  setFormatting: function(config){
  },
  /**   *  */
  setValidator: function(config){
  },
  /**   *  */
  setRegEx: function(config){
  },
  /**   *  */
  setFormatter: function(){
  },
  /**   *  */
  setCultureSymbols: function(config){
  },
  /**   *  */
  setMaskRe: function(config){
  }
});
