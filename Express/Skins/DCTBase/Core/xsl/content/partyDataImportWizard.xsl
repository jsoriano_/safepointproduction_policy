﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	
  <xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<div id="pageTop">
			<div id="pageTitle">
				<xsl:value-of select="/page/content/recordActions/@caption" />
			</div>
		</div>
		<div id="fields">
			<xsl:attribute name="class">
				<xsl:text>styleWithOutPanels</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="/page/content/recordActions" />
		</div>
	</xsl:template>
  
  <xsl:template match="recordAction">
    <xsl:call-template name="makeButton">
      <xsl:with-param name="type">hyperlink</xsl:with-param>
      <xsl:with-param name="onclick">
        <xsl:text>DCT.Util.doPartyRecordAction('</xsl:text>
        <xsl:value-of select="@execute"/>
        <xsl:text>','</xsl:text>
        <xsl:value-of select="@value"/>
        <xsl:text>','</xsl:text>
        <xsl:value-of select="@caption"/>
        <xsl:text>');</xsl:text>
      </xsl:with-param>
      <xsl:with-param name="class">partyRecordAction</xsl:with-param>
      <xsl:with-param name="caption">
        <xsl:value-of select="@caption"/>
      </xsl:with-param>
    </xsl:call-template>
	
	<xsl:call-template name="makeButton">
		<xsl:with-param name="name">nImportCancel</xsl:with-param>
		<xsl:with-param name="id">importCancel</xsl:with-param>
		<xsl:with-param name="onclick">
			<xsl:choose>
				<xsl:when test="/page/content/ExpressCache/ImportPartyDataToPolicyFromPopup=1">
					<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
					<xsl:value-of select="/page/state/quoteID"/>
					<xsl:text>','','1');</xsl:text>								
				</xsl:when>
				<xsl:otherwise>				
					<xsl:text>DCT.Util.closePartyPopup();</xsl:text>
				</xsl:otherwise>
			</xsl:choose>		
		</xsl:with-param>	
		<xsl:with-param name="caption" select="xslNsODExt:getDictRes('Cancel')"/>
		<xsl:with-param name="class">floatRight padRight</xsl:with-param>
	</xsl:call-template>

  </xsl:template>


 
  
</xsl:stylesheet>
