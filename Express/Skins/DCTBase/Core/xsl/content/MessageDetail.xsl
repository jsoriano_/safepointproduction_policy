﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<!--	*************************************************************************************************************
		Global Variables		
		************************************************************************************************************* -->
	<xsl:variable name="helpImage"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noActionBar">0</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for processing the interview response and producing the overall structure of the HTML page.
		************************************************************************************************************* -->
	<xsl:template name="buildPageContent">
		<div id="fields">
			<xsl:call-template name="buildCommonLayout">
				<xsl:with-param name="screenName">
					<xsl:text>messageDetail</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:choose>
					<xsl:when test="normalize-space(/page/content/getPage/body/@caption)!=''">
						<xsl:value-of select="/page/content/getPage/body/@caption"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsODExt:getDictRes('MessageDetail')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:if test="(/page/content//errors)">
					<p>
						<xsl:value-of select="xslNsODExt:getDictRes('ErrorOccuredInterviewCheckInputs')"/>
					</p>
				</xsl:if>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildHiddenDetailFields">
		<input type="hidden" name="_QuoteID" id="_QuoteID">
			<xsl:attribute name="value">
				<xsl:value-of select="//messages/recipients/recipient[@name='quoteID']/@value"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_ClientID">
			<xsl:attribute name="value">
				<xsl:value-of select="//messages/@clientID"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_messageID">
			<xsl:attribute name="value">
				<xsl:value-of select="//messages/@messageID"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value=""/>
		<input type="hidden" name="_attachmentID" id="_attachmentID"/>
	</xsl:template>
</xsl:stylesheet>