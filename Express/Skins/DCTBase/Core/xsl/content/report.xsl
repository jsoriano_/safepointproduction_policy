﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentBillingPage.xsl"/>
	<xsl:import href="..\common\dctCommonControls.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ReportingHome')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildReportHistory">
		<div id="reportHistory">
			<xsl:call-template name="buildLabel">
				<xsl:with-param name="prompt" select="xslNsODExt:getDictRes('ReportHistory')"/>
			</xsl:call-template>
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">_reportHistoryId</xsl:with-param>
				<xsl:with-param name="name">_reportHistory</xsl:with-param>
				<xsl:with-param name="type">select</xsl:with-param>
				<xsl:with-param name="optionlist">
					<option>
						<xsl:attribute name="value"/>
					</option>
					<xsl:for-each select="/page/content/ReportHistory/PastReport">
						<option>
							<xsl:attribute name="value">
								<xsl:value-of select="HistoryId"/>
							</xsl:attribute>
							<xsl:value-of select="CreationDate"/>
						</option>
					</xsl:for-each>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:text></xsl:text>
				</xsl:with-param>
				<xsl:with-param name="controlClass">historyReportComboField</xsl:with-param>
				<xsl:with-param name="extraConfigItems">
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctTargetPage</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>report</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctReportPath</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctActionButton</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>_reportHistoryId</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildReportList">
		<div id="reportList">
			<xsl:call-template name="buildLabel">
				<xsl:with-param name="prompt" select="xslNsODExt:getDictRes('Reports')"/>
			</xsl:call-template>
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">_reportsPath</xsl:with-param>
				<xsl:with-param name="name">_reportsName</xsl:with-param>
				<xsl:with-param name="type">select</xsl:with-param>
				<xsl:with-param name="optionlist">
					<option>
						<xsl:attribute name="value"/>
					</option>
					<xsl:for-each select="/page/content/Reports/Report">
						<option>
							<xsl:attribute name="value">
								<xsl:value-of select="FullPath"/>
							</xsl:attribute>
							<xsl:value-of select="Description"/>
						</option>
					</xsl:for-each>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:text></xsl:text>
				</xsl:with-param>
				<xsl:with-param name="controlClass">reportNameComboField</xsl:with-param>
				<xsl:with-param name="extraConfigItems">
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctTargetPage</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>report</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctActionButton</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>_reportsPath</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildExportList">
		<xsl:param name="appCode"></xsl:param>
		<div id="exportList">
			<xsl:call-template name="buildLabel">
				<xsl:with-param name="prompt" select="xslNsODExt:getDictRes('Export')"/>
			</xsl:call-template>

			<xsl:choose>
				<xsl:when test="$appCode='BIL'">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">_exportType</xsl:with-param>
						<xsl:with-param name="name">_exportName</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="width">140</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value"/>
							</option>
							<xsl:for-each select="/page/content/Formats/Format">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Name"/>
									</xsl:attribute>
									<xsl:value-of select="@LocalizedName"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text></xsl:text>
						</xsl:with-param>
						<xsl:with-param name="controlClass">exportReportComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctTargetPage</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>report</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctReportPath</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctHistoryId</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/ReportHTMLResult/HistoryId"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>_exportType</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>

				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">_exportType</xsl:with-param>
						<xsl:with-param name="name">_exportName</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="width">140</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value"/>
							</option>
							<xsl:for-each select="/page/content/Formats/Format">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Name"/>
									</xsl:attribute>
									<xsl:value-of select="@LocalizedName"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text></xsl:text>
						</xsl:with-param>
						<xsl:with-param name="controlClass">exportPolReportComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctTargetPage</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>report</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctReportPath</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctHistoryId</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/ReportHTMLResult/HistoryId"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>_exportType</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>

				</xsl:otherwise>
			</xsl:choose>

		</div>
	</xsl:template>

	<xsl:template name="buildReportFilters">
		<table style="width:100%">
			<tr>
				<xsl:for-each select="/page/content/Parameters/Parameter">
					<xsl:choose>
						<xsl:when test="./Name='StartDate' or ./Name='EndDate'">
							<td>
								<xsl:variable name="id" select="concat('_report', ./Name)"/>
								<div id="{$id}">
								  <xsl:variable name="prompt" select="./Prompt"/>
									<xsl:call-template name="buildLabel">
										<xsl:with-param name="prompt" select="xslNsODExt:getDictRes(./Prompt)"/>
									</xsl:call-template>
									<xsl:call-template name="buildSystemControl">
										<xsl:with-param name="id" select="concat('_report', ./Name, 'Id')"/>
										<xsl:with-param name="name" select="concat('_report', ./Name)"/>
										<xsl:with-param name="type">date</xsl:with-param>
										<xsl:with-param name="controlClass">systemDateField</xsl:with-param>
										<xsl:with-param name="dateFormat" select="xslNsExt:formatMaskToUse('','','0')"/>
										<xsl:with-param name="value" select="xslNsExt:getToday()"/>

										<xsl:with-param name="width">100</xsl:with-param>
										<xsl:with-param name="extraConfigItems">
											<xsl:call-template name="addConfigProperty">
												<xsl:with-param name="name">dctTargetPage</xsl:with-param>
												<xsl:with-param name="type">string</xsl:with-param>
												<xsl:with-param name="value">
													<xsl:text>report</xsl:text>
												</xsl:with-param>
											</xsl:call-template>
											<xsl:call-template name="addConfigProperty">
												<xsl:with-param name="name">dctReportPath</xsl:with-param>
												<xsl:with-param name="type">string</xsl:with-param>
												<xsl:with-param name="value">
													<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
												</xsl:with-param>
											</xsl:call-template>
										</xsl:with-param>
									</xsl:call-template>
								</div>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="./Name!='TaskStatus' and ./Name!='CultureCode' and ./Name!='ProcessingDate' and ./Name!='Parms' and ./Name!='StartDate' and ./Name!='EndDate'">
								<td>
									<xsl:call-template name="buildFilterControl"/>
								</td>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</tr>
			<tr>
				<td colspan="6">
					<xsl:call-template name="buildTaskFilter" />
					<xsl:call-template name="buildExportList"/>
					<xsl:call-template name="buildReportList"/>
					<xsl:call-template name="buildViewReportButton"/>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="buildTaskFilter">
		<xsl:for-each select="/page/content/Parameters/Parameter">
			<xsl:if test="./Name='TaskStatus'">
				<div id ="reportTaskId">
					<xsl:call-template name="buildLabel">
					  <xsl:with-param name="prompt" select="xslNsODExt:getDictRes('Tasks')" />
					</xsl:call-template>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id" select="concat('_report', 'TaskId')"/>
						<xsl:with-param name="name" select="concat('_report', 'Task')"/>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="width">120</xsl:with-param>
						<xsl:with-param name="controlClass" select="concat('report', 'Task', 'FilterComboField')"/>
						<xsl:with-param name="optionlist">
							<xsl:call-template name="buildFilterOptionList">
								<xsl:with-param name="parameterValues" select="./ValidValues"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text></xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctTargetPage</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>report</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctReportPath</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>

	<xsl:template name="buildFilterControl">
		<xsl:variable name ="id" select="concat('_report', ./Name)"/>
		<div id ="{$id}">
			<xsl:call-template name="buildLabel">
				<xsl:with-param name="prompt" select="xslNsODExt:getDictRes(./Prompt)"/>
			</xsl:call-template>

			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id" select="concat('_report', ./Name, 'Id')"/>
				<xsl:with-param name="name" select="concat('_report', ./Name)"/>
				<xsl:with-param name="type">select</xsl:with-param>
				<xsl:with-param name="width">120</xsl:with-param>
				<xsl:with-param name="controlClass" select="concat('report', ./Name, 'FilterComboField')"/>
				<xsl:with-param name="optionlist">
					<xsl:call-template name="buildFilterOptionList">
						<xsl:with-param name="parameterValues" select="./ValidValues"/>
					</xsl:call-template>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:text></xsl:text>
				</xsl:with-param>
				<xsl:with-param name="extraConfigItems">
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctTargetPage</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>report</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctReportPath</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/ReportHTMLResult/ReportPath"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildLabel">
		<xsl:param name="prompt"/>

		<label>
			<xsl:value-of select="$prompt"/>
			<xsl:text> </xsl:text>
		</label>
	</xsl:template>

	<xsl:template name="buildFilterOptionList">
		<xsl:param name="parameterValues"/>

		<option value="all">All</option>
		<xsl:for-each select="$parameterValues/ValidValue">
			<option>
				<xsl:attribute name="value">
					<xsl:value-of select="./@Value"/>
				</xsl:attribute>
				<xsl:value-of select="./@Label"/>
			</option>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="buildOriginalOptions">
		<xsl:call-template name="buildReportHistory"/>
		<xsl:call-template name="buildExportList">
			<xsl:with-param name="appCode">BIL</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="buildReportList"/>
	</xsl:template>

	<xsl:template name="buildViewReportButton">
		<div id="viewReport">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">
					<xsl:text>_viewReportLink</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="id">
					<xsl:text>viewReportLink</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.openPolicyReportButton();</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('ViewReport')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<div id="reportSelectionDiv">
			<xsl:choose>
				<xsl:when test="/page/content/Reports/@appCode='POL'">
					<table style="width:100%">
						<tr>
							<td>
								<xsl:call-template name="buildReportFilters"/>
							</td>
						</tr>
					</table>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildOriginalOptions"/>
				</xsl:otherwise>
			</xsl:choose>
			<div id="pageInstruction"/>
			<xsl:call-template name="buildReportContent"/>
		</div>
	</xsl:template>

	<xsl:template name="buildReportContent">
		<xsl:value-of select="/page/content/ReportHTMLResult/ReportHTML" disable-output-escaping="yes" />
	</xsl:template>
</xsl:stylesheet>