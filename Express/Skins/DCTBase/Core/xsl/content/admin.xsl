﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:apply-templates select="/page/content"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:variable name="roleSecurity" select="/page/state/@roleSecurity=1"/>
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ChangePassword')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="content">
		<xsl:call-template name="buildPageHeaderSection"/>
		<xsl:call-template name="buildSubNav"/>
		<xsl:call-template name="buildMainContent"/>
	</xsl:template>

	<xsl:template name="buildSubNav">
		<div id="pageSubNav">
		</div>
	</xsl:template>

	<xsl:template name="buildChangePasswordTabContent">
		<div id="buildChangePasswordDialog" class="downLayout">
			<div id="existingPassword" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="existingPasswordLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('CurrentPassword_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">existingPasswordField</xsl:with-param>
						<xsl:with-param name="name">_oldpassword</xsl:with-param>
						<xsl:with-param name="controlClass">systemTextField</xsl:with-param>
						<xsl:with-param name="type">password</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="newpassword" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="newpasswordLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('NewPassword_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">newpasswordField</xsl:with-param>
						<xsl:with-param name="name">_newpassword</xsl:with-param>
						<xsl:with-param name="controlClass">systemTextField</xsl:with-param>
						<xsl:with-param name="type">password</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="confirmpassword" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="confirmpasswordLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('ConfirmNewPassword_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">confirmpasswordField</xsl:with-param>
						<xsl:with-param name="name">_confirmpassword</xsl:with-param>
						<xsl:with-param name="controlClass">systemTextField</xsl:with-param>
						<xsl:with-param name="type">password</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downFormLabel">
				</div>
				<div class="downFormField">
					<div id="_ErrorActions" class="g-btn-bar">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">changePasswordA</xsl:with-param>
							<xsl:with-param name="id">changePasswordA</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.adminAction('password','change');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('ChangePassword')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">cancelChangePasswordA</xsl:with-param>
							<xsl:with-param name="id">cancelChangePasswordA</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:choose>
									<xsl:when test="/page/content/portals/portal[@active = '1']/@name[contains(.,'Billing')]">
										<xsl:text>DCT.Submit.setActiveParent('billingDashBoard');DCT.Submit.submitBillingPage('billingDashBoard','');</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>DCT.Submit.setActiveParent('dashboardHome');DCT.Submit.submitPage('dashboardHome','');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
							</xsl:with-param>
							<xsl:with-param name="type">cancel</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="buildMainContent">
		<xsl:choose>
			<xsl:when test="admin/@section='password'">
				<xsl:call-template name="buildSharedHiddenFormFields"/>
				<xsl:call-template name="buildChangePasswordTabContent"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="buildSharedHiddenFormFields">
		<input type="hidden" name="_pageType" id="_pageType" value=""/>
		<input type="hidden" name="_adminAction" id="_adminAction"/>
		<input type="hidden" name="_startIndex" id="_startIndex" value="{/page/content/agencyList/@startIndex}"/>
		<input type="hidden" name="_listCount" id="_listCount" value="{/page/content/agencyList/@listCount}"/>
		<input type="hidden" name="_maxItems" id="_maxItems" value="50"/>
	</xsl:template>
</xsl:stylesheet>