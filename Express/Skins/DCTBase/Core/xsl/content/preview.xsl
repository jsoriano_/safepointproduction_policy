﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:call-template name="buildContainer">
            <xsl:with-param name="noActionBar">1</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

    <xsl:template name="processPageHeader">
        <xsl:call-template name="buidPageHeader">
            <xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PrintOptions')" />
            </xsl:with-param>
            <xsl:with-param name="pageInstruction">
                <xsl:if test="/page/state/clientName != ''">
                    <p>
						<xsl:value-of select="xslNsODExt:getDictRes('FollowingPrintOptionsAvailable')" />
						<xsl:text> </xsl:text>
                        <xsl:call-template name="makeButton">
                            <xsl:with-param name="name">clientNameA</xsl:with-param>
                            <xsl:with-param name="id">clientNameA</xsl:with-param>
                            <xsl:with-param name="onclick">DCT.Submit.submitPage('interview');</xsl:with-param>
                            <xsl:with-param name="caption">
                                <xsl:value-of disable-output-escaping="yes" select="/page/state/clientName"/>
                            </xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
                        </xsl:call-template>
                    </p>
                </xsl:if>
                <p>
					<xsl:value-of select="xslNsODExt:getDictRes('SelectPrintOption_PressPrint')" />
                </p>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="buildPageContent">
		<div id="printItems"></div>
    </xsl:template>
</xsl:stylesheet>