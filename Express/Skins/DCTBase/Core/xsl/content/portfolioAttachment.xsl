﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PortfolioAttachments')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('OnThisPageAttachmentsPortfolio')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPortfolioButtons">
			<xsl:with-param name="showHeader">True</xsl:with-param>
			<xsl:with-param name="showPortfolioLink">True</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="buildPortfolioAttachments"/>
	</xsl:template>

	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_attachID" id="_attachID" value="{/page/content/attach/@id}"/>
		<input type="hidden" name="_attachmentID" id="_attachmentID" value="0"/>
	</xsl:template>

	<xsl:template name="buildPortfolioAttachments">
		<div id="portfolioAttachmentListHeader" class="toggleHeaderNoHover">
			<xsl:value-of select="xslNsODExt:getDictRes('CurrentPortfolioAttachments')"/>
		</div>
		<div id="portfolioAttachmentList"></div>
		<xsl:call-template name="portfolioAttachmentListWidget"/>
		<div id="portfolioAddAttachmentSection">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('AddAnAttachment')"/>
			</h2>
			<div id="userInformationDialog" class="downLayout">
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('File_colon')"/>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">uploadFile</xsl:with-param>
							<xsl:with-param name="name">_uploadFile</xsl:with-param>
							<xsl:with-param name="size">25</xsl:with-param>
							<xsl:with-param name="maxlength">25</xsl:with-param>
							<xsl:with-param name="controlClass">systemFileField</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Caption_colon')"/>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">caption</xsl:with-param>
							<xsl:with-param name="name">_caption</xsl:with-param>
							<xsl:with-param name="size">25</xsl:with-param>
							<xsl:with-param name="maxlength">50</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel"></div>
					<div class="downFormField">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">newAttachmentA</xsl:with-param>
							<xsl:with-param name="id">newAttachmentA</xsl:with-param>
							<xsl:with-param name="onclick">if (document.forms[0]._caption.value=='' || document.forms[0]._uploadFile.value=='') alert(DCT.T('EnterFilenameCaptionUpload')); else DCT.Submit.submitPageAction('portfolioAttachment','upload');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('AddAttachment')"/>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="portfolioAttachmentListWidget">
		<xsl:call-template name="buildNonPagingGridPanel">
			<xsl:with-param name="gridID">
				<xsl:text>portfolioAttachmentList</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="storeFields">
				<xsl:text>[{name: 'id', type: 'string', mapping: 0},</xsl:text>
				<xsl:text>{name: 'attachmentID', type: 'string', mapping: 0},</xsl:text>
				<xsl:text>{name: 'imageDir', type: 'string', mapping: 1},</xsl:text>
				<xsl:text>{name: 'filename', type: 'string', mapping: 2},</xsl:text>
				<xsl:text>{name: 'caption', type: 'string', mapping: 3},</xsl:text>
				<xsl:text>{name: 'attachDate', type: 'date', dateFormat: 'Y-m-d', mapping: 4}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.portfolioAttachmentRowActionColumnRenderer},</xsl:text>
				<xsl:text>{text: DCT.T("Filename"), dataIndex: 'filename', sortable: true, renderer: DCT.Grid.portfolioAttachmentNameColumnRenderer},</xsl:text>
				<xsl:text>{text: DCT.T("Caption"), dataIndex: 'caption', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T("DateAttached"), dataIndex: 'attachDate', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
				<xsl:value-of select="xslNsExt:formatMaskToUse('', '0', '1')"/>
				<xsl:text>')}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="gridData">
				<xsl:text>[</xsl:text>
				<xsl:for-each select="/page/content/attachments/attachment">
					<xsl:text>[</xsl:text>
					<xsl:value-of select="@attachmentID"/>,"<xsl:value-of select="$imageDir"/>","<xsl:value-of select="@filename"/>","<xsl:value-of select="@caption"/>","<xsl:value-of select="@attachDate"/>
					<xsl:text>"]</xsl:text>
					<xsl:if test="position() != last()">
						<xsl:text>,</xsl:text>
					</xsl:if>
				</xsl:for-each>
				<xsl:text>]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="showToolBar">
				<xsl:text>true</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>