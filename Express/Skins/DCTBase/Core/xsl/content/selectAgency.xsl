﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:variable name="returnPage">
		<xsl:choose>
			<xsl:when test="string-length(/page/content/ExpressCache/_returnPage) = 0 or /page/content/ExpressCache/_returnPage = 'login'">
				<xsl:value-of select="/page/actions/@startPage"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/page/content/ExpressCache/_returnPage"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="cancelReturnPage">
		<xsl:choose>
			<xsl:when test="string-length(/page/content/ExpressCache/_returnPage) = 0">
				<xsl:text>login</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/page/content/ExpressCache/_returnPage"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('SelectAnAgency')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('SelectAgencyList_FilterSearch')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<!-- Override to remove the left panel -->
	<xsl:template name="createLeftPanel">
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:variable name="maxItems">
			<xsl:value-of select="/page/content/agencyListFilter/@maxItems"/>
		</xsl:variable>
		<xsl:variable name="showFilters">
			<xsl:text>1</xsl:text>
		</xsl:variable>
		<xsl:variable name="displayCount">
			<xsl:choose>
				<xsl:when test="$maxItems &gt; 0">
					<xsl:value-of select="$maxItems"/>
				</xsl:when>
				<xsl:otherwise>25</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="agencyListCount">
			<xsl:value-of select="/page/content/agencyList/@listCount"/>
		</xsl:variable>
		<xsl:call-template name="buildHiddenAgencyFields">
			<xsl:with-param name="displayCount" select="$displayCount"/>
		</xsl:call-template>
		<div id="pageTitle">
			<xsl:value-of select="xslNsODExt:getDictRes('SelectAnAgency')"/>
		</div>
		<div id="selectAgency" class="acrossLayout">
			<xsl:if test="$showFilters = 1">
				<div id="agencyFilterBuilder">
					<div id="agencyGlobalFilter">
						<div id="agencyTypeLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('AgencyType')"/>
						</div>
						<div id="agencyTypeItems">
							<xsl:call-template name="buildSystemControl">
								<xsl:with-param name="id">Agency_EntityTypeID_Id</xsl:with-param>
								<xsl:with-param name="name">Agency_EntityTypeID</xsl:with-param>
								<xsl:with-param name="type">select</xsl:with-param>
								<xsl:with-param name="optionlist">
									<option value="">
										<xsl:value-of select="xslNsODExt:getDictRes('ViewAllTypes')"/>
									</option>
									<xsl:for-each select="/page/content/EntityTypes/EntityType">
										<option>
											<xsl:attribute name="value">
												<xsl:value-of select="@id"/>
											</xsl:attribute>
											<xsl:value-of select="@name"/>
										</option>
									</xsl:for-each>
								</xsl:with-param>
								<xsl:with-param name="controlClass">selectagencyComboField</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/content/agencyListFilter/@EntityTypes_ID"/>
								</xsl:with-param>
								<xsl:with-param name="width">110</xsl:with-param>
							</xsl:call-template>
							<xsl:text>  |  </xsl:text>
							<xsl:if test="/page/content/previousPage/@name = 'search' or (/page/state/previousPage = 'selectAgency' and /page/state/activeParentPage = 'search')">
								<a href="javascript:;" id="adminAllAgenciesSelect" onmousemove="window.status='';" class="">
									<xsl:attribute name="onclick">
										<xsl:text>DCT.Util.selectAgency('0',DCT.T('AllAgencies'));</xsl:text>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes('SelectAllAgencies')"/>
								</a>
								<xsl:text>  |  </xsl:text>
							</xsl:if>

							<a href="javascript:;" id="adminMoreOptionsToggle" onmousemove="window.status='';" class="actionLink linkExpand">
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Util.toggleSection('adminMoreOptionsPanelCmp'); return false;</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('MoreOptions')"/>
							</a>
						</div>
					</div>
					<div id="adminMoreOptionsPanel">
						<xsl:element name="div">
							<xsl:attribute name="class">collapsiblePanel</xsl:attribute>
							<xsl:attribute name="data-config">
								<xsl:text>{</xsl:text>
								<xsl:text>renderTo: "adminMoreOptionsPanel",</xsl:text>
								<xsl:text>id: "adminMoreOptionsPanelCmp",</xsl:text>
								<xsl:text>contentEl: "adminMoreOptions",</xsl:text>
								<xsl:text>collapsed: true</xsl:text>
								<xsl:text>}</xsl:text>
							</xsl:attribute>
						</xsl:element>
					</div>
					<div id="adminMoreOptions" class="acrossLayout x-hidden">
						<div class="acrossFieldGroup">
							<div class="acrossAboveFormLabel">
								<label id="Agency_Name_captionText" for="Agency_Name_Id">
									<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
								</label>
							</div>
							<div class="acrossFormField">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">Agency_Name_Id</xsl:with-param>
									<xsl:with-param name="name">Agency_Name</xsl:with-param>
									<xsl:with-param name="size">15</xsl:with-param>
									<xsl:with-param name="maxlength">50</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/agencyListFilter/@Agency_Name"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
						<div class="acrossFieldGroup">
							<div class="acrossAboveFormLabel">
								<label id="Agency_PhoneNumber_captionText" for="Agency_PhoneNumber_Id">
									<xsl:value-of select="xslNsODExt:getDictRes('Phone_colon')"/>
								</label>
							</div>
							<div class="acrossFormField">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">Agency_PhoneNumber_Id</xsl:with-param>
									<xsl:with-param name="name">Agency_PhoneNumber</xsl:with-param>
									<xsl:with-param name="size">10</xsl:with-param>
									<xsl:with-param name="maxlength">20</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/agencyListFilter/@Agency_PhoneNumber"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
						<div class="acrossFieldGroup">
							<div class="acrossAboveFormLabel">
								<label id="Agency_Address1_captionText" for="Agency_Address1_Id">
									<xsl:value-of select="xslNsODExt:getDictRes('Address_colon')"/>
								</label>
							</div>
							<div class="acrossFormField">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">Agency_Address1_Id</xsl:with-param>
									<xsl:with-param name="name">Agency_Address1</xsl:with-param>
									<xsl:with-param name="size">20</xsl:with-param>
									<xsl:with-param name="maxlength">20</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/agencyListFilter/@Agency_Address1"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
						<div class="acrossFieldGroup">
							<div class="acrossAboveFormLabel">
								<label id="Agency_City_captionText" for="Agency_City_Id">
									<xsl:value-of select="xslNsODExt:getDictRes('City_colon')"/>
								</label>
							</div>
							<div class="acrossFormField">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">Agency_City_Id</xsl:with-param>
									<xsl:with-param name="name">Agency_City</xsl:with-param>
									<xsl:with-param name="size">10</xsl:with-param>
									<xsl:with-param name="maxlength">20</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/agencyListFilter/@Agency_City"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
						<div class="acrossFieldGroup">
							<div class="acrossAboveFormLabel">
								<label id="Agency_State_captionText" for="Agency_State_Id">
									<xsl:value-of select="xslNsODExt:getDictRes('State_colon_address')"/>
								</label>
							</div>
							<div class="acrossFormField">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">Agency_State_Id</xsl:with-param>
									<xsl:with-param name="name">Agency_State</xsl:with-param>
									<xsl:with-param name="size">2</xsl:with-param>
									<xsl:with-param name="maxlength">20</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/agencyListFilter/@Agency_State"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">searchLink</xsl:with-param>
							<xsl:with-param name="id">searchLink</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Util.searchAgency(false, 'searchLink');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
							</xsl:with-param>
							<xsl:with-param name="type">search</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">clearLink</xsl:with-param>
							<xsl:with-param name="id">clearLink</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Util.searchAgency(true, 'clearLink');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Clear')"/>
							</xsl:with-param>
							<xsl:with-param name="type">cancel</xsl:with-param>
							<xsl:with-param name="class">btnDisabled</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
			</xsl:if>
			<br/>
			<div id="agencyListing" tabletype="acrossAboveOnce" class="downLayout">
				<!-- Build the paging grid. -->
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="dataOnLoad">true</xsl:with-param>
					<xsl:with-param name="gridID">
						<xsl:text>agencyList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>agencyList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>10</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>agencyList/@listCount</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>agency</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@agencyID</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@name'},</xsl:text>
						<xsl:text>{name: 'primarycontact', type: 'string', mapping: '@primarycontact'},</xsl:text>
						<xsl:text>{name: 'phonenumber', type: 'string', mapping: '@phonenumber'},</xsl:text>
						<xsl:text>{name: 'address1', type: 'string', mapping: '@address1'},</xsl:text>
						<xsl:text>{name: 'city', type: 'string', mapping: "@city"},</xsl:text>
						<xsl:text>{name: 'state', type: 'string', mapping: '@state'},</xsl:text>
						<xsl:text>{name: 'zipcode', type: 'string', mapping: '@zipcode'},</xsl:text>
						<xsl:text>{name: 'entitytypeName', type: 'string', mapping: '@entitytypeName'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T('Name'), width: 120, dataIndex: 'name', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Type'), width: 60, dataIndex: 'entitytypeName', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Address'), width: 120, dataIndex: 'address1', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('City'), width: 80, dataIndex: 'city', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('State'), width: 50, dataIndex: 'state', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Zip'), width: 60, dataIndex: 'zipcode', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Phone'), width: 80, dataIndex: 'phonenumber', sortable: true},</xsl:text>
						<xsl:text>{text: "", dataIndex: 'name', width: 60, sortable: false, renderer: </xsl:text>
						<xsl:text>function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.selectAgencyRenderer('</xsl:text>
						<xsl:value-of select="$returnPage"/>
						<xsl:text>', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }}]</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">cancelLink</xsl:with-param>
				<xsl:with-param name="id">cancelLink</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Util.cancelSelectAgency('</xsl:text>
					<xsl:value-of select="$returnPage"/>
					<xsl:text>');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenAgencyFields">
		<xsl:param name="displayCount"/>
		<input type="hidden" name="_startIndex" id="_startIndex">
			<xsl:attribute name="value">
				<xsl:value-of select="page/content/agencyList/@startIndex"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_displayCount" id="_displayCount">
			<xsl:attribute name="value">
				<xsl:value-of select="$displayCount"/>
			</xsl:attribute>
		</input>
		<input type="hidden" id="_previousPage" name="_previousPage">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/previousPage/@name"/>
			</xsl:attribute>
		</input>
		<input type="hidden" id="_filterXML" name="_filterXML" value="">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/agencyListFilter/@filter"/>
			</xsl:attribute>
		</input>
	</xsl:template>
</xsl:stylesheet>