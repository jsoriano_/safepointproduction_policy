﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('SystemErrorOccurred')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<p>
					<xsl:value-of select="xslNsExt:FormatString1(xslNsODExt:getDictRes('SubmitReport_ContactAdmin'),xslNsODExt:getDictRes('SubmitErrorReport'))" />
				</p>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:apply-templates select="page/content/errors"/>
	</xsl:template>
	<xsl:template match="errors">
		<input type="hidden" name="_QuoteID" id="_QuoteID">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/state/quoteID"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_SuppressAutoSave" id="_SuppressAutoSave" value="1">
		</input>
		<div id="errorTextGroup">
			<xsl:choose>
				<xsl:when test="/page/content//errors/error[not(@express) or @express!='1']">
					<!-- We have an Example Server error. Just display the last occurrence of a Server error. -->
					<xsl:call-template name="displayError">
						<xsl:with-param name="error" select="/page/content//errors/error[not(@express) or @express!='1'][last()]"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="/page/content//errors/error[@express='1']">
						<xsl:call-template name="displayError">
							<xsl:with-param name="error" select="."/>
						</xsl:call-template>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div id="errorActions" class="g-btn-bar">
			<xsl:variable name="doubleQuote">
				<xsl:text>"</xsl:text>
			</xsl:variable>
			
			<xsl:variable name="fullHref">
				<xsl:text disable-output-escaping="yes">mailto:</xsl:text>
				<xsl:value-of select="/page/state/mailTo"/>
				<xsl:text disable-output-escaping="yes">?subject=</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('ErrorReport')" />
				<xsl:text disable-output-escaping="yes">&amp;body=</xsl:text>
				<xsl:for-each select="/page/content//errors/error">
					<xsl:value-of disable-output-escaping="yes" select="translate(.,$doubleQuote,'')"/>
					<xsl:text disable-output-escaping="yes">%0D%0A</xsl:text>
					<!-- new line. -->
				</xsl:for-each>
			</xsl:variable>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="href">
					<xsl:value-of select="substring($fullHref, 1, 1300)"/>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('SubmitErrorReport')" />
				</xsl:with-param>
				<xsl:with-param name="useHref">1</xsl:with-param>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="/page/popUp='1'">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="href">
							<xsl:text>javascript:DCT.Util.closeWindow();</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Close')" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.submitPage('home');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('ReturnToHome')" />
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.submitAction('resetSession');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('ReturnToLogin')" />
						</xsl:with-param>
					</xsl:call-template>
					<xsl:if test="/page/state/activeLOB and /page/state/quoteID > 0">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
								<xsl:value-of select="/page/state/quoteID"/>
								<xsl:text>','</xsl:text>
								<xsl:value-of select="/page/state/activeLOB"/>
								<xsl:text>')</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('ReturnToPolicy')" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="/page/debugMode!='0' and /page/content/@page='interview'">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="onClick">
								<xsl:text>DCT.Submit.submitPage('</xsl:text>
								<xsl:value-of select="//page/content/@page"/>
								<xsl:text>')</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('GoBack')" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="error">
		<tr>
			<td>
				<span>
					<xsl:value-of select="@Source"/>
				</span>
			</td>
			<td>
				<span>
					<xsl:value-of select="@TargetSite"/>
				</span>
			</td>
			<td>
				<span>
					<xsl:value-of select="text()"/>
				</span>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="stack">
		<p class="stackText">
			<xsl:value-of select="text()"/>
		</p>
	</xsl:template>

	<xsl:template name="displayError">
		<xsl:param name="error"/>
		<div class="notificationDiv nError">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="$error"/>
			</div>
		</div>
		<xsl:if test="$error">
			<div id="errorDetailsSection">
				<div id="batchProcessJobTable">
					<h2>
						<xsl:value-of select="xslNsODExt:getDictRes('ErrorDetails')" />
					</h2>
					<table id="batchProcessListTable" class="formTable">
						<tr>
							<th>
								<span>
									<xsl:value-of select="xslNsODExt:getDictRes('Source')" />
								</span>
							</th>
							<th>
								<span>
									<xsl:value-of select="xslNsODExt:getDictRes('TargetSite')" />
								</span>
							</th>
							<th>
								<span>
									<xsl:value-of select="xslNsODExt:getDictRes('Description')" />
								</span>
							</th>
						</tr>
						<xsl:apply-templates select="error"/>
					</table>
				</div>
				<xsl:if test="stack">
					<div id="stackSection">
						<h2>
							<xsl:value-of select="xslNsODExt:getDictRes('StackDetails')" />
						</h2>
						<xsl:apply-templates select="stack"/>
					</div>
				</xsl:if>
			</div>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>