﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:variable name="helpImage"/>
	<xsl:variable name="cultureFormat">
		<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
	</xsl:variable>
	<xsl:variable name="quoteLinkValue">
		<xsl:if test="/page/state/previousPage = 'policy'">
			<xsl:value-of select="/page/state/quoteID"/>
		</xsl:if>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('Notifications')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildMainAreaContent"/>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{$quoteLinkValue}"/>
		<input type="hidden" name="_showBody" id="_showBody" value="1"/>
	</xsl:template>
	<xsl:template name="buildMainAreaContent">
		<xsl:call-template name="messagesList">
			<xsl:with-param name="listTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('Notifications')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="pageSpecificActions"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="pageSpecificActions">
		<div id="subactiongroup">
			<ul id="subMenuActions">
				<li>
					<a>
						<xsl:attribute name="name">newMessageA</xsl:attribute>
						<xsl:attribute name="id">id_NewNotification</xsl:attribute>
						<xsl:attribute name="href">javascript:;</xsl:attribute>
						<xsl:attribute name="onclick">DCT.Submit.actOnMessage('', 'newNotification');</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes('SendNotification')"/>
					</a>
				</li>
			</ul>
		</div>
	</xsl:template>
	<xsl:template name="messagesList">
		<xsl:param name="listTitle"/>
		<xsl:call-template name="buildMessagesListHiddenFormFields"/>
		<xsl:variable name="formatMask" select="xslNsExt:formatMaskToUse('', '', '0')"/>
		<div class="contentFieldGroup">
			<div id="quickQuerySelections">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">quickQueryId</xsl:with-param>
					<xsl:with-param name="name">quickQuery</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="all">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAll')"/>
						</option>
						<option value="read">
							<xsl:value-of select="xslNsODExt:getDictRes('Read')"/>
						</option>
						<option value="unread">
							<xsl:value-of select="xslNsODExt:getDictRes('Unread')"/>
						</option>
						<xsl:if test="$hasAdminSecurityRights or $securityLevel = '2'">
							<xsl:for-each select="/page/content/userList/user">
								<!-- Only get unique users -->
								<xsl:if test="not(preceding-sibling::user) or not(preceding-sibling::user/@name=@name)">
									<option value="{@name}">
										<xsl:choose>
											<xsl:when test="@fullName=''">
												<xsl:value-of select="@name"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="@fullName"/>
											</xsl:otherwise>
										</xsl:choose>
									</option>
								</xsl:if>
							</xsl:for-each>
						</xsl:if>
					</xsl:with-param>
					<xsl:with-param name="controlClass">messageQueryComboField</xsl:with-param>
				</xsl:call-template>
			</div>
			<xsl:if test="$hasAdminSecurityRights and $currentPage='messages'">
				<div id="showEntireDatabase">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="id">showEntireDatabaseCheckBoxGroup</xsl:with-param>
						<xsl:with-param name="checkbox">
							<xsl:call-template name="buildCheckBoxObject">
								<xsl:with-param name="fieldID">customQueryCheckbox</xsl:with-param>
								<xsl:with-param name="name">_customQueryCheckbox</xsl:with-param>
								<xsl:with-param name="inputValue">1</xsl:with-param>
								<xsl:with-param name="dctClassName">dctsystemmessagecheckboxfield</xsl:with-param>
								<xsl:with-param name="boxLabel">
									<xsl:value-of select="xslNsODExt:getDictRes('ViewEntireDatabase')"/>
								</xsl:with-param>
								<xsl:with-param name="checked">
									<xsl:choose>
										<xsl:when test="/page/content/@customQuery='1'">true</xsl:when>
										<xsl:otherwise>false</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
		</div>
		<div id="messageGridList">
			<!-- Build the paging grid. -->
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="toolBarTitle">DCT.T('Notifications')</xsl:with-param>
				<xsl:with-param name="gridID">
					<xsl:text>messagesList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>messagesList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>20</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>messages/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>message</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@messageID</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'sentOn', type: 'date', dateFormat: 'Y-m-d H:i:s'},</xsl:text>
					<xsl:text>{name: 'userID', type: 'string'},</xsl:text>
					<xsl:text>{name: 'sender', type: 'string'},</xsl:text>
					<xsl:text>{name: 'action', type: 'string'},</xsl:text>
					<xsl:text>{name: 'subject', type: 'string'},</xsl:text>
					<xsl:text>{name: 'read', type: 'string', mapping: '@read'},</xsl:text>
					<xsl:text>{name: 'policyID', type: 'string'},</xsl:text>
					<xsl:text>{name: 'policyNumber', type: 'string'},</xsl:text>
					<xsl:text>{name: 'remindOn', type: 'date', dateFormat: 'Y-m-d'},</xsl:text>
					<xsl:text>{name: 'closedDate', type: 'date', dateFormat: 'Y-m-d'},</xsl:text>
					<xsl:text>{name: 'body', type: 'string'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T("Type"), dataIndex: 'action', sortable: true, width: 40, renderer: DCT.Grid.messageTypeColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T("PolicyNumber"), dataIndex: 'policyNumber', sortable: false, width: 40, renderer: DCT.Grid.messagePolicyColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T("Received"), dataIndex: 'sentOn', sortable: true, width: 50, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="$cultureFormat"/>
					<xsl:text>, g:i a')},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'userID', sortable: false, width: 10, renderer: DCT.Grid.messageEditColumnRenderer},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'userID', sortable: false, width: 10, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.messageDeleteColumnRenderer('messagesList', </xsl:text>
					<xsl:value-of select="$canDeleteNotifications"/>
					<xsl:text>, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); } }]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.messageBodyPreview</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
</xsl:stylesheet>