﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonParty.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyIsTitle')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="createPartyIsRelationshipContent"/>
	</xsl:template>
	<xsl:template name="createPartyIsRelationshipContent">
		<xsl:call-template name="buildSelectedPartyContent">
			<xsl:with-param name="selectedTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyIsSubTitle')"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="buildRelatedPartyLink"/>
		<xsl:call-template name="buildRelatedPartySearch"/>
		<xsl:call-template name="buildRelatedPartySearchData"/>
		<div id="partyIsActions" class="g-btn-bar">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nProcessSearch</xsl:with-param>
				<xsl:with-param name="id">processSearch</xsl:with-param>
				<xsl:with-param name="onclick">
					<!--<xsl:text>submitPartyPage('partyIsCandidate');</xsl:text>-->
					<xsl:text>if(DCT.Util.validateSearchData()){DCT.Submit.submitPartyPage('partyIsCandidate');}</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('SearchForParty')"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nProcessCancel</xsl:with-param>
				<xsl:with-param name="id">processCancel</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitPartyPage('partyManagement');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
		<script>DCT.Util.hideRelatedPartySearch();</script>
	</xsl:template>
	<xsl:template name="buildRelatedPartyLink">
		<div id="relatedPartyLink">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nFindRelatedParties</xsl:with-param>
				<xsl:with-param name="id">findRelatedParties</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Util.showRelatedPartySearch();</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyIsFindLink')"/>
				</xsl:with-param>
				<xsl:with-param name="type">hyperlink</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildRelatedPartySearch">
		<div id="relatedPartySearch" class="hideOffset">
			<div>
				<h2>
					<xsl:value-of select="xslNsODExt:getDictRes('FindPeopleAndPlaces')"/>
				</h2>
			</div>
			<div>
				<table class="formTbl">
					<tr>
						<td class="" id="lookingForLabel">
							<label id="lookingForALabel">
								<xsl:value-of select="xslNsODExt:getDictRes('ImLookingForA_colon')"/>
								<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
							</label>
							<label id="lookingForAnLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('ImLookingForAn_colon')"/>
								<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
							</label>
						</td>
						<td>
							<div id="lookingForOption">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">lookingForSelectionId</xsl:with-param>
									<xsl:with-param name="name">nLookingForSelection</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="controlClass">partyLookUpComboField</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option value="">
											<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
										</option>
										<option value="O">
											<xsl:value-of select="xslNsODExt:getDictRes('OrganizationSearch')"/>
										</option>
										<option value="P">
											<xsl:value-of select="xslNsODExt:getDictRes('PersonSearch')"/>
										</option>
										<option value="AG">
											<xsl:value-of select="xslNsODExt:getDictRes('AgencySearch')"/>
										</option>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctSearchByField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>searchBySelectionId</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctLookingForALabel</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>lookingForALabel</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctlookingForAnLabel</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>lookingForAnLabel</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr>
						<td class="">
							<label>
								<xsl:value-of select="xslNsODExt:getDictRes('IWantToSearchBy_colon')"/>
								<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
							</label>
						</td>
						<td>
							<div id="searchByOption">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchBySelectionId</xsl:with-param>
									<xsl:with-param name="name">nSearchBySelection</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="controlClass">partySearchByComboField</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option value="">
											<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
										</option>
										<option value="NA">
											<xsl:value-of select="xslNsODExt:getDictRes('NameAndAddress')"/>
										</option>
										<option value="LC">
											<xsl:value-of select="xslNsODExt:getDictRes('LicenseNumber')"/>
										</option>
										<option value="PH">
											<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumber')"/>
										</option>
										<option value="RF">
											<xsl:value-of select="xslNsODExt:getDictRes('ReferenceNumber')"/>
										</option>
										<option value="OT">
											<xsl:value-of select="xslNsODExt:getDictRes('Other')"/>
										</option>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctLookUpField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>lookingForSelectionId</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctDataRequiredField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>dataRequired1</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctDataRequiredPlusField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>dataRequired2</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctProcessSearchField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>processSearch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctPhoneData</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>phoneData</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctLicenseData</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>licenseData</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctReferenceData</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>referenceData</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildRelatedPartySearchData">
		<div id="relatedPartySearchData" class="hideOffset">
			<div>
				<label id="dataRequired1">
					<xsl:value-of select="xslNsODExt:getDictRes('PartySearchEnterRequired')"/>
				</label>
				<label id="dataRequired2">
					<xsl:value-of select="xslNsODExt:getDictRes('PartySearchEnterRequiredPlus')"/>
				</label>
			</div>
			<div>
				<table id="searchDataTable" class="formTbl">
					<tr id="phoneData">
						<td class="">
							<label>
								<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumber')"/>
								<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
							</label>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchPhoneId</xsl:with-param>
									<xsl:with-param name="name">nSearchPhone</xsl:with-param>
									<xsl:with-param name="controlClass">partyPhoneField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="referenceData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('ReferenceNumber')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchReferenceId</xsl:with-param>
									<xsl:with-param name="name">nSearchReference</xsl:with-param>
									<xsl:with-param name="size">32</xsl:with-param>
									<xsl:with-param name="maxlength">32</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="licenseData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('LicenseNumber')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchLicenseId</xsl:with-param>
									<xsl:with-param name="name">nSearchLicense</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="maxlength">20</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="personNameData">
						<td class="" id="personName">
							<xsl:value-of select="xslNsODExt:getDictRes('LastName')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td id="personNameValue">
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">personPartyNameId</xsl:with-param>
									<xsl:with-param name="name">nPersonPartyName</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
									<xsl:with-param name="maxlength">60</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="organizationNameData">
						<td class="" id="organizationName">
							<xsl:value-of select="xslNsODExt:getDictRes('OrganizationName')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td id="organizationNameValue">
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">organizationPartyNameId</xsl:with-param>
									<xsl:with-param name="name">nOrganizationPartyName</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
									<xsl:with-param name="maxlength">60</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="agencyNameData">
						<td class="" id="agencyName">
							<xsl:value-of select="xslNsODExt:getDictRes('OrganizationOrLastName')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td id="agencyNameValue">
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">agencyPartyNameId</xsl:with-param>
									<xsl:with-param name="name">nAgencyPartyName</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
									<xsl:with-param name="maxlength">60</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="firstNameData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('FirstName')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchFirstNameId</xsl:with-param>
									<xsl:with-param name="name">nSearchFirstName</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">30</xsl:with-param>
									<xsl:with-param name="maxlength">60</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="middleNameData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('MiddleName')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchMiddleNameId</xsl:with-param>
									<xsl:with-param name="name">nSearchMiddleName</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">30</xsl:with-param>
									<xsl:with-param name="maxlength">60</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="dobData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('DOB')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchDOBId</xsl:with-param>
									<xsl:with-param name="name">nSearchDOB</xsl:with-param>
									<xsl:with-param name="type">date</xsl:with-param>
									<xsl:with-param name="dateFormat">
										<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">partyDateField</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="addressData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('Address')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchAddress1Id</xsl:with-param>
									<xsl:with-param name="name">nSearchAddress1</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
									<xsl:with-param name="maxlength">100</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="cityData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('City')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchCityId</xsl:with-param>
									<xsl:with-param name="name">nSearchCity</xsl:with-param>
									<xsl:with-param name="controlClass">partyTextField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="size">30</xsl:with-param>
									<xsl:with-param name="maxlength">100</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="stateData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('State')"/>
							<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchStateId</xsl:with-param>
									<xsl:with-param name="name">nSearchState</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option>
											<xsl:attribute name="value">
												<xsl:text></xsl:text>
											</xsl:attribute>
											<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
										</option>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_USSTATES']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Code"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="controlClass">partyBasicComboField</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="zipData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('ZipCode')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">searchZipCodeId</xsl:with-param>
									<xsl:with-param name="name">nSearchZipCode</xsl:with-param>
									<xsl:with-param name="controlClass">partyZipCodeField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
					<tr id="soundexData">
						<td class="">
							<xsl:value-of select="xslNsODExt:getDictRes('UsePhoneticSearch')"/>
						</td>
						<td>
							<div>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="type">checkbox</xsl:with-param>
									<xsl:with-param name="id">searchSoundexCheckBoxGroup</xsl:with-param>
									<xsl:with-param name="checkbox">
										<xsl:call-template name="buildCheckBoxObject">
											<xsl:with-param name="fieldID">searchSoundexId</xsl:with-param>
											<xsl:with-param name="name">searchSoundex</xsl:with-param>
											<xsl:with-param name="inputValue">
												<xsl:text>1</xsl:text>
											</xsl:with-param>
											<xsl:with-param name="dctClassName">dctpartycheckboxfield</xsl:with-param>
											<xsl:with-param name="boxLabel">
												<xsl:text>false</xsl:text>
											</xsl:with-param>
											<xsl:with-param name="checked">
												<xsl:text>false</xsl:text>
											</xsl:with-param>
											<xsl:with-param name="readOnly">1</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_selectedPartyId" id="_selectedPartyId" value=""/>
		<input type="hidden" name="_searchType" id="_searchTypee" value=""/>
		<input type="hidden" name="_soundexIndicator" id="_soundexIndicator" value=""/>
		<input type="hidden" name="_address1" id="_address1" value=""/>
		<input type="hidden" name="_city" id="_city" value=""/>
		<input type="hidden" name="_stateCode" id="_stateCode" value=""/>
		<input type="hidden" name="_postalCode" id="_postalCode" value=""/>
		<input type="hidden" name="_firstName" id="_firstName" value=""/>
		<input type="hidden" name="_middleName" id="_middleName" value=""/>
		<input type="hidden" name="_partyTypeCode" id="_partyTypeCode" value=""/>
		<input type="hidden" name="_name" id="_name" value=""/>
		<input type="hidden" name="_dob" id="_dob" value=""/>
		<input type="hidden" name="_licenseNumber" id="_licenseNumber" value=""/>
		<input type="hidden" name="_phoneNumber" id="_phoneNumber" value=""/>
		<input type="hidden" name="_referenceNumber" id="_referenceNumber" value=""/>
		<input type="hidden" name="_nameSearchType" id="_nameSearchType" value=""/>
		<input type="hidden" name="_listStart" id="_listStart" value=""/>
		<input type="hidden" name="_listLength" id="_listLength" value=""/>
	</xsl:template>
</xsl:stylesheet>