﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('NewNotification')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction"></xsl:with-param>
		</xsl:call-template>
		<div id="requiredTxt">
			<span class="required">
				<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
			</span>
			<xsl:value-of select="xslNsODExt:getDictRes('IndicatesRequiredField')"/>
		</div>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildNewMessageHiddenFormFields"/>
		<xsl:call-template name="buildSendToGroup"/>
		<xsl:call-template name="buildNewMessageFields"/>
		<div id="messageActions" class="g-btn-bar">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">newMessageAnchor</xsl:with-param>
				<xsl:with-param name="id">newMessageAnchor</xsl:with-param>
				<xsl:with-param name="class">btnDisabled</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitMessage('</xsl:text>
					<xsl:value-of select="/page/state/previousPage"/>
					<xsl:text>');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Send')"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">cancelAnchor</xsl:with-param>
				<xsl:with-param name="id">cancelAnchor</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitPage('</xsl:text>
					<xsl:value-of select="/page/state/previousPage"/>
					<xsl:text>');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildNewMessageFields">
		<div id="newMessageFields">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('MessageDetails')"/>
			</h2>
			<xsl:if test="normalize-space(/page/content/messages/@policyNumber)!=''">
				<div id="policyQuoteLink">
					<xsl:value-of select="xslNsODExt:getDictRes('PolicyQuoteNumSymbol_colon')"/>
					<xsl:text> </xsl:text>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">gotoPolicyLink</xsl:with-param>
						<xsl:with-param name="id">gotoPolicyLink</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.gotoPageForQuote('policy','</xsl:text>
							<xsl:value-of select="/page/content/messages/@quoteID"/>
							<xsl:text>');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption" select="/page/content/messages/@policyNumber"/>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
			<div id="typeField" class="">
				<div class="">
					<label id="messageTypeLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Type_colon')"/>
						<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
					</label>
				</div>
				<div class="">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">messageType</xsl:with-param>
						<xsl:with-param name="name">_messageType</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_MESSAGETYPE']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="controlClass">messagetypeComboField</xsl:with-param>
						<xsl:with-param name="width">125</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="subjectText">
				<div class="">
					<label id="messageSubjectLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Subject_colon')"/>
					</label>
				</div>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">subject</xsl:with-param>
					<xsl:with-param name="name">_subject</xsl:with-param>
					<xsl:with-param name="size">40</xsl:with-param>
					<xsl:with-param name="maxlength">50</xsl:with-param>
				</xsl:call-template>
			</div>
			<div id="messageText">
				<div class="">
					<label id="messageTextLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Message_colon')"/>
						<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
					</label>
				</div>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">body</xsl:with-param>
					<xsl:with-param name="name">_body</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>
					<xsl:with-param name="rows">18</xsl:with-param>
					<xsl:with-param name="width">450</xsl:with-param>
					<xsl:with-param name="controlClass">messagebodyTextAreaField</xsl:with-param>
					<xsl:with-param name="required">1</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildSendToGroup">
		<div id="sendToGroup">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('SendTo')"/>
			</h2>
			<div class="">
				<label id="messageTextLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('SelectRecipientsOfMessage')"/>
					<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
				</label>
			</div>
			<div id="radioGroupContainerId" class="radioGroupContainer">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="type">radio</xsl:with-param>
					<xsl:with-param name="id">entitiesRadioGroup</xsl:with-param>
					<xsl:with-param name="columns">1</xsl:with-param>
					<xsl:with-param name="required">1</xsl:with-param>
					<xsl:with-param name="radiobuttons">
						<xsl:text>{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_allUsersOption</xsl:with-param>
							<xsl:with-param name="name">_sendToOption</xsl:with-param>
							<xsl:with-param name="value">all</xsl:with-param>
							<xsl:with-param name="dctClassName">dctsendtoradiofield</xsl:with-param>
							<xsl:with-param name="checked">true</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('AllUsers')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_selectedUsersOption</xsl:with-param>
							<xsl:with-param name="name">_sendToOption</xsl:with-param>
							<xsl:with-param name="value">users</xsl:with-param>
							<xsl:with-param name="dctClassName">dctsystemmessageradiofield</xsl:with-param>
							<xsl:with-param name="checked">false</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('SelectedUsers')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_selectedEntitiesOption</xsl:with-param>
							<xsl:with-param name="name">_sendToOption</xsl:with-param>
							<xsl:with-param name="value">entities</xsl:with-param>
							<xsl:with-param name="dctClassName">dctsystemmessageradiofield</xsl:with-param>
							<xsl:with-param name="checked">false</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('SelectedEntities')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_selectedEntityGroupsOption</xsl:with-param>
							<xsl:with-param name="name">_sendToOption</xsl:with-param>
							<xsl:with-param name="value">entityGroup</xsl:with-param>
							<xsl:with-param name="dctClassName">dctsystemmessageradiofield</xsl:with-param>
							<xsl:with-param name="checked">false</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('SelectedEntityLabels')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>}</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="vertical">true</xsl:with-param>
				</xsl:call-template>
			</div>
			<xsl:call-template name="buildUserGrids"/>
		</div>
	</xsl:template>
	<xsl:template name="buildUserGrids">
		<div id="sendToGrids">
			<div id="users" style="display:none">
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="gridID">
						<xsl:text>userList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="dataOnLoad">
						<xsl:text>false</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>messageUserList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>10</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>Users/@count</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>User</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@id</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@fullName'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T("User"), dataIndex: 'name', width: 250, sortable: false}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="toolBarTitle">
						<xsl:text>DCT.T('SelectDesiredUsers')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="multipleSelect">true</xsl:with-param>
					<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
					<xsl:with-param name="showToolBar">false</xsl:with-param>
					<xsl:with-param name="deSelectEventFunc">
						<xsl:text>DCT.Grid.senderItemDeselected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="selectEventFunc">
						<xsl:text>DCT.Grid.senderItemSelected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="width">500</xsl:with-param>
					<xsl:with-param name="displayMsg">DCT.T('DynamicRange')</xsl:with-param>
				</xsl:call-template>
			</div>
			<div id="entities" style="display:none">
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="gridID">
						<xsl:text>entitiesList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="dataOnLoad">
						<xsl:text>false</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>messageUserList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>10</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>Entities/@count</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>Entity</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@id</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@name'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T("Entity"), dataIndex: 'name', width: 250, sortable: false}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="toolBarTitle">
						<xsl:text>DCT.T('SelectDesiredEntity')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="multipleSelect">true</xsl:with-param>
					<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
					<xsl:with-param name="showToolBar">false</xsl:with-param>
					<xsl:with-param name="deSelectEventFunc">
						<xsl:text>DCT.Grid.senderItemDeselected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="selectEventFunc">
						<xsl:text>DCT.Grid.senderItemSelected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="width">500</xsl:with-param>
					<xsl:with-param name="displayMsg">DCT.T('DynamicRange')</xsl:with-param>
				</xsl:call-template>
			</div>
			<div id="entityGroup" style="display:none">
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="gridID">
						<xsl:text>entityGroupList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="dataOnLoad">
						<xsl:text>false</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>messageUserList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>20</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>GroupTypes/@count</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>GroupType</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@id</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@name'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T("EntityLabel"), dataIndex: 'name', width: 250, sortable: false}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="toolBarTitle">
						<xsl:text>DCT.T('SelectDesiredEntityLabel')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="multipleSelect">true</xsl:with-param>
					<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
					<xsl:with-param name="showToolBar">false</xsl:with-param>
					<xsl:with-param name="deSelectEventFunc">
						<xsl:text>DCT.Grid.senderItemDeselected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="selectEventFunc">
						<xsl:text>DCT.Grid.senderItemSelected</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="width">500</xsl:with-param>
					<xsl:with-param name="displayMsg">DCT.T('DynamicRange')</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildNewMessageHiddenFormFields">
		<input type="hidden" name="_sender" id="_sender" value="{/page/content/state/user/name}"/>
		<input type="hidden" name="_messageID" id="_messageID" value="-1"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{/page/content/messages/@quoteID}"/>
		<input type="hidden" name="_clientID" id="_clientID" value="{/page/content/messages/@clientID}"/>
		<input type="hidden" id="_previousPage" name="_previousPage" value="{/page/content/previousPage/@name}"/>
		<input type="hidden" id="_pageType" name="_pageType" value=""/>
	</xsl:template>
</xsl:stylesheet>