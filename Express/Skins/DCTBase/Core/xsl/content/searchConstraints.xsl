﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\dctContainer.xsl"/>	
	<xsl:import href="..\common\contentControls.xsl"/>
	<xsl:import href="..\common\dctNonInterviewControls.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:import href="..\common\dctGenericSetFieldFormat.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="searchSettings"/>
	</xsl:template>
</xsl:stylesheet>
