﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('NewAgencySignup')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<p id="greetingText">
					<xsl:value-of select="xslNsODExt:getDictRes('ThankJoin_CompleteForm_Contact')"/>
				</p>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="signupWidget"/>
	</xsl:template>

	<xsl:template name="signupWidget">
		<xsl:call-template name="signupUserInformationDialog"/>
		<xsl:call-template name="signupAgencyInformationDialog"/>
	</xsl:template>

	<xsl:template name="signupUserInformationDialog">
		<div id="userInformationDialog" class="downLayout">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('UserInformation')"/>
			</h2>
			<div id="userName" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="userNameLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Username_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">userNameId</xsl:with-param>
						<xsl:with-param name="name">_userName</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
						<xsl:with-param name="maxlength">50</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="fullName" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="fullNameLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('FullName_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">fullNameId</xsl:with-param>
						<xsl:with-param name="name">_fullName</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="email" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="emailLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Email_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">emailId</xsl:with-param>
						<xsl:with-param name="name">_email</xsl:with-param>
						<xsl:with-param name="controlClass">systemEmailField</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="signupAgencyInformationDialog">
		<div id="userInformationDialog" class="downLayout">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('AgencyInformation')"/>
			</h2>
			<div id="agencyName" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="agencyNameLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('AgencyName_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">nameId</xsl:with-param>
						<xsl:with-param name="name">_name</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="parentAgency" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="parentAgencyLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('ParentAgency_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">parentAgencyId</xsl:with-param>
						<xsl:with-param name="name">_parentAgency</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<label id="countryLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Country_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">countryId</xsl:with-param>
						<xsl:with-param name="name">_country</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectCountry')"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">countryComboField</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectCountry')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueUS')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextUS')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueUK')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextUK')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueAU')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextAU')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueCA')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextCA')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueES')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextES')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueGU')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextGU')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueHK')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextHK')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueIE')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextIE')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueMO')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextMO')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueNZ')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextNZ')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValuePR')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextPR')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueSG')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextSG')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="xslNsODExt:getDictRes('CountryValueAE')"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('CountryTextAE')"/>
							</option>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="contact" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="contactLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('ContactName_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">contactId</xsl:with-param>
						<xsl:with-param name="name">_contact</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="phoneExtension" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="phoneExtensionLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Phone_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">phoneId</xsl:with-param>
						<xsl:with-param name="name">_phone</xsl:with-param>
						<xsl:with-param name="controlClass">systemPhoneField</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<label id="extensionLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('PhoneExtension_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">extensionId</xsl:with-param>
						<xsl:with-param name="name">_extension</xsl:with-param>
						<xsl:with-param name="size">4</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="address" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="addressLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Address_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">address1Id</xsl:with-param>
						<xsl:with-param name="name">_address1</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="address2" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="address2Label">
						<xsl:value-of select="xslNsODExt:getDictRes('AddressCont')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">address2Id</xsl:with-param>
						<xsl:with-param name="name">_address2</xsl:with-param>
						<xsl:with-param name="size">50</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="cityStateZip" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="cityStateZipLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('City_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cityId</xsl:with-param>
						<xsl:with-param name="name">_city</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<label id="stateLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('State_colon_address')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">stateId</xsl:with-param>
						<xsl:with-param name="name">_state</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<label id="zipLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Zip_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">zipId</xsl:with-param>
						<xsl:with-param name="name">_zip</xsl:with-param>
						<xsl:with-param name="controlClass">systemZipCodeField</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup" style="padding:10px 0 0 0;">
				<div class="g-btn-bar">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">signupA</xsl:with-param>
						<xsl:with-param name="id">signupA</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.validateSignup();</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Submit')"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">loginA</xsl:with-param>
						<xsl:with-param name="id">loginA</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.submitPage('login');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
						</xsl:with-param>
						<xsl:with-param name="type">cancel</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>