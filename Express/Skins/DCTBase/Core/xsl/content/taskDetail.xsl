﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:import href="..\common\contentInterviewPage.xsl"/>
    <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
    <!--	*************************************************************************************************************
		Global Variables		
		************************************************************************************************************* -->
    <xsl:variable name="helpImage"/>
    <xsl:template match="/">
        <xsl:call-template name="buildContainer"/>
    </xsl:template>
    <!--	*************************************************************************************************************
		This template is for processing the interview response and producing the overall structure of the HTML page.
		************************************************************************************************************* -->
    <xsl:template name="buildPageContent">
        <xsl:call-template name="buildCommonLayout">
            <xsl:with-param name="screenName">
                <xsl:text>taskDetail</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
	
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="buildTabs">
			<xsl:with-param name="cssClass">
				<xsl:choose>
					<xsl:when test="not($showHeader)">pageNavNoHeader</xsl:when>
					<xsl:otherwise>pageNav</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

    <xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="/page/content/getPage/body/@caption" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
    </xsl:template>

    <xsl:template name="fieldDetail">
        <!-- do nothing -->
    </xsl:template>
  
    <xsl:template name="buildHiddenDetailFields">
		<input type="hidden" name="_taskId" id="_taskId" value="{/page/content/ExpressCache/ExtendedTaskContent/@taskId}"/>
		<input type="hidden" name="_returnPage" id="_returnPage" value="{/page/content/ExpressCache/ExtendedTaskContent/@returnPage}"/>
    <input type="hidden" name="_QuoteID" id="_QuoteID" value="{/page/content/ExpressCache/ExtendedTaskContent/@objectId}"/>
		<input type="hidden" name="_objectId" id="_objectId" value="{/page/content/ExpressCache/ExtendedTaskContent/@objectId}"/>
		<input type="hidden" name="_objectTypeCode" id="_objectTypeCode" value="{/page/content/ExpressCache/ExtendedTaskContent/@objectTypeCode}"/>
		<input type="hidden" name="_detailMode" id="_detailMode" value="{/page/content/ExpressCache/ExtendedTaskContent/@detailMode}"/>
    </xsl:template>
    
	<xsl:template name="buildBasicHiddenTaskDetailFields">
		
	</xsl:template>
	
</xsl:stylesheet>