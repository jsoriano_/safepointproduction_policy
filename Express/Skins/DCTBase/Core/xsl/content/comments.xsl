﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:variable name="helpImage"/>
	<xsl:variable name="cultureFormat">
		<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
	</xsl:variable>
	<xsl:variable name="quoteLinkValue">
		<xsl:if test="/page/state/previousPage = 'policy'">
			<xsl:value-of select="/page/state/quoteID"/>
		</xsl:if>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('Notes_noColon')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildMainAreaContent"/>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{$quoteLinkValue}"/>
		<input type="hidden" name="_showBody" id="_showBody" value="1"/>
		<input type="hidden" name="_hiddenDisplayDtMask" id="_hiddenDisplayDtMask" value="{xslNsExt:formatMaskToUse('', '', '1')}"/>		
	</xsl:template>
	<xsl:template name="buildMainAreaContent">
		<xsl:call-template name="commentsListWidget"/>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="pageSpecificActions"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="pageSpecificActions">
		<div id="subactiongroup">
			<ul id="subMenuActions">	
			</ul>
		</div>
	</xsl:template>
	<xsl:template name="commentsListWidget">
		<input type="hidden" name="_typeOfSearchComments" id="_typeOfSearch" value="comments"/>
		<div id="commentsFilter">
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">commentFilter</xsl:with-param>
				<xsl:with-param name="name">_commentFilter</xsl:with-param>
				<xsl:with-param name="type">select</xsl:with-param>
				<xsl:with-param name="optionlist">
					<option value="A">
						<xsl:value-of select="xslNsODExt:getDictRes('ViewAllNotes')"/>
					</option>
					<option value="O">
						<xsl:value-of select="xslNsODExt:getDictRes('ViewOriginatorNotes')"/>
					</option>
					<option value="D">
						<xsl:value-of select="xslNsODExt:getDictRes('ViewDeletedNotes')"/>
					</option>
				</xsl:with-param>
				<xsl:with-param name="controlClass">commentsFilterComboField</xsl:with-param>
				<xsl:with-param name="value">A</xsl:with-param>
				<xsl:with-param name="extraConfigItems">
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctGridId</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>comments</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
		<xsl:call-template name="buildCommentsListGrid"/>
	</xsl:template>
	<xsl:template name="buildCommentsListGrid">
			<xsl:variable name="cultureFormat">
				<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
			</xsl:variable>
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>comments</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">DCT.T('Notes_noColon')</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>commentsList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>10</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>comments/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>comment</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@commentId</xsl:text>
				</xsl:with-param>
        <xsl:with-param name="className">notesPagingGridPanel</xsl:with-param>
        <xsl:with-param name="classMethod">DCT.Util.refreshNotes</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'creationDateTime', type: 'date', dateFormat: 'Y-m-d H:i', mapping: '@creationDateTime'},</xsl:text>
					<xsl:text>{name: 'originator', type: 'string', mapping: '@originator'},</xsl:text>
					<xsl:text>{name: 'commentTypeCode', type: 'string', mapping: '@commentTypeCode'},</xsl:text>
					<xsl:text>{name: 'commentCode', type: 'string', mapping: '@commentCode'},</xsl:text>					
				<xsl:text>{name: 'commentLockingTS', type: 'string', mapping: '@commentLockingTS'},</xsl:text>
				<xsl:text>{name: 'internalAccess', type: 'string', mapping: '@internalAccess'},</xsl:text>
				<xsl:text>{name: 'deleteAccess', type: 'string', mapping: '@deleteAccess'},</xsl:text>
				<xsl:text>{name: 'objectId', type: 'string', mapping: '@objectId'},</xsl:text>
				<xsl:text>{name: 'objectTypeCode', type: 'string', mapping: '@objectTypeCode'},</xsl:text>
					<xsl:text>{name: 'historyId', type: 'string', mapping: '@historyId'},</xsl:text>					
				<xsl:text>{name: 'policyNumber', type: 'string', mapping: '@policyNumber'},</xsl:text>
					<xsl:text>{name: 'policyStatus', type: 'string', mapping: '@policyStatus'},</xsl:text>
					<xsl:text>{name: 'type', type: 'string', mapping: '@type'},</xsl:text>
					<xsl:text>{name: 'transactionStatus', type: 'string', mapping: '@transactionStatus'},</xsl:text>					
					<xsl:text>{name: 'description', type: 'string', mapping: '@commentText'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T('Date'), dataIndex: 'creationDateTime', sortable: true, width: 35, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="$cultureFormat"/>
					<xsl:text>, g:i a')},</xsl:text>
					<xsl:text>{text: DCT.T('PolicyNumber'), dataIndex: 'policyNumber', sortable: true, width: 35, renderer: DCT.Grid.commentObjectColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('Note'), dataIndex: 'description', sortable: true, width: 120, renderer: DCT.Grid.viewPASCommentDescriptionColumnRender},</xsl:text>
					<xsl:text>{text: DCT.T('TransactionStatus'), dataIndex: 'policyStatus', sortable: true, width: 45, renderer: DCT.Grid.objectStatusColumnRenderer},</xsl:text>						
					<xsl:text>{text: DCT.T('CreatedBy'), dataIndex: 'originator', sortable: true, width: 35},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'commentTypeCode', width: 23, sortable: true, renderer: DCT.Grid.commentTypeColumnRenderer},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'originator', sortable: false, width: 23, renderer: DCT.Grid.commentViewColumnRenderer},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'originator', sortable: false, width: 23, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.commentDeleteColumnRenderer('commentsList'</xsl:text>
					<xsl:text>, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); } }]</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>