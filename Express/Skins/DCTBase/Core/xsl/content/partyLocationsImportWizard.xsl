﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:import href="partyMappingActions.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<div id="pageTop">
			<div id="pageTitle">
				<xsl:value-of select="/page/content/recordActions/@caption" />
			</div>
		</div>
		<div id="fields">
			<xsl:attribute name="class">
				<xsl:text>styleWithOutPanels</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="/page/content/addMappedRecord" />
		</div>
	</xsl:template>
</xsl:stylesheet>
