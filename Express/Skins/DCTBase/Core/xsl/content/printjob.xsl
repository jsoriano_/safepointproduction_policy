﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:variable name="ReadOnly" select="page/content/printDocs/@readOnly"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyForms')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<!-- This param needs to be contained within a root element-->
				<div>
					<xsl:value-of select="xslNsODExt:getDictRes('SelectForms_Added_Remove')"/>
					<xsl:text> </xsl:text>
					<xsl:element name="img">
						<xsl:attribute name="src">
							<xsl:value-of select="$imageDir"/>
							<xsl:text>icons\decline.png</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="class">inlineImage</xsl:attribute>
					</xsl:element>
					<xsl:value-of select="xslNsODExt:getDictRes('PeriodEndSentence')"/>
				</div>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:param name="caption"/>
		<script>
			<xsl:text>DCT.Util.includeHeaderFile("</xsl:text>
			<xsl:value-of select="$cssDir"/>
			<xsl:text>x_printJob.css","css");</xsl:text>
			<xsl:if test="page/state/previousPage/text() and page/state/previousPage/text() != 'printJob'">
				<xsl:text>,"</xsl:text>
				<xsl:value-of select="$cssDir"/>
				<xsl:text>x_</xsl:text>
				<xsl:value-of select="page/state/previousPage/text()"/>
				<xsl:text>.css");</xsl:text>
			</xsl:if>
		</script>
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPageHeaderSection"/>
		<xsl:call-template name="printJobListWidget"/>
		<xsl:call-template name="buildPageAdditionalActions"/>
	</xsl:template>

	<xsl:template name="printJobListWidget">
		<xsl:call-template name="buildPrintJobListActions"/>
		<xsl:call-template name="createPrintJobListGrid"/>
	</xsl:template>

	<xsl:template name="createPrintJobListGrid">
		<div id="printJobList">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>printJobList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="className">
					<xsl:text>printJobGridPanel</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>printJobList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>10</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>printDocs/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>printDoc</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@name</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'printjob', type: 'boolean', mapping: '@printjob'},</xsl:text>
					<xsl:text>{name: 'onpolicy', type: 'boolean', mapping: '@onpolicy'},</xsl:text>
					<xsl:text>{name: 'onPolicyStatus', type: 'string', mapping: '@onPolicyStatus'},</xsl:text>
					<xsl:text>{name: 'name', type: 'string', mapping: '@name'},</xsl:text>
					<xsl:text>{name: 'caption', type: 'string', mapping: '@caption'},</xsl:text>
					<xsl:text>{name: 'type', type: 'string', mapping: '@type'},</xsl:text>
					<xsl:text>{name: 'staysWithPolicy', type: 'string', mapping: '@staysWithPolicy'},</xsl:text>
					<xsl:text>{name: 'category', type: 'string', mapping: '@category'},</xsl:text>
					<xsl:text>{name: 'order', type: 'string', mapping: '@order'},</xsl:text>
					<xsl:text>{name: 'paperBinNum', type: 'string', mapping: '@paperBinNum'},</xsl:text>
					<xsl:text>{name: 'duplex', type: 'boolean', mapping: '@duplex'},</xsl:text>
					<xsl:text>{name: 'review', type: 'string', mapping: '@review'},</xsl:text>
					<xsl:text>{name: 'editStatus', type: 'string', mapping: '@editStatus'},</xsl:text>
					<xsl:text>{name: 'pageRef', type: 'string', mapping: '@pageRef'},</xsl:text>
					<xsl:text>{name: 'topicRef', type: 'string', mapping: '@topicRef'},</xsl:text>
					<xsl:text>{name: 'hasMerge', type: 'boolean', mapping: '@hasMerge'},</xsl:text>
					<xsl:text>{name: 'docManuscript', type: 'string', mapping: '@value'},</xsl:text>
					<xsl:text>{name: 'remove', type: 'boolean', mapping: '@remove'},</xsl:text>
					<xsl:text>{name: 'selected', type: 'string', mapping: '@selected'},</xsl:text>
					<xsl:text>{name: 'printDefault', type: 'string', mapping: '@printDefault'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T('Print'), width:'7%', dataIndex: 'selected', sortable: true, renderer: function() {return "";}},</xsl:text>
					<xsl:text>{text: DCT.T('OnPolicy_Question'), width:'12%', dataIndex: 'onpolicy', sortable: true, renderer: DCT.Grid.onPolicyColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('Form'), width:'15%', dataIndex: 'name', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('Title'), width:'20%', dataIndex: 'caption', sortable: true, renderer: DCT.Grid.titleColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('Category'), width:'10%', dataIndex: 'category', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('Order'), width:'7%', dataIndex: 'order', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('paperBinNum'), dataIndex: 'paperBinNum', sortable: true, hidden: true},</xsl:text>
					<xsl:text>{text: DCT.T('duplex'), dataIndex: 'duplex', sortable: true, hidden: true},</xsl:text>
					<xsl:text>{text: DCT.T('Reviewed'), width:'11%', dataIndex: 'review', sortable: true, renderer: DCT.Grid.printJobRowActionReviewColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('Edit'), width:'8%', dataIndex: 'editStatus', sortable: true, renderer: DCT.Grid.printJobRowActionEditColumnRenderer},</xsl:text>
					<xsl:text>{text: "", width:'13%', dataIndex: 'formAction', sortable: false, renderer: DCT.Grid.printJobRowActionColumnRenderer}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="sortColumn">
					<xsl:value-of select="/page/content/ExpressCache/PrintJobInfo/@sortColumn"/>
				</xsl:with-param>
				<xsl:with-param name="sortOrder">
					<xsl:value-of select="/page/content/ExpressCache/PrintJobInfo/@sortDirection"/>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
				<xsl:with-param name="showGridCheckboxwithButtons">true</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.deselectPrintJob</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="beforeRowSelectEventFunc">
					<xsl:text>DCT.Grid.checkPrintJob</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.selectPrintJob</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="checkboxRenderFunc">
					<xsl:text>DCT.Grid.printCheckBoxColumnRenderer</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="trackPageSelections">
					<xsl:text>false</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:text>"</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('FormsList')"/>
					<xsl:text>"</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="checkCurrentStartIndex">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="adjustedCountFunc">
					<xsl:text>DCT.Grid.printCheckBoxAdjustedCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="setRowSelectedFunc">
					<xsl:text>DCT.Grid.printSetRowSelection</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="checkClickedRowIndex">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showItemsPerPageCombo">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildPrintJobListActions">
		<div id="quickFilterShowType" class="downLayout">
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<label id="quickFilterLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Show_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">formsShow</xsl:with-param>
						<xsl:with-param name="name">_formsShow</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/printDocs/@showType"/>
						</xsl:with-param>
						<xsl:with-param name="optionlist">
							<xsl:element name="option">
								<xsl:attribute name="value">all</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('AllForms')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">onpolicy</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsOnThePolicy')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">printjob</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('AvailableForms')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">add</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsToBeAdded')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">remove</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsToBeRemoved')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">@editStatus!='3'</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsNeedToEdit')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">@editStatus='3'</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsAlreadyEdited')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">hasMerge</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('EditableForms')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">readOnly</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('ReadOnlyForms')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">@printDefault!='Mandatory' and  @onPolicyStatus!='1' and  @onPolicyStatus!='3'</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('AllOptionalForms')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">@selected='1' and (@printDefault='UnSelected' or @printDefault='0')</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('OptionalFormsDefaultUnselected')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">review</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsReviewed')"/>
							</xsl:element>
							<xsl:element name="option">
								<xsl:attribute name="value">@review='0'</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('FormsNeedsToReview')"/>
							</xsl:element>
						</xsl:with-param>
						<xsl:with-param name="controlClass">printListComboField</xsl:with-param>
						<xsl:with-param name="width">310</xsl:with-param>
						<xsl:with-param name="defaultValue">all</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
		<div id="quickFilter" class="downLayout">
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<label id="quickFilterLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Search_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">printJobRestrict</xsl:with-param>
						<xsl:with-param name="name">_printJobRestrict</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/printDocs/@restrict"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
		<div class="g-btn-bar searchActions">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">printJobListRestrictA</xsl:with-param>
				<xsl:with-param name="id">printJobListSearchButton</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Grid.setPrintJobRestrict(1);</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
				</xsl:with-param>
				<xsl:with-param name="type">search</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">printJobListClearRestrictionA</xsl:with-param>
				<xsl:with-param name="id">printJobListClearButton</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Grid.setPrintJobRestrict(0);</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Clear')"/>
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>

		<div class="g-btn-bar floatRight formActions">
			<!--LOB Priv-->
			<xsl:if test="/page/state/user/privileges/privilege[@name='Add'] and /page/state/user/privileges/privilege[@type='AdHocForms'] and /page/state/user/privileges/privilege[@value='1'] and /page/state/user/privileges/privilege[@lob=/page/state/activeLOB or @lob='*'] and ($ReadOnly != '1')">
				<!--Role Priv-->
				<xsl:if test="/page/state/user/privileges/privilege[@name='AdHocAddForm']">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">printJobAddAdHocForm</xsl:with-param>
						<xsl:with-param name="id">printJobAddAdHocFormButton</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Grid.displayAddFormPopUp();</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('AddForm')"/>
						</xsl:with-param>
						<xsl:with-param name="type">add</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
			<xsl:if test="($ReadOnly != '1')">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">printJobListDefaultsAllA</xsl:with-param>
					<xsl:with-param name="id">printJobListDefaultsAllA</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Grid.setPrintJobBulk();</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('ResetSelections')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</div>
	</xsl:template>

	<xsl:template name="buildPageAdditionalActions">
		<div id="pageAdditionalActions">
			<xsl:if test="not(page/content/printDocs/@autoRefresh = 1)">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">resetPrintJobDetaulsA</xsl:with-param>
					<xsl:with-param name="id">resetPrintJobDetaulsA</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Grid.submitPageRefresh();</xsl:with-param>
					<xsl:with-param name="class">
						<xsl:if test="($ReadOnly = '1')">
							<xsl:text> btnDisabled</xsl:text>
						</xsl:if>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Refresh')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">returnToPolicyA</xsl:with-param>
				<xsl:with-param name="id">returnToPolicyA</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Submit.submitAction('closePrintJob');</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('ReturnToPolicy')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_readOnly" id="_readOnly" value="{page/content/printDocs/@readOnly}"/>
		<input type="hidden" name="_autoRefresh" id="_autoRefresh" value="{page/content/printDocs/@autoRefresh}"/>
		<input type="hidden" name="_printJobBulk" id="_printJobBulk"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{/page/state/quoteID}"/>
		<input type="hidden" name="_ClientID" id="_ClientID" value="{/page/content/details/client/@id}"/>
		<input type="hidden" name="_printJobName" id="_printJobName" value="{/page/content/printDocs/@name}"/>
		<input type="hidden" name="_printJobOrder" id="_printOrder" value="{/page/content/printDocs/@listCount}"/>
		<input type="hidden" name="_attachID" id="_attachID" value="{/page/content/attach/@id}"/>
		<input type="hidden" name="_attachmentID" id="_attachmentID" value="0"/>

		<xsl:call-template name="AdHocPrivilegeCheck">
			<xsl:with-param name="name">Add</xsl:with-param>
			<xsl:with-param name="roleName">AdHocAddForm</xsl:with-param>
			<xsl:with-param name="privilege">_canAdd</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="AdHocPrivilegeCheck">
			<xsl:with-param name="name">Reorder</xsl:with-param>
			<xsl:with-param name="roleName">AdHocReorderForm</xsl:with-param>
			<xsl:with-param name="privilege">_canReorder</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="AdHocPrivilegeCheck">
			<xsl:with-param name="name">Delete</xsl:with-param>
			<xsl:with-param name="roleName">AdHocDeleteForm</xsl:with-param>
			<xsl:with-param name="privilege">_canDelete</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="AdHocPrivilegeCheck">
			<xsl:with-param name="name">Download</xsl:with-param>
			<xsl:with-param name="roleName">AdHocDownloadForm</xsl:with-param>
			<xsl:with-param name="privilege">_canDownload</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="AdHocPrivilegeCheck">
			<xsl:with-param name="name">Replace</xsl:with-param>
			<xsl:with-param name="roleName">AdHocReplaceForm</xsl:with-param>
			<xsl:with-param name="privilege">_canReplace</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="AdHocPrivilegeCheck">
		<xsl:param name="name"/>
		<xsl:param name="roleName"/>
		<xsl:param name="privilege"/>

		<xsl:choose>
			<xsl:when test="/page/state/user/privileges/privilege[@name=$name and @type='AdHocForms' and @value='1' and (@lob=/page/state/activeLOB or @lob='*')]">
				<xsl:if test="/page/state/user/privileges/privilege[@name=$roleName]">
					<xsl:element name="input">
						<xsl:attribute name="type">hidden</xsl:attribute>
						<xsl:attribute name="name">
							<xsl:value-of select="$privilege"/>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:value-of select="$privilege"/>
						</xsl:attribute>
						<xsl:attribute name="value">1</xsl:attribute>
					</xsl:element>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="input">
					<xsl:attribute name="type">hidden</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$privilege"/>
					</xsl:attribute>
					<xsl:attribute name="id">
						<xsl:value-of select="$privilege"/>
					</xsl:attribute>
					<xsl:attribute name="value">0</xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>