﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<!--	*************************************************************************************************************
		Global Variables		
		************************************************************************************************************* -->
	<xsl:variable name="helpImage"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for processing the interview response and producing the overall structure of the HTML page.
		************************************************************************************************************* -->
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildCommonLayout">
			<xsl:with-param name="screenName">
				<xsl:text>editPrint</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:choose>
					<xsl:when test="normalize-space(/page/content/getPage/body/@caption)!=''">
						<xsl:value-of select="/page/content/getPage/body/@caption"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsODExt:getDictRes('EditPrintJobPageTitle')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:if test="(/page/content//errors)">
					<p>
						<xsl:value-of select="xslNsODExt:getDictRes('ErrorOccuredInterviewCheckInputs')"/>
					</p>
				</xsl:if>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildEditDetailButtons">
		<xsl:param name="fieldID">reviewedValue</xsl:param>
		<xsl:param name="readOnly"/>
		<xsl:param name="name">_reviewedValue</xsl:param>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="fieldOtherParms"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="actionID"/>
		<xsl:param name="value">
			<xsl:value-of select="/page/content/getPage/@reviewValue"></xsl:value-of>
		</xsl:param>
		<xsl:param name="boxLabel">
			<xsl:value-of select="xslNsODExt:getDictRes('Reviewed')"/>
		</xsl:param>
		<div>
			<xsl:call-template name="buildCheckBoxDiv">
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="readOnly" select="$readOnly"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="checkbox">
					<xsl:call-template name="buildCheckBoxObject">
						<xsl:with-param name="actionID" select="$actionID"/>
						<xsl:with-param name="fieldID" select="$fieldID"/>
						<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
						<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
						<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="dctClassName">dctcheckboxfield</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readOnly"/>
						<xsl:with-param name="boxLabel" select="$boxLabel"/>
						<xsl:with-param name="checked">
							<xsl:choose>
								<xsl:when test="$value!='0' and $value!=''">
									<xsl:text>true</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>false</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
		<div class="g-groupAction">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">previewForm</xsl:with-param>
				<xsl:with-param name="id">previewForm</xsl:with-param>
				<xsl:with-param name="href">
					<xsl:text>javascript:if (DCT.Util.validateFields()) DCT.Submit.submitPreviewAction('editPrintJob','</xsl:text>
					<xsl:value-of select="/page/content/getPage/@reviewFormName"/>
					<xsl:text>|</xsl:text>
					<xsl:value-of select="/page/content/getPage/body/@manuscript"/>
					<xsl:text>');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Preview')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
		<div class="g-groupAction">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">returnToList</xsl:with-param>
				<xsl:with-param name="id">returnToList</xsl:with-param>
				<xsl:with-param name="href">
					<xsl:choose>
						<xsl:when test="/page/content/getPage/@printJobListAutoRefresh = '1'">
							<xsl:text>javascript:if (DCT.Util.validateFields()) DCT.Submit.submitPageAction('printJob',DCT.Submit.getReviewAction());</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>javascript:if (DCT.Util.validateFields()) DCT.Submit.submitPage('printJob');</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('ReturnToList')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenDetailFields">
		<input type="hidden" name="_printJobListAutoRefresh" id="_printJobListAutoRefresh">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@printJobListAutoRefresh"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_printJobListStartIndex" id="_printJobListStartIndex">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@printJobListStartIndex"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_printJobForm">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@formName"/>|<xsl:value-of select="/page/content/getPage/properties/@manuscriptID"/></xsl:attribute>
		</input>
		<input type="hidden" name="_startIndex">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@startIndex"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_displayCount">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@displayCount"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_orderBy">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@orderBy"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_direction">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@orderDirection"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_printJobRestrict">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@restrict"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_reviewFormName">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@reviewFormName"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_reviewValue" id="reviewValueOld">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@reviewValue"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_previewFormName" id="previewFormName">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@previewFormName"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_tempManuScriptID" id="tempManuScriptID">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/getPage/@tempManuScriptID"/>
			</xsl:attribute>
		</input>
	</xsl:template>
</xsl:stylesheet>