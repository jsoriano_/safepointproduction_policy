﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('NotificationDetails')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildMessageDetailHiddenFormFields"/>
		<xsl:call-template name="messageDetailWidget"/>
	</xsl:template>
	<xsl:template name="messageDetailWidget">
		<xsl:call-template name="buildMessageDetailDialog"/>
		<xsl:call-template name="buildMessageDetailActions"/>
	</xsl:template>
	<xsl:template name="buildMessageDetailActions">
		<xsl:if test="/page/content/messages/message/@deleted!='1'">
			<div class="g-btn-bar" id="notificationDetailsActions">
				<xsl:choose>
					<xsl:when test="$canDeleteNotifications = '1'">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">messageDetailDeleteMessageA</xsl:with-param>
							<xsl:with-param name="id">messageDetailDeleteMessageA</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.actOnMessage(</xsl:text>
								<xsl:value-of select="/page/content/messages/message/@messageID"/>
								<xsl:text>, 'deleteMessage');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('DeleteThisNotification')"/>
							</xsl:with-param>
							<xsl:with-param name="type">cancel</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">cancelA</xsl:with-param>
					<xsl:with-param name="id">cancelA</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.submitPage('</xsl:text>
						<xsl:value-of select="/page/state/previousPage"/>
						<xsl:text>');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Back')"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildMessageDetailDialog">
		<div class="downLayout">
			<div id="fromField" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="messageFromLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('From_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:value-of select="/page/content/messages/message/fullname"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('On_lowercase')"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/messages/message/sentOn)"/>
				</div>
			</div>
			<div id="toField" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="messageToLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('To_colon')"/>
					</label>
				</div>
				<div class="downFormField">
					<xsl:for-each select="/page/content/messages/recipients/recipient[@name='userID']">
						<xsl:value-of select="@fullName"/>
						<xsl:if test="position() &lt; last()">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="/page/content/messages/recipients/recipient[@name='entityID']">
						<xsl:value-of select="@fullName"/>
						<xsl:if test="position() &lt; last()">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="/page/content/messages/recipients/recipient[@name='entityGroupID']">
						<xsl:value-of select="@fullName"/>
						<xsl:if test="position() &lt; last()">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:for-each select="/page/content/messages/recipients/recipient[@name='global']">
						<xsl:text>All Users</xsl:text>
					</xsl:for-each>
				</div>
			</div>
			<xsl:if test="(/page/content/messages/@policyLink &gt; 0) and (/page/content/messages/message/policyID &gt; 0)">
				<div id="policyField" class="downFieldGroup">
					<div class="downFormLabel">
						<label id="messageTypeLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('PolicyQuoteNumSymbol_colon')"/>
						</label>
					</div>
					<div class="downFormField">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">messageDetailBackToPolicyA</xsl:with-param>
							<xsl:with-param name="id">messageDetailBackToPolicyA</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
								<xsl:value-of select="/page/content/messages/message/policyID"/>
								<xsl:text>','</xsl:text>
								<xsl:value-of select="/page/content/messages/message/LOB"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:choose>
									<xsl:when test="string-length(/page/content/messages/message/policyNumber) &gt; 0">
										<xsl:value-of select="/page/content/messages/message/policyNumber"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsODExt:getDictRes('Quote')"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
			</xsl:if>
			<xsl:if test="/page/content/messages/message/@deleted='1'">
				<div id="deletedField" class="downFieldGroup">
					<div class="downFormLabel">
						<label id="messageTypeLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Deleted_colon')"/>
						</label>
					</div>
					<div class="downFormField">
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/messages/message/purgeDate)"/>
					</div>
				</div>
			</xsl:if>
			<xsl:if test="count(/page/content/messages/message/attachments/attachment) &gt; 0">
				<div id="attachmentField" class="downFieldGroup">
					<div class="downFormLabel">
						<label id="messageTypeLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Attachments_colon')"/>
						</label>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="/page/content/messages/message/attachments/attachment">
								<table class="formTable" id="attachmentsTableId">
									<xsl:for-each select="/page/content/messages/message/attachments/attachment">
										<tr>
											<td>
												<xsl:call-template name="makeButton">
													<xsl:with-param name="name">downloadAttachment</xsl:with-param>
													<xsl:with-param name="id">downloadAttachment</xsl:with-param>
													<xsl:with-param name="onclick">
														<xsl:text>DCT.Submit.downloadAttachment('</xsl:text>
														<xsl:value-of select="@attachmentID"/>
														<xsl:text>', 'message');</xsl:text>
													</xsl:with-param>
													<xsl:with-param name="caption" select="@caption"/>
													<xsl:with-param name="type">hyperlink</xsl:with-param>
												</xsl:call-template>
											</td>
										</tr>
									</xsl:for-each>
								</table>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text></xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</xsl:if>
			<xsl:if test="not(/page/content/messages/message/subject = '')">
				<div id="fromField" class="downFieldGroup">
					<div class="downFormLabel">
						<label id="messageSubjectLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Subject_colon')"/>
						</label>
					</div>
					<div class="downFormField">
						<xsl:value-of select="/page/content/messages/message/subject"/>
					</div>
				</div>
			</xsl:if>
			<div id="attachmentField" class="downFieldGroup">
				<div class="downFormLabel">
					<label id="messageLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Message_colon')"/>
					</label>
				</div>
				<div id="messageBody" class="downFormField">
					<xsl:value-of select="/page/content/messages/message/body"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildMessageDetailHiddenFormFields">
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{/page/content/messages/@quoteID}"/>
		<input type="hidden" name="_ClientID" id="_ClientID" value="{/page/content/messages/@clientID}"/>
		<input type="hidden" name="_messageID" id="_messageID" value="{/page/content/messages/@messageID}"/>
	</xsl:template>
</xsl:stylesheet>