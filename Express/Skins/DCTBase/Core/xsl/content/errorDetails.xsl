﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ErrorDetails')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('ErrorTechnicalDetails')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<div id="errorDetail" class="downLayout">
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Date_colon')"/>
				</div>
				<div class="downFormField">
					<span>
						<pre>
							<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('MM/dd/yyyy h:mm tt', '', /page/content/ExceptionItemDetail/ExceptionDateTime)"/>
						</pre>
					</span>
				</div>
			</div>

			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Type_colon')"/>
				</div>
				<div class="downFormField">
					<span>
						<pre>
							<xsl:value-of select="/page/content/ExceptionItemDetail/ExceptionType"/>
						</pre>
					</span>
				</div>
			</div>

			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Reference')"/>
				</div>
				<div class="downFormField">
					<span>
						<pre>
							<xsl:value-of select="/page/content/ExceptionItemDetail/ExceptionReferenceId"/>
						</pre>
					</span>
				</div>
			</div>

			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Status_colon')"/>
				</div>
				<div class="downFormField">
					<span>
						<pre>
							<xsl:value-of select="/page/content/ExceptionItemDetail/ExceptionStatus"/>
						</pre>
					</span>
				</div>
			</div>
		</div>
		<br/>
		<div id="errorMessage" class="downLayout">
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('ErrorDescription')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">_errorMessageId</xsl:with-param>
						<xsl:with-param name="name">_errorMessageName</xsl:with-param>
						<xsl:with-param name="type">textarea</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/ExceptionItemDetail/ExceptionMessage"/>
						</xsl:with-param>
						<xsl:with-param name="rows">10</xsl:with-param>
						<xsl:with-param name="width">300</xsl:with-param>
						<xsl:with-param name="controlClass">systemTextAreaField</xsl:with-param>
						<xsl:with-param name="readonly">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
		<div id="errorPayload" class="downLayout">
			<div class="downFieldGroup">
				<div class="downFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Payload_colon')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">_errorPayloadId</xsl:with-param>
						<xsl:with-param name="name">_errorPayloadName</xsl:with-param>
						<xsl:with-param name="type">textarea</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:copy-of select="/page/content/ExceptionItemDetail/Payload"/>
						</xsl:with-param>
						<xsl:with-param name="rows">10</xsl:with-param>
						<xsl:with-param name="width">300</xsl:with-param>
						<xsl:with-param name="controlClass">systemTextAreaField</xsl:with-param>
						<xsl:with-param name="readonly">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>