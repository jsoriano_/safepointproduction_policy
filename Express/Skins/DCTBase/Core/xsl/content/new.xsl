﻿
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('CreateNewQuote')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<p id="newQuoteInfoText">
					<xsl:value-of select="xslNsODExt:getDictRes('SelectProduct_StartExisting')"/>
				</p>
			</xsl:with-param>
		</xsl:call-template>
		<div id="requiredTxt">
			<span class="required">
				<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
			</span>
			<xsl:value-of select="xslNsODExt:getDictRes('IndicatesRequiredField')"/>
		</div>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPageHeaderSection"/>
		<xsl:call-template name="productListWidget"/>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_QuoteID">
			<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="policy/@id">
						<xsl:value-of select="policy/@id"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_clientID" id="_clientID">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/content/session/data/root/client/@id"/>|<xsl:value-of select="/page/content/session/data/root/client"/></xsl:attribute>
		</input>
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value="0"/>
		<input type="hidden" name="_usingPortfolio" id="_usingPortfolio" value="0"/>
	</xsl:template>

	<xsl:template name="productListWidget">
		<xsl:variable name="agencyListCount" select="/page/content/agencyList/@listCount"/>
		<xsl:variable name="agencyListMax" select="/page/content/agencyListFilter/@maxItems"/>
		<xsl:variable name="agencyListFilter" select="/page/content/agencyListFilter/@filter"/>
		<xsl:call-template name="buildProductListHiddenFormFields">
			<xsl:with-param name="agencyListCount" select="$agencyListCount"/>
			<xsl:with-param name="agencyListFilter" select="$agencyListFilter"/>
		</xsl:call-template>
		<div id="agencyProducerFilterSection">
			<xsl:call-template name="buildAgencyProducerFilterOptions">
				<xsl:with-param name="agencyListCount" select="$agencyListCount"/>
				<xsl:with-param name="agencyListMax" select="$agencyListMax"/>
				<xsl:with-param name="agencyListFilter" select="$agencyListFilter"/>
			</xsl:call-template>
			<div id="agencyProducerFilterSection_clear"></div>
		</div>
		<xsl:variable name="cultureFormat">
			<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
		</xsl:variable>
		<!-- Build the paging grid. -->
		<xsl:call-template name="buildPagingGridPanel">
			<xsl:with-param name="dataOnLoad">true</xsl:with-param>
			<xsl:with-param name="gridID">
				<xsl:text>newList</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="targetPage">
				<xsl:text>newList</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="pageSize">
				<xsl:text>10</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordCount">
				<xsl:text>items/@listCount</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordPath">
				<xsl:text>item</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordID">
				<xsl:text>@id</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'family', type: 'string', mapping: "keys/key[@name='family']/@value"},</xsl:text>
				<xsl:text>{name: 'lob', type: 'string', mapping: "keys/key[@name='lob']/@value"},</xsl:text>
				<xsl:text>{name: 'description', type: 'string', mapping: "@description"},</xsl:text>
				<xsl:text>{name: 'state', type: 'string', mapping: "keys/key[@name='state']/@value"},</xsl:text>
				<xsl:text>{name: 'versionDate', type: 'date', dateFormat: 'Y-m-d', mapping: "@versionDate"},</xsl:text>
				<xsl:text>{name: 'renewDate', type: 'date', dateFormat: 'Y-m-d', mapping: "@renewDate"}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: "", dataIndex: "", width: 30, sortable: false, renderer: DCT.Grid.rowActionsRenderer},</xsl:text>
				<xsl:text>{text: DCT.T("ManuScriptName"), width: 240, dataIndex: 'description', sortable: true, renderer: DCT.Grid.newNameColumnRenderer },</xsl:text>
				<xsl:text>{text: DCT.T("Product"), width: 80, dataIndex: 'lob', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T("State"), width: 50, dataIndex: 'state', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T("VersionDate"), width: 60, dataIndex: 'versionDate', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
				<xsl:value-of select="$cultureFormat"/>
				<xsl:text>')},</xsl:text>
				<xsl:text>{text: DCT.T("RecentDate"), width: 80, dataIndex: 'renewDate', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
				<xsl:value-of select="$cultureFormat"/>
				<xsl:text>')}]</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildAgencyProducerFilterOptions">
		<xsl:param name="agencyListCount"/>
		<xsl:param name="agencyListMax"/>
		<xsl:param name="agencyListFilter"/>
		<div id="agencyProducerFilterOptionsSection">
			<xsl:if test="/page/content/agencyList">
				<xsl:call-template name="selectAgencyControl">
					<xsl:with-param name="showLabel">true</xsl:with-param>
					<xsl:with-param name="isRequired">true</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<div id="producerField">
				<div id="producerLabel">
					<label id="producerNameLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Producer_colon')"/>
					</label>
					<SPAN class="required">
						<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
					</SPAN>
				</div>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">ProducerName</xsl:with-param>
					<xsl:with-param name="name">_ProducerName</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<xsl:for-each select="/page/content/userList/user">
							<option value="{@name}">
								<xsl:choose>
									<xsl:when test="@fullName=''">
										<xsl:value-of select="@name"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@fullName"/>
									</xsl:otherwise>
								</xsl:choose>
							</option>
						</xsl:for-each>
					</xsl:with-param>
					<xsl:with-param name="width">120</xsl:with-param>
					<xsl:with-param name="required">1</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="/page/state/user/name"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="buildProductListHiddenFormFields">
		<xsl:param name="agencyListCount"/>
		<xsl:param name="agencyListFilter"/>
		<input type="hidden" id="_agencyListCount" name="_agencyListCount" value="{$agencyListCount}"/>
		<input type="hidden" id="_agencyStartIndex" name="_agencyStartIndex" value=""/>
		<input type="hidden" id="_agencyListFilter" name="_agencyListFilter" value="{$agencyListFilter}"/>
	</xsl:template>
</xsl:stylesheet>