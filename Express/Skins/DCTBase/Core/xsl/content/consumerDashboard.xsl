﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\dctDashboard.xsl"/>
	<xsl:import href="..\common\contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ConsumerAccess')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildConsumerQuotes"/>
		<xsl:call-template name="dashboardWidget"/>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_consumerID" id="_consumerID" value="{/page/state/consumerID}"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value=""/>
		<xsl:if test="/page/content/Policies">
			<input id="_hasPolicies" name="_hasPolicies" type="hidden" value="1"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildConsumerQuotes">
		<div id="consumerQuotesTitle">
		</div>
	</xsl:template>

</xsl:stylesheet>
