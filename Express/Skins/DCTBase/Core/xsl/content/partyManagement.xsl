﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyManagement')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPartyManagementContent">
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPartyManagementContent">
		<div id="selectedParty">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nSearchParty</xsl:with-param>
				<xsl:with-param name="ID">searchParty</xsl:with-param>
				<xsl:with-param name="href">
					<xsl:text>javascript:DCT.Submit.submitPartyAction('partyStart:partyInvolvementSearch','</xsl:text>
					<xsl:text>["_partyReturnActions:updateAccessedParties"</xsl:text>
					<xsl:text>]');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyIsProcess')"/>
				</xsl:with-param>
				<xsl:with-param name="type">hyperlink</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="__OverrideTarget" id="__overrideTarget" value="partyIsRelationship"/>
	</xsl:template>
</xsl:stylesheet>
