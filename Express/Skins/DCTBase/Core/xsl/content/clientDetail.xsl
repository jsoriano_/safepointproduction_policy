﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<!--	*************************************************************************************************************
		Global Variables		
		************************************************************************************************************* -->
	<xsl:variable name="helpImage"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for processing the interview response and producing the overall structure of the HTML page.
		************************************************************************************************************* -->
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildCommonLayout">
			<xsl:with-param name="screenName">
				<xsl:text>clientDetail</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:choose>
					<xsl:when test="normalize-space(/page/content/getPage/body/@caption)!=''">
						<xsl:value-of select="/page/content/getPage/body/@caption"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsODExt:getDictRes('AddNewClient')" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:choose>
					<xsl:when test="(/page/content//errors)">
						<xsl:value-of select="xslNsODExt:getDictRes('ErrorOccuredInterviewCheckInputs')" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:text><!--Use the form below to add a new client to your client list.--></xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildEditDetailButtons">
		<div class="g-btn-bar">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">updateDetails</xsl:with-param>
				<xsl:with-param name="id">updateDetails</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitClientAction('clientInfo', '</xsl:text>
					<xsl:choose>
						<!--<xsl:when test="/page/content/clientID/@value &gt; 0">-->
            <xsl:when test="/page/content/clientID/@value != '0' and /page/content/clientID/@value != '' and /page/content/clientID/@value != '-1'">
							<xsl:value-of select="//content/clientID/@value"/>
							<xsl:text>','clientDetail');</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>0','newClient');</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('UpdateDetails')" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">returnToClient</xsl:with-param>
				<xsl:with-param name="id">returnToClient</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Submit.submitPage('clientInfo');</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')" />
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<xsl:template name="fieldDetail">
		<!-- do nothing -->
	</xsl:template>

	<xsl:template name="buildHiddenDetailFields">
		<input type="hidden" name="_returnPage" id="_returnPage" value="{/page/content/ExpressCache/_returnPage}"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{policy/@id}"/>
		<input type="hidden" name="_ClientID" id="_ClientID" value="{/page/content/clientID/@value}"/>
	</xsl:template>

</xsl:stylesheet>