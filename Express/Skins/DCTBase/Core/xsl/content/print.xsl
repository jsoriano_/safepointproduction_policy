﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:call-template name="buildContainer"/>
    </xsl:template>

    <xsl:template name="processPageHeader">
        <xsl:call-template name="buidPageHeader">
            <xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('CreatePolicyDocuments')" />
            </xsl:with-param>
            <xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('SelectDocumentCreateForPolicy')" />
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

    <xsl:template name="buildPageContent">
        <xsl:if test="/page/state/clientName != ''">
            <p>
				<xsl:value-of select="xslNsODExt:getDictRes('DocsAvailFor')" />
				<xsl:text> </xsl:text>
				<a>
					<xsl:attribute name="name">clientNameA</xsl:attribute>
					<xsl:attribute name="id">clientNameA</xsl:attribute>
					<xsl:attribute name="href">javascript:;</xsl:attribute>
					<xsl:attribute name="onclick">DCT.Submit.submitPage('interview');</xsl:attribute>
				</a>
            </p>
		</xsl:if>

        <xsl:for-each select="/page/content/topics/topic">
			<p>
                <xsl:call-template name="makeButton">
                    <xsl:with-param name="name">topicNameA</xsl:with-param>
                    <xsl:with-param name="id">topicNameA</xsl:with-param>
                    <xsl:with-param name="onclick">
                        <xsl:text>DCT.Submit.printOut('</xsl:text>
                        <xsl:value-of select="@name"/>
                        <xsl:text>','</xsl:text>
                        <xsl:value-of disable-output-escaping="yes" select="@xsl"/>
                        <xsl:text>')</xsl:text>
                    </xsl:with-param>
                    <xsl:with-param name="caption" select="@caption"/>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
                </xsl:call-template>
            </p>
        </xsl:for-each>
		
		<xsl:call-template name="makeButton">
			<xsl:with-param name="name">returnToPolicyA</xsl:with-param>
			<xsl:with-param name="id">returnToPolicyA</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Submit.setActiveParent('interview');</xsl:text>
				<xsl:text>DCT.Submit.submitPage('interview');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="xslNsODExt:getDictRes('ReturnToPolicy')" />
			</xsl:with-param>
			<xsl:with-param name="type">cancel</xsl:with-param>
		</xsl:call-template>
    </xsl:template>
</xsl:stylesheet>