﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentDashboardPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="/page/popUp = 1">
				<xsl:call-template name="reloadPageInParentWindow"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildContainer"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildDashboardHiddenFormFields"/>
		<xsl:call-template name="buildMainAreaContent"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PASDashboardTitle')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildMainAreaContent">

		<div id="pageInstruction">
			<xsl:value-of select="xslNsExt:FormatString1(xslNsODExt:getDictRes('WelcomeToPASDashboard_Phrasing'), /page/state/user/fullName)" />
		</div>
		<xsl:call-template name="dashboardWidget"/>
	</xsl:template>
</xsl:stylesheet>
