﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:variable name="searchTypePolicy" select="/page/content/search/@typeOfSearch = 'policy' or /page/content/search/@quickSearchMode = 'policy'"/>
	<xsl:variable name="searchTypeAllClient" select="/page/content/search/@typeOfSearch = 'allClient'"/>
	<xsl:variable name="searchTypeClient" select="/page/content/search/@typeOfSearch = 'client' or (/page/content/search/@quickSearchMode != 'policy' and normalize-space(/page/content/search/@quickSearchMode)!='')"/>
	<xsl:variable name="searchTypeAllPolicies" select="not($searchTypeClient or $searchTypeAllClient or $searchTypePolicy)"/>
	<xsl:variable name="typeOfSearch">
		<xsl:choose>
			<xsl:when test="$searchTypeClient">client</xsl:when>
			<xsl:when test="$searchTypeAllClient">allClient</xsl:when>
			<xsl:when test="$searchTypePolicy">policy</xsl:when>
			<xsl:otherwise>allPolicies</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('SearchPoliciesQuotesClients')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPageHeaderSection"/>
		<xsl:call-template name="searchFieldGroup"/>
		<xsl:choose>
			<xsl:when test="normalize-space(/page/content/search/@quickSearchMode)=''">
				<script>
					<xsl:text>Ext.onReady(function(){m_activeSearchMode = '</xsl:text>
					<xsl:value-of select="$typeOfSearch"/>
					<xsl:text>';});</xsl:text>
				</script>
			</xsl:when>
			<xsl:otherwise>
				<script>
					<xsl:text>Ext.onReady(function(){DCT.Submit.applySearchFilter('</xsl:text>
					<xsl:value-of select="/page/content/search/@quickSearchMode"/>
					<xsl:text>');})</xsl:text>
				</script>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="0"/>
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value=""/>
		<input type="hidden" name="_MaxChildLevel" id="_MaxChildLevel" value="-1"/>
		<input type="hidden" name="_startIndex" id="_startIndex" value="{/page/content/policyList/@startIndex}"/>
		<input type="hidden" name="_displayCount" id="_displayCount" value="10"/>
		<input type="hidden" name="_hdnSortExpression" id="_hdnSortExpression" value="{/page/content/keys/orderKey/@name}"/>
		<input type="hidden" name="_hdnSortOrder" id="_hdnSortOrder" value="{/page/content/keys/orderKey/@direction}"/>
		<input type="hidden" name="_filterSubset" id="_filterSubset" value="AdvSearch"/>
	</xsl:template>
	<xsl:template name="searchFieldGroup">
		<div id="searchFilter">
			<div id="searchFilterSection">
				<xsl:call-template name="buildSearchTypeGroup"/>
				<div id="searchConstraintsWrapper">
					<xsl:attribute name="class">
						<xsl:text>downFieldGroup</xsl:text>
						<xsl:if test="$searchTypeAllPolicies or $searchTypeAllClient">
							<xsl:text> hideOffset</xsl:text>
						</xsl:if>
					</xsl:attribute>
					<xsl:call-template name="searchSettings"/>
				</div>
				<xsl:call-template name="buildMoreOptionsGroup"/>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">searchFilterActions</xsl:with-param>
					<xsl:with-param name="id">searchFilterActions</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.applySearchFilter(DCT.Submit.activeSearchMode);</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="class">applySearchFilter</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
					</xsl:with-param>
					<xsl:with-param name="type">search</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
		<div id="searchResults">
			<xsl:call-template name="emptyPolicyGridMessage"/>
			<xsl:call-template name="emptyClientGridMessage"/>
			<div id="policyResults">
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="dataOnLoad">
						<xsl:choose>
							<xsl:when test="page/content/@saveFilter='1'">true</xsl:when>
							<xsl:otherwise>false</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="toolBarTitle">DCT.T('SearchResults')</xsl:with-param>
					<xsl:with-param name="gridID">
						<xsl:text>quoteList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>quoteList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>15</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>policyList/@listCount</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>policy</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@policyID</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@clientName'},</xsl:text>
						<xsl:text>{name: 'LOB', type: 'string', mapping: '@LOB'},</xsl:text>
						<xsl:text>{name: 'status', type: 'string', mapping: '@status'},</xsl:text>
						<xsl:text>{name: 'quote.description', type: 'string', mapping: '@description'},</xsl:text>
						<xsl:text>{name: 'description2', type: 'string', mapping: '@description2'},</xsl:text>
						<xsl:text>{name: 'policyNumber', type: 'string', mapping: '@policyNumber'},</xsl:text>
						<xsl:text>{name: 'locked', type: 'bool', mapping: '@locked'},</xsl:text>
						<xsl:text>{name: 'effectiveDate', type: 'date', dateFormat: 'Y-m-d', mapping: '@effectiveDate'},</xsl:text>
						<xsl:text>{name: 'expirationDate', type: 'date', dateFormat: 'Y-m-d', mapping: '@expirationDate'},</xsl:text>
						<xsl:text>{name: 'datemodified', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: '@modifiedDate'},</xsl:text>
						<xsl:text>{name: 'deleted', type: 'string', mapping: '@deleted'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T('PolicyQuoteNumSymbol'), dataIndex: 'policyNumber', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('InsuredName'), dataIndex: 'name', sortable: true, renderer: DCT.Grid.namePolicyColumnRenderer},</xsl:text>
						<xsl:text>{text: DCT.T('Line'), dataIndex: 'LOB', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('EffDate'), dataIndex: 'effectiveDate', sortable: true, width:60, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: DCT.T('ExpDate'), dataIndex: 'expirationDate', sortable: true, width:60, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: DCT.T('Description'), dataIndex: 'description2', sortable: true, hidden: true},</xsl:text>
						<xsl:text>{text: DCT.T('Status'), dataIndex: 'status', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Locked'), dataIndex: 'locked', sortable: false, width:50},</xsl:text>
						<xsl:text>{text: DCT.T('LastTransaction'), dataIndex: 'quote.description', sortable: true, hidden: true},</xsl:text>
						<xsl:text>{text: DCT.T('LastAccessed'), dataIndex: 'datemodified', sortable: true, hidden: true, width:60,renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '3')"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.openRowActionsRenderer}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
					<xsl:with-param name="dataLoadEventFunc">
						<xsl:text>DCT.Util.policyRecordCountCheck</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="startHidden">true</xsl:with-param>
					<xsl:with-param name="pagingPluginType">progress</xsl:with-param>
					<xsl:with-param name="storeClass">
						<xsl:text>'DCT.PolicySearchPagingStore'</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<div id="clientResults">
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="dataOnLoad">
						<xsl:text>false</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="gridID">
						<xsl:text>clientListData</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>clientListData</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="toolBarTitle">DCT.T('SearchResults')</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>15</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>clientList/@listCount</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>client</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:text>@clientID</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:text>[{name: 'name', type: 'string', mapping: '@name'},</xsl:text>
						<xsl:text>{name: 'primaryContact', type: 'string', mapping: '@primaryContact'},</xsl:text>
						<xsl:text>{name: 'Phone.PhoneNumber', type: 'string', mapping: '@phone'},</xsl:text>
						<xsl:text>{name: 'address1', type: 'string', mapping: '@address1'},</xsl:text>
						<xsl:text>{name: 'city', type: 'string', mapping: '@city'},</xsl:text>
						<xsl:text>{name: 'state', type: 'string', mapping: '@state'},</xsl:text>
						<xsl:text>{name: 'zipcode', type: 'string', mapping: '@zipcode'},</xsl:text>
						<xsl:text>{name: 'deleted', type: 'string', mapping: '@deleted'}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:text>[{text: DCT.T('InsuredName'), dataIndex: 'name', sortable: true, renderer: DCT.Grid.nameColumnRenderer},</xsl:text>
						<xsl:text>{text: DCT.T('ContactName'), dataIndex: 'primaryContact', sortable: true, renderer: DCT.Grid.titleColumnRenderer},</xsl:text>
						<xsl:text>{text: DCT.T('Phone'), dataIndex: 'Phone.PhoneNumber', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Address'), dataIndex: 'address1', sortable: true, hidden: true},</xsl:text>
						<xsl:text>{text: DCT.T('City'), dataIndex: 'city', sortable: true, renderer: DCT.Grid.titleColumnRenderer},</xsl:text>
						<xsl:text>{text: DCT.T('State'), dataIndex: 'state', sortable: true},</xsl:text>
						<xsl:text>{text: DCT.T('Zip'), dataIndex: 'zipcode', sortable: true},</xsl:text>
						<xsl:text>{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.clientRowActionsRenderer}]</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
					<xsl:with-param name="dataLoadEventFunc">
						<xsl:text>DCT.Util.clientRecordCountCheck</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="startHidden">true</xsl:with-param>
					<xsl:with-param name="pagingPluginType">progress</xsl:with-param>
				</xsl:call-template>
			</div>
			<div id="createClientButton" style="display:none">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">createClient</xsl:with-param>
					<xsl:with-param name="id">createClient</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Submit.createNewClient();</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('CreateNewClient')"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildSearchTypeGroup">
		<div id="holdTypeGroup" class="searchForGroup">
			<div class="searchSubHeading">
				<xsl:value-of select="xslNsODExt:getDictRes('ImLookingFor')"/>
			</div>
			<div id="searchRadioGroupContainerId" class="radioGroupContainer">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="type">radio</xsl:with-param>
					<xsl:with-param name="id">searchRadioGroup</xsl:with-param>
					<xsl:with-param name="controlClass">systemSearchRadioGroup</xsl:with-param>
					<xsl:with-param name="columns">1</xsl:with-param>
					<xsl:with-param name="required">1</xsl:with-param>
					<xsl:with-param name="radiobuttons">
						<xsl:text>{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_allPolicyQuoteOption</xsl:with-param>
							<xsl:with-param name="name">_typeOfSearch</xsl:with-param>
							<xsl:with-param name="value">allPolicies</xsl:with-param>
							<xsl:with-param name="checked">
								<xsl:choose>
									<xsl:when test="$searchTypeAllPolicies">
										<xsl:text>true</xsl:text>
									</xsl:when>
									<xsl:otherwise>false</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('PoliciesAndQuotes')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_allClientOption</xsl:with-param>
							<xsl:with-param name="name">_typeOfSearch</xsl:with-param>
							<xsl:with-param name="value">allClient</xsl:with-param>
							<xsl:with-param name="checked">
								<xsl:choose>
									<xsl:when test="$searchTypeAllClient">
										<xsl:text>true</xsl:text>
									</xsl:when>
									<xsl:otherwise>false</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('AllClients')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_policyQuoteOption</xsl:with-param>
							<xsl:with-param name="name">_typeOfSearch</xsl:with-param>
							<xsl:with-param name="value">policy</xsl:with-param>
							<xsl:with-param name="checked">
								<xsl:choose>
									<xsl:when test="$searchTypePolicy">
										<xsl:text>true</xsl:text>
									</xsl:when>
									<xsl:otherwise>false</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('SpecificPoliciesAndQuotes')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>},{</xsl:text>
						<xsl:call-template name="buildSystemRadioButtons">
							<xsl:with-param name="fieldID">_clientOption</xsl:with-param>
							<xsl:with-param name="name">_typeOfSearch</xsl:with-param>
							<xsl:with-param name="value">client</xsl:with-param>
							<xsl:with-param name="checked">
								<xsl:choose>
									<xsl:when test="$searchTypeClient">
										<xsl:text>true</xsl:text>
									</xsl:when>
									<xsl:otherwise>false</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="label">
								<xsl:value-of select="xslNsODExt:getDictRes('SpecificClients')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>}</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="vertical">true</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildMoreOptionsGroup">
		<div id="searchListFilterDiv" class="queryBuilderDiv">
			<div id="searchAgencyListGroup">
				<xsl:if test="/page/content/agencyList">
					<div class="searchInGroup">
						<div class="searchSubHeading">
							<xsl:value-of select="xslNsODExt:getDictRes('SearchIn')"/>
						</div>
						<div class="searchInFieldGroup">
							<input type="hidden" id="_agencyStartIndex" name="_agencyStartIndex" value=""/>
							<input type="hidden" id="_agencyListFilter" name="_agencyListFilter" value="{/page/content/agencyListFilter/@filter}"/>
							<xsl:call-template name="selectAgencyControl"/>
						</div>
					</div>
				</xsl:if>
			</div>
			<div id="searchHistoryGroup">
				<xsl:if test="$searchTypeClient or $searchTypeAllClient">
					<xsl:attribute name="class">hideOffset</xsl:attribute>
				</xsl:if>
				<div id="searchHistoryField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="id">searchHistoryCheckBoxGroup</xsl:with-param>
						<xsl:with-param name="checkbox">
							<xsl:call-template name="buildCheckBoxObject">
								<xsl:with-param name="fieldID">searchHistory</xsl:with-param>
								<xsl:with-param name="name">_searchHistory</xsl:with-param>
								<xsl:with-param name="inputValue">0</xsl:with-param>
								<xsl:with-param name="dctClassName">dctsystemcheckboxfield</xsl:with-param>
								<xsl:with-param name="boxLabel">
									<xsl:value-of select="xslNsODExt:getDictRes('IncludePreviousTerms')"/>
								</xsl:with-param>
								<xsl:with-param name="checked">
									<xsl:choose>
										<xsl:when test="page/content/@searchHistory='1'">true</xsl:when>
										<xsl:otherwise>false</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="showDeletedGroup">
				<div id="showDeletedField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="id">showDeletedCheckBoxGroup</xsl:with-param>
						<xsl:with-param name="checkbox">
							<xsl:call-template name="buildCheckBoxObject">
								<xsl:with-param name="fieldID">showDeleted</xsl:with-param>
								<xsl:with-param name="name">_showDeleted</xsl:with-param>
								<xsl:with-param name="inputValue">0</xsl:with-param>
								<xsl:with-param name="dctClassName">dctsystemcheckboxfield</xsl:with-param>
								<xsl:with-param name="boxLabel">
									<xsl:value-of select="xslNsODExt:getDictRes('IncludeDeleted')"/>
								</xsl:with-param>
								<xsl:with-param name="checked">
									<xsl:choose>
										<xsl:when test="page/content/@showDeleted='1'">true</xsl:when>
										<xsl:otherwise>false</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="emptyPolicyGridMessage">
		<div id="policyGridEmpty" class="notificationDiv nWarn" style="display:none">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="xslNsODExt:getDictRes('NoPoliciesFound_ReviseAgain')"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="emptyClientGridMessage">
		<div id="clientGridEmpty" class="notificationDiv nWarn" style="display:none">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="xslNsODExt:getDictRes('NoClients_NewOrSearchAgain_One')"/>
				<b>
					<xsl:value-of select="xslNsODExt:getDictRes('CreateNewClient')"/>
				</b>
				<xsl:value-of select="xslNsODExt:getDictRes('NoClients_NewOrSearchAgain_Two')"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>