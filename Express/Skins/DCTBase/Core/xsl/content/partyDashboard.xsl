﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentDashboardPage.xsl"/>
	<xsl:import href="..\common\dctCommonParty.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
<!--
			<xsl:with-param name="sysRuleSet">
				<xsl:text>topNav</xsl:text>
			</xsl:with-param>
-->
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PeopleAndPlaces')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildDashboardHiddenFormFields"/>
		<xsl:call-template name="createPartyDashboardMainArea">
		<xsl:with-param name="displayNotes">false</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="createPartyDashboardMainArea">
		<xsl:param name="displayNotes">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:call-template name="buildPartyDashboardMainAreaContent"/>
	</xsl:template>
	<xsl:template name="buildPartyDashboardMainAreaContent">
		<div id="pageInstruction">
			<xsl:value-of select="xslNsExt:FormatString1(xslNsODExt:getDictRes('WelcomePeopleAndPlaces_Phrasing'), /page/state/user/fullName)" />
		</div>
		<xsl:call-template name="dashboardWidget"/>
	</xsl:template>
</xsl:stylesheet>