﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
	<xsl:import href="..\common\dctGenericPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	

  <!--  DisableHelpTabbing set to 0 to tab into annotations, set to 1 to skip over annotations -->
  <xsl:variable name="DisableHelpTabbing">
    <xsl:text>1</xsl:text>
  </xsl:variable>

  <xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:if test="not($isPartyPage)"><!--Party pages do not need a left nav-->
			<xsl:call-template name="buildTabs">
				<xsl:with-param name="cssClass">
					<xsl:choose>
						<xsl:when test="not($showHeader)">pageNavNoHeader</xsl:when>
						<xsl:otherwise>pageNav</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>