﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonParty.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyIsTitle')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="createPartyIsRelationshipContent"/>
	</xsl:template>
	<xsl:template name="createPartyIsRelationshipContent">
		<xsl:call-template name="buildSelectedPartyContent">
			<xsl:with-param name="selectedTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('LinkPeoplePlacesSubTitle')"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="buildLinkedPartiesContent"/>
		<div id="partyIsActions" class="g-btn-bar">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">processLink</xsl:with-param>
				<xsl:with-param name="id">processLinkId</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitPartyPageAction('partyManagement', 'partyLinkParties', false, 'processLink');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyLinkTo')"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">processCancel</xsl:with-param>
				<xsl:with-param name="id">processCancelId</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitPartyPage('partyManagement');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildLinkedPartiesContent">
		<div id="PartyLinksTitle">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('ToThesePeopleAndPlaces')"/>
			</h2>
		</div>
		<div id="PartyLinksDetail">
			<table class="formTbl">
				<xsl:call-template name="PartyLink"/>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="PartyLink" name="PartyLink">
		<xsl:for-each select="/page/content/ExpressCache/PartyLinks/PartyLink">
			<xsl:variable name="phoneValue">
				<xsl:value-of select="PhoneNumber"/>
			</xsl:variable>
			<xsl:variable name="addressLine2Value">
				<xsl:value-of select="LocationAddressLine2"/>
			</xsl:variable>
			<xsl:variable name="phoneExtValue">
				<xsl:value-of select="PhoneExtenstion"/>
			</xsl:variable>
			<tr>
				<td class="detailsLabel">
					<xsl:value-of select="FullName"/>
				</td>
			</tr>
			<tr>
				<td class="">
					<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumberExt')"/>
				</td>
				<td>
					<div>
						<span id="phoneNumber">
							<xsl:value-of select="xslNsExt:cultureAwarePhoneFormatter($phoneValue)"/>
							<xsl:if test="$phoneExtValue != ''">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="PhoneExtenstion"/>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</span>
					</div>
				</td>
				<td class="">
					<xsl:value-of select="xslNsODExt:getDictRes('Address')"/>
				</td>
				<td>
					<div>
						<span id="address1">
							<xsl:value-of select="LocationAddressLine1"/>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<xsl:value-of select="xslNsODExt:getDictRes('Email')"/>
				</td>
				<td>
					<div>
						<span id="email">
							<xsl:value-of select="EmailAddress"/>
						</span>
					</div>
				</td>
				<td class="">
					<label>
					</label>
				</td>
				<td>
					<div>
						<span id="address2">
							<xsl:choose>
								<xsl:when test="$addressLine2Value != ''">
									<xsl:value-of select="$addressLine2Value"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="CityStateZipDisplay"/>
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td class="">
					<xsl:value-of select="xslNsODExt:getDictRes('Contact')"/>
				</td>
				<td>
					<div>
						<span id="contact">
						</span>
					</div>
				</td>
				<td class="">
					<label>
					</label>
				</td>
				<td>
					<div>
						<span id="citystatezip">
							<xsl:choose>
								<xsl:when test="$addressLine2Value != ''">
									<xsl:value-of select="CityStateZipDisplay"/>
								</xsl:when>
								<xsl:otherwise></xsl:otherwise>
							</xsl:choose>
						</span>
					</div>
				</td>
			</tr>
			<tr></tr>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
	</xsl:template>
</xsl:stylesheet>