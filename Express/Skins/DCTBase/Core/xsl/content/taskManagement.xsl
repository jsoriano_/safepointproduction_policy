﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions"  xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('TaskManagement')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildMainAreaContent"/>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildMainAreaContent">
		<div id="queueItemSelector" class="downLayout">	
			<div id="queueLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('Queues_colon')" />	
			</div>
			<div class="downFormField">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminQueueEntityIdDisplay</xsl:with-param>
					<xsl:with-param name="name">_entityIdsDisplay</xsl:with-param>
					<xsl:with-param name="title">
						<xsl:value-of select="xslNsExt:convertISOtoDisplay(/page/content/ExpressCache/CurrentSystemDate/@date, 'd')"/>
					</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AllQueues')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>
			</div>
			<a href="javascript:;" onclick="DCT.Util.displayTaskQueuePopUp(10);" name="taskQueueSearch" id="taskQueueSearch"
			   class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskQueues')}"></a>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminQueueEntityId</xsl:with-param>
				<xsl:with-param name="name">_entityId</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
				<xsl:with-param name="value">
					<xsl:text></xsl:text>
				</xsl:with-param>
			</xsl:call-template>				
		</div>		
		<div id="dueItemSelector" class="downLayout">
			<div id="dueLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('DueDate_colon')" />
			</div>
			<div class="downFormField">		
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminFilterDisplay</xsl:with-param>
					<xsl:with-param name="name">_adminFilterDisplay</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AnyDueDate')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>
			</div>
			<a href="javascript:;" onclick="DCT.Util.displayTaskDuePopUp();" name="taskDueSearch" id="taskDueSearch"
				class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskDueDates')}">
			</a>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminFilter</xsl:with-param>
				<xsl:with-param name="name">_adminFilter</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
				<xsl:with-param name="value">
					<xsl:text></xsl:text> 
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">dueStartDate</xsl:with-param>
				<xsl:with-param name="name">_dueStartDate</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
			</xsl:call-template>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">dueEndDate</xsl:with-param>
				<xsl:with-param name="name">_dueEndDate</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
			</xsl:call-template>
		</div>		
		<div id="userItemSelector" class="downLayout">	
			<div id="dueLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('AssignedTo_colon')" />	
			</div>
			<div class="downFormField">						
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminUserIdsDisplay</xsl:with-param>
					<xsl:with-param name="name">_adminUserIdsDisplay</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AnyUserUnassigned')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>	
			</div>
			<a href="javascript:;" onclick="DCT.Util.displayTaskUserPopUp(10);" name="taskUserSearch" id="taskUserSearch"
			   class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskAssignedUsers')}">
			</a>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminAssignedUserId</xsl:with-param>
				<xsl:with-param name="name">_adminAssignedUserId</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
					<xsl:with-param name="value">
					</xsl:with-param>
			</xsl:call-template>	
		</div>		
		<div id="statusItemSelector" class="downLayout">	
			<xsl:variable name="doStatusPopup">
				<xsl:text>DCT.Util.displayTaskStatusPopUp([</xsl:text>	
				<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_TASKSTATUS']/ListEntry">
					<xsl:text>['</xsl:text>					
					<xsl:value-of select="@Code"/>
					<xsl:text>','</xsl:text>										
					<xsl:value-of select="@Description"/>
					<xsl:text>']</xsl:text>	
					<xsl:choose>     
						<xsl:when test="position() != last()">,</xsl:when>   
					</xsl:choose> 
				</xsl:for-each>
				<xsl:text>]);</xsl:text>
			</xsl:variable>				
			<div id="dueLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('Status_colon')" />	
			</div>
			<div class="downFormField">	
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminStatusDisplay</xsl:with-param>
					<xsl:with-param name="name">_adminStatusDisplay</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>				
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AllTasks')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>
			</div>
			<a href="javascript:;" onclick="{$doStatusPopup}" name="taskStatusSearch" id="taskStatusSearch"
			   class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskStatuses')}">
			</a>		
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminStatusFilterId</xsl:with-param>
				<xsl:with-param name="name">adminStatusFilter</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
					<xsl:with-param name="value">
						<xsl:value-of select="/page/content/ExpressCache/TaskManagement/@_adminStatusFilter"/>
					</xsl:with-param>
			</xsl:call-template>	
		</div>	
		<div id="categoryItemSelector" class="downLayout">	
			<xsl:variable name="doCategoryPopup">
				<xsl:text>DCT.Util.displayTaskCategoryPopUp([</xsl:text>	
				<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_TASKTYPE']/ListEntry">
					<xsl:text>['</xsl:text>					
					<xsl:value-of select="@Code"/>
					<xsl:text>','</xsl:text>										
					<xsl:value-of select="@Description"/>
					<xsl:text>']</xsl:text>	
					<xsl:choose>     
						<xsl:when test="position() != last()">,</xsl:when>   
					</xsl:choose> 
				</xsl:for-each>
				<xsl:text>]);</xsl:text>
			</xsl:variable>				
			<div id="dueLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('Category_colon')" />	
			</div>
			<div class="downFormField">	
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminCategoryDisplay</xsl:with-param>
					<xsl:with-param name="name">_adminCategoryDisplay</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>				
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AllCategories')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>
			</div>
			<a href="javascript:;" onclick="{$doCategoryPopup}" name="taskCategorySearch" id="taskCategorySearch"
				class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskCategories')}">
			</a>		
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminCategoryFilterId</xsl:with-param>
				<xsl:with-param name="name">adminCategoryFilter</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
					<xsl:with-param name="value">
						<xsl:value-of select="/page/content/ExpressCache/TaskManagement/@_adminCategoryFilter"/>
					</xsl:with-param>
			</xsl:call-template>	
		</div>	
		<div id="createdOnItemSelector" class="downLayout">
			<div id="createdLabel" class="downFormLabel">
				<xsl:value-of select="xslNsODExt:getDictRes('Created_colon')" />
			</div>
			<div class="downFormField">		
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminCreatedOnDisplay</xsl:with-param>
					<xsl:with-param name="name">_adminCreatedOnDisplay</xsl:with-param>
					<xsl:with-param name="readonly">1</xsl:with-param>
					<xsl:with-param name="type">textarea</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsODExt:getDictRes('AnyCreationDate')"/>
					</xsl:with-param>	
					<xsl:with-param name="rows">1</xsl:with-param>
					<xsl:with-param name="width">300</xsl:with-param>
				</xsl:call-template>
			</div>
			<a href="javascript:;" onclick="DCT.Util.displayTaskCreatedOnPopUp();" name="taskCreatedOnSearch" id="taskCreatedOnSearch"
			   class="editIcon" title="{xslNsODExt:getDictRes('SelectTaskCreationDates')}">
			</a>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">adminCreatedOnFilter</xsl:with-param>
				<xsl:with-param name="name">_adminCreatedOnFilter</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
				<xsl:with-param name="value">
					<xsl:text></xsl:text> 
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">createdOnStartDate</xsl:with-param>
				<xsl:with-param name="name">_createdOnStartDate</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
			</xsl:call-template>	
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">createdOnEndDate</xsl:with-param>
				<xsl:with-param name="name">_createdOnEndDate</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:text>hidden</xsl:text> 
				</xsl:with-param>				
			</xsl:call-template>
		</div>
		<div id="adminMoreOptions" class="downLayout">
			<div id="referenceLabel" name="referenceLabel" class="downFormLabel" data-qtip="{xslNsODExt:getDictRes('TaskReferenceSearchHelp')}" >
				<xsl:value-of select="xslNsODExt:getDictRes('ReferenceNumSymbol_colon')"/>
			</div>
			<div class="downFormField">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminSearchFilterId</xsl:with-param>
					<xsl:with-param name="name">adminSearchFilter</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="policy">
							<xsl:value-of select="xslNsODExt:getDictRes('ReferenceNumSymbol')"/>
						</option>
						<option value="titledesc">
							<xsl:value-of select="xslNsODExt:getDictRes('TitleDesc')"/>
						</option>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:text>policy</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="width">110</xsl:with-param>
					<xsl:with-param name="defaultValue">
						<xsl:text>policy</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<div class="downFormLabel" id="adminSearchFilterForCaption">
			</div>
			<div class="downFormField">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">adminSearchFilterForId</xsl:with-param>
					<xsl:with-param name="name">adminSearchFilterFor</xsl:with-param>
					<xsl:with-param name="extraConfigItems">
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctTaskFilter</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>admin</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
		<xsl:call-template name="makeButton">
				<xsl:with-param name="name">_showTasksLink</xsl:with-param>
				<xsl:with-param name="id">showTasksLink</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Util.reloadTaskQueue(Ext.getCmp('adminQueueEntityId').getValue(), Ext.getCmp('adminAssignedUserId').getValue(), Ext.getCmp('adminFilter').getValue(), 'admin', 'QueueAdmin', false, 'skipCheck');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('ShowMeTasks')"/>
				</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="taskQueueWidget">
			<xsl:with-param name="id">QueueAdmin</xsl:with-param>
			<xsl:with-param name="renderTo">QueueAdminContainer</xsl:with-param>
			<xsl:with-param name="listTitle">
				<xsl:text>'</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('TaskManagement')"/>
				<xsl:text>'</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="listMode">
				<xsl:text>QueueAdmin</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="actionColumnRenderer">
				<xsl:text>function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.generalTaskActionColumnRenderer('taskManagement', true, '</xsl:text>
				<xsl:value-of select="/page/state/user/userID"/>
				<xsl:text>', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="titleColumnRenderer">function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.taskTitleViewRenderer('taskManagement', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }</xsl:with-param>
			<xsl:with-param name="showCheckbox">true</xsl:with-param>
			<xsl:with-param name="multipleSelect">true</xsl:with-param>
			<xsl:with-param name="className">tasksPagingGridPanel</xsl:with-param>
			<xsl:with-param name="classMethod">DCT.Util.refreshAdminTaskQueue</xsl:with-param>				
		</xsl:call-template>
		<xsl:call-template name="makeButton">
			<xsl:with-param name="id">adminMassReassign</xsl:with-param>
			<xsl:with-param name="class"></xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Submit.massReassignTasks('QueueAdmin');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="xslNsODExt:getDictRes('Reassign')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildHiddenFormFields">
	</xsl:template>
</xsl:stylesheet>