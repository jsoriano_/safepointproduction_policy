﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:variable name="partyType" select="/page/content/ExpressCache/PartyType" />

		<!-- build the page header -->
		<xsl:call-template name="partyImportHeader">
			<xsl:with-param name="partyType" select="$partyType"/>
		</xsl:call-template>

		<div id="selectPartyHistoryData">
			<xsl:choose>
				<xsl:when test="$partyType = 'Party'">
					<xsl:call-template name="Party" />
				</xsl:when>
				<xsl:when test="$partyType = 'Location'">
					<xsl:call-template name="Location" />
				</xsl:when>
				<xsl:when test="$partyType = 'LocationInvolvement'">
					<xsl:call-template name="LocationInvolvement" />
				</xsl:when>
				<xsl:when test="$partyType = 'PartyEmail'">
					<xsl:call-template name="PartyEmail" />
				</xsl:when>
				<xsl:when test="$partyType = 'PartyPhone'">
					<xsl:call-template name="PartyPhone" />
				</xsl:when>
			</xsl:choose>
		</div>

		<!-- build the button row-->
		<xsl:call-template name="partyImportActions">
			<xsl:with-param name="partyType" select="$partyType"/>
				</xsl:call-template>
	</xsl:template>

	<!-- Page Headings -->
	<xsl:template name="partyImportHeader">
		<xsl:param name="partyType"/>
		<xsl:variable name="partyImportTitle">
			<xsl:choose>
				<xsl:when test="$partyType = 'Party'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Party_Title')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'Location'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Location_Title')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'LocationInvolvement'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Involvement_Title')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyEmail'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Email_Title')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyPhone'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Phone_Title')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Generic_Title')"/>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:variable>
		<xsl:variable name="partyImportInstructions">
			<xsl:choose>
				<xsl:when test="$partyType = 'Party'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Party_Instructions')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'Location'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Location_Instructions')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'LocationInvolvement'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Involvement_Instructions')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyEmail'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Email_Instructions')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyPhone'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Phone_Instructions')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Generic_Instructions')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<div id="pageTop">
			<div id="pageTitle">
				<xsl:value-of select="$partyImportTitle" />
			</div>
		</div>
		<div id="fields">
			<xsl:attribute name="class">
				<xsl:text>styleWithOutPanels</xsl:text>
			</xsl:attribute>
			<!--<xsl:value-of select="$partyImportInstructions" />-->
		</div>
	</xsl:template>

	<!-- Import Selection Grids -->
	<xsl:template match="Party" name="Party">
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>partyRecordData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyHistoryDataList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>15</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>_partyMappedData/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>Party</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@id</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemSelected</xsl:text>
				</xsl:with-param>
			<xsl:with-param name="toolBarTitle">
				<xsl:text>DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Party_ToolBarTitle')"/>
				<xsl:text>')</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'PartyTypeCode', type: 'string', mapping: 'PartyTypeCode'},</xsl:text>
				<xsl:text>{name: 'PartyCorrespondenceName', type: 'string', mapping: 'PartyCorrespondenceName'},</xsl:text>
				<xsl:text>{name: 'PartyName', type: 'string', mapping: 'PartyName'},</xsl:text>
				<xsl:text>{name: 'PartyFirstName', type: 'string', mapping: 'PartyFirstName'},</xsl:text>
				<xsl:text>{name: 'PartyMiddleName', type: 'string', mapping: 'PartyMiddleName'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PartyTypeCode')"/>
				<xsl:text>'), dataIndex: 'PartyTypeCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PartyCorrespondenceName')"/>
				<xsl:text>'), dataIndex: 'PartyCorrespondenceName', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PartyName')"/>
				<xsl:text>'), dataIndex: 'PartyName', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PartyFirstName')"/>
				<xsl:text>'), dataIndex: 'PartyFirstName', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PartyMiddleName')"/>
				<xsl:text>'), dataIndex: 'PartyMiddleName', sortable: true}]</xsl:text>
			</xsl:with-param>
			</xsl:call-template>
	</xsl:template>

	<xsl:template match="Location" name="Location">
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>partyRecordData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyHistoryDataList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>15</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>_partyMappedData/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>Location</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@id</xsl:text>
				</xsl:with-param>
				  <xsl:with-param name="showToolBar">
					<xsl:choose>
					  <xsl:when test="$action = 'add'">false</xsl:when>
					  <xsl:otherwise>true</xsl:otherwise>
					</xsl:choose>
				  </xsl:with-param>
				<xsl:with-param name="showGridCheckbox">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemSelected</xsl:text>
				</xsl:with-param>
			<xsl:with-param name="toolBarTitle">
				<xsl:text>DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Location_ToolBarTitle')"/>
				<xsl:text>')</xsl:text>
			</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'LocationAddressLine1', type: 'string', mapping: 'LocationAddressLine1'},</xsl:text>
					<xsl:text>{name: 'LocationAddressLine2', type: 'string', mapping: 'LocationAddressLine2'},</xsl:text>
					<xsl:text>{name: 'City', type: 'string', mapping: 'LocationCity'},</xsl:text>
					<xsl:text>{name: 'State', type: 'string', mapping: 'LocationStateCode'},</xsl:text>
					<xsl:text>{name: 'LocationPostalCode', type: 'string', mapping: 'LocationPostalCode'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_LocationAddressLine1')"/>
					<xsl:text>'), dataIndex: 'LocationAddressLine1', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_LocationAddressLine2')"/>
					<xsl:text>'), dataIndex: 'LocationAddressLine2', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_City')"/>
					<xsl:text>'), dataIndex: 'City', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_State')"/>
					<xsl:text>'), dataIndex: 'State', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_LocationPostalCode')"/>
					<xsl:text>'), dataIndex: 'LocationPostalCode', sortable: true}]</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
	</xsl:template>

	<xsl:template match="LocationInvolvement" name="LocationInvolvement">
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>partyRecordData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyHistoryDataList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>15</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>_partyMappedData/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>LocationInvolvement</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@id</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemSelected</xsl:text>
				</xsl:with-param>
			<xsl:with-param name="toolBarTitle">
				<xsl:text>DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_LocationInvolvement_ToolBarTitle')"/>
				<xsl:text>')</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'ObjectTypeCode', type: 'string', mapping: 'ObjectTypeCode'},</xsl:text>
				<xsl:text>{name: 'ObjectId', type: 'string', mapping: 'ObjectId'},</xsl:text>
				<xsl:text>{name: 'RoleCode', type: 'string', mapping: 'RoleCode'},</xsl:text>
				<xsl:text>{name: 'LocationInvolvementTypeCode', type: 'string', mapping: 'LocationInvolvementTypeCode'},</xsl:text>
				<xsl:text>{name: 'EffectiveDate', type: 'string', mapping: 'EffectiveDate'},</xsl:text>
				<xsl:text>{name: 'ExpirationDate', type: 'string', mapping: 'ExpirationDate'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_ObjectTypeCode')"/>
				<xsl:text>'), dataIndex: 'ObjectTypeCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_ObjectId')"/>
				<xsl:text>'), dataIndex: 'ObjectId', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_RoleCode')"/>
				<xsl:text>'), dataIndex: 'RoleCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_LocationInvolvementTypeCode')"/>
				<xsl:text>'), dataIndex: 'LocationInvolvementTypeCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_EffectiveDate')"/>
				<xsl:text>'), dataIndex: 'EffectiveDate', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_ExpirationDate')"/>
				<xsl:text>'), dataIndex: 'ExpirationDate', sortable: true}]</xsl:text>
			</xsl:with-param>
			</xsl:call-template>
	</xsl:template>

	<xsl:template match="PartyEmail" name="PartyEmail">
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>partyRecordData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyHistoryDataList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>15</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>_partyMappedData/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>PartyEmail</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@id</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemSelected</xsl:text>
				</xsl:with-param>
			<xsl:with-param name="toolBarTitle">
				<xsl:text>DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Email_ToolBarTitle')"/>
				<xsl:text>')</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'EmailTypeCode', type: 'string', mapping: 'EmailTypeCode'},</xsl:text>
				<xsl:text>{name: 'EmailAddress', type: 'string', mapping: 'EmailAddress'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_EmailTypeCode')"/>
				<xsl:text>'), dataIndex: 'EmailTypeCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_EmailAddress')"/>
				<xsl:text>'), dataIndex: 'EmailAddress', sortable: true}]</xsl:text>
			</xsl:with-param>
			</xsl:call-template>
	</xsl:template>

	<xsl:template match="PartyPhone" name="PartyPhone">
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>partyRecordData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyHistoryDataList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>15</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>_partyMappedData/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>PartyPhone</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@id</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:choose>
						<xsl:when test="$action = 'add'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">'AdvSearch'</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Grid.partyMappedItemSelected</xsl:text>
				</xsl:with-param>
			<xsl:with-param name="toolBarTitle">
				<xsl:text>DCT.T('</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Phone_ToolBarTitle')"/>
				<xsl:text>')</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'PhoneTypeCode', type: 'string', mapping: 'PhoneTypeCode'},</xsl:text>
				<xsl:text>{name: 'PhoneNumber', type: 'string', mapping: 'PhoneNumber'},</xsl:text>
				<xsl:text>{name: 'PhoneExtension', type: 'string', mapping: 'PhoneExtension'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PhoneTypeCode')"/>
				<xsl:text>'), dataIndex: 'PhoneTypeCode', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PhoneNumber')"/>
				<xsl:text>'), dataIndex: 'PhoneNumber', sortable: true},</xsl:text>
				<xsl:text>{text: DCT.T('</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Column_PhoneExtension')"/>
				<xsl:text>'), dataIndex: 'PhoneExtension', sortable: true}]</xsl:text>
			</xsl:with-param>
			</xsl:call-template>
	</xsl:template>

	<!-- Button Row - partyImportActions -->
	<xsl:template name="partyImportActions">
		<xsl:param name="partyType"/>
		<xsl:variable name="partyImportButtonText">
			<xsl:choose>
				<xsl:when test="$partyType = 'Party'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Party_ButtonText')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'Location'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Location_ButtonText')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'LocationInvolvement'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Involvement_ButtonText')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyEmail'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Email_ButtonText')"/>
				</xsl:when>
				<xsl:when test="$partyType = 'PartyPhone'">
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Phone_ButtonText')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_Generic_ButtonText')"/>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:variable>
		<xsl:variable name="action" select="/page/content/ExpressCache/PartyRecordMappingAction" />
		<xsl:variable name="parentIsPopup" select="/page/content/ExpressCache/ParentIsPopup" />
		<div id="importRecordToolbar" class="g-btn-bar">
			<xsl:if test="$action = 'add'">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">nSelectAll</xsl:with-param>
					<xsl:with-param name="id">selectAll</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Grid.selectAllRows();</xsl:with-param>
					<xsl:with-param name="caption" select="xslNsODExt:getDictRes('SelectAll')"/>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">nClearAll</xsl:with-param>
					<xsl:with-param name="id">clearAll</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Grid.clearAllRows();</xsl:with-param>
					<xsl:with-param name="caption" select="xslNsODExt:getDictRes('ClearAll')"/>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nImportCancel</xsl:with-param>
				<xsl:with-param name="id">importCancel</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:choose>
						<xsl:when test="/page/content/ExpressCache/ImportPartyDataToPolicyFromPopup=1">  			
							<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
							<xsl:value-of select="/page/state/quoteID"/>
							<xsl:text>','','1');</xsl:text>								
						</xsl:when>
						<xsl:otherwise>		
							<xsl:text>DCT.Util.closePartyPopup();</xsl:text>	
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>								
				<xsl:with-param name="caption" select="xslNsODExt:getDictRes('Cancel')"/>
				<xsl:with-param name="class">floatRight padRight</xsl:with-param>
			</xsl:call-template>

					

			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nImportSelected</xsl:with-param>
				<xsl:with-param name="id">importSelected</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Util.importSelectedPartyRecords('partyImportPartyMappedData', 'importSelected','</xsl:text>
					<xsl:value-of select="$parentIsPopup"/>
					<xsl:text>');</xsl:text></xsl:with-param>
				<xsl:with-param name="caption" select="$partyImportButtonText"/>
				<xsl:with-param name="class">btnDisabled floatRight</xsl:with-param>
			</xsl:call-template>
		</div>		
	</xsl:template>
	
</xsl:stylesheet>
