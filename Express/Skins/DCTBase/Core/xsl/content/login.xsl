﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\dctLocalizedDictionary.xsl"/>
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="/page/popUp = 1 and /page/state/CalledFromExternalSystem = 0">
				<xsl:call-template name="reloadPageInParentWindow"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildContainer">
					<xsl:with-param name="noActionBar">1</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle"></xsl:with-param>
			<xsl:with-param name="pageInstruction"></xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildQuickSearch"></xsl:template>
	<xsl:template name="buildProductNavigation"></xsl:template>
	<xsl:template name="buildActionsNavigation"></xsl:template>
	<xsl:template name="buildPageContent">
		<div id="companyLogo"></div>
		<div id="brandLogo">
			<xsl:value-of select="xslNsODExt:getDictRes('CompanyName')"/>
		</div>
		<xsl:if test="(/page/state/AllowMultiLanguageScreens = '1') and (/page/settings/Controls/MultipleLanguages/@enabled = '1')">
			<div id="loginLanguageComboContainer">
				<div id="loginLanguageComboCaption" >
					<label>
						<xsl:value-of select="xslNsODExt:getDictRes('SelectLanguage')"/>
					</label>
				</div>
				<xsl:call-template name="buildLanguageCombo">
					<xsl:with-param name="imageComboContainerId">loginImageComboContainerId</xsl:with-param>
					<xsl:with-param name="imageComboInputId">loginImageComboInputId</xsl:with-param>
					<xsl:with-param name="imageComboHiddenIName">loginImageComboHidden</xsl:with-param>
					<xsl:with-param name="imageComboContainerClass">localeFlagsBanner</xsl:with-param>
				</xsl:call-template>
			</div>
		</xsl:if>
		<div id="Logon">
			<div id="logonFields">
				<div class="logonTextFieldGroup">
					<label id="username_captionText" for="username">
						<xsl:value-of select="xslNsODExt:getDictRes('Username_colon')"/>
					</label>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">username</xsl:with-param>
						<xsl:with-param name="name">_username</xsl:with-param>
						<xsl:with-param name="size">15</xsl:with-param>
						<xsl:with-param name="maxlength">50</xsl:with-param>
						<xsl:with-param name="controlClass">loginTextField</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/state/user/name"/>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctSecure</xsl:with-param>
								<xsl:with-param name="type">boolean</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="/page/state/@secure='1'">
											<xsl:text>true</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>false</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
				<div class="logonTextFieldGroup">
					<label id="password_captionText" for="password">
						<xsl:value-of select="xslNsODExt:getDictRes('Password_colon')"/>
					</label>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">password</xsl:with-param>
						<xsl:with-param name="name">_password</xsl:with-param>
						<xsl:with-param name="size">15</xsl:with-param>
						<xsl:with-param name="maxlength">50</xsl:with-param>
						<xsl:with-param name="controlClass">loginTextField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctSecure</xsl:with-param>
								<xsl:with-param name="type">boolean</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="/page/state/@secure='1'">
											<xsl:text>true</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>false</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="type">password</xsl:with-param>
					</xsl:call-template>
					<xsl:variable name="href">
						<xsl:choose>
							<xsl:when test="/page/state/@secure='1'">
								<xsl:text>DCT.Submit.submitPageAction('welcome','login');</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DCT.Submit.submitAction('login');</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">home</xsl:with-param>
						<xsl:with-param name="id">home</xsl:with-param>
						<xsl:with-param name="onclick" select="$href"/>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Login')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
				<div id="rememberGroup">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="id">rememberCheckBoxGroup</xsl:with-param>
						<xsl:with-param name="class">boldText</xsl:with-param>
						<xsl:with-param name="checkbox">
							<xsl:call-template name="buildCheckBoxObject">
								<xsl:with-param name="elementClass">boldText</xsl:with-param>
								<xsl:with-param name="fieldID">remember</xsl:with-param>
								<xsl:with-param name="name">_remember</xsl:with-param>
								<xsl:with-param name="inputValue">yes</xsl:with-param>
								<xsl:with-param name="dctClassName">dctloginremembercheckbox</xsl:with-param>
								<xsl:with-param name="boxLabel">
									<xsl:value-of select="xslNsODExt:getDictRes('RememberUsername')"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</div>
				<div id="NewAgentsInformation">
					<a href="javascript:;" id="signup" name="signup" onclick="DCT.Submit.submitPage('signup');">
						<xsl:value-of select="xslNsODExt:getDictRes('GetAgencyAccount')"/>
					</a>
				</div>
			</div>
		</div>
	</xsl:template>

</xsl:stylesheet>