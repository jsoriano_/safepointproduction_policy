﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:variable name="helpImage"/>
	<xsl:variable name="cultureFormat">
		<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
	</xsl:variable>

	<xsl:template name="submissionDownloadList">
		<xsl:variable name="formatMask" select="xslNsExt:formatMaskToUse('', '', '0')"/>
		<div id="downloadGridList" title="Download">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="toolBarTitle">DCT.T('Downloads')</xsl:with-param>

				<xsl:with-param name="gridID">
					<xsl:text>submissionDownloadListId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="className">
					<xsl:text>submissionDownloadGridPanel</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>submissionDownloadList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>10</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>Downloads/@totalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showItemsPerPageCombo">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>Downloads/Download</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>SubmissionId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'Status', type: 'string'},</xsl:text>
					<xsl:text>{name: 'SubmissionId', type: 'string'},</xsl:text>
					<xsl:text>{name: 'Date', type: 'date', dateFormat:'c'},</xsl:text>
					<xsl:text>{name: 'TransactionDate', type: 'string'},</xsl:text>
					<xsl:text>{name: 'AgentName', type: 'string'},</xsl:text>
					<xsl:text>{name: 'QuoteID', type: 'string'},</xsl:text>
					<xsl:text>{name: 'LOB', type: 'string'},</xsl:text>
					<xsl:text>{name: 'PolicyNumber', type: 'string'},</xsl:text>
					<xsl:text>{name: 'InsuredName', type: 'string'},</xsl:text>
					<xsl:text>{name: 'Logs', type: 'string'},</xsl:text>
					<xsl:text>{name: 'EffectiveDate', type: 'date', dateFormat:'c'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T('Status'), dataIndex: 'Status', sortable: true, width: 20},</xsl:text>
					<xsl:text>{text: DCT.T('SubmissionIDAbbreviated'), dataIndex: 'SubmissionId', sortable: true, width: 25},</xsl:text>
					<xsl:text>{text: DCT.T('DownloadDateAbbreviated'), dataIndex: 'Date', sortable: true, width: 70, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
					<xsl:text>, g:i a')},</xsl:text>
					<xsl:text>{text: DCT.T('AgentName'), dataIndex: 'AgentName', sortable: true, width: 40},</xsl:text>
					<xsl:text>{text: DCT.T('LOB'), dataIndex: 'LOB', sortable: true, width:20},</xsl:text>
					<xsl:text>{text: DCT.T('TransactionDate'), dataIndex: 'TransactionDate', sortable: true, width: 60, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
					<xsl:text>')},</xsl:text>
					<xsl:text>{text: DCT.T('PolicyQuoteNumSymbol'), dataIndex: 'PolicyNumber', sortable: true, width: 40, renderer: DCT.Grid.submissionPolicyColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('InsuredName'), dataIndex: 'InsuredName', sortable: true, width: 100},</xsl:text>
					<xsl:text>{text: DCT.T('EffDate'), dataIndex: 'EffectiveDate', sortable: true, width: 30, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
					<xsl:text>')}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.submissionLogRowRenderer</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="initialShowPreview">false</xsl:with-param>
				<xsl:with-param name="showPreviewToolTip">
					<xsl:value-of select="xslNsODExt:getDictRes('ShowDetail')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
</xsl:stylesheet>