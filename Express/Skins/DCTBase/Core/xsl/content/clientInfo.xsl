﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ClientInformationPage')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:for-each select="/page/content/clientDetails">
			<xsl:call-template name="clientDetailWidget"/>
		</xsl:for-each>
		<!--<xsl:call-template name="buildPortfolioList" />
		<xsl:apply-templates select="/page/content/policyList" />-->
		<xsl:call-template name="buildPoliciesAndQuotes" />
	</xsl:template>

	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_ClientID" id="_ClientID" value="{/page/content/clientDetails/clientID|page/state/clientID}"/>
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value=""/>
		<input type="hidden" name="_PortfolioID" id="_PortfolioID" value="" />
		<input type="hidden" name="_UsingPortfolio" id="_usingPortfolio" value="0" />
		<input type="hidden" name="_FirstTime" id="_FirstTime" value="0" />
		<input type="hidden" name="_UseClientID" id="_UseClientID" value="0" />
	</xsl:template>
	
	<xsl:template name="clientDetailWidget">
		<xsl:call-template name="buildClientDetailTable"/>
	</xsl:template>

	<xsl:template name="buildClientDetailTable">
		<h2>
			<xsl:value-of select="xslNsODExt:getDictRes('ClientDetails')" />
		</h2>
		<div id="clientDetailSection">
			<table class="formTbl">
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')" />
						</label>
					</td>
					<td>
						<div>
							<xsl:value-of select="name"/>
						</div>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumber_colon')" />
						</label>
					</td>
					<td>
						<div>
							<span id="phoneNumber">
								<xsl:value-of select="phoneNumber"/>
							</span>
							<span id="phoneExt">
								<xsl:value-of select="phoneExtension"/>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('ClientId_colon')" />
						</label>
					</td>
					<td>
						<div>
							<xsl:value-of select="clientID"/>
						</div>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Address_colon')" />
						</label>
					</td>
					<td>
						<div>
							<xsl:value-of select="mailingAddress/address1"/>
						</div>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PrimaryContact_colon')" />
						</label>
					</td>
					<td>
						<div>
							<xsl:value-of select="primaryContact"/>
						</div>
					</td>
					<td class="detailsLabel"></td>
					<td>
						<div>
							<xsl:choose>
								<xsl:when test="mailingAddress/address2 and mailingAddress/address2 != ''">
									<xsl:value-of select="mailingAddress/address2"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="buildCityState"></xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel"></td>
					<td></td>
					<td class="detailsLabel"></td>
					<td>
						<div>
							<xsl:if test="mailingAddress/address2 and mailingAddress/address2 != '' ">
								<xsl:call-template name="buildCityState"></xsl:call-template>
							</xsl:if>
						</div>
					</td>
				</tr>
			</table>
			<xsl:call-template name="buildClientDetailAdditionalActionsSection"/>
		</div>
	</xsl:template>
	<xsl:template name="buildCityState">
		<span>
			<xsl:value-of select="mailingAddress/city"/>,
		</span>
		<span>
			<xsl:value-of select="mailingAddress/state"/>
		</span>
		<span style="padding-left: 10px;">
			<xsl:value-of select="mailingAddress/ZIP"/>
		</span>
	</xsl:template>
	<xsl:template name="buildClientDetailAdditionalActionsSection">
    <xsl:if test="not($IsReadOnly)">
		  <xsl:call-template name="makeButton">
			<xsl:with-param name="name">clientDetailEditClientA</xsl:with-param>
			<xsl:with-param name="id">clientDetailEditClientA</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Submit.editClient('</xsl:text>
				<xsl:value-of select="/page/content/clientDetails/clientID"/>
				<xsl:text>');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="xslNsODExt:getDictRes('UpdateClientDetails')" />
			</xsl:with-param>
			<xsl:with-param name="type">hyperlink</xsl:with-param>
		</xsl:call-template>
    </xsl:if> 
	</xsl:template>

	<xsl:template name="buildPoliciesAndQuotes">

		<div id="portfolioQuoteList">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('Portfolios')" />
			</h2>
			<xsl:variable name="RoleBasedSecurity" select="/page/state/@roleSecurity=1"/>
			<xsl:variable name="PortfolioWriteAccess" select="not($RoleBasedSecurity and /page/state[@securityLevel&gt;='1']) or ($RoleBasedSecurity and /page/content/portfolios/@writeAccess='True')"/>
			<xsl:variable name="PortfolioReadAccess" select="not($RoleBasedSecurity and /page/state[@securityLevel&gt;='1']) or ($RoleBasedSecurity and /page/content/portfolios/@readAccess='True')"/>
			<xsl:choose>
				<xsl:when test="count(page/content/portfolios/portfolio)&gt;0">
					<!-- Display all of the portfolios w/ polices -->
					<xsl:for-each select="page/content/portfolios/portfolio">
						<xsl:if test="count(portfolioPolicyList/policy)&gt;0">
							<xsl:call-template name="portfolio">
								<xsl:with-param name="PortfolioWriteAccess" select="$PortfolioWriteAccess"/>
								<xsl:with-param name="PortfolioReadAccess" select="$PortfolioReadAccess"/>
							</xsl:call-template>
							<xsl:call-template name="portfolioPolicyList"/>
						</xsl:if>
					</xsl:for-each>
					<!-- Display the empty portfolios at the end of the list -->
					<xsl:for-each select="page/content/portfolios/portfolio">
						<xsl:if test="count(portfolioPolicyList/policy)=0">
							<xsl:call-template name="portfolio">
								<xsl:with-param name="PortfolioWriteAccess" select="$PortfolioWriteAccess"/>
								<xsl:with-param name="PortfolioReadAccess" select="$PortfolioReadAccess"/>
							</xsl:call-template>
						</xsl:if>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<p>
						<xsl:value-of select="xslNsODExt:getDictRes('NoPortfoliosExistForClient')" />
					</p>
				</xsl:otherwise>
			</xsl:choose>
      <xsl:if test="not($IsReadOnly)">
			  <xsl:call-template name="makeButton">
				  <xsl:with-param name="name">addPortfolioA</xsl:with-param>
				  <xsl:with-param name="id">addPortfolioA</xsl:with-param>
				  <xsl:with-param name="onclick">
				  <xsl:text>DCT.Submit.addNewPortfolio('</xsl:text>
				  <xsl:value-of select="/page/content/clientDetails/clientID"/>
				  <xsl:text>','</xsl:text>
				  <xsl:value-of select="$newQuoteContentPage"/>
				  <xsl:text>')</xsl:text>
				  </xsl:with-param>
				  <xsl:with-param name="caption">
					  <xsl:value-of select="xslNsODExt:getDictRes('AddPortfolio')" />
				  </xsl:with-param>
				  <xsl:with-param name="type">hyperlink</xsl:with-param>
			  </xsl:call-template>
      </xsl:if >  
		</div>

		<div id="quoteList">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('PoliciesAndQuotes')" />
			</h2>
			<xsl:call-template name="policyList"/>
		</div>
		<xsl:if test="/page/downloadPolicyStatus">
			<script type="text/javascript">
        Ext.onReady(function(){ new DCT.AlertMessage({ title: DCT.T('DownloadStatus'), msg: '<xsl:value-of select="/page/downloadPolicyStatus"/>' }).show();})
      </script>
		</xsl:if>
	</xsl:template>	
	
</xsl:stylesheet>