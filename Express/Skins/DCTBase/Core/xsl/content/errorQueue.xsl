﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentBillingPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ErrorQueue')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="createBillingMainArea">
			<xsl:with-param name="displayNotes">false</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:call-template name="createSubMenuArea"/>
	</xsl:template>
	<xsl:template name="buildBillingMainAreaContent">
		<xsl:call-template name="buildErrorQueueContent"/>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
	</xsl:template>
	<xsl:template name="buildErrorQueueContent">
		<xsl:call-template name="buildErrorFilters"/>
		<!--<br/>-->
		<xsl:call-template name="buildErrorList"/>
		<!--<br/>-->
		<xsl:call-template name="buildErrorActions"/>
	</xsl:template>
	<xsl:template name="buildErrorFilters">
		<div id="errorFilters">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('FilterErrorList')"/>
			</h2>
			<div id="tblErrorFilters" class="downLayout">
				<div class="downFieldGroup">
					<div class="downBeforeFormLabel">
						<span>
							<xsl:value-of select="xslNsODExt:getDictRes('StartDate')"/>
						</span>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="type">date</xsl:with-param>
							<xsl:with-param name="dateFormat">
								<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
							</xsl:with-param>
							<xsl:with-param name="id">errorStartDateField</xsl:with-param>
							<xsl:with-param name="name">_errorStartDate</xsl:with-param>
							<xsl:with-param name="controlClass">systemDateField</xsl:with-param>
							<xsl:with-param name="required">1</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downBeforeFormLabel">
						<span>
							<xsl:value-of select="xslNsODExt:getDictRes('EndDate')"/>
						</span>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">errorEndDateField</xsl:with-param>
							<xsl:with-param name="name">_errorEndDate</xsl:with-param>
							<xsl:with-param name="type">date</xsl:with-param>
							<xsl:with-param name="dateFormat">
								<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">systemDateField</xsl:with-param>
							<xsl:with-param name="required">1</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downBeforeFormLabel">
						<span>
							<xsl:value-of select="xslNsODExt:getDictRes('Type_colon')"/>
						</span>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">errorTypeField</xsl:with-param>
							<xsl:with-param name="name">_errorType</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option>
									<xsl:attribute name="value">
										<xsl:text></xsl:text>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes('All')"/>
								</option>
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_ERRORTYPE']/ListEntry">
									<option>
										<xsl:attribute name="value">
											<xsl:value-of select="@Code"/>
										</xsl:attribute>
										<xsl:value-of select="@Description"/>
									</option>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text></xsl:text>
							</xsl:with-param>
							<xsl:with-param name="watermark">
								<xsl:value-of select="xslNsODExt:getDictRes('All')"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">basicComboField</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downBeforeFormLabel">
						<span>
							<xsl:value-of select="xslNsODExt:getDictRes('Status_colon')"/>
						</span>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">errorStatusField</xsl:with-param>
							<xsl:with-param name="name">_errorStatus</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option>
									<xsl:attribute name="value">
										<xsl:text></xsl:text>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes('All')"/>
								</option>
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_ERRORSTATUS']/ListEntry">
									<option>
										<xsl:attribute name="value">
											<xsl:value-of select="@Code"/>
										</xsl:attribute>
										<xsl:value-of select="@Description"/>
									</option>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text></xsl:text>
							</xsl:with-param>
							<xsl:with-param name="watermark">
								<xsl:value-of select="xslNsODExt:getDictRes('All')"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">basicComboField</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">name_RefreshErrors</xsl:with-param>
					<xsl:with-param name="id">id_RefreshErrors</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Grid.applyErrorFiltersToList('', Ext.getCmp('errorQueueList').getStore());</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('RefreshList')"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildErrorList">
		<div class="errorList" id="errorListId">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="gridID">
					<xsl:text>errorQueueList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>errorQueueList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">10</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>ExceptionsList/Paging/TotalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>ListItem/ItemData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>ExceptionId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'ExceptionDateTime', type: 'date', dateFormat: 'Y-m-dTH:i:s', mapping: 'ExceptionDateTime'},</xsl:text>
					<xsl:text>{name: 'ExceptionType', type: 'string', mapping: 'ExceptionType'},</xsl:text>
					<xsl:text>{name: 'ExceptionReferenceId', type: 'string', mapping: 'ExceptionReferenceId'},</xsl:text>
					<xsl:text>{name: 'Description', type: 'string', mapping: 'Description'},</xsl:text>
					<xsl:text>{name: 'ExceptionStatus', type: 'string', mapping: 'ExceptionStatus'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T("Date"), dataIndex: 'ExceptionDateTime', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
					<xsl:text>, g:i A')},</xsl:text>
					<xsl:text>{text: DCT.T("Type"), dataIndex: 'ExceptionType', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T("Reference"), dataIndex: 'ExceptionReferenceId', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T("Status"), dataIndex: 'ExceptionStatus', sortable: true},</xsl:text>
					<xsl:text>{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.viewErrorDetailsColumnRenderer}</xsl:text>
					<xsl:choose>
						<xsl:when test="/page/content/ResubmitPrivilege/@value='1'">
							<xsl:text>,{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.viewResubmitDetailsColumnRenderer}]</xsl:text>
						</xsl:when>
						<xsl:otherwise>
								<xsl:text>]</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectAllItems">true</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.errorDescriptionRowRenderer</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showToolBar">true</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:text>DCT.T('Errors')</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildErrorActions">
    <div class="downFieldGroup">
      <div class="downFormField">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">name_closeError</xsl:with-param>
				<xsl:with-param name="id">id_closeError</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Grid.applyErrorFiltersAndSubmit('closeErrors','errorQueue', Ext.getCmp('errorQueueList').getStore());</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('CloseSelectedItems')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
      <div class="downFormField">
	    <xsl:choose>
			<xsl:when test="/page/content/ResubmitPrivilege/@value='1'">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">name_submitError</xsl:with-param>
					<xsl:with-param name="id">id_submitError</xsl:with-param>
					<xsl:with-param name="onclick">
					<xsl:text>DCT.Grid.applyErrorFiltersAndSubmit('submitErrors','errorQueue', Ext.getCmp('errorQueueList').getStore());</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('ResubmitSelectedItems')"/>
					</xsl:with-param>
                 </xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
			
			</xsl:otherwise>
		</xsl:choose>
        
      </div>
    </div>
	</xsl:template>
</xsl:stylesheet>