﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>


	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:variable name="pageType" select="/page/content/@_pageType"/>
	<xsl:variable name="jobName" select="/page/content/@_jobName"/>
	<xsl:variable name="itemType">
		<xsl:choose>
			<xsl:when test="/page/content/batchLog/@itemType = 'Extended'">batchExtended</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="/page/content/@_pageType"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('BatchReports')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('ViewLogReportsBatchTransactions')"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildPageHeaderSection"/>
		<xsl:call-template name="buildPageSubNav"/>
		<xsl:choose>
			<xsl:when test="$jobName = ''">
				<xsl:call-template name="buildBatchProcessJobList"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="div">
					<xsl:attribute name="class">
						<xsl:text>toggleHeader</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="id">
						<xsl:text>batchListFilterHeader</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="onclick">
						<xsl:text>DCT.Util.toggleSection('batchListFilterSectionPanelCmp'); return false;</xsl:text>
					</xsl:attribute>
					<xsl:element name="div">
						<xsl:attribute name="class">
							<xsl:text>toggleHeaderInner</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:text>batchListFilterInnerHeader</xsl:text>
						</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes('BatchListFilters')"/>
					</xsl:element>
				</xsl:element>
				<div id="batchListFilterSectionPanel">
					<xsl:element name="div">
						<xsl:attribute name="class">collapsiblePanel</xsl:attribute>
						<xsl:attribute name="data-config">
							<xsl:text>{</xsl:text>
							<xsl:text>renderTo: "batchListFilterSectionPanel",</xsl:text>
							<xsl:text>id: "batchListFilterSectionPanelCmp",</xsl:text>
							<xsl:text>contentEl: "batchListFilterSection",</xsl:text>
							<xsl:text>collapsed: false</xsl:text>
							<xsl:text>}</xsl:text>
						</xsl:attribute>
					</xsl:element>
				</div>
				<div id="batchListFilterSection">
					<xsl:call-template name="buildHiddenKeys"/>
					<xsl:call-template name="buildBatchListQueryBuilderDialog"/>
					<xsl:apply-templates select="/page/content/keys"/>
				</div>
				<xsl:call-template name="buildPagingGridPanel">
					<xsl:with-param name="gridID">
						<xsl:text>batchLogList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="targetPage">
						<xsl:text>batchLogList</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="pageSize">
						<xsl:text>10</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordCount">
						<xsl:text>batchLog/@count</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordPath">
						<xsl:text>batchLogItem</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="recordID">
						<xsl:choose>
							<xsl:when test="$itemType = 'batchExtended'">
								<xsl:text>@pk</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>@itemID</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="columns">
						<xsl:choose>
							<xsl:when test="$itemType = 'batchPolicy'">
								<xsl:text>[{name: 'ClientName', type: 'string', mapping: '@ClientName'},</xsl:text>
								<xsl:text>{name: 'LOB', type: 'string', mapping: '@LOB'},</xsl:text>
								<xsl:text>{name: 'QuoteStatus', type: 'string', mapping: '@QuoteStatus'},</xsl:text>
								<xsl:text>{name: 'EffectiveDate', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: '@EffectiveDate'},</xsl:text>
								<xsl:text>{name: 'batchStatus', type: 'string', mapping: '@batchStatus'}]</xsl:text>
							</xsl:when>
							<xsl:when test="$itemType = 'batchExtended'">
								<xsl:text>[{name: 'itemID', type: 'string', mapping: '@itemID'},</xsl:text>
								<xsl:text>{name: 'batchStatus', type: 'string', mapping: '@batchStatus'}]</xsl:text>
							</xsl:when>
							<xsl:when test="$itemType = 'batchMessage'">
								<xsl:text>[{name: 'Action', type: 'string', mapping: '@Action'},</xsl:text>
								<xsl:text>{name: 'Sender', type: 'string', mapping: '@Sender'},</xsl:text>
								<xsl:text>{name: 'Subject', type: 'string', mapping: '@Subject'},</xsl:text>
								<xsl:text>{name: 'SentOn', type: 'date', dateFormat: 'Y-m-d H:i:s', mapping: '@SentOn'},</xsl:text>
								<xsl:text>{name: 'batchStatus', type: 'string', mapping: '@batchStatus'}]</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="columnModel">
						<xsl:choose>
							<xsl:when test="$itemType = 'batchPolicy'">
								<xsl:text>[{text: DCT.T('ClientName'), dataIndex: 'ClientName', sortable: true, renderer: DCT.Grid.batchClientColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('LOB'), dataIndex: 'LOB', sortable: true},</xsl:text>
								<xsl:text>{text: DCT.T('QuoteStatus'), dataIndex: 'QuoteStatus', sortable: true},</xsl:text>
								<xsl:text>{text: DCT.T('EffectiveDate'), width: 150, dataIndex: 'EffectiveDate', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
								<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
								<xsl:text>, g:i a')},{text: DCT.T('BatchStatus'), dataIndex: 'batchStatus', sortable: true}]</xsl:text>
							</xsl:when>
							<xsl:when test="$itemType = 'batchExtended'">
								<xsl:text>[{text: DCT.T('ExecutionType'), dataIndex: 'itemID', sortable: true, renderer: DCT.Grid.batchExecuteColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('SuccessfulRequests'), dataIndex: '', sortable: false, renderer: DCT.Grid.batchSuccessColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('FailedRequests'), dataIndex: '', sortable: false, renderer: DCT.Grid.batchFailureColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('TotalRequests'), dataIndex: '', sortable: false, renderer: DCT.Grid.batchTotalColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('BatchStatus'), dataIndex: 'batchStatus', sortable: true}]</xsl:text>
							</xsl:when>
							<xsl:when test="$itemType = 'batchMessage'">
								<xsl:text>[{text: DCT.T('Action'), dataIndex: 'Action', sortable: true, renderer: DCT.Grid.messageDescriptionColumnRenderer},</xsl:text>
								<xsl:text>{text: DCT.T('Sender'), dataIndex: 'Sender', sortable: true},</xsl:text>
								<xsl:text>{text: DCT.T('Subject'), dataIndex: 'Subject', sortable: true},</xsl:text>
								<xsl:text>{text: DCT.T('SentOn'), dataIndex: 'SentOn', sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
								<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
								<xsl:text>, g:i a')},{text: DCT.T('BatchStatus'), dataIndex: 'batchStatus', sortable: true}]</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageSubNav">
		<div id="pageSubNav">
			<ul class="subAction">
				<xsl:if test="$jobName != ''">
					<xsl:call-template name="buildTab">
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.batchSelectJob('</xsl:text>
							<xsl:value-of select="$jobName"/>
							<xsl:text>');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="$jobName"/>
						</xsl:with-param>
						<xsl:with-param name="current">1</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</ul>
		</div>
	</xsl:template>
	<xsl:template name="buildBatchProcessJobList">
		<div id="batchProcessJobList">
			<!-- Build the Array Grid -->
			<xsl:call-template name="buildNonPagingGridPanel">
				<xsl:with-param name="toolBarTitle">
					<xsl:text>'</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('Batches')"/>
					<xsl:text>'</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="gridID">
					<xsl:text>batchJobList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="storeFields">
					<xsl:text>['id',{name: 'imageDir', type: 'string', mapping: 1},</xsl:text>
					<xsl:text>{name: 'logName', type: 'string', mapping: 2},</xsl:text>
					<xsl:text>{name: 'totalCount', type: 'string', mapping: 3},</xsl:text>
					<xsl:text>{name: 'successCount', type: 'string', mapping: 4},</xsl:text>
					<xsl:text>{name: 'failureCount', type: 'string', mapping: 5}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{text: "", dataIndex: '', sortable: false, renderer: DCT.Grid.jobRowActionColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('LogName'), dataIndex: 'logName', sortable: true, renderer: DCT.Grid.logNameColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.T('Total'), dataIndex: 'totalCount', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('Success'), dataIndex: 'successCount', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('Fail'), dataIndex: 'failureCount', sortable: true}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="gridData">
					<xsl:text>[</xsl:text>
					<xsl:for-each select="/page/content/listSummary/job">
						<xsl:text>[</xsl:text>
						<xsl:value-of select="@pk"/>
						<xsl:text>,"</xsl:text>
						<xsl:value-of select="$imageDir"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="@logName"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="@totalCount"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="@successCount"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="@failureCount"/>
						<xsl:text>"]</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showToolBar">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_pageType" id="_pageType" value="{/page/content/@_pageType}"/>
		<input type="hidden" name="_jobName" id="_jobName" value="{$jobName}"/>
		<input type="hidden" name="_startIndex" id="_startIndex" value="{/page/content/@_startIndex}"/>
		<input type="hidden" name="_displayCount" id="_displayCount" value="{/page/content/@_displayCount}"/>
		<input type="hidden" name="_jobItemID" id="_jobItemID" value="{/page/content/@_jobItemID}"/>
		<input type="hidden" name="_orderBy" id="_orderBy" value="{/page/content/keys/orderKey/@name}"/>
		<input type="hidden" name="_orderDirection" id="_orderDirection" value="{/page/content/keys/orderKey/@direction}"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="0"/>
		<input type="hidden" name="_messageID" id="_messageID" value="0"/>
	</xsl:template>
	<xsl:template name="buildBatchListQueryBuilderDialog">
		<xsl:variable name="itemType">
			<xsl:choose>
				<xsl:when test="/page/content/batchLog/@itemType = 'Extended'">batchExtended</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/page/content/@_pageType"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div id="queryBuilderSection" class="queryBuilderDiv">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('QueryBuilder')"/>
			</h2>
			<div id="queryBuilderMain">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">keyConnectorField</xsl:with-param>
					<xsl:with-param name="name">_keyconnector</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="and">
							<xsl:value-of select="xslNsODExt:getDictRes('And')"/>
						</option>
						<xsl:if test="count(/page/content/keys/key)&gt;1">
							<option value="or">
								<xsl:value-of select="xslNsODExt:getDictRes('Or')"/>
							</option>
						</xsl:if>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">lastSearchItem</xsl:with-param>
					<xsl:with-param name="name">_keyname</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="width">120</xsl:with-param>
					<xsl:with-param name="optionlist">
						<xsl:if test="$itemType='batchPolicy'">
							<option value="batchStatus">
								<xsl:value-of select="xslNsODExt:getDictRes('Status')"/>
							</option>
							<option value="Party.PartyLegacyName">
								<xsl:value-of select="xslNsODExt:getDictRes('ClientName')"/>
							</option>
							<option value="Quote.LOB">
								<xsl:value-of select="xslNsODExt:getDictRes('LOB')"/>
							</option>
							<option value="Quote.Status">
								<xsl:value-of select="xslNsODExt:getDictRes('QuoteStatus')"/>
							</option>
							<option value="Quote.EffectiveDate">
								<xsl:value-of select="xslNsODExt:getDictRes('EffectiveDate')"/>
							</option>
						</xsl:if>
						<xsl:if test="$itemType='batchExtended'">
							<option value="batchStatus">
								<xsl:value-of select="xslNsODExt:getDictRes('Status')"/>
							</option>
							<option value="itemID">
								<xsl:value-of select="xslNsODExt:getDictRes('ExecutionType')"/>
							</option>
						</xsl:if>
						<xsl:if test="$itemType='batchMessage'">
							<option value="batchStatus">
								<xsl:value-of select="xslNsODExt:getDictRes('Status')"/>
							</option>
							<option value="Action">
								<xsl:value-of select="xslNsODExt:getDictRes('Action')"/>
							</option>
							<option value="Sender">
								<xsl:value-of select="xslNsODExt:getDictRes('Sender')"/>
							</option>
							<option value="Subject">
								<xsl:value-of select="xslNsODExt:getDictRes('Subject')"/>
							</option>
							<option value="SentOn">
								<xsl:value-of select="xslNsODExt:getDictRes('SentDate')"/>
							</option>
						</xsl:if>
					</xsl:with-param>
					<xsl:with-param name="controlClass">batchSearchTypeComboField</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:text>batchStatus</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">lastSearchOper</xsl:with-param>
					<xsl:with-param name="name">_keyop</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_SRCHFILTEROPT']/ListEntry">
							<option>
								<xsl:attribute name="value">
									<xsl:value-of select="@Code"/>
								</xsl:attribute>
								<xsl:value-of select="@Description"/>
							</option>
						</xsl:for-each>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:text>equal</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">lastSearchVal</xsl:with-param>
					<xsl:with-param name="name">_keyvalue</xsl:with-param>
					<xsl:with-param name="size">40</xsl:with-param>
					<xsl:with-param name="controlClass">batchSearchTextField</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">lastSearchDateVal</xsl:with-param>
					<xsl:with-param name="name">_keyvalue</xsl:with-param>
					<xsl:with-param name="size">40</xsl:with-param>
					<xsl:with-param name="type">date</xsl:with-param>
					<xsl:with-param name="dateFormat">
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
					</xsl:with-param>
					<xsl:with-param name="controlClass">batchSearchDateField</xsl:with-param>
					<xsl:with-param name="disabled">true</xsl:with-param>
					<xsl:with-param name="extraConfigItems">
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">hidden</xsl:with-param>
							<xsl:with-param name="type">boolean</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>true</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">msgTarget</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>qtip</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">addQueryFilter</xsl:with-param>
					<xsl:with-param name="id">addQueryFilter</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.addFilterAndQuery();</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="type">loneIcon</xsl:with-param>
					<xsl:with-param name="qtip">
						<xsl:value-of select="xslNsODExt:getDictRes('AddFilter')"/>
					</xsl:with-param>
					<xsl:with-param name="icon">add.png</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="/page/content/keys">
		<xsl:variable name="singlequote">
			<xsl:text>'</xsl:text>
		</xsl:variable>
		<xsl:if test="count(key) &gt; 0">
			<div id="queryFilterList" tabletype="acrossAboveOnce">
				<table id="tblCurrentFilters" class="formTable">
					<tr>
						<th colspan="5">
							<label>
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentFilters')"/>
							</label>
						</th>
					</tr>
					<xsl:for-each select="key">
						<tr>
							<td>
								<xsl:if test="@name!='logName'">
									<xsl:call-template name="makeButton">
										<xsl:with-param name="name">deleteFilterA</xsl:with-param>
										<xsl:with-param name="id">deleteFilterA</xsl:with-param>
										<xsl:with-param name="onclick">
											<xsl:text>DCT.Submit.delFilter('</xsl:text>
											<xsl:value-of select="position()"/>
											<xsl:text>'); DCT.Submit.resetNoQuery('batchProcess');</xsl:text>
										</xsl:with-param>
										<xsl:with-param name="icon">delete.png</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('RemoveThisFilter')"/>
										</xsl:with-param>
										<xsl:with-param name="type">loneIcon</xsl:with-param>
									</xsl:call-template>
								</xsl:if>
							</td>
							<td>
								<xsl:if test="position()!=1">
									<xsl:value-of select="@connect"/>
								</xsl:if>
							</td>
							<td>
								<xsl:choose>
									<xsl:when test="@name='batchStatus'">
										<xsl:value-of select="xslNsODExt:getDictRes('Status')"/>
									</xsl:when>
									<xsl:when test="@name='Party.PartyLegacyName'">
										<xsl:value-of select="xslNsODExt:getDictRes('ClientName')"/>
									</xsl:when>
									<xsl:when test="@name='Quote.LOB'">
										<xsl:value-of select="xslNsODExt:getDictRes('LOB')"/>
									</xsl:when>
									<xsl:when test="@name='Quote.Status'">
										<xsl:value-of select="xslNsODExt:getDictRes('QuoteStatus')"/>
									</xsl:when>
									<xsl:when test="@name='Quote.EffectiveDate'">
										<xsl:value-of select="xslNsODExt:getDictRes('EffectiveDate')"/>
									</xsl:when>
									<xsl:when test="@name='itemID'">
										<xsl:value-of select="xslNsODExt:getDictRes('ExecutionType')"/>
									</xsl:when>
									<xsl:when test="@name='Action'">
										<xsl:value-of select="xslNsODExt:getDictRes('Action')"/>
									</xsl:when>
									<xsl:when test="@name='Sender'">
										<xsl:value-of select="xslNsODExt:getDictRes('Sender')"/>
									</xsl:when>
									<xsl:when test="@name='Subject'">
										<xsl:value-of select="xslNsODExt:getDictRes('Subject')"/>
									</xsl:when>
									<xsl:when test="@name='SentOn'">
										<xsl:value-of select="xslNsODExt:getDictRes('SentDate')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@name"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td>
									<xsl:value-of select="@op"/>
							</td>
							<td>
								<xsl:choose>
									<xsl:when test="@name='SentOn' or @name='Quote.EffectiveDate'">
										<xsl:variable name="translateDt">
											<xsl:value-of select="translate(@value,$singlequote,'')"/>
										</xsl:variable>
										<xsl:text>'</xsl:text>
										<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', $translateDt)"/>
										<xsl:text>'</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@value"/>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<xsl:if test="@name='logName'">
								<td>
									<input type="hidden" name="_keyconnector" id="_keyconnector" value="{@connect}"/>
									<input type="hidden" name="_keyname" id="__keyname{position()}" value="{@name}"/>
									<input type="hidden" name="_keyop" id="_keyop" value="{@op}"/>
									<input type="hidden" name="_keyvalue" id="_keyvalue" value="{@value}"/>
								</td>
							</xsl:if>
						</tr>
					</xsl:for-each>
				</table>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildHiddenKeys">
		<xsl:for-each select="/page/content/keys/key">
			<xsl:if test="@name!='logName'">
				<div id="hiddenQueryKeys">
					<input type="hidden" name="_keyconnector" id="_keyconnector" value="{@connect}"/>
					<input type="hidden" name="_keyname" id="__keyname{position()}" value="{@name}"/>
					<input type="hidden" name="_keyop" id="_keyop" value="{@op}"/>
					<input type="hidden" name="_keyvalue" id="_keyvalue" value="{@value}"/>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>