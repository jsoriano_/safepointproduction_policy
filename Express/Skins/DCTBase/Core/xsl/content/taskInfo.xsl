﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonTasks.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('MyTasks')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction"></xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildMainAreaContent">
		<xsl:call-template name="buildTaskSubHeader">
			<xsl:with-param name="resourceKey">MyTasks</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="buildQueueToolbar">
			<xsl:with-param name="defaultCategory">
				<xsl:choose>
					<xsl:when test="/page/content/portals/portal[@active = '1']/@name = 'Billing'">
						<xsl:text>ACT</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text></xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="queueType">assigned</xsl:with-param>
			<xsl:with-param name="referenceNumber">PolicyQuoteNumSymbol</xsl:with-param>
			<xsl:with-param name="categoryWidth">110</xsl:with-param>
			<xsl:with-param name="queueEntityId">''</xsl:with-param>
			<xsl:with-param name="queueGridId">'QueueAssigned'</xsl:with-param>
		</xsl:call-template>
		
		<xsl:call-template name="buildTaskQueueWidget">
			<xsl:with-param name="returnPage">taskInfo</xsl:with-param>
			<xsl:with-param name="queueType">Assigned</xsl:with-param>
			<xsl:with-param name="resourceKey">MyTasks</xsl:with-param>
			<xsl:with-param name="targetPage">taskList</xsl:with-param>
			<xsl:with-param name="className">tasksPagingGridPanel</xsl:with-param>
			<xsl:with-param name="classMethod">DCT.Util.refreshAssignedTaskQueue</xsl:with-param>			
		</xsl:call-template>

		<xsl:call-template name="buildTaskSubHeader">
			<xsl:with-param name="resourceKey">TasksAvailableForPickup</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="buildQueueToolbar">
			<xsl:with-param name="defaultCategory">
				<xsl:choose>
					<xsl:when test="/page/content/portals/portal[@active = '1']/@name = 'Billing'">
						<xsl:text>ACT</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text></xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="queueType">unassigned</xsl:with-param>
			<xsl:with-param name="referenceNumber">PolicyQuoteNumSymbol</xsl:with-param>
			<xsl:with-param name="categoryWidth">105</xsl:with-param>
			<xsl:with-param name="queueEntityId">Ext.getCmp('unassignedQueueEntityId').getValue()</xsl:with-param>
			<xsl:with-param name="queueGridId">'QueueUnassigned'</xsl:with-param>
		</xsl:call-template>		

		<xsl:call-template name="buildTaskQueueWidget">
			<xsl:with-param name="returnPage">taskInfo</xsl:with-param>
			<xsl:with-param name="queueType">Unassigned</xsl:with-param>
			<xsl:with-param name="resourceKey">TasksAvailableForPickupColumnCaption</xsl:with-param>
			<xsl:with-param name="targetPage">taskList</xsl:with-param>
			<xsl:with-param name="className">tasksPagingGridPanel</xsl:with-param>
			<xsl:with-param name="classMethod">DCT.Util.refreshUnassignedTaskQueue</xsl:with-param>			
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildHiddenFormFields">
	</xsl:template>
</xsl:stylesheet>