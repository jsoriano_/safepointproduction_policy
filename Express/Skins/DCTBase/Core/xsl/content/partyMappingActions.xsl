﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noHeader">1</xsl:with-param>
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<div id="pageTop">
			<div id="pageTitle">
				<xsl:value-of select="/page/content/recordActions/@caption" />
			</div>
		</div>
		<div id="fields">
			<xsl:attribute name="class">
				<xsl:text>styleWithOutPanels</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="/page/content/*[not(self::ExpressCache)]" />
		</div>
	</xsl:template>

	<xsl:template match="addMappedRecord" name="addMappedRecord">
		<xsl:call-template name="makeButton">
			<xsl:with-param name="type">hyperlink</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Util.doPartyRecordAction('content', 'partyPolicyList', '</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_SelectAHistoryRecord')"/>
				<xsl:text>');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="class">partyRecordAction</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_PartyAction_FromAnotherPolicy')"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:call-template name="makeButton">
			<xsl:with-param name="type">hyperlink</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Util.getPartyData('</xsl:text>
				<xsl:value-of select="/page/content/partyMappedAction/@partyId"/>
				<xsl:text>');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="class">partyRecordAction</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_PartyAction_FromExistingPolicy')"/>
			</xsl:with-param>
		</xsl:call-template>

		<xsl:call-template name="makeButton">
			<xsl:with-param name="name">nImportCancel</xsl:with-param>
			<xsl:with-param name="id">importCancel</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:choose>
					<xsl:when test="/page/content/ExpressCache/ImportPartyDataToPolicyFromPopup=1"> 					
						<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
						<xsl:value-of select="/page/state/quoteID"/>
						<xsl:text>','','1');</xsl:text>								
					</xsl:when>
					<xsl:otherwise>				
						<xsl:text>DCT.Util.closePartyPopup();</xsl:text>	
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="caption" select="xslNsODExt:getDictRes('Cancel')"/>
			<xsl:with-param name="class">floatRight padRight</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="updateMappedRecord">
		<xsl:call-template name="addMappedRecord"/>
	</xsl:template>
	
	<xsl:template match="policies">
		<xsl:apply-templates select="policy" />
		<xsl:apply-templates select="noPolicies" />
	</xsl:template>

	<xsl:template match="policy">
		<div class="partyHistoryPolicy">
			<div class="policyInfo">
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyNumber_colon')"/>
				<xsl:value-of select="PolicyNumber"/>
				<xsl:if test="Description2!=''">
					<xsl:text> - </xsl:text>
					<xsl:value-of select="Description2"/>
				</xsl:if>
			</div>
			<div class="policyTransactionList">
				<xsl:apply-templates select="transactions" />
			</div>
		</div>
	</xsl:template>

	<xsl:template match="transaction">
		<xsl:call-template name="makeButton">
			<xsl:with-param name="type">hyperlink</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Util.getPartyDataFromHistory('</xsl:text>
				<xsl:value-of select="HistoryID"/>
				<xsl:text>');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="class">partyRecordAction</xsl:with-param>
			<xsl:with-param name="caption">
				<xsl:value-of select="TypeCaption"/>
				<xsl:text> (</xsl:text>
				<xsl:value-of select="EffectiveDate"/>
				<xsl:text>)</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="noPolicies">
		<div class="noPolicies">
			<p>
				<xsl:value-of select="xslNsODExt:getDictRes('PartyImport_noPolicies')"/>
			</p>

			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nNoPoliciesButton</xsl:with-param>
				<xsl:with-param name="id">noPoliciesButton</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:choose>
						<xsl:when test="/page/content/ExpressCache/ImportPartyDataToPolicyFromPopup=1">  						
							<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
							<xsl:value-of select="/page/state/quoteID"/>
							<xsl:text>','','1');</xsl:text>								
						</xsl:when>
						<xsl:otherwise>					
							<xsl:text>DCT.Util.closePartyPopup();</xsl:text>		
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>						
				<xsl:with-param name="caption" select="xslNsODExt:getDictRes('Ok')"/>
				<xsl:with-param name="class">floatRight padRight</xsl:with-param>
			</xsl:call-template>

			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nImportCancel</xsl:with-param>
				<xsl:with-param name="id">importCancel</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:choose>
						<xsl:when test="/page/content/ExpressCache/ImportPartyDataToPolicyFromPopup=1">  
							<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
							<xsl:value-of select="/page/state/quoteID"/>
							<xsl:text>','','1');</xsl:text>	
						</xsl:when>
						<xsl:otherwise>					
							<xsl:text>DCT.Util.closePartyPopup();</xsl:text>					
						</xsl:otherwise>
					</xsl:choose>		
				</xsl:with-param>
				<xsl:with-param name="caption" select="xslNsODExt:getDictRes('Cancel')"/>
				<xsl:with-param name="class">floatRight padRight</xsl:with-param>
			</xsl:call-template>

		</div>
	</xsl:template>
	
</xsl:stylesheet>
