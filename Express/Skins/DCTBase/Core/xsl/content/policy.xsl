﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyDetails')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:text></xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<div id="policyGroup">
			<xsl:call-template name="policyDetailWidget"/>
		</div>
		<div id="tasksGroup">
			<input type="hidden" name="_typeOfSearchPolicy" id="_typeOfSearchPolicy" value="messages"/>
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('Tasks')"/>
			</h2>
			<div id="taskQueueToolbar">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">policyQueueEntityId</xsl:with-param>
					<xsl:with-param name="name">_entityId</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAllQueues')"/>
						</option>
						<xsl:for-each select="/page/content/QueueList/Queue">
							<option value="{EntityId}">
								<xsl:value-of select="Name"/>
							</option>
						</xsl:for-each>
					</xsl:with-param>
					<xsl:with-param name="controlClass">taskEntityComboField</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:choose>
							<xsl:when test="normalize-space(/page/content/ExpressCache/TaskManagement/@_entityId)!='' and number(/page/content/ExpressCache/TaskManagement/@_entityId)&gt;0">
								<xsl:value-of select="/page/content/ExpressCache/TaskManagement/@_entityId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text></xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="width">120</xsl:with-param>
					<xsl:with-param name="defaultValue">
						<xsl:choose>
							<xsl:when test="normalize-space(/page/content/ExpressCache/TaskManagement/@_entityId)!='' and number(/page/content/ExpressCache/TaskManagement/@_entityId)&gt;0">
								<xsl:value-of select="/page/content/ExpressCache/TaskManagement/@_entityId"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text></xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<xsl:call-template name="taskQueueWidget">
				<xsl:with-param name="id">TaskQueue</xsl:with-param>
				<xsl:with-param name="listMode">
					<xsl:text></xsl:text>
				</xsl:with-param>
				<xsl:with-param name="listTitle">
					<xsl:text>DCT.T('Tasks')</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="actionColumnRenderer">
					<xsl:text>function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.generalTaskActionColumnRenderer('policy', false, '</xsl:text>
					<xsl:value-of select="/page/state/user/userID"/>
					<xsl:text>', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="titleColumnRenderer">function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.taskTitleViewRenderer('policy', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }</xsl:with-param>
				<xsl:with-param name="showCheckbox">false</xsl:with-param>
				<xsl:with-param name="multipleSelect">false</xsl:with-param>
				<xsl:with-param name="className">tasksPagingGridPanel</xsl:with-param>
				<xsl:with-param name="classMethod">DCT.Util.refreshPolicyTaskQueue</xsl:with-param>				
			</xsl:call-template>
      <xsl:if test="not($IsReadOnly)">
			  <xsl:call-template name="makeButton">
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.openAddNewTaskDialog('</xsl:text>
					<xsl:value-of select="/page/content/@page"/>
					<xsl:text>');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('AddTask')"/>
				</xsl:with-param>
				<xsl:with-param name="name">name_NewTask</xsl:with-param>
				<xsl:with-param name="id">id_NewTask</xsl:with-param>
				<xsl:with-param name="type">standard</xsl:with-param>
			</xsl:call-template>
      </xsl:if>
		</div>
		<div id="messageGroup">
			<!-- Force filter for current quote/policy on the notifications grid -->
			<input type="hidden" name="_keynameNotifications" id="_keynameNotifications" value="quoteID"/>
			<input type="hidden" name="_keyvalueNotifications" id="_keyvalueNotifications" value="{/page/content/details/policy/@id}"/>
			<xsl:call-template name="messagesListWidget">
				<xsl:with-param name="listTitle">
					<xsl:value-of select="xslNsODExt:getDictRes('Notifications')"/>
				</xsl:with-param>
				<xsl:with-param name="newButtonText">
					<xsl:value-of select="xslNsODExt:getDictRes('SendNotification')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
		<div id="commentsGroup">
			<input type="hidden" name="_typeOfSearchPolicy" id="_typeOfSearchPolicy" value="comments"/>
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('Notes_noColon')"/>
			</h2>
			<div id="commentsToolbar">
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">commentFilter</xsl:with-param>
					<xsl:with-param name="name">_commentFilter</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="A">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAllNotes')"/>
						</option>
						<option value="O">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewOriginatorNotes')"/>
						</option>
						<option value="D">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewDeletedNotes')"/>
						</option>
					</xsl:with-param>
					<xsl:with-param name="controlClass">commentsFilterComboField</xsl:with-param>
					<xsl:with-param name="value">A</xsl:with-param>
					<xsl:with-param name="extraConfigItems">
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctGridId</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>policyComments</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
			</div>
			<xsl:call-template name="commentsWidget"/>
		</div>
		<div id="attachmentGroup">
			<xsl:call-template name="attachmentsWidget"/>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<!-- 7/21/08 pinkleju issue 97503: Uncommented the below line so that uploading attachments will work. -->
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{/page/content/details/policy/@id}"/>
		<input type="hidden" name="_ClientID" id="_ClientID" value="{/page/content/details/client/@id}"/>
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value="{/page/content/details/policy/LOB}"/>
		<input type="hidden" name="_policyNumber" id="_policyNumber" value="{/page/content/details/policy/policyNumber}"/>
		<input type="hidden" name="_showBody" id="_showBody" value="1"/>
		<input type="hidden" name="_returnPage" id="_returnPage"/>
		<input type="hidden" name="_commentLimit" id="_commentLimit" value="10"/>
		<input type="hidden" name="_hiddenDisplayDtMask" id="_hiddenDisplayDtMask" value="{xslNsExt:formatMaskToUse('', '', '1')}"/>	
	</xsl:template>
	<xsl:template name="policyDetailWidget">
		<xsl:call-template name="buildPolicyDetailTable"/>
	</xsl:template>
	<xsl:template name="buildPolicyDetailAdditionalActions">
		<div id="policyDetailAdditionalActionsSection">
			<xsl:choose>
				<xsl:when test="/page/state/quoteID = /page/content/details/policy/@id">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">policyDetailLoadPolicyA</xsl:with-param>
						<xsl:with-param name="id">policyDetailLoadPolicyA</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
							<xsl:value-of select="/page/content/details/policy/@id"/>
							<xsl:text>');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('LoadPolicy')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildPolicyDetailTable">
		<h2>
			<xsl:value-of select="xslNsODExt:getDictRes('GeneralPolicyInformation')"/>
		</h2>
		<xsl:call-template name="policyDetailsWidget"/>
	</xsl:template>
	<xsl:template name="policyDetailsWidget">
		<div id="policyDetailSection">
			<table class="formTbl">
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('ClientName_colon')"/>
						</label>
					</td>
					<td>
						<a href="javascript:;" onclick="DCT.Submit.gotoPageForClient('clientInfo','{/page/content/details/client/@id}');">
							<xsl:value-of select="/page/content/details/client/name"/>
						</a>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PolicyNumber_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/policy/policyNumber"/>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Status_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/policy/status"/>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Contact_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/client/contactName"/>
					</td>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('EffExpDate_colon')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/details/policy/effectiveDate)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="xslNsODExt:getDictRes('ToRange')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/details/policy/expirationDate)"/>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Agency_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/policy/agency"/>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumber_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/client/phoneNumber"/>
						<span style="padding-right: 10px">
							<xsl:value-of select="/page/content/details/client/phoneExtension"/>
						</span>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('LOB_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/policy/LOB"/>
					</td>
					<td class="detailsLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Producer_colon')"/>
						</label>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/policy/producer"/>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Address_colon')"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/details/mailingAddress/address1"/>
						<br/>
						<xsl:if test="/page/content/details/mailingAddress/address2 and /page/content/details/mailingAddress/address2 != ''">
							<xsl:value-of select="/page/content/details/mailingAddress/address2"/>
							<br/>
						</xsl:if>
						<span style="padding-right: 5px;">
							<xsl:value-of select="/page/content/details/mailingAddress/city"/>
							<xsl:value-of select="xslNsODExt:getDictRes('Comma')"/>
						</span>
						<span style="padding-right: 10px;">
							<xsl:value-of select="/page/content/details/mailingAddress/state"/>
						</span>
						<xsl:value-of select="/page/content/details/mailingAddress/ZIP"/>
					</td>
					<td class="detailsLabel">
					</td>
					<td>
					</td>
					<td class="detailsLabel">
					</td>
					<td>
					</td>
				</tr>
			</table>
			<xsl:call-template name="buildPolicyDetailAdditionalActions"/>
		</div>
	</xsl:template>
	<xsl:template name="policyHistoryWidget">
		<div id="policyHistoryList" tabletype="acrossAboveOnce">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyHistory')"/>
			</h2>
			<table id="policyHistoryListTable" class="formTable">
				<tr>
					<th>
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Date')"/>
						</label>
					</th>
					<th>
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Action')"/>
						</label>
					</th>
					<th>
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PerformedBy')"/>
						</label>
					</th>
				</tr>
				<xsl:for-each select="/page/content/details/history/event">
					<tr>
						<td>
							<span>
								<xsl:value-of select="xslNsExt:convertISOtoDisplay(@date, 'G')"/>
							</span>
						</td>
						<td>
							<span>
								<xsl:value-of select="@what"/>
							</span>
						</td>
						<td>
							<span>
								<xsl:value-of select="@who"/>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<span>
								<xsl:value-of select="@comment"/>
							</span>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<!-- Comments -->
	<xsl:template name="commentsWidget">
		<xsl:variable name="hideInternal">
			<xsl:choose>
				<xsl:when test="/page/content/comments/@internalAccess = '1' ">
					<xsl:text>false</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>true</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:call-template name="buildPolicyCommentsListGrid">
			<xsl:with-param name="hideInternal" select="$hideInternal"/>
		</xsl:call-template>    
		<xsl:call-template name="buildCommentAction">
			<xsl:with-param name="hideInternal" select="$hideInternal"/>
		</xsl:call-template>    
	 </xsl:template>  
	<xsl:template name="buildCommentAction">
		<xsl:param name="hideInternal"/>
    <xsl:if test="not($IsReadOnly)">
      <div id="commentActionGroup">
        <xsl:call-template name="makeButton">
          <xsl:with-param name="name">commentAddAction</xsl:with-param>
          <xsl:with-param name="id">commentActionButton</xsl:with-param>
          <xsl:with-param name="onclick">
            <xsl:text>DCT.Util.displayPASAddCommentsPopUp('commentsPolicyList', Ext.getCmp('policyComments').getStore(), '</xsl:text>
            <xsl:value-of select="/page/content/details/policy/@id"/>
            <xsl:text>','POL', '', </xsl:text>
            <xsl:value-of select="$hideInternal"/>
            <xsl:text>);</xsl:text>
          </xsl:with-param>
          <xsl:with-param name="caption">
            <xsl:value-of select="xslNsODExt:getDictRes('AddANote')"/>
          </xsl:with-param>
          <xsl:with-param name="type">standard</xsl:with-param>
        </xsl:call-template>
      </div>
    </xsl:if >
	</xsl:template>
	<xsl:template name="buildPolicyCommentsListGrid">
		<xsl:param name="hideInternal"/>
		<xsl:variable name="cultureFormat">
			<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
		</xsl:variable>
		<xsl:call-template name="buildPagingGridPanel">
			<xsl:with-param name="gridID">
				<xsl:text>policyComments</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="dataOnLoad">
				<xsl:text>true</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="toolBarTitle">DCT.T('Notes_noColon')</xsl:with-param>
			<xsl:with-param name="targetPage">
				<xsl:text>commentsPolicyList</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="pageSize">
				<xsl:text>10</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordCount">
				<xsl:text>comments/@listCount</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordPath">
				<xsl:text>comment</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordID">
				<xsl:text>@commentId</xsl:text>
			</xsl:with-param>
      <xsl:with-param name="className">notesPagingGridPanel</xsl:with-param>
      <xsl:with-param name="classMethod">DCT.Util.refreshNotes</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'creationDateTime', type: 'date', dateFormat: 'Y-m-d H:i', mapping: '@creationDateTime'},</xsl:text>
				<xsl:text>{name: 'originator', type: 'string', mapping: '@originator'},</xsl:text>
				<xsl:text>{name: 'commentTypeCode', type: 'string', mapping: '@commentTypeCode'},</xsl:text>
				<xsl:text>{name: 'commentCode', type: 'string', mapping: '@commentCode'},</xsl:text>					
				<xsl:text>{name: 'commentLockingTS', type: 'string', mapping: '@commentLockingTS'},</xsl:text>
				<xsl:text>{name: 'historyId', type: 'string', mapping: '@historyId'},</xsl:text>				
				<xsl:text>{name: 'policyStatus', type: 'string', mapping: '@policyStatus'},</xsl:text>
				<xsl:text>{name: 'type', type: 'string', mapping: '@type'},</xsl:text>
				<xsl:text>{name: 'transactionStatus', type: 'string', mapping: '@transactionStatus'},</xsl:text>				
				<xsl:text>{name: 'description', type: 'string', mapping: '@commentText'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:text>[{text: DCT.T('Date'), dataIndex: 'creationDateTime', sortable: true, width: 30, renderer: Ext.util.Format.dateRenderer('</xsl:text>
				<xsl:value-of select="$cultureFormat"/>
				<xsl:text>, g:i a')},</xsl:text>
				<xsl:text>{text: DCT.T('Note'), dataIndex: 'description', sortable: true, width: 120, renderer: DCT.Grid.viewPASCommentDescriptionColumnRender},</xsl:text>
				<xsl:text>{text: DCT.T('TransactionStatus'), dataIndex: 'policyStatus', sortable: true, width: 50, renderer: DCT.Grid.objectStatusColumnRenderer},</xsl:text>	
				<xsl:text>{text: DCT.T('CreatedBy'), dataIndex: 'originator', sortable: true, width: 35},</xsl:text>
				<xsl:text>{text: "", dataIndex: 'commentTypeCode', width: 10, sortable: true, renderer: DCT.Grid.commentTypeColumnRenderer},</xsl:text>
				<xsl:text>{text: "", dataIndex: 'originator', sortable: false, width: 10, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.viewPASCommentColumnRenderer(</xsl:text>
				<xsl:text>dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); } },</xsl:text>
				<xsl:text>{text: "", dataIndex: 'originator', sortable: false, width: 10, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.deletePASCommentColumnRenderer('commentsPolicyList', </xsl:text>
				<xsl:choose>
					<xsl:when test="/page/content/comments/@deleteAccess = '1' ">
						<xsl:text>true</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>false</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); } }]</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>