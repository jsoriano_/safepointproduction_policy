﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer">
			<xsl:with-param name="noActionBar">1</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('ThisPortalSelectionPage')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsExt:FormatString1(xslNsODExt:getDictRes('WelcomeSelectPortal'), page/state/user/fullName)" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildQuickSearch"></xsl:template>
	<xsl:template name="buildProductNavigation"></xsl:template>
	<xsl:template name="buildActionsNavigation"></xsl:template>

	<xsl:template name="buildPageContent">
		<div id="portalInformation" class="downLayout">
			<ul id="logonActions">
				<xsl:for-each select="/page/content/portals/portal">
					<li>
						<a>
							<xsl:attribute name="name">home</xsl:attribute>
							<xsl:attribute name="id">home</xsl:attribute>
							<xsl:attribute name="href">javascript:;</xsl:attribute>
							<xsl:attribute name="onclick">
								<xsl:text>DCT.Submit.setPortal('</xsl:text>
								<xsl:value-of select="@name"/>
								<xsl:text>','</xsl:text>
								<xsl:value-of select="xslNsExt:replaceText(@xsltDir,'\','\\')"/>
								<xsl:text>');</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes(@caption)" />
						</a>
					</li>
				</xsl:for-each>
			</ul>
		</div>
		<div id="portalLogoutButtons">
			<ul class="action">
				<li>
					<a>
						<xsl:attribute name="name">logoutA</xsl:attribute>
						<xsl:attribute name="id">logoutA</xsl:attribute>
						<xsl:attribute name="href">javascript:;</xsl:attribute>
						<xsl:attribute name="onclick">DCT.Submit.submitPage('logout');</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes('Logout')" />
					</a>
				</li>
			</ul>
		</div>		
	</xsl:template>
</xsl:stylesheet>
