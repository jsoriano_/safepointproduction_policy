﻿
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>
	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('SearchResults')"/>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<div id="partyIsCandidatList">
			<xsl:call-template name="createPartyIsCandidateContent"/>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">nLinkSelected</xsl:with-param>
				<xsl:with-param name="id">linkSelected</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.submitPartyPage('partyLinkPeoplePlaces');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('LinkSelected')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="createPartyIsCandidateContent">
		<div name="searchCandidates" id="searchCandidates">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>searchResultsData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">DCT.T('Existing Parties')</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>partyIsCandidateList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>10</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>CandidateList/SearchControl/Paging/TotalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>ListItem/ItemData/PartyRecord</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>PartyRecordPartyId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">true</xsl:with-param>
				<xsl:with-param name="multipleSelect">true</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'FullName', type: 'string', mapping: '@fullName'},</xsl:text>
					<xsl:text>{name: 'CityStateZip', type: 'string', mapping: '@cityStateZip'},</xsl:text>
					<xsl:text>{name: 'DOB', type: 'string', mapping: 'Party/PartyBirthDate'},</xsl:text>
					<xsl:text>{name: 'EntityName', type: 'string', mapping: 'AuthorizedInterest/Name'},</xsl:text>
					<xsl:text>{name: 'PartyType', type: 'string', mapping: 'Party/PartyTypeCode'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T('Name'), dataIndex: 'FullName', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('Address'), dataIndex: 'CityStateZip', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('DOB'), dataIndex: 'DOB', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('EntityName'), dataIndex: 'EntityName', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T('PartyType'), dataIndex: 'PartyType', sortable: true}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:text>DCT.Util.partyIsCandidateDeselected</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:text>DCT.Util.partyIsCandidateSelected</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildHiddenFormFields">
		<input type="hidden" name="_selectedPartyId" id="_selectedPartyId" value=""/>
		<input type="hidden" name="_searchType" id="_searchTypee" value=""/>
		<input type="hidden" name="_soundexIndicator" id="_soundexIndicator" value=""/>
		<input type="hidden" name="_address1" id="_address1" value=""/>
		<input type="hidden" name="_city" id="_city" value=""/>
		<input type="hidden" name="_stateCode" id="_stateCode" value=""/>
		<input type="hidden" name="_postalCode" id="_postalCode" value=""/>
		<input type="hidden" name="_firstName" id="_firstName" value=""/>
		<input type="hidden" name="_middleName" id="_middleName" value=""/>
		<input type="hidden" name="_partyTypeCode" id="_partyTypeCode" value=""/>
		<input type="hidden" name="_name" id="_name" value=""/>
		<input type="hidden" name="_dob" id="_dob" value=""/>
		<input type="hidden" name="_licenseNumber" id="_licenseNumber" value=""/>
		<input type="hidden" name="_phoneNumber" id="_phoneNumber" value=""/>
		<input type="hidden" name="_referenceNumber" id="_referenceNumber" value=""/>
		<input type="hidden" name="_nameSearchType" id="_nameSearchType" value=""/>
		<input type="hidden" name="_listStart" id="_listStart" value=""/>
		<input type="hidden" name="_listLength" id="_listLength" value=""/>
	</xsl:template>
</xsl:stylesheet>
