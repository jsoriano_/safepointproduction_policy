﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>
	<xsl:import href="..\common\dctCommonPolicy.xsl"/>


	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<xsl:call-template name="buildContainer"/>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('DownloadPolicyPageTitle')" />
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
				<xsl:value-of select="xslNsODExt:getDictRes('DownloadPolicyPageInstructions')" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildDownloadPolicyDialog"/>
	</xsl:template>

	<xsl:template name="buildLeftPanelContent">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:call-template name="buildPolicyLeftNavHeader"/>
		<xsl:call-template name="createSubMenuArea"/>
		<xsl:call-template name="buildPolicyRelatedTasks">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="buildDownloadPolicyDialog">
		<div id="downloadPolicyDialog">
			<div id="downloadPolicy" tabletype="acrossAboveOnce" class="acrossLayout">
				<div id="clientName" class="acrossFieldGroup">
					<div id="clientNameLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('ClientName')" />
						</label>
					</div>
					<div id="clientNameField" class="acrossFormField">
						<span>
							<xsl:value-of select="client/name"/>
						</span>
					</div>
				</div>
				<div id="status" class="acrossFieldGroup">
					<div id="statusLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('Status')" />
						</label>
					</div>
					<div id="statusField" class="acrossFormField">
						<span>
							<xsl:value-of select="policy/status"/>
						</span>
					</div>
				</div>
				<div id="lob" class="acrossFieldGroup">
					<div id="lobLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('LOB')" />
						</label>
					</div>
					<div id="lobField" class="acrossFormField">
						<span>
							<xsl:value-of select="policy/LOB"/>
						</span>
					</div>
				</div>
				<div id="policyNumber" class="acrossFieldGroup">
					<div id="policyNumberLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('PolicyNumber')" />
						</label>
					</div>
					<div id="policyNumberField" class="acrossFormField">
						<span>
							<xsl:value-of select="policy/policyNumber"/>
						</span>
					</div>
				</div>
				<div id="effectiveDate" class="acrossFieldGroup">
					<div id="effectiveDateLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('EffectiveDate')" />
						</label>
					</div>
					<div id="effectiveDateField" class="acrossFormField">
						<span>
							<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', policy/effectiveDate)"/>
						</span>
					</div>
				</div>
				<div id="expirationDate" class="acrossFieldGroup">
					<div id="expirationDateLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('ExpirationDate')" />
						</label>
					</div>
					<div id="expirationDateField" class="acrossFormField">
						<span>
							<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', policy/expirationDate)"/>
						</span>
					</div>
				</div>
				<div id="expirationDate" class="acrossFieldGroup">
					<div id="expirationDateLabel" class="acrossFormLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('CurrentPremium')" />
						</label>
					</div>
					<div id="expirationDateField" class="acrossFormField">
						<span>
							<xsl:value-of select="policy/currentPremium"/>
						</span>
					</div>
				</div>
			</div>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">downloadPolicyA</xsl:with-param>
				<xsl:with-param name="id">downloadPolicyA</xsl:with-param>
				<xsl:with-param name="onclick">document.forms[0].submit();</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('PressToDownload')" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">cancelDownloadPolicyA</xsl:with-param>
				<xsl:with-param name="id">cancelDownloadPolicyA</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Submit.submitPage('open');</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('Cancel')" />
				</xsl:with-param>
				<xsl:with-param name="type">cancel</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
</xsl:stylesheet>
