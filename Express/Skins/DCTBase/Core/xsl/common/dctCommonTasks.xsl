﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="..\common\contentPage.xsl"/>

	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template name="buildPageContent">
		<xsl:call-template name="buildHiddenFormFields"/>
		<xsl:call-template name="buildMainAreaContent"/>
	</xsl:template>

	<xsl:template name="buildTaskSubHeader">
		<xsl:param name="resourceKey"/>
		<h2 class="contentSubHeader">
			<xsl:value-of select="xslNsODExt:getDictRes($resourceKey)"/>
		</h2>
	</xsl:template>

	<xsl:template name="buildQueueToolbar">
		<xsl:param name="defaultCategory"/>
		<xsl:param name="queueType"/>
		<xsl:param name="referenceNumber"/>
		<xsl:param name="categoryWidth"/>
		<xsl:param name="queueEntityId"/>
		<xsl:param name="queueGridId"/>
		<div>
			<xsl:attribute name="id">
				<xsl:value-of select="$queueType"/>
				<xsl:text>QueueToolbar</xsl:text>
			</xsl:attribute>
			<div class="basicOptionGroup">
				<xsl:if test="$queueType = 'unassigned'">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unassignedQueueEntityId</xsl:with-param>
						<xsl:with-param name="name">_unassignedQueueEntityId</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option value="">
								<xsl:value-of select="xslNsODExt:getDictRes('ViewAllQueues')"/>
							</option>
							<xsl:for-each select="/page/content/QueueList/Queue">
								<option value="{EntityId}">
									<xsl:value-of select="Name"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="controlClass">taskunassignedqueueComboField</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text></xsl:text>
						</xsl:with-param>
						<xsl:with-param name="width">115</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>FilterId</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:value-of select="$queueType"/>
						<xsl:text>Filter</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="type">select</xsl:with-param>
					<xsl:with-param name="optionlist">
						<option value="">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAllTasks')"/>
						</option>
						<option value="dueToday">
							<xsl:value-of select="xslNsODExt:getDictRes('DueToday')"/>
						</option>
						<option value="dueThisWeek">
							<xsl:value-of select="xslNsODExt:getDictRes('DueThisWeek')"/>
						</option>
						<option value="overdue">
							<xsl:value-of select="xslNsODExt:getDictRes('Overdue')"/>
						</option>
					</xsl:with-param>
					<xsl:with-param name="controlClass">
						<xsl:text>task</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>ComboField</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:text></xsl:text>
					</xsl:with-param>
					<xsl:with-param name="width">110</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>MoreOptionsToggle</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="class"></xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Util.toggleSection('</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>MoreOptionsPanelCmp'); return false;</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('MoreOptions')"/>
					</xsl:with-param>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
				</xsl:call-template>
			</div>
			<div>
				<xsl:attribute name="id">
					<xsl:value-of select="$queueType"/>
					<xsl:text>MoreOptionsPanel</xsl:text>
				</xsl:attribute>
				<xsl:element name="div">
					<xsl:attribute name="class">collapsiblePanel</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>renderTo: "</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>MoreOptionsPanel",</xsl:text>
						<xsl:text>id: "</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>MoreOptionsPanelCmp",</xsl:text>
						<xsl:text>contentEl: "</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>MoreOptions",</xsl:text>
						<xsl:text>collapsed: true</xsl:text>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</div>
			<div class="acrossLayout x-hidden">
				<xsl:attribute name="id">
					<xsl:value-of select="$queueType"/>
					<xsl:text>MoreOptions</xsl:text>
				</xsl:attribute>
				<div class="acrossFieldGroup">
					<xsl:attribute name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>CategoryFilterGroup</xsl:text>
					</xsl:attribute>
					<div class="acrossAboveFormLabel">
						<label>
							<xsl:attribute name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>CategoryFilterCaption</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="for">
								<xsl:value-of select="$queueType"/>
								<xsl:text>CategoryFilterId</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes('Category_colon')"/>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>CategoryFilterId</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:value-of select="$queueType"/>
								<xsl:text>CategoryFilter</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option value="">
									<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
								</option>
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_TASKTYPE']/ListEntry">
									<xsl:sort select="@SortSequence"/>
									<option value="{@Code}">
										<xsl:value-of select="@Description"/>
									</option>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="watermark">
								<xsl:value-of select="xslNsODExt:getDictRes('Select_parentheses')"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">
								<xsl:text>task</xsl:text>
								<xsl:if test="$queueType = 'unassigned'">
									<xsl:value-of select="$queueType"/>
								</xsl:if>
								<xsl:text>filterComboField</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$defaultCategory"/>
							</xsl:with-param>
							<xsl:with-param name="width">
								<xsl:value-of select="$categoryWidth"/>
							</xsl:with-param>
							<xsl:with-param name="defaultValue">
								<xsl:value-of select="$defaultCategory"/>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="acrossFieldGroup">
					<xsl:attribute name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>StatusFilterGroup</xsl:text>
					</xsl:attribute>
					<div class="acrossAboveFormLabel">
						<label>
							<xsl:attribute name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>StatusFilterCaption</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="for">
								<xsl:value-of select="$queueType"/>
								<xsl:text>StatusFilterId</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes('State_colon_status')"/>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>StatusFilterId</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:value-of select="$queueType"/>
								<xsl:text>StatusFilter</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option value="">
									<xsl:value-of select="xslNsODExt:getDictRes('All')"/>
								</option>
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_TASKSTATUS']/ListEntry">
									<xsl:sort select="@SortSequence"/>
									<option value="{@Code}">
										<xsl:value-of select="@Description"/>
									</option>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="controlClass">
								<xsl:text>task</xsl:text>
								<xsl:if test="$queueType = 'unassigned'">
									<xsl:value-of select="$queueType"/>
								</xsl:if>
								<xsl:text>filterComboField</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>OPN</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="width">100</xsl:with-param>
							<xsl:with-param name="defaultValue">
								<xsl:text>OPN</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="acrossFieldGroup">
					<xsl:attribute name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>SearchFilterGroup</xsl:text>
					</xsl:attribute>
					<div class="acrossAboveFormLabel">
						<label>
							<xsl:attribute name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterCaption</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="for">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterId</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes('Search_colon')"/>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterId</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilter</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option value="policy">
									<xsl:value-of select="xslNsODExt:getDictRes('PolicyQuoteNumSymbol')"/>
								</option>
								<option value="titledesc">
									<xsl:value-of select="xslNsODExt:getDictRes('TitleDesc')"/>
								</option>
							</xsl:with-param>
							<xsl:with-param name="controlClass">
								<xsl:text>task</xsl:text>
								<xsl:if test="$queueType = 'unassigned'">
									<xsl:value-of select="$queueType"/>
								</xsl:if>
								<xsl:text>filterComboField</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>policy</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="width">120</xsl:with-param>
							<xsl:with-param name="defaultValue">
								<xsl:text>policy</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div class="acrossFieldGroup">
					<xsl:attribute name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>SearchFilterForGroup</xsl:text>
					</xsl:attribute>
					<div class="acrossAboveFormLabel">
						<label>
							<xsl:attribute name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterForCaption</xsl:text>
							</xsl:attribute>
							<xsl:attribute name="for">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterForId</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes('For_colon')"/>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterForId</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:value-of select="$queueType"/>
								<xsl:text>SearchFilterFor</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="controlClass">taskSearchTextField</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctTaskFilter</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$queueType"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">
						<xsl:value-of select="$queueType"/>
						<xsl:text>SearchLink</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>SearchLink</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Util.reloadTaskQueue(</xsl:text>
						<xsl:value-of select="$queueEntityId"/>
						<xsl:text>, null, Ext.getCmp('</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>FilterId</xsl:text>
						<xsl:text>').getValue(), '</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>', </xsl:text>
						<xsl:value-of select="$queueGridId"/>
						<xsl:text>, false, '</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>SearchLink');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">
						<xsl:value-of select="$queueType"/>
						<xsl:text>ClearButton</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="id">
						<xsl:value-of select="$queueType"/>
						<xsl:text>ClearButton</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="class">btnDisabled</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Util.reloadTaskQueue(</xsl:text>
						<xsl:value-of select="$queueEntityId"/>
						<xsl:text>, null, Ext.getCmp('</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>FilterId</xsl:text>
						<xsl:text>').getValue(), '</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>', </xsl:text>
						<xsl:value-of select="$queueGridId"/>
						<xsl:text>, true, '</xsl:text>
						<xsl:value-of select="$queueType"/>
						<xsl:text>ClearButton');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Clear')"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="buildTaskQueueWidget">
		<xsl:param name="returnPage"/>
		<xsl:param name="onClickMethod"/>
		<xsl:param name="queueType"/>
		<xsl:param name="resourceKey"/>
		<xsl:param name="targetPage"/>
		<xsl:param name="className"/>
		<xsl:param name="classMethod"/>
		<xsl:call-template name="taskQueueWidget">
			<xsl:with-param name="id">
				<xsl:text>Queue</xsl:text>
				<xsl:value-of select="$queueType"/>
			</xsl:with-param>
			<xsl:with-param name="renderTo">
				<xsl:text>Queue</xsl:text>
				<xsl:value-of select="$queueType"/>
				<xsl:text>Container</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="targetPage">
				<xsl:value-of select="$targetPage"/>
			</xsl:with-param>
			<xsl:with-param name="listTitle">
				<xsl:text>'</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes($resourceKey)"/>
				<xsl:text>'</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="listMode">
				<xsl:text>Queue</xsl:text>
				<xsl:value-of select="$queueType"/>
			</xsl:with-param>
			<xsl:with-param name="className">
				<xsl:value-of select="$className"/>
			</xsl:with-param>
			<xsl:with-param name="classMethod">
				<xsl:value-of select="$classMethod"/>
			</xsl:with-param>
			<xsl:with-param name="actionColumnRenderer">
				<xsl:text>function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.generalTaskActionColumnRenderer('taskInfo', false, '</xsl:text>
				<xsl:value-of select="/page/state/user/userID"/>
				<xsl:text>', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); }</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="titleColumnRenderer">
				<xsl:text>function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.taskTitleViewRenderer('</xsl:text>
				<xsl:value-of select="$returnPage"/>
				<xsl:text>', dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore</xsl:text>
				<xsl:if test="normalize-space($onClickMethod) != ''">
					<xsl:text>, </xsl:text>
					<xsl:value-of select="$onClickMethod"/>
				</xsl:if>
				<xsl:text>); }</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="showCheckbox">false</xsl:with-param>
			<xsl:with-param name="multipleSelect">false</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
</xsl:stylesheet>