﻿
<!--
Localization Dictionary

Description:    This style sheet gives the skin support for localization.  (Note: Strings are looked up from 
                .NET Resx files, based on culture, through an XSL extension.
                Example: <xsl:value-of select="xslNsExt:getDictionaryResource($DictionaryRootPath, 'DefaultDictionary', $DictionaryKey)"/>
-->
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<!-- Holds a string with the current culture code for 
  system pages, and other non-ManuScript use.  You don't need this
  to look up strings from a localized dictionary, but it may be 
  helpful for other decisions made in the XSLT. -->
	<xsl:variable name="GlobalCultureCode">
		<xsl:value-of select="xslNsODExt:getCulture()"/>
	</xsl:variable>
	<xsl:variable name="ExtJsLocaleFile">
		<xsl:choose>
			<xsl:when test="/page/debugMode!='0'">
				<xsl:choose>
					<xsl:when test="$GlobalCultureCode='it-IT' or $GlobalCultureCode='it'">
						<xsl:text>locale-it-debug.js</xsl:text>
					</xsl:when>
					<xsl:when test="$GlobalCultureCode='de-DE' or $GlobalCultureCode='de'">
						<xsl:text>locale-de-debug.js</xsl:text>
					</xsl:when>
					<xsl:when test="$GlobalCultureCode='en-US'">
						<xsl:text>locale-en-debug.js</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>locale-en-debug.js</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$GlobalCultureCode='it-IT' or $GlobalCultureCode='it'">
						<xsl:text>locale-it.js</xsl:text>
					</xsl:when>
					<xsl:when test="$GlobalCultureCode='de-DE' or $GlobalCultureCode='de'">
						<xsl:text>locale-de.js</xsl:text>
					</xsl:when>
					<xsl:when test="$GlobalCultureCode='en-US'">
						<xsl:text>locale-en.js</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>locale-en.js</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
</xsl:stylesheet>