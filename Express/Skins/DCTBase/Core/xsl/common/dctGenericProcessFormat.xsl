﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
    <xsl:include href="dctGenericSetFieldFormat.xsl"/>
  <xsl:template match="/">
        <xsl:for-each select="fieldInstance">
            <xsl:call-template name="FormatValue">
                <xsl:with-param name="value">
                    <xsl:choose>
                        <xsl:when test="(@removeZero=1 or @removezero=1) and @value=0">
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@value"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>