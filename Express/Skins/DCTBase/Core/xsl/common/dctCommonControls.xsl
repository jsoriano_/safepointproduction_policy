﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctTemplatesSystem.xsl"/>

	<!--*************************************************************************************************************
		Common control templates
		************************************************************************************************************* -->
	<!-- Blank template to be overriden in content pages. -->
	<xsl:template name="buildEditDetailButtons"/>
	<xsl:template name="buildPageHeaderSection">
	</xsl:template>
	<xsl:template name="messagesListWidget">
		<xsl:param name="listTitle"/>
		<xsl:param name="newButtonText"/>
		<xsl:call-template name="buildMessagesListHiddenFormFields"/>
		<xsl:element name="h2">
			<xsl:value-of select="$listTitle"/>
		</xsl:element>
		<div id="messageListFilterSection">
			<xsl:if test="not(/page/content/@page='policy') and not(/page/content/@page='clientInfo') and /page/state/quoteID!=''">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">messageDetailBackToPolicyA</xsl:with-param>
					<xsl:with-param name="id">messageDetailBackToPolicyA</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.gotoPageForQuote('policy','</xsl:text>
						<xsl:value-of select="/page/state/quoteID"/>
						<xsl:text>');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('BackToPolicyInfo')"/>
					</xsl:with-param>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<xsl:variable name="cultureFormat">
				<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
			</xsl:variable>
			<!-- Build the paging grid. -->
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="toolBarTitle">
					<xsl:text>'</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('Notifications')"/>
					<xsl:text>'</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="gridID">
					<xsl:text>messagesList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>messagesList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="filterSubSet">
					<xsl:text>'Notifications'</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">
					<xsl:text>10</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>messages/@listCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>message</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>@messageID</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'sentOn', type: 'date', dateFormat: 'Y-m-d H:i:s'},</xsl:text>
					<xsl:text>{name: 'userID', type: 'string'},</xsl:text>
					<xsl:text>{name: 'sender', type: 'string'},</xsl:text>
					<xsl:text>{name: 'action', type: 'string'},</xsl:text>
					<xsl:text>{name: 'subject', type: 'string'},</xsl:text>
					<xsl:text>{name: 'read', type: 'string', mapping: '@read'},</xsl:text>
					<xsl:text>{name: 'policyID', type: 'string'},</xsl:text>
					<xsl:text>{name: 'policyNumber', type: 'string'},</xsl:text>
					<xsl:text>{name: 'remindOn', type: 'date', dateFormat: 'Y-m-d'},</xsl:text>
					<xsl:text>{name: 'closedDate', type: 'date', dateFormat: 'Y-m-d'},</xsl:text>
					<xsl:text>{name: 'body', type: 'string'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.Localization.translate("Type"), dataIndex: 'action', sortable: true, width: 40, renderer: DCT.Grid.messageTypeColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.Localization.translate("PolicyNumber"), dataIndex: 'policyNumber', sortable: false, width: 40, renderer: DCT.Grid.messagePolicyColumnRenderer},</xsl:text>
					<xsl:text>{text: DCT.Localization.translate("Received"), dataIndex: 'sentOn', sortable: true, width: 50, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="$cultureFormat"/>
					<xsl:text>, g:i a')},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'userID', sortable: false, width: 10, renderer: DCT.Grid.messageEditColumnRenderer},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'userID', sortable: false, width: 10, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView) { return DCT.Grid.messageDeleteColumnRenderer('messagesList', </xsl:text>
					<xsl:value-of select="$canDeleteNotifications"/>
					<xsl:text>, dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, extView); } }]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.messageBodyPreview</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="not($IsReadOnly)">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">newMessageA</xsl:with-param>
					<xsl:with-param name="id">newMessageA</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Submit.actOnMessage('', 'newNotification');</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="$newButtonText"/>
					</xsl:with-param>
					<xsl:with-param name="type">standard</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="buildMessagesListHiddenFormFields">
		<input type="hidden" name="_messageID" id="_messageID"/>
		<input type="hidden" name="_startIndex" id="_startIndex" value="{/page/content/messages/@startIndex}"/>
		<input type="hidden" name="_displayCount" id="_displayCount" value="{/page/content/@displayCount}"/>
		<input type="hidden" name="_orderBy" id="_orderBy" value="{/page/content/keys/orderKey/@name}"/>
		<input type="hidden" name="_orderDirection" id="_orderDirection" value="{/page/content/keys/orderKey/@direction}"/>
		<input type="hidden" name="_attachmentID" id="_attachmentID"/>
		<input type="hidden" name="_dateformat" id="_dateformat" value="{xslNsExt:formatMaskToUse('', '', '0')}"/>
	</xsl:template>
	<xsl:template name="buildHiddenDetailFields"/>
	<xsl:template name="buildCommonLayout">
		<xsl:param name="screenName"/>

		<xsl:if test="$screenName!=''">
			<xsl:call-template name="buildHiddenDetailFields"/>
		</xsl:if>

		<xsl:if test="not($isPartyPage)">
			<xsl:if test="/page/content/getPage/body[group//fieldInstance[@required='1' and @readOnly='0']]">

				<div id="requiredTxt">
					<span class="required">
						<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
					</span>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('IndicatesRequiredField')"/>
				</div>
			</xsl:if>
		</xsl:if>
		<xsl:if test="/page/content/getPage/body/annotations">
			<div id="annotations">
				<xsl:apply-templates select="/page/content/getPage/body/annotations" mode="now"/>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="/page/content//errors">
				<!-- Need to check into -->
				<xsl:call-template name="errors"/>
			</xsl:when>
			<xsl:otherwise>
				<div id="body" class="{/page/content/*/body/@class}">
					<xsl:apply-templates select="/page/content/*/body"/>
				</div>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Process any actions at the dialog level -->
		<xsl:if test="(/page/content/getPage/actions[@type='page']/action) and not($screenName='taskDetail') and not($screenName='editPrint')">
			<div id="navControls" class="g-pageAction">
				<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@command='previous']">
					<xsl:choose>
						<xsl:when test="/page/content/getPage/body/@prevButton">
							<xsl:if test="/page/content/getPage/body/@prevButton != ''">
								<div id="previous-div" class="previous">
									<xsl:call-template name="buildLink">
										<xsl:with-param name="name">previous-button</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="/page/content/getPage/body/@prevButton"/>
										</xsl:with-param>
										<xsl:with-param name="class">g-previous</xsl:with-param>
										<xsl:with-param name="id">g-previous</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<div id="previous-div" class="previous">
								<xsl:call-template name="buildLink">
									<xsl:with-param name="name">previous-button</xsl:with-param>
									<xsl:with-param name="class">g-previous</xsl:with-param>
									<xsl:with-param name="id">g-previous</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@command='next']">
					<xsl:choose>
						<xsl:when test="/page/content/getPage/body/@nextButton">
							<xsl:if test="/page/content/getPage/body/@nextButton != ''">
								<div id="next-div" class="next">
									<xsl:call-template name="buildLink">
										<xsl:with-param name="name">next-button</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="/page/content/getPage/body/@nextButton"/>
										</xsl:with-param>
										<xsl:with-param name="class">g-next</xsl:with-param>
										<xsl:with-param name="id">g-next</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<div id="next-div" class="next">
								<xsl:call-template name="buildLink">
									<xsl:with-param name="name">next-button</xsl:with-param>
									<xsl:with-param name="class">g-next</xsl:with-param>
									<xsl:with-param name="id">g-next</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$screenName='adminDetail'">
				<xsl:call-template name="buildEditDetailButtons"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not((/page/content/getPage/body and /page/content/getPage/body/@popUp!='0') or (/page/popUp and /page/popUp='1'))">
					<xsl:choose>
						<xsl:when test="$screenName!=''">
							<xsl:call-template name="buildEditDetailButtons"/>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildQuickSearch">
		<xsl:choose>
			<xsl:when test="/page/content/portals/portal[@active = '1']/@portalType = 'pas'">
				<div id="quickSearch">
					<div id="quickSearchLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('QuickSearch')"/>
						<xsl:text> </xsl:text>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">quickSearchModeId</xsl:with-param>
						<xsl:with-param name="name">quickSearchMode</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="class">autoFocusOff</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/search/@quickSearchMode"/>
						</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option value="policy">
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyQuoteNumber')"/>
							</option>
							<option value="clientname">
								<xsl:value-of select="xslNsODExt:getDictRes('ClientName')"/>
							</option>
							<option value="phone">
								<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumber')"/>
							</option>
						</xsl:with-param>
					</xsl:call-template>
					<div id="searchIsLabel">
						<xsl:text> </xsl:text>
						<xsl:value-of select="xslNsODExt:getDictRes('Is')"/>
						<xsl:text> </xsl:text>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">quickSearchTextId</xsl:with-param>
						<xsl:with-param name="name">quickSearchText</xsl:with-param>
						<xsl:with-param name="controlClass">policyQuickSearchTextField</xsl:with-param>
						<xsl:with-param name="class">autoFocusOff</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="/page/content/search/keys/key/@value"/>
						</xsl:with-param>
					</xsl:call-template>
					<a href="javascript:;" onclick="DCT.Submit.setActiveParent('search');DCT.Submit.runQuickSearch();" name="name_quickSearch" id="id_quickSearch">
						<img src="{$imageDir}icons/magnifier.png" alt="{xslNsODExt:getDictRes('RunQuickSearch')}"/>
					</a>
				</div>
			</xsl:when>
			<xsl:when test="/page/content/portals/portal[@active = '1']/@name = 'Billing' and /page/content/@page!= 'BillHome'">
				<div id="quickSearch">
					<div id="searchLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('SearchBy_colon')"/>
						<xsl:text> </xsl:text>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">quickSearchModeId</xsl:with-param>
						<xsl:with-param name="name">quickSearchMode</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option value="account">
								<xsl:value-of select="xslNsODExt:getDictRes('AccountNumber')"/>
							</option>
							<option value="policy">
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyOrArrangementNumber')"/>
							</option>
							<option value="party">
								<xsl:value-of select="xslNsODExt:getDictRes('PartyInformation')"/>
							</option>
							<option value="invoice">
								<xsl:value-of select="xslNsODExt:getDictRes('InvoiceReference')"/>
							</option>
						</xsl:with-param>
						<xsl:with-param name="controlClass">quicksearchComboField</xsl:with-param>
						<xsl:with-param name="width">160</xsl:with-param>
					</xsl:call-template>
					<div id="searchUsingLabel">
						<xsl:text> </xsl:text>
						<xsl:value-of select="xslNsODExt:getDictRes('Using')"/>
						<xsl:text> </xsl:text>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">quickSearchTextId</xsl:with-param>
						<xsl:with-param name="name">quickSearchText</xsl:with-param>
						<xsl:with-param name="controlClass">billingQuickSearchTextField</xsl:with-param>
					</xsl:call-template>
					<a href="javascript:;" onclick="DCT.Submit.billingQuickSearch(Ext.getCmp('quickSearchTextId').getValue(), Ext.getCmp('quickSearchModeId').getValue(), 'accountSearch');" name="name_quickSearch" id="id_quickSearch">
						<img src="{$imageDir}icons/magnifier.png" alt="{xslNsODExt:getDictRes('RunQuickSearch')}"/>
					</a>
				</div>
			</xsl:when>
			<xsl:when test="$showUnderwriting='1'">
				<xsl:if test="/page/content/portals/portal[@active = '1']/@portalType = 'uw'">
					<div id="quickSearch">
						<div id="quickSearchLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('QuickSearch')"/>
							<xsl:text> </xsl:text>
						</div>
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">quickSearchModeId</xsl:with-param>
							<xsl:with-param name="name">quickSearchMode</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="class">autoFocusOff</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="/page/content/search/@quickSearchMode"/>
							</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option value="accountname">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountName')"/>
								</option>
								<option value="accountnumber">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountNumber')"/>
								</option>
								<option value="accounttype">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountType')"/>
								</option>
								<option value="accountowner">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountOwner')"/>
								</option>
								<option value="accountcontact">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountContact')"/>
								</option>
								<option value="accountstatus">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountStatus')"/>
								</option>
								<option value="accountstanding">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountStanding')"/>
								</option>
								<option value="accountperformer">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountPerformer')"/>
								</option>
								<option value="accountparty">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountParty')"/>
								</option>
								<option value="accountquoteid">
									<xsl:value-of select="xslNsODExt:getDictRes('AccountQuoteId')"/>
								</option>
							</xsl:with-param>
						</xsl:call-template>
						<div id="searchIsLabel">
							<xsl:text> </xsl:text>
							<xsl:value-of select="xslNsODExt:getDictRes('Is')"/>
							<xsl:text> </xsl:text>
						</div>
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">quickSearchTextId</xsl:with-param>
							<xsl:with-param name="name">quickSearchText</xsl:with-param>
							<xsl:with-param name="controlClass">policyQuickSearchTextField</xsl:with-param>
							<xsl:with-param name="class">autoFocusOff</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="/page/content/search/keys/key/@value"/>
							</xsl:with-param>
						</xsl:call-template>
						<a href="javascript:;" onclick="DCT.Submit.setActiveParent('search');DCT.Submit.runQuickSearch();" name="name_quickSearch" id="id_quickSearch">
							<img src="{$imageDir}icons/magnifier.png" alt="{xslNsODExt:getDictRes('RunQuickSearch')}"/>
						</a>
					</div>
				</xsl:if>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildProductNavigation">
		<xsl:choose>
			<xsl:when test="$showUnderwriting='1'">
				<xsl:if test="/page/content/portals/portal[@active = '1']/@portalType = 'pas' or /page/content/portals/portal[@active = '1']/@name = 'Billing' or /page/content/portals/portal[@active = '1']/@name = 'Party' or /page/content/portals/portal[@active = '1']/@portalType = 'uw'">
					<div id="portalNavigationDiv">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">productNavSelect</xsl:with-param>
							<xsl:with-param name="name">productNavSelect</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="/page/content/portals/portal[@active = 1]/@name"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">productComboField</xsl:with-param>
							<xsl:with-param name="width">180</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<!-- using this to replace optionList parameter because we need the additional xsltDir datastore field for call to setPortal -->
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctPortalStore</xsl:with-param>
									<xsl:with-param name="type">object</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>[</xsl:text>
										<xsl:for-each select="/page/content/portals/portal[@portalType = 'pas'][1] | /page/content/portals/portal[@name = 'Billing'][1] | /page/content/portals/portal[@name = 'Party'][1] | /page/content/portals/portal[@portalType = 'uw'][1]">
											<xsl:text>["</xsl:text>
											<xsl:value-of select="@name"/>
											<xsl:text>", "</xsl:text>
											<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
											<xsl:text>", "</xsl:text>
											<xsl:value-of select="@xsltDir"/>
											<xsl:text>"]</xsl:text>
											<xsl:if test="position() != last()">
												<xsl:text>,</xsl:text>
											</xsl:if>
										</xsl:for-each>
										<xsl:text>]</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</div>
					<div id="productNavigationDiv">
						<xsl:for-each select="/page/actions/action[@location='product']">
							<xsl:variable name="href">
								<xsl:choose>
									<xsl:when test="@command != 'submitAction'">
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>'</xsl:text>
										<xsl:if test="@post">
											<xsl:text>,'</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>'</xsl:text>
										</xsl:if>
										<xsl:text>);</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@post"/>
										<xsl:text>');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<a href="javascript:;">
								<xsl:attribute name="name">
									<xsl:text>name_</xsl:text>
									<xsl:value-of select="@id"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:text>id_</xsl:text>
									<xsl:value-of select="@id"/>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:value-of select="$href"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
							</a>
						</xsl:for-each>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="/page/content/portals/portal[@active = '1']/@portalType = 'pas' or /page/content/portals/portal[@active = '1']/@name = 'Billing' or /page/content/portals/portal[@active = '1']/@name = 'Party'">
					<div id="productNavigationDiv">
						<xsl:for-each select="/page/content/portals/portal[@portalType = 'pas'][1]">
							<a>
								<xsl:attribute name="name">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="class">
									<xsl:text>pasLink </xsl:text>
									<xsl:if test="@active = '1'">
										<xsl:text>active</xsl:text>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="href">javascript:;</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Submit.setPortal('</xsl:text>
									<xsl:value-of select="@name"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(@xsltDir,'\','\\')"/>
									<xsl:text>');</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
								<xsl:text></xsl:text>
							</a>
						</xsl:for-each>
						<xsl:for-each select="/page/content/portals/portal[@name = 'Billing'][1]">
							<a>
								<xsl:attribute name="name">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="class">
									<xsl:text>billingLink </xsl:text>
									<xsl:if test="@active = '1'">
										<xsl:text>active</xsl:text>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="href">javascript:;</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Submit.setPortal('</xsl:text>
									<xsl:value-of select="@name"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(@xsltDir,'\','\\')"/>
									<xsl:text>');</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
							</a>
						</xsl:for-each>
						<xsl:for-each select="/page/content/portals/portal[@name = 'Party'][1]">
							<a>
								<xsl:attribute name="name">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="@name"/>
								</xsl:attribute>
								<xsl:attribute name="class">
									<xsl:text>partyLink </xsl:text>
									<xsl:if test="@active = '1'">
										<xsl:text>active</xsl:text>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="href">javascript:;</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Submit.setPortal('</xsl:text>
									<xsl:value-of select="@name"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(@xsltDir,'\','\\')"/>
									<xsl:text>');</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
							</a>
						</xsl:for-each>
						<xsl:for-each select="/page/actions/action[@location='product']">
							<xsl:variable name="href">
								<xsl:choose>
									<xsl:when test="@command != 'submitAction'">
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>'</xsl:text>
										<xsl:if test="@post">
											<xsl:text>,'</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>'</xsl:text>
										</xsl:if>
										<xsl:text>);</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@post"/>
										<xsl:text>');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<a href="javascript:;">
								<xsl:attribute name="name">
									<xsl:text>name_</xsl:text>
									<xsl:value-of select="@id"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:text>id_</xsl:text>
									<xsl:value-of select="@id"/>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:value-of select="$href"/>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
							</a>
						</xsl:for-each>
					</div>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildActionsNavigation">
		<xsl:param name="location"/>
		<xsl:if test="/page/content/portals/portal[@active = '1']/@portalType = 'pas' or /page/content/portals/portal[@active = '1']/@name = 'Billing'">
			<xsl:for-each select="/page/actions/action[@location=$location]">
				<li>
					<xsl:variable name="href">
						<xsl:choose>
							<xsl:when test="@command != 'submitAction'">
								<xsl:choose>
									<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
									<xsl:otherwise>
										<xsl:text>DCT.Submit.</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:value-of select="@command"/>
								<xsl:choose>
									<xsl:when test="normalize-space(@page)!=''">
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>'</xsl:text>
										<xsl:if test="@post">
											<xsl:text>,'</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>'</xsl:text>
										</xsl:if>
										<xsl:text>);</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@post"/>
										<xsl:text>');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
									<xsl:otherwise>
										<xsl:text>DCT.Submit.</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:value-of select="@command"/>
								<xsl:text>('</xsl:text>
								<xsl:value-of select="@post"/>
								<xsl:text>');</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<a>
						<xsl:attribute name="name">
							<xsl:text>name_</xsl:text>
							<xsl:value-of select="@id"/>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:text>id_</xsl:text>
							<xsl:value-of select="@id"/>
						</xsl:attribute>
						<xsl:attribute name="href">javascript:;</xsl:attribute>
						<xsl:attribute name="onclick">
							<xsl:value-of select="$href"/>
						</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
					</a>
				</li>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildActionForClient">
		<xsl:variable name="location">client</xsl:variable>
		<xsl:if test="/page/content/portals/portal[@active = '1']/@portalType = 'pas' or /page/content/portals/portal[@active = '1']/@name = 'Billing' or /page/content/portals/portal[@active = '1']/@portalType = 'uw'">
			<xsl:for-each select="/page/actions/action[@location=$location]">
				<li>
					<xsl:variable name="href">
						<xsl:choose>
							<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
							<xsl:otherwise>
								<xsl:text>DCT.Submit.</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="@command"/>
						<xsl:text>(</xsl:text>
						<xsl:if test="@content!=''">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="@content"/>
							<xsl:text>',</xsl:text>
						</xsl:if>
						<xsl:if test="@post!=''">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="@post"/>
							<xsl:text>',</xsl:text>
						</xsl:if>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="/page/state/clientID"/>
						<xsl:text>');</xsl:text>
					</xsl:variable>
					<a>
						<xsl:attribute name="name">
							<xsl:text>name_</xsl:text>
							<xsl:value-of select="@id"/>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:text>id_</xsl:text>
							<xsl:value-of select="@id"/>
						</xsl:attribute>
						<xsl:attribute name="href">javascript:;</xsl:attribute>
						<xsl:attribute name="onclick">
							<xsl:value-of select="$href"/>
						</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
					</a>
				</li>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!--************************************************************************************************************* -->
	<!--*************************************************************************************************************
		Task Management Templates
		***************************************************************************************************************** -->
	<xsl:template name="taskQueueWidget">
		<xsl:param name="id"/>
		<xsl:param name="listTitle"/>
		<xsl:param name="listMode"/>
		<xsl:param name="newButtonText"/>
		<xsl:param name="titleColumnRenderer"/>
		<xsl:param name="actionColumnRenderer"/>
		<xsl:param name="showCheckbox"/>
		<xsl:param name="multipleSelect"/>
		<xsl:param name="targetPage"/>
		<xsl:param name="className"/>
		<xsl:param name="classMethod"/>
		<!-- hidden form fields go here -->
		<xsl:variable name="cultureFormat">
			<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
		</xsl:variable>
		<!-- Build the paging grid. -->
		<xsl:call-template name="buildPagingGridPanel">
			<xsl:with-param name="gridID" select="$id"/>
			<xsl:with-param name="toolBarTitle">
				<xsl:value-of select="$listTitle"/>
			</xsl:with-param>
			<xsl:with-param name="targetPage">
				<xsl:choose>
					<xsl:when test="$targetPage != ''">
						<xsl:value-of select="$targetPage"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>taskList</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="filterSubSet">
				<xsl:text>'</xsl:text>
				<xsl:value-of select="$listMode"/>
				<xsl:text>'</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="pageSize">
				<xsl:text>10</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordCount">
				<xsl:text>TaskList/SearchControl/Paging/TotalCount</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordPath">
				<xsl:text>Task/ItemData</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordID">
				<xsl:text>TaskId</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="className">
				<xsl:value-of select="$className"/>
			</xsl:with-param>
			<xsl:with-param name="classMethod">
				<xsl:value-of select="$classMethod"/>
			</xsl:with-param>
			<xsl:with-param name="columns">
				<xsl:text>[{name: 'taskId', type: 'string', mapping: 'TaskId'},</xsl:text>
				<xsl:text>{name: 'taskLockingTS', type: 'string', mapping: 'TaskLockingTS'},</xsl:text>
				<xsl:text>{name: 'policyId', type: 'string', mapping: 'ObjectId'},</xsl:text>
				<xsl:text>{name: 'objectReference', type: 'string', mapping: 'ObjectReference'},</xsl:text>
				<xsl:text>{name: 'entityId', type: 'string', mapping: 'EntityId'},</xsl:text>
				<xsl:text>{name: 'entityName', type: 'string', mapping: 'EntityName'},</xsl:text>
				<xsl:text>{name: 'assignedUserId', type: 'string', mapping: 'AssignedUserId'},</xsl:text>
				<xsl:text>{name: 'assignedFullName', type: 'string', mapping: 'AssignedFullName'},</xsl:text>
				<xsl:text>{name: 'creationDate', type: 'date', dateFormat: 'Y-m-dTH:i:s', mapping: 'CreationDate'},</xsl:text>
				<xsl:text>{name: 'createdBy', type: 'string', mapping: 'CreatedBy'},</xsl:text>
				<xsl:text>{name: 'createdByFullName', type: 'string', mapping: 'CreatedByFullName'},</xsl:text>
				<xsl:text>{name: 'DueDate', type: 'date', dateFormat: 'Y-m-d', mapping: 'DueDate'},</xsl:text>
				<xsl:text>{name: 'title', type: 'string', mapping: 'TaskTitle'},</xsl:text>
				<xsl:text>{name: 'taskType', type: 'string', mapping: 'TaskType'},</xsl:text>
				<xsl:text>{name: 'taskStatus', type: 'string', mapping: 'TaskStatus'},</xsl:text>
				<xsl:text>{name: 'taskStatusCode', type: 'string', mapping: 'TaskStatusCode'},</xsl:text>
				<xsl:text>{name: 'statusReason', type: 'string', mapping: 'TaskStatusReason'},</xsl:text>
				<xsl:text>{name: 'viewOnly', type: 'string', mapping: '@viewOnly'},</xsl:text>
				<xsl:text>{name: 'canPickup', type: 'string', mapping: '@canPickup'},</xsl:text>
				<xsl:text>{name: 'canReassign', type: 'string', mapping: '@canReassign'},</xsl:text>
				<xsl:text>{name: 'percentComplete', type: 'string', mapping: 'PercentageComplete'}]</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="columnModel">
				<xsl:choose>
					<xsl:when test="$id = 'QueueAdmin'">
						<xsl:text>[{text: DCT.Localization.translate("TaskId"), hidden: true, dataIndex: 'taskId'},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("DueDate"), dataIndex: 'DueDate', width: 85, sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="$cultureFormat"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Queue"), dataIndex: 'entityName', width: 90, sortable: true},</xsl:text>
						<xsl:choose>
							<xsl:when test="$listMode = 'QueueAdmin'">
								<xsl:text>{text: DCT.Localization.translate("Title"), dataIndex: 'title', sortable: true, width: 90, renderer: </xsl:text>
								<xsl:value-of select="$titleColumnRenderer"/>
								<xsl:text>},</xsl:text>
								<xsl:text>{text: DCT.Localization.translate("AssignedTo"), dataIndex: 'assignedFullName', width: 90, sortable: true},</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>{text: DCT.Localization.translate("Title"), dataIndex: 'title', sortable: true, width: 160, renderer: </xsl:text>
								<xsl:value-of select="$titleColumnRenderer"/>
								<xsl:text>},</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>{text: DCT.Localization.translate("ReferenceNumSymbol"), dataIndex: 'objectReference', sortable: true, width: 85},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Status"), dataIndex: 'taskStatus', width: 50, sortable: true},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Category"), dataIndex: 'taskType', width: 60, sortable: true},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Created"), dataIndex: 'creationDate', width: 115, sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '3')"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: "</xsl:text>
						<xsl:text disable-output-escaping="yes">&#xA0;</xsl:text>
						<xsl:text>", dataIndex: '', width: 80, sortable: false, renderer: </xsl:text>
						<xsl:value-of select="$actionColumnRenderer"/>
						<xsl:text>}]</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>[{text: DCT.Localization.translate("TaskId"), hidden: true, dataIndex: 'taskId'},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Queue"), dataIndex: 'entityName', width: 90, sortable: true},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Category"), dataIndex: 'taskType', width: 60, sortable: true},</xsl:text>
						<xsl:choose>
							<xsl:when test="$listMode = 'QueueAdmin'">
								<xsl:text>{text: DCT.Localization.translate("AssignedTo"), dataIndex: 'assignedFullName', width: 90, sortable: true},</xsl:text>
								<xsl:text>{text: DCT.Localization.translate("Title"), dataIndex: 'title', sortable: true, width: 90, renderer: </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>{text: DCT.Localization.translate("Title"), dataIndex: 'title', sortable: true, width: 160, renderer: </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="$titleColumnRenderer"/>
						<xsl:text>},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Status"), dataIndex: 'taskStatus', width: 50, sortable: true},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("ReferenceNumSymbol"), dataIndex: 'objectReference', sortable: true, width: 85},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("Created"), dataIndex: 'creationDate', width: 115, sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '3')"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: DCT.Localization.translate("DueDate"), dataIndex: 'DueDate', width: 85, sortable: true, renderer: Ext.util.Format.dateRenderer('</xsl:text>
						<xsl:value-of select="$cultureFormat"/>
						<xsl:text>')},</xsl:text>
						<xsl:text>{text: "</xsl:text>
						<xsl:text disable-output-escaping="yes">&#xA0;</xsl:text>
						<xsl:text>", dataIndex: '', width: 80, sortable: false, renderer: </xsl:text>
						<xsl:value-of select="$actionColumnRenderer"/>
						<xsl:text>}]</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="rowClassRenderer">DCT.Grid.taskRowRenderer</xsl:with-param>
			<xsl:with-param name="showGridCheckbox">
				<xsl:value-of select="$showCheckbox"/>
			</xsl:with-param>
			<xsl:with-param name="multipleSelect">
				<xsl:value-of select="$multipleSelect"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<!--************************************************************************************************************* -->
	<xsl:template name="createSubMenuArea">
		<xsl:param name="screenName"/>
		<xsl:call-template name="buildSubMenuNavigation"/>
	</xsl:template>
	<!--*************************************************************************************************************
		Attachment Grid Template
		***************************************************************************************************************** -->
	<xsl:template name="attachmentsWidgetGrid">
		<xsl:param name="id"/>
		<xsl:param name="newButtonText"/>
		<xsl:param name="titleColumnRenderer"/>
		<xsl:param name="actionColumnRenderer"/>
		<xsl:param name="showCheckbox"/>
		<xsl:param name="multipleSelect"/>
		<xsl:param name="targetPage"/>
		<xsl:param name="className">
			<xsl:text>attachmentGridPanel</xsl:text>
		</xsl:param>
		<!-- Build the paging grid. -->
		<xsl:call-template name="buildPagingGridPanel">
			<xsl:with-param name="gridID" select="$id"/>
			<xsl:with-param name="targetPage">
				<xsl:text>attachmentList</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordCount">
				<xsl:text>attachments/@recordCount</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordPath">
				<xsl:text>attachment</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="recordID">
				<xsl:text>@attachmentID</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="className">
				<xsl:value-of select="$className"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<!--*************************************************************************************************************
		Build Sub Menu Navigation links 
		************************************************************************************************************* -->
	<xsl:template name="buildSubMenuNavigation">
		<div id="subactiongroup">
			<ul id="subMenuActions">
				<xsl:choose>
					<xsl:when test="count(/page/actions/action[@page=/page/state/activeParentPage]/actionGroup[@id=/page/content/@page])&gt;0">
						<xsl:for-each select="/page/actions/action[@page=/page/state/activeParentPage]/actionGroup[@id=/page/content/@page]/action[@location='header']">
							<li>
								<xsl:if test="$currentPage=@page">
									<xsl:attribute name="class">activeTab</xsl:attribute>
								</xsl:if>
								<xsl:variable name="href">
									<xsl:choose>
										<xsl:when test="@command != 'submitAction'">
											<xsl:choose>
												<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
												<xsl:otherwise>
													<xsl:text>DCT.Submit.</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="@command"/>
											<xsl:text>('</xsl:text>
											<xsl:value-of select="@page"/>
											<xsl:text>'</xsl:text>
											<xsl:if test="@post">
												<xsl:text>,'</xsl:text>
												<xsl:value-of select="@post"/>
												<xsl:text>'</xsl:text>
											</xsl:if>
											<xsl:text>);</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
												<xsl:otherwise>
													<xsl:text>DCT.Submit.</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="@command"/>
											<xsl:text>('</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>');</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<a>
									<xsl:attribute name="name">
										<xsl:text>name_</xsl:text>
										<xsl:value-of select="@id"/>
									</xsl:attribute>
									<xsl:attribute name="id">
										<xsl:text>id_</xsl:text>
										<xsl:value-of select="@id"/>
									</xsl:attribute>
									<xsl:attribute name="href">javascript:;</xsl:attribute>
									<xsl:attribute name="onclick">
										<xsl:value-of select="$href"/>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
									<xsl:text></xsl:text>
								</a>
							</li>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="/page/actions/action[@page=/page/state/activeParentPage]/actionGroup[@id='Default']/action[@location='header']">
							<li>
								<xsl:if test="$currentPage=@page">
									<xsl:attribute name="class">activeTab</xsl:attribute>
								</xsl:if>
								<xsl:variable name="href">
									<xsl:choose>
										<xsl:when test="@command != 'submitAction'">
											<xsl:choose>
												<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
												<xsl:otherwise>
													<xsl:text>DCT.Submit.</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="@command"/>
											<xsl:text>('</xsl:text>
											<xsl:value-of select="@page"/>
											<xsl:text>'</xsl:text>
											<xsl:if test="@post">
												<xsl:text>,'</xsl:text>
												<xsl:value-of select="@post"/>
												<xsl:text>'</xsl:text>
											</xsl:if>
											<xsl:text>);</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:choose>
												<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
												<xsl:otherwise>
													<xsl:text>DCT.Submit.</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
											<xsl:value-of select="@command"/>
											<xsl:text>('</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>');</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<a>
									<xsl:attribute name="name">
										<xsl:text>name_</xsl:text>
										<xsl:value-of select="@id"/>
									</xsl:attribute>
									<xsl:attribute name="id">
										<xsl:text>id_</xsl:text>
										<xsl:value-of select="@id"/>
									</xsl:attribute>
									<xsl:attribute name="href">javascript:;</xsl:attribute>
									<xsl:attribute name="onclick">
										<xsl:value-of select="$href"/>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
								</a>
							</li>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
			</ul>
		</div>
	</xsl:template>
	<xsl:template name="buildPolicyLeftNavHeader">

		<xsl:choose>
			<xsl:when test="/page/content/portals/portal[@active = '1']/@name = 'Billing'">
				<xsl:if test="(number(/page/content/ExpressCache/AccountId) &gt; 0)">
					<div id="policyActiveSection">
						<div id="activeAccount">
							<div class="activeAccountName" id="activeAccountNameId">
								<xsl:call-template name="getFullPartyName">
									<xsl:with-param name="partyId" select="/page/content/ExpressCache/Account/AssociatedParties/Party[@Role='Payor']/@PartyId"/>
								</xsl:call-template>
							</div>
							<div id="activeAccountReferenceId">
								<xsl:value-of select="/page/content/ExpressCache/Account/AccountReference"/>
							</div>
							<div id="activeAccountStatusId">
								<xsl:if test="normalize-space(/page/content/ExpressCache/Account/AccountStatus)!=''">
									<xsl:value-of select="xslNsODExt:getDictRes('OpenParen')"/>
									<xsl:value-of select="/page/content/ExpressCache/Account/AccountStatus"/>
									<xsl:value-of select="xslNsODExt:getDictRes('CloseParen')"/>
								</xsl:if>
							</div>
						</div>
					</div>
				</xsl:if>
			</xsl:when>
			<xsl:when test="/page/content/portals/portal[@active = '1']/@name = 'Party'">
				<!--<xsl:if test="number(/page/state/quoteID) &gt; 0">-->
				<div id="policyActiveSection">
					<div id="activeAccount">
						<xsl:variable name="clientName">
							<xsl:choose>
								<xsl:when test="/page/content/getPage/body/@PartyDisplayName">
									<xsl:value-of select="/page/content/getPage/body/@PartyDisplayName"/>
								</xsl:when>
								<xsl:when test="/page/content/details/client">
									<xsl:value-of disable-output-escaping="no" select="/page/content/details/client/name"/>
								</xsl:when>
								<xsl:when test="/page/content/Party">
									<xsl:value-of disable-output-escaping="no" select="/page/content/Party/PartyCorrespondenceName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of disable-output-escaping="no" select="/page/state/clientName"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="splitClientName">
							<xsl:value-of select="xslNsExt:splitText($clientName,' ','..','24')"/>
						</xsl:variable>
						<xsl:variable name="isSplit">
							<xsl:choose>
								<xsl:when test="contains($splitClientName, '..')">1</xsl:when>
								<xsl:otherwise>0</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:if test="$isSplit = '1'">
							<script>Ext.QuickTips.init();</script>
						</xsl:if>
						<div id="activeAccountNameId">
							<xsl:if test="$isSplit = '1'">
								<xsl:attribute name="ext:qtitle">
									<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
									<xsl:text> </xsl:text>
								</xsl:attribute>
								<xsl:attribute name="data-title">
									<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
									<xsl:text> </xsl:text>
								</xsl:attribute>
								<xsl:attribute name="data-qtip">
									<xsl:value-of select="$clientName"/>
								</xsl:attribute>
								<xsl:attribute name="data-tip">
									<xsl:value-of select="$clientName"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="$splitClientName"/>
						</div>
						<div id="activeAccountReferenceId">
							<!--<xsl:value-of select="/page/state/policyNumber"/>-->
						</div>
						<div id="activeAccountStatusId"></div>
					</div>
				</div>
				<!--</xsl:if>-->
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="number(/page/state/quoteID) &gt; 0">
					<div id="policyActiveSection">
						<div id="activeAccount">
							<xsl:variable name="clientName">
								<xsl:value-of disable-output-escaping="no" select="/page/state/clientName"/>
							</xsl:variable>
							<xsl:variable name="splitClientName">
								<xsl:value-of select="xslNsExt:splitText($clientName,' ','..','24')"/>
							</xsl:variable>
							<xsl:variable name="isSplit">
								<xsl:choose>
									<xsl:when test="contains($splitClientName, '..')">1</xsl:when>
									<xsl:otherwise>0</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:if test="$isSplit = '1'">
								<script>Ext.QuickTips.init();</script>
							</xsl:if>
							<div id="activeAccountNameId">
								<xsl:if test="$isSplit = '1'">
									<xsl:attribute name="ext:qtitle">
										<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
										<xsl:text> </xsl:text>
									</xsl:attribute>
									<xsl:attribute name="data-title">
										<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
										<xsl:text> </xsl:text>
									</xsl:attribute>
									<xsl:attribute name="data-qtip">
										<xsl:value-of select="$clientName"/>
									</xsl:attribute>
									<xsl:attribute name="data-tip">
										<xsl:value-of select="$clientName"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="$splitClientName"/>
							</div>
							<div id="activeAccountReferenceId">
								<xsl:value-of select="/page/state/policyNumber"/>
							</div>
							<div id="activeAccountStatusId"></div>
						</div>
					</div>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildPolicyRelatedTasks">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:if test="(number(/page/state/quoteID) &gt; 0) or (number(/page/state/isConsumerAccess) &gt; 0)">
			<xsl:if test="($showActionBar and $showHeader and (/page/content/portals/portal[@active='1']/@portalType='pas'))">
				<div id="relatedActions">
					<div id="relatedActionsTitle">
						<xsl:choose>
							<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
								<xsl:value-of select="xslNsODExt:getDictRes('ArrangementActions')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyActions')"/>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<ul>
						<xsl:call-template name="buildActiveSessionSection"/>
						<xsl:call-template name="buildDiary">
							<xsl:with-param name="showHeader" select="$showHeader"/>
						</xsl:call-template>
						<xsl:call-template name="buildActionForClient"/>
						<xsl:call-template name="buildActionsNavigation">
							<xsl:with-param name="location">actions</xsl:with-param>
						</xsl:call-template>
					</ul>
					<div>
						<!-- Creating inner div with left-nav-bottom.png background -->
						<xsl:attribute name="class">
							<xsl:text>innerleftpanel</xsl:text>
						</xsl:attribute>
					</div>
				</div>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildActiveSessionSection">
		<xsl:choose>
			<xsl:when test="/page/state/clientName != ''">
				<li>
					<!--<xsl:value-of select="xslNsODExt:getDictRes('CurrentQuotePolicyInProgress')" />-->
					<xsl:variable name="clientName">
						<xsl:value-of disable-output-escaping="no" select="/page/state/clientName"/>
					</xsl:variable>
					<xsl:variable name="splitClientName">
						<xsl:value-of select="xslNsExt:splitText($clientName,' ','..','12')"/>
					</xsl:variable>
					<xsl:variable name="isSplit">
						<xsl:choose>
							<xsl:when test="contains($splitClientName, '..')">1</xsl:when>
							<xsl:otherwise>0</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:if test="$isSplit = '1'">
						<script>Ext.QuickTips.init();</script>
					</xsl:if>
					<a>
						<xsl:attribute name="name">returnToActiveSessionA</xsl:attribute>
						<xsl:attribute name="id">returnToActiveSessionA</xsl:attribute>
						<xsl:attribute name="href">javascript:;</xsl:attribute>
						<xsl:attribute name="onclick">
							<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
							<xsl:value-of select="/page/state/quoteID"/>
							<xsl:text>','','1');</xsl:text>
						</xsl:attribute>

						<xsl:if test="$isSplit = '1'">
							<xsl:attribute name="ext:qtitle">
								<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
								<xsl:text> </xsl:text>
							</xsl:attribute>
							<xsl:attribute name="data-qtip">
								<xsl:value-of select="$clientName"/>
							</xsl:attribute>
							<xsl:attribute name="data-title">
								<xsl:value-of select="xslNsODExt:getDictRes('Name_colon')"/>
								<xsl:text> </xsl:text>
							</xsl:attribute>
							<xsl:attribute name="data-tip">
								<xsl:value-of select="$clientName"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="xslNsODExt:getDictRes('ViewPolicy')"/>
						<xsl:text>(</xsl:text>
						<xsl:value-of select="$splitClientName"/>
						<xsl:text>)</xsl:text>
					</a>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text></xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getFullPartyName">
		<xsl:param name="partyId"/>
		<xsl:if test="normalize-space(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId=$partyId]/Party/PartyFirstName)!=''">
			<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId=$partyId]/Party/PartyFirstName"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:if test="normalize-space(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId=$partyId]/Party/PartyMiddleName)!=''">
			<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId=$partyId]/Party/PartyMiddleName"/>
			<xsl:text> </xsl:text>
		</xsl:if>
		<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId=$partyId]/Party/PartyName"/>
	</xsl:template>
</xsl:stylesheet>