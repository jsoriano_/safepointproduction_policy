﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="contentPage.xsl"/>
	<xsl:import href="dctInterviewControls.xsl"/>
	<xsl:import href="dctGenericSetFieldFormat.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>	

</xsl:stylesheet>
