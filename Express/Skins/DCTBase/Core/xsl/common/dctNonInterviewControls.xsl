﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctTemplatesSystem.xsl"/>
	<!--*************************************************************************************************************
		Common control templates
		************************************************************************************************************* -->

	<!--*************************************************************************************************************
		Build tabs widget
		************************************************************************************************************* -->
	<xsl:template name="buildTab">
		<xsl:param name="tabID">tab</xsl:param>
		<xsl:param name="src"/>
		<xsl:param name="href"/>
		<xsl:param name="caption"/>
		<xsl:param name="current"/>
		<xsl:variable name="class">
			<xsl:choose>
				<xsl:when test="$current = 1">
					<xsl:text>activeTab</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>inactiveTab</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<li class="{$class}">
			<xsl:choose>
				<xsl:when test="$href = ''">
					<xsl:attribute name="name">
						<xsl:value-of select="$tabID"/>
					</xsl:attribute>
					<xsl:value-of select="$caption"/>
				</xsl:when>
				<xsl:otherwise>
					<a>
						<xsl:attribute name="href">
							<xsl:value-of select="$href"/>
						</xsl:attribute>
						<xsl:attribute name="name">
							<xsl:value-of select="$tabID"/>
							<xsl:text>Link</xsl:text>
						</xsl:attribute>
						<xsl:value-of select="$caption"/>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</li>
	</xsl:template>
	<!--*************************************************************************************************************
		Build action group for main actions for section
		************************************************************************************************************* -->
	<xsl:template match="actionGroup">
		<ul>
			<xsl:if test="@id">
				<xsl:attribute name="id">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@headerOnly!='1'">
				<xsl:attribute name="class">
					<xsl:text>action</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="@headerOnly='1'">
					<xsl:apply-templates select="action[@location='header']"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="action"/>
				</xsl:otherwise>
			</xsl:choose>
		</ul>
	</xsl:template>
	<!--*************************************************************************************************************
		Helper Templates
		************************************************************************************************************* -->
	<xsl:template name="limitText">
		<xsl:param name="text"/>
		<xsl:param name="limit"/>
		<xsl:choose>
			<xsl:when test="string-length($text) &gt; number($limit)">
				<xsl:value-of select="substring($text,1,number($limit)-3)"/>
				<xsl:text>...</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
