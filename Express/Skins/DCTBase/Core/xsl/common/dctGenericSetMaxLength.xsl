﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:template name="GetDefaultMaxLength">
		<xsl:param name="fieldMask"/>
		<xsl:param name="maximum"/>
		<xsl:param name="type"/>
		<xsl:choose>
			<xsl:when test="$fieldMask='time'">11</xsl:when>
			<xsl:when test="$fieldMask='time24'">11</xsl:when>
			<xsl:when test="$fieldMask='ssn'">11</xsl:when>
			<xsl:when test="$fieldMask='phone'">12</xsl:when>
			<xsl:when test="$fieldMask='zipcode'">10</xsl:when>
			<xsl:when test="$fieldMask='email'">30</xsl:when>
			<xsl:when test="$fieldMask='ip'">15</xsl:when>
			<xsl:when test="$type='date'">10</xsl:when>
			<xsl:when test="substring($fieldMask, 1, 5)='regex'">ignore</xsl:when>
			<xsl:when test="contains($fieldMask, 'lower')">ignore</xsl:when>
			<xsl:when test="contains($fieldMask, 'upper')">ignore</xsl:when>
			<xsl:when test="$fieldMask!=''">
				<xsl:value-of select="string-length($fieldMask)"/>
			</xsl:when>
			<xsl:when test="$maximum!=''">
				<xsl:choose>
					<!--floats need more room then just string-length.-->
					<xsl:when test="$type!='float'">
						<xsl:value-of select="string-length(string($maximum))"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="(string-length(string($maximum)))+4"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:when test="$fieldMask=''"/>			
			<xsl:otherwise>
				<xsl:value-of select="string-length(translate($fieldMask,'$,',''))"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
