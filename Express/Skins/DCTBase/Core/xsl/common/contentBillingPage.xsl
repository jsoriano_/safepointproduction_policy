﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="contentPage.xsl"/>
	<xsl:import href="dctCommonBilling.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>
</xsl:stylesheet>
