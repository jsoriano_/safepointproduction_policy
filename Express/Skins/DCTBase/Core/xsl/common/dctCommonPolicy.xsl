﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctCommonControls.xsl"/>
	<!--*************************************************************************************************************
		Common control templates
		************************************************************************************************************* -->
	<!--*************************************************************************************************************
		Build Active Session widget
		************************************************************************************************************* -->
	<xsl:template name="selectAgencyControl">
		<xsl:param name="agencyListCount"/>
		<xsl:param name="agencyListMax"/>
		<xsl:param name="agencyListFilter"/>
		<xsl:param name="showLabel"/>
		<xsl:param name="isRequired"/>
		<xsl:choose>
			<xsl:when test="number(/page/state/@agencyCount) &gt; number(/page/content/agencyListFilter/@maxItems)">
				<div id="selectAgencyControlDiv">
					<input type="hidden" id="_AgencyID" name="_AgencyID" value="{/page/content/agencyList/@agencyID}"/>
					<div id="agencyIDField">
						<xsl:variable name="agencyId" select="/page/content/agencyList/@agencyID"/>
						<xsl:choose>
							<xsl:when test="number(/page/content/agencyList/@agencyID) = 0">
								<xsl:value-of select="xslNsODExt:getDictRes('AllAgencies')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="/page/content/agencyList/agency[@agencyID = $agencyId]/@name"/>
								<xsl:if test="/page/content/agencyList/agency[@agencyID = $agencyId]/@city != ''">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="/page/content/agencyList/agency[@agencyID = $agencyId]/@city"/>
								</xsl:if>
								<xsl:if test="/page/content/agencyList/agency[@agencyID = $agencyId]/@state != ''">
									<xsl:text>, </xsl:text>
									<xsl:value-of select="/page/content/agencyList/agency[@agencyID = $agencyId]/@state"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div id="setAgencyButton">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">browseAgenciesA</xsl:with-param>
						<xsl:with-param name="id">setAgencyActions</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Util.popUpAgencies();</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectAnAgency')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:when>
			<xsl:otherwise>
				<div id="agencyField">
					<xsl:if test="$showLabel = 'true'">
						<div id="agencyLabel">
							<label id="agencyFieldLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('Agency_colon')"/>
							</label>
							<xsl:if test="$isRequired = 'true'">
								<SPAN class="required">
									<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
								</SPAN>
							</xsl:if>
						</div>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="$currentPage = $newQuoteContentPage">
							<xsl:call-template name="buildSystemControl">
								<xsl:with-param name="id">AgencyID</xsl:with-param>
								<xsl:with-param name="name">_AgencyID</xsl:with-param>
								<xsl:with-param name="type">select</xsl:with-param>
								<xsl:with-param name="optionlist">
									<xsl:for-each select="/page/content/agencyList/agency">
										<option value="{@agencyID}">
											<xsl:value-of select="@name"/>
											<xsl:if test="@city != ''">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="@city"/>
											</xsl:if>
											<xsl:if test="@state != ''">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="@state"/>
											</xsl:if>
										</option>
									</xsl:for-each>
								</xsl:with-param>
								<xsl:with-param name="controlClass">agencyComboField</xsl:with-param>
								<xsl:with-param name="width">120</xsl:with-param>
								<xsl:with-param name="required">1</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="/page/state/user/agencyID"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="buildSystemControl">
								<xsl:with-param name="id">AgencyID</xsl:with-param>
								<xsl:with-param name="name">_AgencyID</xsl:with-param>
								<xsl:with-param name="type">select</xsl:with-param>
								<xsl:with-param name="optionlist">
									<option value=" ">
										<xsl:value-of select="xslNsODExt:getDictRes('AllAgencies')"/>
									</option>
									<xsl:for-each select="/page/content/agencyList/agency">
										<option value="{@agencyID}">
											<xsl:value-of select="@name"/>
											<xsl:if test="@city != ''">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="@city"/>
											</xsl:if>
											<xsl:if test="@state != ''">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="@state"/>
											</xsl:if>
										</option>
									</xsl:for-each>
								</xsl:with-param>
								<xsl:with-param name="width">100</xsl:with-param>
								<xsl:with-param name="required">1</xsl:with-param>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildPortfolioList">
		<xsl:variable name="RoleBasedSecurity" select="/page/state/@roleSecurity=1"/>
		<xsl:variable name="PortfolioWriteAccess" select="not($RoleBasedSecurity and /page/state[@securityLevel&gt;='1']) or ($RoleBasedSecurity and /page/content/portfolios/@writeAccess='True')"/>
		<xsl:variable name="PortfolioReadAccess" select="not($RoleBasedSecurity and /page/state[@securityLevel&gt;='1']) or ($RoleBasedSecurity and /page/content/portfolios/@readAccess='True')"/>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:text>toggleHeaderNoHover</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:text>portfolioInformationHeader</xsl:text>
			</xsl:attribute>
			<xsl:value-of select="xslNsODExt:getDictRes('PortfolioPoliciesAndQuotes')"/>
		</xsl:element>
		<div id="portfolioInformationSection" class="toggleDivContainer">
			<xsl:if test="not(portfolio) and $PortfolioWriteAccess">
				<div style="margin-left:10px;">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">addPortfolioA</xsl:with-param>
						<xsl:with-param name="id">addPortfolioA</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.addNewPortfolio('</xsl:text>
							<xsl:value-of select="/page/content/clientDetails/clientID"/>
							<xsl:text>','</xsl:text>
							<xsl:value-of select="$newQuoteContentPage"/>
							<xsl:text>')</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('AddNewPortfolio')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
			<div id="quoteList">
				<!-- Display all of the portfolios w/ polices -->
				<xsl:for-each select="page/content/portfolios/portfolio">
					<xsl:if test="count(portfolioPolicyList/policy)&gt;0">
						<xsl:call-template name="portfolio">
							<xsl:with-param name="PortfolioWriteAccess" select="$PortfolioWriteAccess"/>
							<xsl:with-param name="PortfolioReadAccess" select="$PortfolioReadAccess"/>
						</xsl:call-template>
						<xsl:call-template name="portfolioPolicyList"/>
					</xsl:if>
				</xsl:for-each>
				<!-- Display the empty portfolios at the end of the list -->
				<xsl:for-each select="page/content/portfolios/portfolio">
					<xsl:if test="count(portfolioPolicyList/policy)=0">
						<xsl:call-template name="portfolio">
							<xsl:with-param name="PortfolioWriteAccess" select="$PortfolioWriteAccess"/>
							<xsl:with-param name="PortfolioReadAccess" select="$PortfolioReadAccess"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<!-- 
			Template to render the portfolio data. 
			PortfolioWriteAccess is True whenever the user has the proper privileges to write to the portfolio (AdministerPortfolio).
			PortfolioReadAccess is True whenever the user has the proper privileges to read the portfolio information (AccessPortfolioInfo).
		-->
	<xsl:template name="portfolio">
		<xsl:param name="PortfolioWriteAccess"/>
		<xsl:param name="PortfolioReadAccess"/>
		<div class="portfolioHeading">
			<xsl:choose>
				<xsl:when test="$PortfolioReadAccess or $PortfolioWriteAccess">
					<a href="javascript:;">
						<xsl:attribute name="onclick">
							<xsl:text>DCT.Submit.loadPortfolio(</xsl:text>
							<xsl:value-of select="@portfolioID"/>
							<xsl:text>)</xsl:text>
						</xsl:attribute>
						<xsl:value-of select="@description"/>
						<xsl:if test="@description=''">
							<xsl:value-of select="xslNsODExt:getDictRes('Portfolio')"/>
						</xsl:if>
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@description"/>
					<xsl:if test="@description=''">
						<xsl:value-of select="xslNsODExt:getDictRes('Portfolio')"/>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$PortfolioWriteAccess">
				<img src="{$imageDir}icons/delete.png" border="0" class="imageButton" alt="{xslNsODExt:getDictRes('DeletePortfolio')}" title="{xslNsODExt:getDictRes('DeletePortfolio')}">
					<xsl:attribute name="onClick">
						<xsl:text>DCT.Submit.deletePortfolio(</xsl:text>
						<xsl:value-of select="@portfolioID"/>
						<xsl:text>);</xsl:text>
					</xsl:attribute>
				</img>
			</xsl:if>
		</div>
		<br clear="all"/>
	</xsl:template>
	<xsl:template name="buildPolicyGridHeader">
		<tr class="detailsLabel">
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('Product')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('Description')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyStatus')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('CurrentTransaction')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('EffectiveDate')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsODExt:getDictRes('ExpirationDate')"/>
			</td>
			<td>
				<xsl:text> </xsl:text>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="buildPolicyActions">
		<xsl:param name="policyID"/>
		<xsl:param name="status"/>
		<td class="actionCell">
			<img src="{$imageDir}icons/application_view_list.png" border="0" class="imageButton" alt="{xslNsODExt:getDictRes('ViewDetailsAndHistory')}" title="{xslNsODExt:getDictRes('ViewDetailsAndHistory')}">
				<xsl:attribute name="onClick">DCT.Submit.gotoPageForQuote('policy','<xsl:value-of select="$policyID"/>')</xsl:attribute>
			</img>
			<xsl:if test="not($IsReadOnly)">
				<xsl:choose>
					<xsl:when test="/page/state/user/privileges/privilege[@name='DeleteQuotes']">
						<img src="{$imageDir}icons/delete.png" border="0" class="imageButton" alt="{xslNsODExt:getDictRes('DeleteQuote')}" title="{xslNsODExt:getDictRes('DeleteQuote')}">
							<xsl:attribute name="onClick">DCT.Submit.actOnQuote('delete','<xsl:value-of select="$policyID"/>')</xsl:attribute>
						</img>
					</xsl:when>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="/page/state/user/privileges/privilege[@name='DuplicatePolicy'] or /page/state[@securityLevel&gt;='8']">
						<img src="{$imageDir}icons/page_white_copy.png" border="0" class="imageButton" alt="{xslNsODExt:getDictRes('Duplicate')}" title="{xslNsODExt:getDictRes('Duplicate')}">
							<xsl:attribute name="onClick">DCT.Submit.actOnQuote('duplicate','<xsl:value-of select="$policyID"/>')</xsl:attribute>
						</img>
					</xsl:when>
				</xsl:choose>
				<img src="{$imageDir}icons/disk_download.png" border="0" class="imageButton" alt="{xslNsODExt:getDictRes('DownloadXML')}" title="{xslNsODExt:getDictRes('DownloadXML')}">
					<xsl:attribute name="onClick">DCT.Submit.actOnQuote('download','<xsl:value-of select="$policyID"/>')</xsl:attribute>
				</img>
				<img src="{$imageDir}icons/download_policy.png" width="16" border="0" alt="{xslNsODExt:getDictRes('DownloadPolicy')}" title="{xslNsODExt:getDictRes('DownloadPolicy')}" class="imageButton">
					<xsl:attribute name="onClick">DCT.Submit.downloadSubmissionClientPolicy('submissionDownloadPolicy','<xsl:value-of select="$policyID"/>')</xsl:attribute>
				</img>
			</xsl:if>
		</td>
	</xsl:template>
	<!-- 
			Template to render the quotes/policies. 
		-->
	<xsl:template name="portfolioPolicyList">
		<table class="formTbl">
			<xsl:call-template name="buildPolicyGridHeader"/>
			<xsl:choose>
				<xsl:when test="count(.//policy)=0">
					<tr>
						<td colspan="7" align="center">
							<xsl:value-of select="xslNsODExt:getDictRes('NoPoliciesFound')"/>
						</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select=".//policy">
						<tr>
							<td>
								<a href="javascript:;">
									<xsl:attribute name="onclick">DCT.Submit.actOnQuote('load','<xsl:value-of select="QuoteID"/>','<xsl:value-of select="LOB"/>')</xsl:attribute>
									<xsl:value-of select="LOB"/>
								</a>
							</td>
							<td>
								<xsl:value-of select="Description2"/>
								<xsl:if test="Description2=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<td>
								<xsl:value-of select="Status"/>
							</td>
							<td>
								<xsl:value-of select="Description"/>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', EffectiveDate)"/>
								<xsl:if test="EffectiveDate=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', ExpirationDate)"/>
								<xsl:if test="ExpirationDate=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<xsl:call-template name="buildPolicyActions">
								<xsl:with-param name="policyID" select="QuoteID"/>
								<xsl:with-param name="status" select="Status"/>
							</xsl:call-template>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
	</xsl:template>
	<xsl:template name="policyList">
		<table class="formTbl">
			<xsl:call-template name="buildPolicyGridHeader"/>
			<xsl:choose>
				<xsl:when test="count(page/content/policyList/policy)=0">
					<tr>
						<td colspan="7">
							<xsl:choose>
								<xsl:when test="count(page/content/portfolios/portfolio/portfolioPolicyList/policy)&gt;0">
									<xsl:value-of select="xslNsODExt:getDictRes('NoAdditionalPoliciesFound')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsODExt:getDictRes('NoPoliciesFound_lowercasePeriod')"/>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:when>
				<xsl:otherwise>
					<xsl:for-each select="page/content/policyList/policy">
						<tr>
							<td>
								<a href="javascript:;">
									<xsl:attribute name="onclick">DCT.Submit.actOnQuote('load','<xsl:value-of select="@policyID"/>','<xsl:value-of select="@LOB"/>')</xsl:attribute>
									<xsl:value-of select="@LOB"/>
								</a>
							</td>
							<td>
								<xsl:value-of select="@description2"/>
								<xsl:if test="@description2=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<td>
								<xsl:value-of select="@status"/>
							</td>
							<td>
								<xsl:value-of select="@description"/>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', @effectiveDate)"/>
								<xsl:if test="@effectiveDate=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', @expirationDate)"/>
								<xsl:if test="@expirationDate=''">
									<xsl:text> </xsl:text>
								</xsl:if>
							</td>
							<xsl:call-template name="buildPolicyActions">
								<xsl:with-param name="policyID" select="@policyID"/>
								<xsl:with-param name="status" select="@status"/>
							</xsl:call-template>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
		<xsl:if test="not($IsReadOnly)">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">addPolicyA</xsl:with-param>
				<xsl:with-param name="id">addPolicyA</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.gotoPageForQuote('</xsl:text>
					<xsl:value-of select="$newQuoteContentPage"/>
					<xsl:text>','</xsl:text>
					<xsl:value-of select="clientID"/>
					<xsl:text>')</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('StartNewQuote')"/>
				</xsl:with-param>
				<xsl:with-param name="type">hyperlink</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!--*************************************************************************************************************
		AvantGarde Templates
		************************************************************************************************************* -->
	<!--*************************************************************************************************************
		AvantGarde Attachments Templates
		************************************************************************************************************* -->
	<xsl:template name="attachmentsWidget">
		<xsl:param name="screenName">policy</xsl:param>
		<xsl:variable name="attachmentModule" select="/page/content/attachments/@module"/>
		<xsl:if test="not(contains($attachmentModule, 'DuckCreek.Hyland.OnBase.ext')) and /page/content/attachments">
			<xsl:call-template name="buildAttachmentsList">
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildAttachmentsList">
		<h2>
			<xsl:value-of select="xslNsODExt:getDictRes('Attachments')"/>
		</h2>
		<xsl:call-template name="attachmentsWidgetGrid">
			<xsl:with-param name="id">
				<xsl:text>attachmentsListGrid</xsl:text>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="searchSettings">
		<div id="pagePart_searchConstraints" class="">
			<div class="searchByCaption">
				<label id="searchTypeTitle">
					<xsl:value-of select="xslNsODExt:getDictRes('SearchBy_colon')"/>
				</label>
			</div>
			<div class="searchByFieldBuilder">
				<!--//This changes the id of the field when the page refreshes in order to get around an Ajax bug-->
				<xsl:attribute name="id">
					<xsl:text>Searchbyfield</xsl:text>
					<xsl:value-of select="/page/state/@stateID"/>
				</xsl:attribute>
				<table id="searchFilterTable">
					<xsl:for-each select="/page/content/search/keys/key">
						<xsl:variable name="keyName" select="@name"/>
						<xsl:variable name="keyOp" select="@op"/>
						<xsl:variable name="keyValue" select="@value"/>
						<xsl:variable name="keyConnect" select="preceding-sibling::*/@connect"/>
						<xsl:variable name="currentRow" select="position()"/>
						<tr>
							<td class="connectorFieldCell">
								<xsl:if test="position() != 1">
									<xsl:call-template name="buildSystemControl">
										<xsl:with-param name="id">
											<xsl:text>_keyconnectorAdvSearch</xsl:text>
											<xsl:value-of select="position()"/>
										</xsl:with-param>
										<xsl:with-param name="name">_keyconnectorAdvSearch</xsl:with-param>
										<xsl:with-param name="type">select</xsl:with-param>
										<xsl:with-param name="optionlist">
											<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_SRCHFILTERCNT']/ListEntry">
												<option>
													<xsl:attribute name="value">
														<xsl:value-of select="@Code"/>
													</xsl:attribute>
													<xsl:value-of select="@Description"/>
												</option>
											</xsl:for-each>
										</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="$keyConnect"/>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:if>
							</td>
							<td>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">
										<xsl:text>_keynameAdvSearch</xsl:text>
										<xsl:value-of select="position()"/>
									</xsl:with-param>
									<xsl:with-param name="name">_keynameAdvSearch</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_SRCHCLTFILTER']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_SRCHPOLFILTER']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="controlClass">policySearchTypeComboField</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$keyName"/>
									</xsl:with-param>
								</xsl:call-template>
							</td>
							<td>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">
										<xsl:text>_keyopAdvSearch</xsl:text>
										<xsl:value-of select="position()"/>
									</xsl:with-param>
									<xsl:with-param name="name">_keyopAdvSearch</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='POL_SRCHFILTEROPT']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$keyOp"/>
									</xsl:with-param>
								</xsl:call-template>
							</td>
							<td class="searchTable">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">
										<xsl:text>_keyvalueTextSearch</xsl:text>
										<xsl:value-of select="position()"/>
									</xsl:with-param>
									<xsl:with-param name="name">_keyvalueAdvSearch</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$keyValue"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">
										<xsl:text>_keyvalueDateSearch</xsl:text>
										<xsl:value-of select="position()"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">policySearchDateField</xsl:with-param>
									<xsl:with-param name="name">_keyvalueAdvSearch</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$keyValue"/>
									</xsl:with-param>
									<xsl:with-param name="type">date</xsl:with-param>
									<xsl:with-param name="dateFormat">
										<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
									</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</td>
							<td>
								<xsl:choose>
									<xsl:when test="position() = last() and count(/page/content/search/keys/key) &lt;= 1">
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="buildSearchCriteriaActions">
											<xsl:with-param name="showDelete">1</xsl:with-param>
											<xsl:with-param name="showAdd">0</xsl:with-param>
											<xsl:with-param name="deleteTargetRow">
												<xsl:value-of select="$currentRow"/>
											</xsl:with-param>
											<xsl:with-param name="rowPostion">
												<xsl:value-of select="position()"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td>
								<xsl:if test="position() = last()">
									<xsl:call-template name="buildSearchCriteriaActions">
										<xsl:with-param name="showDelete">0</xsl:with-param>
										<xsl:with-param name="showAdd">1</xsl:with-param>
										<xsl:with-param name="rowPostion">
											<xsl:value-of select="position()"/>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:if>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</div>
			<script>Ext.onReady(function(){Ext.Function.defer(DCT.Util.disableCheck, 5);DCT.Util.setSearchField();});</script>
		</div>
	</xsl:template>
	<xsl:template name="buildSearchCriteriaActions">
		<xsl:param name="showDelete">1</xsl:param>
		<xsl:param name="showAdd">1</xsl:param>
		<xsl:param name="deleteTargetRow"></xsl:param>
		<xsl:param name="rowPostion"></xsl:param>
		<xsl:if test="$showDelete = '1'">
			<xsl:variable name="anchorId">
				<xsl:text>removeSearchFilter_</xsl:text>
				<xsl:value-of select="$rowPostion"/>
			</xsl:variable>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">removeSearchFilter</xsl:with-param>
				<xsl:with-param name="id" select="$anchorId"/>
				<xsl:with-param name="anchorId">
					<xsl:text>removeSearchAnchor_</xsl:text>
					<xsl:value-of select="$rowPostion"/>
				</xsl:with-param>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.removeSearchFilter(</xsl:text>
					<xsl:value-of select="$deleteTargetRow"/>
					<xsl:text>);</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="type">loneIcon</xsl:with-param>
				<xsl:with-param name="qtip">
					<xsl:value-of select="xslNsODExt:getDictRes('RemoveThisFilter')"/>
				</xsl:with-param>
				<xsl:with-param name="icon">decline.png</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$showAdd = '1'">
			<xsl:variable name="anchorId">
				<xsl:text>addSearchFilter_</xsl:text>
				<xsl:value-of select="$rowPostion"/>
			</xsl:variable>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">addSearchFilter</xsl:with-param>
				<xsl:with-param name="id" select="$anchorId"/>
				<xsl:with-param name="onclick">
					<xsl:text>DCT.Submit.addSearchFilter('searchFilterTable');</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="type">loneIcon</xsl:with-param>
				<xsl:with-param name="qtip">
					<xsl:value-of select="xslNsODExt:getDictRes('AddFilter')"/>
				</xsl:with-param>
				<xsl:with-param name="icon">add.png</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>