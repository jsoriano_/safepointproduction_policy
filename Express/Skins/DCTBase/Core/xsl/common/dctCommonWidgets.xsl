﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
	<xsl:import href="dctApplication.xsl"/>
	<xsl:template name="createTreeColumn">
		<xsl:param name="id">
			<xsl:text>treePanel</xsl:text>
		</xsl:param>
		<xsl:param name="targetPage"/>
		<xsl:param name="columns"/>
		<xsl:param name="treeTitle"/>
		<xsl:param name="rootNodeName">
			<xsl:text>Root</xsl:text>
		</xsl:param>
		<xsl:param name="model"/>
		<xsl:param name="rowTipFunc">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="rowClassFunc">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="class">
			<xsl:text>treeGridPanel</xsl:text>
		</xsl:param>
		<xsl:param name="toolBarClass">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="extraConfigItems"/>
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="$id"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">
					<xsl:value-of select="$class"/>
				</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$id"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$id"/>
					<xsl:text>",</xsl:text>
					<xsl:text>columns: </xsl:text>
					<xsl:value-of select="$columns"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctTitle: </xsl:text>
					<xsl:value-of select="$treeTitle"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctStoreClass: "DCT.TreeStore",</xsl:text>
					<xsl:text>dctStoreConfig: {</xsl:text>
					<xsl:text>defaultRootText: "</xsl:text>
					<xsl:value-of select="$rootNodeName"/>
					<xsl:text>",</xsl:text>
					<xsl:text>model: "</xsl:text>
					<xsl:value-of select="$model"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctTargetPage: "</xsl:text>
					<xsl:value-of select="$targetPage"/>
					<xsl:text>"</xsl:text>
					<xsl:text>},</xsl:text>
					<xsl:text>dctViewConfig: {</xsl:text>
					<xsl:text>debugModeOn: </xsl:text>
					<xsl:choose>
						<xsl:when test="$includeDebugInfo = 'true'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>debugId: "</xsl:text>
					<xsl:value-of select="normalize-space($id)"/>
					<xsl:text>"</xsl:text>
					<xsl:if test="$rowTipFunc != 'null'">
						<xsl:text>,</xsl:text>
						<xsl:text>getRowTips: </xsl:text>
						<xsl:value-of select="$rowTipFunc"/>
					</xsl:if>
					<xsl:if test="$rowClassFunc != 'null'">
						<xsl:text>,</xsl:text>
						<xsl:text>getRowClass: </xsl:text>
						<xsl:value-of select="$rowClassFunc"/>
					</xsl:if>
					<xsl:text>}</xsl:text>
					<xsl:if test="$toolBarClass != 'null'">
						<xsl:text>,</xsl:text>
						<xsl:text>dctTreeToolbarClass: "</xsl:text>
						<xsl:value-of select="$toolBarClass"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$extraConfigItems">
						<xsl:value-of select="$extraConfigItems"/>
					</xsl:if>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--*************************************************************************************************************
		Initializes all of the non Interview-specific Ext.OnReady events.
		************************************************************************************************************* -->
	<xsl:template name="buildNonInterviewOnReady">
	</xsl:template>
	<xsl:template name="buildEditGridPanel">
		<xsl:param name="targetPage"/>
		<xsl:param name="hidden"/>
		<xsl:param name="pageSize">
			<xsl:text>0</xsl:text>
		</xsl:param>
		<xsl:param name="startIndex">
			<xsl:text>0</xsl:text>
		</xsl:param>
		<xsl:param name="totalProperty"/>
		<xsl:param name="record"/>
		<xsl:param name="idPath"/>
		<xsl:param name="fields"/>
		<xsl:param name="colModel"/>
		<xsl:param name="height"/>
		<xsl:param name="width"/>
		<xsl:param name="loadData">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:param name="gridID"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="groupIndex"/>
		<div class="interviewGrid">
			<xsl:attribute name="id">
				<xsl:value-of select="$gridID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$gridID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewEditorGridPanel</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctColumnModel: </xsl:text>
					<xsl:value-of select="$colModel"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctStoreConfig: {</xsl:text>
					<xsl:text>remoteSort: </xsl:text>
					<xsl:choose>
						<xsl:when test="$pageSize &gt; 0">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>totalProperty: "</xsl:text>
					<xsl:value-of select="$totalProperty"/>
					<xsl:text>",</xsl:text>
					<xsl:text>record: "</xsl:text>
					<xsl:value-of select="$record"/>
					<xsl:text>",</xsl:text>
					<xsl:text>idProperty: "</xsl:text>
					<xsl:value-of select="$idPath"/>
					<xsl:text>",</xsl:text>
					<xsl:text>fields: </xsl:text>
					<xsl:value-of select="$fields"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctStart: </xsl:text>
					<xsl:value-of select="$startIndex"/>
					<xsl:text>,</xsl:text>
					<xsl:text>pageSize: </xsl:text>
					<xsl:value-of select="$pageSize"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctTargetPage: "</xsl:text>
					<xsl:value-of select="$targetPage"/>
					<xsl:text>"</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>dctGridId: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctGroupIndex: "</xsl:text>
					<xsl:value-of select="$groupIndex"/>
					<xsl:text>"</xsl:text>
					<xsl:text>},</xsl:text>
					<xsl:text>dctSelectionConfig: {</xsl:text>
					<xsl:text>selType: 'rowmodel',</xsl:text>
					<xsl:text>mode: 'SINGLE'</xsl:text>
					<xsl:text>},</xsl:text>
					<xsl:text>dctViewConfig: {</xsl:text>
					<xsl:text>debugModeOn: </xsl:text>
					<xsl:choose>
						<xsl:when test="$includeDebugInfo = 'true'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>debugId: "</xsl:text>
					<xsl:value-of select="normalize-space($gridID)"/>
					<xsl:text>"</xsl:text>
					<xsl:text>}</xsl:text>
					<xsl:if test="$pageSize &gt; 0">
						<xsl:text>,</xsl:text>
						<xsl:text>dctPagingBarConfig: {</xsl:text>
						<xsl:text>pageSize: </xsl:text>
						<xsl:value-of select="$pageSize"/>
						<xsl:text>}</xsl:text>
					</xsl:if>
					<xsl:if test="$hidden = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>hidden: true</xsl:text>
					</xsl:if>
					<xsl:if test="normalize-space($width)!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>width: </xsl:text>
						<xsl:value-of select="$width"/>
					</xsl:if>
					<xsl:if test="normalize-space($height)!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>height: </xsl:text>
						<xsl:value-of select="$height"/>
					</xsl:if>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<!--*************************************************************************************************************
	Generate Array Grid Panel:
		@gridID the target div, it is created and used within this template.
		@gridData an array of the data used to generate the grid.
		@storeFields an array of field definition objects, or field name strings, used to create the data store.
		@columns an array of columns to auto create a ColumnModel.
		@showToolBar is a boolean, denoting display or not display the toolbar at the top of the grid.
		@showGridCheckbox is a boolean, denoting display a checkbox column or not.
		@gridView is a Ext.grid.GridView object used to customize the row css based on row data.
				i.e. new Ext.grid.GridView({getRowClass:function (row, index) {return (row.data.read == 1 ? 'read-row' : 'unread-row');}})		

			Note When Using Checkbox Grids:
				To access the selected columns (mainly when using showGridCheckbox (aka CheckboxSelectionModel), use the 
				gridID to retrieve the grid and then to access the datastore by calling getStore().  
				There is a property attached to the dataStore called SelectionModel (i.e. dataStore.SelectionModel).
				This property is the selection model used when creating the grid.  It can be used to find the seleted item
				or the selections.  There is an additional support function called getSelectedItemID.  
				This returns the id for the single-selected grid row.				
	************************************************************************************************************* -->
	<xsl:template name="buildNonPagingGridPanel">
		<xsl:param name="gridID">
			<xsl:text>extJsGrid</xsl:text>
		</xsl:param>
		<xsl:param name="gridData"/>
		<xsl:param name="storeFields"/>
		<xsl:param name="columns"/>
		<xsl:param name="showToolBar">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:param name="showGridCheckbox">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="selectAllItems"/>
		<xsl:param name="gridView">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="multipleSelect">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="deSelectEventFunc"/>
		<xsl:param name="selectEventFunc"/>
		<xsl:param name="beforeRowSelectEventFunc"/>
		<xsl:param name="checkboxRenderFunc"/>
		<xsl:param name="groupingView">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="sortInfo">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="groupField">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="toolBarTitle">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="rowClassRenderer">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="previewRow">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="previewRowRenderer"/>
		<xsl:param name="initialShowPreview">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:param name="includeDebugColumnIds" select="$includeDebugInfo"/>
		<xsl:param name="targetPage"/>
		<xsl:param name="dataPath"/>
		<xsl:param name="recordId"/>
		<xsl:param name="className">
			<xsl:text>nonPagingGridPanel</xsl:text>
		</xsl:param>
		<xsl:param name="previewPlugInId">
			<xsl:text>preview</xsl:text>
		</xsl:param>
		<xsl:param name="adjustedCountFunc"/>
		<xsl:param name="setRowSelectedFunc"/>
		<div class="arrayGrid">
			<xsl:attribute name="id">
				<xsl:value-of select="$gridID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$gridID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">
					<xsl:value-of select="$className"/>
				</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctColumnModel: </xsl:text>
					<xsl:value-of select="$columns"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctGroupView: </xsl:text>
					<xsl:value-of select="$groupingView"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctPreviewGrid: </xsl:text>
					<xsl:value-of select="$previewRow"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctInitialPreview: </xsl:text>
					<xsl:value-of select="$initialShowPreview"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctShowToolBar: </xsl:text>
					<xsl:value-of select="$showToolBar"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctCheckBoxSelection: </xsl:text>
					<xsl:value-of select="$showGridCheckbox"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctTitle: </xsl:text>
					<xsl:value-of select="$toolBarTitle"/>
					<xsl:if test="$previewRow='true'">
						<xsl:text>,</xsl:text>
						<xsl:text>dctPreviewConfig: {</xsl:text>
						<xsl:text>ptype: 'preview',</xsl:text>
						<xsl:text>expanded: </xsl:text>
						<xsl:value-of select="$initialShowPreview"/>
						<xsl:text>,</xsl:text>
						<xsl:text>pluginId: '</xsl:text>
						<xsl:value-of select="$previewPlugInId"/>
						<xsl:text>',</xsl:text>
						<xsl:text>getAdditionalData: </xsl:text>
						<xsl:value-of select="$previewRowRenderer"/>
						<xsl:text>}</xsl:text>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:choose>
						<xsl:when test="$groupingView='true'">
							<xsl:text>dctStoreClass: </xsl:text>
							<xsl:choose>
								<xsl:when test="normalize-space($dataPath)!=''">
									<xsl:text>'DCT.NonPagingStore',</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'DCT.ArrayStore',</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>dctStoreConfig: {</xsl:text>
							<xsl:text>fields: </xsl:text>
							<xsl:value-of select="$storeFields"/>
							<xsl:text>,</xsl:text>
							<xsl:if test="normalize-space($adjustedCountFunc)!=''">
								<xsl:text>dctGetAdjustedCountMethod: </xsl:text>
								<xsl:value-of select="$adjustedCountFunc"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
							<xsl:text>groupField: </xsl:text>
							<xsl:value-of select="$groupField"/>
							<xsl:text>,</xsl:text>
							<xsl:text>sorters: </xsl:text>
							<xsl:value-of select="$sortInfo"/>
							<xsl:choose>
								<xsl:when test="normalize-space($dataPath)!=''">
									<xsl:text>,</xsl:text>
									<xsl:text>record: "</xsl:text>
									<xsl:value-of select="$dataPath"/>
									<xsl:text>",</xsl:text>
									<xsl:text>idProperty: "</xsl:text>
									<xsl:value-of select="$recordId"/>
									<xsl:text>",</xsl:text>
									<xsl:text>dctTargetPage: "</xsl:text>
									<xsl:value-of select="$targetPage"/>
									<xsl:text>"</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text>
									<xsl:text>data: </xsl:text>
									<xsl:value-of select="$gridData"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>}</xsl:text>
							<xsl:text>,</xsl:text>
							<xsl:text>dctViewConfig: {</xsl:text>
							<xsl:text>debugModeOn: </xsl:text>
							<xsl:choose>
								<xsl:when test="$includeDebugInfo = 'true'">
									<xsl:text>true</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>false</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>,</xsl:text>
							<xsl:text>debugId: "</xsl:text>
							<xsl:value-of select="normalize-space($gridID)"/>
							<xsl:text>"</xsl:text>
							<xsl:if test="$rowClassRenderer != 'null'">
								<xsl:text>,</xsl:text>
								<xsl:text>getRowClass: </xsl:text>
								<xsl:value-of select="$rowClassRenderer"/>
							</xsl:if>
							<xsl:text>},</xsl:text>
							<xsl:text>dctGroupingConfig: {</xsl:text>
							<xsl:text>ftype: 'grouping'</xsl:text>
							<xsl:text>}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>dctStoreClass: </xsl:text>
							<xsl:choose>
								<xsl:when test="normalize-space($dataPath)!=''">
									<xsl:text>'DCT.NonPagingStore',</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'DCT.ArrayStore',</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>dctStoreConfig: {</xsl:text>
							<xsl:text>fields: </xsl:text>
							<xsl:value-of select="$storeFields"/>
							<xsl:if test="normalize-space($adjustedCountFunc)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>dctGetAdjustedCountMethod: </xsl:text>
								<xsl:value-of select="$adjustedCountFunc"/>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="normalize-space($dataPath)!=''">
									<xsl:text>,</xsl:text>
									<xsl:text>record: "</xsl:text>
									<xsl:value-of select="$dataPath"/>
									<xsl:text>",</xsl:text>
									<xsl:text>idProperty: "</xsl:text>
									<xsl:value-of select="$recordId"/>
									<xsl:text>",</xsl:text>
									<xsl:text>dctTargetPage: "</xsl:text>
									<xsl:value-of select="$targetPage"/>
									<xsl:text>"</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text>
									<xsl:text>data: </xsl:text>
									<xsl:value-of select="$gridData"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>},</xsl:text>
							<xsl:text>dctViewConfig: {</xsl:text>
							<xsl:text>debugModeOn: </xsl:text>
							<xsl:choose>
								<xsl:when test="$includeDebugInfo = 'true'">
									<xsl:text>true</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>false</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>,</xsl:text>
							<xsl:text>debugId: "</xsl:text>
							<xsl:value-of select="normalize-space($gridID)"/>
							<xsl:text>"</xsl:text>
							<xsl:if test="$rowClassRenderer != 'null' and $previewRow='false'">
								<xsl:text>,</xsl:text>
								<xsl:text>getRowClass: </xsl:text>
								<xsl:value-of select="$rowClassRenderer"/>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:choose>
						<xsl:when test="$showGridCheckbox='true'">
							<xsl:text>dctSelectionConfig: {</xsl:text>
							<xsl:text>selType: 'dctcheckboxmodel',</xsl:text>
							<xsl:text>checkOnly: true,</xsl:text>
							<xsl:choose>
								<xsl:when test="$multipleSelect='false'">
									<xsl:text>allowDeselect: true,</xsl:text>
								</xsl:when>
							</xsl:choose>							
							<xsl:text>mode: </xsl:text>
							<xsl:choose>
								<xsl:when test="$multipleSelect='false'">
									<xsl:text>'SINGLE'</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'MULTI'</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="$selectAllItems='true'">
								<xsl:text>,</xsl:text>
								<xsl:text>selectAllItems: true</xsl:text>
							</xsl:if>
							<xsl:if test="normalize-space($checkboxRenderFunc)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>renderer: </xsl:text>
								<xsl:value-of select="$checkboxRenderFunc"/>
							</xsl:if>
							<xsl:if test="normalize-space($setRowSelectedFunc)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>dctIsSelected: </xsl:text>
								<xsl:value-of select="$setRowSelectedFunc"/>
							</xsl:if>
							<xsl:if test="normalize-space($deSelectEventFunc)!= '' or normalize-space($selectEventFunc)!= '' or normalize-space($beforeRowSelectEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>listeners: {</xsl:text>
								<xsl:if test="normalize-space($deSelectEventFunc)!=''">
									<xsl:text>deselect: </xsl:text>
									<xsl:value-of select="$deSelectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($selectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>select: </xsl:text>
									<xsl:value-of select="$selectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($beforeRowSelectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!='' or normalize-space($selectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>beforeselect: </xsl:text>
									<xsl:value-of select="$beforeRowSelectEventFunc"/>
								</xsl:if>
								<xsl:text>}</xsl:text>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>dctSelectionConfig: {</xsl:text>
							<xsl:text>selType: 'rowmodel',</xsl:text>
							<xsl:text>mode: </xsl:text>
							<xsl:choose>
								<xsl:when test="$multipleSelect='false'">
									<xsl:text>'SINGLE'</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'MULTI'</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="normalize-space($deSelectEventFunc)!= '' or normalize-space($selectEventFunc)!= '' or normalize-space($beforeRowSelectEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>listeners: {</xsl:text>
								<xsl:if test="normalize-space($deSelectEventFunc)!=''">
									<xsl:text>deselect: </xsl:text>
									<xsl:value-of select="$deSelectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($selectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>select: </xsl:text>
									<xsl:value-of select="$selectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($beforeRowSelectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!='' or normalize-space($selectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>beforeselect: </xsl:text>
									<xsl:value-of select="$beforeRowSelectEventFunc"/>
								</xsl:if>
								<xsl:text>}</xsl:text>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<!--*************************************************************************************************************
	Generate Paging Grid
		@targetPage the target express content page.
		@pageSize an interger value of the maximum page sizes.
		@recordCount is the xml path to number of total records returned in the request.
		@recordPath the xpath to the repeated element which contains record information.
		@recordID the xpath relative from the record element to the element that contains a record identifier value.
		@columns an array containing the column mapping i.e. {name: 'name', type: 'string', mapping: '@name'}.
		@columnModel the extjs column model array definition.
		@returnDataStoreName the global javascript varable name of the data store.  Can be used in function to sort, page, etc.
		@emptyMessage the message to display whenever no data is returned.
		@dataOnLoad, boolean, does the data display on the initial load.
		@showToolBar is a boolean, denoting display or not display the toolbar at the top of the grid.
		@showGridCheckbox is a boolean, denoting display a checkbox column or not.
		@gridView is a Ext.grid.GridView object used to customize the row css based on row data.
				i.e. new Ext.grid.GridView({getRowClass:function (row, index) {return (row.data.read == 1 ? 'read-row' : 'unread-row');}})
			
			Note When Using Checkbox Grids:
				To access the selected columns (mainly when using showGridCheckbox (aka CheckboxSelectionModel), use the 
				dataStore variable returned from the buildExtJsGrid function.  There is a property attached to the 
				dataStore called SelectionModel (i.e. dataStore.SelectionModel).
				This property is the selection model used when creating the grid.  It can be used to find the seleted item
				or the selections.  There is an additional support function called getSelectedItemID.  
				This returns the id for the single-selected grid row.
	************************************************************************************************************* -->
	<xsl:template name="buildPagingGridPanel">
		<xsl:param name="targetPage"/>
		<xsl:param name="pageSize">
			<xsl:text>20</xsl:text>
		</xsl:param>
		<xsl:param name="recordCount"/>
		<xsl:param name="recordPath"/>
		<xsl:param name="recordID"/>
		<xsl:param name="columns">
			<xsl:text>[]</xsl:text>
		</xsl:param>
		<xsl:param name="columnModel">
			<xsl:text>[]</xsl:text>
		</xsl:param>
		<xsl:param name="dataOnLoad"/>
		<xsl:param name="filterSubSet">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="columnFiltering"/>
		<xsl:param name="filterArray"/>
		<xsl:param name="dataLoadEventFunc"/>
		<xsl:param name="width"/>
		<xsl:param name="sortColumn"/>
		<xsl:param name="sortOrder"/>
		<xsl:param name="startHidden"/>
		<xsl:param name="pagingPluginType"/>
		<xsl:param name="gridID">
			<xsl:text>extJsGrid</xsl:text>
		</xsl:param>
		<xsl:param name="gridData"/>
		<xsl:param name="showToolBar">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:param name="showGridCheckbox">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="showGridCheckboxwithButtons">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="selectAllItems"/>
		<xsl:param name="trackPageSelections"/>
		<xsl:param name="showItemsPerPageCombo">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="gridView">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="multipleSelect">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="deSelectEventFunc"/>
		<xsl:param name="selectEventFunc"/>
		<xsl:param name="beforeRowSelectEventFunc"/>
		<xsl:param name="checkboxRenderFunc"/>
		<xsl:param name="groupingView">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="sortInfo">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="groupField">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="toolBarTitle">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="rowClassRenderer">
			<xsl:text>null</xsl:text>
		</xsl:param>
		<xsl:param name="highlightRowRenderer">
			<xsl:text>DCT.Grid.highlightRowRenderer</xsl:text>
		</xsl:param>
		<xsl:param name="previewRow">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="previewRowRenderer"/>
		<xsl:param name="initialShowPreview">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:param name="includeDebugColumnIds" select="$includeDebugInfo"/>
		<xsl:param name="className"/>
		<xsl:param name="classMethod">
			<xsl:text>''</xsl:text>
		</xsl:param>
		<xsl:param name="showPreviewToolTip">
			<xsl:value-of select="xslNsODExt:getDictRes('ShowPreview')"/>
		</xsl:param>
		<xsl:param name="checkCurrentStartIndex">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="checkClickedRowIndex">
			<xsl:text>false</xsl:text>
		</xsl:param>
		<xsl:param name="previewPlugInId">
			<xsl:text>preview</xsl:text>
		</xsl:param>
		<xsl:param name="adjustedCountFunc"/>
		<xsl:param name="setRowSelectedFunc"/>
		<xsl:param name="storeClass">
			<xsl:text>'DCT.PagingStore'</xsl:text>
		</xsl:param>
		<div class="pagingGrid">
			<xsl:attribute name="id">
				<xsl:value-of select="$gridID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$gridID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="normalize-space($className)!=''">
							<xsl:value-of select="$className"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>pagingGridPanel</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctColumnModel: </xsl:text>
					<xsl:value-of select="$columnModel"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctGroupView: </xsl:text>
					<xsl:value-of select="$groupingView"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctPreviewGrid: </xsl:text>
					<xsl:value-of select="$previewRow"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctInitialPreview: </xsl:text>
					<xsl:value-of select="$initialShowPreview"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctShowToolBar: </xsl:text>
					<xsl:value-of select="$showToolBar"/>
					<xsl:text>,</xsl:text>
					<xsl:choose>
						<xsl:when test="page/state/clickedRowIndex &gt; -1 and $checkClickedRowIndex = 'true'">
							<xsl:text>dctClickedRowIndex:  </xsl:text>
							<xsl:value-of select="/page/state/clickedRowIndex"/>
							<xsl:text>,</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>dctClickedRowIndex: -1,</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>dctCheckBoxSelection: </xsl:text>
					<xsl:value-of select="$showGridCheckbox"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctGridCheckboxwithButtons: </xsl:text>
					<xsl:value-of select="$showGridCheckboxwithButtons"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctTitle: </xsl:text>
					<xsl:value-of select="$toolBarTitle"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctClassMethod: </xsl:text>
					<xsl:value-of select="$classMethod"/>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctShowPreviewToolTip</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$showPreviewToolTip"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:if test="$previewRow='true'">
						<xsl:text>,</xsl:text>
						<xsl:text>dctPreviewConfig: {</xsl:text>
						<xsl:text>ptype: 'preview',</xsl:text>
						<xsl:text>expanded: </xsl:text>
						<xsl:value-of select="$initialShowPreview"/>
						<xsl:text>,</xsl:text>
						<xsl:text>pluginId: '</xsl:text>
						<xsl:value-of select="$previewPlugInId"/>
						<xsl:text>',</xsl:text>
						<xsl:text>getAdditionalData: </xsl:text>
						<xsl:value-of select="$previewRowRenderer"/>
						<xsl:text>}</xsl:text>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:text>dctViewConfig: {</xsl:text>
					<xsl:text>debugModeOn: </xsl:text>
					<xsl:choose>
						<xsl:when test="$includeDebugInfo = 'true'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>debugId: "</xsl:text>
					<xsl:value-of select="normalize-space($gridID)"/>
					<xsl:text>"</xsl:text>
					<xsl:choose>
						<xsl:when test="$rowClassRenderer != 'null' and $previewRow='false' and $checkClickedRowIndex='false'">
							<xsl:text>,</xsl:text>
							<xsl:text>getRowClass: </xsl:text>
							<xsl:value-of select="$rowClassRenderer"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$highlightRowRenderer!= 'null' and $checkClickedRowIndex='true' and $previewRow='false'">
								<xsl:text>,</xsl:text>
								<xsl:text>getRowClass: </xsl:text>
								<xsl:value-of select="$highlightRowRenderer"/>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>},</xsl:text>
					<xsl:choose>
						<xsl:when test="$groupingView='true'">
							<xsl:text>dctStoreClass: </xsl:text>
							<xsl:value-of select="$storeClass"/>
							<xsl:text>,</xsl:text>
							<xsl:text>dctStoreConfig: {</xsl:text>
							<xsl:text>fields: </xsl:text>
							<xsl:value-of select="$columns"/>
							<xsl:text>,</xsl:text>
							<xsl:if test="normalize-space($adjustedCountFunc)!=''">
								<xsl:text>dctGetAdjustedCountMethod: </xsl:text>
								<xsl:value-of select="$adjustedCountFunc"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
							<xsl:text>groupField: </xsl:text>
							<xsl:value-of select="$groupField"/>
							<xsl:text>,</xsl:text>
							<xsl:text>sorters: </xsl:text>
							<xsl:value-of select="$sortInfo"/>
							<xsl:text>,</xsl:text>
							<xsl:text>record: "</xsl:text>
							<xsl:value-of select="$recordPath"/>
							<xsl:text>",</xsl:text>
							<xsl:text>idProperty: "</xsl:text>
							<xsl:value-of select="$recordID"/>
							<xsl:text>",</xsl:text>
							<xsl:text>totalProperty: "</xsl:text>
							<xsl:value-of select="$recordCount"/>
							<xsl:text>",</xsl:text>
							<xsl:text>dctTargetPage: "</xsl:text>
							<xsl:value-of select="$targetPage"/>
							<xsl:text>",</xsl:text>
							<xsl:text>dctStart: 0,</xsl:text>
							<xsl:text>pageSize: </xsl:text>
							<xsl:value-of select="$pageSize"/>
							<xsl:text>,</xsl:text>
							<xsl:text>dctFilterSubSet: </xsl:text>
							<xsl:value-of select="$filterSubSet"/>
							<xsl:choose>
								<xsl:when test="$dataOnLoad='false'">
									<xsl:text>,</xsl:text>
									<xsl:text>autoLoad: false</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text>
									<xsl:text>autoLoad: {</xsl:text>
									<xsl:text>params:{start: 0</xsl:text>
									<xsl:text>,</xsl:text>
									<xsl:text>limit: </xsl:text>
									<xsl:value-of select="$pageSize"/>
									<xsl:if test="normalize-space($sortColumn)!=''">
										<xsl:text>,</xsl:text>
										<xsl:text>sort: </xsl:text>
										<xsl:value-of select="$sortColumn"/>
										<xsl:text>,</xsl:text>
										<xsl:text>dir: </xsl:text>
										<xsl:value-of select="$sortOrder"/>
									</xsl:if>
									<xsl:text>}</xsl:text>
									<xsl:text>}</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="normalize-space($dataLoadEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>dctLoadMethod: </xsl:text>
								<xsl:value-of select="$dataLoadEventFunc"/>
							</xsl:if>
							<xsl:text>},</xsl:text>
							<xsl:text>dctGroupingConfig: {</xsl:text>
							<xsl:text>ftype: 'grouping'</xsl:text>
							<xsl:text>}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>dctStoreClass: </xsl:text>
							<xsl:value-of select="$storeClass"/>
							<xsl:text>,</xsl:text>
							<xsl:text>dctStoreConfig: {</xsl:text>
							<xsl:text>fields: </xsl:text>
							<xsl:value-of select="$columns"/>
							<xsl:text>,</xsl:text>
							<xsl:if test="normalize-space($adjustedCountFunc)!=''">
								<xsl:text>dctGetAdjustedCountMethod: </xsl:text>
								<xsl:value-of select="$adjustedCountFunc"/>
								<xsl:text>,</xsl:text>
							</xsl:if>
							<xsl:text>idProperty: "</xsl:text>
							<xsl:value-of select="$recordID"/>
							<xsl:text>",</xsl:text>
							<xsl:text>record: "</xsl:text>
							<xsl:value-of select="$recordPath"/>
							<xsl:text>",</xsl:text>
							<xsl:text>totalProperty: "</xsl:text>
							<xsl:value-of select="$recordCount"/>
							<xsl:text>",</xsl:text>
							<xsl:text>dctTargetPage: "</xsl:text>
							<xsl:value-of select="$targetPage"/>
							<xsl:text>",</xsl:text>
							<xsl:choose>
								<xsl:when test="page/state/currentStartIndex &gt; -1 and $checkCurrentStartIndex = 'true'">
									<xsl:text>dctStart: </xsl:text>
									<xsl:value-of select="page/state/currentStartIndex"/>
									<xsl:text>,</xsl:text>
									<xsl:text>pageSize: </xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>dctStart: 0, pageSize: </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="$pageSize"/>
							<xsl:text>,</xsl:text>
							<xsl:if test="normalize-space($sortColumn)!=''">
								<xsl:text>sorters: [{</xsl:text>
								<xsl:text>property: "</xsl:text>
								<xsl:value-of select="$sortColumn"/>
								<xsl:text>",</xsl:text>
								<xsl:text>direction: "</xsl:text>
								<xsl:value-of select="$sortOrder"/>
								<xsl:text>"</xsl:text>
								<xsl:text>}],</xsl:text>
							</xsl:if>
							<xsl:text>dctFilterSubSet: </xsl:text>
							<xsl:value-of select="$filterSubSet"/>
							<xsl:choose>
								<xsl:when test="$dataOnLoad='false'">
									<xsl:text>,</xsl:text>
									<xsl:text>autoLoad: false</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text>
									<xsl:text>autoLoad: {</xsl:text>
									<xsl:choose>
										<xsl:when test="page/state/currentStartIndex &gt; -1 and $checkCurrentStartIndex = 'true'">
											<xsl:text>params:{start: </xsl:text>
											<xsl:value-of select="page/state/currentStartIndex"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>params:{start: 0</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:text>,</xsl:text>
									<xsl:text>limit: </xsl:text>
									<xsl:value-of select="$pageSize"/>
									<xsl:if test="normalize-space($sortColumn)!=''">
										<xsl:text>,</xsl:text>
										<xsl:text>sort: "</xsl:text>
										<xsl:value-of select="$sortColumn"/>
										<xsl:text>",</xsl:text>
										<xsl:text>dir: "</xsl:text>
										<xsl:value-of select="$sortOrder"/>
										<xsl:text>"</xsl:text>
									</xsl:if>
									<xsl:text>}</xsl:text>
									<xsl:text>}</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="normalize-space($dataLoadEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>dctLoadMethod: </xsl:text>
								<xsl:value-of select="$dataLoadEventFunc"/>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:choose>
						<xsl:when test="$showGridCheckbox='true'">
							<xsl:text>dctSelectionConfig: {</xsl:text>
							<xsl:text>selType: 'dctcheckboxmodel',</xsl:text>
							<xsl:text>checkOnly: true,</xsl:text>
							<xsl:text>mode: </xsl:text>
							<xsl:choose>
								<xsl:when test="$multipleSelect='false'">
									<xsl:text>'SINGLE'</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'MULTI'</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="$selectAllItems='true'">
								<xsl:text>,</xsl:text>
								<xsl:text>selectAllItems: true</xsl:text>
							</xsl:if>
							<xsl:if test="$trackPageSelections='false'">
								<xsl:text>,</xsl:text>
								<xsl:text>trackPageSelections: false</xsl:text>
							</xsl:if>
							<xsl:if test="normalize-space($checkboxRenderFunc)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>renderer: </xsl:text>
								<xsl:value-of select="$checkboxRenderFunc"/>
							</xsl:if>
							<xsl:if test="normalize-space($setRowSelectedFunc)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>dctIsSelected: </xsl:text>
								<xsl:value-of select="$setRowSelectedFunc"/>
							</xsl:if>
							<xsl:if test="normalize-space($deSelectEventFunc)!= '' or normalize-space($selectEventFunc)!= '' or normalize-space($beforeRowSelectEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>listeners: {</xsl:text>
								<xsl:if test="normalize-space($deSelectEventFunc)!=''">
									<xsl:text>deselect: </xsl:text>
									<xsl:value-of select="$deSelectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($selectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>select: </xsl:text>
									<xsl:value-of select="$selectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($beforeRowSelectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!='' or normalize-space($selectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>beforeselect: </xsl:text>
									<xsl:value-of select="$beforeRowSelectEventFunc"/>
								</xsl:if>
								<xsl:text>}</xsl:text>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>dctSelectionConfig: {</xsl:text>
							<xsl:text>selType: 'rowmodel',</xsl:text>
							<xsl:text>mode: </xsl:text>
							<xsl:choose>
								<xsl:when test="$multipleSelect='false'">
									<xsl:text>'SIMPLE'</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>'MULTI'</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="normalize-space($deSelectEventFunc)!= '' or normalize-space($selectEventFunc)!= '' or normalize-space($beforeRowSelectEventFunc)!= ''">
								<xsl:text>,</xsl:text>
								<xsl:text>listeners: {</xsl:text>
								<xsl:if test="normalize-space($deSelectEventFunc)!=''">
									<xsl:text>deselect: </xsl:text>
									<xsl:value-of select="$deSelectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($selectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>select: </xsl:text>
									<xsl:value-of select="$selectEventFunc"/>
								</xsl:if>
								<xsl:if test="normalize-space($beforeRowSelectEventFunc)!=''">
									<xsl:if test="normalize-space($deSelectEventFunc)!='' or normalize-space($selectEventFunc)!=''">
										<xsl:text>,</xsl:text>
									</xsl:if>
									<xsl:text>beforeselect: </xsl:text>
									<xsl:value-of select="$beforeRowSelectEventFunc"/>
								</xsl:if>
								<xsl:text>}</xsl:text>
							</xsl:if>
							<xsl:text>}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>dctPagingBarConfig: {</xsl:text>
					<xsl:text>dctPagingPluginType: "</xsl:text>
					<xsl:value-of select="$pagingPluginType"/>
					<xsl:text>"</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>pageSize: </xsl:text>
					<xsl:value-of select="$pageSize"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctGridId: "</xsl:text>
					<xsl:value-of select="$gridID"/>
					<xsl:text>"</xsl:text>
					<xsl:if test="$showItemsPerPageCombo='true'">
						<xsl:text>,</xsl:text>
						<xsl:text>showItemsPerPageCombo: true</xsl:text>
					</xsl:if>
					<xsl:text>}</xsl:text>
					<xsl:if test="$startHidden='true'">
						<xsl:text>,</xsl:text>
						<xsl:text>hidden: true</xsl:text>
					</xsl:if>
					<xsl:if test="normalize-space($width)!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>width: </xsl:text>
						<xsl:value-of select="$width"/>
					</xsl:if>
					<xsl:if test="$columnFiltering='true'">
						<xsl:text>,</xsl:text>
						<xsl:text>dctFilteringConfig: {</xsl:text>
						<xsl:text>ptype: 'gridfilters'</xsl:text>
						<xsl:text>}</xsl:text>
					</xsl:if>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>

	<xsl:template name="addConfigProperty">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="type"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:text>:</xsl:text>
		<xsl:choose>
			<xsl:when test="$type='string'">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode($value, '0')"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:when test="$type='number' or $type='boolean' or $type='object' or $type='array' or $type='regExp'">
				<xsl:value-of select="$value"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
  <!--Notification Widget Template -->
  <xsl:template name="notificationWidget">
    <xsl:if test="/page/state/ResponseInfo/NotificationResponse/NotificationRecord">
      <div class="notificationDiv">
        <xsl:attribute name="class">
          <xsl:text>notificationDiv</xsl:text>
          <xsl:if test="/page/state/ResponseInfo/NotificationResponse/NotificationRecord/@type">
            <xsl:text> n</xsl:text>
            <xsl:value-of select="/page/state/ResponseInfo/NotificationResponse/NotificationRecord/@type"/>
          </xsl:if>
        </xsl:attribute>
        <xsl:for-each select="/page/state/ResponseInfo/NotificationResponse/NotificationRecord">
          <xsl:variable name="notificationRecord">
            <xsl:copy-of select="."/>
          </xsl:variable>
          <xsl:copy-of select="xslNsExt:getNotificationInfo($notificationRecord)"/>
        </xsl:for-each>
      </div>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>