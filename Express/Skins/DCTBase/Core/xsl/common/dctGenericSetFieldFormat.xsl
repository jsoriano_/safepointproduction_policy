﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">

  <!--
		helper template to format text for output;
		call this template rather than replace-linebreaks
		because we can scale this in the future to handle
		other special formatting if necessary
		~~~~~ params ~~~~~
		[text]: the string to format
		-->
  <xsl:template name="formatText">
    <xsl:param name="text" select="."/>
    <xsl:value-of select="$text"/>
  </xsl:template>
  <!-- 
		replaces code for \n in given text with HTML BReaks
		if passed a \r\n, the \r is left in place and only the
		\n is replaced, however the \r is ignored in HTML
		-->
  <xsl:template name="replace-linebreaks">
    <xsl:param name="text" select="."/>
    <xsl:choose>
      <xsl:when test="contains($text, '&#xA;')">
        <xsl:value-of select="substring-before($text, '&#xA;')"/>
        <xsl:text disable-output-escaping="yes">&lt;br/&gt;</xsl:text>
        <xsl:call-template name="replace-linebreaks">
          <xsl:with-param name="text" select="substring-after($text, '&#xA;')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="contains($text, '&#xD;')">
        <xsl:value-of select="substring-before($text, '&#xD;')"/>
        <xsl:text></xsl:text>
        <xsl:call-template name="replace-linebreaks">
          <xsl:with-param name="text" select="substring-after($text, '&#xD;')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="FormatValue">
    <xsl:param name="value"/>
    <xsl:param name="type"/>
    <xsl:param name="formatMask"/>
    <xsl:param name="hideOn">
      <xsl:text>never</xsl:text>
    </xsl:param>
    <xsl:variable name="manuscriptBaseCulture">
      <xsl:choose>
        <xsl:when test="/page/content/getPage/properties/@cultureCode">
          <xsl:value-of select="/page/content/getPage/properties/@cultureCode"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@cultureCode"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="currencyCode">
     <xsl:value-of select="/page/state/currencyCode"/>
    </xsl:variable>
    <xsl:variable name="fieldMask">
      <xsl:choose>
        <xsl:when test="$formatMask!=''">
          <xsl:value-of select="$formatMask"/>
        </xsl:when>
        <xsl:when test="@formatMask">
          <xsl:choose>
            <xsl:when test="(substring(@formatMask,1,4)='hide') and (@type='int')">-?,???,???,??#</xsl:when>
            <xsl:when test="(substring(@formatMask,1,4)='hide') and (@type='float')">-??,???,???,??#.????</xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="getFormatMaskHide"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="@type='int'">-?,???,???,??#</xsl:when>
            <xsl:when test="@type='float'">-??,???,???,??#.????</xsl:when>
            <xsl:otherwise/>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="dateValue">
      <xsl:if test="@type='date'">
        <xsl:value-of select="xslNsExt:cultureAwareDateFormatter($fieldMask, @readOnly, $value)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="typeValue">
      <xsl:choose>
        <xsl:when test="$type!=''">
          <xsl:value-of select="$type"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@type"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="@readOnly != '0'">

        <xsl:choose>
          <xsl:when test="($typeValue='int' or $typeValue='float') and $value!=''">
            <xsl:variable name="cultureAwareFormattedValue">
            <xsl:choose>
              <xsl:when test="string-length($fieldMask) = '2' and (starts-with($fieldMask, 'C') or starts-with($fieldMask, 'c'))">
                <xsl:value-of select="xslNsExt:currencyFormatter($currencyCode, $fieldMask, $value)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($manuscriptBaseCulture, $fieldMask, $value)"/>
              </xsl:otherwise>
            </xsl:choose>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="$hideOn='display' or $hideOn='always'">
                <xsl:value-of select="xslNsExt:maskValue($cultureAwareFormattedValue)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$cultureAwareFormattedValue"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="@type='date' and $value != ''">
            <xsl:value-of select="$dateValue"/>
          </xsl:when>
          <xsl:when test="$typeValue='datetime' and $value != ''">
            <xsl:variable name="dateTimeValue">
              <xsl:choose>
                <xsl:when test="contains($value,'-')">
                  <xsl:value-of select="translate(substring($value, 1, 19), 'T', ' ')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="xslNsExt:convertToISOFormat($value, 's')"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="xslNsExt:convertISOtoDisplay($dateTimeValue, 'g')"/>
          </xsl:when>
          <xsl:when test="@type='string' and $value!='' and @formatMask='phone'">
            <xsl:variable name="cultureAwarePhoneFormatter" select="xslNsExt:cultureAwarePhoneFormatter($value)"/>
            <xsl:choose>
              <xsl:when test="$hideOn='display' or $hideOn='always'">
                <xsl:value-of select="xslNsExt:maskValue($cultureAwarePhoneFormatter)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$cultureAwarePhoneFormatter"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="@type='string' and $value!='' and @formatMask='phoneNonUS_UK'">
            <xsl:variable name="cultureAwarePhoneFormatter" select="xslNsExt:cultureAwarePhoneFormatter($value,'{0:0000 000 0000}')"/>
            <xsl:choose>
              <xsl:when test="$hideOn='display' or $hideOn='always'">
                <xsl:value-of select="xslNsExt:maskValue($cultureAwarePhoneFormatter)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$cultureAwarePhoneFormatter"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:when test="@type='string' and $value!='' and @formatMask='referenceNumber'">
            <xsl:value-of select="xslNsExt:maskCard($value)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="formattedText">
              <xsl:call-template name="formatText">
                <xsl:with-param name="text" select="$value"/>
              </xsl:call-template>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="$hideOn='display' or $hideOn='always'">
                <xsl:value-of select="xslNsExt:maskValue($formattedText)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="formatText">
                  <xsl:with-param name="text" select="$value"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$typeValue='date'">
            <xsl:value-of select="$dateValue"/>
          </xsl:when>
          <xsl:when test="$typeValue='datetime' and $value != ''">
            <xsl:variable name="dateTimeValue">
              <xsl:choose>
                <xsl:when test="contains($value,'-')">
                  <xsl:value-of select="translate(substring($value, 1, 19), 'T', ' ')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="xslNsExt:convertToISOFormat($value, 's')"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="xslNsExt:convertISOtoDisplay($dateTimeValue, 'g')"/>
          </xsl:when>
          <xsl:when test="string-length($fieldMask) = '2' and (starts-with($fieldMask, 'C') or starts-with($fieldMask, 'c'))">
            <xsl:value-of select="xslNsExt:currencyFormatter($currencyCode, $fieldMask, $value)"/>
          </xsl:when>
          <xsl:when test="($typeValue='int' or $typeValue='float') and $value!=''">
            <xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($manuscriptBaseCulture, $fieldMask, $value)"/>
          </xsl:when>
          <xsl:when test="@type='string' and $value!='' and @formatMask='phone'">
            <xsl:value-of select="xslNsExt:cultureAwarePhoneFormatter($value)"/>
          </xsl:when>
          <xsl:when test="$typeValue='datetime' and $value != ''">
            <xsl:variable name="dateTimeValue">
              <xsl:value-of select="translate(substring($value, 1, 19), 'T', ' ')"/>
            </xsl:variable>
            <xsl:value-of select="xslNsExt:convertISOtoDisplay($dateTimeValue, 'g')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$value"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="getFormatMaskHide">
    <xsl:choose>
      <xsl:when test="substring(@formatMask,1,14)='hideOnDisplay:'">
        <xsl:value-of select="substring(@formatMask,15)"/>
      </xsl:when>
      <xsl:when test="substring(@formatMask,1,13)='hideOnDisplay'">
        <xsl:value-of select="substring(@formatMask,14)"/>
      </xsl:when>
      <xsl:when test="substring(@formatMask,1,12)='hideOnEntry:'">
        <xsl:value-of select="substring(@formatMask,13)"/>
      </xsl:when>
      <xsl:when test="substring(@formatMask,1,11)='hideOnEntry'">
        <xsl:value-of select="substring(@formatMask,12)"/>
      </xsl:when>
      <xsl:when test="substring(@formatMask,1,5)='hide:'">
        <xsl:value-of select="substring(@formatMask,6)"/>
      </xsl:when>
      <xsl:when test="substring(@formatMask,1,4)='hide'">
        <xsl:value-of select="substring(@formatMask,5)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@formatMask"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
