﻿<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions">
	<xsl:template name="setPartyIds">
		<xsl:attribute name="dctPartyRecordMappingId">
			<xsl:value-of select="@partyRecordMappingId"/>
		</xsl:attribute>
		<xsl:attribute name="dctPartyMappingField">
			<xsl:value-of select="@partyMappingField"/>
		</xsl:attribute>
		<xsl:attribute name="dctPartyPopUpTitle">
			<xsl:value-of select="@partyPopUpTitle"/>
		</xsl:attribute>
	</xsl:template>
	<xsl:template name="setPartyClasses">
		<xsl:if test="@partyMappingField">
			<xsl:text> isPartyMapped </xsl:text>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>