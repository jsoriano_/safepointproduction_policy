﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://www.duckcreektech.com/xsltDoc" exclude-result-prefixes="doc" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctTemplatesCommon.xsl"/>
	<xsl:import href="dctGenericSetFieldFormat.xsl"/>
	<xsl:output method="xml" indent="yes"/>

	<!-- WIDGET CONTAINER RENDERING///////////////// -->

	<doc:summary>
		<doc:text>Matches generic widgets in a getPage response and sets up the container DIV with the appropriate UID
			and then defers to another template to do the actual rendering of the body</doc:text>
	</doc:summary>
	<xsl:template match="widget" name="interviewWidget">
		<xsl:param name="sort"/>
		<xsl:param name="table"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableheader"/>
		<xsl:param name="tableType"/>

		<!-- Setup the unique id to come from the Server-generated UID attribute - this is important to allow AJAX to work correctly) -->
		<xsl:variable name="widgetID">
			<xsl:value-of select="@uid"/>
			<xsl:value-of select="@widgetType"/>
		</xsl:variable>

		<xsl:variable name="displayType">
			<xsl:value-of select="@displayType"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$tabledata">
				<xsl:element name="td">
					<xsl:attribute name="class">
						<xsl:value-of select="@cellStyle"/>
					</xsl:attribute>
					<div id="{$widgetID}" class="widget{$displayType}">
						<!-- Defer to another template to build the actual body of the widget -->
						<xsl:apply-templates select="." mode="interviewWidgets">
							<xsl:with-param name="table" select="$table"/>
							<xsl:with-param name="tableType" select="$tableType"/>
						</xsl:apply-templates>
					</div>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<div id="{$widgetID}" class="widget{$displayType}">
					<!-- Defer to another template to build the actual body of the widget -->
					<xsl:apply-templates select="." mode="interviewWidgets">
					</xsl:apply-templates>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<doc:summary>
		<doc:text>Handles rendering the accordion widget</doc:text>
	</doc:summary>
	<xsl:template match="widget[@widgetType='accordion']" name="accordion">

		<!-- Setup the unique id to come from the Server-generated UID attribute - this is important to allow AJAX to work correctly) -->
		<div>
			<xsl:attribute name="id">
				<xsl:value-of select="@uid"/>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:text>accordionContainer</xsl:text>
			</xsl:attribute>
			<xsl:variable name="AccordionID">
				<xsl:value-of select="@uid"/>
			</xsl:variable>
			<!-- Each item inside the accordion -->
			<xsl:for-each select="./section">
				<div>
					<xsl:attribute name="id">
						<xsl:value-of select="$AccordionID"/>
						<xsl:text>Section</xsl:text>
						<xsl:value-of select="@index"/>
					</xsl:attribute>
					<xsl:apply-templates select="."/>
				</div>
			</xsl:for-each>
			<xsl:element name="div">
				<xsl:attribute name="class">accordion</xsl:attribute>
				<xsl:attribute name="id">
					<xsl:value-of select="@uid"/>
					<xsl:text>Group</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$AccordionID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="@uid"/>
					<xsl:text>accordian</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>dctAccordion: "</xsl:text>
					<xsl:value-of select="@uid"/>
					<xsl:text>Group</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>title: "</xsl:text>
					<xsl:value-of select="@widgetCaption"/>
					<xsl:text>"</xsl:text>
					<xsl:choose>
						<xsl:when test="@width">
							<xsl:text>,</xsl:text>
							<xsl:text>width: </xsl:text>
							<xsl:value-of select="@width"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>,</xsl:text>
							<xsl:text>autoWidth: true</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<xsl:when test="@height">
							<xsl:text>,</xsl:text>
							<xsl:text>height: </xsl:text>
							<xsl:value-of select="@height"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>,</xsl:text>
							<xsl:text>autoHeight: true</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="@class">
						<xsl:text>,</xsl:text>
						<xsl:text>cls: "</xsl:text>
						<xsl:value-of select="@class"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
				<!-- Each item inside the accordion -->
				<xsl:for-each select="./section">
					<xsl:element name="div">
						<xsl:attribute name="class">accordionItem</xsl:attribute>
						<xsl:attribute name="data-config">
							<xsl:text>{</xsl:text>
							<xsl:text>contentEl: "</xsl:text>
							<xsl:value-of select="$AccordionID"/>
							<xsl:text>Section</xsl:text>
							<xsl:value-of select="@index"/>
							<xsl:text>",</xsl:text>
							<xsl:text>id: "</xsl:text>
							<xsl:value-of select="$AccordionID"/>
							<xsl:text>Section</xsl:text>
							<xsl:value-of select="@index"/>
							<xsl:text>accordianItem</xsl:text>
							<xsl:text>",</xsl:text>
							<xsl:text>title: "</xsl:text>
							<xsl:value-of select="@caption"/>
							<xsl:text>",</xsl:text>
							<xsl:text>expandEvent: "</xsl:text>
							<xsl:value-of select="@expandEvent"/>
							<xsl:text>",</xsl:text>
							<xsl:text>expandEventUid: "</xsl:text>
							<xsl:value-of select="@uid"/>
							<xsl:text>"</xsl:text>
							<xsl:text>}</xsl:text>
						</xsl:attribute>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</div>
	</xsl:template>

	<doc:summary>
		<doc:text>Handles the special needs of rendering the ID and container for a progress bar widget</doc:text>
	</doc:summary>
	<xsl:template match="widget[@widgetType='progress']" name="progress">

		<!-- Setup the unique id to come from the Server-generated UID attribute AND a trailing '_div' (uid is important to allow AJAX to work correctly) -->
		<div>
			<xsl:attribute name="id">
				<xsl:value-of select="@uid"/>
				<xsl:text>_div</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="style">
				<xsl:if test="@width">
					<xsl:text>width:</xsl:text>
					<xsl:value-of select="@width"/>
					<xsl:text>;</xsl:text>
				</xsl:if>
				<xsl:if test="@height">
					<xsl:text>height:</xsl:text>
					<xsl:value-of select="@height"/>
					<xsl:text>;</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewProgressBar</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="@uid"/>
					<xsl:text>_div</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="@uid"/>
					<xsl:text>"</xsl:text>
					<xsl:if test="@width">
						<xsl:text>,</xsl:text>
						<xsl:text>width: </xsl:text>
						<xsl:value-of select="@width"/>
					</xsl:if>
					<xsl:if test="@height">
						<xsl:text>,</xsl:text>
						<xsl:text>height: </xsl:text>
						<xsl:value-of select="@height"/>
					</xsl:if>
					<xsl:if test="@class">
						<xsl:text>,</xsl:text>
						<xsl:text>cls: "</xsl:text>
						<xsl:value-of select="@class"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="@message">
						<xsl:text>,</xsl:text>
						<xsl:text>text: "</xsl:text>
						<xsl:value-of select="@message"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
				<xsl:call-template name="addDataValue">
					<xsl:with-param name="name">value</xsl:with-param>
					<xsl:with-param name="type">number</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:choose>
							<xsl:when test="@value">
								<xsl:value-of select="@value"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>0</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:element>
		</div>
	</xsl:template>

	<!-- END//////////////////////////////////////// -->




	<!-- WIDGET BODY RENDERING////////////////////// -->

	<!-- stub overridden in dctInterviewControls.xsl -->
	<xsl:template name="createField"/>

	<doc:summary>
		<doc:text>Handles the actual rendering of the body of the widget for a Comparison List widget</doc:text>
	</doc:summary>
	<xsl:template match="widget[@widgetType='comparison']" mode="interviewWidgets" name="buildComparisonListInterviewWidgetBody">
		<xsl:param name="table"/>
		<xsl:param name="tableType"/>

		<xsl:choose>
			<xsl:when test="$table='1'">
				<xsl:for-each select="fieldInstance[1]">
					<xsl:call-template name="createField">
						<xsl:with-param name="direction">
							<xsl:value-of select="../../*/@direction"/>
						</xsl:with-param>
						<xsl:with-param name="table">
							<xsl:text>1</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="tableType">
							<xsl:value-of select="$tableType"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="*">
					<xsl:with-param name="direction">
						<xsl:value-of select="../../*/@direction"/>
					</xsl:with-param>
					<xsl:with-param name="captionPosition">
						<xsl:value-of select="../../*/@captionPosition"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<doc:summary>
		<doc:text>Handles the custom caption building behavior of options in a comparison option list</doc:text>
	</doc:summary>
	<xsl:template match="@caption[../../../parent::widget/@widgetType='comparison']" mode="buildOptionCaption">

		<xsl:variable name="extendedCaption">
			<xsl:choose>
				<xsl:when test="../../@comparisonType = 'float' or ../../@comparisonType = 'int'">
					<xsl:value-of select="parent::*/@difference"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="parent::*/@comparisonValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="formatMask">
			<xsl:choose>
				<xsl:when test="parent::*/@formatMask != ''">
					<xsl:value-of select="parent::*/@formatMask"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../../../../@formatMaskOverride"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="formattedExtendedCaption">
			<xsl:call-template name="FormatValue">
				<xsl:with-param name="value" select="$extendedCaption"/>
				<xsl:with-param name="type" select="../../@comparisonType"/>
				<xsl:with-param name="formatMask" select="$formatMask"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:value-of select="."/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$formattedExtendedCaption"/>
	</xsl:template>


	<doc:summary>
		<doc:text>Handles the custom caption building behavior of options in a deluxe drop down comparison option list</doc:text>
	</doc:summary>
	<xsl:template match="@caption[../../../parent::widget/@widgetType='comparison']" mode="buildComparisonCaption">

		<xsl:variable name="extendedCaption">
			<xsl:choose>
				<xsl:when test="../../@comparisonType = 'float' or ../../@comparisonType = 'int'">
					<xsl:value-of select="parent::*/@difference"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="parent::*/@comparisonValue"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="formatMask">
			<xsl:choose>
				<xsl:when test="parent::*/@formatMask != ''">
					<xsl:value-of select="parent::*/@formatMask"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../../../../@formatMaskOverride"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="formattedExtendedCaption">
			<xsl:call-template name="FormatValue">
				<xsl:with-param name="value" select="$extendedCaption"/>
				<xsl:with-param name="type" select="../../@comparisonType"/>
				<xsl:with-param name="formatMask" select="$formatMask"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:value-of select="$formattedExtendedCaption"/>
	</xsl:template>

	<!-- END//////////////////////////////////////// -->

	<doc:summary>
		<doc:text>Handles the custom properties for all ExtJs controls</doc:text>
	</doc:summary>



</xsl:stylesheet>