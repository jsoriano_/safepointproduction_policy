﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctCommonControls.xsl"/>
	<!--*************************************************************************************************************
		Common control templates
		************************************************************************************************************* -->
	<!--*************************************************************************************************************
		Billing Templates
		************************************************************************************************************* -->
	<xsl:template name="buildBillingActiveSection">
		<xsl:param name="accountType"/>
		<div class="{/page/content/ExpressCache/Account/AccountStatus}">
			<xsl:attribute name="id">
				<xsl:choose>
					<xsl:when test="$accountType='AGT' or $accountType='COL2'">
						<xsl:text>billingAgencyActiveSection</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>billingActiveSection</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<div id="activeAccount">
				<div class="activeAccountName" id="activeAccountNameId">
					<xsl:call-template name="getFullPartyName">
						<xsl:with-param name="partyId" select="/page/content/ExpressCache/Account/AssociatedParties/Party[@Role='Payor']/@PartyId"/>
					</xsl:call-template>
				</div>
				<xsl:choose>
					<xsl:when test="$accountType='AGT' or $accountType='COL2'">
						<xsl:variable name="contactId" select="/page/content/ExpressCache/Account/AssociatedParties/Party[@Role='Receivable Contact']/@PartyId"/>
						<div id="activeAccountReferenceId">
							<xsl:value-of select="/page/content/ExpressCache/Account/AccountReference"/>
						</div>
						<!-- Commented out until requirements for this are clear 
						<div id="agencyChangeReceivableContact">
							<xsl:value-of select="xslNsODExt:getDictRes('ReceivableContact')"/>
							<xsl:choose>
								<xsl:when test="normalize-space($contactId)=''">
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">
											<xsl:text>DCT.Submit.submitPartyAction('partyStart:partySearch','</xsl:text>
											<xsl:text>["_partyRole:Receivable Contact","_partyReturnActions:addInvolvement"</xsl:text>
											<xsl:text>]');</xsl:text>
										</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('Add_parentheses')"/>
										</xsl:with-param>
										<xsl:with-param name="name">addContactRole</xsl:with-param>
										<xsl:with-param name="id">addContactRole_Id</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">
											<xsl:text>DCT.Submit.submitPartyAction('partyStart:partySearch','</xsl:text>
											<xsl:text>["_partyRole:Receivable Contact","_partyReturnActions:changeInvolvement"</xsl:text>
											<xsl:text>]');</xsl:text>
										</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('Change_parentheses')"/>
										</xsl:with-param>
										<xsl:with-param name="name">changeContactRole</xsl:with-param>
										<xsl:with-param name="id">changeContactRole_Id</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</div>
						<div id="agencyReceivableContactId">
							<xsl:call-template name="getFullPartyName">
								<xsl:with-param name="partyId" select="$contactId"/>
							</xsl:call-template>
						</div>
						<div id="agencyReceivableContactMainPhone">
							<xsl:call-template name="getPhoneNumber">
								<xsl:with-param name="partyId" select="$contactId"/>
								<xsl:with-param name="phoneType">B</xsl:with-param>
							</xsl:call-template>
						</div>
						<div id="agencyReceivableContactFaxPhone">
							<xsl:call-template name="getPhoneNumber">
								<xsl:with-param name="partyId" select="$contactId"/>
								<xsl:with-param name="phoneType">F</xsl:with-param>
							</xsl:call-template>
						</div>
						<div id="agencyReceivableContactEmailAgency">
							<xsl:if test="normalize-space(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $contactId]/PartyEmails/PartyEmail/EmailAddress)!=''">
								<xsl:call-template name="makeButton">
									<xsl:with-param name="href">
										<xsl:text disable-output-escaping="yes">mailto:</xsl:text>
										<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $contactId]/PartyEmails/PartyEmail/EmailAddress"/>
									</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $contactId]/PartyEmails/PartyEmail/EmailAddress"/>
									</xsl:with-param>
									<xsl:with-param name="type">hyperlink</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</div>
						-->
					</xsl:when>
					<xsl:otherwise>
						<div id="activeAccountReferenceId">
							<xsl:value-of select="/page/content/ExpressCache/Account/AccountReference"/>
						</div>
						<div id="activeAccountStatusId">
							<xsl:if test="normalize-space(/page/content/ExpressCache/Account/AccountStatus)!=''">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="/page/content/ExpressCache/Account/AccountStatus"/>
								<xsl:text>)</xsl:text>
							</xsl:if>
						</div>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="getPhoneNumber">
		<xsl:param name="partyId"/>
		<xsl:param name="phoneType"/>
		<xsl:if test="normalize-space(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneNumber)!=''">
			<xsl:choose>
				<xsl:when test="$phoneType='B'">
					<xsl:value-of select="xslNsODExt:getDictRes('Phone_colon')"/>
				</xsl:when>
				<xsl:when test="$phoneType='F'">
					<xsl:value-of select="xslNsODExt:getDictRes('Fax_colon')"/>
				</xsl:when>
			</xsl:choose>
			<xsl:text>(</xsl:text>
			<xsl:value-of select="substring(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneNumber, 1,3)"/>
			<xsl:text>)</xsl:text>
			<xsl:value-of select="substring(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneNumber, 4,3)"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="substring(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneNumber, 7,4)"/>
			<xsl:if test="normalize-space(/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneExtension)!=''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="/page/content/ExpressCache/Account/*/PartyRecord[PartyRecordPartyId = $partyId and PartyPhones/PartyPhone/PhoneTypeCode = $phoneType]/PartyPhones/PartyPhone/PhoneExtension"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template name="notificationWidget">
		<xsl:if test="/page/state/ResponseInfo/NotificationResponse/NotificationRecord">
			<xsl:for-each select="/page/state/ResponseInfo/NotificationResponse/NotificationRecord">
				<div class="notificationDiv">
					<xsl:attribute name="class">
						<xsl:text>notificationDiv</xsl:text>
						<xsl:if test="/page/state/ResponseInfo/NotificationResponse/NotificationRecord/@type">
							<xsl:text> n</xsl:text>
							<xsl:value-of select="/page/state/ResponseInfo/NotificationResponse/NotificationRecord/@type"/>
						</xsl:if>
					</xsl:attribute>
					<xsl:variable name="notificationRecord">
						<xsl:copy-of select="."/>
					</xsl:variable>
					<xsl:copy-of select="xslNsExt:getNotificationInfo($notificationRecord)"/>
				</div>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	
   <xsl:template name="createBillingMainArea">
		<xsl:param name="displayNotes">
			<xsl:text>true</xsl:text>
		</xsl:param>
		<xsl:if test="$displayNotes='true' and ($canAddNotes or $canAccessAllAreas)">
			<xsl:call-template name="notesWidget"/>
		</xsl:if>
		<xsl:call-template name="notificationWidget"/>
		<xsl:call-template name="buildBillingMainAreaContent"/>
		<xsl:call-template name="buildCurrentSystemDate"/>
	</xsl:template>
	<!-- template stub is overridden in each content page -->
	<xsl:template name="buildBillingMainAreaContent"/>

	<xsl:template name="buildBillingPaymentSections">
		<xsl:param name="accountType"/>
		<div class="paymentSections" id="paymentSections">
			<xsl:call-template name="billingOutstandingInvoiceWidget">
				<xsl:with-param name="accountType" select="$accountType"/>
			</xsl:call-template>
			<xsl:call-template name="billingRecentPaymentWidget"/>
			<xsl:call-template name="billingUpcomingWidget">
				<xsl:with-param name="accountType" select="$accountType"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="billingOutstandingInvoiceWidget">
		<xsl:param name="accountType"/>
		<div id="billingOutstandingInvoice">
			<table id="tblBillingOutstandingInvoice" class="acctSummaryInvoiceTable">
				<xsl:choose>
					<xsl:when test="$accountType='AGT' or $accountType='COL2'">
						<tr class="acctSummaryInvoiceTableTitle">
							<td colspan="4">
								<xsl:value-of select="xslNsODExt:getDictRes('MostRecentStatement')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableHdr">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('StatementDate')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Docs')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('DueDate')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableRow">
							<td>
								<div id="invoiceDateId">
								</div>
							</td>
							<td>
								<div id="invoiceDocId">
								</div>
							</td>
							<td>
								<div id="invoiceCurrentDueAmountId">
								</div>
							</td>
							<td>
								<div id="invoiceDueDateId">
								</div>
							</td>
						</tr>
					</xsl:when>
					<xsl:otherwise>
						<tr class="acctSummaryInvoiceTableTitle">
							<td colspan="4">
								<div class="tableTitle">
									<xsl:value-of select="xslNsODExt:getDictRes('MostRecentInvoice')"/>
								</div>
								<xsl:if test="/page/content/*/Invoices/Invoice/InvoiceDate != ''">
									<xsl:if test="$canReprintInvoice or $canAccessAllAreas">
										<xsl:call-template name="makeButton">
											<xsl:with-param name="onclick">
												<xsl:text>DCT.Submit.submitBillingInvoiceReprint('accountSummary', 'reprintInvoice', '</xsl:text>
												<xsl:value-of select="/page/content/*/Invoices/Invoice/InvoiceId"/>
												<xsl:text>');</xsl:text>
											</xsl:with-param>
											<xsl:with-param name="icon">printer_small.png</xsl:with-param>
											<xsl:with-param name="name">name_invoicePrint</xsl:with-param>
											<xsl:with-param name="id">id_invoicePrint</xsl:with-param>
											<xsl:with-param name="qtitle">
												<xsl:value-of select="xslNsODExt:getDictRes('ReprintInvoice')"/>
											</xsl:with-param>
											<xsl:with-param name="type">loneIcon</xsl:with-param>
										</xsl:call-template>
									</xsl:if>
								</xsl:if>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableHdr">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('InvoiceDate')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Docs')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('DueDate')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableRow">
							<td>
								<div id="invoiceDateId">
									<xsl:if test="/page/content/*/Invoices/Invoice/InvoiceDate != ''">
										<!-- add back once screen has been created 
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">
											<xsl:text>DCT.Submit.submitBillingProcessAction('invoiceDetail','</xsl:text>
											<xsl:value-of select="/page/content/*/Invoices/Invoice/InvoiceId"/>
											<xsl:text>');</xsl:text>
										</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/Invoices/Invoice/InvoiceDate)"/>
										</xsl:with-param>
										<xsl:with-param name="name">name_NextScheduledInvoice</xsl:with-param>
										<xsl:with-param name="id">id_NextScheduledInvoice</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>  
									-->
										<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/Invoices/Invoice/InvoiceDate)"/>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="invoiceDocId">
									<xsl:if test="/page/content/*/Invoices/Invoice/InvoiceDate != '' and /page/content/*/Invoices/Invoice/FileName != ''">
										<xsl:if test="$canViewInvoice or $canAccessAllAreas">
											<xsl:call-template name="makeButton">
												<xsl:with-param name="onclick">
													<xsl:text>DCT.Util.displayPDFAttachment('viewPDF','</xsl:text>
													<xsl:choose>
														<xsl:when test="/page/content/*/Invoices/Invoice/AttachmentId != ''">
															<xsl:value-of select="/page/content/*/Invoices/Invoice/AttachmentId"/>
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="/page/content/*/Invoices/Invoice/AttachmentID"/>
														</xsl:otherwise>
													</xsl:choose>
													<xsl:text>','</xsl:text>
													<xsl:value-of select="substring-before(/page/content/*/Invoices/Invoice/Moniker, '\')"/>
													<xsl:text>\\</xsl:text>
													<xsl:value-of select="substring-after(/page/content/*/Invoices/Invoice/Moniker, '\')"/>
													<xsl:text>','</xsl:text>
													<xsl:value-of select="/page/content/*/Invoices/Invoice/FileName"/>
													<xsl:text>');</xsl:text>
												</xsl:with-param>
												<xsl:with-param name="icon">attach_small.png</xsl:with-param>
												<xsl:with-param name="name">name_invoiceAttachment</xsl:with-param>
												<xsl:with-param name="id">id_invoiceAttachment</xsl:with-param>
												<xsl:with-param name="type">loneIcon</xsl:with-param>
											</xsl:call-template>
										</xsl:if>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="invoiceCurrentDueAmountId">
									<xsl:if test="/page/content/*/Invoices/Invoice/InvoiceDate != ''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/*/Invoices/Invoice/CurrencyCulture, 'c2', /page/content/*/Invoices/Invoice/InvoiceTotalAmount)"/>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="invoiceDueDateId">
									<xsl:if test="/page/content/*/Invoices/Invoice/InvoiceDate != ''">
										<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/Invoices/Invoice/DueDate)"/>
									</xsl:if>
								</div>
							</td>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingRecentPaymentWidget">
		<div id="billingRecentPayment">
			<table id="tblBillingRecentPayment" class="acctSummaryInvoiceTable">
				<tr class="acctSummaryInvoiceTableTitle">
					<td colspan="3">
						<xsl:value-of select="xslNsODExt:getDictRes('LastPaymentReceived')"/>
					</td>
				</tr>
				<tr class="acctSummaryInvoiceTableHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Date')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Method')"/>
					</td>
				</tr>
				<tr class="acctSummaryInvoiceTableRow">
					<td>
						<div id="recentPaymentDateId">
							<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/Payments/Payment/PaymentEntryDate)"/>
						</div>
					</td>
					<td>
						<div id="recentPaymentAmountId">
							<xsl:if test="/page/content/*/Payments/Payment/PaymentId">
								<xsl:choose>
									<xsl:when test="$canAccessPaymentDetail or $canAccessAllAreas">
										<xsl:call-template name="makeButton">
											<xsl:with-param name="onclick">
												<xsl:text>DCT.Submit.submitBillingProcessAction('paymentDetails',</xsl:text>
												<xsl:value-of select="/page/content/*/Payments/Payment/PaymentId"/>
												<xsl:text>);</xsl:text>
											</xsl:with-param>
											<xsl:with-param name="caption">
												<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/*/Payments/Payment/CurrencyCulture, 'c2', /page/content/*/Payments/Payment/PaymentAmount)"/>
											</xsl:with-param>
											<xsl:with-param name="name">name_RecentPaymentDetails</xsl:with-param>
											<xsl:with-param name="id">id_RecentPaymentDetails</xsl:with-param>
											<xsl:with-param name="type">hyperlink</xsl:with-param>
										</xsl:call-template>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/*/Payments/Payment/CurrencyCulture, 'c2', /page/content/*/Payments/Payment/PaymentAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
						</div>
					</td>
					<td>
						<div id="recentPaymentMethodId">
							<xsl:value-of select="/page/content/*/Payments/Payment/PaymentMethod"/>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingUpcomingWidget">
		<xsl:param name="accountType"/>
		<div id="billingUpcoming">
			<table id="tblBillingUpcoming" class="acctSummaryInvoiceTable">
				<xsl:choose>
					<xsl:when test="$accountType='AGT'">
						<tr class="acctSummaryInvoiceTableTitle">
							<td colspan="3">
								<xsl:value-of select="xslNsODExt:getDictRes('NextScheduledStatement')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableHdr">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('StatementDate')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableRow">
							<td>
								<div id="upcomingInvoiceDateId">
									<xsl:if test="/page/content/Account/NextScheduledStatement/StatementDate != ''">
										<xsl:choose>
											<xsl:when test="$canAccessInstallmentSchedule or $canAccessAllAreas">
												<xsl:call-template name="makeButton">
													<xsl:with-param name="onclick">
														<xsl:text>DCT.Submit.submitBillingInstallment('installmentSchedule','</xsl:text>
														<xsl:value-of select="/page/content/Account/NextScheduledInvoice/InvoiceDate"/>
														<xsl:text>');</xsl:text>
													</xsl:with-param>
													<xsl:with-param name="caption">
														<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/Account/NextScheduledStatement/StatementDate)"/>
													</xsl:with-param>
													<xsl:with-param name="name">name_NextScheduledInstallment</xsl:with-param>
													<xsl:with-param name="id">id_NextScheduledInstallment</xsl:with-param>
													<xsl:with-param name="type">hyperlink</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/Account/NextScheduledStatement/StatementDate)"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="upcomingInvoiceAmountId">
									<xsl:if test="/page/content/Account/NextScheduledStatement/StatementDate != ''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/Account/CurrencyCulture, 'c2', /page/content/Account/NextScheduledStatement/Amount)"/>
									</xsl:if>
								</div>
							</td>
						</tr>
					</xsl:when>
					<xsl:otherwise>
						<tr class="acctSummaryInvoiceTableTitle">
							<td colspan="3">
								<xsl:value-of select="xslNsODExt:getDictRes('NextScheduledInvoice')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableHdr">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('InvoiceDate')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('DueDate')"/>
							</td>
						</tr>
						<tr class="acctSummaryInvoiceTableRow">
							<td>
								<div id="upcomingInvoiceDateId">
									<xsl:if test="/page/content/Account/NextScheduledInvoice/InvoiceDate != ''">
										<xsl:choose>
											<xsl:when test="$canAccessInstallmentSchedule or $canAccessAllAreas">
												<xsl:call-template name="makeButton">
													<xsl:with-param name="onclick">
														<xsl:text>DCT.Submit.submitBillingInstallment('installmentSchedule','</xsl:text>
														<xsl:value-of select="/page/content/Account/NextScheduledInvoice/InvoiceDate"/>
														<xsl:text>');</xsl:text>
													</xsl:with-param>
													<xsl:with-param name="caption">
														<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/Account/NextScheduledInvoice/InvoiceDate)"/>
													</xsl:with-param>
													<xsl:with-param name="name">name_NextScheduledInstallment</xsl:with-param>
													<xsl:with-param name="id">id_NextScheduledInstallment</xsl:with-param>
													<xsl:with-param name="type">hyperlink</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/Account/NextScheduledInvoice/InvoiceDate)"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="upcomingInvoiceAmountId">
									<xsl:if test="/page/content/Account/NextScheduledInvoice/InvoiceDate != ''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/Account/CurrencyCulture, 'c2', /page/content/Account/NextScheduledInvoice/Amount)"/>
									</xsl:if>
								</div>
							</td>
							<td>
								<div id="upcomingInvoiceDueDateId">
									<xsl:if test="/page/content/Account/NextScheduledInvoice/InvoiceDate != ''">
										<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/Account/NextScheduledInvoice/InvoiceDueDate)"/>
									</xsl:if>
								</div>
							</td>
						</tr>
					</xsl:otherwise>
				</xsl:choose>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildBillingSummaryGrids">
		<xsl:param name="type"/>
		<xsl:param name="accountType"/>
		<div class="billingSummaryGrids" id="billingSummaryGrids">
			<xsl:call-template name="billingSummaryActivityGrid">
				<xsl:with-param name="type" select="$type"/>
				<xsl:with-param name="accountType" select="$accountType"/>
			</xsl:call-template>
			<xsl:choose>
				<xsl:when test="$accountType='COLL'">
					<xsl:call-template name="billingCollectionsPolicySummaryGrid"/>
				</xsl:when>
				<xsl:when test="$accountType='ACT' or $accountType='AL'">
					<xsl:call-template name="billingPolicySummaryGrid">
						<xsl:with-param name="type" select="$type"/>
					</xsl:call-template>
				</xsl:when>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="billingSummaryActivityGrid">
		<xsl:param name="type"/>
		<xsl:param name="accountType"/>
		<div class="billingActivityList" id="billingActivityListId">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">activityList</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:choose>
						<xsl:when test="$type='EFTMaintenance'">
							<xsl:text>eftRequestList</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="$accountType='AGT'">
									<xsl:text>agencyAccountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='ACT'">
									<xsl:text>accountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='AL'">
									<xsl:text>accountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='COLL'">
									<xsl:text>accountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='COL2'">
									<xsl:text>agencyAccountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='COMM'">
									<xsl:text>accountActivityList</xsl:text>
								</xsl:when>
								<xsl:when test="$accountType='CLM'">
									<xsl:text>accountActivityList</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>policyActivityList</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="pageSize">25</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>SearchControl/Paging/TotalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>ListItem/ItemData</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>ActivityId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'ActivityTransactionDateTime', type: 'date', dateFormat: 'Y-m-dTH:i:s', mapping: 'ActivityTransactionDateTime'},</xsl:text>
					<xsl:text>{name: 'ActivityType', type: 'string', mapping: 'ActivityType'},</xsl:text>
					<xsl:text>{name: 'ActivityUserFullName', type: 'string', mapping: 'ActivityUserFullName'},</xsl:text>
					<xsl:text>{name: 'ActivityAmount', type: 'string', mapping: 'ActivityAmount'},</xsl:text>
					<xsl:text>{name: 'Attachment', type: 'string', mapping: 'AttachmentId'},</xsl:text>
					<xsl:text>{name: 'Subject', type: 'string', mapping: 'SubjectId'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T("Date"), dataIndex: 'ActivityTransactionDateTime', sortable: true, width: 60, renderer: Ext.util.Format.dateRenderer('</xsl:text>
					<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
					<xsl:text>, g:i a'),</xsl:text>
					<xsl:text>filter: {type: 'date', dateBefore: false, dateOn: false, dateFormat: 'Y-m-d'}},</xsl:text>
					<xsl:text>{text: DCT.T("ActivityType"), dataIndex: 'ActivityType', sortable: true, width: 95,</xsl:text>
					<xsl:text>filter: {type: 'list',options: [</xsl:text>
					<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_ACTIVITYTYPE']/ListEntry">
						<xsl:text>[</xsl:text>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="@Code"/>
						<xsl:text>'</xsl:text>
						<xsl:text>,</xsl:text>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="@Description"/>
						<xsl:text>']</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]}},</xsl:text>
					<xsl:text>{text: DCT.T("CreatedBy"), dataIndex: 'ActivityUserFullName', sortable: true, width: 55},</xsl:text>
					<xsl:text>{text: DCT.T("Amount"), dataIndex: 'ActivityAmount', sortable: true, width: 35, renderer: DCT.Grid.activityAmountColumnRenderer, align: 'right'},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'Attachment', sortable: false, width: 15, renderer: DCT.Grid.documentColumnRenderer},</xsl:text>
					<xsl:text>{text: "", dataIndex: 'Subject', sortable: false, width: 10, renderer: DCT.Grid.reprintInvoiceColumnRenderer}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.activityDescriptionRowRenderer</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnFiltering">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:choose>
						<xsl:when test="$type='Account'">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="xslNsODExt:getDictRes('RecentAccountActivity')"/>
							<xsl:text>'</xsl:text>
						</xsl:when>
						<xsl:when test="$type='EFTMaintenance'">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="xslNsODExt:getDictRes('EftTransactions')"/>
							<xsl:text>'</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>'</xsl:text>
							<xsl:value-of select="xslNsODExt:getDictRes('RecentPolicyActivity')"/>
							<xsl:text>'</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="billingCollectionsPolicySummaryGrid">
		<div id="billingCollectionsPolicyListId">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">collectionsPolicyList</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:text>collectionsPolicyList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="pageSize">25</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>PolicyTermList/Paging/TotalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>PolicyTermList/PolicyTermItem</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>PolicyTermId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'CollectionsPayor', type: 'string', mapping: 'AssociatedParties/Party/@PartyId'},</xsl:text>
					<xsl:text>{name: 'PolicyReference', type: 'string', mapping: 'PolicyReference'},</xsl:text>
					<xsl:text>{name: 'PolicyTermEffectiveDate', type: 'string', mapping: 'PolicyTermEffectiveDate'},</xsl:text>
					<xsl:text>{name: 'PolicyTermExpirationDate', type: 'string', mapping: 'PolicyTermExpirationDate'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T("PolicyReference"), dataIndex: 'PolicyReference', sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T("PolicyTerm"), dataIndex: 'PolicyTermEffectiveDate', sortable: true, renderer: DCT.Grid.policyTermRenderer},</xsl:text>
					<xsl:text>{text: DCT.T("CollectionsPayor"), dataIndex: 'CollectionsPayor', sortable: true, renderer: DCT.Grid.collectorsPayorRenderer}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnFiltering">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="filterArray">
					<xsl:text>[{type: 'string', dataIndex: 'PolicyReference'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:text>'</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('PoliciesAssociatedWithThisAccount')"/>
					<xsl:text>'</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="billingPolicySummaryGrid">
		<xsl:param name="type"/>
		<div class="billingPolicyList" id="billingPolicyListId">

			<xsl:call-template name="buildNonPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:text>policyList</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="storeFields">
					<xsl:text>[{name: 'id', type: 'string', mapping: 0},</xsl:text>
					<xsl:text>{name: 'policyId', type: 'string', mapping: 0},</xsl:text>
					<xsl:if test="$type='Account'">
						<xsl:text>{name: 'policyNumberLob', type: 'string', mapping: 1},</xsl:text>
					</xsl:if>
					<xsl:text>{name: 'status', type: 'string', mapping: 2},</xsl:text>
					<xsl:text>{name: 'policyTerm', type: 'string', mapping: 3},</xsl:text>
					<xsl:text>{name: 'termBalance', type: 'string', mapping: 4},</xsl:text>
					<xsl:text>{name: 'amountDue', type: 'string', mapping: 5},</xsl:text>
					<xsl:text>{name: 'holdTypeCode', type: 'string', mapping: 6},</xsl:text>
					<xsl:text>{name: 'quoteId', type: 'string', mapping: 7},</xsl:text>
					<xsl:text>{name: 'holdReasonCode', type: 'string', mapping: 8},</xsl:text>
					<xsl:text>{name: 'PolicyTermHoldReason', type: 'string', mapping: 9},</xsl:text>
					<xsl:text>{name: 'collectionStatusCode', type: 'string', mapping: 10}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="gridData">
					<xsl:text>[</xsl:text>
					<xsl:for-each select="/page/content/*/PolicyTerms/PolicyTerm">
						<xsl:variable name="PolicyTermId" select="PolicyTermId" />
						<xsl:text>[</xsl:text>
						<xsl:value-of select="PolicyTermId"/>
						<xsl:text>,"</xsl:text>
						<xsl:value-of select="PolicyReference"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="PolicyLineOfBusiness"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="PolicyTermStatus"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', PolicyTermEffectiveDate)"/>
						<xsl:text> - </xsl:text>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', PolicyTermExpirationDate)"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/Account/CurrencyCulture, 'c2', OutstandingBalance)"/>
						<xsl:text>","</xsl:text>
						<xsl:choose>
							<xsl:when test="normalize-space(CurrentDueAmount)=''">
								<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/Account/CurrencyCulture, 'c2', '0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/Account/CurrencyCulture, 'c2', CurrentDueAmount)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="HoldTypeCode"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="PASPolicyId"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="HoldReasonCode"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="PolicyTermHoldReason"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="/page/content/CollectionsRecords/CollectionsRecord[PolicyTermId=$PolicyTermId]/StatusCode"/>
						<xsl:text>"]</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{</xsl:text>
					<xsl:choose>
						<xsl:when test="$type='Account'">
							<xsl:text>text: DCT.T("Reference"), dataIndex: 'policyNumberLob', sortable: true, width: 60},</xsl:text>
							<xsl:choose>
								<xsl:when test="$canAccessPolicySummary or $canAccessAllAreas">
									<xsl:text>{text: DCT.T("Term"), dataIndex: 'policyTerm', sortable: true, width: 70, renderer: DCT.Grid.policyTermColumnRenderer, groupRenderer: DCT.Grid.removeAnchorsFromGroupHeader},</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>{text: DCT.T("Term"), dataIndex: 'policyTerm', sortable: true, width: 70, groupRenderer: DCT.Grid.removeAnchorsFromGroupHeader},</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>{text: DCT.T("Status"), dataIndex: 'status', sortable: true, width: 50},</xsl:text>
							<xsl:text>{text: DCT.T("CurrentDue"), dataIndex: 'amountDue', width: 40, sortable: true, renderer: DCT.Grid.alignAmountRenderer},</xsl:text>
							<xsl:text>{text: DCT.T("OutstandingBalance"), dataIndex: 'termBalance', width: 50, sortable: true, renderer: DCT.Grid.alignAmountRenderer},</xsl:text>
							<xsl:text>{text: DCT.T(""), dataIndex: 'quoteId', width: 12, sortable: false, renderer: DCT.Grid.policyLinkToPASRenderer},</xsl:text>
							<xsl:text>{text: DCT.T(""), dataIndex: 'holdReasonCode', width: 12, sortable: true, renderer: DCT.Grid.checkIfPolicyHold}]</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>text: DCT.T("Term"), dataIndex: 'policyTerm', sortable: true, width: 70, renderer: DCT.Grid.policyTermColumnRenderer, groupRenderer: DCT.Grid.removeAnchorsFromGroupHeader},</xsl:text>
							<xsl:text>{text: DCT.T("Status"), dataIndex: 'status', sortable: true, width: 50},</xsl:text>
							<xsl:text>{text: DCT.T("CurrentDue"), dataIndex: 'amountDue', width: 40, sortable: true, align: 'right'},</xsl:text>
							<xsl:text>{text: DCT.T("OutstandingBalance"), dataIndex: 'termBalance', width: 50, sortable: true, align: 'right'},</xsl:text>
							<xsl:text>{text: DCT.T(""), dataIndex: 'quoteId', width: 12, sortable: false, renderer: DCT.Grid.policyLinkToPASRenderer},</xsl:text>
							<xsl:text>{text: DCT.T(""), dataIndex: 'holdReasonCode', width: 12, sortable: true, renderer: DCT.Grid.checkIfPolicyHold}]</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="groupingView">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="sortInfo">
					<xsl:choose>
						<xsl:when test="$type='Account'">
							<xsl:text>{property: 'policyNumberLob', direction: "ASC"}</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>{property: 'policyTerm', direction: "ASC"}</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="groupField">
					<xsl:choose>
						<xsl:when test="$type='Account'">
							<xsl:text>'policyNumberLob'</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>'policyTerm'</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:choose>
						<xsl:when test="$type='Account'">
							<xsl:text>'</xsl:text>
							<xsl:value-of select="xslNsODExt:getDictRes('PoliciesAssociatedWithThisAccount')"/>
							<xsl:text>'</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>'</xsl:text>
							<xsl:choose>
							<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
								<xsl:value-of select="xslNsODExt:getDictRes('OtherPolicyTermsAssociatedArrangement')" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="xslNsODExt:getDictRes('OtherPolicyTermsAssociatedPolicy')" />
							</xsl:otherwise>
							</xsl:choose>
							<xsl:text>'</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="rowClassRenderer">
					<xsl:text>DCT.Grid.highLightRow</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="billingContactsWidget">
		<xsl:param name="screenType"/>
		<xsl:param name="accountType"/>
		<xsl:for-each select="/page/content/ExpressCache/Account/AssociatedParties/Party">
			<xsl:if test="@Role !='Agent'">
				<xsl:variable name="divId">
					<xsl:value-of select="@Role"/>
					<xsl:value-of select="position()"/>
					<xsl:text>Id</xsl:text>
				</xsl:variable>
				<div id="{$divId}" class="x-hidden">
					<xsl:choose>
						<xsl:when test="/page/content/ExpressCache/Account/AccountTypeCode='CLM'">
							<xsl:if test="@Role='Payor' or @Role='Payee'">
								<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
								<xsl:attribute name="data-config">
									<xsl:text>{</xsl:text>
									<xsl:text>id: "contact</xsl:text>
									<xsl:value-of select="@Role"/>
									<xsl:value-of select="position()"/>
									<xsl:text>",</xsl:text>
									<xsl:text>title: "</xsl:text>
									<xsl:value-of select="@Role"/>
									<xsl:text>",</xsl:text>
									<xsl:text>contentEl: "</xsl:text>
									<xsl:value-of select="$divId"/>
									<xsl:text>",</xsl:text>
									<xsl:text>collapsed: </xsl:text>
									<xsl:choose>
										<xsl:when test="$screenType='accountSummary'">
											<xsl:text>false</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>true</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:text>,</xsl:text>
									<xsl:text>iconCls: "</xsl:text>
									<xsl:choose>
										<xsl:when test="@Role='Payor'">
											<xsl:text>i2_user</xsl:text>
										</xsl:when>
										<xsl:when test="@Role='Payee'">
											<xsl:text>i2_user_suit</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>i2_user_gray</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:text>"</xsl:text>
									<xsl:text>}</xsl:text>
								</xsl:attribute>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
							<xsl:attribute name="data-config">
								<xsl:text>{</xsl:text>
								<xsl:text>id: "contact</xsl:text>
								<xsl:value-of select="@Role"/>
								<xsl:value-of select="position()"/>
								<xsl:text>",</xsl:text>
								<xsl:text>title: "</xsl:text>
								<xsl:value-of select="@Role"/>
								<xsl:text>",</xsl:text>
								<xsl:text>contentEl: "</xsl:text>
								<xsl:value-of select="$divId"/>
								<xsl:text>",</xsl:text>
								<xsl:text>collapsed: </xsl:text>
								<xsl:choose>
									<xsl:when test="$screenType='accountSummary'">
										<xsl:text>false</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>true</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>,</xsl:text>
								<xsl:text>iconCls: "</xsl:text>
								<xsl:choose>
									<xsl:when test="@Role='Payor'">
										<xsl:text>i2_user</xsl:text>
									</xsl:when>
									<xsl:when test="@Role='Payee'">
										<xsl:text>i2_user_suit</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>i2_user_gray</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>"</xsl:text>
								<xsl:text>}</xsl:text>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<div id="billingContactName{$divId}" class="billingContactName">
						<xsl:choose>
							<xsl:when test="@Role!='Payor' and @Role!='Payee'">
								<xsl:call-template name="getFullPartyName">
									<xsl:with-param name="partyId" select="@PartyId"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="makeButton">
									<xsl:with-param name="onclick">
										<xsl:text>DCT.Submit.submitPartyAction('partyStart:partyUpdate','</xsl:text>
										<xsl:text>["_partyRole:</xsl:text>
										<xsl:value-of select="@Role"/>
										<xsl:text>","_partyIndex:</xsl:text>
										<xsl:value-of select="@PartyId"/>
										<xsl:text>","_partyReturnStructure:none"</xsl:text>
										<xsl:text>]');</xsl:text>
									</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:call-template name="getFullPartyName">
											<xsl:with-param name="partyId" select="@PartyId"/>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="name">name_<xsl:value-of select="@Role"/>ContactName</xsl:with-param>
									<xsl:with-param name="id">id_<xsl:value-of select="@Role"/>ContactName</xsl:with-param>
									<xsl:with-param name="type">hyperlink</xsl:with-param>
                                                                        <xsl:with-param name="class">g-wrap</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<div id="billingContactAddress{$divId}" class="billingContactAddress">
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationAddressLine1"/>
					</div>
					<div id="billingContactCityStateZip{$divId}" class="billingContactCityStateZip">
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationCity"/>
						<xsl:if test="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationCity !=''">
							<xsl:text>, </xsl:text>
						</xsl:if>
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationStateCode"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationPostalCode"/>
					</div>
					<xsl:if test="normalize-space(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber)!=''">
						<div id="billingContactPhone{$divId}" class="billingContactPhone">
							<xsl:choose>
								<xsl:when test="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneFormatCode='US1'">
									<xsl:text>(</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 1,3)"/>
									<xsl:text>)</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 4,3)"/>
									<xsl:text>-</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 7,4)"/>
									<xsl:if test="normalize-space(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension)!=''">
										<xsl:text> </xsl:text>
										<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension"/>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber"/>
									<xsl:if test="normalize-space(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension)!=''">
										<xsl:text> </xsl:text>
										<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension"/>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</xsl:if>
					<div id="billingContactEmail{$divId}" class="billingContactEmail">
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyEmails/PartyEmail/EmailAddress"/>
					</div>
					<xsl:if test="@Role='Payor' or @Role='Payee'">
						<div id="billingChangeRole{$divId}" class="billingChangeRole">
							<xsl:if test="$accountType !='COMM'">
								<xsl:if test="$canChangePayorOrPayeeRoles or $canAccessAllAreas">
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">
											<xsl:text>DCT.Submit.submitPartyAction('partyStart:partySearch','</xsl:text>
											<xsl:text>["_partyRole:</xsl:text>
											<xsl:value-of select="@Role"/>
											<xsl:text>","_partyReturnActions:changeInvolvement"</xsl:text>
											<xsl:text>]');</xsl:text>
										</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('Change')"/>
											<xsl:text> </xsl:text>
											<xsl:value-of select="@Role"/>
										</xsl:with-param>
										<xsl:with-param name="name">name_<xsl:value-of select="@Role"/>ChangeRole</xsl:with-param>
										<xsl:with-param name="id">id_<xsl:value-of select="@Role"/>ChangeRole</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</xsl:if>
							</xsl:if>
						</div>
					</xsl:if>
					<xsl:if test="@Role='Payor'">
						<div id="billingAccountsPayor{$divId}" class="billingAccountsPayor">
							<xsl:if test="$canChangePayorOrPayeeRoles or $canAccessAllAreas">
                <xsl:if test="normalize-space(/page/content/ExpressCache/AssociatedAccounts/Account) !=''">
								<table>
									<tr>
										<td colspan="3">
											<h1>
												<xsl:value-of select="xslNsODExt:getDictRes('AssociatedAccounts')"/>
											</h1>
										</td>
									</tr>
									<xsl:for-each select="/page/content/ExpressCache/AssociatedAccounts/Account">
										<xsl:variable name="accountTypeCode" select="AccountTypeCode"/>
										<tr>
											<td>
												<xsl:call-template name="makeButton">
													<xsl:with-param name="onclick">
														<xsl:text>DCT.Submit.submitBillingAccountSummaryPage('accountSummary','</xsl:text>
														<xsl:value-of select="AccountId"/>
														<xsl:text>');</xsl:text>
													</xsl:with-param>
													<xsl:with-param name="caption">
														<xsl:value-of select="AccountReference"/>
													</xsl:with-param>
													<xsl:with-param name="type">hyperlink</xsl:with-param>
												</xsl:call-template>
											</td>
											<td>
												<xsl:text> </xsl:text>
											</td>
											<td>
												<div class="g-btn-m">
													<xsl:value-of select="xslNsODExt:getDictRes($accountTypeCode)"/>
												</div>
											</td>
										</tr>
									</xsl:for-each>
								</table>
                </xsl:if>
							</xsl:if>
						</div>
					</xsl:if>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="billingLedgerWidget">
		<xsl:param name="accountType"/>
		<xsl:variable name="currencyCulture" select="/page/content/ExpressCache/Account/CurrencyCulture"/>
		<div id="billingLedger" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "ledger",</xsl:text>
				<xsl:text>title: "</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('AccountLedger')"/>
				<xsl:text>",</xsl:text>
				<xsl:text>contentEl: "billingLedger",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblBillingLedger" class="formTable">
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('OutstandingBalance_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingOutstandingBalanceId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/OutstandingBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/OutstandingBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>

				<xsl:if test="$accountType!='COLL'">
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentAmountDue')"/>
							</div>
						</td>
						<td>
							<div id="billingCurrentAmountDueId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/Account/CurrentDueAmount)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/CurrentDueAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('PastDueAmount')"/>
							</div>
						</td>
						<td>
							<div id="billingPastDueAmountId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/Account/PastDueAmount)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/PastDueAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('InSuspenseAmount')"/>
							</div>
						</td>
						<td>
							<div id="billingOInSuspenseAmountId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/Account/InSuspenseAmount)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/InSuspenseAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('PendingDisbursements')"/>
							</div>
						</td>
						<td>
							<div id="billingOInSuspenseAmountId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/Account/PendingDisbursements)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/PendingDisbursements)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</xsl:if>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingDetailsWidget">
		<xsl:param name="screenType"/>
		<div id="billingDetail" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "details",</xsl:text>
				<xsl:text>title: "</xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('AccountDetails')"/>
				<xsl:text>",</xsl:text>
				<xsl:text>contentEl: "billingDetail",</xsl:text>
				<xsl:text>collapsed: </xsl:text>
				<xsl:choose>
					<xsl:when test="$screenType='accountSummary'">
						<xsl:text>false</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>true</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblBillingDetail" class="formTable">
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('TargetDueDays')"/>
						</div>
					</td>
					<td>
						<div id="billingTargetDueDaysId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/TargetDueDays"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('AccountType_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingAccountTypeId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/AccountType"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('AccountClass')"/>
						</div>
					</td>
					<td>
						<div id="billingAmountClassId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/AccountClass"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('CollectionMethod')"/>
						</div>
					</td>
					<td>
						<div id="billingCollectionMethodId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/CollectionMethod"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('DisbursementHold')"/>
						</div>
					</td>
					<td>
						<div id="billingDisbursementHoldId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/DisbursementHold"/>
						</div>
					</td>
				</tr>				
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('NsfCounter')"/>
						</div>
					</td>
					<td>
						<div id="billingNSFCounterId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/Account/NSFCounter"/>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingAgencyLedgerWidget">
		<xsl:param name="id"/>
		<xsl:param name="title"/>
		<xsl:variable name="currencyCulture" select="/page/content/ExpressCache/Account/CurrencyCulture"/>
		<div id="billingLedger" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<!--<xsl:text>id: "agencyLedger",</xsl:text>-->
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$id"/>
				<xsl:text>",</xsl:text>
				<!--<xsl:text>title: "Agency Ledger",</xsl:text>-->
				<xsl:text>title: "</xsl:text>
				<xsl:value-of select="$title"/>
				<xsl:text>",</xsl:text>
				<xsl:text>contentEl: "billingLedger",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblBillingLedger" class="formTable">
				<tr>
					<td colspan="2">
						<div class="titleLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('AccountTotal')"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Gross_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingCurrentAccountAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/AccountBalance/GrossBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/AccountBalance/GrossBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Net_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingCurrentAccountNetAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/AccountBalance/NetBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/AccountBalance/NetBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<!--
				<tr>
					<td colspan="2">
						<div class="titleLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('OpenItemsBalance')"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Gross_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingCurrentGrossAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/OpenItemBalance/GrossBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/OpenItemBalance/GrossBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Net_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingCurrentNetAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/OpenItemBalance/NetBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/OpenItemBalance/NetBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
-->
				<tr>
					<td colspan="2">
						<div class="titleLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('MostRecentStatement')"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Gross_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingStatementGrossAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/MostRecentStatement/RecentGross)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/MostRecentStatement/RecentGross)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="ledgerGroupLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Net_colon')"/>
						</div>
					</td>
					<td>
						<div id="billingStatementNetAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/MostRecentStatement/RecentNet)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/MostRecentStatement/RecentNet)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('PromisedTotal')"/>
						</div>
					</td>
					<td>
						<div id="billingPromisedAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/PromisedBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/PromisedBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<!--	
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('PastDueNet')"/>
						</div>
					</td>
					<td>
						<div id="billingPastDueAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/PastDueBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/PastDueBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('HoldBillNet')"/>
						</div>
					</td>
					<td>
						<div id="billingOInSuspenseAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/InStopBill)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/Account/InStopBill)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('NumSymbolLatePayments')"/>
						</div>
					</td>
					<td>
						<div id="billingOInSuspenseAmountId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/Account/NumberLatePayments)=''">
									<xsl:text>0</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="/page/content/ExpressCache/Account/NumberLatePayments"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
-->
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingAgencyProgramsWidget">
		<div id="billingPrograms" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "agencyPrograms",</xsl:text>
				<xsl:text>title: "Programs",</xsl:text>
				<xsl:text>contentEl: "billingPrograms",</xsl:text>
				<xsl:text>collapsed: true</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblBillingPrograms" class="formTable">
				<tr>
					<td>
						<div class="programNameLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Name')"/>
						</div>
					</td>
					<td>
						<div class="programCodeLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('Code')"/>
						</div>
					</td>
				</tr>
				<xsl:for-each select="/page/content/ExpressCache/Account/Programs/Program">
					<tr>
						<td>
							<div id="billingProgramNameId" class="dataItem">
								<xsl:value-of select="@Description"/>
							</div>
						</td>
						<td>
							<div id="billingProgramCodeId" class="dataItem">
								<xsl:value-of select="@Code"/>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingAgencyDetailsWidget">
		<xsl:param name="id"/>
		<xsl:param name="title"/>
		<xsl:variable name="payorId" select="/page/content/ExpressCache/Account/AssociatedParties/Party[@Role='Payor']/@PartyId"/>
		<div id="billingDetail" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<!--<xsl:text>id: "agencyDetails",</xsl:text>-->
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$id"/>
				<xsl:text>",</xsl:text>
				<!--<xsl:text>title: "Agency Details",</xsl:text>-->
				<xsl:text>title: "</xsl:text>
				<xsl:value-of select="$title"/>
				<xsl:text>",</xsl:text>
				<xsl:text>contentEl: "billingDetail",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<div id="agencyDetailsAgencyReference" class="billingAgencyDetailsReference">
				<xsl:text> </xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('AgencyReference')"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="/page/content/ExpressCache/Account/AgencyAccount/AgencyReference"/>
			</div>
			<div id="agencyDetailsAddressId" class="billingAgencyDetailsAddress">
				<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/Locations/Location/LocationAddressLine1"/>
			</div>
			<div id="agencyDetailsCityStateZipId" class="billingAgencyDetailsCityStateZip">
				<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/Locations/Location/LocationCity"/>
				<xsl:text>, </xsl:text>
				<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/Locations/Location/LocationStateCode"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/Locations/Location/LocationPostalCode"/>
			</div>
			<xsl:if test="normalize-space(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneNumber)!=''">
				<div id="agencyDetailsPhoneId" class="billingAgencyDetailsPhone">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneNumber, 1,3)"/>
					<xsl:text>)</xsl:text>
					<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneNumber, 4,3)"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="substring(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneNumber, 7,4)"/>
					<xsl:if test="normalize-space(/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneExtension)!=''">
						<xsl:text> </xsl:text>
						<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="/page/content/ExpressCache/Account/Parties/PartyRecord[PartyRecordPartyId = $payorId]/PartyPhones/PartyPhone/PhoneExtension"/>
					</xsl:if>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="paymentActionTableWidget">
		<xsl:param name="amountHeader"/>
		<xsl:param name="totalText"/>
		<xsl:param name="actionButton"/>
		<xsl:param name="titleText"/>
		<xsl:param name="screenType"/>
		<xsl:variable name="currencyCulture">
			<xsl:choose>
				<xsl:when test="$screenType = 'suspend'">
					<xsl:value-of select="/page/content/PaymentDetail/CurrencyCulture"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/page/content/SuspenseList/SuspenseRecords/SuspenseRecord/CurrencyCulture"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div id="itemListDetail" tabletype="acrossAboveOnce">
			<xsl:call-template name="buildHiddenInput">
				<xsl:with-param name="id">
					<xsl:text>_hiddenDisplayAmtMask</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="name">
					<xsl:text>_hiddenDisplayAmtMask</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="xslNsExt:numberFormatMaskToUse('-$???????????.00', $currencyCulture)"/>
				</xsl:with-param>
			</xsl:call-template>
			<div class="displayTblTitle">
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2',/page/content/TotalInfo/TotalAmounts/Available)"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="$titleText"/>
			</div>
			<table id="tblItemListDetail" class="displayTbl">
				<tr class="displayTblHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Direction')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('AvailableAmount')"/>
					</td>
					<xsl:if test="$screenType!='suspend'">
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('DisbursementGroup')"/>
						</td>
					</xsl:if>
					<td>
						<xsl:value-of select="$amountHeader"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('AmountRemaining')"/>
					</td>
					<xsl:if test="$screenType='refundToSource'">
						<xsl:if test="/page/content/SuspenseList/SuspenseRecords/SuspenseRecord/RefundMethodCode != 'CK'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('RefundMethod')"/>
							</td>
						</xsl:if>
					</xsl:if>
					<td>
						<xsl:if test="$screenType='suspend'">
							<xsl:value-of select="xslNsODExt:getDictRes('HoldInSuspense')"/>
						</xsl:if>
					</td>
					<xsl:if test="$screenType='suspend'">
						<td></td>
					</xsl:if>
					<xsl:if test="$screenType='refundToSource'">
						<td></td>
					</xsl:if>
				</tr>
				<xsl:choose>
					<xsl:when test="$screenType='suspend'">
						<xsl:for-each select="/page/content/PaymentDetail/PaymentAllocations/PaymentAllocation">
							<xsl:variable name="rowNumber">
								<xsl:value-of select="position()"/>
							</xsl:variable>
							<xsl:call-template name="buildItemListRows">
								<xsl:with-param name="currencyCulture" select="$currencyCulture"/>
								<xsl:with-param name="itemListRowId" select="PaymentAllocationId"/>
								<xsl:with-param name="lockingTS" select="PaymentAllocationLockingTS"/>
								<xsl:with-param name="rowNumber" select="$rowNumber"/>
								<xsl:with-param name="screenType" select="$screenType"/>
								<xsl:with-param name="actionButton" select="$actionButton"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="/page/content/SuspenseList/SuspenseRecords/SuspenseRecord">
							<xsl:variable name="rowNumber">
								<xsl:value-of select="position()"/>
							</xsl:variable>
							<xsl:call-template name="buildItemListRows">
								<xsl:with-param name="currencyCulture" select="$currencyCulture"/>
								<xsl:with-param name="itemListRowId" select="SuspenseId"/>
								<xsl:with-param name="lockingTS" select="SuspenseLockingTS"/>
								<xsl:with-param name="rowNumber" select="$rowNumber"/>
								<xsl:with-param name="screenType" select="$screenType"/>
								<xsl:with-param name="actionButton" select="$actionButton"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
			</table>
			<div id="totalSection" class="displayTblFooter">
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">actionAmountTotalId</xsl:with-param>
					<xsl:with-param name="name">actionAmountTotal</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="/page/content/TotalInfo/TotalAmounts/Processed"/>
					</xsl:with-param>
				</xsl:call-template>
				<div id="displayTotalAmount">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2',/page/content/TotalInfo/TotalAmounts/Processed)"/>
				</div>
				<div id="displayTotalTextId">
					<xsl:value-of select="$totalText"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('Divide')"/>
					<xsl:text> </xsl:text>
				</div>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">amountRemainingTotalId</xsl:with-param>
					<xsl:with-param name="name">amountRemainingTotal</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="/page/content/TotalInfo/TotalAmounts/Remaining"/>
					</xsl:with-param>
				</xsl:call-template>
				<div id="displayTotalRemAmount">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/TotalInfo/TotalAmounts/Remaining)"/>
				</div>
				<div id="displayTotalRemainingTextId">
					<xsl:value-of select="xslNsODExt:getDictRes('Remaining')"/>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildItemListRows">
		<xsl:param name="currencyCulture"/>
		<xsl:param name="rowNumber"/>
		<xsl:param name="itemListRowId"/>
		<xsl:param name="lockingTS"/>
		<xsl:param name="screenType"/>
		<xsl:param name="actionButton"/>
		<tr class="displayTblRow">
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenItemId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenItemIdName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemListRowId"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:value-of select="Direction"/>
			</td>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenAmountIdName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="Amount"/>
					</xsl:with-param>
				</xsl:call-template>
				<span>
					<xsl:attribute name="id">
						<xsl:text>_displayAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:attribute>
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', Amount)"/>
				</span>
				<!-- Capturing ProcessingOrgUnitCode-->
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessingOrgUnitCodeId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessingOrgUnitCodeName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="ProcessingOrgUnitCode"/>
					</xsl:with-param>
				</xsl:call-template>
				<!-- Capturing RefundMethodCode-->
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenRefundMethodCodeId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenRefundMethodCodeName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="RefundMethodCode"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<xsl:if test="$screenType!='suspend'">
				<td>
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">
							<xsl:text>_hiddenDisbursementGroupId_</xsl:text>
							<xsl:value-of select="$rowNumber"/>
						</xsl:with-param>
						<xsl:with-param name="name">
							<xsl:text>_hiddenDisbursementGroupName</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="DisbursementGroup"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:value-of select="DisbursementGroup"/>
				</td>
			</xsl:if>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="Processed"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="displayAmount" select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', Processed)"/>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">
						<xsl:text>_processAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_processAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="type">text</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsExt:replaceText($displayAmount,',','')"/>
					</xsl:with-param>
					<xsl:with-param name="controlClass">processAmountFloatField</xsl:with-param>
					<xsl:with-param name="extraConfigItems">
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', $currencyCulture)"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctNumberMax</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="Amount"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctNumberMin</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>0.00</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctRowNumber</xsl:with-param>
							<xsl:with-param name="type">number</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$rowNumber"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctTotalAvailableField</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>actionAmountTotalId</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctTotalRemainingField</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>amountRemainingTotalId</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctActionButton</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$actionButton"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:choose>
							<xsl:when test="$screenType='suspend'">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctHoldCheckbox</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>_processHoldId_</xsl:text>
										<xsl:value-of select="$rowNumber"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctHiddenCheckbox</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>_hiddenHoldId_</xsl:text>
										<xsl:value-of select="$rowNumber"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctReasonField</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>billingSuspendReasonField</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$screenType='transfer'">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctReasonField</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>billingTransferReason</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="$screenType='writeoff'">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctReasonField</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>billingWriteOffSuspenseActionField</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
						</xsl:choose>
					</xsl:with-param>
				</xsl:call-template>
			</td>

			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenRemAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenRemAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="Remaining"/>
					</xsl:with-param>
				</xsl:call-template>
				<span>
					<xsl:attribute name="id">
						<xsl:text>_displayRemAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:attribute>
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', Remaining)"/>
				</span>
			</td>
			<xsl:if test="$screenType='refundToSource'">
				<td>
					<!-- Added the Checkbox for "Refund to Check" here-->
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">
							<xsl:text>_hiddenRefundMethodId_</xsl:text>
							<xsl:value-of select="$rowNumber"/>
						</xsl:with-param>
						<xsl:with-param name="name">
							<xsl:text>_hiddenRefundMethodName</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="value">0</xsl:with-param>
					</xsl:call-template>


					<xsl:if test="/page/content/ExpressCache/Account/AccountConfig/Disbursements/@RefundToSource='1'">
						<xsl:if test="/page/content/SuspenseList/SuspenseRecords/SuspenseRecord/RefundMethodCode != 'CK'">
							<xsl:choose>
								<xsl:when test="($canRefundToCheck or $canAccessAllAreas)">
									<xsl:call-template name="buildSystemControl">
										<xsl:with-param name="type">checkbox</xsl:with-param>
										<xsl:with-param name="id">
											<xsl:text>refundToCheckBoxGroup</xsl:text>
											<xsl:value-of select="$rowNumber"/>
										</xsl:with-param>
										<xsl:with-param name="checkbox">
											<xsl:call-template name="buildCheckBoxObject">
												<xsl:with-param name="fieldID">
													<xsl:text>_processRefundMethodId_</xsl:text>
													<xsl:value-of select="$rowNumber"/>
												</xsl:with-param>
												<xsl:with-param name="name">
													<xsl:text>_processRefundMethodName</xsl:text>
												</xsl:with-param>
												<xsl:with-param name="inputValue">
													<xsl:text>0</xsl:text>
												</xsl:with-param>
												<xsl:with-param name="dctClassName">dctbillingholdcheckboxfield</xsl:with-param>
												<xsl:with-param name="boxLabel">
													<xsl:value-of select="xslNsODExt:getDictRes('RefundToCheck')"/>
												</xsl:with-param>
												<xsl:with-param name="checked">
													<xsl:text>false</xsl:text>
												</xsl:with-param>
												<xsl:with-param name="extraConfigItems">
													<xsl:call-template name="addConfigProperty">
														<xsl:with-param name="name">dctHiddenField</xsl:with-param>
														<xsl:with-param name="type">string</xsl:with-param>
														<xsl:with-param name="value">
															<xsl:text>_hiddenRefundMethodId_</xsl:text>
															<xsl:value-of select="$rowNumber"/>
														</xsl:with-param>
													</xsl:call-template>
												</xsl:with-param>
											</xsl:call-template>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:when>
							</xsl:choose>
						</xsl:if>
					</xsl:if>
				</td>
			</xsl:if>
			<xsl:if test="$screenType='suspend'">
				<td>
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">
							<xsl:text>_hiddenHoldId_</xsl:text>
							<xsl:value-of select="$rowNumber"/>
						</xsl:with-param>
						<xsl:with-param name="name">
							<xsl:text>_hiddenHoldName</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="value">0</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="type">checkbox</xsl:with-param>
						<xsl:with-param name="id">
							<xsl:text>processHoldCheckBoxGroup</xsl:text>
							<xsl:value-of select="$rowNumber"/>
						</xsl:with-param>
						<xsl:with-param name="checkbox">
							<xsl:call-template name="buildCheckBoxObject">
								<xsl:with-param name="fieldID">
									<xsl:text>_processHoldId_</xsl:text>
									<xsl:value-of select="$rowNumber"/>
								</xsl:with-param>
								<xsl:with-param name="name">
									<xsl:text>_processHoldName</xsl:text>
								</xsl:with-param>
								<xsl:with-param name="inputValue">
									<xsl:text>0</xsl:text>
								</xsl:with-param>
								<xsl:with-param name="dctClassName">dctbillingholdcheckboxfield</xsl:with-param>
								<xsl:with-param name="boxLabel">
									<xsl:text>false</xsl:text>
								</xsl:with-param>
								<xsl:with-param name="checked">
									<xsl:text>false</xsl:text>
								</xsl:with-param>
								<xsl:with-param name="extraConfigItems">
									<xsl:call-template name="addConfigProperty">
										<xsl:with-param name="name">dctHiddenField</xsl:with-param>
										<xsl:with-param name="type">string</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:text>_hiddenHoldId_</xsl:text>
											<xsl:value-of select="$rowNumber"/>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</td>
			</xsl:if>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenLockingTSId</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenLockingTSName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$lockingTS"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="buildCommonAccountContent">
		<xsl:param name="accountType"/>
		<xsl:param name="screenType"/>
		<xsl:call-template name="buildBillingActiveSection">
			<xsl:with-param name="accountType" select="$accountType"/>
		</xsl:call-template>
		<xsl:call-template name="createSubMenuArea"/>
		<div id="accountInformationArea">
			<div id="accountInformationId" class="x-hidden">
				<xsl:element name="div">
					<xsl:attribute name="class">collapsibleGroupPanel</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>renderTo: "accountInformationId",</xsl:text>
						<xsl:text>id: "accountInformationGroup"</xsl:text>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
				</xsl:element>

				<xsl:choose>
					<xsl:when test="$accountType='AGT'">
						<xsl:call-template name="billingAgencyLedgerWidget">
							<xsl:with-param name="id">agencyLedger</xsl:with-param>
							<xsl:with-param name="title">
								<xsl:value-of select="xslNsODExt:getDictRes('AgencyLedger')"/>
							</xsl:with-param>
						</xsl:call-template>
						<!--<xsl:call-template name="billingAgencyProgramsWidget"/> -->
						<xsl:call-template name="billingAgencyDetailsWidget">
							<xsl:with-param name="id">agencyDetails</xsl:with-param>
							<xsl:with-param name="title">
								<xsl:value-of select="xslNsODExt:getDictRes('AgencyDetails')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$accountType='COL2'">
						<xsl:call-template name="billingAgencyLedgerWidget">
							<xsl:with-param name="id">collectionsLedger</xsl:with-param>
							<xsl:with-param name="title">
								<xsl:value-of select="xslNsODExt:getDictRes('CollectionsLedger')"/>
							</xsl:with-param>
						</xsl:call-template>
						<!--<xsl:call-template name="billingAgencyProgramsWidget"/> -->
						<xsl:call-template name="billingAgencyDetailsWidget">
							<xsl:with-param name="id">collectionsDetails</xsl:with-param>
							<xsl:with-param name="title">
								<xsl:value-of select="xslNsODExt:getDictRes('CollectionsDetails')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$screenType='installmentSchedule'">
							<xsl:call-template name="buildScheduleHistoryEvents"/>
						</xsl:if>
						<xsl:call-template name="billingLedgerWidget">
							<xsl:with-param name="accountType" select="$accountType"/>
						</xsl:call-template>
						<xsl:if test="$accountType!='COLL'">
							<xsl:call-template name="billingDetailsWidget">
								<xsl:with-param name="screenType" select="$screenType"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:call-template name="billingContactsWidget">
							<xsl:with-param name="screenType" select="$screenType"/>
							<xsl:with-param name="accountType" select="$accountType"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="allocationTableWidget">
		<xsl:param name="totalText"/>
		<xsl:param name="actionButton"/>
		<xsl:param name="titleText"/>
		<xsl:param name="screenType"/>
		<xsl:param name="totalRemaining"/>
		<xsl:variable name="currencyCulture">
			<xsl:value-of select="/page/content/ManualPaymentAllocationRecord/CurrencyCulture"/>
		</xsl:variable>
		<xsl:variable name="isAgencyLite">
			<xsl:value-of select="not(/page/content/ExpressCache/Account/AccountTypeCode[text()!='AL'])"/>
		</xsl:variable>
		<xsl:variable name="isNet">
			<xsl:value-of select="not(/page/content/ExpressCache/Account/AccountConfig/Payment[@GrNetInd != 'N'])"/>
		</xsl:variable>
		<div id="itemListDetail" tabletype="acrossAboveOnce">
			<xsl:call-template name="buildHiddenInput">
				<xsl:with-param name="id">
					<xsl:text>_hiddenDisplayAmtMask</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="name">
					<xsl:text>_hiddenDisplayAmtMask</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="xslNsExt:numberFormatMaskToUse('-$???????????.00', $currencyCulture)"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:call-template name="buildHiddenInput">
				<xsl:with-param name="id">
					<xsl:text>_hiddenAgencyLiteInd</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="name">
					<xsl:text>_hiddenAgencyLiteInd</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$isAgencyLite"/>
				</xsl:with-param>
			</xsl:call-template>
			<div class="displayTblTitle">
				<xsl:choose>
					<xsl:when test="$screenType='allocateSuspensePayment'">
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2',$totalRemaining)"/>
						<xsl:text> </xsl:text>
						<xsl:value-of select="$titleText"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$titleText"/>
					</xsl:otherwise>
				</xsl:choose>
			</div>
			<table id="tblItemListDetail" class="displayTbl">
				<tr class="displayTblHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Direction')"/>
					</td>
					<xsl:choose>
						<xsl:when test="$isAgencyLite='true' and $isNet='true'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentNetDue')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('PastNetDue')"/>
							</td>
						</xsl:when>
						<xsl:when test="$isAgencyLite='true' and $isNet='false'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentGrossDue')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('PastGrossDue')"/>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentDue')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('PastDue')"/>
							</td>
						</xsl:otherwise>
					</xsl:choose>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('OutstandingBalance')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('SuspenseState')"/>
					</td>
				</tr>

				<xsl:for-each select="/page/content/ManualPaymentAllocationRecord/PolicyTerms/PolicyTerm">
					<xsl:variable name="rowNumber">
						<xsl:value-of select="position()"/>
					</xsl:variable>
					<xsl:variable name="allowPayAlloc">
						<xsl:value-of select="PolicyTermExtendedData/AllowPaymentAllocation"/>
					</xsl:variable>
					<xsl:call-template name="buildAllocationItemListRows">
						<xsl:with-param name="currencyCulture" select="$currencyCulture"/>
						<xsl:with-param name="itemListRowId" select="PolicyReference"/>
						<xsl:with-param name="rowNumber" select="$rowNumber"/>
						<xsl:with-param name="actionButton" select="$actionButton"/>
						<xsl:with-param name="direction" select="PolicyReference"/>
						<xsl:with-param name="accountRow">
							<xsl:text>false</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="screenType" select="$screenType"/>
						<xsl:with-param name="defaultAmount">
							<xsl:choose>
								<xsl:when test="$isAgencyLite='true'">

									<xsl:choose>
										<xsl:when test="$isNet='true'">
											<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'f2', CurrentDueNetAmount + PastDueNetAmount)"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'f2', CurrentDueAmount + PastDueAmount)"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>0.00</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="currentDue">
							<xsl:choose>
								<xsl:when test="$isAgencyLite='true' and $isNet='true'">
									<xsl:value-of select="CurrentDueNetAmount"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="CurrentDueAmount"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="pastDue">
							<xsl:choose>
								<xsl:when test="$isAgencyLite='true' and $isNet='true'">
									<xsl:value-of select="PastDueNetAmount"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="PastDueAmount"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="allowPaymentAllocation" select="$allowPayAlloc"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each select="/page/content/ManualPaymentAllocationRecord">
					<xsl:variable name="rowNumber">
						<xsl:value-of select="count(/page/content/ManualPaymentAllocationRecord/PolicyTerms/PolicyTerm) + number(1)"/>
					</xsl:variable>
					<xsl:call-template name="buildAllocationItemListRows">
						<xsl:with-param name="currencyCulture" select="$currencyCulture"/>
						<xsl:with-param name="rowNumber" select="$rowNumber"/>
						<xsl:with-param name="actionButton" select="$actionButton"/>
						<xsl:with-param name="direction">
							<xsl:text>Account Level</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="accountRow">
							<xsl:text>true</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="screenType" select="$screenType"/>
					</xsl:call-template>
				</xsl:for-each>
			</table>
			<div id="totalSection" class="displayTblFooter">
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">totalSuspenseAmtId</xsl:with-param>
					<xsl:with-param name="name">totalSuspenseAmt</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$totalRemaining"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">amountAllocatedTotalId</xsl:with-param>
					<xsl:with-param name="name">amountAllocatedTotal</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:text>0.00</xsl:text>
					</xsl:with-param>
				</xsl:call-template>
				<div id="displayTotalAmount">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2','0.00')"/>
				</div>
				<div id="displayTotalTextId">
					<xsl:value-of select="xslNsODExt:getDictRes('Allocated')"/>
					<xsl:text> / </xsl:text>
				</div>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">amountRemainingTotalId</xsl:with-param>
					<xsl:with-param name="name">amountRemainingTotal</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$totalRemaining"/>
					</xsl:with-param>
				</xsl:call-template>
				<div id="displayTotalRemAmount">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $totalRemaining)"/>
				</div>
				<div id="displayTotalRemainingTextId">
					<xsl:value-of select="xslNsODExt:getDictRes('Remaining')"/>
				</div>
			</div>
		</div>
		<div id="messageNote">
			<xsl:value-of select="xslNsODExt:getDictRes('RememberBalanceOnHoldSuspense')"/>
		</div>
	</xsl:template>
	<xsl:template name="buildAllocationItemListRows">
		<xsl:param name="currencyCulture"/>
		<xsl:param name="rowNumber"/>
		<xsl:param name="itemListRowId"/>
		<xsl:param name="screenType"/>
		<xsl:param name="actionButton"/>
		<xsl:param name="direction"/>
		<xsl:param name="accountRow"/>
		<xsl:param name="defaultAmount" select="0.00"/>
		<xsl:param name="currentDue" select="CurrentDueAmount"/>
		<xsl:param name="pastDue" select="PastDueAmount"/>
		<xsl:param name="allowPaymentAllocation"/>
		<tr class="displayTblRow">
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$defaultAmount"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessDefaultAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessDefaultAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$defaultAmount"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:choose>
					<xsl:when test="$allowPaymentAllocation='0'">
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $defaultAmount)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:text>_processAmountId_</xsl:text>
								<xsl:value-of select="$rowNumber"/>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:text>_processAmountName</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="type">text</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $defaultAmount)"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">allocationAmountFloatField</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', $currencyCulture)"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:if test="number(OutstandingBalance) &gt; 0">
									<xsl:call-template name="addConfigProperty">
										<xsl:with-param name="name">dctNumberMax</xsl:with-param>
										<xsl:with-param name="type">string</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="OutstandingBalance"/>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:if>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctNumberMin</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>0.00</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctRowNumber</xsl:with-param>
									<xsl:with-param name="type">number</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$rowNumber"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenItemId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenItemIdName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemListRowId"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:value-of select="$direction"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $currentDue)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $pastDue)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', OutstandingBalance)"/>
			</td>
			<td>
				<xsl:choose>
					<xsl:when test="$accountRow='true'">
						<xsl:call-template name="buildHiddenInput">
							<xsl:with-param name="name">
								<xsl:text>_suspsenseState</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='BIL_SUSPHOLD']/ListEntry[@Code='N']/@Code"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='BIL_SUSPHOLD']/ListEntry[@Code='N']/@Description"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="OutstandingBalance" select="OutstandingBalance"/>
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:text>_suspsenseState</xsl:text>
								<xsl:value-of select="$rowNumber"/>
							</xsl:with-param>
							<xsl:with-param name="name">_suspsenseState</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_SUSPHOLD']/ListEntry">
									<xsl:choose>
										<xsl:when test="number($OutstandingBalance)=0 and (@Code='M' or @Code='P')">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:when>
										<xsl:when test="number($OutstandingBalance)&gt;0 and (@Code='M' or @Code='N')">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:choose>
									<xsl:when test="number($OutstandingBalance)=0">
										<xsl:text>M</xsl:text>
									</xsl:when>
									<xsl:when test="number($OutstandingBalance)&gt;0">
										<xsl:text>N</xsl:text>
									</xsl:when>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="width">100</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="notesWidget">
		<div id="supportIcons">
			<xsl:call-template name="makeButton">
				<xsl:with-param name="name">addNotes</xsl:with-param>
				<xsl:with-param name="id">addNotes</xsl:with-param>
				<xsl:with-param name="onclick">DCT.Util.displayNotesPopUp();</xsl:with-param>
				<xsl:with-param name="caption">
					<xsl:value-of select="xslNsODExt:getDictRes('AddNote')"/>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildBatchDetail">
		<xsl:param name="screen"/>
		<div id="billingPaymentDetail">
			<div class="displayTblTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('BatchDetails')"/>
			</div>
			<table id="tblBillingBatchDetail" class="displayTbl">
				<tr class="displayTblHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('ControlTotal')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('DateEntered')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('CreatedBy')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Description')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('ProcessingOrgUnit')"/>
					</td>
				</tr>
				<tr class="displayTblRow">
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/ExpressCache/PaymentBatch/CurrencyCulture, 'c2', /page/content/ExpressCache/PaymentBatch/ControlTotal)"/>
					</td>
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/ExpressCache/PaymentBatch/EntryDate)"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/ExpressCache/PaymentBatch/EntryUserId"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/ExpressCache/PaymentBatch/Description"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/ExpressCache/PaymentBatch/ProcessingOrgUnit"/>
					</td>
				</tr>
			</table>
		</div>
		<div id="batchTotal" class="displayTblFooter">
			<xsl:value-of select="xslNsODExt:getDictRes('CurrentBatchTotal')"/>
			<xsl:choose>
				<xsl:when test="number(/page/content/ExpressCache/PaymentBatch/BatchTotal)&gt;0">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/ExpressCache/PaymentBatch/CurrencyCulture, 'c2', /page/content/ExpressCache/PaymentBatch/BatchTotal)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/ExpressCache/PaymentBatch/CurrencyCulture, 'c2', '0.00')"/>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<div class="g-btn-bar">
			<xsl:if test="$screen='batchPaymentList'">
				<xsl:if test="/page/content/ExpressCache/PaymentBatch/StatusCode='E' or /page/content/ExpressCache/PaymentBatch/StatusCode='O'">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">updateBatchDetails</xsl:with-param>
						<xsl:with-param name="id">updateBatchDetails</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('editPaymentBatch');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('EditBatchDetails')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$screen='batchPayment'">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">viewBatch</xsl:with-param>
						<xsl:with-param name="id">viewBatch</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('batchPaymentList');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewBatchPaymentList')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$screen='batchPaymentList'">
					<xsl:if test="/page/content/ExpressCache/PaymentBatch/StatusCode='E' or /page/content/ExpressCache/PaymentBatch/StatusCode='O'">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">addPayment</xsl:with-param>
							<xsl:with-param name="anchorId">addPayment</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('batchPayment');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('AddAnotherPayment')"/>
							</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
				</xsl:when>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildBatchDetailFields">
		<xsl:param name="screen"/>
		<div id="batchDetailFieldGroup">
			<div id="batchDetailInformation" class="downLayout">
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('BatchNumber')"/>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="$screen='newPaymentBatch'">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">batchNbr</xsl:with-param>
									<xsl:with-param name="name">_batchNbr</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingBatchNbrTextField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctTargetPage</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>batchPayment</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctAction</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckEvent</xsl:with-param>
											<xsl:with-param name="type">boolean</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>false</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckField</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>paymentMethod</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">batchNbr</xsl:with-param>
									<xsl:with-param name="name">_batchNbr</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/PaymentBatch/BatchNumber"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingTextField</xsl:with-param>
									<xsl:with-param name="disabled">true</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('ControlTotal_colon')"/>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="$screen='newPaymentBatch'">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">controlTotal</xsl:with-param>
									<xsl:with-param name="name">_controlTotal</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingControlNbrFloatField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', /page/content/DefaultCultureCode/@value)"/>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctNumberMin</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>0.00</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctTargetPage</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>batchPayment</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctAction</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckEvent</xsl:with-param>
											<xsl:with-param name="type">boolean</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>false</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctHiddenControlTotal</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>_hiddenControlTotal</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">controlTotal</xsl:with-param>
									<xsl:with-param name="name">_controlTotal</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/PaymentBatch/CurrencyCulture, '$-??????????#.00', /page/content/PaymentBatch/ControlTotal)"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingEditControlFloatField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', /page/content/PaymentBatch/CurrencyCulture)"/>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctNumberMin</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>0.00</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctHiddenControlTotal</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>_hiddenControlTotal</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('DefaultPaymentMethod')"/>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="$screen='newPaymentBatch' or $screen='editPaymentBatch'">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">paymentMethod</xsl:with-param>
									<xsl:with-param name="name">_paymentMethod</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option>
											<xsl:attribute name="value">
												<xsl:text></xsl:text>
											</xsl:attribute>
											<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
										</option>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTMETHOD']/ListEntry">
											<xsl:if test="@Code='CK' or @Code='CS'">
												<option>
													<xsl:attribute name="value">
														<xsl:value-of select="@Code"/>
													</xsl:attribute>
													<xsl:value-of select="@Description"/>
												</option>
											</xsl:if>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>CK</xsl:text>
									</xsl:with-param>
									<xsl:with-param name="controlClass">batchPaymentMethodComboField</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="width">180</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">paymentMethod</xsl:with-param>
									<xsl:with-param name="name">_paymentMethod</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option>
											<xsl:attribute name="value">
												<xsl:text></xsl:text>
											</xsl:attribute>
											<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
										</option>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTMETHOD']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>updateBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/PaymentBatch/DefaultPaymentMethodCode"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicBlankCheckComboField</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="width">180</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('ProcessingOrgUnit')"/>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="$screen='newPaymentBatch'">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">processingOrgUnit</xsl:with-param>
									<xsl:with-param name="name">_processingOrgUnit</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option>
											<xsl:attribute name="value">
												<xsl:text></xsl:text>
											</xsl:attribute>
											<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
										</option>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PROCESSINGBRANCH']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<!--<xsl:with-param name="value">
                    <xsl:text>CK</xsl:text>
                  </xsl:with-param>-->
									<xsl:with-param name="controlClass">batchProcessingOrgUnitComboField</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="width">180</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">processingOrgUnit</xsl:with-param>
									<xsl:with-param name="name">_processingOrgUnit</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<option>
											<xsl:attribute name="value">
												<xsl:text></xsl:text>
											</xsl:attribute>
											<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
										</option>
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PROCESSINGBRANCH']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="watermark">
										<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>updateBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/PaymentBatch/ProcessingOrgUnitCode"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicBlankCheckComboField</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
									<xsl:with-param name="width">180</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
				<div class="downFieldGroup">
					<div class="downFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Description_colon')"/>
					</div>
					<div class="downFormField">
						<xsl:choose>
							<xsl:when test="$screen='newPaymentBatch'">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">description</xsl:with-param>
									<xsl:with-param name="name">_description</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text></xsl:text>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingEnterTextField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctTargetPage</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>batchPayment</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctAction</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>createBatch</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckEvent</xsl:with-param>
											<xsl:with-param name="type">boolean</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>false</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">description</xsl:with-param>
									<xsl:with-param name="name">_description</xsl:with-param>
									<xsl:with-param name="type">text</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="/page/content/PaymentBatch/Description"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">billingDescriptionTextField</xsl:with-param>
									<xsl:with-param name="size">50</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</div>
			<div id="createBatchButton" class="g-btn-bar">
				<xsl:choose>
					<xsl:when test="$screen='newPaymentBatch'">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">createBatch</xsl:with-param>
							<xsl:with-param name="id">createBatch</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('batchPayment', 'createBatch', false, 'createBatch');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Create')"/>
							</xsl:with-param>
							<xsl:with-param name="class">btnDisabled</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">cancelBatch</xsl:with-param>
							<xsl:with-param name="id">cancelBatch</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('cashManagement');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
							</xsl:with-param>
							<xsl:with-param name="type">cancel</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">updateBatch</xsl:with-param>
							<xsl:with-param name="id">updateBatch</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('batchPaymentList', 'editBatch', false, 'updateBatch');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Update')"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">cancelBatch</xsl:with-param>
							<xsl:with-param name="id">cancelBatch</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('batchPaymentList');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
							</xsl:with-param>
							<xsl:with-param name="type">cancel</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildAccountSearchNotification">
		<xsl:choose>
			<xsl:when test="number(/page/content/NoData/ResponseInfo/ReturnCode)=0">
				<xsl:call-template name="buildAccountNotFound"/>
			</xsl:when>
			<xsl:when test="number(/page/content/CandidateList/SearchControl/Paging/ReturnCount)=0">
				<xsl:call-template name="buildAccountNotFound"/>
			</xsl:when>
			<xsl:when test="number(/page/content/AccountRecord/ResponseInfo/ReturnCode)&gt;0">
				<xsl:call-template name="buildAccountFound"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildAccountNotFound">
		<div id="accountNotFound" class="notificationDiv nWarn">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="xslNsODExt:getDictRes('NoAccountsMatch_EnterAgain')"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildAccountFound">
		<div id="accountFound" class="notificationDiv">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="xslNsODExt:getDictRes('SearchSuccessful_Continue')"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildPaymentOptionsRadioGroup">
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="defaultUnidentifiedAccount"/>
		<xsl:param name="accountId"/>
		<xsl:param name="accountReference"/>
		<xsl:param name="policyReference"/>
		<xsl:param name="displayUnidentifiedGroup"/>
		<xsl:param name="unidentifiedLastName"/>
		<xsl:param name="unidentifiedFirstName"/>
		<xsl:param name="unidentifiedMiddleName"/>
		<xsl:param name="unidentifiedAccountReference"/>
		<xsl:param name="unidentifiedPolicyReference"/>
		<xsl:param name="unidentifiedOtherReference"/>
		<xsl:param name="unidentifiedPaymentProcessing"/>
		<div id="paymentReferenceGroup">
			<h2>
				<xsl:choose>
					<xsl:when test="$screen='batchPayment' or $screen='editBatchPayment'">
						<xsl:value-of select="xslNsODExt:getDictRes('Reference')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsODExt:getDictRes('PaymentReference')"/>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
			<div id="paymentOptionsRadioGroup">
				<div id="paymentRadioGroupContainerId" class="radioGroupContainer">
					<div id="paymentOptionsAccount">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="type">radio</xsl:with-param>
							<xsl:with-param name="id">paymentOptionsRadiobuttons</xsl:with-param>
							<xsl:with-param name="columns">1</xsl:with-param>
							<xsl:with-param name="required">1</xsl:with-param>
							<xsl:with-param name="width">470</xsl:with-param>
							<xsl:with-param name="radiobuttons">
								<xsl:text>{</xsl:text>
								<xsl:call-template name="buildSystemRadioButtons">
									<xsl:with-param name="fieldID">accountPolicyName</xsl:with-param>
									<xsl:with-param name="name">_referenceOption</xsl:with-param>
									<xsl:with-param name="value">accountPolicyName</xsl:with-param>
									<xsl:with-param name="dctClassName">
										<xsl:choose>
											<xsl:when test="$unidentifiedPaymentProcessing='true'">
												<xsl:text>dctbillingunidentifiedpaymentradiofield</xsl:text>
											</xsl:when>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:text>dctbillingpaymentradiofield</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>dctbillingeditpaymentradiofield</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="checked">
										<xsl:choose>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:text>true</xsl:text>
											</xsl:when>
											<xsl:when test="$processType!='unidentified'">
												<xsl:text>true</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>false</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="label">
										<xsl:value-of select="xslNsODExt:getDictRes('IKnowAcctPolNumName')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:choose>
											<xsl:when test="$unidentifiedPaymentProcessing='true'">
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctActionButton</xsl:with-param>
													<xsl:with-param name="type">string</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>paymentSave</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
													<xsl:with-param name="type">object</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>true</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>true</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctActionButton</xsl:with-param>
													<xsl:with-param name="type">string</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>paymentAction</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:text>},{</xsl:text>
								<xsl:call-template name="buildSystemRadioButtons">
									<xsl:with-param name="fieldID">unidentifiedReference</xsl:with-param>
									<xsl:with-param name="name">_referenceOption</xsl:with-param>
									<xsl:with-param name="value">unidentified</xsl:with-param>
									<xsl:with-param name="dctClassName">
										<xsl:choose>
											<xsl:when test="$unidentifiedPaymentProcessing='true'">
												<xsl:text>dctbillingunidentifiedpaymentradiofield</xsl:text>
											</xsl:when>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:text>dctbillingpaymentradiofield</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>dctbillingeditpaymentradiofield</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="checked">
										<xsl:choose>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:text>false</xsl:text>
											</xsl:when>
											<xsl:when test="$processType='unidentified'">
												<xsl:text>true</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>false</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="label">
										<xsl:value-of select="xslNsODExt:getDictRes('ThisIsUnidentifiedPaymentNotAssocd')"/>
									</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:choose>
											<xsl:when test="$unidentifiedPaymentProcessing='true'">
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctActionButton</xsl:with-param>
													<xsl:with-param name="type">string</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>paymentSave</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
													<xsl:with-param name="type">object</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>true</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>true</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctHidePaymentDetail</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>false</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctRemoveData</xsl:with-param>
													<xsl:with-param name="type">boolean</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>true</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
												<xsl:call-template name="addConfigProperty">
													<xsl:with-param name="name">dctActionButton</xsl:with-param>
													<xsl:with-param name="type">string</xsl:with-param>
													<xsl:with-param name="value">
														<xsl:text>paymentAction</xsl:text>
													</xsl:with-param>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:text>}</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="vertical">true</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<div id="radioOptionOutput">
					<div id="accountPolicyNameGroup" class="downLayout">
						<xsl:call-template name="buildAccountPolicyNameSearch">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="defaultUnidentifiedAccount" select="$defaultUnidentifiedAccount"/>
							<xsl:with-param name="unidentifiedAccountReference" select="$unidentifiedAccountReference"/>
						</xsl:call-template>
						<xsl:if test="((number(/page/content/AccountRecord/ResponseInfo/ReturnCode)&gt;0) or (normalize-space($accountId)!=''))">
							<xsl:call-template name="buildAccountPolicyGroup">
								<xsl:with-param name="screen" select="$screen"/>
								<xsl:with-param name="accountReference" select="$accountReference"/>
								<xsl:with-param name="policyReference" select="$policyReference"/>
								<xsl:with-param name="unidentifiedPaymentProcessing" select="$unidentifiedPaymentProcessing"/>
							</xsl:call-template>
						</xsl:if>
					</div>
					<div id="unidentifiedGroup">
						<xsl:call-template name="buildUnidentifiedFields">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="displayUnidentifiedGroup" select="$displayUnidentifiedGroup"/>
							<xsl:with-param name="unidentifiedLastName" select="$unidentifiedLastName"/>
							<xsl:with-param name="unidentifiedFirstName" select="$unidentifiedFirstName"/>
							<xsl:with-param name="unidentifiedMiddleName" select="$unidentifiedMiddleName"/>
							<xsl:with-param name="unidentifiedAccountReference" select="$unidentifiedAccountReference"/>
							<xsl:with-param name="unidentifiedPolicyReference" select="$unidentifiedPolicyReference"/>
							<xsl:with-param name="unidentifiedOtherReference" select="$unidentifiedOtherReference"/>
						</xsl:call-template>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildAccountPolicyNameSearch">
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="defaultUnidentifiedAccount"/>
		<xsl:param name="unidentifiedAccountReference"/>
		<div id="accountPolicyNameSearchId" class="downLayout">
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('SearchForAccountBy')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">searchByOption</xsl:with-param>
						<xsl:with-param name="name">_searchByOption</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text>account</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('AccountNumber')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:text>policy</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyOrArrangementNumber')"/>
							</option>
							<option>
								<xsl:attribute name="value">
									<xsl:text>party</xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('PartyInformation')"/>
							</option>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' and /page/content/ExpressCache/CachedRequestForm/_searchByOption='account'">
									<xsl:text>account</xsl:text>
								</xsl:when>
								<xsl:when test="($processType='acctNotFound') and (/page/content/ExpressCache/CachedRequestForm/_searchByOption='policy')">
									<xsl:text>policy</xsl:text>
								</xsl:when>
								<xsl:when test="($processType='acctNotFound') and (/page/content/ExpressCache/CachedRequestForm/_searchByOption='party')">
									<xsl:text>party</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>account</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">accountSearchComboField</xsl:with-param>
						<xsl:with-param name="width">160</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="accountSearchByField" class="acrossLayout">
				<div id="searchNumberGroup" class="downFieldGroup">
					<div class="downBeforeFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('EnterNumber')"/>
					</div>
					<div class="downFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">searchByNbr</xsl:with-param>
							<xsl:with-param name="name">_searchByNbr</xsl:with-param>
							<xsl:with-param name="type">text</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:choose>
									<xsl:when test="$processType='acctNotFound'">
										<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_searchByNbr"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if test="$defaultUnidentifiedAccount='true'">
											<xsl:value-of select="$unidentifiedAccountReference"/>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="controlClass">billingSearchByTextField</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctTargetPage</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$screen"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctSearchTypeField</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>searchByOption</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="id">searchForAccountButton</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.submitReferenceSearch(Ext.getCmp('searchByOption').getValue(), '</xsl:text>
						<xsl:value-of select="$screen"/>
						<xsl:text>');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
					</xsl:with-param>
					<xsl:with-param name="type">search</xsl:with-param>
					<xsl:with-param name="class">downFormField</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildAccountPolicyGroup">
		<xsl:param name="screen"/>
		<xsl:param name="accountReference"/>
		<xsl:param name="policyReference"/>
		<xsl:param name="unidentifiedPaymentProcessing"/>
		<div id="accountPolicyReferenceId">
			<table id="accountPolicyReferenceTbl">
				<tr class="accountPolicyReferenceTblRow">
					<td class="accountPolicyReferenceDataColumn">
						<div class="labelItem">
							<xsl:value-of select="xslNsODExt:getDictRes('AccountReference_colon')"/>
						</div>
					</td>
					<td>
						<div class="dataItem">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/AccountRecord/AccountReference)!=''">
									<xsl:value-of select="/page/content/AccountRecord/AccountReference"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$accountReference"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyReference)!=''">
					<tr class="accountPolicyReferenceTblRow">
						<td class="accountPolicyReferenceDataColumn">
							<div class="acrossFormLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyReference_colon')"/>
							</div>
						</td>
						<td>
							<div class="dataItem">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyReference)!=''">
										<xsl:value-of select="/page/content/ExpressCache/PolicyReference"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="$policyReference"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</xsl:if>
			</table>
			<div id="selectAnotherAccount">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="id">selectAccount</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:choose>
							<xsl:when test="$unidentifiedPaymentProcessing='true'">
								<xsl:text>DCT.Util.showHideReferenceSearchFields(true);DCT.Util.removeReferenceFields(true);DCT.Util.removefield('paymentAction');DCT.Util.removefield('placeOnHoldGroup');</xsl:text>
							</xsl:when>
							<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
								<xsl:text>DCT.Util.showHideReferenceSearchFields(true);DCT.Util.removeReferenceFields(true);</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DCT.Util.showHideReferenceFields('changeAcct');DCT.Util.removeReferenceFields(true);DCT.Util.enableDisableActionButton(true, 'paymentAction');</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('SelectAnotherReference')"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildUnidentifiedFields">
		<xsl:param name="screen"/>
		<xsl:param name="displayUnidentifiedGroup"/>
		<xsl:param name="processType"/>
		<xsl:param name="unidentifiedLastName"/>
		<xsl:param name="unidentifiedFirstName"/>
		<xsl:param name="unidentifiedMiddleName"/>
		<xsl:param name="unidentifiedAccountReference"/>
		<xsl:param name="unidentifiedPolicyReference"/>
		<xsl:param name="unidentifiedOtherReference"/>
		<div id="unidentifiedFieldsId">
			<xsl:attribute name="class">
				<xsl:text>downLayout</xsl:text>
				<xsl:if test="$displayUnidentifiedGroup='false'">
					<xsl:text> hideOffset</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<div class="instText">
				<xsl:value-of select="xslNsODExt:getDictRes('EnterOneOfFollowingInformation')"/>
			</div>
			<div id="lastnameGroup" class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('LastName_colon')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedLastname</xsl:with-param>
						<xsl:with-param name="name">_unidentifedLastname</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedLastName"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text>billingPaymentShowTextField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='batchPayment' or $screen='createPayment' or 'unidentifiedPaymentsDetail'">
											<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>DCT.Util.checkIfReferenceAndPaymentFieldsBlank</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='unidentifiedPaymentsDetail'">
											<xsl:text>paymentSave</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>paymentAction</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">35</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="firstnameGroup" class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('FirstName_colon')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedFirstname</xsl:with-param>
						<xsl:with-param name="name">_unidentifedFirstname</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedFirstName"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text>billingPaymentShowTextField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='batchPayment' or $screen='createPayment' or 'unidentifiedPaymentsDetail'">
											<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>DCT.Util.checkIfReferenceAndPaymentFieldsBlank</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='unidentifiedPaymentsDetail'">
											<xsl:text>paymentSave</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>paymentAction</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">30</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div id="middlenameGroup" class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('Middle')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedMiddlename</xsl:with-param>
						<xsl:with-param name="name">_unidentifedMiddlename</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedMiddleName"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="size">25</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('AccountReference_colon')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedAccount</xsl:with-param>
						<xsl:with-param name="name">_unidentifedAccount</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedAccountReference"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text>billingPaymentShowTextField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='batchPayment' or $screen='createPayment' or 'unidentifiedPaymentsDetail'">
											<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>DCT.Util.checkIfReferenceAndPaymentFieldsBlank</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='unidentifiedPaymentsDetail'">
											<xsl:text>paymentSave</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>paymentAction</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">35</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('PolicyReference_colon')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedPolicy</xsl:with-param>
						<xsl:with-param name="name">_unidentifedPolicy</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedPolicyReference"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text>billingPaymentShowTextField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='batchPayment' or $screen='createPayment' or 'unidentifiedPaymentsDetail'">
											<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>DCT.Util.checkIfReferenceAndPaymentFieldsBlank</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='unidentifiedPaymentsDetail'">
											<xsl:text>paymentSave</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>paymentAction</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">35</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<div class="downFieldGroup">
				<div class="downBeforeFormLabel">
					<xsl:value-of select="xslNsODExt:getDictRes('OtherInformation')"/>
				</div>
				<div class="downFormField">
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">unidentifedOtherInfo</xsl:with-param>
						<xsl:with-param name="name">_unidentifedOtherInfo</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:if test="$processType='unidentified'">
								<xsl:value-of select="$unidentifiedOtherReference"/>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text>billingPaymentShowTextField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='batchPayment' or $screen='createPayment' or 'unidentifiedPaymentsDetail'">
											<xsl:text>DCT.Util.checkIfReferenceFieldsBlank</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>DCT.Util.checkIfReferenceAndPaymentFieldsBlank</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="$screen='unidentifiedPaymentsDetail'">
											<xsl:text>paymentSave</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>paymentAction</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">45</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
			<xsl:if test="$screen='unidentifiedPaymentsDetail'">
				<div id="updateReference">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">paymentSave</xsl:with-param>
						<xsl:with-param name="id">paymentSave</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('unidentifiedPayments', 'unidentifiedPaymentUpdate', false, 'paymentSave');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('UpdateReference')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="buildAgencyPaymentDetails">
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="currencyCulture"/>
		<xsl:param name="paymentAmount"/>
		<xsl:param name="checkNumber"/>
		<xsl:param name="allocationClassCode"/>
		<xsl:param name="paymentMethodCode"/>
		<xsl:param name="postMarkDate"/>
		<xsl:param name="entryDate"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="isAgency"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="bankRouteNumber"/>
		<xsl:param name="bankAccountNumber"/>
		<xsl:param name="bankAcctType"/>
		<xsl:param name="firstName"/>
		<xsl:param name="lastName"/>
		<xsl:param name="wireTransferNumber"/>
		<xsl:param name="agencyId"/>
		<xsl:param name="setTotalsFunction"/>
		<div id="paymentDetails">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('PaymentDetails')"/>
			</h2>
			<table id="paymentDetailsTbl" class="formTbl">
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildPaymentAmountLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildPaymentAmount">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="paymentAmount" select="$paymentAmount"/>
							<xsl:with-param name="currencyCulture" select="/page/content/ExpressCache/Account/CurrencyCulture"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="setTotalsFunction" select="$setTotalsFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPaymentTypeLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildPaymentType">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="allocationClassCode" select="$allocationClassCode"/>
							<xsl:with-param name="isAgency" select="$isAgency"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildPaymentMethodLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildPaymentMethod">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="paymentMethodCode" select="$paymentMethodCode"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="isAgency">true</xsl:with-param>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildReceivedDateLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildReceivedDate">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="entryDate" select="$entryDate"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="isAgency">true</xsl:with-param>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildCheckNumberLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildCheckNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="checkNumber" select="$checkNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPostMarkDateLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildPostMarkDate">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="postMarkDate" select="$postMarkDate"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="isAgency">true</xsl:with-param>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildWireTransferNumberLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildWireTransferNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="wireTransferNumber" select="$wireTransferNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildAgentIdLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildAgentId">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="agencyId" select="$agencyId"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildFirstNameLabel"/>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="/page/content/ExpressCache/Account/Parties/PartyRecord/Party/PartyTypeCode ='O'">
								<xsl:call-template name="buildFirstNameForBusinessParty">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="firstName" select="$firstName"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildFirstName">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="firstName" select="$firstName"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:call-template name="buildFullNameLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildLastName">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="lastName" select="$lastName"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildPaymentDetails">
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="currencyCulture"/>
		<xsl:param name="paymentAmount"/>
		<xsl:param name="checkNumber"/>
		<xsl:param name="paymentReference"/>
		<xsl:param name="cardType"/>
		<xsl:param name="allocationClassCode"/>
		<xsl:param name="paymentMethodCode"/>
		<xsl:param name="postMarkDate"/>
		<xsl:param name="cardNumber"/>
		<xsl:param name="entryDate"/>
		<xsl:param name="identificationCode"/>
		<xsl:param name="expiryMonth"/>
		<xsl:param name="expiryYear"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="isAgency"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="bankRouteNumber"/>
		<xsl:param name="bankAccountNumber"/>
		<xsl:param name="bankAcctType"/>
		<xsl:param name="firstName"/>
		<xsl:param name="lastName"/>
    <xsl:param name="nameOnCheck"/>
		<xsl:param name="address"/>
		<xsl:param name="city"/>
		<xsl:param name="state"/>
		<xsl:param name="postalCode"/>
		<xsl:param name="country"/>
		<xsl:param name="phone"/>
		<xsl:param name="email"/>
		<xsl:param name="issueNumber"/>
		<xsl:param name="authCode"/>
		<xsl:param name="startMonth"/>
		<xsl:param name="startYear"/>
		<xsl:param name="referenceNumber"/>
		<xsl:param name="invoiceReference"/>
		<div id="paymentDetails">
			<h2>
				<xsl:value-of select="xslNsODExt:getDictRes('PaymentDetails')"/>
			</h2>
			<table id="paymentDetailsTbl" class="formTbl">
				<xsl:choose>
					<xsl:when test="$screen='batchPayment' or $screen='editBatchPayment'">
						<tr class="paymentDetailsTblRow">
							<td>
								<xsl:call-template name="buildPaymentReferenceLabel"/>
							</td>
							<td>
								<xsl:call-template name="buildPaymentReference">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="paymentReference" select="$paymentReference"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
								</xsl:call-template>
							</td>
							<td>
								<xsl:call-template name="buildInvoiceReferenceLabel"/>
							</td>
							<td>
								<xsl:call-template name="buildInvoiceReference">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="invoiceReference" select="$invoiceReference"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
								</xsl:call-template>
							</td>
						</tr>
					</xsl:when>
				</xsl:choose>
				<tr class="paymentDetailsTblRow">
					<xsl:choose>
						<xsl:when test="$screen='makeAPayment'">
							<td>
								<xsl:call-template name="buildPaymentMethodLabel"/>
							</td>
							<td>
								<xsl:call-template name="buildPaymentMethod">
									<xsl:with-param name="screen" select="$screen"/>
									<xsl:with-param name="paymentMethodCode" select="$paymentMethodCode"/>
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:call-template name="buildPaymentAmountLabel"/>
							</td>
							<td>
								<xsl:call-template name="buildPaymentAmount">
									<xsl:with-param name="screen" select="$screen"/>
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="paymentAmount" select="$paymentAmount"/>
									<xsl:with-param name="currencyCulture" select="$currencyCulture"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
						</xsl:otherwise>
					</xsl:choose>
					<td>
						<xsl:call-template name="buildPaymentTypeLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildPaymentType">
							<xsl:with-param name="screen" select="$screen"/>
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="allocationClassCode" select="$allocationClassCode"/>
							<xsl:with-param name="isAgency" select="$isAgency"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<xsl:choose>
						<xsl:when test="$screen='makeAPayment'">
							<td>
								<xsl:call-template name="buildReceivedDateLabel"/>
								<xsl:call-template name="buildTypeOfCardLabel">
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
							<td>
								<xsl:call-template name="buildReceivedDate">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="entryDate" select="$entryDate"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
								<xsl:call-template name="buildTypeOfCard">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="cardType" select="$cardType"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
						</xsl:when>
						<xsl:otherwise>
							<td>
								<xsl:call-template name="buildPaymentMethodLabel"/>
							</td>
							<td>
								<xsl:call-template name="buildPaymentMethod">
									<xsl:with-param name="screen" select="$screen"/>
									<xsl:with-param name="paymentMethodCode" select="$paymentMethodCode"/>
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
							<td>
								<xsl:call-template name="buildReceivedDateLabel"/>
								<xsl:call-template name="buildTypeOfCardLabel">
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
							<td>
								<xsl:call-template name="buildReceivedDate">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="entryDate" select="$entryDate"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
								<xsl:call-template name="buildTypeOfCard">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="cardType" select="$cardType"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</td>
						</xsl:otherwise>
					</xsl:choose>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildCheckNumberLabel"/>
						<xsl:call-template name="buildCardNumberLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildCheckNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="checkNumber" select="$checkNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
						<xsl:call-template name="buildCardNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="cardNumber" select="$cardNumber"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildBankAcctTypeLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
						<xsl:call-template name="buildPostMarkDateLabel"/>
						<xsl:call-template name="buildVerificationIdLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildBankAcctType">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="bankAcctType" select="$bankAcctType"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
						<xsl:call-template name="buildPostMarkDate">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="postMarkDate" select="$postMarkDate"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
						<xsl:call-template name="buildVerificationId">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="identificationCode" select="$identificationCode"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildBankRouteNumberLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildBankRouteNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="bankRouteNumber" select="$bankRouteNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildBankAccountNumberLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildBankAccountNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="bankAccountNumber" select="$bankAccountNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildFirstNameLabel"/>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="/page/content/AccountRecord/Parties/PartyRecord/Party/PartyTypeCode !='O'">
								
								<xsl:call-template name="buildFirstName">
              <xsl:with-param name="processType" select="$processType"/>
              <xsl:with-param name="nameOnCheck" select="$nameOnCheck"/>
              <xsl:with-param name="firstName" select="$firstName"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildFirstNameForBusinessParty">
									<xsl:with-param name="processType" select="$processType"/>
									<xsl:with-param name="firstName" select="$firstName"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:call-template name="buildLastNameLabel"/>
					</td>
					<td>
						<xsl:call-template name="buildLastName">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="lastName" select="$lastName"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildAddressLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildAddress">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="address" select="$address"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCityLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCity">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="city" select="$city"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildStateLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildState">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="state" select="$state"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPostalCodeLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPostalCode">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="postalCode" select="$postalCode"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildCountryLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCountry">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="country" select="$country"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPhoneLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildPhone">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="phone" select="$phone"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">

					<td>
						<xsl:call-template name="buildCardExpirationLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCardExpiration">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="expiryMonth" select="$expiryMonth"/>
							<xsl:with-param name="expiryYear" select="$expiryYear"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildEmailLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildEmail">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="email" select="$email"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildReferenceNumberLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildReferenceNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="referenceNumber" select="$referenceNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCardStartDateLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildCardStartDate">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="startMonth" select="$startMonth"/>
							<xsl:with-param name="startYear" select="$startYear"/>
							<xsl:with-param name="blankFieldCheckFunction" select="$blankFieldCheckFunction"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
				<tr class="paymentDetailsTblRow">
					<td>
						<xsl:call-template name="buildIssueNumberLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildIssueNumber">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="issueNumber" select="$issueNumber"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildAuthCodeLabel">
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
					<td>
						<xsl:call-template name="buildAuthCode">
							<xsl:with-param name="processType" select="$processType"/>
							<xsl:with-param name="authCode" select="$authCode"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
						</xsl:call-template>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildPaymentAmountLabel">
		<span>
			<xsl:value-of select="xslNsODExt:getDictRes('PaymentAmount')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPaymentAmount">
		<xsl:param name="currencyCulture"/>
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="paymentAmount"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="setTotalsFunction"/>
		<div id="paymentAmountId">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $paymentAmount)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">
							<xsl:text>paymentAmount</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="name">
							<xsl:text>_paymentAmount</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$screen='batchPayment' or $screen='createPayment'">
									<xsl:text></xsl:text>
								</xsl:when>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_hiddenPaymentAmount"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, '$-??????????#.00', $paymentAmount)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">paymentAmountFloatField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', $currencyCulture)"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctNumberMin</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>0.00</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctHiddenField</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>_hiddenPaymentAmount</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:if test="normalize-space($setTotalsFunction)!=''">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctSetTotalsFunction</xsl:with-param>
									<xsl:with-param name="type">object</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$setTotalsFunction"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCheckNumberLabel">
		<span id="checkNumberHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('CheckNumber')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPaymentReferenceLabel">
		<span id="PaymentReferenceHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('PaymentReference_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildInvoiceReferenceLabel">
		<span id="PaymentReferenceHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('InvoiceReference_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPaymentReference">
		<xsl:param name="processType"/>
		<xsl:param name="paymentReference"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="PaymentReferenceInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="$paymentReference"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">PaymentReference</xsl:with-param>
						<xsl:with-param name="name">_PaymentReference</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_buildPaymentReference"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$paymentReference"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildInvoiceReference">
		<xsl:param name="processType"/>
		<xsl:param name="invoiceReference"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="InvoiceReferenceInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="$invoiceReference"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">InvoiceReference</xsl:with-param>
						<xsl:with-param name="name">_InvoiceReference</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_buildInvoiceReference"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$invoiceReference"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCheckNumber">
		<xsl:param name="processType"/>
		<xsl:param name="checkNumber"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="checkNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="$checkNumber"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">checkNumber</xsl:with-param>
						<xsl:with-param name="name">_checkNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_checkNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$checkNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildBankRouteNumberLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="bankRouteNumberHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('BankRoutingNumber')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildBankRouteNumber">
		<xsl:param name="processType"/>
		<xsl:param name="bankRouteNumber"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="bankRouteNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">bankRouteNumber</xsl:with-param>
						<xsl:with-param name="name">_bankRouteNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_bankRouteNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$bankRouteNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildFirstNameLabel">
		<span id="firstNameCheckHeader">			
      <xsl:value-of select="xslNsODExt:getDictRes('NameOnCheck')"/>
		</span>
		<span id="firstNameCardHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('FirstNameOnCard')"/>
		</span>
		<span id="firstNameAgentHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('AgentFirstName')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildFirstName">
		<xsl:param name="processType"/>
		<xsl:param name="firstName"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="firstNameInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="$firstName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">firstName</xsl:with-param>
						<xsl:with-param name="name">_firstName</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_firstName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$firstName"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
  <xsl:template name="buildNameOnCheck">
    <xsl:param name="processType"/>
    <xsl:param name="nameOnCheck"/>
    <xsl:param name="firstName"/>
    <xsl:param name="lastName"/>
    <xsl:param name="readOnly"/>
    <xsl:param name="blankFieldCheckFunction"/>
    <div id="firstNameInput">
      <xsl:choose>
        <xsl:when test="$readOnly='true'">
          <xsl:value-of select="$nameOnCheck"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="buildSystemControl">
            <xsl:with-param name="id">nameOnCheck</xsl:with-param>
            <xsl:with-param name="name">_nameOnCheck</xsl:with-param>
            <xsl:with-param name="type">text</xsl:with-param>
            <xsl:with-param name="value">
              <xsl:choose>
                <xsl:when test="$nameOnCheck!=''">
                    <xsl:value-of select="$nameOnCheck"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select ="concat($firstName,' ',$lastName )"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="controlClass">
              <xsl:text>billingCheckFunctionEnableTextField</xsl:text>
            </xsl:with-param>
            <xsl:with-param name="extraConfigItems">
              <xsl:call-template name="addConfigProperty">
                <xsl:with-param name="name">dctCheckFunction</xsl:with-param>
                <xsl:with-param name="type">object</xsl:with-param>
                <xsl:with-param name="value">
                  <xsl:value-of select="$blankFieldCheckFunction"/>
                </xsl:with-param>
              </xsl:call-template>
              <xsl:call-template name="addConfigProperty">
                <xsl:with-param name="name">dctActionButton</xsl:with-param>
                <xsl:with-param name="type">string</xsl:with-param>
                <xsl:with-param name="value">
                  <xsl:text>paymentAction</xsl:text>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:with-param>
            <xsl:with-param name="required">1</xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>
	<xsl:template name="buildFirstNameForBusinessParty">
		<xsl:param name="processType"/>
		<xsl:param name="firstName"/>
		<xsl:param name="readOnly"/>
		<div id="firstNameInput">
			<xsl:call-template name="buildSystemControl">
				<xsl:with-param name="id">firstName</xsl:with-param>
				<xsl:with-param name="name">_firstName</xsl:with-param>
				<xsl:with-param name="type">text</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:choose>
						<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
							<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_firstName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$firstName"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="controlClass">
					<xsl:text></xsl:text>
				</xsl:with-param>
				<xsl:with-param name="extraConfigItems">
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctActionButton</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>paymentAction</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildLastNameLabel">
		<span id="lastNameCheckHeader">
			<xsl:choose>
				<xsl:when test="/page/content/ExpressCache/Account/Parties/PartyRecord/Party/PartyTypeCode ='O'">
					<xsl:value-of select="xslNsODExt:getDictRes('OrgLastNameOnCheck')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsODExt:getDictRes('LastNameOnCheck')"/>
				</xsl:otherwise>
			</xsl:choose>
		</span>
		<span id="lastNameCardHeader">
			<xsl:choose>
				<xsl:when test="/page/content/ExpressCache/Account/Parties/PartyRecord/Party/PartyTypeCode ='O'">
					<xsl:value-of select="xslNsODExt:getDictRes('OrgLastNameOnCard')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsODExt:getDictRes('LastNameOnCard')"/>
				</xsl:otherwise>
			</xsl:choose>
		</span>
		<span id="lastNameAgentHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('AgentLastName')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildFullNameLabel">
		<span id="lastNameCheckHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('NameOnCheck')"/>
		</span>
		<span id="lastNameCardHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('NameOnCard')"/>
		</span>
		<span id="lastNameAgentHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('AgentName')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildLastName">
		<xsl:param name="processType"/>
		<xsl:param name="lastName"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="lastNameInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="$lastName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">lastName</xsl:with-param>
						<xsl:with-param name="name">_lastName</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_lastName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$lastName"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildAddressLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="addressHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('Address_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildAddress">
		<xsl:param name="processType"/>
		<xsl:param name="address"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="addressInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">address</xsl:with-param>
						<xsl:with-param name="name">_address</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_address"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$address"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">40</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCityLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cityHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('City_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildCity">
		<xsl:param name="processType"/>
		<xsl:param name="city"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="cityInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">city</xsl:with-param>

						<xsl:with-param name="name">_city</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_city"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$city"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildStateLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="stateHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('State_colon_address')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildState">
		<xsl:param name="processType"/>
		<xsl:param name="state"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="stateInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="stateCheck">
						<xsl:choose>
							<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
								<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_state"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$state"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">state</xsl:with-param>
						<xsl:with-param name="name">_state</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectState')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_USSTATES']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectState')"/>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="normalize-space($stateCheck)=''">
									<xsl:text></xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$stateCheck"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildPostalCodeLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="postalCodeHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('PostalCode_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildPostalCode">
		<xsl:param name="processType"/>
		<xsl:param name="postalCode"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="postalCodeInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">postalCode</xsl:with-param>
						<xsl:with-param name="name">_postalCode</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_postalCode"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$postalCode"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingPostCodeEnableZipCodeField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildPhoneLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="phoneHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('Phone_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildPhone">
		<xsl:param name="processType"/>
		<xsl:param name="phone"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="phoneInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">phone</xsl:with-param>
						<xsl:with-param name="name">_phone</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_phone"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$phone"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingEnablingPhoneField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildEmailLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="emailHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('Email_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildEmail">
		<xsl:param name="processType"/>
		<xsl:param name="email"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="emailInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">email</xsl:with-param>
						<xsl:with-param name="name">_email</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_email"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$email"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingEnablingEmailField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="size">30</xsl:with-param>
						<xsl:with-param name="width">128</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCountryLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="countryHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('Country_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildCountry">
		<xsl:param name="processType"/>
		<xsl:param name="country"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="countryInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="countryCheck">
						<xsl:choose>
							<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
								<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_country"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$country"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">country</xsl:with-param>
						<xsl:with-param name="name">_country</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectCountry')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_COUNTRIES']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="normalize-space($countryCheck)"/>
						</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectCountry')"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildBankAcctTypeLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="bankAcctTypeHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('BankAccountType')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildBankAcctType">
		<xsl:param name="processType"/>
		<xsl:param name="bankAcctType"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="bankAcctTypeInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="bankAcctTypeCheck">
						<xsl:choose>
							<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
								<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_bankAcctType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$bankAcctType"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">bankAcctType</xsl:with-param>
						<xsl:with-param name="name">_bankAcctType</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectAccountType')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_BANKACTTYPE']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectAccountType')"/>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="normalize-space($bankAcctTypeCheck)"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildBankAccountNumberLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="bankAccountNumberHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('BankAccountNumber')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildBankAccountNumber">
		<xsl:param name="processType"/>
		<xsl:param name="bankAccountNumber"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<div id="bankAccountNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">bankAccountNumber</xsl:with-param>
						<xsl:with-param name="name">_bankAccountNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_bankAccountNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$bankAccountNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableTextField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildTypeOfCardLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardTypeHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('TypeOfCard')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildTypeOfCard">
		<xsl:param name="cardType"/>
		<xsl:param name="processType"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<div id="cardTypeInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="cardTypeCheck">
						<xsl:choose>
							<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
								<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_cardType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$cardType"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cardType</xsl:with-param>
						<xsl:with-param name="name">_cardType</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectType')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_CARDTYPE']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectType')"/>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="normalize-space($cardTypeCheck)"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">billingCardTypeComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCardTypeField</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>cardType</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCardNumberField</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>_cardNumberId</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildPaymentTypeLabel">
		<span>
			<xsl:value-of select="xslNsODExt:getDictRes('PaymentType')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPaymentType">
		<xsl:param name="processType"/>
		<xsl:param name="allocationClassCode"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="isAgency"/>
		<xsl:param name="readOnly"/>
		<div id="paymentTypeId">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTALLOCCLASS']/ListEntry[@Code=$allocationClassCode]/@Description"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="paymentType">
						<xsl:choose>
							<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
								<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_paymentType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$allocationClassCode"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">paymentType</xsl:with-param>
						<xsl:with-param name="name">_paymentType</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<option>
								<xsl:attribute name="value">
									<xsl:text></xsl:text>
								</xsl:attribute>
								<xsl:value-of select="xslNsODExt:getDictRes('SelectType')"/>
							</option>
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTALLOCCLASS']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="normalize-space($paymentType)"/>
						</xsl:with-param>
						<xsl:with-param name="watermark">
							<xsl:value-of select="xslNsODExt:getDictRes('SelectType')"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">paymentTypeComboField</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:if test="normalize-space($isAgency)!=''">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctAgency</xsl:with-param>
									<xsl:with-param name="type">boolean</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$isAgency"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:with-param>
						<xsl:with-param name="width">110</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildPaymentMethodLabel">
		<span>
			<xsl:value-of select="xslNsODExt:getDictRes('PaymentMethod_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPaymentMethod">
		<xsl:param name="screen"/>
		<xsl:param name="processType"/>
		<xsl:param name="paymentMethodCode"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="isAgency">false</xsl:param>
		<div id="paymentMethodId">
			<div>
				<xsl:choose>
					<xsl:when test="$readOnly='true'">
						<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTMETHOD']/ListEntry[@Code=$paymentMethodCode]/@Description"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="paymentMethod">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_paymentMethod"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$paymentMethodCode"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">paymentMethod</xsl:with-param>
							<xsl:with-param name="name">_paymentMethod</xsl:with-param>
							<xsl:with-param name="type">select</xsl:with-param>
							<xsl:with-param name="optionlist">
								<option>
									<xsl:attribute name="value">
										<xsl:text></xsl:text>
									</xsl:attribute>
									<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
								</option>
								<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_PYMTMETHOD']/ListEntry[@Code!='EFT']">
									<xsl:if test="$screen='batchPayment' or $screen='editPaymentBatch'">
										<xsl:if test="@Code='CK' or @Code='CS'">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:if>
									</xsl:if>
									<xsl:if test="$screen!='batchPayment' and $screen!='editPaymentBatch'">
										<option>
											<xsl:attribute name="value">
												<xsl:value-of select="@Code"/>
											</xsl:attribute>
											<xsl:value-of select="@Description"/>
										</option>
									</xsl:if>
								</xsl:for-each>
							</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="normalize-space($paymentMethod)"/>
							</xsl:with-param>
							<xsl:with-param name="watermark">
								<xsl:value-of select="xslNsODExt:getDictRes('SelectMethod')"/>
							</xsl:with-param>
							<xsl:with-param name="controlClass">paymentMethodComboField</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctActionButton</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>paymentAction</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
									<xsl:with-param name="type">object</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$blankFieldCheckFunction"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctAgency</xsl:with-param>
									<xsl:with-param name="type">boolean</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="$isAgency"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
							<xsl:with-param name="required">1</xsl:with-param>
							<xsl:with-param name="width">180</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildPostMarkDateLabel">
		<span id="postmarkDateHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('PostmarkDate_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildPostMarkDate">
		<xsl:param name="processType"/>
		<xsl:param name="postMarkDate"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="isAgency"/>
		<div id="postmarkDateInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="xslNsExt:convertISOtoDisplay($postMarkDate, 'd')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">checkPostDate</xsl:with-param>
						<xsl:with-param name="name">_checkPostDate</xsl:with-param>
						<xsl:with-param name="type">date</xsl:with-param>
						<xsl:with-param name="dateFormat">
							<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$isAgency='true'">
									<xsl:text>agencyPostDateField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemDateField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_checkPostDate"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:convertISOtoDisplay($postMarkDate, 'd')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCardNumberLabel">
		<span id="cardNumberHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('CardNumber_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildCardNumber">
		<xsl:param name="processType"/>
		<xsl:param name="cardNumber"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<div id="cardNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="xslNsExt:maskCard($cardNumber)"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">_cardNumberId</xsl:with-param>
						<xsl:with-param name="name">_cardNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_cardNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$cardNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingCheckFunctionEnableCardField</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCardTypeField</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>cardType</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildReferenceNumberLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardReferenceNumberHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('ReferenceNumber_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildReferenceNumber">
		<xsl:param name="processType"/>
		<xsl:param name="referenceNumber"/>
		<xsl:param name="readOnly"/>
		<div id="cardReferenceNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cardReferenceNumber</xsl:with-param>
						<xsl:with-param name="name">_cardReferenceNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/TransactionID"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$referenceNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildAuthCodeLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardAuthCodeHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('AuthorizationCode')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildAuthCode">
		<xsl:param name="processType"/>
		<xsl:param name="authCode"/>
		<xsl:param name="readOnly"/>
		<div id="cardAuthCodeInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cardAuthCode</xsl:with-param>
						<xsl:with-param name="name">_cardAuthCode</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_cardAuthCode"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$authCode"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildIssueNumberLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardIssueNumberHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('IssueNumber')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildIssueNumber">
		<xsl:param name="processType"/>
		<xsl:param name="issueNumber"/>
		<xsl:param name="readOnly"/>
		<div id="cardIssueNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cardIssueNumber</xsl:with-param>
						<xsl:with-param name="name">_cardIssueNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_cardIssueNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$issueNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildVerificationIdLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardVerificationHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('VerificationNumber')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildVerificationId">
		<xsl:param name="processType"/>
		<xsl:param name="verificationId"/>
		<xsl:param name="readOnly"/>
		<div id="cardVerificationInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">cardVerificationId</xsl:with-param>
						<xsl:with-param name="name">_cardVerification</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_cardVerification"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$verificationId"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildReceivedDateLabel">
		<span id="checkRecvDateHeader">
			<xsl:value-of select="xslNsODExt:getDictRes('DateReceived_colon')"/>
		</span>
	</xsl:template>
	<xsl:template name="buildReceivedDate">
		<xsl:param name="processType"/>
		<xsl:param name="entryDate"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="isAgency"/>
		<div id="checkRecvDateInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
					<xsl:value-of select="xslNsExt:convertISOtoDisplay($entryDate, 'd')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">checkRecvDate</xsl:with-param>
						<xsl:with-param name="name">_checkRecvDate</xsl:with-param>
						<xsl:with-param name="type">date</xsl:with-param>
						<xsl:with-param name="dateFormat">
							<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '0')"/>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:choose>
								<xsl:when test="$isAgency='true'">
									<xsl:text>agencyReceivedDateField</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>basicKeyUpDateField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_checkRecvDate"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:convertISOtoDisplay($entryDate, 'd')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="extraConfigItems">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctActionButton</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:text>paymentAction</xsl:text>
								</xsl:with-param>
							</xsl:call-template>
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
								<xsl:with-param name="type">object</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$blankFieldCheckFunction"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="required">1</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildCardStartDateLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardStartDateHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('StartDate_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildCardStartDate">
		<xsl:param name="processType"/>
		<xsl:param name="startMonth"/>
		<xsl:param name="startYear"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<div id="cardStartDateInput">
				<div class="acrossFieldGroup">
					<div class="acrossFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Month')"/>
					</div>
					<div class="acrossFormField">
						<xsl:choose>
							<xsl:when test="$readOnly='true'">
								<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='PLT_MONTHNUMBERS']/ListEntry[@Code=$startMonth]/@Description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="stMonth">
									<xsl:choose>
										<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_startMonth"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$startMonth"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">startMonth</xsl:with-param>
									<xsl:with-param name="name">_startMonth</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_MONTHNUMBERS']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="normalize-space($startMonth)"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>paymentAction</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
											<xsl:with-param name="type">object</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="$blankFieldCheckFunction"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<!--</div>-->
					<!--<div class="acrossFieldGroup">-->
					<div class="acrossFormLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Year')"/>
					</div>
					<div class="acrossFormField">
						<xsl:choose>
							<xsl:when test="$readOnly='true'">
								<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='YEARNUMBERS']/ListEntry[@Code=$startYear]/@Description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="stYear">
									<xsl:choose>
										<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_startYear"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$startYear"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">startYear</xsl:with-param>
									<xsl:with-param name="name">_startYear</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='YEARNUMBERS']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="normalize-space($startYear)"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>paymentAction</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
											<xsl:with-param name="type">object</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="$blankFieldCheckFunction"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildCardExpirationLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="cardExpirationHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('ExpirationDate')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildCardExpiration">
		<xsl:param name="processType"/>
		<xsl:param name="expiryMonth"/>
		<xsl:param name="expiryYear"/>
		<xsl:param name="blankFieldCheckFunction"/>
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<div id="cardExpirationInput">
				<div class="acrossFieldGroup">
					<div class="acrossFormLabel" style="margin-left:5px;">
						<xsl:value-of select="xslNsODExt:getDictRes('Month')"/>
					</div>
					<div class="acrossFormField">
						<xsl:choose>
							<xsl:when test="$readOnly='true'">
								<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='PLT_MONTHNUMBERS']/ListEntry[@Code=$expiryMonth]/@Description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="expMonth">
									<xsl:choose>
										<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_expMonth"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$expiryMonth"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">expMonth</xsl:with-param>
									<xsl:with-param name="name">_expMonth</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='PLT_MONTHNUMBERS']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="normalize-space($expMonth)"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>paymentAction</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
											<xsl:with-param name="type">object</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="$blankFieldCheckFunction"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<!--</div>
					<div class="acrossFieldGroup">-->
					<div class="acrossFormLabel" style="margin-left:25px;">
						<xsl:value-of select="xslNsODExt:getDictRes('Year')"/>
					</div>
					<div class="acrossFormField">
						<xsl:choose>
							<xsl:when test="$readOnly='true'">
								<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='YEARNUMBERS']/ListEntry[@Code=$expiryYear]/@Description"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="expYear">
									<xsl:choose>
										<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_expYear"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="$expiryYear"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">expYear</xsl:with-param>
									<xsl:with-param name="name">_expYear</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='YEARNUMBERS']/ListEntry">
											<option>
												<xsl:attribute name="value">
													<xsl:value-of select="@Code"/>
												</xsl:attribute>
												<xsl:value-of select="@Description"/>
											</option>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="normalize-space($expYear)"/>
									</xsl:with-param>
									<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
									<xsl:with-param name="extraConfigItems">
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctActionButton</xsl:with-param>
											<xsl:with-param name="type">string</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:text>paymentAction</xsl:text>
											</xsl:with-param>
										</xsl:call-template>
										<xsl:call-template name="addConfigProperty">
											<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
											<xsl:with-param name="type">object</xsl:with-param>
											<xsl:with-param name="value">
												<xsl:value-of select="$blankFieldCheckFunction"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:with-param>
									<xsl:with-param name="required">1</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildWireTransferNumberLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="wireTransferNumberHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('WireTransferNumber_colon')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildWireTransferNumber">
		<xsl:param name="processType"/>
		<xsl:param name="wireTransferNumber"/>
		<xsl:param name="readOnly"/>
		<div id="wireTransferNumberInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">wireTransferNumber</xsl:with-param>
						<xsl:with-param name="name">_wireTransferNumber</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_wireTransferNumber"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$wireTransferNumber"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildAgentIdLabel">
		<xsl:param name="readOnly"/>
		<xsl:if test="$readOnly!='true'">
			<span id="agentIdHeader">
				<xsl:value-of select="xslNsODExt:getDictRes('AgentId')"/>
			</span>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildAgentId">
		<xsl:param name="processType"/>
		<xsl:param name="agencyId"/>
		<xsl:param name="readOnly"/>
		<div id="agentIdInput">
			<xsl:choose>
				<xsl:when test="$readOnly='true'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">agentId</xsl:with-param>
						<xsl:with-param name="name">_agentId</xsl:with-param>
						<xsl:with-param name="type">text</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$processType='acctNotFound' or $processType='acctFound'">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_agentId"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$agencyId"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="controlClass">
							<xsl:text>billingTextField</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template name="buildBillingPaymentDetail">
		<div id="billingPaymentDetail">
			<div class="displayTblTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PaymentDetails')"/>
			</div>
			<table id="tblBillingPaymentDetail" class="displayTbl">
				<tr class="displayTblHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Amount')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('DateEntered')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Status')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('PaymentMethod')"/>
					</td>
					<xsl:choose>
            <xsl:when test="/page/content/*/PaymentMethodCode='1EFT'">
             <td>
                 <xsl:value-of select="xslNsODExt:getDictRes('BankAccountNumberHeader')"/>
            </td>
            </xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='CK'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CheckNumberSymbol')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('NameOnCheck')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('PostmarkDate')"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='EC'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CheckNumberSymbol')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('NameOnCheck')"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='1XCC' or /page/content/*/PaymentMethodCode='1XDC'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CardNumber')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('AuthorizationCodeHeader')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('TransactionStatus')"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='RCC' or /page/content/*/PaymentMethodCode='RDC'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CardNumber')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('AuthorizationCodeHeader')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('TransactionStatus')"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='CC' or /page/content/*/PaymentMethodCode='DC'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('CardNumber')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('NameOnCard')"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='WT'">
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('WireTransferNumberSymbol')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('NameOnTransfer')"/>
							</td>
							<td>
								<xsl:value-of select="xslNsODExt:getDictRes('PostedDate')"/>
							</td>
						</xsl:when>
					</xsl:choose>
				</tr>
				<tr class="displayTblRow">
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(/page/content/*/CurrencyCulture, 'c2', /page/content/*/Amount)"/>
					</td>
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/EntryDate)"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/*/Status"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/*/PaymentMethod"/>
					</td>          
          <xsl:choose>
            <xsl:when test="/page/content/*/PaymentMethodCode='1EFT'">
              <td>
                <xsl:value-of select="xslNsExt:maskCard(/page/content/*/PaymentMethodDetail/*/BankAccountNumber)"/>
					</td>
            </xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='CK'">
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CheckNumber"/>
							</td>
							<td>
								<xsl:if test="normalize-space(/page/content/*/PaymentMethodDetail/*/BillToFirstName)!=''">
									<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToFirstName"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToLastName"/>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/NameOnCheck"/>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/PostMarkDate)"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='EC'">
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CheckNumber"/>
							</td>
							<td>
								<xsl:if test="normalize-space(/page/content/*/PaymentMethodDetail/*/BillToFirstName)!=''">
									<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToFirstName"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToLastName"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='1XCC' or /page/content/*/PaymentMethodCode='1XDC'">
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardNumber"/>
							</td>
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardTransactionDetails/AuthorizationCode"/>
							</td>
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardTransactionDetails/CardPaymentStatusDesc"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='RCC' or /page/content/*/PaymentMethodCode='RDC'">
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardNumber"/>
							</td>
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardTransactionDetails/AuthorizationCode"/>
							</td>
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/CardTransactionDetails/CardPaymentStatusDesc"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='CC' or /page/content/*/PaymentMethodCode='DC'">
							<td>
								<xsl:value-of select="xslNsExt:maskCard(/page/content/*/PaymentMethodDetail/*/PCardNumber)"/>
							</td>
							<td>
								<xsl:if test="normalize-space(/page/content/*/PaymentMethodDetail/*/BillToFirstName)!=''">
									<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToFirstName"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToLastName"/>
							</td>
						</xsl:when>
						<xsl:when test="/page/content/*/PaymentMethodCode='WT'">
							<td>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/WireTransferNumber"/>
							</td>
							<td>
                <xsl:choose>
                  <xsl:when test="normalize-space(/page/content/*/PaymentMethodDetail/*/BillToFirstName)!=''">
									<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToFirstName"/>
									<xsl:text> </xsl:text>
								<xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToLastName"/>
                  </xsl:when>
                  <xsl:when test="normalize-space(/page/content/*/PaymentMethodDetail/*/Name)!=''">
                    <xsl:value-of select="/page/content/*/PaymentMethodDetail/*/Name"/>
                    <xsl:text> </xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="/page/content/*/PaymentMethodDetail/*/BillToLastName"/>
                  </xsl:otherwise>
                </xsl:choose>
							</td>
							<td>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/*/PostMarkDate)"/>
							</td>
						</xsl:when>
					</xsl:choose>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="billingPaymentReversedInfo">
		<xsl:param name="type"/>
		<div class="billingPaymentReversed" id="billingPaymentReversedId">
			<div class="displayTblTitle">
				<xsl:value-of select="xslNsODExt:getDictRes('PaymentReversalInformation')"/>
			</div>
			<table id="tblBillingWriteOffNotification" class="displayTbl">
				<tr class="displayTblHdr">
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('PaymentReversed')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Reason')"/>
					</td>
					<xsl:if test="/page/content/PaymentDetail/RejectionReason!=''">
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('RejectionReason')"/>
						</td>
					</xsl:if>
						<td>
						<xsl:value-of select="xslNsODExt:getDictRes('UserId')"/>
					</td>
				</tr>
				<tr class="displayTblRow">
					<td>
						<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/PaymentDetail/ReversalDate)"/>
					</td>
					<td>
						<xsl:value-of select="/page/content/PaymentDetail/ReversalReason"/>
					</td>
					<xsl:if test="/page/content/PaymentDetail/RejectionReason!=''">
						<td>
							<xsl:value-of select="concat(/page/content/PaymentDetail/RejectionReason,' - ',/page/content/PaymentDetail/RejectionDescription)"/>
						</td>
					</xsl:if>
					<td>
						<xsl:value-of select="/page/content/PaymentDetail/ReversalUserId"/>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildPolicyContacts">
		<xsl:for-each select="/page/content/ExpressCache/PolicyTerm/AssociatedParties/Party">
			<xsl:variable name="divId">
				<xsl:value-of select="@Role"/>
				<xsl:value-of select="position()"/>
				<xsl:text>Id</xsl:text>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="/page/content/ExpressCache/Account/AccountTypeCode='CLM'">
					<xsl:if test="@Role!='Agent'">
						<div id="{$divId}" class="x-hidden">
							<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
							<xsl:attribute name="data-config">
								<xsl:text>{</xsl:text>
								<xsl:text>id: "contact</xsl:text>
								<xsl:value-of select="@Role"/>
								<xsl:value-of select="position()"/>
								<xsl:text>",</xsl:text>
								<xsl:text>title: "</xsl:text>
								<xsl:value-of select="@Role"/>
								<xsl:text>",</xsl:text>
								<xsl:text>contentEl: "</xsl:text>
								<xsl:value-of select="$divId"/>
								<xsl:text>",</xsl:text>
								<xsl:text>collapsed: false</xsl:text>
								<xsl:text>}</xsl:text>
							</xsl:attribute>
							<div id="policyContactNameId" class="billingContactName">
								<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyFirstName)!=''">
									<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyFirstName"/>
									<xsl:text> </xsl:text>
								</xsl:if>
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyName"/>
							</div>
							<div id="policyContactAddressId" class="billingContactAddress">
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationAddressLine1"/>
							</div>
							<div id="policyContactCityStateZipId" class="billingContactCityStateZip">
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationCity"/>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationStateCode"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationPostalCode"/>
							</div>
							<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber)!=''">
								<div id="policyContactPhoneId" class="billingContactPhone">
									<xsl:text>(</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 1,3)"/>
									<xsl:text>)</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 4,3)"/>
									<xsl:text>-</xsl:text>
									<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 7,4)"/>
									<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension)!=''">
										<xsl:text> </xsl:text>
										<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
										<xsl:text> </xsl:text>
										<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension"/>
									</xsl:if>
								</div>
							</xsl:if>
							<div id="policyContactEmailId" class="billingContactEmail">
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyEmails/PartyEmail/EmailAddress"/>
							</div>
						</div>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<div id="{$divId}" class="x-hidden">
						<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
						<xsl:attribute name="data-config">
							<xsl:text>{</xsl:text>
							<xsl:text>id: "contact</xsl:text>
							<xsl:value-of select="@Role"/>
							<xsl:value-of select="position()"/>
							<xsl:text>",</xsl:text>
							<xsl:text>title: "</xsl:text>
							<xsl:value-of select="@Role"/>
							<xsl:text>",</xsl:text>
							<xsl:text>contentEl: "</xsl:text>
							<xsl:value-of select="$divId"/>
							<xsl:text>",</xsl:text>
							<xsl:text>collapsed: false</xsl:text>
							<xsl:text>}</xsl:text>
						</xsl:attribute>
						<div id="policyContactNameId" class="billingContactName">
							<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyFirstName)!=''">
								<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyFirstName"/>
								<xsl:text> </xsl:text>
							</xsl:if>
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Party/PartyName"/>
						</div>
						<div id="policyContactAddressId" class="billingContactAddress">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationAddressLine1"/>
						</div>
						<div id="policyContactCityStateZipId" class="billingContactCityStateZip">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationCity"/>
							<xsl:text>, </xsl:text>
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationStateCode"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/Locations/Location/LocationPostalCode"/>
						</div>
						<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber)!=''">
							<div id="policyContactPhoneId" class="billingContactPhone">
								<xsl:text>(</xsl:text>
								<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 1,3)"/>
								<xsl:text>)</xsl:text>
								<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 4,3)"/>
								<xsl:text>-</xsl:text>
								<xsl:value-of select="substring(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneNumber, 7,4)"/>
								<xsl:if test="normalize-space(/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension)!=''">
									<xsl:text> </xsl:text>
									<xsl:value-of select="xslNsODExt:getDictRes('Ext')"/>
									<xsl:text> </xsl:text>
									<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyPhones/PartyPhone/PhoneExtension"/>
								</xsl:if>
							</div>
						</xsl:if>
						<div id="policyContactEmailId" class="billingContactEmail">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/Parties/PartyRecord[PartyRecordPartyId = current()/@PartyId]/PartyEmails/PartyEmail/EmailAddress"/>
						</div>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="buildPolicyLedger">
		<xsl:param name="accountType"/>
		<xsl:variable name="currencyCulture">
			<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/CurrencyCulture"/>
		</xsl:variable>
		<div id="policyLedger" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "ledger",</xsl:text>
				<xsl:text>title: DCT.T("PolicyTermLedger"),</xsl:text>
				<xsl:text>contentEl: "policyLedger",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblPolicyLedger" class="formTable">
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('OutstandingBalance_colon')"/>
						</div>
					</td>
					<td>
						<div id="outstandingBalanceId" class="dataItem textAlignedRight">
							<xsl:choose>
								<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/OutstandingBalance)=''">
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/OutstandingBalance)"/>
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</td>
				</tr>
				<xsl:if test="$accountType!='COLL'">
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('CurrentAmountDue')"/>
							</div>
						</td>
						<td>
							<div id="currentAmountDueId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/CurrentDueAmount)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/CurrentDueAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('PastDueAmount')"/>
							</div>
						</td>
						<td>
							<div id="pastDueAmountId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/PastDueAmount)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/PastDueAmount)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('ReceivableAmount')"/>
							</div>
						</td>
						<td>
							<div id="recievableAmountId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/TotalReceivable)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/TotalReceivable)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('PaidOnPolicyTerm')"/>
							</div>
						</td>
						<td>
							<div id="paidOnPolicyTermId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/TotalPaid)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/TotalPaid)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('WrittenOffWaived_colon')"/>
							</div>
						</td>
						<td>
							<div id="writtenOffWaiveId" class="dataItem textAlignedRight">
								<xsl:choose>
									<xsl:when test="normalize-space(/page/content/ExpressCache/PolicyTerm/TotalWrittenOff)=''">
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', '0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', /page/content/ExpressCache/PolicyTerm/TotalWrittenOff)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</xsl:if>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildPolicyDetails">
		<div id="policyDetail" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "detail",</xsl:text>
				<xsl:text>title: DCT.T("PolicyTermDetails"),</xsl:text>
				<xsl:text>contentEl: "policyDetail",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<table id="tblPolicyDetail" class="formTable">
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('BillingFrequency')"/>
						</div>
					</td>
					<td>
						<div id="billingFrequencyId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/BillingFrequency"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('MasterCompany')"/>
						</div>
					</td>
					<td>
						<div id="masterCompanyId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/PolicyMasterCompanyCode"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="headerLabel">
							<xsl:value-of select="xslNsODExt:getDictRes('LineOfBusiness')"/>
							<xsl:text>:</xsl:text>
						</div>
					</td>
					<td>
						<div id="lineOfBusinessId" class="dataItem">
							<xsl:value-of select="/page/content/ExpressCache/PolicyTerm/PolicyLineOfBusinessCode"/>
						</div>
					</td>
				</tr>
				<xsl:if test="/page/content/ExpressCache/Account/AccountTypeCode!='CLM'">
					<tr>
						<td>
							<div class="headerLabel">
								<xsl:value-of select="xslNsODExt:getDictRes('EquityDate')"/>
							</div>
						</td>
						<td>
							<div id="equityDateId" class="dataItem">
								<xsl:choose>
									<xsl:when test="/page/content/ExpressCache/PolicyTerm/EquityDate = ''">
										<xsl:text> </xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', /page/content/ExpressCache/PolicyTerm/EquityDate)"/>
									</xsl:otherwise>
								</xsl:choose>
							</div>
						</td>
					</tr>
				</xsl:if>
				<xsl:for-each select="/page/content/ExpressCache/PolicyTerm/ExternalData/Detail">
					<tr>
						<td>
							<div calss="headerLabel">
								<xsl:value-of select="Caption"/>:</div>
						</td>
						<td>
							<div id="externalData" class="dataItem">
								<xsl:value-of select="Value"/>
							</div>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildScheduleHistoryEvents">
		<div id="scheduleHistoryEvents" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "scheduleHistory",</xsl:text>
				<xsl:text>title: "Schedule History",</xsl:text>
				<xsl:text>contentEl: "scheduleHistoryEvents",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:for-each select="/page/content//HistoryDates/HistoryDate">
				<div class="scheduleHistoryEventTitle" id="scheduleHistoryEventTitleId">
					<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', @Date)"/>
				</div>
				<xsl:for-each select="HistorySnapshot">
					<div class="scheduleHistoryEventLink" id="scheduleHistoryEvent">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Util.loadScheduleHistory('</xsl:text>
								<xsl:value-of select="HistoryEventId"/>
								<xsl:text>', '</xsl:text>
								<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', HistoryDate)"/>
								<xsl:text>: </xsl:text>
								<xsl:value-of select="PolicyTermDescription"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="PolicyTermDescription"/>
							</xsl:with-param>
							<xsl:with-param name="name">_historyEvent<xsl:value-of select="HistoryEventId"/>
							</xsl:with-param>
							<xsl:with-param name="id">_historyEvent<xsl:value-of select="HistoryEventId"/>
							</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
				</xsl:for-each>
			</xsl:for-each>
		</div>
	</xsl:template>
	<xsl:template name="buildBillingPolicyActions">
		<xsl:param name="accountType"/>
		<div id="policyActions" class="x-hidden">
			<xsl:attribute name="class">collapsibleGroupItem</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>id: "actions",</xsl:text>
				<xsl:choose>
					<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
						<xsl:text>title: DCT.T("ArrangementActions"),</xsl:text>
					</xsl:when>
					<xsl:otherwise>
				<xsl:text>title: DCT.T("PolicyTermActions"),</xsl:text>
					</xsl:otherwise>
					</xsl:choose>
				<xsl:text>contentEl: "policyActions",</xsl:text>
				<xsl:text>collapsed: false</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:variable name="PolicyTermId" select="page/content/PolicyTerm/PolicyTermId" />
			<xsl:if test="((/page/content/followUpManager=1) or (/page/content/followUpManager='True')) and (/page/content/PolicyTerm/PolicyTermTypeCode!='ARRG')">
				<xsl:choose>
					<xsl:when test="not(/page/content/CollectionsRecords/CollectionsRecord[PolicyTermId=$PolicyTermId])">
						<xsl:if test="(/page/state/user/privileges/privilege[@name='BIL_CollectionsStart']) or 
								(/page/state/user/privileges/privilege[@name='BIL_AccessAllAreas'])">
							<div id="policySummary">
								<xsl:call-template name="makeButton">
									<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'transferToInternalCollections', true);</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:value-of select="xslNsODExt:getDictRes('TransferPolicyTermToInternalCollections')"/>
									</xsl:with-param>
									<xsl:with-param name="name">_policySummary</xsl:with-param>
									<xsl:with-param name="id">_policySummary</xsl:with-param>
									<xsl:with-param name="type">hyperlink</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="(/page/state/user/privileges/privilege[@name='BIL_CollectionsStop']) or 
								(/page/state/user/privileges/privilege[@name='BIL_AccessAllAreas'])">
							<div id="policySummary">
								<xsl:call-template name="makeButton">
									<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'transferOutOfInternalCollections', true);</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:value-of select="xslNsODExt:getDictRes('TransferPolicyTermOutOfInternalCollections')"/>
									</xsl:with-param>
									<xsl:with-param name="name">_policySummary</xsl:with-param>
									<xsl:with-param name="id">_policySummary</xsl:with-param>
									<xsl:with-param name="type">hyperlink</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:if>
						<xsl:if test="(/page/content/CollectionsRecords/CollectionsRecord[PolicyTermId=$PolicyTermId and StatusCode='STRT']) and 
								((/page/state/user/privileges/privilege[@name='BIL_CollectionsPause']) or 
								(/page/state/user/privileges/privilege[@name='BIL_AccessAllAreas']))">
							<div id="policySummary">
								<xsl:call-template name="makeButton">
									<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'pauseInternalCollections', true);</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:value-of select="xslNsODExt:getDictRes('PauseInternalCollections')"/>
									</xsl:with-param>
									<xsl:with-param name="name">_policySummary</xsl:with-param>
									<xsl:with-param name="id">_policySummary</xsl:with-param>
									<xsl:with-param name="type">hyperlink</xsl:with-param>
								</xsl:call-template>
							</div>
						</xsl:if>
						<xsl:if test="/page/content/CollectionsRecords/CollectionsRecord[PolicyTermId=$PolicyTermId and StatusCode='PAUS']">
							<xsl:if test="(/page/state/user/privileges/privilege[@name='BIL_CollectionsRestart']) or 
								(/page/state/user/privileges/privilege[@name='BIL_AccessAllAreas'])">
								<div id="policySummary">
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'restartInternalCollections', true);</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('RestartInternalCollections')"/>
										</xsl:with-param>
										<xsl:with-param name="name">_policySummary</xsl:with-param>
										<xsl:with-param name="id">_policySummary</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>
							<xsl:if test="(/page/state/user/privileges/privilege[@name='BIL_CollectionsResume']) or 
								(/page/state/user/privileges/privilege[@name='BIL_AccessAllAreas'])">
								<div id="policySummary">
									<xsl:call-template name="makeButton">
										<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'resumeInternalCollections', true);</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('ResumeInternalCollections')"/>
										</xsl:with-param>
										<xsl:with-param name="name">_policySummary</xsl:with-param>
										<xsl:with-param name="id">_policySummary</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="$accountType!='AGT' and $accountType!='COL2'">
				<xsl:if test="/page/content/ExpressCache/Account/AccountTypeCode!='COLL'">
					<xsl:if test="/page/content/ExpressCache/Account/AccountTypeCode!='CLM'">
						<xsl:if test="/page/content/@page!='transferPolicy'">
							<xsl:if test="$canTransferPolicy or $canAccessAllAreas">
								<div id="policySummary">
									<xsl:call-template name="makeButton">
										<!--<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('transferPolicy');</xsl:with-param>-->
										<xsl:with-param name="onclick">DCT.Submit.submitBillingPageTransferPolicyAction('transferPolicy','General');</xsl:with-param>
										<xsl:with-param name="caption">
										<xsl:choose>
											<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
						 						<xsl:value-of select="xslNsODExt:getDictRes('TransferArrangement')" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="xslNsODExt:getDictRes('TransferPolicyTerm')" />
											</xsl:otherwise>
										</xsl:choose>										
										</xsl:with-param>
										<xsl:with-param name="name">_transferPolicy</xsl:with-param>
										<xsl:with-param name="id">_transferPolicy</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>

							<xsl:if test="$canTransferPolicy or $canTransferToCollections or $canAccessAllAreas">
								<div id="policySummary">
									<xsl:call-template name="makeButton">
										<!--<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('transferPolicy');</xsl:with-param>-->
										<xsl:with-param name="onclick">DCT.Submit.submitBillingPageTransferPolicyAction('transferPolicy','Collections');</xsl:with-param>
										<xsl:with-param name="caption">
											<xsl:value-of select="xslNsODExt:getDictRes('TransferPolicyTermToCollections')"/>
										</xsl:with-param>
										<xsl:with-param name="name">_transferPolicyToCollections</xsl:with-param>
										<xsl:with-param name="id">_transferPolicyToCollections</xsl:with-param>
										<xsl:with-param name="type">hyperlink</xsl:with-param>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:if>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<xsl:if test="$canChangePolicySettings or $canAccessAllAreas">
				<div id="policySummary">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policyTermEdit', 'billingStartClean', false, '_policySettings');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:choose>
								<xsl:when test="$accountType='COLL'">
									<xsl:value-of select="xslNsODExt:getDictRes('ViewPolicyTermSettings')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
											<xsl:value-of select="xslNsODExt:getDictRes('ViewEditArrangementSettings')" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="xslNsODExt:getDictRes('ViewEditPolicyTermSettings')" />
								</xsl:otherwise>
							</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="name">_policySettings</xsl:with-param>
						<xsl:with-param name="id">_policySettings</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
			<xsl:if test="($canSubmitMPR or $canAccessAllAreas) and (/page/content/PolicyTerm/PolicyTermConfig/MPR/@MPR='1')">
				<div id="policySummary">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('submitMPR', 'billingStartClean', false, '_policySettings');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:choose>
								<xsl:when test="$accountType='COLL'">
									<xsl:value-of select="xslNsODExt:getDictRes('ViewPolicyTermSettings')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsODExt:getDictRes('SubmitMpr')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="name">_policySettings</xsl:with-param>
						<xsl:with-param name="id">_policySettings</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
			<xsl:if test="($canChangeRenewalSettings or $canChangePolicySettings or $canAccessAllAreas) and $accountType!='COLL' and (/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode!='ARRG')">
				<xsl:if test="/page/content/ExpressCache/ShowRenewalPlanChangeLink=1">
					<div id="policySummary">
						<!--Link for renewal plan change-->
						<xsl:call-template name="makeButton">
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policyTermEdit', 'renewalPlanChange', false, '_policySettingsRenewal');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:choose>
									<xsl:when test="$accountType='COLL'">
										<xsl:value-of select="xslNsODExt:getDictRes('ChangeRenewalPlan')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsODExt:getDictRes('ChangeRenewalPlan')"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="name">_policySettings</xsl:with-param>
							<xsl:with-param name="id">_policySettings</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
				</xsl:if>
			</xsl:if>
			<xsl:if test="/page/content/ExpressCache/Account/AccountTypeCode!='CLM'">
				<xsl:if test="/page/content/ExpressCache/PolicyTerm/HoldTypeCode='N' or normalize-space(/page/content/ExpressCache/PolicyTerm/HoldTypeCode)=''">
					<xsl:if test="($canHoldBilling or $canAccessAllAreas) and $accountType!='COLL'">
						<div id="policySummary">
							<xsl:call-template name="makeButton">
								<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('holdBillingForPolicyTerm');</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:choose>
										<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
											<xsl:value-of select="xslNsODExt:getDictRes('HoldBillingForArrangement')" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="xslNsODExt:getDictRes('HoldBillingForPolicyTerm')" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
								<xsl:with-param name="name">_holdBillingPolicyTerm</xsl:with-param>
								<xsl:with-param name="id">_holdBillingPolicyTerm</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</div>
					</xsl:if>
				</xsl:if>
			</xsl:if>
			<xsl:if test="/page/content/ExpressCache/PolicyTerm/PolicyTermStatusCode='PC'">
				<xsl:if test="$canManuallyRescind or $canAccessAllAreas">
					<div id="policySummary">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('policySummary', 'rescindPolicyTerm', true, "_rescindPolicy");</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:choose>
									<xsl:when test="/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode='ARRG'">
										<xsl:value-of select="xslNsODExt:getDictRes('RescindThisArrangement')" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="xslNsODExt:getDictRes('RescindThisPolicyTerm')" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="name">_rescindPolicy</xsl:with-param>
							<xsl:with-param name="id">_rescindPolicy</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
				</xsl:if>
			</xsl:if>
			<xsl:if test="$canPIFDiscount or $canAccessAllAreas and (/page/content/ExpressCache/PolicyTerm/PolicyTermTypeCode!='ARRG')">
				<div id="policySummary">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('PaidInFullDiscount', 'billingStartClean', false, "");</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('PaidInFullDiscount')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<xsl:template name="buildPolicyContent">
		<xsl:param name="accountType"/>
		<xsl:call-template name="buildBillingActiveSection">
			<xsl:with-param name="accountType" select="$accountType"/>
		</xsl:call-template>
		<xsl:call-template name="createSubMenuArea"/>
		<div id="policyInformationArea">
			<div id="policyInformationId" class="x-hidden">
				<xsl:element name="div">
					<xsl:attribute name="class">collapsibleGroupPanel</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>renderTo: "policyInformationId",</xsl:text>
						<xsl:text>id: "policyInformationGroup"</xsl:text>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
				</xsl:element>
				<xsl:if test="$accountType!='COL2'">
					<xsl:call-template name="buildBillingPolicyActions">
						<xsl:with-param name="accountType" select="$accountType"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:call-template name="buildPolicyDetails"/>
				<xsl:call-template name="buildPolicyLedger">
					<xsl:with-param name="accountType" select="$accountType"/>
				</xsl:call-template>
				<xsl:call-template name="buildPolicyContacts"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildCurrentSystemDate">
		<div id="currentSystemDate">
			<xsl:variable name="currentSystemDate" select="/page/content/ExpressCache/CurrentSystemDate/@date"/>
			<xsl:value-of select="xslNsODExt:getDictRes('CurrentSystemDateIs')"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsExt:convertISOtoDisplay($currentSystemDate, 'MMMM dd, yyyy')"/>
			<xsl:element name="input">
				<xsl:attribute name="type">
					<xsl:text>hidden</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:text>_currentSystemDate</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="id">
					<xsl:text>_currentSystemDate</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="value">
					<xsl:value-of select="$currentSystemDate"/>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template name="buildScheduledItemsGrid">
		<xsl:param name="gridTitle"/>
		<xsl:param name="gridOnloadFunction"/>
		<xsl:param name="gridCheckboxDeselectFunction"/>
		<xsl:param name="gridCheckboxSelectFunction"/>
		<xsl:param name="containerScreen"/>
		<div class="billingScheduledItemsList" id="billingScheduledItemsId">
			<xsl:call-template name="buildPagingGridPanel">
				<xsl:with-param name="gridID">
					<xsl:choose>
						<xsl:when test="$containerScreen='openItems'">
							<xsl:text>openItemsList</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>scheduledItemsList</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="dataOnLoad">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="targetPage">
					<xsl:choose>
						<xsl:when test="$containerScreen='openItems'">
							<xsl:text>openItemsList</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>scheduledItemsList</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="pageSize">25</xsl:with-param>
				<xsl:with-param name="recordCount">
					<xsl:text>OpenList/Paging/TotalCount</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordPath">
					<xsl:text>OpenList/OpenItem</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="recordID">
					<xsl:text>AgencyOpenItemsId</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columns">
					<xsl:text>[{name: 'PolicyTermId', type: 'string', mapping: 'PolicyTermId'},</xsl:text>
					<xsl:text>{name: 'PolicyReference', type: 'string', mapping: 'PolicyReference'},</xsl:text>
					<xsl:text>{name: 'InsuredId', type: 'string', mapping: 'InsuredId'},</xsl:text>
					<xsl:text>{name: 'InsuredName', type: 'string', mapping: 'InsuredName'},</xsl:text>
					<xsl:text>{name: 'TransactionTypeCode', type: 'string', mapping: 'TransactionTypeCode'},</xsl:text>
					<xsl:text>{name: 'EffectiveDate', type: 'date', dateFormat: 'Y-m-d', mapping: 'EffectiveDate'},</xsl:text>
					<xsl:text>{name: 'GrossAmount', type: 'string', mapping: 'GrossAmount'},</xsl:text>
					<xsl:text>{name: 'NetAmount', type: 'string', mapping: 'NetAmount'},</xsl:text>
					<xsl:text>{name: 'AmountDue', type: 'string', mapping: 'AmountDue'},</xsl:text>
					<xsl:text>{name: 'ItemReasonCode', type: 'string', mapping: 'ItemReasonCode'},</xsl:text>
					<xsl:text>{name: 'ItemCmtDescription', type: 'string', mapping: 'ItemComment/@Description'},</xsl:text>
					<xsl:text>{name: 'ItemCmtDate', type: 'date', dateFormat: 'Y-m-dTH:i:s.u', mapping: 'ItemComment/@CreationDate'},</xsl:text>
					<xsl:text>{name: 'ItemCmtOriginator', type: 'string', mapping: 'ItemComment/@Originator'},</xsl:text>
					<xsl:text>{name: 'ItemCmtCount', type: 'string', mapping: 'ItemComment/@CommentCount'},</xsl:text>
					<xsl:text>{name: 'ItemCmtSize', type: 'string', mapping: 'ItemComment/@CommentSize'},</xsl:text>
					<xsl:text>{name: 'currencyCulture', type: 'string', mapping: 'CurrencyCulture'},</xsl:text>
					<xsl:text>{name: 'currencySymbols', type: 'string', mapping: 'CurrencySymbols'},</xsl:text>
					<xsl:text>{name: 'lockingTS', type: 'string', mapping: 'ItemLockingTS'},</xsl:text>
					<xsl:text>{name: 'ItemObjectType', type: 'string', mapping: 'OriginatingTypeCode'},</xsl:text>
					<xsl:text>{name: 'ItemObjectId', type: 'string', mapping: 'OriginatingId'},</xsl:text>
					<xsl:text>{name: 'ItemStatusCode', type: 'string', mapping: 'ItemStatusCode'}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnModel">
					<xsl:text>[{text: DCT.T("PolicyNumberSymbol"), dataIndex: 'PolicyReference', width: 70, sortable: true, renderer: DCT.Grid.formatPolicyReferenceRenderer},</xsl:text>
					<xsl:text>{text: DCT.T("Insured"), dataIndex: 'InsuredName', width: 65, sortable: false},</xsl:text>
					<xsl:text>{text: DCT.T("Type"), dataIndex: 'TransactionTypeCode', width: 25, sortable: true},</xsl:text>
					<xsl:text>{text: DCT.T("EffDate"), dataIndex: 'EffectiveDate', width: 38, sortable: true, renderer: DCT.Grid.formatEffectiveDateRenderer},</xsl:text>
					<xsl:text>{text: DCT.T("Gross"), dataIndex: 'GrossAmount', width: 45, sortable: true, align: 'right'},</xsl:text>
					<xsl:text>{text: DCT.T("Net"), dataIndex: 'NetAmount', width: 45, sortable: true, align: 'right'},</xsl:text>
					<xsl:text>{text: DCT.T("Balance"), dataIndex: 'AmountDue', width: 45, sortable: true, align: 'right'},</xsl:text>
					<xsl:if test="$containerScreen='openItems'">
						<xsl:text>{text: DCT.T("Hold"), dataIndex: 'ItemReasonCode', width: 25, sortable: true},</xsl:text>
					</xsl:if>
					<xsl:text>{text: "", dataIndex: 'ItemCmtDescription', width: 10, sortable: false, renderer: DCT.Grid.formatItemCommentColumnRenderer}]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRow">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="previewRowRenderer">
					<xsl:text>DCT.Grid.scheduledItemsDescriptionRowRenderer</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="columnFiltering">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="filterArray">
					<xsl:text>[{type: 'string', dataIndex: 'PolicyReference'},</xsl:text>
					<xsl:text>{type: 'date', dataIndex: 'EffectiveDate', dateBefore: true, dateOn: true, dateFormat: 'Y-m-d'},</xsl:text>
					<xsl:text>{type: 'numeric', dataIndex: 'GrossAmount'},</xsl:text>
					<xsl:text>{type: 'numeric', dataIndex: 'NetAmount'},</xsl:text>
					<xsl:text>{type: 'numeric', dataIndex: 'AmountDue'},</xsl:text>
					<xsl:text>{type: 'list', dataIndex: 'TransactionTypeCode',options: [</xsl:text>
					<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_ITEMTRANTYPE']/ListEntry">
						<xsl:text>[</xsl:text>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="@Code"/>
						<xsl:text>'</xsl:text>
						<xsl:text>,</xsl:text>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="@Description"/>
						<xsl:text>']</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]}</xsl:text>
					<xsl:if test="$containerScreen='openItems'">
						<xsl:text>,{type: 'list', dataIndex: 'ItemReasonCode',options: [</xsl:text>
						<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_AGYITEMREASON']/ListEntry">
							<xsl:text>[</xsl:text>
							<xsl:text>'</xsl:text>
							<xsl:value-of select="@Code"/>
							<xsl:text>'</xsl:text>
							<xsl:text>,</xsl:text>
							<xsl:text>'</xsl:text>
							<xsl:value-of select="@Description"/>
							<xsl:text>']</xsl:text>
							<xsl:if test="position() != last()">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>

						<xsl:text>]}</xsl:text>
					</xsl:if>
					<xsl:text>]</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="toolBarTitle">
					<xsl:value-of select="$gridTitle"/>
				</xsl:with-param>
				<xsl:with-param name="showGridCheckbox">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="selectAllItems">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="multipleSelect">
					<xsl:text>true</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="deSelectEventFunc">
					<xsl:value-of select="$gridCheckboxDeselectFunction"/>
				</xsl:with-param>
				<xsl:with-param name="selectEventFunc">
					<xsl:value-of select="$gridCheckboxSelectFunction"/>
				</xsl:with-param>
				<xsl:with-param name="checkboxRenderFunc">
					<xsl:text>DCT.Grid.setScheduledItemSelectionCheckbox</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="beforeRowSelectEventFunc">
					<xsl:text>DCT.Grid.checkScheduledItemsSelectionCheckbox</xsl:text>
				</xsl:with-param>
				<xsl:with-param name="showToolBar">true</xsl:with-param>
				<xsl:with-param name="dataLoadEventFunc">
					<xsl:value-of select="$gridOnloadFunction"/>
				</xsl:with-param>
				<xsl:with-param name="initialShowPreview">false</xsl:with-param>
				<xsl:with-param name="adjustedCountFunc">
					<xsl:text>DCT.Grid.scheduledItemCheckboxAdjustedCount</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildSelectedItemsTotals">
		<xsl:param name="header"/>
		<xsl:param name="headColumn1"/>
		<xsl:param name="headColumn2"/>
		<xsl:param name="headColumn3"/>
		<xsl:param name="valueColumn1"/>
		<xsl:param name="valueColumn2"/>
		<xsl:param name="valueColumn3"/>
		<div id="billingSelectedTotals">
			<table id="tblBillingSelectedTotals" class="selectedTotalsTable">
				<tr class="selectedTotalsTableTitle">
					<td colspan="3">
						<xsl:value-of select="$header"/>
					</td>
				</tr>
				<tr class="selectedTotalsTableHdr">
					<td>
						<xsl:value-of select="$headColumn1"/>
					</td>
					<td>
						<xsl:value-of select="$headColumn2"/>
					</td>
					<td>
						<xsl:value-of select="$headColumn3"/>
					</td>
				</tr>
				<tr class="selectedTotalsTableRow">
					<div id="companyItemsId">
						<td>
							<xsl:value-of select="$valueColumn1"/>
						</td>
					</div>
					<div id="agentItemsId">
						<td>
							<xsl:value-of select="$valueColumn2"/>
						</td>
					</div>
					<div id="balanceId">
						<td>
							<xsl:value-of select="$valueColumn3"/>
						</td>
					</div>
				</tr>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildSelectedItemsList">
		<xsl:param name="tableTitle"/>
		<xsl:param name="pathToItemList"/>
		<xsl:param name="removeItemsButtonVisible"/>
		<xsl:param name="displayItemNumber"/>
		<div id="itemListDetail" tabletype="acrossAboveOnce">
			<div class="displayTblTitle">
				<xsl:value-of select="$tableTitle"/>
			</div>
			<table id="tblItemListDetail" class="displayTbl">
				<tr class="displayTblHdr">
					<xsl:if test="$displayItemNumber='true'">
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('ItemNumSymbol')"/>
						</td>
					</xsl:if>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('PolicyNumberSymbol')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Insured')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Type')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('EffDate')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Gross')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Comm1')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Rate')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Net')"/>
					</td>
					<td>
						<xsl:value-of select="xslNsODExt:getDictRes('Hold')"/>
					</td>
					<xsl:if test="$removeItemsButtonVisible='true'">
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('RemoveItem')"/>
						</td>
					</xsl:if>
				</tr>
				<xsl:for-each select="$pathToItemList/Item">
					<xsl:variable name="rowNumber">
						<xsl:value-of select="position()"/>
					</xsl:variable>
					<xsl:call-template name="buildSelectedItemListRows">
						<xsl:with-param name="currencyCulture" select="CurrencyCulture"/>
						<xsl:with-param name="itemListRowId" select="ItemId"/>
						<xsl:with-param name="rowNumber" select="$rowNumber"/>
						<xsl:with-param name="policyReference" select="PolicyReference"/>
						<xsl:with-param name="policyTermId" select="PolicyTermId"/>
						<xsl:with-param name="insured" select="InsuredName"/>
						<xsl:with-param name="transactionType" select="TransactionTypeCode"/>
						<xsl:with-param name="effectiveDate" select="EffectiveDate"/>
						<xsl:with-param name="actualGrossAmount" select="ActualGrossAmount"/>
						<xsl:with-param name="grossAmount" select="GrossAmount"/>
						<xsl:with-param name="actualCommission" select="ActualCommissionAmount"/>
						<xsl:with-param name="commission" select="CommissionAmount"/>
						<xsl:with-param name="rate" select="ItemCommissionPercent"/>
						<xsl:with-param name="actualNetAmount" select="ActualNetAmount"/>
						<xsl:with-param name="netAmount" select="NetAmount"/>
						<xsl:with-param name="reasonCode" select="ItemReasonCode"/>
						<xsl:with-param name="itemLockingTS" select="ItemLockingTS"/>
						<xsl:with-param name="itemTypeCode" select="ObjectTypeCode"/>
						<xsl:with-param name="itemNumber" select="ItemNumber"/>
						<xsl:with-param name="displayRemoveButton" select="$removeItemsButtonVisible"/>
						<xsl:with-param name="displayItemNumber" select="$displayItemNumber"/>
					</xsl:call-template>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
	<xsl:template name="buildSelectedItemListRows">
		<xsl:param name="currencyCulture"/>
		<xsl:param name="rowNumber"/>
		<xsl:param name="itemListRowId"/>
		<xsl:param name="policyReference"/>
		<xsl:param name="policyTermId"/>
		<xsl:param name="insured"/>
		<xsl:param name="transactionType"/>
		<xsl:param name="effectiveDate"/>
		<xsl:param name="actualGrossAmount"/>
		<xsl:param name="grossAmount"/>
		<xsl:param name="actualCommission"/>
		<xsl:param name="commission"/>
		<xsl:param name="rate"/>
		<xsl:param name="actualNetAmount"/>
		<xsl:param name="netAmount"/>
		<xsl:param name="reasonCode"/>
		<xsl:param name="itemLockingTS"/>
		<xsl:param name="itemTypeCode"/>
		<xsl:param name="itemNumber"/>
		<xsl:param name="displayRemoveButton"/>
		<xsl:param name="displayItemNumber"/>
		<tr class="displayTblRow">
			<xsl:if test="$displayItemNumber='true'">
				<td>
					<xsl:value-of select="$itemNumber"/>
				</td>
			</xsl:if>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemId</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemId</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemListRowId"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemLockingTS</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemLockingTS</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemLockingTS"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemTypeCd</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemTypeCode</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemTypeCode"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:value-of select="$policyReference"/>
			</td>
			<td>
				<xsl:value-of select="$insured"/>
			</td>
			<td>
				<xsl:value-of select="$transactionType"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', $effectiveDate)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $grossAmount)"/>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemGrossAmt</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemGrossAmt</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$actualGrossAmount"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $commission)"/>

				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemCommissionAmt</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemCommissionAmt</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$actualCommission"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'f2', $rate)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $netAmount)"/>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenItemNetAmt</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_selectItemNetAmt</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$actualNetAmount"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:value-of select="$reasonCode"/>
			</td>
			<xsl:if test="$displayRemoveButton='true'">
				<td>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">removeAction</xsl:with-param>
						<xsl:with-param name="id">removeAction</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.removeSelectedItem(</xsl:text>
							<xsl:value-of select="$itemListRowId"/>
							<xsl:text>, 'matchScheduledItems');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('RemoveSelectedItem')"/>
						</xsl:with-param>
						<xsl:with-param name="icon">decline.png</xsl:with-param>
						<xsl:with-param name="type">loneIcon</xsl:with-param>
					</xsl:call-template>
				</td>
			</xsl:if>
		</tr>
	</xsl:template>


	<xsl:template name="buildScheduledItemFilters">
		<xsl:param name="containerScreen"/>
		<xsl:param name="policyTerm"/>
		<xsl:param name="gridId"/>
		<xsl:variable name="insuredName">
			<xsl:call-template name="getInsuredPartyName"/>
		</xsl:variable>
		<div class="acrossLayout" id="scheduledItemFiltersGroup">
			<div id="mainFilterGroup">
				<xsl:if test="$policyTerm !='1'">
					<div id="aggregationFieldGroup" class="acrossFieldGroup">
						<div class="acrossFormLabel">
							<label>
								<xsl:text> </xsl:text>
							</label>
						</div>
						<div class="acrossFormField">
							<xsl:call-template name="buildSystemControl">
								<xsl:with-param name="id">aggregationField</xsl:with-param>
								<xsl:with-param name="name">_policyAggregation</xsl:with-param>
								<xsl:with-param name="type">select</xsl:with-param>
								<xsl:with-param name="optionlist">
									<option>
										<xsl:attribute name="value">
											<xsl:text> </xsl:text>
										</xsl:attribute>
										<xsl:value-of select="xslNsODExt:getDictRes('AllPrograms')"/>
									</option>
									<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_POLAGGREGATION']/ListEntry">
										<option>
											<xsl:attribute name="value">
												<xsl:value-of select="@Code"/>
											</xsl:attribute>
											<xsl:value-of select="@Description"/>
										</option>
									</xsl:for-each>
								</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="normalize-space(/page/content/ExpressCache/CachedRequestForm/_policyAggregation)!=''">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_policyAggregation"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text> </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
								<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
								<xsl:with-param name="extraConfigItems">
									<xsl:call-template name="addConfigProperty">
										<xsl:with-param name="name">dctActionButton</xsl:with-param>
										<xsl:with-param name="type">string</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:text>clearFilterAction</xsl:text>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:with-param>
							</xsl:call-template>
						</div>
					</div>
				</xsl:if>
				<xsl:if test="$containerScreen!='closedMatches'">
					<div id="subAgentFieldGroup" class="acrossFieldGroup">
						<div class="acrossFormLabel">
							<label>
								<xsl:text> </xsl:text>
							</label>
						</div>
						<div class="acrossFormField">
							<xsl:call-template name="buildSystemControl">
								<xsl:with-param name="id">subAgentsField</xsl:with-param>
								<xsl:with-param name="name">_subAgent</xsl:with-param>
								<xsl:with-param name="type">select</xsl:with-param>
								<xsl:with-param name="disabled">true</xsl:with-param>
								<xsl:with-param name="optionlist">
									<option>
										<xsl:attribute name="value">
											<xsl:text> </xsl:text>
										</xsl:attribute>
										<xsl:value-of select="xslNsODExt:getDictRes('AllSubAgents')"/>
									</option>
									<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_SUBAGENTS']/ListEntry">
										<option>
											<xsl:attribute name="value">
												<xsl:value-of select="@Code"/>
											</xsl:attribute>
											<xsl:value-of select="@Description"/>
										</option>
									</xsl:for-each>
								</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:choose>
										<xsl:when test="normalize-space(/page/content/ExpressCache/CachedRequestForm/_subAgent)!=''">
											<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_subAgent"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text> </xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:with-param>
								<xsl:with-param name="controlClass">basicEnablingComboField</xsl:with-param>
								<xsl:with-param name="extraConfigItems">
									<xsl:call-template name="addConfigProperty">
										<xsl:with-param name="name">dctActionButton</xsl:with-param>
										<xsl:with-param name="type">string</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:text>clearFilterAction</xsl:text>
										</xsl:with-param>
									</xsl:call-template>
								</xsl:with-param>
							</xsl:call-template>
						</div>
					</div>
				</xsl:if>
				<div id="moreFiltersFieldGroup" class="acrossFieldGroup">
					<div class="acrossFormLabel">
						<label>
							<xsl:text> </xsl:text>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">moreFiltersToggle</xsl:with-param>
							<xsl:with-param name="id">moreFiltersToggle</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Util.toggleSection('moreOptionsGroupPanelCmp');</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('MoreFilters')"/>
							</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
			</div>
			<div id="moreOptionsGroupPanel">
				<xsl:element name="div">
					<xsl:attribute name="class">collapsiblePanel</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>renderTo: "moreOptionsGroupPanel",</xsl:text>
						<xsl:text>id: "moreOptionsGroupPanelCmp",</xsl:text>
						<xsl:text>contentEl: "moreOptionsGroup",</xsl:text>
						<xsl:text>collapsed: </xsl:text>
						<xsl:choose>
							<xsl:when test="normalize-space($insuredName)=''">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</div>
			<div id="moreOptionsGroup" class="acrossLayout x-hidden">
				<div id="commissionPercentGroup" class="acrossFieldGroup">
					<div class="acrossFormLabel">
						<label id="commissionPercentCaption" for="commissionPercent">
							<xsl:value-of select="xslNsODExt:getDictRes('CommissionPercent')"/>
						</label>
					</div>
					<div class="acrossFormField">
						<xsl:call-template name="buildSystemControl">
							<xsl:with-param name="id">
								<xsl:text>commissionPercent</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="name">
								<xsl:text>_commissionPercent</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="type">text</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:if test="/page/content/ExpressCache/CachedRequestForm/_commissionAmount">
									<xsl:value-of select="/page/content/ExpressCache/CachedRequestForm/_commissionAmount"/>
								</xsl:if>
							</xsl:with-param>
							<xsl:with-param name="controlClass">commissionPercentFloatField</xsl:with-param>
							<xsl:with-param name="extraConfigItems">
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:value-of select="xslNsExt:numberFormatMaskToUse('??.##', /page/content/SystemDefaults/DefaultCultureCode/@value)"/>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctNumberMin</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>0.00</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctActionButton</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>clearFilterAction</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
								<xsl:call-template name="addConfigProperty">
									<xsl:with-param name="name">dctHiddenField</xsl:with-param>
									<xsl:with-param name="type">string</xsl:with-param>
									<xsl:with-param name="value">
										<xsl:text>hiddenCommissionPercent</xsl:text>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</div>
				<xsl:if test="normalize-space($insuredName)!=''">
					<div id="insuredGroup" class="acrossFieldGroup">
						<div class="acrossFormLabel">
							<label id="insuredCaption">
								<xsl:value-of select="xslNsODExt:getDictRes('Insured_colon')"/>
							</label>
						</div>
						<div class="acrossFormField">
							<xsl:value-of select="$insuredName"/>
						</div>
					</div>
				</xsl:if>
				<div id="searchForInsuredActionGroup">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">searchForInsuredAction</xsl:with-param>
						<xsl:with-param name="id">searchForInsuredAction</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.searchPartyForInsured();</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:choose>
								<xsl:when test="normalize-space($insuredName)=''">
									<xsl:text>Search for insured</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Change insured</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
		<div id="filterActionGroup">
			<div id="scheduledItemsFilterActions" class="g-btn-bar">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">applyFilterAction</xsl:with-param>
					<xsl:with-param name="id">applyFilterAction</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Grid.applyScheduledItemsFiltersToList(Ext.getCmp('</xsl:text>
						<xsl:value-of select="$gridId"/>
						<xsl:text>').getStore());</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('ApplyFilter')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">clearFilterAction</xsl:with-param>
					<xsl:with-param name="id">clearFilterAction</xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Grid.clearScheduledItemsFiltersToList(Ext.getCmp('</xsl:text>
						<xsl:value-of select="$gridId"/>
						<xsl:text>').getStore(), 'clearFilterAction');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Clear')"/>
					</xsl:with-param>
					<xsl:with-param name="class">
						<xsl:if test="normalize-space($insuredName)=''">btnDisabled</xsl:if>
					</xsl:with-param>
					<xsl:with-param name="type">cancel</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="getInsuredPartyName">
		<xsl:choose>
			<xsl:when test="normalize-space(/page/content/PartyRecord/Party/PartyCorrespondenceName)!=''">
				<xsl:value-of select="/page/content/PartyRecord/Party/PartyCorrespondenceName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="normalize-space(/page/content/PartyRecord/Party/PartyFirstName)!=''">
					<xsl:value-of disable-output-escaping="yes" select="/page/content/PartyRecord/Party/PartyFirstName"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:if test="normalize-space(/page/content/PartyRecord/Party/PartyMiddleName)!=''">
					<xsl:value-of disable-output-escaping="yes" select="/page/content/PartyRecord/Party/PartyMiddleName"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of disable-output-escaping="yes" select="/page/content/PartyRecord/Party/PartyName"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildScheduledItemStatusChange">
		<xsl:param name="applyActionField"/>
		<xsl:param name="actionField"/>
		<xsl:param name="itemId"/>
		<xsl:param name="fieldArray"/>
		<xsl:param name="focusField"/>
		<xsl:param name="gridId"/>
		<div id="removeItems" class="notificationDiv nWarn hideOffset">
			<div class="InformationDiv">
				<div class="notificationIcon"></div>
				<xsl:value-of select="xslNsODExt:getDictRes('RemoveSelectedItemsFromBill')"/>
				<div id="approvalGroup" class="g-btn-bar">
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">
							<xsl:choose>
								<xsl:when test="normalize-space($itemId)!=''">
									<xsl:text>DCT.Submit.processScheduledItems('</xsl:text>
									<xsl:value-of select="$actionField"/>
									<xsl:text>',null, '</xsl:text>
									<xsl:value-of select="$itemId"/>
									<xsl:text>');</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT.Submit.processScheduledItems('</xsl:text>
									<xsl:value-of select="$actionField"/>
									<xsl:text>',Ext.getCmp('</xsl:text>
									<xsl:value-of select="$gridId"/>
									<xsl:text>').getStore(), '');</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('YesRemove')"/>
						</xsl:with-param>
						<xsl:with-param name="name">_removeItem</xsl:with-param>
						<xsl:with-param name="id">_removeItem</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">
							<xsl:choose>
								<xsl:when test="normalize-space($itemId)!=''">
									<xsl:text>DCT.Util.cancelItemRemoval('</xsl:text>
									<xsl:value-of select="$focusField"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$fieldArray"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$itemId"/>
									<xsl:text>');</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT.Util.cancelItemRemoval('</xsl:text>
									<xsl:value-of select="$focusField"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$fieldArray"/>
									<xsl:text>',Ext.getCmp('</xsl:text>
									<xsl:value-of select="$gridId"/>
									<xsl:text>').getStore());</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Cancel')"/>
						</xsl:with-param>
						<xsl:with-param name="name">_cancelRemoval</xsl:with-param>
						<xsl:with-param name="id">_cancelRemoval</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template name="ExternalPartyPaymentFields">
		<input type="hidden" name="_acctRef" id="_acctRef">
			<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="normalize-space(/page/content/AccountRecord/AccountReference)!=''">
						<xsl:value-of select="/page/content/AccountRecord/AccountReference"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="/page/content/ExpressCache/AccountReference"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_partyName" id="_partyName" value="{/page/content/ExpressCache/DisburseName}"/>
		<input type="hidden" name="_paymentMethods" id="paymentMethods" value="{/page/state/Use3rdPartyPaymentMethods}"/>
		<input type="hidden" name="_billingInterfaces" id="_billingInterfaces" value="{/page/state/ExternalPaymentInterface}"/>
	</xsl:template>
	<xsl:template name="promisesToPayTableWidget">
		<xsl:param name="totalText"/>
		<xsl:param name="actionButton"/>
		<xsl:param name="titleText"/>
		<xsl:param name="screenType"/>
		<xsl:param name="totalRemaining"/>
		<xsl:param name="checkValidationFunction">null</xsl:param>
		<xsl:param name="messageNote"/>
		<xsl:variable name="currencyCulture">
			<xsl:value-of select="/page/content/PromiseToPayments/PromiseToPayment/CurrencyCulture"/>
		</xsl:variable>
		<div id="promiseToPayListGroup">
			<div id="promisesToPayList" tabletype="acrossAboveOnce">
				<div class="displayTblTitle">
					<xsl:choose>
						<xsl:when test="$screenType='allocateSuspensePayment'">
							<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2',$totalRemaining)"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="$titleText"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$titleText"/>
						</xsl:otherwise>
					</xsl:choose>
				</div>
				<table id="tblItemListDetail" class="displayTbl">
					<tr class="displayTblHdr">
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('AmountAppliedToday')"/>
						</td>
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('Created')"/>
						</td>
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('StatementMonth')"/>
						</td>
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('OriginalDue')"/>
						</td>
						<td>
							<xsl:value-of select="xslNsODExt:getDictRes('AmtOutstanding')"/>
						</td>

						<td width="0">
							<xsl:text></xsl:text>
						</td>
						<td width="0">
							<xsl:text></xsl:text>
						</td>
					</tr>
					<xsl:for-each select="/page/content/PromiseToPayments/PromiseToPayment">
						<xsl:variable name="rowNumber">
							<xsl:value-of select="position()"/>
						</xsl:variable>
						<xsl:call-template name="buildPromisesToPayListRows">
							<xsl:with-param name="itemListRowId" select="AgencyAgreementPaymentId"/>
							<xsl:with-param name="itemListRowTS" select="AgencyAgreementPaymentLockingTS"/>
							<xsl:with-param name="rowNumber" select="$rowNumber"/>
							<xsl:with-param name="actionButton" select="$actionButton"/>
							<xsl:with-param name="screenType" select="$screenType"/>
							<xsl:with-param name="defaultAmount">
								<xsl:text>0.00</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="createdDate" select="TransactionDate"/>
							<xsl:with-param name="statementMonth" select="StatementDate"/>
							<xsl:with-param name="originalAmtDue" select="PromiseAmount"/>
							<xsl:with-param name="outstandingAmt" select="OutstandingAmount"/>
							<xsl:with-param name="checkValidationFunction" select="$checkValidationFunction"/>
						</xsl:call-template>
					</xsl:for-each>
				</table>
				<div id="totalSection" class="displayTblFooter">
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">totalSuspenseAmtId</xsl:with-param>
						<xsl:with-param name="name">totalSuspenseAmt</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$totalRemaining"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">amountAllocatedTotalId</xsl:with-param>
						<xsl:with-param name="name">amountAllocatedTotal</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:text>0.00</xsl:text>
						</xsl:with-param>
					</xsl:call-template>
					<div id="displayTotalAmount">
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2','0.00')"/>
					</div>
					<div id="displayTotalTextId">
						<xsl:value-of select="xslNsODExt:getDictRes('Allocated')"/>
						<xsl:text> / </xsl:text>
					</div>
					<xsl:call-template name="buildHiddenInput">
						<xsl:with-param name="id">amountRemainingTotalId</xsl:with-param>
						<xsl:with-param name="name">amountRemainingTotal</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$totalRemaining"/>
						</xsl:with-param>
					</xsl:call-template>
					<div id="displayTotalRemAmount">
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter($currencyCulture, 'c2', $totalRemaining)"/>
					</div>
					<div id="displayTotalRemainingTextId">
						<xsl:value-of select="xslNsODExt:getDictRes('Remaining')"/>
					</div>
				</div>
			</div>
			<div id="messageNote">
				<xsl:value-of select="$messageNote"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="buildPromisesToPayListRows">
		<xsl:param name="rowNumber"/>
		<xsl:param name="itemListRowId"/>
		<xsl:param name="itemListRowTS"/>
		<xsl:param name="screenType"/>
		<xsl:param name="actionButton"/>
		<xsl:param name="defaultAmount"/>
		<xsl:param name="createdDate"/>
		<xsl:param name="statementMonth"/>
		<xsl:param name="originalAmtDue"/>
		<xsl:param name="outstandingAmt"/>
		<xsl:param name="checkValidationFunction">null</xsl:param>
		<tr class="displayTblRow">
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$defaultAmount"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenProcessDefaultAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenProcessDefaultAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$defaultAmount"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="formatToUse">
					<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-???????????.00', CurrencyCulture)"/>
				</xsl:variable>
				<xsl:call-template name="buildSystemControl">
					<xsl:with-param name="id">
						<xsl:text>_processAmountId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_processAmountName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="type">text</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(CurrencyCulture, 'c2', $defaultAmount)"/>
					</xsl:with-param>
					<xsl:with-param name="controlClass">allocationAmountFloatField</xsl:with-param>
					<xsl:with-param name="extraConfigItems">
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctNumberFormat</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="xslNsExt:numberFormatMaskToUse('$-??????????#.00', CurrencyCulture)"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:if test="number(OutstandingBalance) &gt; 0">
							<xsl:call-template name="addConfigProperty">
								<xsl:with-param name="name">dctNumberMax</xsl:with-param>
								<xsl:with-param name="type">string</xsl:with-param>
								<xsl:with-param name="value">
									<xsl:value-of select="$outstandingAmt"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:if>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctNumberMin</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:text>0.00</xsl:text>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctRowNumber</xsl:with-param>
							<xsl:with-param name="type">number</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$rowNumber"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctCheckFunction</xsl:with-param>
							<xsl:with-param name="type">object</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$checkValidationFunction"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctActionButton</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$actionButton"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenItemId_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenItemIdName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemListRowId"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>_hiddenItemTS_</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_hiddenItemTSName</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$itemListRowTS"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', $createdDate)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:convertISOtoDisplay($statementMonth, 'y')"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(CurrencyCulture, 'c2', $originalAmtDue)"/>
			</td>
			<td>
				<xsl:value-of select="xslNsExt:cultureAwareCurrencyFormatter(CurrencyCulture, 'c2', $outstandingAmt)"/>
			</td>
			<td>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenComment</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_itemCmtDescription</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="ItemComment/@Description"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="buildHiddenInput">
					<xsl:with-param name="id">
						<xsl:text>hiddenCmtTypeCode</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>_itemCmtTypeCode</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="value"/>
				</xsl:call-template>
				<div>
					<xsl:attribute name="id">
						<xsl:text>itemCommentActionGroup</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:attribute>
					<!--
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">commentAddAction</xsl:with-param>
						<xsl:with-param name="id">commentAddAction</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Util.displayAddBillingCommentsPopUp('', null, '', '', '</xsl:text>
							<xsl:text>hiddenComment</xsl:text>
							<xsl:value-of select="$rowNumber"/>
							<xsl:text>', '</xsl:text>
							<xsl:text>hiddenCmtTypeCode</xsl:text>
							<xsl:value-of select="$rowNumber"/>
							<xsl:text>', '</xsl:text>
							<xsl:text>itemViewCommentGroup</xsl:text>
							<xsl:value-of select="$rowNumber"/>
							<xsl:text>',true);</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="icon">comments.png</xsl:with-param>
						<xsl:with-param name="type">loneIcon</xsl:with-param>
					</xsl:call-template>
					-->
				</div>
			</td>
			<td>
				<div>
					<xsl:attribute name="id">
						<xsl:text>itemViewCommentGroup</xsl:text>
						<xsl:value-of select="$rowNumber"/>
					</xsl:attribute>
					<xsl:if test="normalize-space(ItemComment/@Description)=''">
						<xsl:attribute name="class">
							<xsl:text>hideOffset</xsl:text>
						</xsl:attribute>
					</xsl:if>
					<!--
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">commentViewAction</xsl:with-param>
						<xsl:with-param name="id">commentViewAction</xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Util.displayBillingComment(null, null, Ext.get('hiddenComment</xsl:text>
							<xsl:value-of select="$rowNumber"/>
							<xsl:text>').getValue());</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="icon">magnifier.png</xsl:with-param>
						<xsl:with-param name="type">loneIcon</xsl:with-param>
					</xsl:call-template>
					-->
				</div>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>