﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
	<xsl:import href="dctDocumentation.xsl"/>
	<xsl:import href="dctInterviewWidgets.xsl"/>
	<xsl:import href="dctInterviewPartySupport.xsl"/>
	<!-- To Override getHelpTabbingFlag template-->
	<xsl:variable name="DisableHelpTabbing">
		<xsl:call-template name="getHelpTabbingFlag"/>
	</xsl:variable>
	<!--*************************************************************************************************************
		This template is for getting the appropriate input field for the question (based on type).
		************************************************************************************************************* -->
	<xsl:template name="questionInput">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="actionID"/>
		<xsl:param name="direction"/>
		<xsl:param name="tableType"/>
		<xsl:variable name="extraConfigItems">
			<xsl:call-template name="addConfigProperty">
				<xsl:with-param name="name">dctSpotlight</xsl:with-param>
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="@spotlightID"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="readOnly">
			<xsl:call-template name="getReadOnly"/>
		</xsl:variable>
		<xsl:variable name="hasList">
			<xsl:call-template name="getHasList"/>
		</xsl:variable>
		<xsl:variable name="hideOn">
			<xsl:call-template name="getHideOn"/>
		</xsl:variable>
		<xsl:variable name="formatMask">
			<xsl:call-template name="getFormatMask"/>
		</xsl:variable>
		<xsl:variable name="fieldMask">
			<xsl:call-template name="getFieldMask"/>
		</xsl:variable>
		<xsl:variable name="fieldID">
			<xsl:call-template name="getId"/>
		</xsl:variable>
		<xsl:variable name="fieldOtherParms">
			<xsl:call-template name="getOtherParameters">
				<xsl:with-param name="type">
					<xsl:value-of select="@type"/>
				</xsl:with-param>
				<xsl:with-param name="fieldMask">
					<xsl:value-of select="$fieldMask"/>
				</xsl:with-param>
				<xsl:with-param name="formatMask">
					<xsl:value-of select="$formatMask"/>
				</xsl:with-param>
				<xsl:with-param name="maximum">
					<xsl:value-of select="@maximum"/>
				</xsl:with-param>
				<xsl:with-param name="minimum">
					<xsl:value-of select="@minimum"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="fieldRef_Val">
			<xsl:call-template name="getFieldRef"/>
		</xsl:variable>
		<xsl:variable name="objectRef_Val">
			<xsl:call-template name="getObjectRef"/>
		</xsl:variable>
		<xsl:if test="$showPathBubble='1'">
			<xsl:attribute name="onmouseover">
				<xsl:text>DCT.Util.showBubblePath('</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>', 'fieldID = ' + '</xsl:text>
				<xsl:value-of select="@fieldRef"/>
				<xsl:text>' + '</xsl:text>
				<xsl:text disable-output-escaping="yes">&lt;br&gt;path = </xsl:text>
				<xsl:text>' + '</xsl:text>
				<xsl:value-of select="@path"/>
				<xsl:text>')</xsl:text>
			</xsl:attribute>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@referenceAction!=''">
				<xsl:call-template name="buildReferenceActions">
					<xsl:with-param name="tableType" select="$tableType"/>
				</xsl:call-template>
			</xsl:when>
			<!--==================================================================================-->
			<!-- Option List                                                                      -->
			<!--==================================================================================-->
			<xsl:when test="$hasList='1' and substring(@formatMask,1,5)!='input'">
				<xsl:choose>
					<xsl:when test="$readOnly != '0'">
						<xsl:choose>
							<xsl:when test="../@displayType='slider'">
								<xsl:call-template name="buildSliderList">
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
									<xsl:with-param name="objectRef" select="$objectRef_Val"/>
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildReadOnlyField">
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
									<xsl:with-param name="objectRef" select="$objectRef_Val"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
									<xsl:with-param name="value">
										<xsl:choose>
											<xsl:when test="list/option/@cacheIndex and (/page/content/getPage/@currentPageID and (/page/content/getPage/@currentPageID != ''))">
												<xsl:value-of select="list/option[@cacheIndex=$value]/@caption"/>
											</xsl:when>
											<xsl:when test="list/option[@value=$value]/@caption != ''">
												<xsl:value-of select="list/option[@value=$value]/@caption"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="contains($formatMask, 'blankOption') and $value = ''"/>
													<xsl:otherwise>
														<xsl:value-of select="list/option[1]/@caption"/>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<!-- comparison widget display type overrides field format mask property -->
							<xsl:when test="../@displayType='radio'">
								<xsl:for-each select="./list">
									<xsl:call-template name="buildExtendedRadio">
										<xsl:with-param name="name" select="$name"/>
										<xsl:with-param name="value" select="$value"/>
										<xsl:with-param name="actionID" select="$actionID"/>
										<xsl:with-param name="fieldID" select="$fieldID"/>
										<xsl:with-param name="required" select="../@required"/>
										<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
										<xsl:with-param name="objectRef" select="$objectRef_Val"/>
										<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
										<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
									</xsl:call-template>
								</xsl:for-each>
							</xsl:when>
							<xsl:when test="(not(../@displayType) and substring(@formatMask,1,5)='radio')">
								<xsl:for-each select="./list">
									<xsl:call-template name="buildRadio">
										<xsl:with-param name="name" select="$name"/>
										<xsl:with-param name="value" select="$value"/>
										<xsl:with-param name="actionID" select="$actionID"/>
										<xsl:with-param name="fieldID" select="$fieldID"/>
										<xsl:with-param name="required" select="../@required"/>
										<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
										<xsl:with-param name="objectRef" select="$objectRef_Val"/>
										<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
										<xsl:with-param name="formatMask" select="$formatMask"/>
										<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
									</xsl:call-template>
								</xsl:for-each>
							</xsl:when>
							<!--extended drop down list comparison widget-->
							<xsl:when test="../@displayType='extended'">
								<xsl:call-template name="buildExtendedList">
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<!--slider bar comparison widget-->
							<xsl:when test="../@displayType='slider'">
								<xsl:call-template name="buildSliderList">
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
									<xsl:with-param name="objectRef" select="$objectRef_Val"/>
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildSelect">
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!--==================================================================================-->
			<!-- Boolean                                                                         -->
			<!--==================================================================================-->
			<xsl:when test="@type='boolean'">
				<xsl:choose>
					<xsl:when test="substring(@formatMask,1,5)='radio'">
						<xsl:call-template name="buildBooleanRadioButtons">
							<xsl:with-param name="actionID" select="$actionID"/>
							<xsl:with-param name="fieldID" select="$fieldID"/>
							<xsl:with-param name="required" select="@required"/>
							<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
							<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
							<xsl:with-param name="name" select="$name"/>
							<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
							<xsl:with-param name="value" select="$value"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="formatMask" select="$formatMask"/>
							<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildCheckBox">
							<xsl:with-param name="actionID" select="$actionID"/>
							<xsl:with-param name="fieldID" select="$fieldID"/>
							<xsl:with-param name="required" select="@required"/>
							<xsl:with-param name="caption" select="@caption"/>
							<xsl:with-param name="type" select="@type"/>
							<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
							<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
							<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
							<xsl:with-param name="name" select="$name"/>
							<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
							<xsl:with-param name="readOnly" select="$readOnly"/>
							<xsl:with-param name="value" select="$value"/>
							<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!--==================================================================================-->
			<!-- Everything Else                                                                 -->
			<!--==================================================================================-->
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$readOnly != '0'">
						<xsl:call-template name="buildReadOnlyField">
							<xsl:with-param name="fieldID" select="$fieldID"/>
							<xsl:with-param name="fieldRef" select="$fieldRef_Val"/>
							<xsl:with-param name="objectRef" select="$objectRef_Val"/>
							<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
							<xsl:with-param name="hideOn" select="$hideOn"/>
							<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
							<xsl:with-param name="value">
								<xsl:call-template name="FormatValue">
									<xsl:with-param name="value">
										<xsl:value-of select="$value"/>
									</xsl:with-param>
									<xsl:with-param name="hideOn">
										<xsl:value-of select="$hideOn"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="contains($fieldMask,'file')">
								<xsl:call-template name="buildInputFile">
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
									<xsl:with-param name="fieldMask" select="$fieldMask"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="contains($fieldMask,'richtextarea')">
								<xsl:call-template name="buildInputRichTextArea">
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
									<xsl:with-param name="fieldMask" select="$fieldMask"/>
									<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:when test="contains($fieldMask,'textarea')">
								<xsl:call-template name="buildInputTextArea">
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
									<xsl:with-param name="fieldMask" select="$fieldMask"/>
									<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildInput">
									<xsl:with-param name="actionID" select="$actionID"/>
									<xsl:with-param name="fieldID" select="$fieldID"/>
									<xsl:with-param name="required" select="@required"/>
									<xsl:with-param name="caption" select="@caption"/>
									<xsl:with-param name="type" select="@type"/>
									<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
									<xsl:with-param name="fieldMask" select="$fieldMask"/>
									<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
									<xsl:with-param name="formatMask" select="$formatMask"/>
									<xsl:with-param name="hideOn" select="$hideOn"/>
									<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
									<xsl:with-param name="name" select="$name"/>
									<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
									<xsl:with-param name="readOnly" select="$readOnly"/>
									<xsl:with-param name="value" select="$value"/>
									<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="@sharedID">
			<xsl:call-template name="buildSharedDataChange">
				<xsl:with-param name="fieldID" select="$fieldID"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<!--*************************************************************************************************************
		This template is for handling the field groups
		************************************************************************************************************* -->
	<xsl:template match="fieldGroup">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:if test="not(@eliminate = '1' or @eliminate = '-1')">
			<xsl:element name="div">
				<xsl:attribute name="class">
					<xsl:value-of select="$direction"/>
					<xsl:text>FieldGroup</xsl:text>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="$docMode='1'">
						<xsl:apply-templates select="*" mode="doc"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="*">
							<xsl:with-param name="sort">
								<xsl:value-of select="$sort"/>
							</xsl:with-param>
							<xsl:with-param name="direction">
								<xsl:value-of select="@direction"/>
							</xsl:with-param>
							<xsl:with-param name="captionPosition">
								<xsl:value-of select="@captionPosition"/>
							</xsl:with-param>
						</xsl:apply-templates>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!--*************************************************************************************************************
		This template is for building extended radio buttons from an option list
		************************************************************************************************************* -->
	<xsl:template name="buildExtendedRadio">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="actionID"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="fieldRef"/>
		<xsl:param name="objectRef"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="extraConfigItems"/>
		<div class="controlContainer">
			<xsl:attribute name="id">
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$fieldID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewRadioGroup</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>vertical: true</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>columns: 1</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>dctRequired: "</xsl:text>
					<xsl:choose>
						<xsl:when test="$required='1'">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
					<xsl:text>",</xsl:text>
					<xsl:text>dctRadioButtons: [</xsl:text>
					<xsl:for-each select="option">
						<xsl:variable name="truncatedRadioValue">
							<xsl:choose>
								<xsl:when test="@cacheIndex and (/page/content/getPage/@currentPageID and (/page/content/getPage/@currentPageID != ''))">
									<xsl:value-of select="@cacheIndex"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@value"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:text>{</xsl:text>
						<xsl:text>id: "</xsl:text>
						<xsl:value-of select="$fieldID"/>
						<xsl:text>_</xsl:text>
						<xsl:value-of select="count(preceding-sibling::option)"/>
						<xsl:text>",</xsl:text>
						<xsl:text>dctExtendedWidget: true</xsl:text>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctCaption</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="@caption"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">dctAffectedValue</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:apply-templates select="@caption" mode="buildComparisonCaption"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>,</xsl:text>
						<xsl:text>name: "</xsl:text>
						<xsl:value-of select="$name"/>
						<xsl:text>"</xsl:text>
						<xsl:call-template name="addConfigProperty">
							<xsl:with-param name="name">inputValue</xsl:with-param>
							<xsl:with-param name="type">string</xsl:with-param>
							<xsl:with-param name="value">
								<xsl:value-of select="$truncatedRadioValue"/>
							</xsl:with-param>
						</xsl:call-template>
						<xsl:text>,</xsl:text>
						<xsl:text>ctCls: "</xsl:text>
						<xsl:if test="@special='true'">
							<xsl:text>optSpecialContainer</xsl:text>
						</xsl:if>
						<xsl:text>",</xsl:text>
						<xsl:text>itemCls: "</xsl:text>
						<xsl:if test="$value=$truncatedRadioValue">
							<xsl:text>optSpecialSelected</xsl:text>
						</xsl:if>
						<xsl:if test="@special='true'">
							<xsl:text> optSpecialRadio</xsl:text>
						</xsl:if>
						<xsl:text>",</xsl:text>
						<xsl:text>checked: </xsl:text>
						<xsl:choose>
							<xsl:when test="$value=$truncatedRadioValue">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="$includeDebugInfo = 'true'">
							<xsl:text>,</xsl:text>
							<xsl:text>fieldRef: "</xsl:text>
							<xsl:value-of select="$fieldRef"/>
							<xsl:text>",</xsl:text>
							<xsl:text>objectRef: "</xsl:text>
							<xsl:value-of select="$objectRef"/>
							<xsl:text>"</xsl:text>
						</xsl:if>
						<xsl:if test="$actionID!=''">
							<xsl:text>,</xsl:text>
							<xsl:text>dctActionId: "</xsl:text>
							<xsl:value-of select="$actionID"/>
							<xsl:text>"</xsl:text>
						</xsl:if>
						<xsl:if test="$extraConfigItems">
							<xsl:value-of select="$extraConfigItems"/>
						</xsl:if>
						<xsl:call-template name="addCustomExtendedRadioProperties"/>
						<xsl:text>}</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]</xsl:text>
					<xsl:call-template name="addCustomExtendedRadioGroupProperties"/>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<!--*************************************************************************************************************
		This template is for	building radio buttons from an option list
		************************************************************************************************************* -->
	<xsl:template name="buildRadio">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="actionID"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="fieldRef"/>
		<xsl:param name="objectRef"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="extraConfigItems"/>
		<div class="controlContainer">
			<xsl:call-template name="buildRadioDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="actionID" select="$actionID"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="formatMask" select="$formatMask"/>
				<xsl:with-param name="class">interviewRadioGroup</xsl:with-param>
				<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
				<xsl:with-param name="radiobuttons">
					<xsl:call-template name="buildInterviewRadioButtons">
						<xsl:with-param name="actionID" select="$actionID"/>
						<xsl:with-param name="fieldID" select="$fieldID"/>
						<xsl:with-param name="fieldRef_Val" select="$fieldRef"/>
						<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="objectRef_Val" select="$objectRef"/>
						<xsl:with-param name="value" select="$value"/>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for	the dialog.  It is the kick-off point for all the questionGroup
		processing.
		************************************************************************************************************* -->
	<xsl:template match="body">
		<xsl:apply-templates select="*"/>
		<xsl:if test="$rateMessageShowBar">
			<xsl:call-template name="buildRatingMessages"/>
		</xsl:if>
		<xsl:call-template name="buildPrintFailureMessages"/>
	</xsl:template>

	<xsl:template name="buildRatingMessages">
		<xsl:if test="@calculate &gt; 0 and /page/content/getPage/messages/message">
			<xsl:variable name="ccType">ErrorPanel</xsl:variable>
			<div class="cc-bubble cc_{$ccType}">
				<xsl:attribute name="class">
					<xsl:text>cc-bubble cc_ErrorPanel</xsl:text>
				</xsl:attribute>
				<div class="cc-bubble-tl">
					<div class="cc-bubble-tr">
						<div class="cc-bubble-tc"></div>
					</div>
				</div>
				<div class="cc-bubble-bwrap">
					<div class="cc-bubble-ml">
						<div class="cc-bubble-mr">
							<div class="cc-bubble-mc">
								<div class="cc-bubble-body cc_{@cc}_body">
									<div id="ratemessages">
										<xsl:element name="div">
											<xsl:attribute name="class">ratingMessages</xsl:attribute>
											<xsl:attribute name="data-config">
												<xsl:text>{id: "ratingMsgToolBar"}</xsl:text>
											</xsl:attribute>
										</xsl:element>
										<h4>
											<xsl:value-of select="xslNsODExt:getDictRes('RatingMessages')"/>
										</h4>
										<ul id="justMessages" class="rateMsgList">
											<xsl:for-each select="/page/content/getPage/messages/message">
												<li class="rateFlag_{@flag}">
													<xsl:value-of select="text()"/>
												</li>
											</xsl:for-each>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="cc-bubble-bl">
						<div class="cc-bubble-br">
							<div class="cc-bubble-bc"></div>
						</div>
					</div>
				</div>
			</div>
		</xsl:if>
	</xsl:template>

	<xsl:template name="buildPrintFailureMessages">
		<xsl:if test="//printJobValidation/validation[@result='failure']">
			<div id="printjob">
				<h2>
					<xsl:value-of select="xslNsODExt:getDictRes('PrintJobValidation')"/>
				</h2>
				<div class="printerrors">
					<xsl:for-each select="//printJobValidation/validation[@result='failure']/required">
						<p>
							<xsl:value-of select="xslNsExt:FormatString2(xslNsODExt:getDictRes('FieldInFormRequired_Phrasing'), @field, @form)"/>
						</p>
					</xsl:for-each>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
	These templates are for the panels.
	************************************************************************************************************* -->
	<xsl:template name="ProcessPanel">
		<div class="{@class}">
			<xsl:attribute name="id">
				<xsl:text>inner</xsl:text>
				<xsl:value-of select="@position"/>
			</xsl:attribute>
			<xsl:apply-templates select="*"/>
		</div>
	</xsl:template>
	<xsl:template match="panel">
		<xsl:call-template name="ProcessPanel"/>
	</xsl:template>
	<xsl:template match="panel[@position = 'header' or @position = 'firstheader' or @position = 'secondheader' or @position = 'footer' or @position = 'leftpanel' or @position = 'leftpanelpopup' or @position = 'rightpanel' or @position = 'toprightpanel' or @position = 'submenupanel']">
		<!-- Do nothing because we don't want to have these output here. -->
	</xsl:template>
	<!--	*************************************************************************************************************
		group is the general wrapper
		************************************************************************************************************* -->
	<xsl:template match="group">
		<xsl:param name="tabledata"/>
		<xsl:choose>
			<xsl:when test="$docMode='1'">
				<xsl:apply-templates select="*" mode="doc"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="*">
					<xsl:with-param name="startIndex">
						<xsl:value-of select="@startIndex"/>
					</xsl:with-param>
					<xsl:with-param name="maxItems">
						<xsl:value-of select="@maxItems"/>
					</xsl:with-param>
					<xsl:with-param name="listCount">
						<xsl:value-of select="@listCount"/>
					</xsl:with-param>
					<xsl:with-param name="groupIndex">
						<xsl:value-of select="@index"/>
					</xsl:with-param>
					<xsl:with-param name="tabledata" select="$tabledata"/>
				</xsl:apply-templates>
				<xsl:call-template name="groupActions">
					<xsl:with-param name="tableData" select="$tabledata"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="GetTableType">
		<xsl:choose>
			<xsl:when test="@direction='down' and @captionPosition='before' and @showCaption='once'">
				<xsl:text>downBeforeOnce</xsl:text>
			</xsl:when>
			<xsl:when test="@direction='down' and @captionPosition='before' and @showCaption='repeat'">
				<xsl:text>downBeforeRepeat</xsl:text>
			</xsl:when>
			<xsl:when test="@direction='down' and @captionPosition='above' and @showCaption='repeat'">
				<xsl:text>downAboveRepeat</xsl:text>
			</xsl:when>
			<xsl:when test="@direction='across' and @captionPosition='above' and @showCaption='once'">
				<xsl:text>acrossAboveOnce</xsl:text>
			</xsl:when>
			<xsl:when test="@direction='across' and @captionPosition='above' and @showCaption='repeat'">
				<xsl:text>acrossAboveRepeat</xsl:text>
			</xsl:when>
			<xsl:when test="@direction='across' and @captionPosition='before' and @showCaption='repeat'">
				<xsl:text>acrossBeforeRepeat</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		groupActions
		************************************************************************************************************* -->
	<xsl:template name="groupActions">
		<xsl:param name="tableData"/>
		<xsl:if test="actions">
			<!-- Check on the processing message-->
			<xsl:if test="count(actions/action[@enabled='1']) &gt; 0">
				<xsl:choose>
					<xsl:when test="$tableData='1'">
						<td>
							<xsl:call-template name="buildEachAction"/>
						</td>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildEachAction"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template name="buildEachAction">
		<xsl:for-each select="actions/action">
			<xsl:if test="@enabled='1'">
				<div class="g-groupAction">
					<xsl:choose>
						<xsl:when test="server/requests/ManuScript.getPageRq/@command='add' and server/requests/ManuScript.getPageRq/@object = ../../@id">
							<xsl:variable name="tableType">
								<xsl:for-each select="../../tableLayout">
									<xsl:call-template name="GetTableType"/>
								</xsl:for-each>
							</xsl:variable>
							<xsl:call-template name="buildLink">
								<xsl:with-param name="name" select="@uid"/>
								<xsl:with-param name="value" select="@caption"/>
								<xsl:with-param name="ignoreValidation" select="./server/requests/ManuScript.getPageRq/@ignoreValidation"/>
								<xsl:with-param name="cancelChanges" select="./server/requests/ManuScript.getPageRq/@cancelChanges"/>
								<xsl:with-param name="processingMsg" select="./server/requests/ManuScript.getPageRq/@processingMsg"/>
								<xsl:with-param name="tableType" select="$tableType"/>
								<xsl:with-param name="tableId" select="../../tableLayout/@uid"/>
								<xsl:with-param name="class" select="@actClass"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="buildLink">
								<xsl:with-param name="name" select="@uid"/>
								<xsl:with-param name="value" select="@caption"/>
								<xsl:with-param name="ignoreValidation" select="./server/requests/ManuScript.getPageRq/@ignoreValidation"/>
								<xsl:with-param name="cancelChanges" select="./server/requests/ManuScript.getPageRq/@cancelChanges"/>
								<xsl:with-param name="processingMsg" select="./server/requests/ManuScript.getPageRq/@processingMsg"/>
								<xsl:with-param name="class" select="@actClass"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--*************************************************************************************************************
		Initializes all of the Interview-specific Ext.OnReady events.
		************************************************************************************************************* -->
	<xsl:template name="buildInterviewOnReady">
	</xsl:template>

	<!--*************************************************************************************************************
	Tabbed Section:
	creates a div wrapper that we will target (applyTo) with the javascript.
	this is a container for the individual tabs, which match on singular tab='1'
	************************************************************************************************************* -->
	<xsl:template match="section[@tabs='1']">
		<xsl:variable name="id">
			<xsl:value-of select="@uid"/>
			<xsl:text>_tabs</xsl:text>
		</xsl:variable>

		<xsl:element name="div">
			<xsl:attribute name="class">tabsContainer</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="$id"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">tabs</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$id"/>
					<xsl:text>"</xsl:text>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
			<!--
			this hidden class moves the content to an unviewable area while the tabs are built
			-->
			<xsl:element name="div">
				<xsl:attribute name="class">x-hidden-offsets</xsl:attribute>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!--*************************************************************************************************************
	Tab for a Tabbed Section:
	creates a div wrapper for the tab which is then consumed by the Tabbed Section
	note: we set the class to x-tab because extJs can then automatically add it
	as a tab if we choose by setting "autotabs: true" in the Tabbed section.
	************************************************************************************************************* -->
	<xsl:template match="section[@tab='1']">

		<xsl:element name="div">
			<xsl:attribute name="class">x-tab</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="@uid"/>
				<xsl:text>_tab</xsl:text>
			</xsl:attribute>

			<!-- todo: do we need to handle more/different caption[s] here? -->
			<xsl:attribute name="title">
				<xsl:value-of select="@toggleCaption"/>
			</xsl:attribute>

			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!--	*************************************************************************************************************
	section defines the section.  Example:
	<section direction="down" captionPosition="above" showCaption="once" caption="General Info">
	************************************************************************************************************* -->
	<xsl:template match="section">
		<xsl:param name="startIndex"/>
		<xsl:param name="maxItems"/>
		<xsl:param name="listCount"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="groupIndex"/>
		<xsl:choose>
			<xsl:when test="$tabledata='1'">
				<xsl:element name="td">
					<xsl:attribute name="class">
						<xsl:value-of select="@cellStyle"/>
					</xsl:attribute>
					<xsl:call-template name="processSection">
						<xsl:with-param name="startIndex" select="$startIndex"/>
						<xsl:with-param name="maxItems" select="$maxItems"/>
						<xsl:with-param name="listCount" select="$listCount"/>
						<xsl:with-param name="groupIndex" select="$groupIndex"/>
					</xsl:call-template>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="processSection">
					<xsl:with-param name="startIndex" select="$startIndex"/>
					<xsl:with-param name="maxItems" select="$maxItems"/>
					<xsl:with-param name="listCount" select="$listCount"/>
					<xsl:with-param name="groupIndex" select="$groupIndex"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		tableLayout defines the layout of the tables rendered from getPage.
		************************************************************************************************************* -->
	<xsl:template match="tableLayout">
		<xsl:param name="startIndex"/>
		<xsl:param name="maxItems"/>
		<xsl:param name="listCount"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="groupIndex"/>
		<xsl:variable name="tableType">
			<xsl:call-template name="GetTableType"/>
		</xsl:variable>


		<xsl:choose>

			<xsl:when test="$tabledata='1'">
				<xsl:element name="td">
					<xsl:attribute name="class">
						<xsl:value-of select="@cellStyle"/>
					</xsl:attribute>
					<xsl:call-template name="processTable">
						<xsl:with-param name="startIndex" select="$startIndex"/>
						<xsl:with-param name="maxItems" select="$maxItems"/>
						<xsl:with-param name="listCount" select="$listCount"/>
						<xsl:with-param name="tabledata" select="$tabledata"/>
						<xsl:with-param name="tableType" select="$tableType"/>
						<xsl:with-param name="groupIndex" select="$groupIndex"/>
					</xsl:call-template>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>

				<xsl:call-template name="processTable">
					<xsl:with-param name="startIndex" select="$startIndex"/>
					<xsl:with-param name="maxItems" select="$maxItems"/>
					<xsl:with-param name="listCount" select="$listCount"/>
					<xsl:with-param name="tabledata" select="$tabledata"/>
					<xsl:with-param name="tableType" select="$tableType"/>
					<xsl:with-param name="groupIndex" select="$groupIndex"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="XHTMLWidget">
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:choose>
					<xsl:when test="@identifier">
						<xsl:value-of select="@identifier"/>
					</xsl:when>
					<xsl:when test="@id">
						<xsl:value-of select="@id"/>
					</xsl:when>
					<xsl:when test="@uid">
						<xsl:value-of select="@uid"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>mainBody</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="@class">
						<xsl:value-of select="@class"/>
					</xsl:when>
					<xsl:when test="@classname">
						<xsl:value-of select="@classname"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>defaultXHTML</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<h2>
				<xsl:choose>
					<xsl:when test="@captionClass">
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="normalize-space(@caption)!=''">
									<xsl:value-of select="@captionClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@captionClass"/>
									<xsl:text> emptyCaption</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</xsl:when>
					<xsl:when test="not(normalize-space(@caption)!='')">
						<xsl:attribute name="class">
							<xsl:text>emptyCaption</xsl:text>
						</xsl:attribute>
					</xsl:when>
				</xsl:choose>
				<xsl:value-of select="@caption"/>
			</h2>
			<xsl:value-of disable-output-escaping="yes" select="@content"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="xhtml">
		<!-- If widget is part of a table then enclose it with td element-->
		<xsl:choose>
			<xsl:when test="name(parent::node())='data'">
				<xsl:element name="td">
					<xsl:call-template name="XHTMLWidget">
					</xsl:call-template>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="XHTMLWidget">
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--*************************************************************************************************************
		This template builds and formats the table.
		************************************************************************************************************* -->
	<xsl:template name="processTable">
		<xsl:param name="startIndex"/>
		<xsl:param name="maxItems"/>
		<xsl:param name="listCount"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableType"/>
		<xsl:param name="groupIndex"/>

		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:choose>
					<xsl:when test="@identifier">
						<xsl:value-of select="@identifier"/>
					</xsl:when>
					<xsl:when test="@id">
						<xsl:value-of select="@id"/>
					</xsl:when>
					<xsl:when test="@uid">
						<xsl:value-of select="@uid"/>
						<xsl:text>_tableDivId</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>mainBody</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="@direction='across'">
						<xsl:text>acrossLayout</xsl:text>
					</xsl:when>
					<xsl:when test="@direction='down'">
						<xsl:text>downLayout</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>defaultLayout</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="@class">
						<xsl:value-of select="@class"/>
					</xsl:when>
					<xsl:when test="@classname">
						<xsl:value-of select="@classname"/>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="@readOnly='1'">
					<xsl:text> isReadOnlyContent</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="normalize-space(@caption)!='' and @captionPosition='before' and @style='across' and @showCaption='none'">
					<h2>
						<xsl:if test="@captionClass">
							<xsl:attribute name="class">
								<xsl:value-of select="@captionClass"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@caption"/>
					</h2>
				</xsl:when>
				<xsl:when test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down'"/>
				<!-- Do Nothing Now -->
				<xsl:when test="normalize-space(@caption)!=''">
					<h2>
						<xsl:if test="@captionClass">
							<xsl:attribute name="class">
								<xsl:value-of select="@captionClass"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@caption"/>
					</h2>
				</xsl:when>
			</xsl:choose>
			<xsl:if test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down'">
				<!-- Processing was skipped above, process now -->
				<h2>
					<xsl:if test="@captionClass">
						<xsl:attribute name="class">
							<xsl:value-of select="@captionClass"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="@caption"/>
				</h2>
			</xsl:if>
			<table class="formTable">
				<xsl:attribute name="id">
					<xsl:value-of select="@uid"/>
				</xsl:attribute>
				<xsl:if test="$tableType!=''">
					<xsl:attribute name="tabletype">
						<xsl:value-of select="$tableType"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:apply-templates select="*">
					<xsl:with-param name="direction">
						<xsl:value-of select="@direction"/>
					</xsl:with-param>
					<xsl:with-param name="table">
						<xsl:text>1</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="tableType">
						<xsl:value-of select="$tableType"/>
					</xsl:with-param>
				</xsl:apply-templates>
			</table>
			<xsl:if test="$startIndex!=''">
				<input type="hidden" name="_iterationStartIndex" id="_iterationStartIndex">
					<xsl:attribute name="value">
						<xsl:value-of select="$startIndex"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_iterationMaxItems" id="_maxItems">
					<xsl:attribute name="value">
						<xsl:value-of select="$maxItems"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_iterationListCount" id="_listCount">
					<xsl:attribute name="value">
						<xsl:value-of select="$listCount"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_pageChange" id="_pageChange"/>
				<div id="paging" class="g-btn-bar">
					<xsl:if test="$startIndex &gt; 1">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">previousPage</xsl:with-param>
							<xsl:with-param name="id">previousPage</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.interviewPaging(</xsl:text>
								<xsl:value-of select="$startIndex - $maxItems"/>
								<xsl:text>, 'interview', '</xsl:text>
								<xsl:value-of select="$groupIndex"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('PrevPage')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$startIndex + $maxItems &lt; $listCount + 1">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">nextPage</xsl:with-param>
							<xsl:with-param name="id">nextPage</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.interviewPaging(</xsl:text>
								<xsl:value-of select="$startIndex + $maxItems"/>
								<xsl:text>, 'interview', '</xsl:text>
								<xsl:value-of select="$groupIndex"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('NextPage')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
				</div>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<!--*************************************************************************************************************
		This template builds and formats the sections.
		************************************************************************************************************* -->
	<xsl:template name="processSection">
		<xsl:param name="startIndex"/>
		<xsl:param name="maxItems"/>
		<xsl:param name="listCount"/>
		<xsl:param name="groupIndex"/>
		<xsl:variable name="divId">
			<xsl:choose>
				<xsl:when test="@identifier">
					<xsl:value-of select="@identifier"/>
				</xsl:when>
				<xsl:when test="@id">
					<xsl:value-of select="@id"/>
				</xsl:when>
				<xsl:when test="@uid">
					<xsl:value-of select="@uid"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@collapsible='1'">
			<xsl:element name="div">
				<xsl:choose>
					<xsl:when test="(/page/content/getPage/body/@popUp and /page/content/getPage/body/@popUp!='0') or (/page/popUp and /page/popUp!='0')">
						<xsl:attribute name="class">
							<xsl:text>toggleHeader</xsl:text>
							<xsl:if test="@startClosed='1'">
								<xsl:text> toggle-collapsed</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:value-of select="$divId"/>
							<xsl:text>_toggleHeader</xsl:text>
						</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">
							<xsl:text>toggleHeader</xsl:text>
							<xsl:if test="@startClosed='1'">
								<xsl:text> toggle-collapsed</xsl:text>
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:value-of select="$divId"/>
							<xsl:text>_toggleHeader</xsl:text>
						</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:attribute name="onclick">
					<xsl:text>DCT.Util.toggleInterviewSection('</xsl:text>
					<xsl:value-of select="$divId"/>
					<xsl:text>toggleItem</xsl:text>
					<xsl:text>');return false; </xsl:text>
				</xsl:attribute>
				<xsl:element name="div">
					<xsl:attribute name="class">
						<xsl:text>toggleHeaderInner</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="id">
						<xsl:value-of select="$divId"/>
						<xsl:text>_toggleHeaderInner</xsl:text>
					</xsl:attribute>
					<xsl:value-of select="@toggleCaption"/>
				</xsl:element>
			</xsl:element>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@collapsible='1'">
				<xsl:element name="div">
					<xsl:attribute name="id">
						<xsl:value-of select="$divId"/>
						<xsl:text>toggleItem</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="class">
						<xsl:text>collapsibleSection</xsl:text>
						<xsl:if test="@startClosed='1'">
							<xsl:text> x-hidden</xsl:text>
						</xsl:if>
					</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>id: "</xsl:text>
						<xsl:value-of select="$divId"/>
						<xsl:text>toggleItem</xsl:text>
						<xsl:text>",</xsl:text>
						<xsl:text>collapsed: </xsl:text>
						<xsl:value-of select="@startClosed='1'"/>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
					<xsl:call-template name="buildSectionDiv">
						<xsl:with-param name="divId" select="$divId"/>
						<xsl:with-param name="docMode" select="$docMode"/>
						<xsl:with-param name="listCount" select="$listCount"/>
						<xsl:with-param name="maxItems" select="$maxItems"/>
						<xsl:with-param name="startIndex" select="$startIndex"/>
						<xsl:with-param name="groupIndex" select="$groupIndex"/>
					</xsl:call-template>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@cc!=''">
				<xsl:element name="div">
					<xsl:attribute name="class">
						<xsl:text>cc-bubble </xsl:text>
						<xsl:text>cc_</xsl:text>
						<xsl:value-of select="@cc"/>
					</xsl:attribute>
					<div class="cc-bubble-tl">
						<div class="cc-bubble-tr">
							<div class="cc-bubble-tc">
								<xsl:if test="@panelHeader!=''">
									<div>
										<xsl:attribute name="class">
											<xsl:text>cc-bubble-title </xsl:text>
											<xsl:choose>
												<xsl:when test="@alignHeader='center'">x_TextAlignCenter</xsl:when>
												<xsl:when test="@alignHeader='right'">x_TextAlignRight</xsl:when>
												<!--otherwise aligns left by default-->
											</xsl:choose>
										</xsl:attribute>
										<span>
											<xsl:value-of select="@panelHeader"/>
										</span>
									</div>
								</xsl:if>
							</div>
						</div>
					</div>
					<div class="cc-bubble-bwrap">
						<div class="cc-bubble-ml">
							<div class="cc-bubble-mr">
								<div class="cc-bubble-mc">
									<div class="cc-bubble-body cc_{@cc}_body">
										<xsl:call-template name="buildSectionDiv">
											<xsl:with-param name="divId" select="$divId"/>
											<xsl:with-param name="docMode" select="$docMode"/>
											<xsl:with-param name="listCount" select="$listCount"/>
											<xsl:with-param name="maxItems" select="$maxItems"/>
											<xsl:with-param name="startIndex" select="$startIndex"/>
											<xsl:with-param name="groupIndex" select="$groupIndex"/>
										</xsl:call-template>
									</div>
								</div>
							</div>
						</div>
						<div class="cc-bubble-bl">
							<div class="cc-bubble-br">
								<div class="cc-bubble-bc">
								</div>
							</div>
						</div>
					</div>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildSectionDiv">
					<xsl:with-param name="divId" select="$divId"/>
					<xsl:with-param name="docMode" select="$docMode"/>
					<xsl:with-param name="listCount" select="$listCount"/>
					<xsl:with-param name="maxItems" select="$maxItems"/>
					<xsl:with-param name="startIndex" select="$startIndex"/>
					<xsl:with-param name="groupIndex" select="$groupIndex"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildSectionDiv">
		<xsl:param name="divId"/>
		<xsl:param name="docMode"/>
		<xsl:param name="startIndex"/>
		<xsl:param name="maxItems"/>
		<xsl:param name="listCount"/>
		<xsl:param name="groupIndex"/>
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="$divId"/>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="@direction='across'">
						<xsl:text>acrossLayout</xsl:text>
					</xsl:when>
					<xsl:when test="@direction='down'">
						<xsl:text>downLayout</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>defaultLayout</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<!--
					If all of the fields have beem marked to be eliminated, then the "emptyLayout" class should be added.
					The provided XPath expression is as simplified as possible.
				-->
				<xsl:if test="count(fieldInstance) &gt; 0 and count(fieldInstance[not(normalize-space(@eliminate) != '' and normalize-space(@eliminate) != '0')]) = 0">
					<xsl:text>emptyLayout </xsl:text>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="@class">
						<xsl:value-of select="@class"/>
					</xsl:when>
					<xsl:when test="@classname">
						<xsl:value-of select="@classname"/>
					</xsl:when>
				</xsl:choose>
				<xsl:if test="@readOnly='1'">
					<xsl:text> isReadOnlyContent</xsl:text>
				</xsl:if>
			</xsl:attribute>



			<xsl:variable name="doSort">
				<xsl:if test="contains(@style,'Sortable')">1</xsl:if>
			</xsl:variable>
			<xsl:choose>
				<xsl:when test="name(parent::node())='widget' and parent::node()/@widgetType='accordion'">
					<!-- Do Not display the Accordion Section Header, since it appears on the accordion bar. -->
				</xsl:when>
				<xsl:when test="normalize-space(@caption)!='' and @captionPosition='before' and @style='across' and @showCaption='none' and not(@validRef='False')">
					<xsl:call-template name="buildSectionCaption"/>
				</xsl:when>
				<xsl:when test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down'"/>
				<xsl:when test="normalize-space(@caption)!='' and not(@validRef='False')">
					<xsl:call-template name="buildSectionCaption"/>
				</xsl:when>
			</xsl:choose>
			<xsl:if test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down' and not(@validRef='False')">
				<!-- Processing was skipped above, process now -->
				<xsl:call-template name="buildSectionCaption"/>
			</xsl:if>
			<xsl:call-template name="buildExternalModulePanel">
				<xsl:with-param name="type" select="@staticContentType"/>
			</xsl:call-template>
			<xsl:if test="annotations">
				<ul class="annotations">
					<li>
						<xsl:apply-templates select="annotations" mode="now"/>
					</li>
				</ul>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$docMode='1'">
					<xsl:apply-templates select="*" mode="doc"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="*">
						<xsl:with-param name="sort">
							<xsl:value-of select="$doSort"/>
						</xsl:with-param>
						<xsl:with-param name="direction">
							<xsl:value-of select="@direction"/>
						</xsl:with-param>
						<xsl:with-param name="captionPosition">
							<xsl:value-of select="@captionPosition"/>
						</xsl:with-param>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$startIndex!=''">
				<input type="hidden" name="_iterationStartIndex" id="_iterationStartIndex">
					<xsl:attribute name="value">
						<xsl:value-of select="$startIndex"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_iterationMaxItems" id="_maxItems">
					<xsl:attribute name="value">
						<xsl:value-of select="$maxItems"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_iterationListCount" id="_listCount">
					<xsl:attribute name="value">
						<xsl:value-of select="$listCount"/>
					</xsl:attribute>
				</input>
				<input type="hidden" name="_pageChange" id="_pageChange"/>
				<div id="paging" class="g-btn-bar">
					<xsl:if test="$startIndex &gt; 1">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">previousPage</xsl:with-param>
							<xsl:with-param name="id">previousPage</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.interviewPaging(</xsl:text>
								<xsl:value-of select="$startIndex - $maxItems"/>
								<xsl:text>, 'interview', '</xsl:text>
								<xsl:value-of select="$groupIndex"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('PrevPage')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
					<xsl:if test="$startIndex + $maxItems &lt; $listCount + 1">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">nextPage</xsl:with-param>
							<xsl:with-param name="id">nextPage</xsl:with-param>
							<xsl:with-param name="onclick">
								<xsl:text>DCT.Submit.interviewPaging(</xsl:text>
								<xsl:value-of select="$startIndex + $maxItems"/>
								<xsl:text>, 'interview', '</xsl:text>
								<xsl:value-of select="$groupIndex"/>
								<xsl:text>');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('NextPage')"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:if>
				</div>
			</xsl:if>
			<xsl:if test="contains(@style,'hyperlink')">
				<div id="backTop">
					<a href="#topOfDoc" id="backToTop">
						<div id="linkText">
							<xsl:value-of select="xslNsODExt:getDictRes('BackToTop')"/>
						</div>
					</a>
				</div>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<xsl:template name="buildSectionCaption">
		<h2>
			<xsl:if test="@captionClass">
				<xsl:attribute name="class">
					<xsl:value-of select="@captionClass"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@icon">
				<img src="{$imageDir}icons/{@icon}" class="sectionCaptionIcon"/>
			</xsl:if>
			<xsl:value-of select="@caption"/>
		</h2>
	</xsl:template>
	<!--*************************************************************************************************************
	row defines the table row
	************************************************************************************************************* -->
	<xsl:template match="row">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="table"/>
		<xsl:param name="tableType"/>
		<xsl:variable name="rowdata">
			<xsl:copy-of select="descendant::*"/>
		</xsl:variable>
		<xsl:if test="xslNsExt:hasChildren($rowdata) = 'Yes'">
			<xsl:choose>
				<xsl:when test="$direction = 'down'">
					<xsl:if test="count(*/*[@placeholder='1']) != count(*/*)">
						<tr>
							<xsl:apply-templates select="*">
								<xsl:with-param name="sort">
									<xsl:value-of select="$sort"/>
								</xsl:with-param>
								<xsl:with-param name="direction">
									<xsl:value-of select="$direction"/>
								</xsl:with-param>
								<xsl:with-param name="table">
									<xsl:value-of select="$table"/>
								</xsl:with-param>
								<xsl:with-param name="tableType">
									<xsl:value-of select="$tableType"/>
								</xsl:with-param>
							</xsl:apply-templates>
						</tr>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<xsl:apply-templates select="*">
							<xsl:with-param name="sort">
								<xsl:value-of select="$sort"/>
							</xsl:with-param>
							<xsl:with-param name="direction">
								<xsl:value-of select="$direction"/>
							</xsl:with-param>
							<xsl:with-param name="table">
								<xsl:value-of select="$table"/>
							</xsl:with-param>
							<xsl:with-param name="tableType">
								<xsl:value-of select="$tableType"/>
							</xsl:with-param>
						</xsl:apply-templates>
					</tr>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!--*************************************************************************************************************
	header defines the table header cell
	************************************************************************************************************* -->
	<xsl:template match="header">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="table"/>
		<xsl:param name="tableType"/>
		<xsl:apply-templates select="*">
			<xsl:with-param name="sort">
				<xsl:value-of select="$sort"/>
			</xsl:with-param>
			<xsl:with-param name="direction">
				<xsl:value-of select="$direction"/>
			</xsl:with-param>
			<xsl:with-param name="table">
				<xsl:value-of select="$table"/>
			</xsl:with-param>
			<xsl:with-param name="tableheader">
				<xsl:text>1</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="tableType">
				<xsl:value-of select="$tableType"/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<!--*************************************************************************************************************
	data defines the table data cell
	************************************************************************************************************* -->
	<xsl:template match="data">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="table"/>
		<xsl:param name="tableType"/>
		<xsl:apply-templates select="*">
			<xsl:with-param name="sort">
				<xsl:value-of select="$sort"/>
			</xsl:with-param>
			<xsl:with-param name="direction">
				<xsl:value-of select="$direction"/>
			</xsl:with-param>
			<xsl:with-param name="table">
				<xsl:value-of select="$table"/>
			</xsl:with-param>
			<xsl:with-param name="tabledata">
				<xsl:text>1</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="tableType">
				<xsl:value-of select="$tableType"/>
			</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<!--*************************************************************************************************************
	caption
	************************************************************************************************************* -->
	<xsl:template match="caption[@placeholder='1']">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:param name="table"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableheader"/>
		<xsl:variable name="index" select="@index"/>
		<xsl:variable name="xpath">
			<xsl:text></xsl:text>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$table='1' and ../../..//row/data/*[@index=./@index and not(@placeholder)]">

				<xsl:choose>
					<xsl:when test="$tableheader='1'">
						<th class="thEmpty">
						</th>
					</xsl:when>
					<xsl:when test="$tabledata='1'">
						<td class="tdEmpty">
						</td>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="caption">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:param name="table"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableheader"/>
		<xsl:choose>
			<xsl:when test="$table='1'">
				<xsl:variable name="required" select="../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@required"/>
				<xsl:variable name="eliminate">
					<xsl:choose>
						<xsl:when test="@eliminate">
							<xsl:value-of select="@eliminate"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@eliminate"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="for">
					<xsl:value-of select="@uid"/>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$tableheader='1' and not($eliminate = '1' or $eliminate = '-1')">
						<th>
							<xsl:call-template name="createLabel">
								<xsl:with-param name="required">
									<xsl:value-of select="$required"/>
								</xsl:with-param>
								<xsl:with-param name="eliminate">
									<xsl:value-of select="$eliminate"/>
								</xsl:with-param>
								<xsl:with-param name="for">
									<xsl:value-of select="$for"/>
								</xsl:with-param>
								<xsl:with-param name="sort">
									<xsl:value-of select="$sort"/>
								</xsl:with-param>
								<xsl:with-param name="table">
									<xsl:value-of select="$table"/>
								</xsl:with-param>
							</xsl:call-template>
						</th>
					</xsl:when>
					<xsl:when test="$tabledata='1' and not($eliminate = '1' or $eliminate = '-1')">
						<td>
							<xsl:call-template name="createLabel">
								<xsl:with-param name="required">
									<xsl:value-of select="$required"/>
								</xsl:with-param>
								<xsl:with-param name="eliminate">
									<xsl:value-of select="$eliminate"/>
								</xsl:with-param>
								<xsl:with-param name="for">
									<xsl:value-of select="$for"/>
								</xsl:with-param>
								<xsl:with-param name="sort">
									<xsl:value-of select="$sort"/>
								</xsl:with-param>
								<xsl:with-param name="table">
									<xsl:value-of select="$table"/>
								</xsl:with-param>
							</xsl:call-template>
						</td>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="fieldRef" select="current()/@fieldRef"/>
				<xsl:variable name="fieldGroupIndex" select="current()/@index"/>
				<xsl:if test="((normalize-space(@value)!= '') or (../fieldInstance[@index=$fieldGroupIndex]/@fieldRef=$fieldRef)) and (not(../fieldInstance[@index=$fieldGroupIndex]/@eliminate = '1' or ../fieldInstance[@index=$fieldGroupIndex]/@eliminate = '-1'))">
					<xsl:variable name="required" select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@required"/>
					<xsl:variable name="eliminate">
						<xsl:choose>
							<xsl:when test="@eliminate">
								<xsl:value-of select="@eliminate"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@eliminate"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="for">
						<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@uid"/>
					</xsl:variable>
					<xsl:if test="../fieldInstance[@fieldRef=$fieldRef]">
						<xsl:text disable-output-escaping="yes">&lt;div class="</xsl:text>
						<xsl:value-of select="$direction"/>
						<xsl:text disable-output-escaping="yes">FieldGroup" id="</xsl:text> 
						<xsl:value-of select="$direction"/>
						<xsl:text disable-output-escaping="yes">FieldGroup</xsl:text>
						<xsl:value-of select="@uid"/>
						<xsl:text disable-output-escaping="yes">"&gt;</xsl:text>
					</xsl:if>
					<xsl:call-template name="createLabel">
						<xsl:with-param name="required">
							<xsl:value-of select="$required"/>
						</xsl:with-param>
						<xsl:with-param name="eliminate">
							<xsl:value-of select="$eliminate"/>
						</xsl:with-param>
						<xsl:with-param name="for">
							<xsl:value-of select="$for"/>
						</xsl:with-param>
						<xsl:with-param name="sort">
							<xsl:value-of select="$sort"/>
						</xsl:with-param>
						<xsl:with-param name="table">
							<xsl:value-of select="$table"/>
						</xsl:with-param>
						<xsl:with-param name="direction">
							<xsl:value-of select="$direction"/>
						</xsl:with-param>
						<xsl:with-param name="captionPosition">
							<xsl:value-of select="$captionPosition"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--*************************************************************************************************************
		This template builds and formats the labels.
		************************************************************************************************************* -->
	<xsl:template name="createLabel">
		<xsl:param name="required"/>
		<xsl:param name="eliminate">0</xsl:param>
		<xsl:param name="for"/>
		<xsl:param name="sort"/>
		<xsl:param name="table"/>
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:if test="not($eliminate = '1' or $eliminate = '-1')">
			<xsl:choose>
				<xsl:when test="$table='1'">
					<xsl:call-template name="outputLabel">
						<xsl:with-param name="required">
							<xsl:value-of select="$required"/>
						</xsl:with-param>
						<xsl:with-param name="for">
							<xsl:value-of select="$for"/>
						</xsl:with-param>
						<xsl:with-param name="sort">
							<xsl:value-of select="$sort"/>
						</xsl:with-param>
						<xsl:with-param name="table">
							<xsl:value-of select="$table"/>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@caption"/>
						</xsl:with-param>
						<xsl:with-param name="annotations">
							<xsl:choose>
								<xsl:when test="@fieldRef and ../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@annotations = '1' or @annotations = '1'">
									<xsl:text>1</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>0</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="hideAnnotation">
							<xsl:value-of select="../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@hideAnnotation"/>
						</xsl:with-param>
						<xsl:with-param name="AjaxHelp">
							<xsl:value-of select="../../..//row/data/fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@hoverHelp"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<div>
						<xsl:call-template name="outputLabel">
							<xsl:with-param name="required">
								<xsl:value-of select="$required"/>
							</xsl:with-param>
							<xsl:with-param name="for">
								<xsl:value-of select="$for"/>
							</xsl:with-param>
							<xsl:with-param name="sort">
								<xsl:value-of select="$sort"/>
							</xsl:with-param>
							<xsl:with-param name="table">
								<xsl:value-of select="$table"/>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@caption"/>
							</xsl:with-param>
							<xsl:with-param name="annotations">
								<xsl:choose>
									<xsl:when test="@fieldRef and ../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@annotations = '1' or @annotations = '1'">
										<xsl:text>1</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>0</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:with-param>
							<xsl:with-param name="hideAnnotation">
								<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@hideAnnotation"/>
							</xsl:with-param>
							<xsl:with-param name="AjaxHelp">
								<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@hoverHelp"/>
							</xsl:with-param>
							<xsl:with-param name="direction">
								<xsl:value-of select="$direction"/>
							</xsl:with-param>
							<xsl:with-param name="captionPosition">
								<xsl:value-of select="$captionPosition"/>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="outputLabel">
		<xsl:param name="required"/>
		<xsl:param name="for"/>
		<xsl:param name="sort"/>
		<xsl:param name="table"/>
		<xsl:param name="caption"/>
		<xsl:param name="annotations"/>
		<xsl:param name="hideAnnotation"/>
		<xsl:param name="AjaxHelp"/>
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:variable name="cacheID">
			<xsl:choose>
				<xsl:when test="$table='1'">
					<xsl:value-of select="../../..//fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@cacheID"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@cacheID"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="indexID">
			<xsl:choose>
				<xsl:when test="$table='1'">
					<xsl:value-of select="../../..//fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@index"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@index"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="icon">
			<xsl:choose>
				<xsl:when test="$table='1'">
					<xsl:value-of select="../../..//fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@icon"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@icon"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$table='1'">
				<xsl:variable name="fieldInstanceForCaption" select="../../../row/data/fieldInstance[@fieldRef=current()/@fieldRef and @index=current()/@index]"/>
				<!-- <xsl:variable name="icon">
					<xsl:if test="$fieldInstanceForCaption/@icon!=''">
						<xsl:value-of select="$fieldInstanceForCaption/@icon"/>
					</xsl:if>
				</xsl:variable> -->
				<xsl:attribute name="class">
					<xsl:if test="../../../@capClass">
						<xsl:text> </xsl:text>
						<xsl:value-of select="../../../@capClass"/>
					</xsl:if>
					<xsl:if test="$fieldInstanceForCaption/@capClass">
						<xsl:text> </xsl:text>
						<xsl:value-of select="$fieldInstanceForCaption/@capClass"/>
					</xsl:if>
				</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<!-- 5/9/08 pinkleju issue 82068: Added the "and" index check on the select below so two fields w/same caption can still be uniquely identified. -->
				<xsl:variable name="fieldInstanceForCaption" select="../fieldInstance[@fieldRef=current()/@fieldRef and @index=current()/@index]"/>
				<xsl:variable name="fieldCaption" select="../caption[@fieldRef=current()/@fieldRef and @index=current()/@index]"/>
				<!-- <xsl:variable name="icon">
					<xsl:if test="$fieldInstanceForCaption/@icon!=''">
						<xsl:value-of select="$fieldInstanceForCaption/@icon"/>
					</xsl:if>
				</xsl:variable> -->
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="$fieldInstanceForCaption">
							<xsl:value-of select="$direction"/>
							<xsl:choose>
								<xsl:when test="$captionPosition = 'before'">
									<xsl:text>Before</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Above</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>FormLabel</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$direction"/>
							<xsl:choose>
								<xsl:when test="$captionPosition = 'before'">
									<xsl:text>Before</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>Above</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>FormLabelNoField</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="../@capClass">
						<xsl:text> </xsl:text>
						<xsl:value-of select="../@capClass"/>
					</xsl:if>
					<xsl:if test="$fieldCaption/@capClass">
						<xsl:text> </xsl:text>
						<xsl:value-of select="$fieldCaption/@capClass"/>
					</xsl:if>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="@value != ''">
			<label>
				<xsl:attribute name="id">
					<xsl:value-of select="@uid"/>
				</xsl:attribute>
				<xsl:choose>
					<xsl:when test="normalize-space($for)!='_'">
						<xsl:if test="$table!='1'">
							<xsl:attribute name="for">
								<xsl:value-of select="$for"/>
							</xsl:attribute>
						</xsl:if>
					</xsl:when>
				</xsl:choose>
				<xsl:attribute name="class">
					<xsl:if test="$required='1'">
						<xsl:text>requiredField </xsl:text>
					</xsl:if>
					<xsl:if test="$icon!=''">
						<xsl:text> </xsl:text>
						<xsl:text>labelWithIcon </xsl:text>
					</xsl:if>
					<xsl:if test="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@truncateCaption='1'">
						<xsl:text> </xsl:text>
						<xsl:text>truncateWithDots</xsl:text>
					</xsl:if>
				</xsl:attribute>
				<xsl:if test="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@truncateCaption='1'">
					<xsl:attribute name="data-qtip">
						<xsl:value-of select="@value"/>
					</xsl:attribute>
					<xsl:attribute name="data-tip">
						<xsl:value-of select="@value"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:variable name="display">
					<xsl:if test="$icon!=''">
						<img src="{$imageDir}icons/{$icon}" class="fieldCaptionIcon" alt="{@value}" data-qtip="{@value}" data-tip="{@value}"/>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="@value">
							<xsl:value-of select="@value"/>
							<xsl:if test="$required='1'">
								<span class="required">
									<xsl:text>*</xsl:text>
								</span>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$caption"/>
							<xsl:if test="$required='1'">
								<span class="required">
									<xsl:text>*</xsl:text>
								</span>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="@value">
						<xsl:choose>
							<xsl:when test="@value='' or @value='delete'">
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test=" not($annotations='0') and(normalize-space($annotations)!='') and not($hideAnnotation='1' or /page/content/getPage/body/@hideAnnotations='1')">
										<xsl:choose>
											<xsl:when test="$helpImage=''">
												<xsl:choose>
													<xsl:when test="$AjaxHelp='1' or /page/content/getPage/body/@hoverHelp='1'">
														<xsl:element name="a">
															<xsl:if test="$DisableHelpTabbing='1'">
																<xsl:attribute name="tabIndex">-1</xsl:attribute>
															</xsl:if>
															<xsl:attribute name="href">javascript:;</xsl:attribute>
															<xsl:attribute name="onclick">
																<xsl:text>DCT.Util.doAnnotationPopup('</xsl:text>
																<xsl:value-of select="@fieldRef"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="$cacheID"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/actions[@type='page']/action[@command='getField']/@uid"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/@currentPageID"/>
																<xsl:text>');</xsl:text>
															</xsl:attribute>
															<xsl:attribute name="onMouseOver">
																<xsl:text disable-output-escaping="yes">return DCT.ajaxProcess.doAjaxHelp(this,'</xsl:text>
																<xsl:value-of select="@fieldRef"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="$cacheID"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/actions[@type='page']/action[@command='getField']/@uid"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/@currentPageID"/>
																<xsl:text>','</xsl:text>
																<xsl:choose>
																	<xsl:when test="@value">
																		<xsl:value-of select="@value"/>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:value-of select="$caption"/>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:text disable-output-escaping="yes">');</xsl:text>
															</xsl:attribute>
															<xsl:attribute name="id">
																<xsl:text>labelanchor_</xsl:text>
																<xsl:value-of select="$indexID"/>
															</xsl:attribute>
															<xsl:if test="$DisableHelpTabbing!='1'">
																<xsl:attribute name="onfocus">
																	<xsl:text>DCT.Util.setFocusField('</xsl:text>
																	<xsl:text>labelanchor_</xsl:text>
																	<xsl:value-of select="$indexID"/>
																	<xsl:text>');</xsl:text>
																</xsl:attribute>
															</xsl:if>
															<xsl:attribute name="onMouseMove">
																<xsl:text>window.status='';</xsl:text>
															</xsl:attribute>
															<xsl:if test="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@truncateCaption='1'">
																<xsl:attribute name="data-qtip">
																	<xsl:value-of select="@value"/>
																</xsl:attribute>
																<xsl:attribute name="data-tip">
																	<xsl:value-of select="@value"/>
																</xsl:attribute>
															</xsl:if>
															<xsl:copy-of select="$display"/>
														</xsl:element>
													</xsl:when>
													<xsl:otherwise>
														<xsl:element name="a">
															<xsl:attribute name="href">javascript:;</xsl:attribute>
															<xsl:if test="$DisableHelpTabbing='1'">
																<xsl:attribute name="tabIndex">-1</xsl:attribute>
															</xsl:if>
															<xsl:attribute name="onclick">
																<xsl:text>DCT.Util.doAnnotationPopup('</xsl:text>
																<xsl:value-of select="@fieldRef"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="$cacheID"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/actions[@type='page']/action[@command='getField']/@uid"/>
																<xsl:text>','</xsl:text>
																<xsl:value-of select="/page/content/getPage/@currentPageID"/>
																<xsl:text>');</xsl:text>
															</xsl:attribute>
															<xsl:attribute name="id">
																<xsl:text>labelanchor_</xsl:text>
																<xsl:value-of select="$indexID"/>
															</xsl:attribute>
															<xsl:if test="$DisableHelpTabbing!='1'">
																<xsl:attribute name="onfocus">
																	<xsl:text>DCT.Util.setFocusField('</xsl:text>
																	<xsl:text>labelanchor_</xsl:text>
																	<xsl:value-of select="$indexID"/>
																	<xsl:text>');</xsl:text>
																</xsl:attribute>
															</xsl:if>
															<xsl:attribute name="onmousemove">
																<xsl:text>window.status='';</xsl:text>
															</xsl:attribute>
															<xsl:attribute name="onmouseout">
																<xsl:text>window.status='';</xsl:text>
															</xsl:attribute>
															<xsl:if test="../fieldInstance[@objectRef=current()/@objectRef and @index=current()/@index]/@truncateCaption='1'">
																<xsl:attribute name="data-qtip">
																	<xsl:value-of select="@value"/>
																</xsl:attribute>
																<xsl:attribute name="data-tip">
																	<xsl:value-of select="@value"/>
																</xsl:attribute>
															</xsl:if>
															<xsl:copy-of select="$display"/>
														</xsl:element>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="$display"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="$sort='1'">
												<a id="sort" href="javascript:;">
													<xsl:attribute name="onclick">
														<xsl:text>Sort('</xsl:text>
														<xsl:value-of select="position()"/>
														<xsl:text>');</xsl:text>
													</xsl:attribute>
													<xsl:copy-of select="$display"/>
												</a>
											</xsl:when>
											<xsl:otherwise>
												<xsl:copy-of select="$display"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="$sort='1'">
								<a id="sort" href="javascript:;">
									<xsl:attribute name="onclick">
										<xsl:text>Sort('</xsl:text>
										<xsl:value-of select="position()"/>
										<xsl:text>');</xsl:text>
									</xsl:attribute>
									<xsl:copy-of select="$display"/>
								</a>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="$display"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</label>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		fieldInstance
		************************************************************************************************************* -->
	<xsl:template name="fieldInstance" match="fieldInstance">
		<xsl:param name="sort"/>
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:param name="table"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableheader"/>
		<xsl:param name="tableType"/>
		<xsl:if test="$sort=1">
			<xsl:attribute name="id">
				<xsl:value-of select="position()"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$table='1'">
				<xsl:choose>
					<xsl:when test="$tableheader='1' and not(@eliminate = '1' or @eliminate = '-1')">
						<th>
							<xsl:call-template name="createField">
								<xsl:with-param name="direction">
									<xsl:value-of select="$direction"/>
								</xsl:with-param>
								<xsl:with-param name="table">
									<xsl:value-of select="$table"/>
								</xsl:with-param>
							</xsl:call-template>
						</th>
					</xsl:when>
					<xsl:when test="$tabledata='1' and not(@eliminate = '1' or @eliminate = '-1')">
						<td>
							<xsl:call-template name="createField">
								<xsl:with-param name="direction">
									<xsl:value-of select="$direction"/>
								</xsl:with-param>
								<xsl:with-param name="table">
									<xsl:value-of select="$table"/>
								</xsl:with-param>
								<xsl:with-param name="tableType">
									<xsl:value-of select="$tableType"/>
								</xsl:with-param>
							</xsl:call-template>
						</td>
					</xsl:when>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not(@eliminate='1' or @eliminate='-1')">
					<!-- Print N/A if $applicable='0', Otherwise, process field instance -->
					<xsl:call-template name="createField">
						<xsl:with-param name="direction">
							<xsl:value-of select="$direction"/>
						</xsl:with-param>
						<xsl:with-param name="captionPosition">
							<xsl:value-of select="$captionPosition"/>
						</xsl:with-param>
						<xsl:with-param name="table">
							<xsl:value-of select="$table"/>
						</xsl:with-param>
					</xsl:call-template>
					<!-- Revome this code once switch to new page XML-->
					<xsl:variable name="fieldRef" select="current()/@fieldRef"/>
					<xsl:variable name="fieldGroupIndex" select="current()/@index"/>
					<xsl:if test="(normalize-space(../caption[@index=$fieldGroupIndex]/@value)!= '') or (../caption[@index=$fieldGroupIndex]/@fieldRef=$fieldRef)">
						<xsl:text disable-output-escaping="yes">&lt;/div&gt;</xsl:text>
					</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="createField">
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:param name="table"/>
		<xsl:param name="tableType"/>
		<xsl:if test="not(@eliminate = '1' or @eliminate = '-1')">
			<xsl:choose>
				<xsl:when test="$table='1'">
					<xsl:choose>
						<xsl:when test="@referenceAction!=''">
							<ul>
								<xsl:attribute name="class">
									<xsl:text>tableAction</xsl:text>
								</xsl:attribute>
								<xsl:call-template name="outputField">
									<xsl:with-param name="direction">
										<xsl:value-of select="$direction"/>
									</xsl:with-param>
									<xsl:with-param name="tableType">
										<xsl:value-of select="$tableType"/>
									</xsl:with-param>
								</xsl:call-template>
							</ul>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">
								<xsl:if test="../../../@fldClass">
									<xsl:text> </xsl:text>
									<xsl:value-of select="../../../@fldClass"/>
								</xsl:if>
								<xsl:if test="@fldClass">
									<xsl:text> </xsl:text>
									<xsl:value-of select="@fldClass"/>
								</xsl:if>
							</xsl:attribute>
							<xsl:if test="@qtip">
								<xsl:attribute name="title">
									<xsl:value-of select="@qtip"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:call-template name="outputField">
								<xsl:with-param name="direction">
									<xsl:value-of select="$direction"/>
								</xsl:with-param>
								<xsl:with-param name="tableType">
									<xsl:value-of select="$tableType"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="@referenceAction!=''">
					<div>
						<xsl:attribute name="class">
							<xsl:value-of select="$direction"/>
							<xsl:text>FormField</xsl:text>
						</xsl:attribute>
						<ul class="action">
							<xsl:call-template name="outputField">
								<xsl:with-param name="direction">
									<xsl:value-of select="$direction"/>
								</xsl:with-param>
								<xsl:with-param name="captionPosition">
									<xsl:value-of select="$captionPosition"/>
								</xsl:with-param>
							</xsl:call-template>
						</ul>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<div>
						<xsl:attribute name="class">
							<xsl:value-of select="$direction"/>
							<xsl:text>FormField</xsl:text>
							<xsl:if test="../@fldClass">
								<xsl:text> </xsl:text>
								<xsl:value-of select="../@fldClass"/>
							</xsl:if>
							<xsl:if test="@fldClass">
								<xsl:text> </xsl:text>
								<xsl:value-of select="@fldClass"/>
							</xsl:if>
							<xsl:if test="../@widgetType='comparison' and ../@class">
								<xsl:text> </xsl:text>
								<xsl:value-of select="../@class"/>
							</xsl:if>
						</xsl:attribute>
						<xsl:call-template name="outputField">
							<xsl:with-param name="direction">
								<xsl:value-of select="$direction"/>
							</xsl:with-param>
							<xsl:with-param name="captionPosition">
								<xsl:value-of select="$captionPosition"/>
							</xsl:with-param>
						</xsl:call-template>
					</div>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="outputField">
		<xsl:param name="direction"/>
		<xsl:param name="captionPosition"/>
		<xsl:param name="tableType"/>
		<xsl:choose>
			<xsl:when test="@applicable='0'">
				<xsl:element name="label">
					<xsl:attribute name="id">
						<xsl:value-of select="@uid"/>
						<xsl:text>_Label</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="for">
						<xsl:value-of select="@uid"/>
					</xsl:attribute>
					<xsl:element name="span">
						<xsl:attribute name="id">
							<xsl:value-of select="@uid"/>
						</xsl:attribute>
						<xsl:value-of select="xslNsODExt:getDictRes('NA')"/>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@worksheet='1'">
				<!-- For rating worksheets - value may be defined as int, but we're still rendering string -->
				<xsl:call-template name="replace-linebreaks">
					<xsl:with-param name="text" select="@value"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="blurName">
					<xsl:if test="(@onBlurID)">
						<!--<xsl:text>_</xsl:text>-->
						<!--<xsl:value-of select="@onBlurID"/> -->
						<xsl:value-of select="/page/content/getPage/actions[@type='page']/action[@id=current()/@onBlurID]/@uid"/>
					</xsl:if>
				</xsl:variable>
				<xsl:variable name="currentPageID">
					<xsl:value-of select="/page/content/getPage/@currentPageID"/>
				</xsl:variable>
				<xsl:call-template name="questionInput">
					<xsl:with-param name="name">
						<xsl:value-of select="@type"/>
						<xsl:text>_</xsl:text>
						<xsl:value-of select="@cacheID"/>
					</xsl:with-param>
					<!--Check if the fieldparameter 'removeZero' is set to 1, if so then the value of that field will be set to "" if the value is 0-->
					<xsl:with-param name="value">
						<xsl:choose>
							<xsl:when test="(@removeZero=1 or @removezero=1) and @value=0">
							</xsl:when>
							<xsl:when test="@cacheIndex and ($currentPageID and ($currentPageID != ''))">
								<xsl:text>__cacheINDEX:</xsl:text>
								<xsl:value-of select="@cacheIndex"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@value"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:with-param>
					<xsl:with-param name="actionID" select="$blurName"/>
					<xsl:with-param name="direction" select="$direction"/>
					<xsl:with-param name="captionPosition" select="$captionPosition"/>
					<xsl:with-param name="tableType" select="$tableType"/>
				</xsl:call-template>
				<xsl:if test="(@annotations = '1' or ../caption[@fieldRef=current()/@fieldRef and @index=current()/@index]/@annotations = '1') and $helpImage != ''">
					<xsl:element name="a">
						<xsl:if test="$DisableHelpTabbing='1'">
							<xsl:attribute name="tabIndex">-1</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="onclick">
							<xsl:text>DCT.Util.doAnnotationPopup('</xsl:text>
							<xsl:value-of select="@fieldRef"/>
							<xsl:text>','</xsl:text>
							<xsl:value-of select="@cacheID"/>
							<xsl:text>','</xsl:text>
							<xsl:value-of select="/page/content/getPage/actions[@type='page']/action[@command='getField']/@uid"/>
							<xsl:text>','</xsl:text>
							<xsl:value-of select="/page/content/getPage/@currentPageID"/>
							<xsl:text>');void(0);</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:value-of select="@index"/>
						</xsl:attribute>
						<xsl:if test="$DisableHelpTabbing!='1'">
							<xsl:attribute name="onfocus">
								<xsl:text>DCT.Util.setFocusField('</xsl:text>
								<xsl:value-of select="@index"/>
								<xsl:text>');</xsl:text>
							</xsl:attribute>
						</xsl:if>
						<xsl:attribute name="onmousemove">
							<xsl:text>window.status='';</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="onmouseout">
							<xsl:text>window.status='';</xsl:text>
						</xsl:attribute>
						<img src="{$helpImage}" border="0"/>
					</xsl:element>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is to keep messages from automatically processing
		************************************************************************************************************* -->
	<xsl:template match="messages">
	</xsl:template>
	<!--	*************************************************************************************************************
		helpPopup
		************************************************************************************************************* -->
	<xsl:template name="doPopup">
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
		<html>
			<head>
				<xsl:call-template name="buildMetaInformation"/>
				<title>
					<xsl:value-of select="/page/content/getField/properties/@caption"/>
				</title>
				<!-- load the ExtJS Core, ExtJs Overrides and base styles -->
				<xsl:choose>
					<xsl:when test="/page/debugMode!='0'">
						<link rel="stylesheet" type="text/css" href="{$cssDir}{/page/settings/Themes/ExtJsTheme/@cssFile}-debug.css" id="ExtJsCoreCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}extJS-Overrides-debug.css" id="ExtJsOverridesCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}dct-base-debug.css"/>
					</xsl:when>
					<xsl:otherwise>
						<link rel="stylesheet" type="text/css" href="{$cssDir}{/page/settings/Themes/ExtJsTheme/@cssFile}.css" id="ExtJsCoreCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}extJS-Overrides.css" id="ExtJsOverridesCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}dct-base.css"/>
					</xsl:otherwise>
				</xsl:choose>
				<!-- load the page styles -->
				<xsl:if test="normalize-space(/page/content/@page)!=''">
					<link rel="stylesheet" type="text/css" id="pageCSS" href="{$cssDir}x_{/page/content/@page}.css"/>
				</xsl:if>
				<!-- Themes always loaded last -->
				<link rel="stylesheet" type="text/css" href="{$SkinsDir}/themes/{$theme}/css/theme.css" id="SkinTheme"/>
				<xsl:choose>
					<xsl:when test="/page/debugMode!='0'">
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all-debug.js</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base-debug.js</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all.js</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base.js</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="buildJavaScriptInclude">
					<xsl:with-param name="dir" select="$customscriptDir"/>
					<xsl:with-param name="filename">custom.js</xsl:with-param>
				</xsl:call-template>
			</head>
			<body class="mainBody dct_{$currentPage} ruleset_{$ruleSet} dct_popup">
				<form method="POST" action="default.aspx" name="popupForm">
					<input type="hidden" name="_targetPage" id="_targetPage" value=""/>
					<input type="hidden" name="_submitAction" id="_submitAction" value=""/>
					<input type="hidden" name="_action" id="_action" value=""/>
					<input type="hidden" name="_fieldChanged" id="_fieldChanged" value=""/>
					<input type="hidden" name="_focusField" id="_focusField" value=""/>
					<div id="wrapper">
						<div id="formContent">
							<div id="main">
								<div id="fields">
									<xsl:choose>
										<xsl:when test="/page/content/getField[@mode='detail']">
											<xsl:for-each select="/page/content/getField/*[1]">
												<xsl:call-template name="fieldDetail"/>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:for-each select="/page/content/getField/*[1]">
												<xsl:call-template name="fieldDetail"/>
											</xsl:for-each>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</div>
					</div>
				</form>
			</body>
		</html>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for		
		************************************************************************************************************* -->
	<xsl:template match="annotations" mode="now">
		<xsl:for-each select="annotation">
			<xsl:value-of disable-output-escaping="yes" select="text()"/>
			<xsl:for-each select="comments/comment">
				<xsl:value-of disable-output-escaping="yes" select="text()"/>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for		
		************************************************************************************************************* -->
	<xsl:template match="annotations" mode="doc">

		<xsl:for-each select="annotation">
			<p>
				<xsl:value-of select="text()"/>
			</p>
			<xsl:for-each select="comments/comment">
				<p>
					<xsl:value-of select="text()"/>
				</p>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<!--	*************************************************************************************************************
	This template is for		
	************************************************************************************************************* -->
	<xsl:template match="annotations">
		<!-- Do nothing by default -->
	</xsl:template>
	<!--*************************************************************************************************************
	This template is for processing the page annotations
	************************************************************************************************************* -->
	<xsl:template match="annotations" mode="page">
		<xsl:for-each select="annotation">
			<xsl:value-of disable-output-escaping="yes" select="text()"/>
		</xsl:for-each>
	</xsl:template>
	<!--*************************************************************************************************************
	actions
	************************************************************************************************************* -->
	<xsl:template match="actions">
		<!-- block the generic handling of actions -->
	</xsl:template>
	<!--*************************************************************************************************************
	action
	************************************************************************************************************* -->
	<xsl:template match="action">
		<xsl:param name="table"/>
		<xsl:param name="tableheader"/>
		<xsl:param name="tabledata"/>
		<xsl:param name="tableType"/>
		<xsl:variable name="displayType">
			<xsl:choose>
				<xsl:when test="@displayType and @displayType!='action'">
					<xsl:value-of select="@displayType"/>
				</xsl:when>
			</xsl:choose>

			<!-- Default styles shared by actions of all types reside in "action" class -->
			<!-- "action" must go at the end because actionTable may append "Across" to the class name, and actionAcross is a valid class -->
			<xsl:text> action</xsl:text>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="ancestor::actionGroup">
				<xsl:if test="(@page != 'batchProcess') or ((@page = 'batchProcess') and (($securityLevel = '8') or ($securityLevel = '2')))">
					<xsl:variable name="command">
						<xsl:choose>
							<xsl:when test="@isJavaScriptCmd='1'">
								<xsl:value-of select="@command"/>
							</xsl:when>
							<xsl:when test="@checkSecurity='1'">
								<xsl:choose>
									<xsl:when test="./pageActionDctSecure and /page/state/@secure='1'">
										<xsl:text disable-output-escaping="yes">DCT.Submit.submitPageAction('</xsl:text>
										<xsl:value-of select="./pageActionDctSecure/targetPage"/>
										<xsl:text disable-output-escaping="yes">','</xsl:text>
										<xsl:value-of select="./pageActionDctSecure/submitAction"/>
										<xsl:text disable-output-escaping="yes">');</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text disable-output-escaping="yes">DCT.Submit.submitPageAction('</xsl:text>
										<xsl:value-of select="./pageActionDctUnsecure/targetPage"/>
										<xsl:text disable-output-escaping="yes">','</xsl:text>
										<xsl:value-of select="./pageActionDctUnsecure/submitAction"/>
										<xsl:text disable-output-escaping="yes">');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:when test="./actionDct">
								<xsl:text disable-output-escaping="yes">DCT.Submit.submitAction('</xsl:text>
								<xsl:value-of select="./actionDct/submitAction"/>
								<xsl:text disable-output-escaping="yes">');</xsl:text>
							</xsl:when>
							<xsl:when test="@command='submitPage'">
								<xsl:text>DCT.ajaxProcess.submitAjax('PageNavigation','</xsl:text>
								<xsl:value-of select="@page"/>
								<xsl:text>');</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@command"/>
								<xsl:text>('</xsl:text>
								<xsl:value-of select="@page"/>
								<xsl:text>'</xsl:text>
								<xsl:if test="@post">
									<xsl:text>,'</xsl:text>
									<xsl:value-of select="@post"/>
									<xsl:text>'</xsl:text>
								</xsl:if>
								<xsl:text>);</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:call-template name="actionAncGroup">
						<xsl:with-param name="command">
							<xsl:value-of select="$command"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="not(.//@referenceName)">
					<xsl:if test="@enabled='1'">
						<!-- Need to check on -->
						<xsl:choose>
							<xsl:when test="$table='1'">
								<xsl:choose>
									<xsl:when test="$tableheader">
										<xsl:call-template name="actionTableHeader">
											<xsl:with-param name="tableType">
												<xsl:value-of select="$tableType"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:when>
									<xsl:when test="$tabledata">
										<xsl:call-template name="actionTable">
											<xsl:with-param name="tableType">
												<xsl:value-of select="$tableType"/>
											</xsl:with-param>
											<xsl:with-param name="actionClass">
												<xsl:value-of select="$displayType"/>
											</xsl:with-param>
										</xsl:call-template>
									</xsl:when>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>

								<xsl:call-template name="actionSection">
									<xsl:with-param name="displayType">
										<xsl:value-of select="$displayType"/>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="actionAncGroup">
		<xsl:param name="command"/>
		<li>
			<xsl:call-template name="makeButton">
				<xsl:with-param name="onclick" select="$command"/>
				<xsl:with-param name="caption" select="@caption"/>
				<xsl:with-param name="id" select="@page"/>
				<xsl:with-param name="class">
					<xsl:if test="../@actClass">
						<xsl:value-of select="../@actClass"/>
					</xsl:if>
					<xsl:if test="@actClass">
						<xsl:text> </xsl:text>
						<xsl:value-of select="@actClass"/>
					</xsl:if>
				</xsl:with-param>
				<xsl:with-param name="type">hyperlink</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates select="actionGroup"/>
		</li>
	</xsl:template>
	<xsl:template name="actionTableHeader">
		<xsl:param name="tableType"/>
		<th>
			<xsl:call-template name="buildButton">
				<xsl:with-param name="tableType">
					<xsl:value-of select="$tableType"/>
				</xsl:with-param>
			</xsl:call-template>
		</th>
	</xsl:template>
	<xsl:template name="actionTable">
		<xsl:param name="tableType"/>
		<xsl:param name="actionClass"/>
		<xsl:variable name="displayType">
			<xsl:text>g-action</xsl:text>
			<xsl:if test="$tableType='acrossAboveOnce' or $tableType='acrossAboveRepeat' or $tableType='acrossBeforeRepeat'">
				<xsl:text>Across</xsl:text>
			</xsl:if>
		</xsl:variable>
		<td>
			<div>
				<xsl:attribute name="class">
					<xsl:value-of select="$displayType"/>
				</xsl:attribute>
				<xsl:call-template name="buildButton">
					<xsl:with-param name="tableType">
						<xsl:value-of select="$tableType"/>
					</xsl:with-param>
				</xsl:call-template>
			</div>
		</td>
	</xsl:template>
	<xsl:template name="actionSection">
		<xsl:param name="displayType"/>
		<div>
			<xsl:attribute name="class">
				<xsl:text>g-action</xsl:text>
				<xsl:if test="../@actClass">
					<xsl:text> </xsl:text>
					<xsl:value-of select="../@actClass"/>
				</xsl:if>
				<xsl:if test="@actClass">
					<xsl:text> </xsl:text>
					<xsl:value-of select="@actClass"/>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="./ancestor::section[1]/@captionPosition!=''">
						<xsl:text> g-action-caption-</xsl:text>
						<xsl:value-of select="./ancestor::section[1]/@captionPosition"/>
					</xsl:when>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="id">
				<xsl:value-of select="@uid"/>
				<xsl:text>_action</xsl:text>
			</xsl:attribute>

			<!-- [kellyke Nov 18, 10] send through an empty class so that we don't doulbe up the classes.  A 
			common problem scenario is when a CD adds x_LeftMargin25 to move the button right 25px, but then
			it moves right 50px since the class is applied both to the outer element and the one inside buildLink. 
			This also needs to be addressed in AG1.  -->
			<xsl:call-template name="buildLink">
				<xsl:with-param name="name" select="@uid"/>
				<xsl:with-param name="value" select="@caption"/>
				<xsl:with-param name="ignoreValidation" select=".//@ignoreValidation"/>
				<xsl:with-param name="cancelChanges" select=".//@cancelChanges"/>
				<xsl:with-param name="processingMsg" select="./server/requests/ManuScript.getPageRq/@processingMsg"/>
				<xsl:with-param name="class"/>
			</xsl:call-template>
		</div>
	</xsl:template>

	<!--************************************************************************************************************
	This template build buttons.
	************************************************************************************************************* -->
	<xsl:template name="buildButton">
		<xsl:param name="tableType"/>
		<xsl:call-template name="buildLink">
			<xsl:with-param name="name" select="@uid"/>
			<xsl:with-param name="value" select="@caption"/>
			<xsl:with-param name="ignoreValidation" select=".//@ignoreValidation"/>
			<xsl:with-param name="cancelChanges" select=".//@cancelChanges"/>
			<xsl:with-param name="processingMsg" select="./server/requests/ManuScript.getPageRq/@processingMsg"/>
			<xsl:with-param name="tableType" select="$tableType"/>
			<xsl:with-param name="class" select="@actClass"/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="grid" name="grid">
		<xsl:param name="tabledata">
			<xsl:text>0</xsl:text>
		</xsl:param>
		<xsl:param name="groupIndex"/>
		<xsl:variable name="gridID">
			<xsl:choose>
				<xsl:when test="@identifier">
					<xsl:value-of select="@identifier"/>
				</xsl:when>
				<xsl:when test="@id">
					<xsl:value-of select="@id"/>
				</xsl:when>
				<xsl:when test="@uid">
					<xsl:value-of select="@uid"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$tabledata='1'">
				<xsl:element name="td">
					<xsl:call-template name="buildGrid">
						<xsl:with-param name="gridID" select="$gridID"/>
						<xsl:with-param name="gridXml">
							<xsl:text>&lt;root&gt;</xsl:text>
							<xsl:value-of select="xslNsExt:outerXmlToString(..)"/>
							<xsl:text>&lt;/root&gt;</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="groupIndex" select="$groupIndex"/>
					</xsl:call-template>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildGrid">
					<xsl:with-param name="gridID" select="$gridID"/>
					<xsl:with-param name="gridXml">
						<xsl:text>&lt;root&gt;</xsl:text>
						<xsl:value-of select="xslNsExt:outerXmlToString(..)"/>
						<xsl:text>&lt;/root&gt;</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="groupIndex" select="$groupIndex"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="buildGrid">
		<xsl:param name="gridID"/>
		<xsl:param name="gridXml"/>
		<xsl:param name="groupIndex"/>

		<!-- build the hidden div used for inputs on an editable grid -->
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="$gridID"/>
				<xsl:text>_gridHiddenInputs</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="class">gridHiddenInputs</xsl:attribute>
			<xsl:attribute name="style">
				<xsl:text>display: none;</xsl:text>
			</xsl:attribute>
			<xsl:if test="normalize-space($gridXml) != ''">
				<xsl:attribute name="data-grid-xml">
					<xsl:value-of select="$gridXml"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:for-each select="row[position()&gt;1]/data[1]/child::*">
				<xsl:choose>
					<!-- <xsl:when test="name()='fieldInstance' and @readOnly='0'"> -->
					<xsl:when test="name()='fieldInstance'">
						<xsl:element name="div">
							<xsl:call-template name="fieldInstance">
								<xsl:with-param name="doSort">0</xsl:with-param>
								<xsl:with-param name="direction">down</xsl:with-param>
								<xsl:with-param name="captionPosition">above</xsl:with-param>
							</xsl:call-template>
						</xsl:element>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</xsl:element>
		<xsl:if test="../@startIndex!=''">
			<input type="hidden" name="_iterationStartIndex" id="_iterationStartIndex">
				<xsl:attribute name="value">
					<xsl:value-of select="../@startIndex"/>
				</xsl:attribute>
			</input>
			<input type="hidden" name="_iterationMaxItems" id="_iterationMaxItems">
				<xsl:attribute name="value">
					<xsl:value-of select="../@maxItems"/>
				</xsl:attribute>
			</input>
			<input type="hidden" name="_iterationListCount" id="_iterationListCount">
				<xsl:attribute name="value">
					<xsl:value-of select="../@listCount"/>
				</xsl:attribute>
			</input>
			<input type="hidden" name="_pageChange" id="_pageChange"/>
		</xsl:if>
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="$gridID"/>
				<xsl:text>_gridDiv</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="@direction='across'">
						<xsl:text>acrossLayout</xsl:text>
					</xsl:when>
					<xsl:when test="@direction='down'">
						<xsl:text>downLayout</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>defaultLayout</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text> </xsl:text>
				<xsl:choose>
					<xsl:when test="@class">
						<xsl:value-of select="@class"/>
					</xsl:when>
					<xsl:when test="@classname">
						<xsl:value-of select="@classname"/>
					</xsl:when>
				</xsl:choose>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="normalize-space(@caption)!='' and @captionPosition='before' and @style='across' and @showCaption='none'">
					<h2>
						<xsl:if test="@captionClass">
							<xsl:attribute name="class">
								<xsl:value-of select="@captionClass"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@caption"/>
					</h2>
				</xsl:when>
				<xsl:when test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down'"/>
				<!-- Do Nothing Now -->
				<xsl:when test="normalize-space(@caption)!=''">
					<h2>
						<xsl:if test="@captionClass">
							<xsl:attribute name="class">
								<xsl:value-of select="@captionClass"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@caption"/>
					</h2>
				</xsl:when>
			</xsl:choose>
			<xsl:if test="ancestor::section[1]/@style='across' and normalize-space(@caption)!='' and @style='down'">
				<!-- Processing was skipped above, process now -->
				<h2>
					<xsl:if test="@captionClass">
						<xsl:attribute name="class">
							<xsl:value-of select="@captionClass"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="@caption"/>
				</h2>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="../@startIndex">
					<xsl:if test="row[1]/header/child::*">
						<xsl:call-template name="buildEditGridPanel">
							<xsl:with-param name="targetPage">
								<xsl:text>interviewList</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="pageSize">
								<xsl:value-of select="../@maxItems"/>
							</xsl:with-param>
							<xsl:with-param name="startIndex">
								<xsl:value-of select="number(../@startIndex)-1"/>
							</xsl:with-param>
							<xsl:with-param name="totalProperty">
								<xsl:text>group[@startIndex]/@listCount</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="record">
								<xsl:text>grid[@uid=\'</xsl:text>
								<xsl:value-of select="@uid"/>
								<xsl:text>\']/row/data</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="idPath">
								<xsl:text>*/@uid</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="fields">
								<xsl:call-template name="buildInterviewGridColumnNodes"/>
							</xsl:with-param>
							<xsl:with-param name="colModel">
								<xsl:call-template name="buildInterviewGridColumnDef"/>
							</xsl:with-param>
							<xsl:with-param name="gridID">
								<xsl:value-of select="$gridID"/>
							</xsl:with-param>
							<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
							<xsl:with-param name="groupIndex" select="$groupIndex"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="row[2]/data[1]/child::*">
						<xsl:call-template name="buildEditGridPanel">
							<xsl:with-param name="targetPage">
								<xsl:text>interviewList</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="record">
								<xsl:text>grid[@uid=\'</xsl:text>
								<xsl:value-of select="@uid"/>
								<xsl:text>\']/row/data</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="idPath">
								<xsl:text>*/@uid</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="fields">
								<xsl:call-template name="buildInterviewGridColumnNodes"/>
							</xsl:with-param>
							<xsl:with-param name="colModel">
								<xsl:call-template name="buildInterviewGridColumnDef"/>
							</xsl:with-param>
							<xsl:with-param name="gridID">
								<xsl:value-of select="$gridID"/>
							</xsl:with-param>
							<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for building an icon to allow a user to view/accept data that is different in the shared data.
		************************************************************************************************************* -->
	<xsl:template name="buildSharedDataChange">
		<xsl:param name="fieldID"/>
		<img src="{$imageDir}warning.gif" class="imageButton" align="absmiddle">
			<xsl:attribute name="onclick">
				<xsl:text>DCT.Util.showShareDataChanges(this, '</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>', 'divSharedField</xsl:text>
				<xsl:value-of select="@sharedID"/>
				<xsl:text>');</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="onmouseover">
				<xsl:text>DCT.Util.previewSharedDataChange(this, 'divSharedField</xsl:text>
				<xsl:value-of select="@sharedID"/>
				<xsl:text>');</xsl:text>
			</xsl:attribute>
		</img>
	</xsl:template>
	<!--*************************************************************************************************************
	Common templates to Control Grid processing for Interview screens
	************************************************************************************************************* -->
	<xsl:template name="buildInterviewGridColumnNodes">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="row[1]/header/child::*">
			<xsl:variable name="pos" select="position()"/>
			<xsl:if test="not(@hideColumn) or @hideColumn='0'">
				<xsl:variable name="followingSibling" select="following-sibling::*[not(@hideColumn) or @hideColumn='0']"/>
				<xsl:variable name="columnName" select="name((../../../row/data/child::*[@id and position()=$pos])[1])"/>
				<xsl:variable name="columnType" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@type"/>
				<xsl:variable name="firstValidCaption" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@caption"/>
				<xsl:variable name="caption">
					<xsl:choose>
						<xsl:when test="@value and @value!=''">
							<xsl:value-of select="@value"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$firstValidCaption"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$columnName='fieldInstance'">
						<!-- start config section data -->
						<xsl:text>{ </xsl:text>
						<!-- set name data -->
						<xsl:text>name: '</xsl:text>
						<xsl:value-of select="@fieldRef"/>
						<xsl:text>', </xsl:text>
						<!-- set mapping data -->
						<xsl:text>mapping: 'fieldInstance[@fieldRef="</xsl:text>
						<xsl:value-of select="@fieldRef"/>
						<xsl:text>"]/@value', </xsl:text>
						<!-- set type data -->
						<!-- * if applicable: set dateFormat data -->
						<xsl:choose>
							<xsl:when test="$columnType='date'">
								<xsl:text>type: 'date', </xsl:text>
								<xsl:text>dateFormat: 'Y-m-d' </xsl:text>
							</xsl:when>
							<xsl:when test="$columnType='boolean'">
								<xsl:text>type: 'boolean' </xsl:text>
							</xsl:when>
							<xsl:when test="$columnType='int'">
								<xsl:text>type: 'number' </xsl:text>
							</xsl:when>
							<xsl:when test="$columnType='float'">
								<xsl:text>type: 'number' </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>type: 'string' </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<!-- end config section data -->
						<xsl:text>},</xsl:text>
						<!-- create reference to fieldintance -->
						<xsl:text>{ </xsl:text>
						<!-- set name data -->
						<xsl:text>name: '</xsl:text>
						<xsl:text>uid_</xsl:text>
						<xsl:value-of select="@fieldRef"/>
						<xsl:text>', </xsl:text>
						<!-- set mapping data -->
						<xsl:text>mapping: 'fieldInstance[@fieldRef="</xsl:text>
						<xsl:value-of select="@fieldRef"/>
						<xsl:text>"]/@uid'</xsl:text>
						<!-- end config section data -->
						<xsl:text>}</xsl:text>
						<!-- pre for next config section -->
						<xsl:if test="$followingSibling">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- start config section data -->
						<xsl:text>{ </xsl:text>
						<!-- set name data -->
						<xsl:text>name: '</xsl:text>
						<xsl:value-of select="@index"/>
						<xsl:text>', </xsl:text>
						<!-- set mapping data -->
						<xsl:text>mapping: 'action[@index="</xsl:text>
						<xsl:value-of select="@index"/>
						<xsl:text>"]/@caption'</xsl:text>
						<!-- end config section data -->
						<xsl:text>},</xsl:text>
						<!-- create field for actionId -->
						<xsl:text>{ </xsl:text>
						<!-- set name data -->
						<xsl:text>name: '</xsl:text>
						<xsl:text>actionId_</xsl:text>
						<xsl:value-of select="@index"/>
						<xsl:text>', </xsl:text>
						<!-- set mapping data -->
						<xsl:text>mapping: 'action[@index="</xsl:text>
						<xsl:value-of select="@index"/>
						<xsl:text>"]/@id'</xsl:text>
						<!-- end config section data -->
						<xsl:text>}</xsl:text>
						<!-- pre for next config section -->
						<xsl:if test="$followingSibling">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>
	<xsl:template name="buildInterviewGridColumnDef">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="row[1]/header/child::*">
			<xsl:variable name="pos" select="position()"/>
			<xsl:if test="not(@hideColumn) or @hideColumn='0'">
				<xsl:variable name="followingSibling" select="following-sibling::*[not(@hideColumn) or @hideColumn='0']"/>
				<xsl:variable name="columnName" select="name((../../../row/data/child::*[@id and position()=$pos])[1])"/>
				<xsl:variable name="columnType" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@type"/>
				<xsl:variable name="required" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@required"/>
				<xsl:variable name="dataEliminate" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@eliminate"/>
				<xsl:variable name="dataReadOnly" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@readOnly"/>
				<xsl:variable name="dataFieldCls" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@fldClass"/>
				<xsl:variable name="readOnly" select="(../../../@readOnly)"/>
				<xsl:variable name="firstValidCaption" select="(../../../row/data/child::*[@id and position()=$pos])[1]/@caption"/>
				<xsl:variable name="caption">
					<xsl:choose>
						<xsl:when test="@value and @value!=''">
							<xsl:value-of select="@value"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$firstValidCaption"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$columnName = 'fieldInstance'">
						<!-- start config data -->
						<xsl:text>{ </xsl:text>
						<!-- set header data -->
						<xsl:text>text: '</xsl:text>
						<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode($caption, '0')"/>
						<xsl:if test="$required='1' and $readOnly='0'">
							<xsl:text>*</xsl:text>
						</xsl:if>
						<xsl:text>', </xsl:text>
						<!-- set xtype data -->
						<xsl:choose>
							<xsl:when test="$columnType='boolean'">
								<xsl:text>xtype: '</xsl:text>
								<xsl:text>checkcolumn</xsl:text>
								<xsl:text>', </xsl:text>
							</xsl:when>
						</xsl:choose>
						<!-- set dataIndex data -->
						<xsl:text>dataIndex: '</xsl:text>
						<xsl:value-of select="@fieldRef"/>
						<xsl:text>', </xsl:text>
						<xsl:text>dataReadOnly: </xsl:text>
						<xsl:choose>
							<xsl:when test="$dataReadOnly = '1'">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>, </xsl:text>
						<xsl:if test="$dataFieldCls">
							<xsl:text>dataFieldCls: '</xsl:text>
							<xsl:value-of select="$dataFieldCls"/>
							<xsl:text>', </xsl:text>
						</xsl:if>
						<xsl:if test="$columnType='float'">
							<xsl:text>align: 'right',</xsl:text>
						</xsl:if>
						<!-- set sortable data -->
						<xsl:text>sortable: </xsl:text>
						<xsl:choose>
							<xsl:when test="../../../@sort">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<!-- set width data -->
						<xsl:if test="@columnWidth and @columnWidth!=''">
							<xsl:text>, width: </xsl:text>
							<xsl:value-of select="@columnWidth"/>
						</xsl:if>
						<!-- set renderer data -->
						<xsl:choose>
							<xsl:when test="$columnType='boolean'">
								<xsl:text>, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view){ return </xsl:text>
								<xsl:text>DCT.Util.booleanInterviewColumnRender(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, </xsl:text>
								<xsl:value-of select="../../../@readOnly='0'"/>
								<xsl:text>);}</xsl:text>
								<xsl:text>, listeners: { </xsl:text>
								<xsl:text>beforecheckchange:  DCT.Util.booleanInterviewBeforeChange,</xsl:text>
								<xsl:text>checkchange:  DCT.Util.booleanInterviewAfterChange</xsl:text>
								<xsl:text>}</xsl:text>
							</xsl:when>
							<xsl:when test="$columnType='date' or $columnType='datetime'">
								<xsl:text>, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view){ return </xsl:text>
								<xsl:text>DCT.Util.dateInterviewColumnRender(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, </xsl:text>
								<xsl:value-of select="../../../@readOnly='0'"/>
								<xsl:text>);}</xsl:text>
							</xsl:when>
							<xsl:when test="(../../../row/data/child::*[@id and position()=$pos])[1]/list">
								<xsl:text>, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view){ return </xsl:text>
								<xsl:text>DCT.Util.listInterviewColumnRender(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, </xsl:text>
								<xsl:value-of select="../../../@readOnly='0'"/>
								<xsl:text>);}</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>, renderer: function(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view){ return </xsl:text>
								<xsl:text>DCT.Util.textInterviewColumnRender(dataValue, cellMetaData, extRecord, rowIndex, colIndex, extDataStore, view, </xsl:text>
								<xsl:value-of select="../../../@readOnly='0'"/>
								<xsl:text>);}</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<!-- set editor data -->
						<xsl:if test="../../../@readOnly='0' and $columnType!='boolean'">
							<xsl:text>, editor: new Ext.form.TextField({ })</xsl:text>
						</xsl:if>
						<!-- end config data -->
						<xsl:text>}</xsl:text>
						<!-- pre for next config section -->
						<xsl:if test="$followingSibling">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<!-- start config data -->
						<xsl:text>{ </xsl:text>
						<!-- set header data -->
						<xsl:text>text: '</xsl:text>
						<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(@value, '0')"/>
						<xsl:text>', </xsl:text>
						<!-- set dataIndex data -->
						<xsl:text>dataIndex: '</xsl:text>
						<xsl:value-of select="@index"/>
						<!-- set default sortable and renderer data -->
						<xsl:text>', sortable: false, renderer: DCT.Util.anchorInterviewColumnRenderer</xsl:text>
						<!-- set width data -->
						<xsl:if test="@columnWidth and @columnWidth!=''">
							<xsl:text>, width: </xsl:text>
							<xsl:value-of select="@columnWidth"/>
						</xsl:if>
						<!-- end config data -->
						<xsl:text>}</xsl:text>
						<!-- pre for next config section -->
						<xsl:if test="$followingSibling">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>
	<xsl:template name="getReadOnly">
		<xsl:choose>
			<xsl:when test="@sharedID">0</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="/page/content/getPage/body/@readOnly='1'">
						<xsl:value-of select="/page/content/getPage/body/@readOnly"/>
					</xsl:when>
					<xsl:when test="@readOnly">
						<xsl:value-of select="@readOnly"/>
					</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getHasList">
		<xsl:choose>
			<xsl:when test="list">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getHideOn">
		<xsl:choose>
			<xsl:when test="substring(@formatMask,1,13)='hideOnDisplay'">
				<xsl:text>display</xsl:text>
			</xsl:when>
			<xsl:when test="substring(@formatMask,1,11)='hideOnEntry'">
				<xsl:text>entry</xsl:text>
			</xsl:when>
			<xsl:when test="substring(@formatMask,1,4)='hide'">
				<xsl:text>always</xsl:text>
			</xsl:when>
			<xsl:otherwise>never</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getFormatMask">
		<xsl:choose>
			<xsl:when test="@formatMask">
				<xsl:call-template name="getFormatMaskHide"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getFieldMask">
		<xsl:choose>
			<xsl:when test="@formatMask">
				<xsl:choose>
					<xsl:when test="(substring(@formatMask,1,4)='hide') and (@type='int')">-?,???,???,??#</xsl:when>
					<xsl:when test="(substring(@formatMask,1,4)='hide') and (@type='float')">-??,???,???,??#.????</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="getFormatMaskHide"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="@type='int'">-?,???,???,??#</xsl:when>
					<xsl:when test="@type='float'">-??,???,???,??#.????</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getId">
		<xsl:value-of select="@uid"/>
	</xsl:template>
	<xsl:template name="getFieldRef">
		<xsl:value-of select="@fieldRef"/>
	</xsl:template>
	<xsl:template name="getObjectRef">
		<xsl:value-of select="@objectRef"/>
	</xsl:template>
	<xsl:template name="buildReferenceActions">
		<xsl:param name="tableType"/>
		<xsl:variable name="referenceAction">
			<xsl:value-of select="@referenceAction"/>
		</xsl:variable>
		<xsl:if test="../..//action[server/requests/ManuScript.getPageRq/@referenceName]">
			<xsl:for-each select="../..//action[server/requests/ManuScript.getPageRq/@referenceName]">
				<xsl:if test="server/requests/ManuScript.getPageRq/@referenceName = $referenceAction">
					<li>
						<xsl:call-template name="buildButton">
							<xsl:with-param name="tableType">
								<xsl:value-of select="$tableType"/>
							</xsl:with-param>
						</xsl:call-template>
					</li>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildReadOnlyField">
		<xsl:param name="fieldID"/>
		<xsl:param name="fieldRef"/>
		<xsl:param name="objectRef"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="value"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:param name="hideOn">
			<xsl:text>never</xsl:text>
		</xsl:param>
		<div>
			<xsl:attribute name="class">
				<xsl:text>controlContainer</xsl:text>
				<xsl:if test="@truncateField='1'">
					<xsl:text> truncateWithDots</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<xsl:call-template name="buildReadOnlyDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="class">interviewDisplayField</xsl:with-param>
				<xsl:with-param name="hideOn" select="$hideOn"/>
				<xsl:with-param name="truncateField" select="@truncateField"/>
				<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildExtendedList">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<div class="controlContainer">
			<xsl:attribute name="id">
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$fieldID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewExtendedComboField</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>"</xsl:text>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctCaption</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$caption"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:text>,</xsl:text>
					<xsl:text>dctType: "</xsl:text>
					<xsl:value-of select="$type"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctRequired: "</xsl:text>
					<xsl:choose>
						<xsl:when test="$required='1'">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
					<xsl:text>",</xsl:text>
					<xsl:text>type: "text",</xsl:text>
					<xsl:text>hiddenName: "</xsl:text>
					<xsl:value-of select="$name"/>
					<xsl:text>",</xsl:text>
					<xsl:text>readOnly: </xsl:text>
					<xsl:value-of select="@readOnly"/>
					<xsl:text>,</xsl:text>
					<xsl:text>dctComparisonStyle: "</xsl:text>
					<xsl:value-of select="@class"/>
					<xsl:text>",</xsl:text>
					<xsl:text>emptyText: </xsl:text>
					<xsl:choose>
						<xsl:when test="@watermark!=''">
							<xsl:text>"</xsl:text>
							<xsl:value-of select="xslNsExt:replaceText(@watermark,$doubleQuote,$escapedDoubleQuote)"/>
							<xsl:text>"</xsl:text>
						</xsl:when>
						<xsl:when test="substring(@formatMask,1,11)='blankOption'">
							<xsl:text>"</xsl:text>
							<xsl:choose>
								<xsl:when test="substring(@formatMask,1,12)='blankOption:'">
									<xsl:value-of select="xslNsExt:replaceText(substring-after(@formatMask,':'),$doubleQuote,$escapedDoubleQuote)"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="xslNsExt:replaceText(xslNsExt:FormatString1(xslNsODExt:getDictRes('Select_FormatMask'), xslNsODExt:getDictRes('Select_lower')),$doubleQuote,$escapedDoubleQuote)"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>"</xsl:text>
						</xsl:when>
						<xsl:otherwise>null</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>dctStore: [</xsl:text>
					<xsl:choose>
						<xsl:when test="@watermark!=''">
							<xsl:text>[ "", "</xsl:text>
							<xsl:value-of select="xslNsExt:replaceText(@watermark,$doubleQuote,$escapedDoubleQuote)"/>
							<xsl:text>"]</xsl:text>
							<xsl:if test="list[1]/option[1]">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:when test="substring(@formatMask,1,11)='blankOption'">
							<xsl:choose>
								<xsl:when test="substring(@formatMask,1,12)='blankOption:'">
									<xsl:text>[ "", "</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(substring-after(@formatMask,':'),$doubleQuote,$escapedDoubleQuote)"/>
									<xsl:text>"]</xsl:text>
									<xsl:if test="list[1]/option[1]">
										<xsl:text>,</xsl:text>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>[ "", "</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(xslNsExt:FormatString1(xslNsODExt:getDictRes('Select_FormatMask'), xslNsODExt:getDictRes('Select_lower')),$doubleQuote,$escapedDoubleQuote)"/>
									<xsl:text>"]</xsl:text>
									<xsl:if test="list[1]/option[1]">
										<xsl:text>,</xsl:text>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
					<xsl:for-each select="list/option">
						<xsl:text>[</xsl:text>
						<xsl:text>"</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText(@value,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:variable name="optionCaption">
							<xsl:apply-templates select="@caption" mode="buildOptionCaption"/>
						</xsl:variable>
						<xsl:value-of select="xslNsExt:replaceText($optionCaption,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText(@caption,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:variable name="comparisonCaption">
							<xsl:apply-templates select="@caption" mode="buildComparisonCaption"/>
						</xsl:variable>
						<xsl:value-of select="xslNsExt:replaceText($comparisonCaption,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>",</xsl:text>
						<xsl:value-of select="@special"/>
						<xsl:text>]</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]</xsl:text>
					<xsl:if test="$includeDebugInfo = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>fieldRef: "</xsl:text>
						<xsl:value-of select="$fieldRef_Val"/>
						<xsl:text>",</xsl:text>
						<xsl:text>objectRef: "</xsl:text>
						<xsl:value-of select="$objectRef_Val"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$actionID!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>dctActionId: "</xsl:text>
						<xsl:value-of select="$actionID"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="/page/settings/Controls/ComboBoxes/@maxSize">
						<xsl:text>,</xsl:text>
						<xsl:text>dctMaxSize: "</xsl:text>
						<xsl:value-of select="/page/settings/Controls/ComboBoxes/@maxSize"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:call-template name="addCustomExtendedListProperties"/>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
				<xsl:call-template name="addDataValue">
					<xsl:with-param name="type">string</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$value"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template name="buildSliderList">
		<xsl:param name="fieldID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="fieldRef"/>
		<xsl:param name="objectRef"/>
		<xsl:param name="actionID"/>
		<xsl:param name="extraConfigItems"/>
		<div class="controlContainer">
			<xsl:attribute name="id">
				<xsl:value-of select="../@uid"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="../@uid"/>
			</xsl:attribute>
			<xsl:element name="input">
				<xsl:attribute name="type">hidden</xsl:attribute>
				<xsl:attribute name="id">
					<xsl:value-of select="$fieldID"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="$name"/>
				</xsl:attribute>
				<xsl:attribute name="value">
					<xsl:value-of select="$value"/>
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewSlider</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="../@uid"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="../@uid"/>
					<xsl:text>",</xsl:text>
					<xsl:text>hiddenId: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>"</xsl:text>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctInitialValue</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$value"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:text>,</xsl:text>
					<xsl:text>disabled: </xsl:text>
					<xsl:choose>
						<xsl:when test="$readOnly='1'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>dctOptions: [</xsl:text>
					<xsl:for-each select="list/option">
						<xsl:text>[</xsl:text>
						<xsl:text>"</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText(@value,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText(@caption,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText(@comparisonValue,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>","</xsl:text>
						<xsl:variable name="optionCaption">
							<xsl:apply-templates select="@caption" mode="buildOptionCaption"/>
						</xsl:variable>
						<xsl:value-of select="xslNsExt:replaceText($optionCaption,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>"</xsl:text>
						<xsl:text>]</xsl:text>
						<xsl:if test="position() != last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>]</xsl:text>
					<xsl:if test="$includeDebugInfo = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>fieldRef: "</xsl:text>
						<xsl:value-of select="$fieldRef"/>
						<xsl:text>",</xsl:text>
						<xsl:text>objectRef: "</xsl:text>
						<xsl:value-of select="$objectRef"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$actionID!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>dctActionId: "</xsl:text>
						<xsl:value-of select="$actionID"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="../@class">
						<xsl:text>,</xsl:text>
						<xsl:text>ctCls: "</xsl:text>
						<xsl:value-of select="../@class"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$extraConfigItems">
						<xsl:value-of select="$extraConfigItems"/>
					</xsl:if>
					<xsl:call-template name="addCustomSliderProperties"/>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template name="buildSelect">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="extraConfigItems"/>
		
		<div class="controlContainer">
			<xsl:call-template name="buildSelectDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="caption" select="$caption"/>
				<xsl:with-param name="type" select="$type"/>
				<xsl:with-param name="actionID" select="$actionID"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="watermark" select="@watermark"/>
				<xsl:with-param name="formatMask" select="@formatMask"/>
				<xsl:with-param name="maxLength" select="@maxLength"/>
				<xsl:with-param name="fldClass" select="@fldClass"/>
				<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
				<xsl:with-param name="class">interviewComboField</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildBooleanRadioButtons">
		<xsl:param name="fieldID"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="name"/>
		<xsl:param name="actionID"/>
		<xsl:param name="value"/>
		<xsl:param name="required"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="formatMask"/>
		<div class="controlContainer">
			<xsl:attribute name="id">
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$fieldID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewRadioGroup</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>",</xsl:text>
					<xsl:text>dctRequired: "</xsl:text>
					<xsl:choose>
						<xsl:when test="$required='1'">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
					<xsl:text>"</xsl:text>
					<xsl:if test="contains($formatMask,'radio:1')">
						<xsl:text>,</xsl:text>
						<xsl:text>vertical: true</xsl:text>
						<xsl:text>,</xsl:text>
						<xsl:text>columns: 1</xsl:text>
					</xsl:if>
					<xsl:text>,</xsl:text>
					<xsl:text>dctRadioButtons: [</xsl:text>
					<xsl:text>{</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>_0",</xsl:text>
					<xsl:text>boxLabel: "</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('No')"/>
					<xsl:text>",</xsl:text>
					<xsl:text>name: "</xsl:text>
					<xsl:value-of select="$name"/>
					<xsl:text>",</xsl:text>
					<xsl:text>inputValue: "0",</xsl:text>
					<xsl:text>checked: </xsl:text>
					<xsl:choose>
						<xsl:when test="$value='0'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>disabled: </xsl:text>
					<xsl:choose>
						<xsl:when test="$readOnly='1'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$includeDebugInfo = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>fieldRef: "</xsl:text>
						<xsl:value-of select="$fieldRef_Val"/>
						<xsl:text>",</xsl:text>
						<xsl:text>objectRef: "</xsl:text>
						<xsl:value-of select="$objectRef_Val"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$actionID!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>dctActionId: "</xsl:text>
						<xsl:value-of select="$actionID"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:call-template name="addCustomBooleanRadioProperties"/>
					<xsl:text>}</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>{</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>_1",</xsl:text>
					<xsl:text>boxLabel: "</xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('Yes')"/>
					<xsl:text>",</xsl:text>
					<xsl:text>name: "</xsl:text>
					<xsl:value-of select="$name"/>
					<xsl:text>",</xsl:text>
					<xsl:text>inputValue: "1",</xsl:text>
					<xsl:text>checked: </xsl:text>
					<xsl:choose>
						<xsl:when test="$value='1'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:text>,</xsl:text>
					<xsl:text>disabled: </xsl:text>
					<xsl:choose>
						<xsl:when test="$readOnly='1'">
							<xsl:text>true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="$includeDebugInfo = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>fieldRef: "</xsl:text>
						<xsl:value-of select="$fieldRef_Val"/>
						<xsl:text>",</xsl:text>
						<xsl:text>objectRef: "</xsl:text>
						<xsl:value-of select="$objectRef_Val"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="$actionID!=''">
						<xsl:text>,</xsl:text>
						<xsl:text>dctActionId: "</xsl:text>
						<xsl:value-of select="$actionID"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:call-template name="addCustomBooleanRadioProperties"/>
					<xsl:text>}</xsl:text>
					<xsl:text>]</xsl:text>
					<xsl:call-template name="addCustomBooleanRadioGroupProperties"/>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template name="buildCheckBox">
		<xsl:param name="fieldID"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="name"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="fieldOtherParms"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="actionID"/>
		<xsl:param name="value"/>
		<xsl:param name="extraConfigItems"/>
		<div class="controlContainer">
			<xsl:call-template name="buildCheckBoxDiv">
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="readOnly" select="$readOnly"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
				<xsl:with-param name="checkbox">
					<xsl:call-template name="buildCheckBoxObject">
						<xsl:with-param name="actionID" select="$actionID"/>
						<xsl:with-param name="fieldID" select="$fieldID"/>
						<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
						<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
						<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="dctClassName">dctcheckboxfield</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readOnly"/>
						<xsl:with-param name="boxLabel">false</xsl:with-param>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
						<xsl:with-param name="checked">
							<xsl:choose>
								<xsl:when test="$value!='0' and $value!=''">
									<xsl:text>true</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>false</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildInputFile">
		<xsl:param name="fieldMask"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="name"/>
		<div class="controlContainer">
			<xsl:attribute name="id">
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-cmpid">
				<xsl:value-of select="$fieldID"/>
			</xsl:attribute>
			<xsl:element name="div">
				<xsl:attribute name="class">interviewFileField</xsl:attribute>
				<xsl:attribute name="data-config">
					<xsl:text>{</xsl:text>
					<xsl:text>renderTo: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>Cmp</xsl:text>
					<xsl:text>",</xsl:text>
					<xsl:text>id: "</xsl:text>
					<xsl:value-of select="$fieldID"/>
					<xsl:text>"</xsl:text>
					<xsl:call-template name="addConfigProperty">
						<xsl:with-param name="name">dctCaption</xsl:with-param>
						<xsl:with-param name="type">string</xsl:with-param>
						<xsl:with-param name="value">
							<xsl:value-of select="$caption"/>
						</xsl:with-param>
					</xsl:call-template>
					<xsl:text>,</xsl:text>
					<xsl:text>dctRequired: "</xsl:text>
					<xsl:choose>
						<xsl:when test="$required='1'">1</xsl:when>
						<xsl:otherwise>0</xsl:otherwise>
					</xsl:choose>
					<xsl:text>",</xsl:text>
					<xsl:text>name: "</xsl:text>
					<xsl:value-of select="$name"/>
					<xsl:text>"</xsl:text>
					<xsl:if test="$includeDebugInfo = 'true'">
						<xsl:text>,</xsl:text>
						<xsl:text>fieldRef: "</xsl:text>
						<xsl:value-of select="$fieldRef_Val"/>
						<xsl:text>",</xsl:text>
						<xsl:text>objectRef: "</xsl:text>
						<xsl:value-of select="$objectRef_Val"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:if test="contains($fieldMask,'width:')">
						<xsl:text>,</xsl:text>
						<xsl:text>dctWidth: "</xsl:text>
						<xsl:value-of select="substring-before(substring-after($fieldMask,'width:'),':')"/>
						<xsl:text>"</xsl:text>
					</xsl:if>
					<xsl:call-template name="addCustomInputFileProperties"/>
					<xsl:text>}</xsl:text>
				</xsl:attribute>
			</xsl:element>
		</div>
	</xsl:template>
	<xsl:template name="buildInputTextArea">
		<xsl:param name="fieldMask"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="actionID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<div class="controlContainer">
			<xsl:call-template name="buildTextAreaDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="caption" select="$caption"/>
				<xsl:with-param name="actionID" select="$actionID"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="fieldMask" select="$fieldMask"/>
				<xsl:with-param name="fldClass" select="@fldClass"/>
				<xsl:with-param name="maxLength" select="@maxLength"/>
				<xsl:with-param name="readOnly" select="@readOnly"/>
				<xsl:with-param name="class">interviewTextAreaField</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildInputRichTextArea">
		<xsl:param name="fieldMask"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="actionID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<div class="controlContainer">
			<xsl:call-template name="buildRichTextAreaDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="caption" select="$caption"/>
				<xsl:with-param name="actionID" select="$actionID"/>
				<xsl:with-param name="name" select="$name"/>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="fieldMask" select="$fieldMask"/>
				<xsl:with-param name="fldClass" select="@fldClass"/>
				<xsl:with-param name="maxLength" select="@maxLength"/>
				<xsl:with-param name="readOnly" select="@readOnly"/>
				<xsl:with-param name="class">interviewRichTextAreaField</xsl:with-param>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="buildInput">
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="readOnly"/>
		<xsl:param name="actionID"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="fieldOtherParms"/>
		<xsl:param name="hideOn"/>
		<xsl:param name="name"/>
		<xsl:param name="fieldMask"/>
		<xsl:param name="value"/>
		<xsl:param name="extraConfigItems"/>
		<div>
			<xsl:attribute name="class">
				<xsl:text>controlContainer</xsl:text>
			</xsl:attribute>
			<xsl:variable name="defaultMaxLength">
				<xsl:call-template name="GetDefaultMaxLength">
					<xsl:with-param name="fieldMask" select="$fieldMask"/>
					<xsl:with-param name="maximum" select="@maximum"/>
					<xsl:with-param name="type" select="$type"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="maxLength2">
				<xsl:choose>
					<xsl:when test="@maxLength=0">
						<xsl:value-of select="$defaultMaxLength"/>
					</xsl:when>
					<xsl:when test="@maxLength">
						<xsl:value-of select="@maxLength"/>
					</xsl:when>
					<xsl:otherwise></xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:call-template name="buildInputDiv">
				<xsl:with-param name="fieldRef_Val" select="$fieldRef_Val"/>
				<xsl:with-param name="objectRef_Val" select="$objectRef_Val"/>
				<xsl:with-param name="fieldID" select="$fieldID"/>
				<xsl:with-param name="required" select="$required"/>
				<xsl:with-param name="caption" select="$caption"/>
				<xsl:with-param name="actionID" select="$actionID"/>
				<xsl:with-param name="name">
					<xsl:value-of select="$name"/>
					<xsl:if test="$type='date'">
						<xsl:text>|</xsl:text>
						<xsl:value-of select="xslNsExt:formatMaskToUse($formatMask, '0', '')"/>
					</xsl:if>
          <xsl:if test="$type='string' or $type='float'">
						<xsl:text>|</xsl:text>
						<xsl:value-of select="$formatMask"/>
					</xsl:if>
				</xsl:with-param>
				<xsl:with-param name="value" select="$value"/>
				<xsl:with-param name="includeDebugInfo" select="$includeDebugInfo"/>
				<xsl:with-param name="fieldMask" select="$fieldMask"/>
				<xsl:with-param name="formatMask" select="$formatMask"/>
				<xsl:with-param name="class">
					<xsl:choose>
						<xsl:when test="$type='date'">
							<xsl:text>inputDateField</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="getJSInputClass">
								<xsl:with-param name="type">
									<xsl:value-of select="$type"/>
								</xsl:with-param>
								<xsl:with-param name="fieldMask">
									<xsl:value-of select="$fieldMask"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="fieldOtherParms" select="$fieldOtherParms"/>
				<xsl:with-param name="dctType" select="$type"/>
				<xsl:with-param name="fldClass" select="@fldClass"/>
				<xsl:with-param name="maxLength">
					<xsl:if test="normalize-space($maxLength2)!='' and $maxLength2!='ignore'">
						<xsl:value-of select="$maxLength2"/>
					</xsl:if>
				</xsl:with-param>
				<xsl:with-param name="minimum">
					<xsl:choose>
						<xsl:when test="$type='date'">
							<xsl:value-of select="@minimum"/>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="maximum">
					<xsl:choose>
						<xsl:when test="$type='date'">
							<xsl:value-of select="@maximum"/>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="dateFormat">
					<xsl:choose>
						<xsl:when test="$type='date'">
							<xsl:value-of select="xslNsExt:formatMaskToUse($formatMask, $readOnly, '0')"/>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="watermark" select="@watermark"/>
				<xsl:with-param name="jSFormatter">
					<xsl:call-template name="getJSFormatter">
						<xsl:with-param name="type">
							<xsl:value-of select="$type"/>
						</xsl:with-param>
						<xsl:with-param name="fieldMask">
							<xsl:value-of select="$fieldMask"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:with-param>
				<xsl:with-param name="size">
					<xsl:choose>
						<xsl:when test="$maxLength2 &lt; 50">
							<xsl:variable name="fieldMaskLength" select="string-length($fieldMask)"/>
							<xsl:choose>
								<xsl:when test="( $fieldMaskLength &gt; $maxLength2 ) and $defaultMaxLength!='ignore'">
									<xsl:value-of select="$fieldMaskLength"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$maxLength2"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="contains($fieldMask,'width:')">
							<xsl:variable name="fieldSize">
								<xsl:value-of select="substring-before(substring-after($fieldMask,'width:'),':')"/>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="normalize-space($fieldSize)!=''">
									<xsl:value-of select="$fieldSize"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>50</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="($fieldMask!='' or @maximum!='') and $defaultMaxLength!='ignore'">
							<xsl:value-of select="$defaultMaxLength"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>50</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="type">
					<xsl:choose>
						<xsl:when test="$hideOn='entry' or $hideOn='always'">
							<xsl:text>password</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>text</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
				<xsl:with-param name="partyRecordMappingId" select="@partyRecordMappingId"/>
				<xsl:with-param name="partyMappingField" select="@partyMappingField"/>
				<xsl:with-param name="partyPopUpTitle" select="@partyPopUpTitle"/>
				<xsl:with-param name="partyMappingSlideOut" select="@partyMappingSlideOut"/>
				<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
			</xsl:call-template>
		</div>
	</xsl:template>
	<xsl:template name="getHelpTabbingFlag">
		<xsl:text>0</xsl:text>
	</xsl:template>
</xsl:stylesheet>