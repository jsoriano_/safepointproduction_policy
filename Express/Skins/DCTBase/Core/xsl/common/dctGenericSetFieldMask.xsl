﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:include href="dctApplication.xsl"/>
	<!--*************************************************************************************************************
		This template is for generating the field properties
		************************************************************************************************************* -->
	<xsl:template name="getJSFormatter">
		<xsl:param name="type"/>
		<xsl:param name="fieldMask"/>
		<xsl:choose>
			<xsl:when test="contains($fieldMask, 'upper')">upper</xsl:when>
			<xsl:when test="$fieldMask='lower'">lower</xsl:when>
			<xsl:when test="($type='string') and ($fieldMask != '')">string</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getJSInputClass">
		<xsl:param name="type"/>
		<xsl:param name="fieldMask"/>
		<xsl:choose>
			<xsl:when test="contains($fieldMask, 'upper')">interviewInputStringField</xsl:when>
			<xsl:when test="$fieldMask='lower'">interviewInputStringField</xsl:when>
			<xsl:when test="$fieldMask='time' or $fieldMask='time24'">interviewInputTimeField</xsl:when>
			<xsl:when test="$fieldMask='ssn'">interviewInputSSNField</xsl:when>
			<xsl:when test="$fieldMask='phone'">interviewInputPhoneField</xsl:when>
			<xsl:when test="contains($fieldMask, 'phoneNonUS_')">
				<xsl:value-of select="concat('interviewInputPhoneField', substring($fieldMask, 12))" />
			</xsl:when>
			<xsl:when test="$fieldMask='zipcode'">interviewInputZipCodeField</xsl:when>
			<xsl:when test="contains($fieldMask, 'zipcodeNonUS_')">
				<xsl:value-of select="concat('interviewInputZipCodeField', substring($fieldMask, 14))" />
			</xsl:when>			
			<xsl:when test="$fieldMask='email'">interviewInputEmailField</xsl:when>
			<xsl:when test="$fieldMask='ip'">interviewInputIPField</xsl:when>
			<xsl:when test="$type='int'">interviewInputIntField</xsl:when>
			<xsl:when test="$type='float'">interviewInputFloatField</xsl:when>
			<xsl:otherwise>interviewInputStringField</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="getOtherParameters">
		<xsl:param name="type"/>
		<xsl:param name="fieldMask"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="maximum"/>
		<xsl:param name="minimum"/>
		<xsl:choose>
			<xsl:when test="starts-with($fieldMask,'regex:')">
				<xsl:variable name="regexString">
					<xsl:value-of select="substring($fieldMask,7)"/>
				</xsl:variable>
				<xsl:text>,</xsl:text>
				<xsl:text>dctRegEx: "</xsl:text>
				<xsl:choose>
					<xsl:when test ="contains($regexString,':')">
						<xsl:value-of select="substring-before($regexString,':')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$regexString"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>"</xsl:text>
				<xsl:text>,</xsl:text>
        <xsl:if test ="substring-after(substring-after($regexString,':'), ':') != ''">
        <xsl:text>dctRegExMsg: "</xsl:text>
        <xsl:value-of select="xslNsExt:replaceText(substring-after($regexString,':'),substring-after(substring-after($regexString,':'), ':'),'')"/>
        <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:if test ="substring-after(substring-after($regexString,':'), ':') = ''">
        <xsl:text>dctRegExMsg: "</xsl:text>
        <xsl:value-of select="substring-after($regexString,':')"/>
        <xsl:text>"</xsl:text>
        </xsl:if>
        <xsl:text>,</xsl:text>
				<xsl:text>dctRegExAttribs: "</xsl:text>
				<xsl:value-of select="substring-after(substring-after($regexString,':'), ':')"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:when test="$fieldMask='time'">
				<xsl:text>,</xsl:text>
				<xsl:text>dctMilitaryTime: "0"</xsl:text>
			</xsl:when>
			<xsl:when test="$fieldMask='time24'">
				<xsl:text>,</xsl:text>
				<xsl:text>dctMilitaryTime: "1"</xsl:text>
			</xsl:when>
			<xsl:when test="($type='string') and ($fieldMask != '')">
				<xsl:text>,</xsl:text>
				<xsl:text>dctFormatMask: "</xsl:text>
				<xsl:value-of select="$formatMask"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:when test="$type='int' or $type='float'">
				<xsl:text>,</xsl:text>
				<xsl:text>dctNumberFormat: "</xsl:text>
				<xsl:choose>
					<xsl:when test="normalize-space($formatMask)!=''">
						<xsl:value-of select="xslNsExt:numberFormatMaskToUse($formatMask, $manuscriptBaseCulture)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsExt:numberFormatMaskToUse($fieldMask, $manuscriptBaseCulture)"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>dctCurrencyCode: "</xsl:text>
				<xsl:value-of select="/page/state/currencyCode"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctNumberMax: "</xsl:text>
				<xsl:value-of select="$maximum"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctNumberMin: "</xsl:text>
				<xsl:value-of select="$minimum"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>