﻿
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctCommonWidgets.xsl"/>
	<xsl:import href="dctGenericSetMaxLength.xsl"/>
	<xsl:import href="dctGenericSetFieldMask.xsl"/>
	<xsl:import href="dctGenericSetFieldFormat.xsl"/>
	<!--	*************************************************************************************************************
		This template is for building drop down div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildSelectDiv">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="watermark"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="maxLength"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class"/>
		<xsl:param name="optionList"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="readOnly">false</xsl:param>
		<xsl:param name="disabled">false</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="defaultValue"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>"</xsl:text>
				<xsl:call-template name="addConfigProperty">
					<xsl:with-param name="name">dctCaption</xsl:with-param>
					<xsl:with-param name="type">string</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$caption"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:text>,</xsl:text>
				<xsl:text>dctType: "</xsl:text>
				<xsl:value-of select="$type"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>inputType: "text",</xsl:text>
				<xsl:text>hiddenName: "</xsl:text>
				<xsl:value-of select="$name"/>
				<xsl:text>",</xsl:text>
				<xsl:text>emptyText: </xsl:text>
				<xsl:choose>
					<xsl:when test="$watermark!=''">
						<xsl:text>"</xsl:text>
						<xsl:value-of select="xslNsExt:replaceText($watermark,$doubleQuote,$escapedDoubleQuote)"/>
						<xsl:text>"</xsl:text>
					</xsl:when>
					<xsl:when test="substring($formatMask,1,11)='blankOption'">
						<xsl:text>"</xsl:text>
						<xsl:choose>
							<xsl:when test="substring($formatMask,1,12)='blankOption:'">
								<xsl:value-of select="xslNsExt:replaceText(substring-after($formatMask,':'),$doubleQuote,$escapedDoubleQuote)"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="xslNsExt:replaceText(xslNsExt:FormatString1(xslNsODExt:getDictRes('Select_FormatMask'), xslNsODExt:getDictRes('Select_lower')),$doubleQuote,$escapedDoubleQuote)"/>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>"</xsl:text>
					</xsl:when>
					<xsl:otherwise>null</xsl:otherwise>
				</xsl:choose>
				<xsl:text>,</xsl:text>
				<xsl:text>dctStore: [</xsl:text>
				<xsl:if test="$interviewItems='true'">
					<xsl:choose>
						<xsl:when test="$watermark!=''">
							<xsl:text>[ "", "</xsl:text>
							<xsl:value-of select="xslNsExt:replaceText($watermark,$doubleQuote,$escapedDoubleQuote)"/>
							<xsl:text>"]</xsl:text>
							<xsl:if test="list[1]/option[1]">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:when>
						<xsl:when test="substring($formatMask,1,11)='blankOption'">
							<xsl:choose>
								<xsl:when test="substring($formatMask,1,12)='blankOption:'">
									<xsl:text>[ "", "</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(substring-after($formatMask,':'),$doubleQuote,$escapedDoubleQuote)"/>
									<xsl:text>"]</xsl:text>
									<xsl:if test="list[1]/option[1]">
										<xsl:text>,</xsl:text>
									</xsl:if>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>[ "", "</xsl:text>
									<xsl:value-of select="xslNsExt:replaceText(xslNsExt:FormatString1(xslNsODExt:getDictRes('Select_FormatMask'), xslNsODExt:getDictRes('Select_lower')),$doubleQuote,$escapedDoubleQuote)"/>
									<xsl:text>"]</xsl:text>
									<xsl:if test="list[1]/option[1]">
										<xsl:text>,</xsl:text>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
					</xsl:choose>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="$interviewItems='true'">
						<xsl:variable name="maxLengthSwitch" select="$maxLength"/>
						<xsl:variable name="fieldType" select="$type"/>
						<xsl:for-each select="list">
							<xsl:call-template name="buildOptions">
								<xsl:with-param name="maxLength" select="$maxLengthSwitch"/>
								<xsl:with-param name="fieldType" select="$fieldType"/>
							</xsl:call-template>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="msxsl:node-set($optionList)/option">
							<xsl:text>[ "</xsl:text>
							<xsl:value-of select="@value"/>
							<xsl:text>", "</xsl:text>
							<xsl:value-of select="."/>
							<xsl:text>"]</xsl:text>
							<xsl:if test="position() != last()">
								<xsl:text>,</xsl:text>
							</xsl:if>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>]</xsl:text>
				<xsl:if test="normalize-space($disabled)!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>disabled: </xsl:text>
					<xsl:value-of select="$disabled"/>
				</xsl:if>
				<xsl:if test="normalize-space($readOnly)!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>readOnly: </xsl:text>
					<xsl:value-of select="$readOnly"/>
				</xsl:if>
				<xsl:if test="$includeDebugInfo = 'true'">
					<xsl:text>,</xsl:text>
					<xsl:text>fieldRef: "</xsl:text>
					<xsl:value-of select="$fieldRef_Val"/>
					<xsl:text>",</xsl:text>
					<xsl:text>objectRef: "</xsl:text>
					<xsl:value-of select="$objectRef_Val"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$actionID!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>dctActionId: "</xsl:text>
					<xsl:value-of select="$actionID"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="/page/settings/Controls/ComboBoxes/@maxSize">
					<xsl:text>,</xsl:text>
					<xsl:text>dctMaxSize: "</xsl:text>
					<xsl:value-of select="/page/settings/Controls/ComboBoxes/@maxSize"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="normalize-space($elementClass)!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>cls: "</xsl:text>
					<xsl:value-of select="$elementClass"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="normalize-space($width)!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomComboProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="value">
					<xsl:choose>
						<xsl:when test="$value != ''">
							<xsl:value-of select="$value"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="msxsl:node-set($optionList)/option[1]/@value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="normalize-space($defaultValue)!=''">
				<xsl:element name="input">
					<xsl:attribute name="value">
						<xsl:value-of select="$defaultValue"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>hidden</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building richtextarea div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildRichTextAreaDiv">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldMask"/>
		<xsl:param name="maxLength"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="disabled">false</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="height"/>
		<xsl:param name="defaultValue"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>"</xsl:text>
				<xsl:call-template name="addConfigProperty">
					<xsl:with-param name="name">dctCaption</xsl:with-param>
					<xsl:with-param name="type">string</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$caption"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:text>,</xsl:text>
				<xsl:text>fontFamilies : ['</xsl:text>
				<xsl:for-each select="xslNsExt:GetInstalledFonts()">
					<xsl:value-of select="."/>
					<xsl:choose>
						<xsl:when test="position() != last()">
							<xsl:text>','</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>'</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<xsl:text>]</xsl:text>
				<xsl:text>,</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>name: "</xsl:text>
				<xsl:value-of select="$name"/>
				<xsl:text>",</xsl:text>
				<xsl:text>readOnly: </xsl:text>
				<xsl:choose>
					<xsl:when test="$readOnly='1'">
						<xsl:text>true</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>false</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$includeDebugInfo = 'true'">
					<xsl:text>,</xsl:text>
					<xsl:text>fieldRef: "</xsl:text>
					<xsl:value-of select="$fieldRef_Val"/>
					<xsl:text>",</xsl:text>
					<xsl:text>objectRef: "</xsl:text>
					<xsl:value-of select="$objectRef_Val"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$actionID!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>dctActionId: "</xsl:text>
					<xsl:value-of select="$actionID"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$maxLength">
					<xsl:if test="$maxLength != '' and $maxLength &gt; 0">
						<xsl:text>,</xsl:text>
						<xsl:text>maxLength: </xsl:text>
						<xsl:value-of select="$maxLength"/>
					</xsl:if>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'rows:')">
					<xsl:text>,</xsl:text>
					<xsl:text>rows: </xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'rows:'),':')"/>
					<xsl:text></xsl:text>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'width:')">
					<xsl:text>,</xsl:text>
					<xsl:text>dctWidth: "</xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'width:'),':')"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'showToolbar:')">
					<xsl:text>,</xsl:text>
					<xsl:text>dctShowToolbar: </xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'showToolbar:'),':')"/>
					<xsl:text></xsl:text>
				</xsl:if>
				<xsl:if test="$elementClass">
					<xsl:text>,</xsl:text>
					<xsl:text>cls: "</xsl:text>
					<xsl:value-of select="$elementClass"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$height">
					<xsl:text>,</xsl:text>
					<xsl:text>height: </xsl:text>
					<xsl:value-of select="$height"/>
				</xsl:if>
				<xsl:if test="$width">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomTextareaProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$value"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="$defaultValue">
				<xsl:element name="input">
					<xsl:attribute name="value">
						<xsl:value-of select="$defaultValue"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>hidden</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building textarea div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildTextAreaDiv">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="fieldMask"/>
		<xsl:param name="maxLength"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="disabled">false</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="rows"/>
		<xsl:param name="defaultValue"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>"</xsl:text>
				<xsl:call-template name="addConfigProperty">
					<xsl:with-param name="name">dctCaption</xsl:with-param>
					<xsl:with-param name="type">string</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$caption"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:text>,</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>name: "</xsl:text>
				<xsl:value-of select="$name"/>
				<xsl:text>",</xsl:text>
				<xsl:text>readOnly: </xsl:text>
				<xsl:choose>
					<xsl:when test="$readOnly='1'">
						<xsl:text>true</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>false</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:if test="$includeDebugInfo = 'true'">
					<xsl:text>,</xsl:text>
					<xsl:text>fieldRef: "</xsl:text>
					<xsl:value-of select="$fieldRef_Val"/>
					<xsl:text>",</xsl:text>
					<xsl:text>objectRef: "</xsl:text>
					<xsl:value-of select="$objectRef_Val"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$actionID!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>dctActionId: "</xsl:text>
					<xsl:value-of select="$actionID"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$maxLength">
					<xsl:if test="$maxLength != '' and $maxLength &gt; 0">
						<xsl:text>,</xsl:text>
						<xsl:text>maxWarningLength: </xsl:text>
						<xsl:value-of select="$maxLength"/>
					</xsl:if>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'rows:')">
					<xsl:text>,</xsl:text>
					<xsl:text>rows: "</xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'rows:'),':')"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'width:')">
					<xsl:text>,</xsl:text>
					<xsl:text>dctWidth: "</xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'width:'),':')"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="contains($fieldMask,'height:')">
					<xsl:text>,</xsl:text>
					<xsl:text>dctHeight: "</xsl:text>
					<xsl:value-of select="substring-before(substring-after($fieldMask,'height:'),':')"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$elementClass">
					<xsl:text>,</xsl:text>
					<xsl:text>cls: "</xsl:text>
					<xsl:value-of select="$elementClass"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$rows">
					<xsl:text>,</xsl:text>
					<xsl:text>rows: </xsl:text>
					<xsl:value-of select="$rows"/>
				</xsl:if>
				<xsl:if test="$width">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomTextareaProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$value"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="$defaultValue">
				<xsl:element name="input">
					<xsl:attribute name="value">
						<xsl:value-of select="$defaultValue"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>hidden</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<!--*************************************************************************************************************
		This template is for building input div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildInputDiv">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="dctType"/>
		<xsl:param name="type">text</xsl:param>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="fieldMask"/>
		<xsl:param name="maxLength"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="disabled">false</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="defaultValue"/>
		<xsl:param name="fieldOtherParms"/>
		<xsl:param name="minimum"/>
		<xsl:param name="maximum"/>
		<xsl:param name="watermark"/>
		<xsl:param name="jSFormatter"/>
		<xsl:param name="size">20</xsl:param>
		<xsl:param name="dateFormat"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:param name="partyRecordMappingId"/>
		<xsl:param name="partyMappingField"/>
		<xsl:param name="spotlight"/>
		<xsl:param name="partyPopUpTitle"/>
		<xsl:param name="partyMappingSlideOut"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:choose>
						<xsl:when test="$interviewItems='true'">
							<xsl:call-template name="FormatValue">
								<xsl:with-param name="value">
									<xsl:value-of select="$value"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$value"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>"</xsl:text>
				<xsl:call-template name="addConfigProperty">
					<xsl:with-param name="name">dctCaption</xsl:with-param>
					<xsl:with-param name="type">string</xsl:with-param>
					<xsl:with-param name="value">
						<xsl:value-of select="$caption"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:text>,</xsl:text>
				<xsl:text>dctType: "</xsl:text>
				<xsl:value-of select="$dctType"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>dctJSFormatter: "</xsl:text>
				<xsl:value-of select="$jSFormatter"/>
				<xsl:text>",</xsl:text>
				<xsl:text>inputType: "</xsl:text>
				<xsl:value-of select="$type"/>
				<xsl:text>",</xsl:text>
				<xsl:text>name: "</xsl:text>
				<xsl:value-of select="$name"/>
				<xsl:text>",</xsl:text>
				<xsl:text>size: </xsl:text>
				<xsl:value-of select="$size"/>
				<xsl:if test="$includeDebugInfo = 'true'">
					<xsl:text>,</xsl:text>
					<xsl:text>fieldRef: "</xsl:text>
					<xsl:value-of select="$fieldRef_Val"/>
					<xsl:text>",</xsl:text>
					<xsl:text>objectRef: "</xsl:text>
					<xsl:value-of select="$objectRef_Val"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$maxLength!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>maxLength: </xsl:text>
					<xsl:value-of select="$maxLength"/>
				</xsl:if>
				<xsl:if test="$disabled!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>disabled: </xsl:text>
					<xsl:value-of select="$disabled"/>
				</xsl:if>
				<xsl:if test="$watermark!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>emptyText: "</xsl:text>
					<xsl:value-of select="$watermark"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$actionID!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>dctActionId: "</xsl:text>
					<xsl:value-of select="$actionID"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$minimum!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>minValue: "</xsl:text>
					<xsl:value-of select="$minimum"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$maximum!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>maxValue: "</xsl:text>
					<xsl:value-of select="$maximum"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$dateFormat!=''">
					<xsl:text>,</xsl:text>
					<xsl:text>dateFormat: "</xsl:text>
					<xsl:value-of select="$dateFormat"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$elementClass">
					<xsl:text>,</xsl:text>
					<xsl:text>cls: "</xsl:text>
					<xsl:value-of select="$elementClass"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$width">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$partyRecordMappingId">
					<xsl:text>,</xsl:text>
					<xsl:text>dctPartyRecordMappingId: "</xsl:text>
					<xsl:value-of select="$partyRecordMappingId"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$partyMappingField">
					<xsl:text>,</xsl:text>
					<xsl:text>dctPartyMappingField: "</xsl:text>
					<xsl:value-of select="$partyMappingField"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$partyPopUpTitle">
					<xsl:text>,</xsl:text>
					<xsl:text>dctPartyPopUpTitle: "</xsl:text>
					<xsl:value-of select="$partyPopUpTitle"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$partyMappingSlideOut">
					<xsl:text>,</xsl:text>
					<xsl:text>dctPartyMappingSlideOut: "</xsl:text>
					<xsl:value-of select="$partyMappingSlideOut"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="normalize-space($fieldOtherParms)!=''">
					<xsl:value-of select="$fieldOtherParms"/>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomInputProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:if test="$defaultValue">
				<xsl:element name="input">
					<xsl:attribute name="value">
						<xsl:value-of select="$defaultValue"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>hidden</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for building radio div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildRadioDiv">
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="caption"/>
		<xsl:param name="type"/>
		<xsl:param name="actionID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="formatMask"/>
		<xsl:param name="maxLength"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="disabled">false</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="columns">1</xsl:param>
		<xsl:param name="vertical">false</xsl:param>
		<xsl:param name="defaultValue"/>
		<xsl:param name="radiobuttons"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="normalize-space($class)!=''">
						<xsl:value-of select="$class"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>interviewRadioGroup</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>"</xsl:text>
				<xsl:choose>
					<xsl:when test="contains($formatMask,'radio:1')">
						<xsl:text>,</xsl:text>
						<xsl:text>vertical: true</xsl:text>
						<xsl:text>,</xsl:text>
						<xsl:text>columns: 1</xsl:text>
					</xsl:when>
					<xsl:when test="$vertical">
						<xsl:text>,</xsl:text>
						<xsl:text>vertical: </xsl:text>
						<xsl:value-of select="$vertical"/>
						<xsl:if test="$vertical='true'">
							<xsl:text>,</xsl:text>
							<xsl:text>columns: </xsl:text>
							<xsl:value-of select="$columns"/>
						</xsl:if>
					</xsl:when>
				</xsl:choose>
				<xsl:text>,</xsl:text>
				<xsl:text>dctRadioButtons: [</xsl:text>
				<xsl:value-of select="$radiobuttons"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$width">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomRadioGroupProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$value"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building drop down list options for interview pages
		************************************************************************************************************* -->
	<xsl:template name="buildInterviewRadioButtons">
		<xsl:param name="fieldID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="actionID"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:for-each select="option">
			<xsl:variable name="truncatedRadioValue">
				<xsl:choose>
					<xsl:when test="@cacheIndex and (/page/content/getPage/@currentPageID and (/page/content/getPage/@currentPageID != ''))">
						<xsl:value-of select="@cacheIndex"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@value"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:text>{</xsl:text>
			<xsl:text>id: "</xsl:text>
			<xsl:value-of select="$fieldID"/>
			<xsl:text>_</xsl:text>
			<xsl:value-of select="count(preceding-sibling::option)"/>
			<xsl:text>"</xsl:text>
			<xsl:call-template name="addConfigProperty">
				<xsl:with-param name="name">boxLabel</xsl:with-param>
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:call-template name="formatText">
						<xsl:with-param name="text" select="@caption"/>
					</xsl:call-template>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:text>,</xsl:text>
			<xsl:text>name: "</xsl:text>
			<xsl:value-of select="$name"/>
			<xsl:text>"</xsl:text>
			<xsl:call-template name="addConfigProperty">
				<xsl:with-param name="name">inputValue</xsl:with-param>
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$truncatedRadioValue"/>
				</xsl:with-param>
			</xsl:call-template>
			<xsl:text>,</xsl:text>
			<xsl:text>checked: </xsl:text>
			<xsl:choose>
				<xsl:when test="$value=$truncatedRadioValue">
					<xsl:text>true</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>false</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$includeDebugInfo = 'true'">
				<xsl:text>,</xsl:text>
				<xsl:text>fieldRef: "</xsl:text>
				<xsl:value-of select="$fieldRef_Val"/>
				<xsl:text>",</xsl:text>
				<xsl:text>objectRef: "</xsl:text>
				<xsl:value-of select="$objectRef_Val"/>
				<xsl:text>"</xsl:text>
			</xsl:if>
			<xsl:if test="$actionID!=''">
				<xsl:text>,</xsl:text>
				<xsl:text>dctActionId: "</xsl:text>
				<xsl:value-of select="$actionID"/>
				<xsl:text>"</xsl:text>
			</xsl:if>
			<xsl:if test="$extraConfigItems">
				<xsl:value-of select="$extraConfigItems"/>
			</xsl:if>
			<xsl:call-template name="addCustomRadioProperties"/>
			<xsl:text>}</xsl:text>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building radio button for system pages
		************************************************************************************************************* -->
	<xsl:template name="buildSystemRadioButtons">
		<xsl:param name="fieldID"/>
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="label"/>
		<xsl:param name="checked">false</xsl:param>
		<xsl:param name="dctClassName">dctsystemradiofield</xsl:param>
		<xsl:param name="extraConfigItems"/>
		<xsl:text>id: "</xsl:text>
		<xsl:value-of select="$fieldID"/>
		<xsl:text>"</xsl:text>
		<xsl:call-template name="addConfigProperty">
			<xsl:with-param name="name">boxLabel</xsl:with-param>
			<xsl:with-param name="type">string</xsl:with-param>
			<xsl:with-param name="value">
				<xsl:value-of select="$label"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<xsl:text>name: "</xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:text>"</xsl:text>
		<xsl:call-template name="addConfigProperty">
			<xsl:with-param name="name">inputValue</xsl:with-param>
			<xsl:with-param name="type">string</xsl:with-param>
			<xsl:with-param name="value">
				<xsl:value-of select="$value"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text>,</xsl:text>
		<xsl:text>checked: </xsl:text>
		<xsl:value-of select="$checked"/>
		<xsl:text>,</xsl:text>
		<xsl:text>dctClassName: "</xsl:text>
		<xsl:value-of select="$dctClassName"/>
		<xsl:text>"</xsl:text>
		<xsl:if test="$extraConfigItems">
			<xsl:value-of select="$extraConfigItems"/>
		</xsl:if>
		<xsl:call-template name="addCustomRadioProperties"/>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building checkbox div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildCheckBoxDiv">
		<xsl:param name="fieldID"/>
		<xsl:param name="required"/>
		<xsl:param name="name"/>
		<xsl:param name="interviewItems">true</xsl:param>
		<xsl:param name="class">interviewCheckBoxGroup</xsl:param>
		<xsl:param name="defaultValue"/>
		<xsl:param name="checkbox"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="width"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="type">boolean</xsl:param>
		<xsl:param name="extraConfigItems"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Grp</xsl:text>
		</xsl:attribute>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Grp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>dctType: "</xsl:text>
				<xsl:value-of select="$type"/>
				<xsl:text>",</xsl:text>
				<xsl:text>dctRequired: "</xsl:text>
				<xsl:choose>
					<xsl:when test="$required='1'">1</xsl:when>
					<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>dctCheckBoxes: [</xsl:text>
				<xsl:value-of select="$checkbox"/>
				<xsl:text>]</xsl:text>
				<xsl:if test="$width">
					<xsl:text>,</xsl:text>
					<xsl:text>width: </xsl:text>
					<xsl:value-of select="$width"/>
				</xsl:if>
				<xsl:if test="$elementClass">
					<xsl:text>,</xsl:text>
					<xsl:text>cls: "</xsl:text>
					<xsl:value-of select="$elementClass"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomCheckboxGroupProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:if test="$defaultValue">
				<xsl:element name="input">
					<xsl:attribute name="value">
						<xsl:value-of select="$defaultValue"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="$name"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>hidden</xsl:text>
					</xsl:attribute>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building checkbox options for interview pages
		************************************************************************************************************* -->
	<xsl:template name="buildCheckBoxObject">
		<xsl:param name="fieldID"/>
		<xsl:param name="name"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="actionID"/>
		<xsl:param name="readOnly">0</xsl:param>
		<xsl:param name="boxLabel"/>
		<xsl:param name="inputValue">1</xsl:param>
		<xsl:param name="checked">false</xsl:param>
		<xsl:param name="dctClassName"/>
		<xsl:param name="elementClass"/>
		<xsl:param name="hidden"/>
		<xsl:param name="type">boolean</xsl:param>
		<xsl:param name="extraConfigItems"/>
		<xsl:text>{</xsl:text>
		<xsl:text>id: "</xsl:text>
		<xsl:value-of select="$fieldID"/>
		<xsl:text>",</xsl:text>
		<xsl:text>disabled: </xsl:text>
		<xsl:choose>
			<xsl:when test="$readOnly='1'">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<xsl:choose>
			<xsl:when test="$boxLabel='false' or normalize-space($boxLabel)=''">
				<xsl:text>hideLabel: true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>boxLabel: "</xsl:text>
				<xsl:value-of select="$boxLabel"/>
				<xsl:text>"</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>,</xsl:text>
		<xsl:text>name: "</xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:text>",</xsl:text>
		<xsl:text>inputValue: "</xsl:text>
		<xsl:value-of select="$inputValue"/>
		<xsl:text>",</xsl:text>
		<xsl:text>checked: </xsl:text>
		<xsl:value-of select="$checked"/>
		<xsl:if test="$includeDebugInfo = 'true'">
			<xsl:text>,</xsl:text>
			<xsl:text>fieldRef: "</xsl:text>
			<xsl:value-of select="$fieldRef_Val"/>
			<xsl:text>",</xsl:text>
			<xsl:text>objectRef: "</xsl:text>
			<xsl:value-of select="$objectRef_Val"/>
			<xsl:text>"</xsl:text>
		</xsl:if>
		<xsl:if test="$elementClass">
			<xsl:text>,</xsl:text>
			<xsl:text>ctCls: "</xsl:text>
			<xsl:value-of select="$elementClass"/>
			<xsl:text>"</xsl:text>
		</xsl:if>
		<xsl:if test="$actionID!=''">
			<xsl:text>,</xsl:text>
			<xsl:text>dctActionId: "</xsl:text>
			<xsl:value-of select="$actionID"/>
			<xsl:text>"</xsl:text>
		</xsl:if>
		<xsl:if test="$hidden">
			<xsl:text>,</xsl:text>
			<xsl:text>hidden: </xsl:text>
			<xsl:value-of select="$hidden"/>
		</xsl:if>
		<xsl:if test="$extraConfigItems">
			<xsl:value-of select="$extraConfigItems"/>
		</xsl:if>
		<xsl:text>,</xsl:text>
		<xsl:text>dctType: "</xsl:text>
		<xsl:value-of select="$type"/>
		<xsl:text>",</xsl:text>
		<xsl:text>dctClassName: "</xsl:text>
		<xsl:value-of select="$dctClassName"/>
		<xsl:text>"</xsl:text>
		<xsl:call-template name="addCustomCheckboxProperties"/>
		<xsl:text>}</xsl:text>
	</xsl:template>

	<!--	*************************************************************************************************************
		This template is for building drop down list options for interview pages
		************************************************************************************************************* -->
	<xsl:template name="buildOptions">
		<xsl:param name="maxLength"/>
		<xsl:param name="fieldType"/>
		<xsl:for-each select="option">
			<xsl:text>[ "</xsl:text>
			<xsl:choose>
				<xsl:when test="@cacheIndex and (/page/content/getPage/@currentPageID and (/page/content/getPage/@currentPageID != ''))">
					<xsl:value-of select="xslNsExt:replaceText(@cacheIndex,$doubleQuote,$escapedDoubleQuote)"/>
				</xsl:when>
				<xsl:when test="$maxLength &gt; 0">
					<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(substring(@value, 1, $maxLength), '0')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(@value, '0')"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>", "</xsl:text>
			<xsl:choose>
				<xsl:when test="$fieldType='date' and substring-before(@caption,' ')!=''">
					<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(substring-before(@caption,' '), '0')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(@caption, '0')"/>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>"]</xsl:text>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!--*************************************************************************************************************
		This template is for building display div that defines the ExtJs component
		************************************************************************************************************* -->
	<xsl:template name="buildReadOnlyDiv">
		<xsl:param name="fieldID"/>
		<xsl:param name="fieldRef_Val"/>
		<xsl:param name="objectRef_Val"/>
		<xsl:param name="includeDebugInfo"/>
		<xsl:param name="class"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:param name="value"/>
		<xsl:param name="hideOn">
			<xsl:text>never</xsl:text>
		</xsl:param>
		<xsl:param name="truncateField"/>
		<xsl:param name="width"/>
		<xsl:attribute name="id">
			<xsl:value-of select="$fieldID"/>
			<xsl:text>Cmp</xsl:text>
		</xsl:attribute>
		<xsl:attribute name="data-cmpid">
			<xsl:value-of select="$fieldID"/>
		</xsl:attribute>
		<xsl:if test="$truncateField='1'">
			<xsl:attribute name="data-qtip">
				<xsl:value-of select="$value"/>
			</xsl:attribute>
			<xsl:attribute name="data-tip">
				<xsl:value-of select="$value"/>
			</xsl:attribute>
		</xsl:if>
		<xsl:element name="div">
			<xsl:attribute name="class">
				<xsl:value-of select="$class"/>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>renderTo: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>Cmp</xsl:text>
				<xsl:text>",</xsl:text>
				<xsl:text>id: "</xsl:text>
				<xsl:value-of select="$fieldID"/>
				<xsl:text>"</xsl:text>
				<xsl:if test="$includeDebugInfo = 'true'">
					<xsl:text>,</xsl:text>
					<xsl:text>fieldRef: "</xsl:text>
					<xsl:value-of select="$fieldRef_Val"/>
					<xsl:text>",</xsl:text>
					<xsl:text>objectRef: "</xsl:text>
					<xsl:value-of select="$objectRef_Val"/>
					<xsl:text>"</xsl:text>
				</xsl:if>
				<xsl:if test="$extraConfigItems">
					<xsl:value-of select="$extraConfigItems"/>
				</xsl:if>
				<xsl:call-template name="addCustomReadOnlyProperties"/>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
			<xsl:call-template name="addDataValue">
				<xsl:with-param name="type">string</xsl:with-param>
				<xsl:with-param name="value">
					<xsl:value-of select="$value"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:element>
	</xsl:template>

	<xsl:template name="buildExternalModulePanel">
		<xsl:param name="class"/>
		<xsl:param name="type"/>
		<xsl:param name="extraConfigItems"/>
		<xsl:if test="$type != ''">
			<xsl:element name="div">
				<xsl:attribute name="externalContentType">
					<xsl:value-of select="$type"/>
				</xsl:attribute>
				<xsl:if test="$class != ''">
					<xsl:attribute name="class">
						<xsl:value-of select="$class"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="id">
					<xsl:value-of select="@uid"/>
					<xsl:text>-externalContentCmp</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="data-cmpid">
					<xsl:value-of select="@uid"/>
					<xsl:text>-externalContentPanel</xsl:text>
				</xsl:attribute>
				<xsl:element name="div">
					<xsl:attribute name="class">
						<xsl:text>externalModules</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="data-config">
						<xsl:text>{</xsl:text>
						<xsl:text>renderTo: "</xsl:text>
						<xsl:value-of select="@uid"/>
						<xsl:text>-externalContentCmp</xsl:text>
						<xsl:text>",</xsl:text>
						<xsl:text>id: "</xsl:text>
						<xsl:value-of select="@uid"/>
						<xsl:text>-externalContentPanel</xsl:text>
						<xsl:text>",</xsl:text>
						<xsl:text>dctContentType: "</xsl:text>
						<xsl:value-of select="$type"/>
						<xsl:text>",</xsl:text>
						<xsl:text>dctContentName: </xsl:text>
						<xsl:choose>
							<xsl:when test="normalize-space(@contentName)!=''">
								<xsl:text>"</xsl:text>
								<xsl:value-of select="@contentName"/>
								<xsl:text>"</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>null</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:text>dctContentScrollable: </xsl:text>
						<xsl:choose>
							<xsl:when test="@contentScrollable='1'">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:text>dctContentUrl: </xsl:text>
						<xsl:choose>
							<xsl:when test="normalize-space(@contentUrl)!=''">
								<xsl:text>"</xsl:text>
								<xsl:value-of select="@contentUrl"/>
								<xsl:text>"</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>null</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:text>border: </xsl:text>
						<xsl:choose>
							<xsl:when test="@contentBorder='show'">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:text>collapsible: </xsl:text>
						<xsl:choose>
							<xsl:when test="@contentCollapsible='1'">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text>,</xsl:text>
						<xsl:text>frame: </xsl:text>
						<xsl:choose>
							<xsl:when test="@contentFrame='show'">
								<xsl:text>true</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>false</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="normalize-space(@contentTitle)!=''">
							<xsl:text>,</xsl:text>
							<xsl:text>title: "</xsl:text>
							<xsl:value-of select="@contentTitle"/>
							<xsl:text>"</xsl:text>
						</xsl:if>
						<xsl:if test="normalize-space(@contentHeight)!=''">
							<xsl:text>,</xsl:text>
							<xsl:text>height: </xsl:text>
							<xsl:value-of select="@contentHeight"/>
						</xsl:if>
						<xsl:if test="$type='html'">
							<xsl:if test="normalize-space(@contentUrl)!=''">
								<xsl:text>,</xsl:text>
								<xsl:text>autoLoad: "</xsl:text>
								<xsl:value-of select="@contentUrl"/>
								<xsl:text>"</xsl:text>
							</xsl:if>
							<xsl:text>,</xsl:text>
							<xsl:text>autoScroll: </xsl:text>
							<xsl:choose>
								<xsl:when test="@contentScrollable='1'">
									<xsl:text>true</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>false</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
						<xsl:if test="$extraConfigItems">
							<xsl:value-of select="$extraConfigItems"/>
						</xsl:if>
						<xsl:call-template name="addCustomInputProperties"/>
						<xsl:text>}</xsl:text>
					</xsl:attribute>
				</xsl:element>
				<!-- [kellyke Nov 17, 10] so I definitely don't like the concept of inline
						style tags, but in this case it's useful to prevent flicker.  If a hard-set
						height is specified anyway, then let's go ahead and make the initial div that height
						so while it's being rendered, content below the page doesn't jump.
				<xsl:attribute name="style">
					<xsl:if test="contentHeight != ''">
						<xsl:text>height:</xsl:text>
						<xsl:value-of select="contentHeight"/>
						<xsl:text>px;</xsl:text>
					</xsl:if>
				</xsl:attribute>-->
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="addConfigProperty">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="type"/>
		<xsl:text>,</xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:text>:</xsl:text>
		<xsl:choose>
			<xsl:when test="$type='string'">
				<xsl:text>"</xsl:text>
				<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode($value, '0')"/>
				<xsl:text>"</xsl:text>
			</xsl:when>
			<xsl:when test="$type='number' or $type='boolean' or $type='object' or $type='array' or $type='regExp'">
				<xsl:value-of select="$value"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="addDataValue">
		<xsl:param name="value"/>
		<xsl:param name="type"/>
		<xsl:attribute name="data-value">
			<xsl:value-of select="$value"/>
		</xsl:attribute>
	</xsl:template>

	<!-- These are used to add custom properties to each type of control.  To add a new property, call the template called 'addConfigProperty'. -->
	<!-- It will take three parameters, 'name', 'value', and 'type'.  The type can be 'string', 'number', 'boolean', 'object', 'array', 'regEx' -->
	<!-- Example -->
	<!-- 	<xsl:call-template name="addConfigProperty"> -->
	<!-- 		<xsl:with-param name="name">newpropertyname</xsl:with-param> -->
	<!-- 		<xsl:with-param name="value">newpropertyvalue</xsl:with-param> -->
	<!-- 		<xsl:with-param name="type">string</xsl:with-param> -->
	<!-- 	</xsl:call-template> -->
	<xsl:template name="addCustomInputProperties">
	</xsl:template>
	<xsl:template name="addCustomExtendedRadioGroupProperties">
	</xsl:template>
	<xsl:template name="addCustomExtendedRadioProperties">
	</xsl:template>
	<xsl:template name="addCustomRadioGroupProperties">
	</xsl:template>
	<xsl:template name="addCustomRadioProperties">
	</xsl:template>
	<xsl:template name="addCustomReadOnlyProperties">
	</xsl:template>
	<xsl:template name="addCustomExtendedListProperties">
	</xsl:template>
	<xsl:template name="addCustomSliderProperties">
	</xsl:template>
	<xsl:template name="addCustomComboProperties">
	</xsl:template>
	<xsl:template name="addCustomBooleanRadioGroupProperties">
	</xsl:template>
	<xsl:template name="addCustomBooleanRadioProperties">
	</xsl:template>
	<xsl:template name="addCustomCheckboxGroupProperties">
	</xsl:template>
	<xsl:template name="addCustomCheckboxProperties">
	</xsl:template>
	<xsl:template name="addCustomInputFileProperties">
	</xsl:template>
	<xsl:template name="addCustomTextareaProperties">
	</xsl:template>
</xsl:stylesheet>