﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctTemplatesCommon.xsl"/>
	<xsl:import href="dctCommonWidgets.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:template name="buildContainer">
		<xsl:param name="noActionBar"/>
		<xsl:param name="noHeader"/>
		<xsl:param name="sysRuleSet"/>
		<xsl:variable name="showHeader" select="not(/page/state/@noHeader='1') and not(/page/content/getPage/body/@noHeader='1') and not($noHeader='1')"/>
		<xsl:variable name="showActionBar" select="not(/page/state/@noActionMenu='1') and not($noActionBar='1')"/>
		<xsl:call-template name="createHtml">
			<xsl:with-param name="showActionBar" select="$showActionBar"/>
			<xsl:with-param name="showHeader" select="$showHeader"/>
			<xsl:with-param name="sysRuleSet" select="$sysRuleSet"/>
		</xsl:call-template>
		<!--/xsl:otherwise>
		</xsl:choose-->
	</xsl:template>
	<!--*************************************************************************************************************
			Transform all html.
		************************************************************************************************************* -->
	<xsl:template name="createHtml">
		<xsl:param name="showActionBar"/>
		<xsl:param name="showHeader"/>
		<xsl:param name="sysRuleSet"/>
		<xsl:variable name="namedRuleSet">
			<xsl:choose>
				<xsl:when test="$sysRuleSet !=''">
					<xsl:value-of select="$sysRuleSet"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$ruleSet"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
		<html id="htmlParent">
			<xsl:attribute name="class">
				<xsl:if test="/page/content/getPage/@page != ''">
					<xsl:value-of select="/page/content/getPage/@page"/>
					<xsl:text>_html</xsl:text>
				</xsl:if>
			</xsl:attribute>
			<head>
				<xsl:call-template name="buildMetaInformation"/>
				<xsl:call-template name="buildHtmlTitle"/>
				<!-- load the ExtJS Core, ExtJs Overrides and base styles -->
				<xsl:choose>
					<xsl:when test="/page/debugMode!='0'">
						<link rel="stylesheet" type="text/css" href="{$cssDir}{/page/settings/Themes/ExtJsTheme/@cssFile}-debug.css" id="ExtJsCoreCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}extJS-Overrides-debug.css" id="ExtJsOverridesCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}dct-base-debug.css" id="dctBaseCSS"/>
					</xsl:when>
					<xsl:otherwise>
						<link rel="stylesheet" type="text/css" href="{$cssDir}{/page/settings/Themes/ExtJsTheme/@cssFile}.css" id="ExtJsCoreCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}extJS-Overrides.css" id="ExtJsOverridesCSS"/>
						<link rel="stylesheet" type="text/css" href="{$cssDir}dct-base.css" id="dctBaseCSS"/>
					</xsl:otherwise>
				</xsl:choose>
				<!-- load the page styles -->
				<xsl:if test="normalize-space(/page/content/@page)!=''">
					<link rel="stylesheet" type="text/css" id="pageCSS" href="{$cssDir}x_{/page/content/@page}.css"/>
				</xsl:if>
				<xsl:if test="normalize-space(/page/content/getPage/properties/keys/keyInfo[@name='family']/@value)!=''">
					<link rel="stylesheet" type="text/css" id="familyCSS" href="{$cssDir}x_{/page/content/getPage/properties/keys/keyInfo[@name='family']/@value}.css"/>
				</xsl:if>
				<!-- Taskbar Styling -->
				<link rel="stylesheet" type="text/css" href="{$winGuidPath}./assets/css/dctTaskbar.css"/>
				<link rel="stylesheet" type="text/css" href="{$winGuidPath}./assets/css/dctWorkbenchToolbar.css"/>
				<xsl:for-each select="xslNsODExt:getFiles('customassets\css','*.css')//file">
					<xsl:if test=". != ''">
						<link rel="stylesheet" type="text/css" href="{$winGuidPath}customassets/css/{.}"></link>
					</xsl:if>
				</xsl:for-each>
				<!-- Themes always loaded last -->
				<link rel="stylesheet" type="text/css" href="{$SkinsDir}/themes/{$theme}/css/theme.css" id="SkinTheme"/>
				<xsl:choose>
					<xsl:when test="/page/debugMode!='0'">
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all-debug.js</xsl:with-param>
						</xsl:call-template>
						<!-- Localization Javascript reference - needs to load before the dct file so the strings are known -->
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base-debug.js</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all.js</xsl:with-param>
						</xsl:call-template>
						<!-- Localization Javascript reference - needs to load before the dct file so the strings are known -->
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base.js</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:call-template name="setAjaxTimeout"/>
				<xsl:call-template name="includeTaskbar"/>
				<xsl:call-template name="buildJavaScriptInclude">
					<xsl:with-param name="dir" select="$customscriptDir"/>
					<xsl:with-param name="filename">custom.js</xsl:with-param>
				</xsl:call-template>
			</head>
			<body id="htmlBody">
				<xsl:call-template name="addCSSClasses"/>
				<xsl:if test="/page/settings/Conversion/ExtJsControls/@ignoreIECheck='1'">
					<xsl:attribute name="data-ignoreIECheck">1</xsl:attribute>
				</xsl:if>
				<div id="innerBody">
					<xsl:call-template name="buildLoadingWindow"/>
					<xsl:call-template name="createInnerBody">
						<xsl:with-param name="showActionBar" select="$showActionBar"/>
						<xsl:with-param name="showHeader" select="$showHeader"/>
					</xsl:call-template>
				</div>
			</body>
		</html>
	</xsl:template>

	<!-- Taskbar Support -->
	<xsl:template name="includeTaskbar">
		<xsl:choose>
			<xsl:when test="/page/debugMode!='0'">
				<script type="text/javascript" src="assets/js/dctTaskbar-debug.js"></script>
			</xsl:when>
			<xsl:otherwise>
				<script type="text/javascript" src="assets/js/dctTaskbar.js"></script>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Spin up the Workbench Toolbar. -->
		<xsl:if test="/page/settings/Controls/WorkbenchToolbar/@enabled='1'">
			<xsl:call-template name="loadWorkbenchToolbar"/>
		</xsl:if>
		<!-- Load Custom/Override CSS/Javascript files. -->
		<xsl:for-each select="xslNsODExt:getFiles('customassets\js', '*.js')//file">
			<xsl:if test=". != ''">
				<script type="text/javascript" src="customassets/js/{.}"></script>
			</xsl:if>
		</xsl:for-each>

		<!-- Load any custom taskbars defined by the carrier -->
		<xsl:call-template name="loadCustomTaskbars"/>

		<!-- Load Hidden Toolbar Fields.. -->
		<xsl:if test="/page/settings/Controls/WorkbenchToolbar/@enabled='1'">
			<xsl:call-template name="hiddenToolbarFields"/>
		</xsl:if>
	</xsl:template>

	<xsl:template name="loadWorkbenchToolbar">
		<xsl:choose>
			<xsl:when test="/page/debugMode!='0'">
				<script type="text/javascript" src="assets/js/dctWorkbenchToolbar-debug.js"></script>
			</xsl:when>
			<xsl:otherwise>
				<script type="text/javascript" src="assets/js/dctWorkbenchToolbar.js"></script>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="loadCustomTaskbars"/>
	<!-- end Taskbar Support -->

	<xsl:template name="setAjaxTimeout">
		<script type="text/javascript">
			<xsl:text>Ext.Ajax.timeout = </xsl:text>
			<xsl:choose>
				<xsl:when test="/page/settings/express/ajaxRequestTimeout &lt;= 0">
					<!-- When -1 or 0, simulate an "infinite" timeout
					http://www.sencha.com/forum/showthread.php?33694-Timeout-infinity -->
					<xsl:text>604800000</xsl:text>
					<!-- 1000 * 60 * 60 * 24 * 7 -->
				</xsl:when>
				<xsl:when test="/page/settings/express/ajaxRequestTimeout">
					<xsl:value-of select="/page/settings/express/ajaxRequestTimeout"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- 30 seconds seems like a reasonable default -->
					<xsl:text>30000</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>;</xsl:text>
		</script>
	</xsl:template>

	<xsl:template name="buildLoadingWindow">
		<div id="loadingWindow" style="display:none;">
			<xsl:element name="div">
				<xsl:attribute name="id">loadingMask</xsl:attribute>
				<xsl:text> </xsl:text>
			</xsl:element>
			<div id="loadingMessage" style="display:none;z-index:20000;position:fixed;">
				<img src="{$imageDir}loadingInd.gif"></img>
				<span id="loadingText">
					<xsl:value-of select="xslNsODExt:getDictRes('IsProcessingYourRequest')"/>
				</span>
				
			</div>
		</div>
	</xsl:template>
	<!-- stubs overridden in content pages -->
	<xsl:template name="buildHtmlTitle"/>
	<xsl:template name="buildMetaInformation"/>
	<!--*************************************************************************************************************
			Transform only the inner body to do a replace on the bodyContent div.
		************************************************************************************************************* -->
	<xsl:template name="createInnerBody">
		<xsl:param name="showActionBar"/>
		<xsl:param name="showHeader"/>
		<div id="bodyContent">
			<xsl:call-template name="addCSSClasses"/>
			<xsl:call-template name="createBodyContent">
				<xsl:with-param name="showActionBar" select="$showActionBar"/>
				<xsl:with-param name="showHeader" select="$showHeader"/>
			</xsl:call-template>

			<xsl:if test="/page/state/clientID != '0' and /page/content/getPage/body[@usePartyMappings = '1']">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="id">partyTools</xsl:with-param>
					<xsl:with-param name="onclick">DCT.Util.selectPartyData();</xsl:with-param>
					<xsl:with-param name="class">x-hidden-display</xsl:with-param>
					<xsl:with-param name="href">#</xsl:with-param>
					<xsl:with-param name="icon">application_view_list.png</xsl:with-param>
					<xsl:with-param name="qtip">Search</xsl:with-param>
					<xsl:with-param name="type">loneIcon</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</div>
	</xsl:template>
	<!-- Allows for customers to append extra classes to body tag -->
	<xsl:template name="addCustomCSSClasses">
	</xsl:template>
	<xsl:template name="addCSSClasses">
		<xsl:attribute name="class">
			<xsl:if test="$GlobalCultureCode != ''">
				<xsl:text> lang_</xsl:text>
				<xsl:value-of select="$GlobalCultureCode"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="normalize-space(/page/state/activeLOB) !=''">
				<xsl:value-of select="/page/state/activeLOB"/>
				<xsl:text>_lob </xsl:text>
			</xsl:if>
			<xsl:for-each select="/page/state/user/roles/Role">
				<xsl:text> role_</xsl:text>
				<xsl:value-of select="@name"/>
				<xsl:text> </xsl:text>
			</xsl:for-each>
			<xsl:choose>
				<xsl:when test="/page/content//errors">
					<xsl:text> errorsPage </xsl:text>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>dct_</xsl:text>
			<xsl:value-of select="$currentPage"/>
			<xsl:text> ruleset_</xsl:text>
			<xsl:value-of select="$ruleSet"/>
			<xsl:if test="/page/content/getPage/@page != ''">
				<xsl:text> </xsl:text>
				<xsl:value-of select="/page/content/getPage/@page"/>
				<xsl:text>_body</xsl:text>
			</xsl:if>
			<xsl:if test="/page/settings/Conversion/ExtJsControls/@convert='0'">
				<xsl:text> ext-unconverted</xsl:text>
			</xsl:if>
			<xsl:if test="$isPopupPage">
				<xsl:text> dct_popup</xsl:text>
			</xsl:if>
			<xsl:if test="normalize-space(/page/content/portals/portal[@active = '1']/@portalType)!=''">
				<xsl:text> dct_</xsl:text>
				<xsl:value-of select="/page/content/portals/portal[@active = '1']/@portalType"/>
			</xsl:if>
			<xsl:call-template name="addCustomCSSClasses"/>
		</xsl:attribute>
	</xsl:template>
	<xsl:template name="createBodyContent">
		<xsl:param name="showActionBar"/>
		<xsl:param name="showHeader"/>
		<xsl:if test="not(page/content/@allowBack = 1)">
			<script language="JavaScript" type="text/javascript">javascript:window.history.forward(1);</script>
		</xsl:if>
		<form name="mainform" id="mainForm" method="post">
			<xsl:attribute name="action">
				<xsl:text>default.aspx</xsl:text>
			</xsl:attribute>
			<!-- Pressing the enter key in a DCT PopupWindow causes a refresh that
				"confuses" Express (i.e. the skins render the popup as the main window).
				Thus, we're blocking that behavior for popups. Clicking buttons is the
				only acceptable way to exit a popup. -->
			<xsl:if test="$isPopupPage">
				<xsl:attribute name="onSubmit">
					<xsl:text>return false;</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<input type="hidden" value="{$isPopupPage}" id="_popUp" name="_popUp"/>
			<xsl:choose>
				<xsl:when test="page/content/semaphoreAction/@popup='1'">
					<xsl:call-template name="buildWaitLayout">
						<xsl:with-param name="waitType">PopUp</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="/page/content/semaphoreAction/@notify='working'">
							<xsl:call-template name="buildWaitLayout"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="createContainerArea">
								<xsl:with-param name="showActionBar" select="$showActionBar"/>
								<xsl:with-param name="showHeader" select="$showHeader"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</form>
	</xsl:template>

	<!--*************************************************************************************************************
			Build the form Content area.
		************************************************************************************************************* -->
	<xsl:template name="createContainerArea">
		<xsl:param name="showActionBar"/>
		<xsl:param name="showHeader"/>
		<div id="wrapper">
			<xsl:if test="not($isPopupPage)">
				<xsl:if test="$showHeader">
					<xsl:call-template name="buildHeader">
						<xsl:with-param name="showActionBar" select="$showActionBar"/>
						<xsl:with-param name="showHeader" select="$showHeader"/>
					</xsl:call-template>
				</xsl:if>
			</xsl:if>
			<div id="formContent">
				<xsl:if test="/page/content/getPage/body/@name">
					<xsl:attribute name="class">
						<xsl:value-of select="/page/content/getPage/body/@name"/>
					</xsl:attribute>
				</xsl:if>
				<script type="text/javascript">
					<xsl:call-template name="buildGlobalJsVariables"/>
				</script>
				<xsl:call-template name="buildOnReady"/>
				<xsl:call-template name="hiddenFields"/>
				<xsl:call-template name="createLeftPanel">
					<xsl:with-param name="showHeader" select="$showHeader"/>
					<xsl:with-param name="showActionBar" select="$showActionBar"/>
				</xsl:call-template>
				<xsl:call-template name="buildMain">
					<xsl:with-param name="showHeader" select="$showHeader"/>
				</xsl:call-template>
			</div>
			<div class="contentAreaBottom">
				<div class="cornerBottomLeftWhite"></div>
				<div class="cornerBottomRightWhite"></div>
			</div>
			<xsl:if test="not($isPopupPage)">
				<xsl:if test="$showHeader">
					<xsl:call-template name="buildFooter"/>
				</xsl:if>
			</xsl:if>
			<xsl:if test="$ShowDeveloperTools='1'">
				<div id="dctDeveloperToolbarContainer" class="autoFocusOff">
					<xsl:element name="div">
						<xsl:attribute name="class">developerToolbar</xsl:attribute>
						<xsl:attribute name="data-config">
							<xsl:text>{id: "dctDeveloperToolbar"}</xsl:text>
						</xsl:attribute>
					</xsl:element>
				</div>
			</xsl:if>
		</div>
	</xsl:template>
	<!--*************************************************************************************************************
	Build Header section
	************************************************************************************************************* -->
	<xsl:template name="buildHeader">
		<xsl:param name="showActionBar"/>
		<xsl:param name="showHeader"/>
		<div id="banner">
			<div id="logo">
				<xsl:text> </xsl:text>
			</div>
			<xsl:call-template name="buildProductNavigation"/>
			<xsl:if test="@Image='1'">
				<div id="bannerimage">
					<xsl:text> </xsl:text>
				</div>
			</xsl:if>
			<xsl:if test="$showActionBar and /page/actions/action[@id='Search'] or /page/content/portals/portal[@active = '1']/@name = 'Billing'">
				<xsl:call-template name="buildQuickSearch"/>
			</xsl:if>
			<xsl:if test="($showActionBar and $showHeader)">
				<xsl:call-template name="buildMainNavigation"/>
			</xsl:if>
		</div>
		<xsl:if test="($showActionBar and $showHeader) and not($isNewApplicationType)">
			<xsl:call-template name="buildActionsNavigation">
				<xsl:with-param name="location">actions</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="buildQuickSearch"></xsl:template>
	<xsl:template name="buildProductNavigation"></xsl:template>
	<xsl:template name="buildActionsNavigation"></xsl:template>
	<!--*************************************************************************************************************
		Build Footer section
		************************************************************************************************************* -->
	<xsl:template name="buildFooter">
		<div id="footer">
			<xsl:if test="/page/actions/action/@location='footer_one' or /page/actions/action/@location='logonOptions'">
				<div id="footer_one">
					<ul>
						<xsl:for-each select="/page/actions/action">
							<xsl:if test="@location='footer_one' or @location='logonOptions'">
								<xsl:if test="$isNewApplicationType or ((@page != 'batchProcess') or ((@page = 'batchProcess') and (($isSystemAdmin or $hasAdminSecurityRights) or ($isUnderwriter))))">
									<li id="{@id}">
										<xsl:if test="$currentPage=@page">
											<xsl:attribute name="class">active</xsl:attribute>
										</xsl:if>
										<xsl:variable name="href">
											<xsl:choose>
												<xsl:when test="@command != 'submitAction'">
													<xsl:text>DCT.Submit.setActiveParent('</xsl:text>
													<xsl:value-of select="@page"/>
													<xsl:text>');</xsl:text>
													<xsl:choose>
														<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
														<xsl:otherwise>
															<xsl:text>DCT.Submit.</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
													<xsl:value-of select="@command"/>
													<xsl:text>('</xsl:text>
													<xsl:value-of select="@page"/>
													<xsl:text>'</xsl:text>
													<xsl:if test="@post">
														<xsl:text>,'</xsl:text>
														<xsl:value-of select="@post"/>
														<xsl:text>'</xsl:text>
													</xsl:if>
													<xsl:text>);</xsl:text>
												</xsl:when>
												<xsl:otherwise>
													<xsl:choose>
														<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
														<xsl:otherwise>
															<xsl:text>DCT.Submit.</xsl:text>
														</xsl:otherwise>
													</xsl:choose>
													<xsl:value-of select="@command"/>
													<xsl:text>('</xsl:text>
													<xsl:value-of select="@post"/>
													<xsl:text>');</xsl:text>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:variable>

										<!--KDK - make sure this <A> is defined as a block element-->
										<a onclick="{$href}" id="footer_one_{@id}" class="bulletLink" href="javascript:;">
											<xsl:choose>
												<xsl:when test="@image and @image!=''">
													<img src="{$imageDir}{@image}"/>
												</xsl:when>
												<xsl:otherwise>
													<img src="{$imageDir}icons/bullet_white.png"/>
												</xsl:otherwise>
											</xsl:choose>
											<span class="mainActionsCaption">
												<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
											</span>
										</a>
									</li>
								</xsl:if>
							</xsl:if>
						</xsl:for-each>
					</ul>
					<xsl:if test="(/page/state/AllowMultiLanguageScreens = '1') and (/page/settings/Controls/MultipleLanguages/@enabled = '1')">
						<div id="footerLanguageComboContainerID">
							<div class="languageComboCaptionContainer">
								<div class="languageComboCaptionLabel">
									<label>
										<xsl:value-of select="xslNsODExt:getDictRes('SelectLanguage')"/>
									</label>
								</div>
							</div>
							<xsl:call-template name="buildLanguageCombo">
								<xsl:with-param name="imageComboContainerId">footerImageComboContainerId</xsl:with-param>
								<xsl:with-param name="imageComboInputId">footerImageComboInputId</xsl:with-param>
								<xsl:with-param name="imageComboHiddenIName">footerImageComboHidden</xsl:with-param>
								<xsl:with-param name="imageComboContainerClass">footerLanguageComboContainer autoFocusOff</xsl:with-param>
							</xsl:call-template>
						</div>
					</xsl:if>
				</div>
			</xsl:if>
			<div id="_bubbleDiv" class="bubble" style="display:none">
				<xsl:value-of select="xslNsODExt:getDictRes('Bubble')"/>
			</div>
			<xsl:call-template name="buildHtmlFooter"/>
		</div>
	</xsl:template>
	<!-- stub overridden for content pages -->
	<xsl:template name="buildHtmlFooter"/>

	<!--*************************************************************************************************************
	Build Error Notification section
	************************************************************************************************************* -->
	<xsl:template name="buildErrorNotification">
		<xsl:choose>
		<xsl:when test="/page/state/policyVersionMismatchOccured = 1">
			<div class="policyNotificationDiv nError">
				<div class="notificationIcon"/>
				<xsl:value-of select="xslNsODExt:getDictRes('UnableToSavePolicyModified')"/>
				<br/>
				<br/>
				<xsl:value-of select="xslNsODExt:getDictRes('UnableToSavePolicyModifiedNote')"/>
				<div>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.reloadCurrentPage();</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('Reload')"/>
						</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</xsl:when>
		<xsl:when test="/page/state/displayMessageInline = 1">
			<div class="policyNotificationDiv nError">
				<div class="notificationIcon"/>
				<div class="notificationMessage">
				<xsl:value-of select="xslNsExt:AntiXssHtmlEncode(page/notifyMessage)"/>
				</div>
			</div>
		</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--*************************************************************************************************************
	Build Main section
	************************************************************************************************************* -->
	<xsl:template name="buildMain">
		<xsl:param name="showHeader"/>
		<div>
			<xsl:if test="((/page/content/getPage/body/@ruleSet='topNav') and (/page/content/getPage/panel/@position ='leftpanel'))">
				<xsl:attribute name="class">
					<xsl:text>topmain</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="id">
				<xsl:text>main</xsl:text>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="$isInterviewPage">
					<xsl:for-each select="/page/content/getPage/panel[@position = 'firstheader']">
						<div class="{@class}">
							<xsl:attribute name="id">
								<xsl:value-of select="@position"/>
								<xsl:value-of select="group/@uid"/>
							</xsl:attribute>
							<xsl:apply-templates select="*"/>
						</div>
					</xsl:for-each>
					<xsl:call-template name="processPageHeader"/>
					<xsl:if test="not(/page/content//errors)">
						<xsl:if test="not($isNewApplicationType)">
							<xsl:call-template name="buildDiary">
								<xsl:with-param name="showHeader" select="$showHeader"/>
							</xsl:call-template>
						</xsl:if>
						<xsl:if test="not($isPopupPage)">
							<xsl:if test="not($isNewApplicationType)">
								<xsl:call-template name="buildTabs">
									<xsl:with-param name="cssClass">
										<xsl:choose>
											<xsl:when test="not($showHeader)">pageNavNoHeader</xsl:when>
											<xsl:otherwise>pageNav</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
								</xsl:call-template>
							</xsl:if>
						</xsl:if>
						<xsl:call-template name="buildPortfolioHeader">
							<xsl:with-param name="showHeader" select="$showHeader"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="$showHeader">
						<div id="header"></div>
						<xsl:call-template name="processPageHeader"/>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="$isInterviewPage">
					<!-- Build the header panels after the first header has been rendered -->
					<xsl:for-each select="/page/content/getPage/panel[@position = 'header']">
						<div class="{@class}">
							<xsl:attribute name="id">
								<xsl:text>inner</xsl:text>
								<xsl:value-of select="@position"/>
								<xsl:value-of select="group/@uid"/>
							</xsl:attribute>
							<xsl:apply-templates select="*"/>
						</div>
					</xsl:for-each>
					<div id="fields">
						<xsl:attribute name="class">
							<xsl:choose>
								<xsl:when test="/page/content/getPage/panel/@position='rightpanel'">
									<xsl:text>styleWithPanels</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="((/page/content/getPage/body/@ruleSet='topNav') and (/page/content/getPage/panel/@position ='leftpanel'))">
											<xsl:text>styleWithTopLeftPanels</xsl:text>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>styleWithOutPanels</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:choose>
							<xsl:when test="/page/content/semaphoreAction/@notify='working'">
								<xsl:call-template name="buildWaitLayout"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildPageContent"/>
							</xsl:otherwise>
						</xsl:choose>
					</div>
					<xsl:for-each select="/page/content/getPage/panel[@position = 'toprightpanel']">
						<div class="{@class}">
							<xsl:attribute name="id">
								<xsl:text>inner</xsl:text>
								<xsl:value-of select="@position"/>
								<xsl:value-of select="group/@uid"/>
							</xsl:attribute>
							<xsl:apply-templates select="*"/>
						</div>
					</xsl:for-each>
					<xsl:for-each select="/page/content/getPage/panel[@position = 'rightpanel']">
						<div class="{@class}">
							<xsl:attribute name="id">
								<xsl:text>inner</xsl:text>
								<xsl:value-of select="@position"/>
								<xsl:value-of select="group/@uid"/>
							</xsl:attribute>
							<xsl:attribute name="class">
								<xsl:text>inner</xsl:text>
								<xsl:value-of select="@position"/>
							</xsl:attribute>
							<xsl:apply-templates select="*"/>
						</div>
					</xsl:for-each>
					<xsl:for-each select="/page/content/getPage/panel[@position = 'footer']">
						<div class="{@class}">
							<xsl:attribute name="id">
								<xsl:text>inner</xsl:text>
								<xsl:value-of select="@position"/>
								<xsl:value-of select="group/@uid"/>
							</xsl:attribute>
							<xsl:apply-templates select="*"/>
						</div>
					</xsl:for-each>
				</xsl:when>
				<xsl:when test="$isEditPrintJobPage">
					<div id="fields">
						<xsl:choose>
							<xsl:when test="/page/content/semaphoreAction/@notify='working'">
								<xsl:call-template name="buildWaitLayout"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="buildPageContent"/>
							</xsl:otherwise>
						</xsl:choose>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="/page/content/semaphoreAction/@notify='working'">
							<xsl:call-template name="buildWaitLayout"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="buildPageContent"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<!-- stub overridden by content pages -->
	<xsl:template name="processPageHeader"/>
	<!--*************************************************************************************************************
		Hidden variables for processing 
	************************************************************************************************************* -->
	<xsl:template name="hiddenFields">
		<div id="hiddenFields">
			<input type="hidden" name="_sortColumn" id="_sortColumn" value="{page/content/ExpressCache/PrintJobInfo/@sortColumn}"/>
			<input type="hidden" name="_sortDirection" id="_sortDirection" value="{page/content/ExpressCache/PrintJobInfo/@sortDirection}"/>
			<input type="hidden" name="_targetPage" id="_targetPage"/>
			<input type="hidden" name="_submitAction" id="_submitAction"/>
			<input type="hidden" name="_action" id="_action"/>
			<input type="hidden" name="_pageSet" id="_pageSet"/>
			<input type="hidden" name="_fieldChanged" id="_fieldChanged" value="{page/state/dataChanged}"/>
			<input type="hidden" name="_autoSaveState" id="_autoSaveState" value="{page/state/@autoSaveState}"/>
			<input type="hidden" name="_stateID" id="_stateID" value="{page/state/@stateID}"/>
			<input type="hidden" name="_focusField" id="_focusField" value=""/>
			<input type="hidden" name="_prevFocusField" id="_prevFocusField" value=""/>
			<input type="hidden" name="_previewXSL" id="_previewXSL"/>
			<input type="hidden" name="_currentPageID" id="_currentPageID" value="{/page/content/getPage/@currentPageID}"/>
			<input type="hidden" name="_parentGroupIsPaged" id="_parentGroupIsPaged"/>
			<input type="hidden" name="_hiddenFormatNumMask" id="_hiddenFormatNumMask" value="{xslNsExt:numberFormatMaskToUse('', $manuscriptBaseCulture)}"/>
			<input type="hidden" name="_secure" id="_secure">
				<xsl:attribute name="value">
					<xsl:if test="page/state/@secure='1'">1</xsl:if>
				</xsl:attribute>
			</input>
			<input type="hidden" name="_hiddenFormatDtMask" id="_hiddenFormatDtMask" value="{xslNsExt:formatMaskToUse('', '', '0')}"/>
			<input type="hidden" name="_hiddenExtJsDateformat" id="_hiddenExtJsDateformat" value="{xslNsExt:formatMaskToUse('', '', '1')}"/>
			<input type="hidden" name="_hiddenExtJsDateTimeformat" id="_hiddenExtJsDateTimeformat" value="{xslNsExt:formatMaskToUse('', '', '3')}"/>
			<input type="hidden" name="_windowGuid" id="_windowGuid" value="{page/state/winGuid}"/>
			<input type="hidden" name="_newTabOpened" id="_newTabOpened" value="{page/state/newTabOpened}"/>
			<input type="hidden" name="_duplicateWindow" id="_duplicateWindow" value="0"/>
			<input type="hidden" name="_sectionStates" id="_sectionStates" value="{page/content/ExpressCache/sectionStates}"/>
			<!-- group paging -->
			<xsl:if test="count(/page/content/getPage//group[@startIndex]) &gt; 1">
				<input type="hidden" name="_groupPaging" id="_groupPaging">
					<xsl:attribute name="value">
						<xsl:text>&lt;paging&gt;</xsl:text>
						<xsl:for-each select="/page/content/getPage//group[@startIndex]">
							<xsl:text>&lt;group id=&quot;</xsl:text>
							<xsl:value-of select="@id"/>
							<xsl:text>&quot; startIndex=&quot;</xsl:text>
							<xsl:value-of select="@startIndex"/>
							<xsl:text>&quot; maxItems=&quot;</xsl:text>
							<xsl:value-of select="@maxItems"/>
							<xsl:text>&quot; index=&quot;</xsl:text>
							<xsl:value-of select="@index"/>
							<xsl:text>&quot;/&gt;</xsl:text>
						</xsl:for-each>
						<xsl:text>&lt;/paging&gt; </xsl:text>
					</xsl:attribute>
				</input>
			</xsl:if>

			<xsl:call-template name="buildCustomCommonHiddenFields"/>
		</div>
	</xsl:template>
	<xsl:template name="hiddenToolbarFields">
		<div id="hiddenToolbarFields">
			<input type="hidden" name="_enabledGroups" id="_enabledGroups">
				<xsl:attribute name="value">
					<xsl:for-each select="/page/settings/Controls/WorkbenchToolbar/ToolbarGroups/Group[@enabled='1']">
						<xsl:value-of select="@name"/>
						<xsl:if test="position()!=last()">
							<xsl:text>,</xsl:text>
						</xsl:if>
					</xsl:for-each>
				</xsl:attribute>
			</input>
		</div>
	</xsl:template>
	<xsl:template name="buildCustomCommonHiddenFields">
		<!-- nothing -->
	</xsl:template>
	<xsl:template name="buidPageHeader">
		<xsl:param name="pageTitle"/>
		<xsl:param name="pageInstruction"/>
		<div id="pageTop">
			<div id="pageTitle">
				<xsl:value-of select="$pageTitle"/>
			</div>
			<div id="pageInst">
				<xsl:copy-of select="$pageInstruction"/>
				<xsl:if test="$isInterviewPage and not(/page/content//errors)">
					<xsl:call-template name="buildInterviewHeader"/>
				</xsl:if>
			</div>
		</div>
		<xsl:call-template name="buildErrorNotification"/>
    <xsl:if test="$isInterviewPage and not(/page/content//errors)">
      <xsl:call-template name="notificationWidget"/>
    </xsl:if>
	</xsl:template>
	<!--*************************************************************************************************************
		Build and initialize the required global JavaScript variables.
	************************************************************************************************************* -->
	<xsl:template name="buildGlobalJsVariables">
		<xsl:text>DCT.sessionID = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/state/sessionID)"/>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.manuscriptID = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@manuscriptID)"/>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.topic = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/@topic)"/>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.page = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/@page)"/>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.spotlightConfidenceThreshold = </xsl:text>
		<xsl:if test="$SpotlightConfidenceThreshold = ''">
			<xsl:text>10</xsl:text>
		</xsl:if>
		<xsl:value-of select="$SpotlightConfidenceThreshold"/>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.spotlightEnabled = </xsl:text>
		<xsl:choose>
			<xsl:when test="$SpotlightEnabled = ''">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:when test="$SpotlightEnabled != '1'">
				<xsl:text>false</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>true</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.fieldValidationLogsEnabled = </xsl:text>
		<xsl:choose>
			<xsl:when test="$FieldValidationLogsEnabled = ''">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:when test="$FieldValidationLogsEnabled = '1'">
				<xsl:text>true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>;</xsl:text>
		<xsl:text>DCT.savePageSet = "</xsl:text>
		<xsl:if test="page/state/savePageSet != ''">
			<xsl:text>1</xsl:text>
		</xsl:if>
		<xsl:text>";</xsl:text>
		<xsl:text>DCT.imageDir = '</xsl:text>
		<xsl:value-of select="$imageDir"/>
		<xsl:text>';</xsl:text>
		<xsl:text>DCT.skinsDir = '</xsl:text>
		<xsl:value-of select="$SkinsDir"/>
		<xsl:text>';</xsl:text>
		<xsl:text>DCT.currentPage = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/content/@page)"/>
		<xsl:text>;</xsl:text>
		<xsl:text>Ext.Ajax.timeout = Math.max(</xsl:text>
		<xsl:value-of select="page/state/timeOut"/>
		<xsl:text>, 30000);</xsl:text>
		<xsl:choose>
			<xsl:when test="page/notifyMessage != '' and /page/state/displayMessageInline != 1">
				<xsl:choose>
					<xsl:when test="contains(page/notifyMessage,'|CLOSE')">
						<xsl:text>DCT.notifyMessage = </xsl:text>
						<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(substring-before(page/notifyMessage,'|CLOSE'))"/>
						<xsl:text>;</xsl:text>
						<xsl:text>DCT.closePopup = true;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>DCT.notifyMessage = </xsl:text>
						<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/notifyMessage)"/>
						<xsl:text>;</xsl:text>
						<xsl:text>DCT.closePopup = false;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>DCT.notifyMessage = "";</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="page/redirectToFormsLogin != ''">
				<xsl:text>DCT.redirectToFormsLogin = </xsl:text>
				<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/redirectToFormsLogin)"/>
				<xsl:text>;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>DCT.redirectToFormsLogin = "";</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="page/content/@page = 'login'">
			<xsl:text>DCT.Util.setFocusField("username");</xsl:text>
		</xsl:if>
		<xsl:if test="page/content/@focusField != ''">
			<xsl:text>DCT.Util.setFocusField(</xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/content/@focusField)"/>
			<xsl:text>);</xsl:text>
		</xsl:if>
		<xsl:text>DCT.clientName = </xsl:text>
		<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/state/clientName)"/>
		<xsl:text>;</xsl:text>

		<xsl:text>DCT.portals = </xsl:text>
		<xsl:text>[</xsl:text>
		<xsl:for-each select="page/content/portals/portal">
			<xsl:text>{</xsl:text>
			<xsl:for-each select="@*">
				<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(name())"/>
				<xsl:text>:</xsl:text>
				<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(.)"/>
				<xsl:if test="position() != last()">
					<xsl:text>,</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>}</xsl:text>
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>];</xsl:text>
		<!-- DCT properties only used for developer toolbar.  if it is turned off, dont assign the variables -->
		<xsl:if test="$ShowDeveloperTools='1'">
			<xsl:text>DCT.versionID = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@versionID)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.versionDate = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@versionDate)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.caption = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@caption)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.inherited = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@inherited)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.inheritedPage = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@inheritedPage)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.cultureCode = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/content/getPage/properties/@cultureCode)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.currentUser = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/state/user/name)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.versionServer = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/state/serverVersion)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.versionExpress = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/state/expressVersion)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.systemCulture = '</xsl:text>
			<xsl:value-of select="xslNsODExt:getCulture()"/>
			<xsl:text>';</xsl:text>
			<xsl:text>DCT.currentFullName = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(/page/state/user/fullName)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.activeAgency = </xsl:text>
			<xsl:value-of select="xslNsExt:AntiXssJavaScriptEncode(page/state/user/agencyName)"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.userRoles = "</xsl:text>
			<xsl:for-each select="page/state/user/roles/Role">
				<xsl:value-of select="@name"/>
				<xsl:if test="position() != last()">
					<xsl:text>|</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>";</xsl:text>
			<xsl:text>DCT.userContexts = "</xsl:text>
			<xsl:for-each select="page/state/user/contexts/context">
				<xsl:value-of select="@name"/>
				<xsl:if test="position() != last()">
					<xsl:text>|</xsl:text>
				</xsl:if>
			</xsl:for-each>
			<xsl:text>";</xsl:text>
			<xsl:text>DCT.skipRequiredHighlightOnBlur=</xsl:text>
			<xsl:value-of select="/page/settings/Controls/ValidationHighlight/@SkipRequiredOnBlur='1'"/>
			<xsl:text>;</xsl:text>
			<xsl:text>DCT.IsReadOnly = </xsl:text>
			<xsl:value-of select="$IsReadOnly"/>
			<xsl:text>;</xsl:text>
		</xsl:if>
		<!-- [kellyke Nov 15, 10] todo: rather than making all of these variables used by the dev toolbar, 
		use an xmlreader to read the debug content file and when the current info window is shown.  -->
		<xsl:call-template name="buildCustomJsVariables"/>
	</xsl:template>

	<xsl:template name="buildCustomJsVariables">
		<!-- [kellyke Dec 8, 10] this template is to be left empty in the base product, and to serve as an easily overridable
			template for building custom JS vars specific to individual implementations. Usage:  copy this template into
			your custom skin folder's common/dctContainer.xsl and modify it there. -->
	</xsl:template>
	<!--*************************************************************************************************************
		Initialize the majority of Ext.OnReady commands.
	************************************************************************************************************* -->
	<xsl:template name="buildOnReady">
		<script type="text/javascript" defer="defer">
			<xsl:text>Ext.onReady( function() {	</xsl:text>
			<xsl:text>DCT.Util.processOnLoad();</xsl:text>
			<xsl:choose>
				<xsl:when test="not($isInterviewPage)">
					<xsl:call-template name="buildNonInterviewOnReady"/>
				</xsl:when>
				<xsl:when test="$isInterviewPage and not(/page/content/errors)">
					<xsl:call-template name="buildInterviewOnReady"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- Non-Interview OnReady  Initialization -->
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>});</xsl:text>
		</script>
	</xsl:template>
	<xsl:template name="buildInterviewOnReady"></xsl:template>

	<!--*************************************************************************************************************
		Interview Header 
		************************************************************************************************************* -->
	<xsl:template name="buildInterviewHeader">
		<xsl:if test="/page/content/getPage/header//fieldInstance">
			<span id="headerInfo">
				<xsl:apply-templates select="/page/content/getPage/header"/>
			</span>
		</xsl:if>
	</xsl:template>
	<!--*************************************************************************************************************
		Build Main Navigation links 
		************************************************************************************************************* -->
	<xsl:template name="buildMainNavigation">
		<div id="actiongroup">
			<ul id="mainactions">
				<xsl:for-each select="/page/actions/action[@location='header']">
					<xsl:if test="$isNewApplicationType or ((@page != 'batchProcess') or ((@page = 'batchProcess') and (($isSystemAdmin or $hasAdminSecurityRights) or ($isUnderwriter))))">
						<li id="{@id}">
							<xsl:if test="$currentPage=@page">
								<xsl:attribute name="class">active</xsl:attribute>
							</xsl:if>
							<xsl:variable name="href">
								<xsl:choose>
									<xsl:when test="@command != 'submitAction'">
										<xsl:text>DCT.Submit.setActiveParent('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>');</xsl:text>
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>'</xsl:text>
										<xsl:if test="@post">
											<xsl:text>,'</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>'</xsl:text>
										</xsl:if>
										<xsl:text>);</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@post"/>
										<xsl:text>');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:variable name="nameText">
								<xsl:text>name_</xsl:text>
								<xsl:value-of select="@id"/>
							</xsl:variable>
							<xsl:variable name="idText">
								<xsl:text>id_</xsl:text>
								<xsl:value-of select="@id"/>
							</xsl:variable>

							<!--KDK - make sure this <A> is defined as a block element-->
							<a onclick="{$href}" name="{$nameText}" id="{$idText}" class="bulletLink" href="javascript:;">
								<xsl:if test="@image and @image!=''">
									<img src="{$imageDir}{@image}"/>
								</xsl:if>
								<span class="mainActionsCaption">
									<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
								</span>
							</a>
						</li>
					</xsl:if>
				</xsl:for-each>
			</ul>
		</div>
		<div id="specialactions">
			<xsl:for-each select="/page/actions/action[@location='special']">
				<xsl:variable name="href">
					<xsl:choose>
						<xsl:when test="@command = 'submitPagePopup'">
							<xsl:choose>
								<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT.Submit.</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="@command"/>
							<xsl:text>('</xsl:text>
							<xsl:value-of select="@page"/>
							<xsl:text>',</xsl:text>
							<xsl:choose>
								<xsl:when test="@post">
									<xsl:text>'</xsl:text>
									<xsl:value-of select="@post"/>
									<xsl:text>'</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>null</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>, </xsl:text>
							<xsl:choose>
								<xsl:when test="@width">
									<xsl:value-of select="@width"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>null</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>, </xsl:text>
							<xsl:choose>
								<xsl:when test="@height">
									<xsl:value-of select="@height"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>null</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>,true);</xsl:text>
						</xsl:when>
						<xsl:when test="@command != 'submitAction'">
							<xsl:choose>
								<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT.Submit.</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="@command"/>
							<xsl:text>('</xsl:text>
							<xsl:value-of select="@page"/>
							<xsl:text>'</xsl:text>
							<xsl:if test="@post">
								<xsl:text>,'</xsl:text>
								<xsl:value-of select="@post"/>
								<xsl:text>'</xsl:text>
							</xsl:if>
							<xsl:text>);</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
								<xsl:otherwise>
									<xsl:text>DCT.Submit.</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:value-of select="@command"/>
							<xsl:text>('</xsl:text>
							<xsl:value-of select="@post"/>
							<xsl:text>');</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="nameText">
					<xsl:text>name_</xsl:text>
					<xsl:value-of select="@id"/>
				</xsl:variable>
				<xsl:variable name="idText">
					<xsl:text>id_</xsl:text>
					<xsl:value-of select="@id"/>
				</xsl:variable>

				<!--KDK - make sure this <A> is defined as a block element-->
				<a onclick="{$href}" name="{$nameText}" id="{$idText}" href="javascript:;">
					<div class="specialActionLeft"></div>
					<div class="specialActionMiddle">
						<xsl:if test="@image and @image!=''">
							<img src="{$imageDir}{@image}"/>
						</xsl:if>
						<span class="specialActionsCaption">
							<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
						</span>
					</div>
					<div class="specialActionRight"></div>
				</a>
			</xsl:for-each>
		</div>
		<div id="contextactions">
			<xsl:for-each select="/page/actions/action[@location='context']">
			<xsl:variable name="canView360" select="(/page/state/user/privileges/privilege/@name = 'CanView360') 
				and ((/page/content/portals/portal[@active = '1']/@portalType = 'pas' and (number(/page/state/quoteID) &gt; 0)) 
						or (/page/content/portals/portal[@active = '1']/@portalType = 'bil' and (number(/page/content/ExpressCache/AccountId) &gt; 0)))
				and (/page/state/EnableParty360 = '1')"/>
			<xsl:if test="$canView360">
				<xsl:variable name="action">
					<xsl:text>'partyStart:party360'</xsl:text>
				</xsl:variable>					
				<xsl:variable name="objectData">
					<xsl:choose>
						<xsl:when test="/page/content/portals/portal[@active = '1']/@portalType = 'pas'">
							<xsl:text>","_objectId360:</xsl:text>
							<xsl:value-of select="/page/state/quoteID"/>
							<xsl:text>","_objectTypeCode360:</xsl:text>
							<xsl:text>POL</xsl:text>
							<xsl:text>","_objectReference360:</xsl:text>
							<xsl:value-of select="/page/state/policyNumber"/>
						</xsl:when>
						<xsl:when test="/page/content/portals/portal[@active = '1']/@portalType = 'bil'">
							<xsl:text>","_objectId360:</xsl:text>
							<xsl:value-of select="/page/content/ExpressCache/AccountId"/>
							<xsl:text>","_objectTypeCode360:</xsl:text>
							<xsl:text>ACT</xsl:text>
							<xsl:text>","_objectReference360:</xsl:text>
							<xsl:value-of select="/page/content/ExpressCache/AccountReference"/>
						</xsl:when>							
					</xsl:choose>
				</xsl:variable>	
				<xsl:variable name="partyContextJson">
					<xsl:text>'["_partyReturnStructure:none","_partyReturnAction:none360</xsl:text>
						<xsl:value-of select="$objectData"/>
					<xsl:text>"]'</xsl:text> 
				</xsl:variable>					
				<xsl:variable name="href">
					<xsl:text>DCT.Util.popUpPartyCommon(</xsl:text>
					<xsl:value-of select="$action"/>
					<xsl:text>,</xsl:text>						
					<xsl:value-of select="$partyContextJson"/>						
					<xsl:text>);</xsl:text>						
				</xsl:variable>	
				<a onclick="{$href}" name="name_Party360" id="id_Party360" href="javascript:;">
					<div class="contextActionLeft"></div>
					<div class="contextActionMiddle">
							<span class="contextActionsCaption">
								<xsl:value-of select="xslNsODExt:getDictRes(@caption)"/>
						</span>
					</div>
					<div class="contextActionRight"></div>
				</a>		
			</xsl:if>
			</xsl:for-each>
		</div>		
	</xsl:template>
	<!-- stubs to be overriden -->
	<xsl:template name="buildPageContent"/>

	<!--************************************************************************************************************
		This template is for processing the actions that exist at the dialog level where type=page for the actions
		************************************************************************************************************* -->
	<xsl:template name="buildTabs">
		<xsl:param name="cssClass">pageNav</xsl:param>
		<xsl:param name="id">pageNav</xsl:param>
		<xsl:variable name="maxTabsPerRow">
			<xsl:value-of select="/page/content/getPage/panel/@maxTabsPerRow"/>
		</xsl:variable>
		<xsl:variable name="tabCount" select="count(/page/content/getPage/actions[@type='page']/action[@hidden='0'])"></xsl:variable>
		<xsl:variable name="numOfTabRows" select="ceiling(number($tabCount) div number($maxTabsPerRow))"></xsl:variable>
		<xsl:choose>
			<xsl:when test="($maxTabsPerRow != '') and ($numOfTabRows &gt; 1)">
				<xsl:call-template name="buildMultipleRowTab">
					<xsl:with-param name="maxTabsPerRow" select="$maxTabsPerRow"/>
					<xsl:with-param name="cssClass" select="$cssClass"/>
					<xsl:with-param name="id" select="$id"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildSingleRowTab">
					<xsl:with-param name="cssClass" select="$cssClass"/>
					<xsl:with-param name="id" select="$id"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--************************************************************************************************************
		This template is for processing a single row tab.
	************************************************************************************************************* -->
	<xsl:template name="buildSingleRowTab">
		<xsl:param name="cssClass">pageNav</xsl:param>
		<xsl:param name="id">pageNav</xsl:param>
		<div class="{$cssClass}" id="{$id}">
			<ul>
				<xsl:call-template name="buildTabActions"/>
			</ul>
		</div>
	</xsl:template>
	<!--************************************************************************************************************
		This template is for processing a multiple row tab.  Each row is divided by the maxTabsPerRow attribute.
	************************************************************************************************************* -->
	<xsl:template name="buildMultipleRowTab">
		<xsl:param name="maxTabsPerRow"/>
		<xsl:param name="cssClass">pageNav</xsl:param>
		<xsl:param name="id">pageNav</xsl:param>
		<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@hidden='0']">
			<xsl:variable name="currentIndex" select="position()"/>
			<xsl:variable name="previousRow" select="ceiling(($currentIndex - 1) div $maxTabsPerRow) - 1"/>
			<xsl:variable name="currentRow" select="ceiling($currentIndex div $maxTabsPerRow) - 1"/>
			<xsl:variable name="rowChanged" select="not($previousRow = $currentRow)"/>
			<xsl:variable name="cssClassRow">
				<xsl:value-of select="$cssClass"/>
				<xsl:if test="$rowChanged">
					<xsl:if test="$currentRow &gt; 0">
						<xsl:value-of select="$currentRow"/>
					</xsl:if>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="idRow">
				<xsl:value-of select="$id"/>
				<xsl:if test="$rowChanged">
					<xsl:if test="$currentRow &gt; 0">
						<xsl:value-of select="$currentRow"/>
					</xsl:if>
				</xsl:if>
			</xsl:variable>
			<xsl:if test="$rowChanged">
				<div class="{$cssClassRow}" id="{$idRow}">
					<ul>
						<xsl:if test="$currentRow &gt; 0">
							<xsl:attribute name="class">subAction</xsl:attribute>
						</xsl:if>
						<xsl:call-template name="buildTabActions">
							<xsl:with-param name="currentRow" select="$currentRow"/>
							<xsl:with-param name="maxTabsPerRow" select="$maxTabsPerRow"/>
						</xsl:call-template>
					</ul>
				</div>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--************************************************************************************************************
		This template is for processing each tab action.  To support the maxTabsPerRow option, it must iterate
		through multipe times making sure it belongs on the current row.
	************************************************************************************************************* -->
	<xsl:template name="buildTabActions">
		<xsl:param name="currentRow"/>
		<xsl:param name="maxTabsPerRow"/>
		<!--  SECTION ADDED BY JOEL FOR QUOTE SUMMARY IN LEFT HAND PANEL-->
		<!-- Localization TODO: This is going to break once ManuScripts begin supporting Localization of their captions -->
		<xsl:for-each select="/page/content/getPage/panel[@position = 'righttpanel'][@caption = 'Quote Summary']">
			<div class="QuoteSummaryLeftPanel" id="QuoteSummaryLeftPanel">
				<xsl:apply-templates select="*"/>
			</div>
		</xsl:for-each>
		<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@hidden='0']">
			<xsl:variable name="currentIndex" select="position()"/>
			<xsl:variable name="itemsRow" select="ceiling($currentIndex div $maxTabsPerRow) - 1"/>
			<!-- Determine if the item belongs on the current row.  This is done by checking if the item's calculated row is equal to the current actual row. -->
			<xsl:variable name="displayOnRow">
				<xsl:choose>
					<xsl:when test="$maxTabsPerRow != '' and $currentRow != ''">
						<xsl:value-of select="$itemsRow = $currentRow"/>
					</xsl:when>
					<xsl:otherwise>true</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="currentPos">
				<xsl:value-of select="/page/content/getPage/actions/action[@command='current']/@position + 1"/>
			</xsl:variable>
			<!-- Only display tabbed item if it belongs on the row.  The currentRow is equal to its calculated row location. -->
			<xsl:if test="$displayOnRow = 'true'">
				<xsl:if test="@enabled='1' or (@enabled='0' and actions) or @partialRefresh='1'">
					<xsl:variable name="tabClass">
						<xsl:choose>
							<xsl:when test="not(@command) or @command!='current'">
								<xsl:text>inactiveTab</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>activeTab</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<li>
						<xsl:attribute name="class">
							<xsl:if test="$currentIndex&gt;$currentPos">
								<xsl:text disable-output-escaping="yes">followingLink </xsl:text>
							</xsl:if>
							<xsl:value-of select="$tabClass"/>
							<xsl:if test="@navClass!=''">
								<xsl:text> </xsl:text>
								<xsl:value-of select="$tabClass"/>
								<xsl:value-of select="@navClass"/>
								<xsl:text> </xsl:text>
								<xsl:value-of select="@navClass"/>
							</xsl:if>
						</xsl:attribute>
						<!-- AJAX START -->
						<xsl:if test="@partialRefresh = 1">
							<xsl:if test="@showConditionValue = 'False'">
								<xsl:attribute name="style">display:none;</xsl:attribute>
							</xsl:if>
							<xsl:attribute name="objectRef">
								<xsl:value-of select="@showConditionObjectRef"/>
							</xsl:attribute>
							<xsl:attribute name="fieldRef">
								<xsl:value-of select="@showConditionFieldRef"/>
							</xsl:attribute>
							<xsl:attribute name="onShowHide">showHide</xsl:attribute>
						</xsl:if>
						<!-- AJAX END -->
						<xsl:choose>
							<xsl:when test="@enabled=0 and not(@partialRefresh='1')">
								<div id="disabledAction">
									<xsl:value-of select="@caption"/>
								</div>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="@command='current'">
									<input type="hidden" name="_currentAction" id="_currentAction">
										<xsl:attribute name="value">
											<xsl:value-of select="@uid"/>
										</xsl:attribute>
									</input>
								</xsl:if>
								<xsl:call-template name="buildNavLink">
									<xsl:with-param name="ajaxActionName">
										<xsl:value-of select="@uid"/>
									</xsl:with-param>
									<xsl:with-param name="actionName">
										<xsl:value-of select="@id"/>
									</xsl:with-param>
									<xsl:with-param name="value" select="@caption"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
						<!-- SECTION ADDED BY JOEL FOR SUB MENUS ON LEFT HAND PANEL-->
						<xsl:variable name="caption">
							<xsl:value-of select="@caption"/>
						</xsl:variable>
						<xsl:for-each select="/page/content/getPage/panel[@position = 'submenupanel'][@caption = $caption]">
							<xsl:variable name="cssClass">
								<xsl:value-of select="translate($caption, '() ', '')"/>
								<xsl:text>SubMenu</xsl:text>
							</xsl:variable>
							<div>
								<xsl:attribute name="class">
									<xsl:value-of select="$cssClass"></xsl:value-of>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:text>SubMenu</xsl:text>
									<xsl:value-of select="group/@uid"/>								
								</xsl:attribute>
								<xsl:apply-templates select="*"/>
							</div>
						</xsl:for-each>
						<!-- END SECTION ADDED BY JOEL FOR SUB MENUS ON LEFT HAND PANEL-->
					</li>
				</xsl:if>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<!--************************************************************************************************************
	This template is for setting the waiting message for asynchronous actions. 
	************************************************************************************************************* -->
	<xsl:template name="buildWaitLayout">
		<xsl:param name="waitType"/>
		<xsl:variable name="submitPage">
			<xsl:value-of select="page/content/semaphoreAction/@submitPage"/>
		</xsl:variable>
		<xsl:variable name="showButton">
			<xsl:value-of select="page/content/semaphoreAction/@showButton"/>
		</xsl:variable>
		<xsl:call-template name="hiddenFields"/>
		<script language="javascript" defer="defer">
			<xsl:text>DCT.LoadingMessage.polling = true;DCT.LoadingMessage.show();</xsl:text>
		</script>
		<script language="javascript" defer="defer">
			<xsl:text>DCT.LoadingMessage.drawDots();</xsl:text>
		</script>
		<input type="hidden" name="_downloadMode">
			<xsl:attribute name="value">
				<xsl:value-of select="page/content/printPreviewDownloadMode/@mode"/>
			</xsl:attribute>
		</input>
		<input type="hidden" name="_semaphoreID">
			<xsl:attribute name="value">
				<xsl:value-of select="page/content/semaphoreAction/@semaphoreID"/>
			</xsl:attribute>
		</input>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for	building the anchor links or the button links (from actions) that
		will be used to navigate from page to page, add/delete items or to popup help, etc.
		************************************************************************************************************* -->
	<xsl:template name="buildNavLink">
		<xsl:param name="actionName"/>
		<xsl:param name="ajaxActionName"/>
		<xsl:param name="value"/>
		<xsl:param name="cssClass"/>
		<xsl:param name="runInterviewActionAsync" />
		<xsl:param name="runInterviewActionAsyncAuto" />
		<xsl:variable name="samePage">
			<!-- Check path -->
			<xsl:choose>
				<xsl:when test="(current()/server/requests/ManuScript.getPageRq/@page=/page/content/getPage/@page) and (current()/server/requests/ManuScript.getPageRq/@topic=/page/content/getPage/@topic)">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="javaCode">
			<xsl:text>DCT.ajaxProcess.submitAjax('PageContainer','</xsl:text>
			<xsl:value-of select="$ajaxActionName"/>
			<xsl:text>','0','0','','','');</xsl:text>
		</xsl:variable>
		<xsl:variable name="runAsync">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@async"/>
		</xsl:variable>
		<xsl:variable name="runAsyncAutomatic">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncAutomatic"/>
		</xsl:variable>
		<xsl:variable name="runActionAsyncObjectId">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncObjectID"/>
		</xsl:variable>
		<xsl:variable name="runActionAsyncObjectType">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncObjectType"/>
		</xsl:variable>
		<xsl:variable name="runActionAsyncAllowWait">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncAllowWait"/>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$runAsync='2' or $runAsync='3'">
				<xsl:choose>
					<xsl:when test="$runAsyncAutomatic='' or $runAsyncAutomatic='0'">
						<!-- Show split link navigation-->
						<span class="splitNavLinkSpan">
							<a href="javascript:;" class="splitNavLinkNormal" onmousemove="window.status='';" onmouseout="window.status='';">
								<xsl:attribute name="id">
									<xsl:value-of select="substring-after($actionName,'_')"/>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:value-of select="$javaCode"/>
								</xsl:attribute>
								<xsl:if test="$cssClass">
									<xsl:attribute name="class">
										<xsl:value-of select="$cssClass"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:value-of select="@caption"/>
							</a>
							<a href="javascript:;" onmousemove="window.status='';" onmouseout="window.status='';" class="clockIcon splitNavLinkGlyph">
								<xsl:attribute name="id">
									<xsl:value-of select="substring-after($actionName,'_')"/>
								</xsl:attribute>
								<xsl:attribute name="onclick">
								 <xsl:text>DCT.Util.setInterviewActionAsync();</xsl:text>
									<xsl:text>DCT.Util.processInterviewActionAsync('</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectType"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectId"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncAllowWait"/>
									<xsl:text>');</xsl:text>
									<xsl:value-of select="$javaCode"/>
								</xsl:attribute>
								<xsl:if test="$cssClass">
									<xsl:attribute name="class">
										<xsl:value-of select="$cssClass"/>
									</xsl:attribute>
								</xsl:if>
							</a>
						</span>
					</xsl:when>
					<xsl:otherwise>
						<!-- Show single link async navigation-->
						<a href="javascript:;" onmousemove="window.status='';" onmouseout="window.status='';">
							<xsl:attribute name="id">
								<xsl:value-of select="substring-after($actionName,'_')"/>
							</xsl:attribute>
							<xsl:attribute name="onclick">
								<xsl:text>DCT.Util.setInterviewActionAsync();</xsl:text>
								<xsl:text>DCT.Util.processInterviewActionAsync('</xsl:text>
								<xsl:value-of select="$runActionAsyncObjectType"/>
								<xsl:text>','</xsl:text>
								<xsl:value-of select="$runActionAsyncObjectId"/>
								<xsl:text>','</xsl:text>
								<xsl:value-of select="$runActionAsyncAllowWait"/>
								<xsl:text>');</xsl:text>
									<xsl:value-of select="$javaCode"/>
							</xsl:attribute>
							<xsl:if test="$cssClass">
								<xsl:attribute name="class">
									<xsl:value-of select="$cssClass"/>
								</xsl:attribute>
							</xsl:if>
							<xsl:value-of select="@caption"/>
							<clock class="clockIconActive asyncNavLinkGlyph" />
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Show normal navigation link-->
				<a href="javascript:;" onmousemove="window.status='';" onmouseout="window.status='';">
					<xsl:attribute name="id">
						<xsl:value-of select="substring-after($actionName,'_')"/>
					</xsl:attribute>
					<xsl:attribute name="onclick">
						<xsl:value-of select="$javaCode"/>
					</xsl:attribute>
					<xsl:if test="$cssClass">
						<xsl:attribute name="class">
							<xsl:value-of select="$cssClass"/>
						</xsl:attribute>
					</xsl:if>
					<xsl:value-of select="@caption"/>
				</a>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
	<!--	*************************************************************************************************************
	This template is for building the header that appears on the portfolio pages.
	************************************************************************************************************* -->
	<xsl:template name="buildPortfolioHeader">
		<xsl:param name="showHeader"/>
		<xsl:call-template name="buildPortfolioButtons">
			<xsl:with-param name="showHeader" select="$showHeader"/>
			<xsl:with-param name="showClientLink" select="/page/state/usingPortfolio=1"/>
			<xsl:with-param name="showPortfolioLink" select="/page/state/portfolioID&gt;0 and /page/state/usingPortfolio=0"/>
		</xsl:call-template>
		<xsl:if test="/page/state/portfolioID&gt;0">
			<xsl:call-template name="buildSharedDataDivs"/>
			<xsl:call-template name="buildPortfolioHelp"/>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
	This template is for building the action buttons that appear on the portfolio buttons.
	************************************************************************************************************* -->
	<xsl:template name="buildPortfolioButtons">
		<xsl:param name="showHeader"/>
		<xsl:param name="showClientLink"/>
		<xsl:param name="showPortfolioLink"/>
		<xsl:if test="$showHeader">
			<xsl:if test="$showClientLink or $showPortfolioLink">
				<div id="divPortfolioButtons">
					<xsl:if test="$showClientLink">
						<a id="aClientView" onclick="DCT.Submit.gotoPageForClient('clientInfo', '{/page/state/clientID}')" href="javascript:;">
							<xsl:value-of select="xslNsODExt:getDictRes('ClientInformationPage')"/>
						</a>
					</xsl:if>
					<xsl:if test="$showPortfolioLink">
						<a id="aPortfolioView" onclick="DCT.Submit.loadPortfolio('{/page/state/portfolioID}')" href="javascript:;">
							<xsl:value-of select="xslNsODExt:getDictRes('PortfolioView')"/>
						</a>
					</xsl:if>
				</div>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
	This template is for building the shared data differences on the quote page within a portfolio.
	************************************************************************************************************* -->
	<xsl:template name="buildSharedDataDivs">
		<div id="divSharedData">
			<xsl:call-template name="sharedDataChanges"/>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		This template is for	building radio buttons from an option list
	************************************************************************************************************* -->
	<xsl:template name="sharedDataChanges">
		<xsl:for-each select="/page/content/getPage/sharedDataChanges/sharedField">
			<div id="divSharedField{@sharedID}" style="visibility:hidden;display:none;">
				<div>
					<h3>
						<xsl:value-of select="xslNsODExt:getDictRes('PolicyValue')"/>
					</h3>
					<xsl:if test="@fieldInstanceValue=''">
						<xsl:value-of select="xslNsODExt:getDictRes('NoValue')"/>
					</xsl:if>
					<xsl:value-of select="@fieldInstanceValue"/>
				</div>
				<br/>
				<div>
					<h3>
						<xsl:value-of select="xslNsODExt:getDictRes('SharedDataValue')"/>
					</h3>
					<xsl:if test="@sharedDataValue=''">
						<xsl:value-of select="xslNsODExt:getDictRes('NoValue')"/>
					</xsl:if>
					<xsl:value-of select="@sharedDataValue"/>
				</div>
				<div class="sharedDataValue" style="visibility:hidden;display:none;">
					<xsl:value-of select="@sharedDataValue"/>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="buildPortfolioHelp">
		<xsl:if test="count(/page/content/getPage/sharedDataChanges/sharedField)&gt;0">
			<div id="divSharedDataHelp">
				<img src="{$imageDir}warning.gif" alt="{xslNsODExt:getDictRes('PortfolioDifferencesHelp')}" title="{xslNsODExt:getDictRes('PortfolioDifferencesHelp')}">
					<xsl:attribute name="onclick">
						<xsl:text>DCT.Util.showPopupMessage(DCT.T('PortfolioInstructions'));</xsl:text>
					</xsl:attribute>
				</img>
				<xsl:value-of select="xslNsODExt:getDictRes('PolicyInfoDiffersFromPortfolio')"/>
				<xsl:text> </xsl:text>
				<a href="javascript:;" class="AnchorWithUnderline">
					<xsl:attribute name="onclick">
						<xsl:text>DCT.Util.showPopupMessage(DCT.T('PortfolioInstructions'));</xsl:text>
					</xsl:attribute>
					<span class="AnchorWithUnderline">
						<xsl:value-of select="xslNsODExt:getDictRes('Instructions')"/>
					</span>
				</a>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template name="createLeftPanel">
		<xsl:param name="showHeader"/>
		<xsl:param name="showActionBar"/>
		<xsl:if test="$isNewApplicationType">
			<div id="leftPanelArea">
				<div id="leftPanelAreaBody">
					<xsl:call-template name="buildLeftPanelContent">
						<xsl:with-param name="showHeader" select="$showHeader"/>
						<xsl:with-param name="showActionBar" select="$showActionBar"/>
					</xsl:call-template>
					<xsl:choose>
						<xsl:when test="not((/page/content/getPage/body/@ruleSet='topNav') and (/page/content/getPage/panel/@position ='leftpanel'))">
							<xsl:call-template name="buildExtendedLeftNav"/>
						</xsl:when>
					</xsl:choose>
					<xsl:call-template name="buildExternalModules">
						<xsl:with-param name="location">leftPanel</xsl:with-param>
					</xsl:call-template>
				</div>
			</div>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="((/page/content/getPage/body/@ruleSet='topNav') and (/page/content/getPage/panel/@position ='leftpanel'))">
				<div id="outerLeftPanelArea">
					<xsl:call-template name="buildExtendedLeftNav"/>
				</div>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="buildExternalModules">
		<xsl:param name="location"/>
		<xsl:for-each select="/page/settings/StaticPanelContent[@location=$location]/ExternalModule">

			<xsl:choose>
				<xsl:when test="@cc!=''">
					<xsl:element name="div">
						<xsl:attribute name="class">
							<xsl:text>cc-bubble </xsl:text>
							<xsl:text>cc_</xsl:text>
							<xsl:value-of select="@cc"/>
						</xsl:attribute>
						<div class="cc-bubble-tl">
							<div class="cc-bubble-tr">
								<div class="cc-bubble-tc">
									<xsl:if test="@panelHeader!=''">
										<div>
											<xsl:attribute name="class">
												<xsl:text>cc-bubble-title </xsl:text>
												<xsl:choose>
													<xsl:when test="@alignHeader='center'">x_TextAlignCenter</xsl:when>
													<xsl:when test="@alignHeader='right'">x_TextAlignRight</xsl:when>
												</xsl:choose>
											</xsl:attribute>
											<span>
												<xsl:value-of select="@panelHeader"/>
											</span>
										</div>
									</xsl:if>
								</div>
							</div>
						</div>
						<div class="cc-bubble-bwrap">
							<div class="cc-bubble-ml">
								<div class="cc-bubble-mr">
									<div class="cc-bubble-mc">
										<div class="cc-bubble-body cc_{@cc}_body">
											<xsl:call-template name="buildExternalModulePanel">
												<xsl:with-param name="class">externalModule-leftPanel</xsl:with-param>
												<xsl:with-param name="type" select="@staticContentType"></xsl:with-param>
											</xsl:call-template>
										</div>
									</div>
								</div>
							</div>
							<div class="cc-bubble-bl">
								<div class="cc-bubble-br">
									<div class="cc-bubble-bc">
									</div>
								</div>
							</div>
						</div>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildExternalModulePanel">
						<xsl:with-param name="class">externalModule-leftPanel</xsl:with-param>
						<xsl:with-param name="type" select="@staticContentType"></xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="buildLeftPanelContent"></xsl:template>
	<xsl:template name="buildExtendedLeftNav">
		<xsl:if test="(not($isPopupPage)) or (($isPopupPage) and (/page/content/getPage/panel[@position = 'leftpanelpopup']))">
			<xsl:for-each select="/page/actions/action[@location='extendedLeftNav']">
				<xsl:choose>
					<xsl:when test="@command='createConsumerQuote'">
						<div id="consumerNewQuoteWidget">
							<div id="consumerNewQuoteWidgetTitle">
								<xsl:value-of select="xslNsODExt:getDictRes('StartNewQuote')"/>
							</div>
							<div id="consumerQuoteType">
								<xsl:value-of select="xslNsODExt:getDictRes('QuoteType')"/>
							</div>
							<xsl:element name="input">
								<xsl:attribute name="type">hidden</xsl:attribute>
								<xsl:attribute name="name">_clientID</xsl:attribute>
								<xsl:attribute name="value">
									<xsl:value-of select="/page/state/clientID"/>
								</xsl:attribute>
							</xsl:element>
							<div id="consumerNewLOB">
								<xsl:call-template name="buildSystemControl">
									<xsl:with-param name="id">manuscriptLOB</xsl:with-param>
									<xsl:with-param name="name">_manuscriptLOB</xsl:with-param>
									<xsl:with-param name="type">select</xsl:with-param>
									<xsl:with-param name="optionlist">
										<xsl:for-each select="/page/content/ExpressCache/Consumer.listProductsRs/items/item">
											<xsl:element name="option">
												<xsl:attribute name="value">
													<xsl:value-of select="@id"/>
													<xsl:text>|</xsl:text>
													<xsl:value-of select="keys/key[@name='lob']/@value"/>
												</xsl:attribute>
												<xsl:value-of select="@description"/>
											</xsl:element>
										</xsl:for-each>
									</xsl:with-param>
									<xsl:with-param name="width">180</xsl:with-param>
								</xsl:call-template>
							</div>
							<div id="consumerNewQuoteButton">
								<xsl:call-template name="makeButton">
									<xsl:with-param name="name">_consumerNewQuote</xsl:with-param>
									<xsl:with-param name="id">_consumerNewQuote</xsl:with-param>
									<xsl:with-param name="onclick">
										<xsl:choose>
											<xsl:when test="/page/state/isConsumerAccess = '1'">
												<xsl:choose>
													<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
													<xsl:otherwise>
														<xsl:text>DCT.Submit.</xsl:text>
													</xsl:otherwise>
												</xsl:choose>
												<xsl:value-of select="@command"></xsl:value-of>
												<xsl:text>();</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.submitAction('newQuote');</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:with-param>
									<xsl:with-param name="caption">
										<xsl:value-of select="xslNsODExt:getDictRes('StartQuote')"/>
									</xsl:with-param>
								</xsl:call-template>
							</div>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<li>
							<xsl:variable name="href">
								<xsl:choose>
									<xsl:when test="@command != 'submitAction'">
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@page"/>
										<xsl:text>'</xsl:text>
										<xsl:if test="@post">
											<xsl:text>,'</xsl:text>
											<xsl:value-of select="@post"/>
											<xsl:text>'</xsl:text>
										</xsl:if>
										<xsl:text>);</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:choose>
											<xsl:when test="starts-with(@command, 'DCT.Submit.')"></xsl:when>
											<xsl:otherwise>
												<xsl:text>DCT.Submit.</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
										<xsl:value-of select="@command"/>
										<xsl:text>('</xsl:text>
										<xsl:value-of select="@post"/>
										<xsl:text>');</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="onclick" select="$href"/>
								<xsl:with-param name="caption" select="@caption"/>
								<xsl:with-param name="name">name_<xsl:value-of select="@id"/></xsl:with-param>
								<xsl:with-param name="id">id_<xsl:value-of select="@id"/></xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</li>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<xsl:for-each select="/page/content/getPage/panel[@position = 'leftpanel' or @position = 'leftpanelpopup']">
				<div>
					<!-- Creating outer div with left-nav-top.png background. Removed id for class since the loop would create duplicates of the same id and it is not necessary -->
					<xsl:attribute name="class">
						<xsl:text>outer</xsl:text>
						<xsl:value-of select="@position"/>
					</xsl:attribute>
					<div>
						<!-- Creating inner div with left-nav-bottom.png background -->
						<xsl:attribute name="class">
							<xsl:text>inner</xsl:text>
							<xsl:value-of select="@position"/>
						</xsl:attribute>
						<xsl:apply-templates select="*"/>
					</div>
				</div>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>