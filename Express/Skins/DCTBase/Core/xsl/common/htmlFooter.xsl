﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<!--*************************************************************************************************************
		Build title for html
		************************************************************************************************************* -->
	<xsl:template name="buildHtmlFooter">
		<div class="pageFooter">
			<div class="clear_bottomLeft">
				<xsl:value-of disable-output-escaping="yes" select="xslNsODExt:GetYearStampedDictRes('CopyrightDct')"/>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet> 