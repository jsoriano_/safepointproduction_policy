﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctSecurity.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<!--*************************************************************
			Global variables
	**************************************************************-->
	<xsl:variable name="doubleQuote">
		<xsl:text>"</xsl:text>
	</xsl:variable>
	<xsl:variable name="escapedDoubleQuote">
		<xsl:text>\"</xsl:text>
	</xsl:variable>

	<!--*************************************************************
			Directories
	**************************************************************-->
	<xsl:variable name="winGuidPath">
		<xsl:text>$Guid=</xsl:text>
		<xsl:value-of select="page/state/winGuid"/>
		<xsl:text>/</xsl:text>
	</xsl:variable>
	<xsl:variable name="SkinsDir">
		<xsl:value-of select="$winGuidPath"/>
		<xsl:value-of select="translate(/page/state/xslt,'\','/')"/>
	</xsl:variable>

	<xsl:variable name="scriptDir">
		<xsl:value-of select="$winGuidPath"/>
		<xsl:text>skins/DCTBase/Core/scripts/</xsl:text>
	</xsl:variable>
	<xsl:variable name="customscriptDir">
		<xsl:value-of select="$SkinsDir"/>
		<xsl:text>/scripts/</xsl:text>
	</xsl:variable>
	<xsl:variable name="cssDir">
		<xsl:value-of select="$SkinsDir"/>
		<xsl:text>/css/</xsl:text>
	</xsl:variable>
	<xsl:variable name="localeDir">
		<xsl:value-of select="$SkinsDir"/>
		<xsl:text>/locale/</xsl:text>
	</xsl:variable>
	<xsl:variable name="theme">
		<xsl:choose>
			<xsl:when test="normalize-space(/page/settings/Themes/Theme/@name)!=''">
				<xsl:value-of select="/page/settings/Themes/Theme/@name"/>
			</xsl:when>
			<xsl:otherwise>
				<!-- default theme -->
				<xsl:text>blueberry</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="imageDir">
		<xsl:value-of select="$SkinsDir"/>
		<xsl:text>/themes/</xsl:text>
		<xsl:value-of select="$theme"/>
		<xsl:text>/images/</xsl:text>
	</xsl:variable>


	<!--*************************************************************
			State info
	**************************************************************-->
	<xsl:variable name="includeDebugInfo">
		<xsl:choose>
			<xsl:when test="/page/state/includeFieldInfo = '1'">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="currentPage" select="/page/content/@page"/>
	<xsl:variable name="manuscriptBaseCulture" select="/page/content/getPage/properties/@cultureCode"/>
	<xsl:variable name="securityLevel" select="/page/state/@securityLevel"/>
	<xsl:variable name="isUnderwriter" select="$securityLevel = 2"/>
	<xsl:variable name="isSystemAdmin" select="$securityLevel = 8"/>
	<xsl:variable name="hasAdminSecurityRights" select="$securityLevel &gt;= 8"/>
	<xsl:variable name="SpotlightConfidenceThreshold">
		<xsl:value-of select='xslNsExt:getConfigSetting("SpotlightConfidenceThreshold")'/>
	</xsl:variable>
	<xsl:variable name="SpotlightEnabled">
		<xsl:value-of select='xslNsExt:getConfigSetting("SpotlightEnabled")'/>
	</xsl:variable>
	<xsl:variable name="FieldValidationLogsEnabled">
		<xsl:value-of select='xslNsExt:getConfigSetting("FieldValidationLogsEnabled")'/>
	</xsl:variable>
	<xsl:variable name="showPathBubble">
		<xsl:value-of select='xslNsExt:getConfigSetting("showPathBubble")'/>
	</xsl:variable>
	<xsl:variable name="ShowDeveloperTools">
		<xsl:value-of select='xslNsExt:getConfigSetting("ShowDeveloperTools")'/>
	</xsl:variable>
	<xsl:variable name="isPopupPage" select="(/page/content/getPage/body/@popUp and /page/content/getPage/body/@popUp!='0') or (/page/popUp and /page/popUp!='0')"/>
	<xsl:variable name="isInterviewPage" select="/page/state/content != '' or /page/content[@page='interview' or @page='taskDetail' or @page='billingTaskDetail']"/>
	<xsl:variable name="isPartyPage" select="/page/content/portals/portal[@active = '1']/@portalType = 'pty'"/>

	<xsl:variable name="ruleSet">
		<xsl:choose>
			<xsl:when test="/page/content/getPage/body/@ruleSet !=''">
				<xsl:value-of select="/page/content/getPage/body/@ruleSet"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>leftNav</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="isEditPrintJobPage" select="/page/content[@page='editPrintJob']"/>
	<xsl:variable name="hasQuoteID" select="((number(/page/state/quoteID) &gt; 0) or ((number(/page/state/isConsumerAccess) &gt; 0) and (number(/page/state/consumerQuoteID) &gt; 0)))"/>
	<xsl:variable name="showUnderwriting">
		<xsl:value-of select='xslNsExt:getConfigSetting("showUnderwriting")'/>
	</xsl:variable>
	<xsl:variable name="isNewApplicationUWType" select="$showUnderwriting='1' and /page/content/portals/portal[@active = '1']/@portalType = 'uw'" />
	<xsl:variable name="isNewApplicationType" select="/page/content/portals/portal[@active = '1']/@portalType = 'pas' or /page/content/portals/portal[@active = '1']/@name = 'Billing' or /page/content/portals/portal[@active = '1']/@name = 'Party' or /page/content/portals/portal[@active = '1']/@portalType = 'uw'"/>
	<xsl:variable name="newQuoteContentPage">
		<xsl:choose>
			<xsl:when test="/page/actions/action[@id='New']">
				<xsl:value-of select="/page/actions/action[@id='New']/@content"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>new</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="rateMessageShowBar" select="/page/settings/Panels/RatingMessages/@showBar='1'"/>
</xsl:stylesheet>