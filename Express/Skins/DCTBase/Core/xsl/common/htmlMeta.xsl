﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<!--*************************************************************************************************************
		Build Meta tags for html
		************************************************************************************************************* -->
	<xsl:template name="buildMetaInformation">
		<!--Keith Kelly - *removing* this IE7 Compatibility tag so we cna get our Avant Garde skins to be truly compatible with IE8.
        There are a couple of other ways to switch to IE7 compatibility if necessary
        1) Compatibility button in IE8
        2) Website HTTP Headers to do this can be defined for a website in IIS Config
        -->
		<!--<meta http-equiv="X-UA-Compatible" content="IE=8"/> -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	</xsl:template>
</xsl:stylesheet>