﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctApplication.xsl"/>
	<xsl:import href="contentControls.xsl"/>
	<xsl:import href="dctLocalizedDictionary.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<!-- TODO: put buildLink back in dctInterviewControls.xsl (or some interview specific file) once we take out 
		its usage in dctCommonControls, which is supposed to be for system pages. -->
	<!--	*************************************************************************************************************
		This template is for	building the anchor links or the button links (from actions) that
		will be used to navigate from page to page, add/delete items or to popup help, etc.
		************************************************************************************************************* -->
	<xsl:template name="buildLink">
		<xsl:param name="name"/>
		<xsl:param name="value"/>
		<xsl:param name="ignoreValidation"/>
		<xsl:param name="cancelChanges">0</xsl:param>
		<xsl:param name="tableType"/>
		<xsl:param name="tableId"/>
		<xsl:param name="class"/>
		<xsl:param name="id"/>
		<xsl:param name="type"/>
		<xsl:param name="icon"/>
		<xsl:param name="iconPosition">left</xsl:param>
		<xsl:param name="qtip"></xsl:param>
		<xsl:variable name="manuscriptID">
			<xsl:choose>
				<xsl:when test="./ancestor::panel/@manuscript!=''">
					<xsl:value-of select="./ancestor::panel/@manuscript"/>
				</xsl:when>
				<xsl:when test="./ancestor::body/@manuscript!=''">
					<xsl:value-of select="./ancestor::body/@manuscript"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="/page/content/getPage/properties/@manuscriptID"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="preEvent">
			<xsl:if test="./server/requests/ManuScript.getPageRq/@checkSave='1'">
				<xsl:text>save</xsl:text>
			</xsl:if>
		</xsl:variable>	
		<xsl:variable name="preEventField">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventField"/>
				<xsl:choose>
			<xsl:when test="./server/requests/ManuScript.getPageRq/@preEventField !=''">
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventField"/>
			</xsl:when>
				<xsl:when test="./server/requests/Request.ifRq/@preEventField !=''">
				<xsl:value-of select="./server/requests/Request.ifRq/@preEventField"/>
			</xsl:when>
		</xsl:choose>
		</xsl:variable>
		<xsl:variable name="interfaceAction">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@postAction"/>
		</xsl:variable>
		<xsl:variable name="partyID">
			<xsl:value-of select="./server/requests//PartyObj.getPartyLiteRq/PartyId"/>
		</xsl:variable>
		<xsl:variable name="preEventFieldConfirmText">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventFieldConfirmText"/>
		</xsl:variable>
		<xsl:variable name="confirmText">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@confirmText"/>
		</xsl:variable>
		<xsl:variable name="actionName">
			<xsl:value-of select="$name"/>
		</xsl:variable>
		<xsl:variable name="samePage">
			<!-- Check path -->
			<xsl:choose>
				<xsl:when test="(current()/server/requests/ManuScript.getPageRq/@page=/page/content/getPage/@page) and (current()/server/requests/ManuScript.getPageRq/@topic=/page/content/getPage/@topic)">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="skipValidation">
			<xsl:choose>
				<xsl:when test="$ignoreValidation='0'">0</xsl:when>
				<!-- If explicitly set to 1 then don't ignoreValidation -->
				<xsl:when test="$ignoreValidation='1' or $samePage='1'">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="popUpWidth">
			<!-- Check into -->
			<xsl:choose>
				<xsl:when test="./server/requests/ManuScript.getPageRq/@popUpWidth!=''">
					<xsl:value-of select="./server/requests/ManuScript.getPageRq/@popUpWidth"/>
				</xsl:when>
				<xsl:otherwise>750</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="popUpHeight">
			<xsl:choose>
				<xsl:when test="./server/requests/ManuScript.getPageRq/@popUpHeight!=''">
					<xsl:value-of select="./server/requests/ManuScript.getPageRq/@popUpHeight"/>
				</xsl:when>
				<xsl:otherwise>500</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="hideCloseButton">
			<xsl:choose>
				<xsl:when test="./server/requests/ManuScript.getPageRq/@hideCloseButton!=''">
					<xsl:value-of select="./server/requests/ManuScript.getPageRq/@hideCloseButton"/>
				</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="popUpClass">
			<xsl:choose>
				<xsl:when test="@popUpClass!=''">
					<xsl:value-of select="@popUpClass"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<!-- the phrases have whitespace removed to reduce size of rendered HTML with embedded JavaScript -->
		<xsl:element name="div">
			<xsl:attribute name="id">
				<xsl:value-of select="$actionName"/>
				<xsl:text>_div</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="data-config">
				<xsl:text>{</xsl:text>
				<xsl:text>alertMsg: "</xsl:text>
				<xsl:value-of select="xslNsExt:escapeTick(current()/server/requests/ManuScript.getPageRq/@alertMsg)"/>
				<xsl:text>",</xsl:text>
				<xsl:text>alertMsgTitle: "</xsl:text>
				<xsl:value-of select="xslNsExt:escapeTick(current()/server/requests/ManuScript.getPageRq/@alertMsgTitle)"/>
				<xsl:text>",</xsl:text>
				<xsl:text>alertMsgClass: "</xsl:text>
				<xsl:value-of select="xslNsExt:escapeTick(current()/server/requests/ManuScript.getPageRq/@alertMsgClass)"/>
				<xsl:text>"</xsl:text>
				<xsl:if test="./server/requests/ManuScript.getPageRq/@command='delete' or ./server/requests/ManuScript.getPageRq/@command='markForDelete'">
					<xsl:text>,</xsl:text>
					<xsl:text>isDeleteCmd: true</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:choose>
						<xsl:when test="ancestor::group[1]/@startIndex!=''">
							<xsl:text>isList: true</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>isList: false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:text>,</xsl:text>
				<xsl:text>manuscriptId: "</xsl:text>
				<xsl:value-of select="$manuscriptID"/>
				<xsl:text>",</xsl:text>
				<xsl:text>preEventField: "</xsl:text>
				<xsl:value-of select="$preEventField"/>
				<xsl:text>",</xsl:text>
				<xsl:text>sessionId: "</xsl:text>
				<xsl:value-of select="/page/state/sessionID"/>
				<xsl:text>",</xsl:text>
				<xsl:text>skipValidation: "</xsl:text>
				<xsl:value-of select="$skipValidation"/>
				<xsl:text>",</xsl:text>
				<xsl:text>preEventFieldAlertOnly: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventFieldAlertOnly"/>
				<xsl:text>",</xsl:text>
				<xsl:text>integrationImportRq: "</xsl:text>
				<xsl:value-of select="server/requests/ManuScript.getPageRq/@IntegrationImportRq"/>
				<xsl:text>",</xsl:text>
				<xsl:text>pdfPage: "</xsl:text>
				<xsl:value-of select="server/requests/ManuScript.getPageRq/@PDFPage"/>
				<xsl:text>",</xsl:text>
				<xsl:text>actionName: "</xsl:text>
				<xsl:value-of select="$actionName"/>
				<xsl:text>",</xsl:text>
				<xsl:text>suppressRefresh: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@suppressRefresh"/>
				<xsl:text>",</xsl:text>
				<xsl:text>preEventFieldConfirm: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventFieldConfirm"/>
				<xsl:text>",</xsl:text>
				<xsl:text>popUp: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@popUp"/>
				<xsl:text>",</xsl:text>
				<xsl:text>preEventFieldConfirmText: "</xsl:text>
				<xsl:value-of select="$preEventFieldConfirmText"/>
				<xsl:text>",</xsl:text>
				<xsl:text>cancelChanges: "</xsl:text>
				<xsl:value-of select="$cancelChanges"/>
				<xsl:text>",</xsl:text>
				<xsl:text>hideCloseButton: "</xsl:text>
				<xsl:value-of select="$hideCloseButton"/>
				<xsl:text>",</xsl:text>
				<xsl:text>popUpClass: "</xsl:text>
				<xsl:value-of select="$popUpClass"/>
				<xsl:text>",</xsl:text>
				<xsl:text>popUpWidth: </xsl:text>
				<xsl:value-of select="$popUpWidth"/>
				<xsl:text>,</xsl:text>
				<xsl:text>popUpHeight: </xsl:text>
				<xsl:value-of select="$popUpHeight"/>
				<xsl:text>,</xsl:text>
				<xsl:text>popUpTitle: "</xsl:text>
				<xsl:value-of select="xslNsExt:escapeTick(./server/requests/ManuScript.getPageRq/@popUpTitle)"/>
				<xsl:text>"</xsl:text>
				<xsl:choose>
					<xsl:when test="./server/requests/ManuScript.getPageRq/@Confirm='1'">
						<xsl:text>,</xsl:text>
						<xsl:text>isConfirm: "1"</xsl:text>
					</xsl:when>
					<xsl:when test="./server/requests/ManuScript.getPageRq/@AreYouSure='1' or ./server/requests/ManuScript.getPageRq/@areyousure='1'or ./server/requests/ManuScript.getPageRq/@checkDelete='1'or ./server/requests/ManuScript.getPageRq/@checkdelete='1'">
						<xsl:text>,</xsl:text>
						<xsl:text>isDelete: "1"</xsl:text>
					</xsl:when>
				</xsl:choose>
				<xsl:text>,</xsl:text>
				<xsl:text>confirmText: "</xsl:text>
				<xsl:value-of select="$confirmText"/>
				<xsl:text>",</xsl:text>
				<xsl:text>postAction: "</xsl:text>
				<xsl:value-of select="$interfaceAction"/>
				<xsl:text>",</xsl:text>
				<xsl:text>preEventFieldAsync: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@preEventFieldAsync"/>
				<xsl:text>",</xsl:text>
				<xsl:text>asyncPreEvent: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@asyncPreEvent"/>
				<xsl:text>",</xsl:text>
				<xsl:text>asyncPostEvent: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@asyncPostEvent"/>
				<xsl:text>",</xsl:text>
				<xsl:text>asyncQuoteId: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@quoteID"/>
				<xsl:text>",</xsl:text>
				<xsl:text>partyId: "</xsl:text>
				<xsl:choose>
					<xsl:when test="contains($interfaceAction,'partyCancel')">
						<xsl:text>CANCEL</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$partyID"/>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>",</xsl:text>
				<xsl:text>actionUrl: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@actionUrl"/>
				<xsl:text>",</xsl:text>
				<xsl:text>newWindow: "</xsl:text>
				<xsl:value-of select="./server/requests/ManuScript.getPageRq/@newWindow"/>
				<xsl:text>"</xsl:text>
				<xsl:if test="server/requests/ManuScript.getPageRq/@skinCommand != ''">
					<xsl:text>,</xsl:text>
					<xsl:text>handleSkinCommand: true</xsl:text>
					<xsl:text>,</xsl:text>
					<xsl:text>skinCommand: </xsl:text>
					<xsl:call-template name="handleSkinCommand">
						<xsl:with-param name="command" select="server/requests/ManuScript.getPageRq/@skinCommand"/>
					</xsl:call-template>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="(current()/server/requests/ManuScript.getPageRq/@alertMsg) and (current()/server/requests/ManuScript.getPageRq/@alertMsg != '')">
						<xsl:text>,processType: "alertMsg"</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="./server/requests/ManuScript.getPageRq/@runForPortal">
								<xsl:text>,processType: "runForPortal"</xsl:text>
							</xsl:when>
							<xsl:when test="./server/requests/ManuScript.getPageRq/@PDFPage">
								<xsl:text>,processType: "pdfPage"</xsl:text>
							</xsl:when>
							<xsl:when test="./server/requests/ManuScript.getPageRq/@actionUrl!=''">
								<xsl:text>,processType: "actionUrl"</xsl:text>
							</xsl:when>
							<xsl:when test="contains($interfaceAction,'partyReturn')">
								<xsl:text>,processType: "partyReturn"</xsl:text>
							</xsl:when>
							<xsl:when test="contains($interfaceAction,'integrationReturn')">
								<xsl:text>,processType: "integrationReturn"</xsl:text>
							</xsl:when>
							<xsl:when test="./server/requests/ManuScript.getPageRq/@popUp = '1' or ./server/requests/ManuScript.getPageRq/@popUp = '2' or ./server/requests/ManuScript.getPageRq/@popUp = '3'">
								<xsl:text>,processType: "popUp"</xsl:text>
								<xsl:if test="./server/requests/ManuScript.getPageRq/@isCancel">
									<xsl:text>,isCancel: "1"</xsl:text>
								</xsl:if>
							</xsl:when>
					              <xsl:when test="./server/requests/ManuScript.getPageRq/@newWindow = '1'">
					                <xsl:text>,processType: "newWindow"</xsl:text>
					                <xsl:if test="./server/requests/ManuScript.getPageRq/@portalType">
					                  <xsl:text>,portalType: "</xsl:text>
					                  <xsl:value-of select="./server/requests/ManuScript.getPageRq/@portalType"/>
					                  <xsl:text>",portalName: "</xsl:text>
					                  <xsl:variable name="portalType" select="./server/requests/ManuScript.getPageRq/@portalType" />
					                  <xsl:value-of select="/page/content/portals/portal[xslNsExt:toLowerCase(@portalType) = xslNsExt:toLowerCase($portalType)][1]/@name"/>
					                  <xsl:text>"</xsl:text>
					                </xsl:if>
					              </xsl:when>
							<xsl:when test="contains($interfaceAction,'download')">
								<xsl:text>,returnPage: "</xsl:text>
								<xsl:value-of select="./server/requests/ManuScript.getPageRq/@returnPage"/>
								<xsl:text>",attachmentId: "</xsl:text>
								<xsl:value-of select="./server/requests/ManuScript.getPageRq/@attachmentId"/>
								<xsl:text>",interViewAction: "</xsl:text>
								<xsl:value-of select="$interfaceAction"/>
								<xsl:text>",processType: "download"</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="contains($interfaceAction,'billingReturn')">
										<xsl:text>,processType: "billingReturn"</xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>,processType: "default"</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>,parameters:{</xsl:text>
			        <xsl:for-each select="current()/server/requests/ManuScript.getPageRq/@*">
			          <xsl:value-of select="name()"/>
			          <xsl:text>:"</xsl:text>
			          <xsl:value-of select="."/>
			          <xsl:text>",</xsl:text>
			        </xsl:for-each>
			        <xsl:if test="./server/requests/ManuScript.getPageRq/@runForPortal">
			          <xsl:text>_PortalName:"</xsl:text>
			          <xsl:variable name="portalType" select="./server/requests/ManuScript.getPageRq/@runForPortal" />
			          <xsl:value-of select="/page/content/portals/portal[xslNsExt:toLowerCase(@portalType) = xslNsExt:toLowerCase($portalType)][1]/@name"/>
			          <xsl:text>"</xsl:text>
			        </xsl:if>
				<xsl:text>}</xsl:text>
				<xsl:text>}</xsl:text>
			</xsl:attribute>
		</xsl:element>
		<!-- Render the action as a hyperlink -->
		<xsl:call-template name="makeButton">
			<xsl:with-param name="caption">
				<xsl:choose>
					<xsl:when test="$value!=''">
						<xsl:value-of select="$value"/>
					</xsl:when>
					<xsl:when test="$name='next-button'">
						<xsl:value-of select="xslNsODExt:getDictRes('Next')"/>
					</xsl:when>
					<xsl:when test="$name='previous-button'">
						<xsl:value-of select="xslNsODExt:getDictRes('Previous')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@caption"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="type">
				<xsl:choose>
					<xsl:when test="./server/requests/ManuScript.getPageRq/@type!=''">
						<xsl:value-of select="./server/requests/ManuScript.getPageRq/@type"/>
					</xsl:when>
					<xsl:when test="$type!=''">
						<xsl:value-of select="$type"/>
					</xsl:when>
					<xsl:when test="@displayType='hyperlink'">
						<xsl:text>hyperlink</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="icon">
				<xsl:choose>
					<xsl:when test="./server/requests/ManuScript.getPageRq/@icon!=''">
						<xsl:value-of select="./server/requests/ManuScript.getPageRq/@icon"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$icon"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="iconPosition">
				<xsl:choose>
					<xsl:when test="@iconPosition!=''">
						<xsl:value-of select="@iconPosition"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$iconPosition"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="class">
				<xsl:value-of select="$class"/>
			</xsl:with-param>
			<xsl:with-param name="id">
				<xsl:choose>
					<xsl:when test="$name='next-button'">
						<xsl:text>next</xsl:text>
					</xsl:when>
					<xsl:when test="$name='previous-button'">
						<xsl:text>previous</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="substring-after($actionName,'_')"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="anchorId">
				<xsl:if test="$name!='next-button' and $name!='previous-button'">
					<xsl:value-of select="substring-after($actionName,'_')"/>
					<xsl:text>_anchorId</xsl:text>
				</xsl:if>
			</xsl:with-param>
			<xsl:with-param name="href">javascript:;</xsl:with-param>
			<xsl:with-param name="onclick">
				<xsl:text>DCT.Util.customOnClick(this);</xsl:text>
				<xsl:if test="not(./server/requests/ManuScript.getPageRq/@suppressFocus) or (./server/requests/ManuScript.getPageRq/@suppressFocus)='0'">
					<xsl:if test="(/page/content/getPage/body and /page/content/getPage/body/@popUp='0') or (/page/popUp and /page/popUp!='1')">
						<xsl:if test="$name!='next' and $name!='previous'">
							<xsl:text>DCT.Util.setFocusField('</xsl:text>
							<xsl:value-of select="substring-after($actionName,'_')"/>
							<xsl:text>_anchorId</xsl:text>
							<xsl:text>');</xsl:text>
						</xsl:if>
					</xsl:if>
				</xsl:if>
				<xsl:text>DCT.Util.processInterviewButtonAnchor('</xsl:text>
				<xsl:value-of select="$actionName"/>
				<xsl:text>_div');</xsl:text>
			</xsl:with-param>
			<xsl:with-param name="onfocus">
				<xsl:choose>
					<xsl:when test="not(./server/requests/ManuScript.getPageRq/@suppressFocus) or (./server/requests/ManuScript.getPageRq/@suppressFocus)='0'">
						<xsl:if test="(/page/content/getPage/body and /page/content/getPage/body/@popUp='0') or (/page/popUp and /page/popUp!='1')">
							<xsl:if test="$name!='next-button' and $name!='previous-button'">
								<xsl:text>DCT.Util.setFocusField('</xsl:text>
								<xsl:value-of select="substring-after($actionName,'_')"/>
								<xsl:text>_anchorId</xsl:text>
								<xsl:text>');</xsl:text>
							</xsl:if>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Ext.getDom('_prevFocusField').value = Ext.getDom('_focusField').value</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="onblur">
				<xsl:if test="(./server/requests/ManuScript.getPageRq/@suppressFocus) and (./server/requests/ManuScript.getPageRq/@suppressFocus)='1'">
					<xsl:text>DCT.Util.setFocusField(Ext.getDom('_prevFocusField').value)</xsl:text>
				</xsl:if>
			</xsl:with-param>
			<xsl:with-param name="qtip">
				<xsl:choose>
					<xsl:when test="./server/requests/ManuScript.getPageRq/@qtip!=''">
						<xsl:value-of select="./server/requests/ManuScript.getPageRq/@qtip"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$qtip"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="handleSkinCommand">
		<xsl:param name="command"/>
		<xsl:choose>
			<xsl:when test="$command='windowClose'">
				<xsl:text>window.close</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>eval(</xsl:text>
				<xsl:value-of select="$command"/>
				<xsl:text>)</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--*************************************************************************************************************
		Diary widget 
		************************************************************************************************************* -->
	<xsl:template name="buildDiary">
		<xsl:param name="showHeader"/>
		<xsl:choose>
      <xsl:when test="(/page/content/policyList/policy/@policyID != '')">
        <li>
          <a href="javascript:;" onclick="DCT.Submit.gotoPageForQuote('policy','{/page/content/policyList/policy/@policyID}')" id="diary">
            <xsl:value-of select="xslNsODExt:getDictRes('ViewPolicyDetails')"/>
          </a>
        </li>
      </xsl:when>
		<xsl:when test="(/page/state/quoteID != 0) and $showHeader">
			<li>
				<a href="javascript:;" onclick="DCT.Submit.gotoPageForQuote('policy','{/page/state/quoteID}')" id="diary">
					<xsl:value-of select="xslNsODExt:getDictRes('ViewPolicyDetails')"/>
				</a>
			</li>
		</xsl:when>
	</xsl:choose>
	</xsl:template>


	<xsl:variable name="helpImage"/>


	<xsl:template name="buildJavaScriptInclude">
		<xsl:param name="filename"/>
		<xsl:param name="dir" select="$scriptDir"/>
		<xsl:text disable-output-escaping="yes">&lt;script type="text/javascript" src="</xsl:text>
		<xsl:value-of select="$dir"/>
		<xsl:value-of select="$filename"/>
		<xsl:text disable-output-escaping="yes">&quot;&gt;&lt;/script&gt;</xsl:text>
	</xsl:template>
	
	<xsl:template name="reloadPageInParentWindow">
		<html>
			<head>

				<xsl:choose>
					<xsl:when test="/page/debugMode!='0'">
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-base-debug.js</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all-debug.js</xsl:with-param>
						</xsl:call-template>
						<!-- Localization Javascript reference - needs to load before the dct file so the strings are known -->
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename"><xsl:value-of select="$winGuidPath"/>DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base-debug.js</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-base.js</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">ext-all.js</xsl:with-param>
						</xsl:call-template>
						<!-- Localization Javascript reference - needs to load before the dct file so the strings are known -->
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename"><xsl:value-of select="$winGuidPath"/>DYNAMIC_LocalizationDictionary.jsloc</xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="dir" select="$localeDir"/>
							<xsl:with-param name="filename" select="$ExtJsLocaleFile"></xsl:with-param>
						</xsl:call-template>
						<xsl:call-template name="buildJavaScriptInclude">
							<xsl:with-param name="filename">dct-base.js</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
				<script>
				Ext.onReady(function (){
					var  parentWindow = DCT.Util.getParent();
					parentWindow.DCT.Submit.submitPage(DCT.currentPage);
				});
				</script>
			</head>
			<body>
			</body>
		</html>
	</xsl:template>

	<!--
		///CommentStart		
		*************************************************************************************************************
		makeButton template
		************************************************************************************************************* 
		The makeButton template supports a variety of visual presentation options for HTML anchors (links).  
	
		** Parameters ******
		<param name="caption">
			The text that will be displayed in the button.  
			When type is set to "loneIcon" the caption value will be used as the body content 
			for the ExtJs QuickTip (qtip).
		</param>
			
		:: caption - The text that will be displayed in the button.  
			When type is set to "loneIcon" the caption value will be used as the body content 
			for the ExtJs QuickTip (qtip).
		:: href - The href attribute of the included anchor element.
		:: name - The name attribute of the included anchor element.
		:: icon - The filename of the image to be used as the button icon.  
			The file should be located in the /your_skin_directory/images/icons folder.
		:: iconPosition - Indicator determining the position of the icon relative to the button text.
			Currently supported values are "left" and "right".  The setting of this value alters the 
			internal structure of the button html.
		:: onclick - The onclick attribute of the included anchor element.
		:: onfocus - The onfocus attribute of the included anchor element.
		:: class - The class attribute of the outer div element.
		:: id - The id attribute of the outer div element.
		:: type - The button type.  
			This is set as the class on the outer div (ex: class="g-{$type}").  Left undefined,
			type defaults to a value of "standard".  Using the type "loneIcon" will alter the internal 
			structure of the button html such that no text (span) is displayed.  Additionally the 
			AvantGarde2 base skin supports a type called "hyperlink" which will display the button as 
			a hyperlink.  In this scenario, icons to the left or right of the text are still supported.
		:: qtip - The body content for the ExtJs Quick Tip 
		:: qtitle - The title for the ExtJs Quick Tip 
		
		
		** Sample Configurations ******
		Here's a few sample configurations to accomplish common scenarios:
		
		:: Simple Hyperlink
			- type = "hyperlink"
			Note: In this scenario classes g-btn-l and g-btn-r will be set to display:none and
				g-btn-m will not have a visible background.
				
			*** Produces the following html
			<div class="g-btn g-hyperlink {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  
		
		:: Hyperlink with Icon on left
			- type = "hyperlink"
			- icon = "myicon.png"
			Note: In this scenario classes g-btn-l and g-btn-r will be set to display:none and
				g-btn-m will not have a visible background.

			*** Produces the following html
			<div class="g-btn g-hyperlink {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<img src="path_to_image_dir/icons/myicon.png" class="g-btn-img-left" qtip="{$qtip}" />
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  

		:: Hyperlink with Icon on right
			- type = "hyperlink"
			- icon = "myicon.png"
			- iconPosition = "right"
			Note: In this scenario classes g-btn-l and g-btn-r will be set to display:none and
				g-btn-m will not have a visible background.

			*** Produces the following html
			<div class="g-btn g-hyperlink {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
						<img src="path_to_image_dir/icons/myicon.png" class="g-btn-img-right" qtip="{$qtip}" />
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  			
			
		:: Simple Button

			*** Produces the following html
			<div class="g-btn g-standard {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  

		
		:: Button with Icon on the Left
			- icon = "myicon.png"

			*** Produces the following html
			<div class="g-btn g-{$type} {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<img src="path_to_image_dir/icons/myicon.png" class="g-btn-img-left" qtip="{$qtip}" />
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  
		
		:: Button with Icon on the Right
			- icon = "myicon.png"
			- iconPosition = "right"

			*** Produces the following html
			<div class="g-btn g-{$type} {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<span class="g-btn-text"><xsl:value-of select="$caption"/></span>
						<img src="path_to_image_dir/icons/myicon.png" class="g-btn-img-right" qtip="{$qtip}" />
					</div>       
					<div class="g-btn-r"></div>
				</a>
			</div>  
		
		:: Icon only (no text)
			- type = "loneIcon"
			- icon = "myicon.png"

			*** Produces the following html
			<div class="g-btn g-loneIcon {$class}" id="{$id}">
				<a 	onmousemove="window.status='';" onmouseout="window.status='';"
					name="{$name}" href="{$href}" onclick="{$onclick}" onfocus="{$onfocus}"
					qtip="{$qtip}" qtitle="{$qtitle}">
					<div class="g-btn-l"></div>     
					<div class="g-btn-m">    
						<img src="path_to_image_dir/icons/myicon.png" class="g-btn-img-loneIcon" qtip="{$caption}" />
					</div>       
					<div class="g-btn-r"></div>		
				</a>
			</div>  
	///CommentEnd
				
		-->
	<xsl:template name="makeButton">
		<xsl:param name="caption"/>
		<xsl:param name="href">javascript:;</xsl:param>
		<xsl:param name="name"/>
		<xsl:param name="icon"/>
		<xsl:param name="iconPosition">left</xsl:param>
		<xsl:param name="onclick"/>
		<xsl:param name="onfocus"/>
		<xsl:param name="onblur" />
		<xsl:param name="class"/>
		<xsl:param name="id"/>
		<xsl:param name="type"/>
		<xsl:param name="qtip"/>
		<xsl:param name="qtitle"/>
		<xsl:param name="anchorId"/>
		<xsl:param name="runInterviewActionAsync" />
		<xsl:param name="runInterviewActionAsyncAuto" />
		
		<xsl:variable name="buttonType">
			<xsl:choose>
				<xsl:when test="$type!=''">
					<xsl:value-of select="$type"/>
				</xsl:when>
				<xsl:otherwise>standard</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="qtipValue">
			<xsl:choose>
				<xsl:when test="$qtip!=''">
					<xsl:value-of select="$qtip"/>
				</xsl:when>
				<xsl:when test="$buttonType='loneIcon'">
					<xsl:value-of select= "$caption"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="iconSource">
			<xsl:value-of select="$imageDir"/>
			<xsl:text>icons/</xsl:text>
			<xsl:value-of select="$icon"/>
		</xsl:variable>
		<xsl:variable name="runAsync">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@async"/>
		</xsl:variable>
		<xsl:variable name="runAsyncAutomatic">
			<xsl:choose>
				<xsl:when test="$runInterviewActionAsyncAuto=''">
					<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncAutomatic"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$runInterviewActionAsyncAuto"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="runActionAsyncObjectType">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncObjectType"/>
		</xsl:variable>
		<xsl:variable name="runActionAsyncObjectId">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncObjectID"/>
		</xsl:variable>
		<xsl:variable name="runActionAsyncAllowWait">
			<xsl:value-of select="./server/requests/ManuScript.getPageRq/@runActionAsyncAllowWait"/>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$runAsync='2' or $runAsync='3'">
				<xsl:choose>
					<xsl:when test="$runAsyncAutomatic='' or $runAsyncAutomatic='0'">

						<!-- Show split async button-->
						<div class="g-btn g-{$buttonType} {$class} splitButtonDiv" id="{$id}">
							<a onmousemove="window.status='';" class="splitButtonNormal" onmouseout="window.status='';">
								<xsl:attribute name="name">
									<xsl:value-of select="$name"/>
								</xsl:attribute>
								<xsl:attribute name="href">
									<xsl:value-of select="$href"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="$anchorId"/>
								</xsl:attribute>
								<xsl:attribute name="onfocus">
									<xsl:if test="$onfocus!=''">
										<xsl:value-of select="$onfocus"/>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:value-of select="$onclick"/>
								</xsl:attribute>
								<xsl:attribute name="onblur">
									<xsl:value-of select="$onblur" />
								</xsl:attribute>
								<xsl:if test="normalize-space($qtip)!=''">
									<xsl:attribute name="data-qtip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
									<xsl:attribute name="data-tip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:if test="normalize-space($qtitle)!=''">
									<xsl:attribute name="ext:qtitle">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
									<xsl:attribute name="data-title">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
								</xsl:if>
								<!--button contents-->
								<xsl:choose>
									<xsl:when test="$icon!=''">
										<xsl:choose>
											<xsl:when test="$buttonType='loneIcon'">
												<img src="{$iconSource}" class="g-btn-img-loneIcon" title ="{$qtipValue}" data-tip="{$qtipValue}"/>
											</xsl:when>
											<xsl:otherwise>
												<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
													<xsl:value-of select="$caption"/>
												</span>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
											<xsl:value-of select="$caption"/>
										</span>
									</xsl:otherwise>
								</xsl:choose>
							</a>
							<!--Split button: right side content-->
							<a onmousemove="window.status='';" onmouseout="window.status='';" class="clockIcon splitButtonGlyph">
								<xsl:attribute name="name">
									<xsl:value-of select="$name"/>
								</xsl:attribute>
								<xsl:attribute name="href">
									<xsl:value-of select="$href"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="$anchorId"/>
								</xsl:attribute>
								<xsl:attribute name="onfocus">
									<xsl:if test="$onfocus!=''">
										<xsl:value-of select="$onfocus"/>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Util.setInterviewActionAsync();</xsl:text>
									<xsl:text>DCT.Util.processInterviewActionAsync('</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectType"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectId"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncAllowWait"/>
									<xsl:text>');</xsl:text>
									<xsl:value-of select="$onclick"/>
								</xsl:attribute>
								<xsl:attribute name="onblur">
									<xsl:value-of select="$onblur" />
								</xsl:attribute>
								<xsl:if test="normalize-space($qtip)!=''">
									<xsl:attribute name="data-qtip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
									<xsl:attribute name="data-tip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:if test="normalize-space($qtitle)!=''">
									<xsl:attribute name="ext:qtitle">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
									<xsl:attribute name="data-title">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
								</xsl:if>
							</a>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<!-- Show single async button-->
						<div class="g-btn g-{$buttonType} {$class}" id="{$id}">
							<a onmousemove="window.status='';" onmouseout="window.status='';">
								<xsl:attribute name="name">
									<xsl:value-of select="$name"/>
								</xsl:attribute>
								<xsl:attribute name="href">
									<xsl:value-of select="$href"/>
								</xsl:attribute>
								<xsl:attribute name="id">
									<xsl:value-of select="$anchorId"/>
								</xsl:attribute>
								<xsl:attribute name="onfocus">
									<xsl:if test="$onfocus!=''">
										<xsl:value-of select="$onfocus"/>
									</xsl:if>
								</xsl:attribute>
								<xsl:attribute name="onclick">
									<xsl:text>DCT.Util.setInterviewActionAsync();</xsl:text>
									<xsl:text>DCT.Util.processInterviewActionAsync('</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectType"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncObjectId"/>
									<xsl:text>','</xsl:text>
									<xsl:value-of select="$runActionAsyncAllowWait"/>
									<xsl:text>');</xsl:text>
									<xsl:value-of select="$onclick"/>
								</xsl:attribute>
								<xsl:attribute name="onblur">
									<xsl:value-of select="$onblur" />
								</xsl:attribute>
								<xsl:if test="normalize-space($qtip)!=''">
									<xsl:attribute name="data-qtip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
									<xsl:attribute name="data-tip">
										<xsl:value-of select="$qtipValue"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:if test="normalize-space($qtitle)!=''">
									<xsl:attribute name="ext:qtitle">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
									<xsl:attribute name="data-title">
										<xsl:value-of select="$qtitle"/>
									</xsl:attribute>
								</xsl:if>
								<!-- button contents -->
								<xsl:choose>
									<xsl:when test="$icon!=''">
										<xsl:choose>
											<xsl:when test="$buttonType='loneIcon'">
												<img src="{$iconSource}" class="g-btn-img-loneIcon" title ="{$qtipValue}" data-tip="{$qtipValue}"/>
											</xsl:when>
											<xsl:otherwise>
												<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
													<xsl:value-of select="$caption"/>
												</span>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
											<xsl:value-of select="$caption"/>
										</span>
									</xsl:otherwise>
								</xsl:choose>
								<clock class="clockIconActive asyncButtonGlyph" />
							</a>
						</div>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Show normal button-->
				<div class="g-btn g-{$buttonType} {$class}" id="{$id}">
					<a onmousemove="window.status='';" onmouseout="window.status='';">
						<xsl:attribute name="name">
							<xsl:value-of select="$name"/>
						</xsl:attribute>
						<xsl:attribute name="href">
							<xsl:value-of select="$href"/>
						</xsl:attribute>
						<xsl:attribute name="id">
							<xsl:value-of select="$anchorId"/>
						</xsl:attribute>
						<!-- these aren't being used yet
			<xsl:attribute name="class"></xsl:attribute>
			-->
						<xsl:attribute name="onfocus">
							<xsl:if test="$onfocus!=''">
								<xsl:value-of select="$onfocus"/>
							</xsl:if>
						</xsl:attribute>
						<xsl:attribute name="onclick">
							<xsl:value-of select="$onclick"/>
						</xsl:attribute>
						<xsl:attribute name="onblur">
							<xsl:value-of select="$onblur" />
						</xsl:attribute>
						<xsl:if test="normalize-space($qtip)!=''">
							<xsl:attribute name="data-qtip">
								<xsl:value-of select="$qtipValue"/>
							</xsl:attribute>
							<xsl:attribute name="data-tip">
								<xsl:value-of select="$qtipValue"/>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="normalize-space($qtitle)!=''">
							<xsl:attribute name="ext:qtitle">
								<xsl:value-of select="$qtitle"/>
							</xsl:attribute>
							<xsl:attribute name="data-title">
								<xsl:value-of select="$qtitle"/>
							</xsl:attribute>
						</xsl:if>

						<!-- button contents -->
						<xsl:choose>
							<xsl:when test="$icon!=''">
								<xsl:choose>
									<xsl:when test="$buttonType='loneIcon'">
										<img src="{$iconSource}" class="g-btn-img-loneIcon" title ="{$qtipValue}" data-tip="{$qtipValue}"/>
									</xsl:when>
									<xsl:otherwise>
										<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
											<xsl:value-of select="$caption"/>
										</span>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<span class="g-btn-text" data-qtip="{$qtipValue}" data-tip="{$qtipValue}">
									<xsl:value-of select="$caption"/>
								</span>
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</div>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:template>
  <xsl:template name="buildLanguageCombo">
    <xsl:param name="imageComboContainerId"></xsl:param>
    <xsl:param name="imageComboInputId"></xsl:param>
    <xsl:param name="imageComboHiddenIName"></xsl:param>
    <xsl:param name="imageComboContainerClass"></xsl:param>
    <xsl:variable name="GlobalCultureCode">
      <xsl:value-of select="xslNsODExt:getCulture()"/>
    </xsl:variable>
    <xsl:element name="div">
      <xsl:attribute name="id">
        <xsl:value-of select="$imageComboContainerId"/>
      </xsl:attribute>
      <xsl:attribute name="class">
        <xsl:value-of select="$imageComboContainerClass"/>
      </xsl:attribute>
      <xsl:element name="div">
        <xsl:attribute name="class">languageComboField</xsl:attribute>
        <xsl:attribute name="data-config">
          <xsl:text>{</xsl:text>
          <xsl:text>renderTo: "</xsl:text>
          <xsl:value-of select="$imageComboContainerId"/>
          <xsl:text>",  value: "</xsl:text>
          <xsl:value-of select="$GlobalCultureCode"/>
          <xsl:text>",</xsl:text>
          <xsl:text>dctComboImageInputCls: "</xsl:text>
          <xsl:text>languageComboInputFlag</xsl:text>
          <xsl:text>",</xsl:text>
          <xsl:text>id: "</xsl:text>
          <xsl:value-of select="$imageComboInputId"/>
          <xsl:text>",</xsl:text>
          <xsl:text>hiddenName: "</xsl:text>
          <xsl:value-of select="$imageComboHiddenIName"/>
          <xsl:text>",</xsl:text>
          <xsl:text>dctStore:  </xsl:text>
          <xsl:text>[</xsl:text>
          <xsl:call-template name="buildLanguageComboDataStore">
            <xsl:with-param name="resourceName">LanguageEN</xsl:with-param>
            <xsl:with-param name="name">en-US</xsl:with-param>
            <xsl:with-param name="imageFile">no_Flag.gif</xsl:with-param>
          </xsl:call-template>
          <xsl:text>,</xsl:text>
          <xsl:call-template name="buildLanguageComboDataStore">
            <xsl:with-param name="resourceName">LanguageDE</xsl:with-param>
            <xsl:with-param name="name">de-DE</xsl:with-param>
            <xsl:with-param name="imageFile">no_Flag.gif</xsl:with-param>
          </xsl:call-template>
          <xsl:text>,</xsl:text>
          <xsl:call-template name="buildLanguageComboDataStore">
            <xsl:with-param name="resourceName">LanguageIT</xsl:with-param>
            <xsl:with-param name="name">it-IT</xsl:with-param>
            <xsl:with-param name="imageFile">no_Flag.gif</xsl:with-param>
          </xsl:call-template>
          <xsl:text>]</xsl:text>
          <xsl:text> }</xsl:text>
        </xsl:attribute>
      </xsl:element>
    </xsl:element>
  </xsl:template>
	<xsl:template name="buildLanguageComboDataStore">
		<xsl:param name="resourceName" />
		<xsl:param name="name" />
		<xsl:param name="imageFile" />
		<xsl:text>[</xsl:text>
		<xsl:text>DCT.T("</xsl:text>
		<xsl:value-of select="$resourceName"/>
		<xsl:text>"),"</xsl:text>
		<xsl:value-of select="$name"/>
		<xsl:text>","</xsl:text>
		<xsl:value-of select="$imageFile"/>
		<xsl:text>"</xsl:text>
		<xsl:text>]</xsl:text>
	</xsl:template>
</xsl:stylesheet>