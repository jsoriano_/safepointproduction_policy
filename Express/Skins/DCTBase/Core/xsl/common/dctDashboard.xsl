﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<xsl:variable name="cultureFormat">
		<xsl:value-of select="xslNsExt:formatMaskToUse('', '', '1')"/>
	</xsl:variable>
	<xsl:template name="dashboardWidget">
		<div id="dashboard"></div>
		<xsl:apply-templates select="//pageLayout"/>
		<xsl:apply-templates select="//module" mode="content"/>
	</xsl:template>
	<xsl:template match="pageLayout">
		<script type="text/javascript">
			<xsl:text disable-output-escaping="yes">
			Ext.onReady(function() {
			var PortalManager = new DCTPortalManager({ renderTo: 'dashboard' });
			</xsl:text>
			<xsl:apply-templates/>
			<xsl:text disable-output-escaping="yes">
			});
			</xsl:text>
		</script>
	</xsl:template>
	<xsl:template match="zone">
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.add(new Ext.DCTPortalZone({ type: 'zone'}));</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.popActiveContainer();</xsl:text>
	</xsl:template>
	<xsl:template match="column">
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.add(new Ext.DCTPortalColumn({ type: 'column', columnWidth: </xsl:text>
		<xsl:value-of select="@width"/>
		<xsl:text>}));</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.popActiveContainer();</xsl:text>
	</xsl:template>

	<xsl:template match="tabPanel">
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.add(new Ext.DCTPortalModule({ </xsl:text>
		<xsl:text>id: '</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>Module',</xsl:text>
		<xsl:text>title: '</xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes(@title)"/>
		<xsl:text>', </xsl:text>

		<xsl:if test="@simple and (@simple = 1)">
			<xsl:text>frame:false, header:false, bodyBorder:false,</xsl:text>
		</xsl:if>
		<xsl:text disable-output-escaping="yes">
			items:[{
				 xtype:'tabpanel'
				,id:'tp_</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text disable-output-escaping="yes">'
				,activeTab:0
				,stateId:'tabPanel_</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text disable-output-escaping="yes">'
				,stateEvents:['tabchange']
				,getState:function() {
					return {
						activeTab:this.items.indexOf(this.getActiveTab())
					};
				}
				,defaults:{layout:'fit', border:false, bodyBorder:false, style:"padding:0px 0px 0px 0px"}
				,plain:true
				,frame:false
				,bodyBorder:false
			}],type: 'tabpanel'}));
		</xsl:text>

		<xsl:text>
		</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.popActiveContainer();</xsl:text>
	</xsl:template>
	<xsl:template match="module">
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.add(new Ext.DCTPortalModule({ </xsl:text>
		<xsl:text>id: '</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>Module',</xsl:text>
		<xsl:text>title: '</xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes(@title)"/>
		<xsl:text>', </xsl:text>

		<xsl:if test="@iconCls!=''">
			<xsl:text>iconCls:'</xsl:text>
			<xsl:value-of select="@iconCls"/>
			<xsl:text>',</xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@collapsed='1'">
				<xsl:text>collapsed:true,</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>collapsed:false,</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="@simple and (@simple = 1)">
				<xsl:text>frame:false, header:false, bodyBorder:false,</xsl:text>
			</xsl:when>
			<xsl:when test="name(..)='tabPanel'">
				<xsl:text>header:false, border:false, bodyBorder:false,</xsl:text>
			</xsl:when>
		</xsl:choose>


		<xsl:text>contentEl: '</xsl:text>
		<xsl:value-of select="@id"/>
		<xsl:text>', type: 'module'}));</xsl:text>
		<xsl:text>
		</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>
		</xsl:text>
		<xsl:text>PortalManager.popActiveContainer();</xsl:text>
	</xsl:template>
	<xsl:template match="module[@id='newQuote']" mode="content">
		<xsl:if test="/page/content/ExtendedNewQuoteContent[@isInterview='1']">
			<!-- if a NewQuote MS is defined, otherwise this module is invalid -->
			<div id="{@id}" class="x-hidden">
				<div id="fields">
					<xsl:call-template name="buildCommonLayout">
					</xsl:call-template>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<xsl:template match="module[@id='taskQueue']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:choose>
				<xsl:when test="not(/page/content/TaskList/Task)">
					<div class="noItems">
						<xsl:value-of select="xslNsODExt:getDictRes('YouHaveNoTasksDueToday')"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<table id="moduleDisplayTable">
						<tr class="tblHeader">
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Category')"/>
							</th>
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Title')"/>
							</th>
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('PolicyQuoteNumSymbol')"/>
							</th>
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Due')"/>
							</th>
						</tr>
						<xsl:for-each select="/page/content/TaskList/Task/ItemData[ObjectTypeCode='POL']">
							<tr class="tblItem">
								<td>
									<xsl:value-of select="TaskType"/>
								</td>
								<td>
									<a>
										<xsl:attribute name="name">mruLoadQuote</xsl:attribute>
										<xsl:attribute name="class">mruLoadQuote</xsl:attribute>
										<xsl:attribute name="href">javascript:;</xsl:attribute>
										<xsl:attribute name="onclick">
											<xsl:text>DCT.Submit.viewTask('</xsl:text>
											<xsl:value-of select="TaskId"/>
											<xsl:text>','</xsl:text>
											<xsl:value-of select="/page/content/@page"/>
											<xsl:text>');</xsl:text>
										</xsl:attribute>
										<xsl:call-template name="limitText">
											<xsl:with-param name="text" select="TaskTitle"/>
											<xsl:with-param name="limit">50</xsl:with-param>
										</xsl:call-template>
									</a>
								</td>
								<td>
									<xsl:choose>
										<xsl:when test="string-length(ObjectReference) = 0">
											<xsl:value-of select="xslNsODExt:getDictRes('Quote')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="ObjectReference"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
								<td>
									<xsl:value-of select="xslNsExt:cultureAwareDateFormatter('', '', DueDate)"/>
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">taskQueueGoPage</xsl:with-param>
						<xsl:with-param name="id">taskQueueGoPage</xsl:with-param>
						<xsl:with-param name="onclick">DCT.Submit.submitPage('taskInfo');</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAllTasks')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='notifications']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:choose>
				<xsl:when test="not(/page/content/messages/message)">
					<div class="noItems">
						<xsl:value-of select="xslNsODExt:getDictRes('YouHaveNoNotifications')"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<table id="moduleDisplayTable">
						<tr class="tblHeader">
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Type')"/>
							</th>
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Message')"/>
							</th>
							<th>
								<xsl:value-of select="xslNsODExt:getDictRes('Received')"/>
							</th>
						</tr>
						<xsl:for-each select="/page/content/messages/message">
							<tr class="tblItem">
								<td>
									<xsl:variable name="actionCode" select="action"/>
									<xsl:value-of select="/page/content/CodeLists/CodeList[@ListName='POL_MESSAGETYPE']/ListEntry[@Code=$actionCode]/@Description"/>
								</td>
								<td>
									<a>
										<xsl:attribute name="name">notifyLoadMessage</xsl:attribute>
										<xsl:attribute name="class">notifyLoadMessage</xsl:attribute>
										<xsl:attribute name="href">javascript:;</xsl:attribute>
										<xsl:attribute name="onclick">
											<xsl:text>DCT.Submit.actOnMessage('</xsl:text>
											<xsl:value-of select="@messageID"/>
											<xsl:text>','readMessage');</xsl:text>
										</xsl:attribute>
										<xsl:call-template name="limitText">
											<xsl:with-param name="text" select="body"/>
											<xsl:with-param name="limit">50</xsl:with-param>
										</xsl:call-template>
									</a>
								</td>
								<td>
									<xsl:value-of select="xslNsExt:convertISOtoDisplay(sentOn, 'G')"/>
								</td>
							</tr>
						</xsl:for-each>
					</table>
					<xsl:call-template name="makeButton">
						<xsl:with-param name="name">notifyGoPage</xsl:with-param>
						<xsl:with-param name="ID">notifyGoPage</xsl:with-param>
						<xsl:with-param name="class"></xsl:with-param>
						<xsl:with-param name="onclick">
							<xsl:text>DCT.Submit.submitPage('messages');</xsl:text>
						</xsl:with-param>
						<xsl:with-param name="caption">
							<xsl:value-of select="xslNsODExt:getDictRes('ViewAllNotifications')"/>
						</xsl:with-param>
						<xsl:with-param name="type">hyperlink</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='mruQuote']" mode="content">
		<div id="{@id}" class="x-hidden">
			<div id="mruList">
				<xsl:choose>
					<xsl:when test="not(/page/content/policies/policy)">
						<div class="noItems">
							<xsl:value-of select="xslNsODExt:getDictRes('NoRecentlyAccessedItems_period')"/>
						</div>
					</xsl:when>
					<xsl:otherwise>
						<table id="moduleDisplayTable">
							<xsl:for-each select="/page/content/policies/policy">
								<xsl:variable name="clientName">
									<xsl:value-of select="clientName"/>
								</xsl:variable>
								<xsl:variable name="splitClientName">
									<xsl:value-of select="xslNsExt:splitText($clientName,' ','..','17')"/>
								</xsl:variable>
								<xsl:variable name="isSplit">
									<xsl:choose>
										<xsl:when test="contains($splitClientName, '..')">1</xsl:when>
										<xsl:otherwise>0</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:if test="$isSplit = '1'">
									<script>Ext.QuickTips.init();</script>
								</xsl:if>
								<tr class="tblItem">
									<td class="itemIconEmpty" valign="top"></td>
									<td class="clientName">
										<a>
											<xsl:attribute name="name">mruLoadQuote</xsl:attribute>
											<xsl:attribute name="class">mruLoadQuote</xsl:attribute>
											<xsl:attribute name="href">javascript:;</xsl:attribute>
											<xsl:attribute name="onclick">
												<xsl:text>DCT.Submit.gotoPageForClient('clientInfo','</xsl:text>
												<xsl:value-of select="clientName/@clientID"/>
												<xsl:text>');</xsl:text>
											</xsl:attribute>
											<xsl:if test="$isSplit = '1'">
												<xsl:attribute name="ext:qtitle">
													<xsl:value-of select="xslNsODExt:getDictRes('NameLengthExceedsDisplay_Full')"/>
													<xsl:text> </xsl:text>
												</xsl:attribute>
												<xsl:attribute name="data-title">
													<xsl:value-of select="xslNsODExt:getDictRes('NameLengthExceedsDisplay_Full')"/>
													<xsl:text> </xsl:text>
												</xsl:attribute>
												<xsl:attribute name="data-qtip">
													<xsl:value-of select="$clientName"/>
												</xsl:attribute>
												<xsl:attribute name="data-tip">
													<xsl:value-of select="$clientName"/>
												</xsl:attribute>
											</xsl:if>
											<xsl:value-of select="$splitClientName"/>
										</a>
									</td>
									<td>
										<xsl:value-of select="lob"/>
									</td>
									<td>
										<a>
											<xsl:attribute name="name">mruLoadQuote</xsl:attribute>
											<xsl:attribute name="class">mruLoadQuote</xsl:attribute>
											<xsl:attribute name="href">javascript:;</xsl:attribute>
											<xsl:attribute name="onclick">
												<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
												<xsl:value-of select="@policyID"/>
												<xsl:text>','</xsl:text>
												<xsl:value-of select="lob"/>
												<xsl:text>');</xsl:text>
											</xsl:attribute>
											<xsl:choose>
												<xsl:when test="string-length(policyNumber) = 0">
													<xsl:value-of select="xslNsODExt:getDictRes('Quote')"/>
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="policyNumber"/>
												</xsl:otherwise>
											</xsl:choose>
										</a>
									</td>
								</tr>
							</xsl:for-each>
						</table>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='consumerAccess']" mode="content">
		<div id="{@id}" class="x-hidden">
			<div id="fields">
				<xsl:call-template name="buildCommonLayout">
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='partyActions']" mode="content">
		<div id="{@id}" class="x-hidden">
			<div id="partyPortalProcessing">
				<div id="partyActionsLabel">
					<label>
						<xsl:value-of select="xslNsODExt:getDictRes('IWantTo_lowercase')"/>
					</label>
				</div>
				<div id="partyActionsLinks">
                    <xsl:if test="not($IsReadOnly)">
                        <div id="partyDashboardAdd">
                            <xsl:call-template name="makeButton">
                                <xsl:with-param name="name">addParty</xsl:with-param>
                                <xsl:with-param name="ID">addParty</xsl:with-param>
                                <xsl:with-param name="href">
                                    <xsl:text>javascript:DCT.Submit.submitPartyAction(':partyAdd','</xsl:text>
                                    <xsl:text>["_isStandalone:1"</xsl:text>
                                    <xsl:text>]');</xsl:text>
                                </xsl:with-param>
                                <xsl:with-param name="caption">
                                    <xsl:value-of select="xslNsODExt:getDictRes('AddNewPersonOrPlace')"/>
                                </xsl:with-param>
                                <xsl:with-param name="type">hyperlink</xsl:with-param>
                            </xsl:call-template>
                        </div>
                    </xsl:if>
					<div id="partyDashboardSearch">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">searchParty</xsl:with-param>
							<xsl:with-param name="ID">searchParty</xsl:with-param>
							<xsl:with-param name="href">
								<xsl:text>javascript:DCT.Submit.submitPartyAction(':partySearch','</xsl:text>
								<xsl:text>["_isStandalone:1","_partyReturnActions:updateAccessedParties"</xsl:text>
								<xsl:text>]');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('SearchForPersonOrPlace')"/>
							</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>
					<!--<div id="partyDashboardPartyManagement">
						<xsl:call-template name="makeButton">
							<xsl:with-param name="name">partyManagement</xsl:with-param>
							<xsl:with-param name="id">partyManagement</xsl:with-param>
							<xsl:with-param name="onclick">DCT.Submit.submitPartyPage('partyManagement');</xsl:with-param>
							<xsl:with-param name="href">
								<xsl:text>javascript:DCT.Submit.submitPartyPage('partyManagement');</xsl:text>
							</xsl:with-param>
							<xsl:with-param name="caption">
								<xsl:value-of select="xslNsODExt:getDictRes('ManagePeopleAndPlaces')"/>
							</xsl:with-param>
							<xsl:with-param name="icon">link_go.png</xsl:with-param>
							<xsl:with-param name="type">hyperlink</xsl:with-param>
						</xsl:call-template>
					</div>-->
				</div>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='partyList']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:choose>
				<xsl:when test="not(/page/content/MRUObjects/MRUObject)">
					<div class="noItems">No recently accessed items</div>
				</xsl:when>
				<xsl:otherwise>
					<table id="moduleDisplayTable">
						<xsl:for-each select="/page/content/MRUObjects/MRUObject">
							<xsl:variable name="partyName">
								<xsl:value-of select="MRUObjectExtendedData/PartyFullName"/>
							</xsl:variable>
							<xsl:variable name="splitPartyName">
								<xsl:value-of select="xslNsExt:splitText($partyName,' ','..','17')"/>
							</xsl:variable>
							<xsl:variable name="isSplit">
								<xsl:choose>
									<xsl:when test="contains($splitPartyName, '..')">1</xsl:when>
									<xsl:otherwise>0</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							<xsl:if test="$isSplit = '1'">
								<script>Ext.QuickTips.init();</script>
							</xsl:if>
							<tr class="tblItem">
								<td>
									<a>
										<xsl:attribute name="name">partyLink</xsl:attribute>
										<xsl:attribute name="id">partyLinkId</xsl:attribute>
										<xsl:attribute name="class">partyLink</xsl:attribute>
										<xsl:attribute name="href">#</xsl:attribute>
										<xsl:attribute name="onclick">
											<xsl:text>DCT.Submit.submitPartyAction(':partyUpdate','["_partyIndex:</xsl:text>
											<xsl:value-of select="ObjectId"/>
											<xsl:text>", "_partyReturnStructure:none"]');</xsl:text>
										</xsl:attribute>
										<xsl:if test="$isSplit = '1'">
											<xsl:attribute name="ext:qtitle">
												<xsl:value-of select="xslNsODExt:getDictRes('NameLengthExceedsDisplay_Full')"/>
											</xsl:attribute>
											<xsl:attribute name="data-title">
												<xsl:value-of select="xslNsODExt:getDictRes('NameLengthExceedsDisplay_Full')"/>
											</xsl:attribute>
											<xsl:attribute name="data-qtip">
												<xsl:value-of select="$partyName"/>
											</xsl:attribute>
											<xsl:attribute name="data-tip">
												<xsl:value-of select="$partyName"/>
											</xsl:attribute>
										</xsl:if>
										<xsl:value-of select="$splitPartyName"/>
									</a>
								</td>
								<td>
									<xsl:choose>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='today'">
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 't')"/>
										</xsl:when>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='yesterday'">
											<xsl:text>Yesterday</xsl:text>
										</xsl:when>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='before'">
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 'd')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 'd')"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='searchBilling']" mode="content">
		<div id="{@id}" class="x-hidden">
			<div id="AccountSearchId">
				<div id="searchModeGroup">
					<div id="searchBillingLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('IWantToFindAccountBy')"/>
							<xsl:text> </xsl:text>
						</label>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">searchMode</xsl:with-param>
						<xsl:with-param name="name">_searchMode</xsl:with-param>
						<xsl:with-param name="type">select</xsl:with-param>
						<xsl:with-param name="optionlist">
							<xsl:for-each select="/page/content/CodeLists/CodeList[@ListName='BIL_ACCTSEARCHTYPE']/ListEntry">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="@Code"/>
									</xsl:attribute>
									<xsl:value-of select="@Description"/>
								</option>
							</xsl:for-each>
						</xsl:with-param>
						<xsl:with-param name="controlClass">dashboardsearchComboField</xsl:with-param>
						<xsl:with-param name="width">160</xsl:with-param>
					</xsl:call-template>
				</div>
				<div id="searchNumberGroup">
					<div id="searchNumberBillingLabel">
						<label>
							<xsl:value-of select="xslNsODExt:getDictRes('SearchingFor')"/>
							<xsl:text> </xsl:text>
						</label>
					</div>
					<xsl:call-template name="buildSystemControl">
						<xsl:with-param name="id">searchByNbr</xsl:with-param>
						<xsl:with-param name="name">_searchByNbr</xsl:with-param>
						<xsl:with-param name="controlClass">billingDashboardSearchTextField</xsl:with-param>
					</xsl:call-template>
				</div>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.billingQuickSearch(Ext.getCmp('searchByNbr').getValue(), Ext.getCmp('searchMode').getValue(), 'accountDashBoardSearch');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Search')"/>
					</xsl:with-param>
					<xsl:with-param name="name">
						<xsl:text>name_</xsl:text>
						<xsl:text>quickSearch</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="id">searchForAccountButton</xsl:with-param>
					<xsl:with-param name="type">search</xsl:with-param>
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='accountList']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:choose>
				<xsl:when test="/page/content/CandidateList/SearchControl/Paging/TotalCount='0'">
					<div class="noItems">
						<xsl:value-of select="xslNsODExt:getDictRes('NoRecentlyAccessedItems')"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<table id="moduleDisplayTable">
						<xsl:for-each select="/page/content/MRUObjects/MRUObject">
							<tr class="tblItem">
								<td>
									<a>
										<xsl:attribute name="name">accountLink</xsl:attribute>
										<xsl:attribute name="id">accountLinkId</xsl:attribute>
										<xsl:attribute name="class">accountLink</xsl:attribute>
										<xsl:attribute name="href">javascript:;</xsl:attribute>
										<xsl:attribute name="onclick">
											<xsl:text>DCT.Submit.openAccountSummary('accountSummary', '</xsl:text>
											<xsl:value-of select="ObjectId"/>
											<xsl:text>', 'accountLinkId');</xsl:text>
										</xsl:attribute>
										<xsl:value-of select="MRUObjectExtendedData/AccountReference"/>
										<xsl:text></xsl:text>
									</a>
								</td>
								<td>
									<xsl:value-of select="MRUObjectExtendedData/AccountName"/>
								</td>
								<td>
									<xsl:choose>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='today'">
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 't')"/>
										</xsl:when>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='yesterday'">
											<xsl:value-of select="xslNsODExt:getDictRes('Yesterday')"/>
										</xsl:when>
										<xsl:when test="xslNsExt:compareDate(/page/content/CurrentSystemDate/@date, AccessedDateTime)='before'">
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 'd')"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="xslNsExt:convertISOtoDisplay(AccessedDateTime, 'd')"/>
										</xsl:otherwise>
									</xsl:choose>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='reportList']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:choose>
				<xsl:when test="not(/page/content/Reports)">
					<div class="noItems">
						<xsl:value-of select="xslNsODExt:getDictRes('NoReportsAvailable')"/>
					</div>
				</xsl:when>
				<xsl:when test="/page/content/Reports/Error">
					<div class="noItems">
						<xsl:value-of select="/page/content/Reports/Error"/>
					</div>
				</xsl:when>
				<xsl:otherwise>
					<table id="moduleDisplayTable">
						<xsl:variable name="appCode" select="/page/content/Reports/@appCode"/>
						<xsl:for-each select="/page/content/Reports/Report">
							<tr class="tblItem">
								<td>
									<a>
										<xsl:attribute name="name">reportLink</xsl:attribute>
										<xsl:attribute name="id">reportLinkId</xsl:attribute>
										<xsl:attribute name="class">reportLink</xsl:attribute>
										<xsl:attribute name="href">javascript:;</xsl:attribute>
										<xsl:attribute name="onclick">
											<xsl:text>DCT.Submit.openReport('report', '</xsl:text>
											<xsl:value-of select="FullPath"/>
											<xsl:text>','','</xsl:text>
											<xsl:value-of select="$appCode"/>
											<xsl:text>','reportLinkId');</xsl:text>
										</xsl:attribute>
										<xsl:value-of select="Description"/>
									</a>
								</td>
							</tr>
						</xsl:for-each>
					</table>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='activityList']" mode="content">
		<div id="{@id}" class="x-hidden">
			<table id="moduleDisplayTable">
				<xsl:if test="$canPaymentSearch or $canAccessAllAreas">
					<tr class="tblItem">
						<td>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="name">searchForPayment</xsl:with-param>
								<xsl:with-param name="id">searchForPayment</xsl:with-param>
								<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('searchForPayment');</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:value-of select="xslNsODExt:getDictRes('SearchForAPayment')"/>
								</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="$canMakePaymentOutsideAccount or $canAccessAllAreas">
					<tr class="tblItem">
						<td>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="name">EnterASinglePayment</xsl:with-param>
								<xsl:with-param name="id">EnterASinglePayment</xsl:with-param>
								<xsl:with-param name="onclick">DCT.Submit.submitBillingPageAction('EnterASinglePayment', 'billingStartClean', false, "");</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:value-of select="xslNsODExt:getDictRes('EnterASinglePayment')"/>
								</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="$canAccessUnidentifiedPayments or $canAccessAllAreas">
					<tr class="tblItem">
						<td>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="name">unidentifiedPayments</xsl:with-param>
								<xsl:with-param name="id">unidentifiedPayments</xsl:with-param>
								<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('unidentifiedPayments');</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:value-of select="xslNsODExt:getDictRes('UnidentifiedPayments')"/>
								</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="$canCreateAccount or $canAccessAllAreas">
					<tr class="tblItem">
						<td>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="onclick">DCT.Submit.submitAction('billingStartClean:billingStart');</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:value-of select="xslNsODExt:getDictRes('CreateANewAccount')"/>
								</xsl:with-param>
								<xsl:with-param name="name">newAccount</xsl:with-param>
								<xsl:with-param name="id">newAccount</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="$canImportStatements or $canAccessAllAreas"> 
					<tr class="tblItem">
						<td>
							<xsl:call-template name="makeButton">
								<xsl:with-param name="onclick">DCT.Submit.submitBillingPage('statementImport');</xsl:with-param>
								<xsl:with-param name="caption">
									<xsl:value-of select="xslNsODExt:getDictRes('ImportStatements')"/>
								</xsl:with-param>
								<xsl:with-param name="name">importStatments</xsl:with-param>
								<xsl:with-param name="id">importStatments</xsl:with-param>
								<xsl:with-param name="type">hyperlink</xsl:with-param>
							</xsl:call-template>
						</td>
					</tr>
				</xsl:if>
			</table>
		</div>
	</xsl:template>
	<xsl:template match="module[@staticContentType!='']" mode="content">
		<div id="{@id}" class="x-hidden">
			<xsl:call-template name="buildExternalModulePanel"/>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='manuscriptWidget']" mode="content">
		<div id="{@id}" class="x-hidden">
			<div id="fields">
				<xsl:call-template name="buildCommonLayout">
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="module[@id='mruAccounts']" mode="content">
		<xsl:apply-templates />
	</xsl:template>
	<xsl:template match="module[@id='upcomingRenewals']" mode="content">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template name="buildDashboardHiddenFormFields">
		<input type="hidden" name="_manuscriptLOB" id="_manuscriptLOB" value=""/>
		<input type="hidden" name="_blurPostPageScope" id="_blurPostPageScope" value="fields"/>
		<input type="hidden" name="_blurPostHTMLContentPath" id="_blurPostHTMLContentPath" value="//div[@id='fields']"/>
	</xsl:template>
</xsl:stylesheet>