﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctApplication.xsl"/>
	<xsl:variable name="docMode">
		<xsl:if test="/page/content/getDocumentation">1</xsl:if>
		<xsl:if test="/page/content/getField">1</xsl:if>
	</xsl:variable>
	<!--*************************************************************************************************************
		layout mode doc defines the layout during documentation mode.
		************************************************************************************************************* -->
	<xsl:template match="section" mode="doc">
		<xsl:if test="@caption">
			<div>
				<xsl:attribute name="id">
					<xsl:choose>
						<xsl:when test="@identifier">
							<xsl:value-of select="@identifier"/>
						</xsl:when>
						<xsl:when test="@id">
							<xsl:value-of select="@id"/>
						</xsl:when>
						<xsl:when test="@uid">
							<xsl:value-of select="@uid"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>mainBody</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="class">
					<xsl:choose>
						<xsl:when test="@class">
							<xsl:value-of select="@class"/>
						</xsl:when>
						<xsl:when test="@classname">
							<xsl:value-of select="@classname"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="@direction='across'">
									<xsl:text>acrossLayout</xsl:text>
								</xsl:when>
								<xsl:when test="@direction='down'">
									<xsl:text>downLayout</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>defaultLayout</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<h2>
					<xsl:attribute name="name">
						<xsl:value-of select="@caption"/>
					</xsl:attribute>
					<xsl:value-of select="@caption"/>
				</h2>
			</div>
		</xsl:if>
		<div id="documentation">
			<xsl:apply-templates select="*" mode="doc"/>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		fieldInstance mode="doc"
		************************************************************************************************************* -->
	<xsl:template match="fieldInstance" mode="doc">
		<xsl:for-each select="child::*">
			<xsl:call-template name="fieldDetail"/>
		</xsl:for-each>
	</xsl:template>
	<!--	*************************************************************************************************************
		fieldDetail
		************************************************************************************************************* -->
	<xsl:template name="fieldDetail">
		<xsl:variable name="caption">
			<xsl:choose>
				<xsl:when test="@caption">
					<xsl:value-of select="@caption"/>
				</xsl:when>
				<xsl:when test="definition/caption/@value">
					<xsl:value-of select="definition/caption/@value"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@id"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<div id="helpInformation" class="downLayout">
			<div id="helpTitle">
				<h2>
					<xsl:attribute name="name">
						<xsl:value-of select="$caption"/>
					</xsl:attribute>
					<xsl:value-of select="$caption"/>
				</h2>
			</div>
			<xsl:choose>
				<xsl:when test="name()='table'">
					<xsl:apply-templates select="." mode="doc"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- Annotations -->
					<xsl:if test="annotations">
						<div id="annotations">
							<xsl:apply-templates select="annotations" mode="doc"/>
						</div>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="definition">
							<xsl:apply-templates select="definition" mode="doc"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="*" mode="doc"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<xsl:template match="definition" mode="doc">
		<xsl:apply-templates select="*" mode="doc"/>
	</xsl:template>
	<!--	*************************************************************************************************************
		maxLength
		************************************************************************************************************* -->
	<xsl:template match="maxLength" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('MaxLength')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		maxLength
		************************************************************************************************************* -->
	<xsl:template match="caption" mode="doc">
		<xsl:variable name="parentName">
			<xsl:for-each select="parent::*[1]">
				<xsl:value-of select="name()"/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:if test="$parentName='definition'">
			<div class="helpGroup">
				<div class="helpCaption">
					<xsl:value-of select="xslNsODExt:getDictRes('Caption')" />
				</div>
				<div class="helpContent">
					<xsl:call-template name="ruleType"/>
				</div>
			</div>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		formatMask
		************************************************************************************************************* -->
	<xsl:template match="formatMask" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('FormatMask')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		Applicable
		************************************************************************************************************* -->
	<xsl:template match="applicable" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Applicable')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		required
		************************************************************************************************************* -->
	<xsl:template match="required" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Required')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		readOnly
		************************************************************************************************************* -->
	<xsl:template match="readOnly" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('ReadOnly')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		value
		************************************************************************************************************* -->
	<xsl:template match="value" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		minimum
		************************************************************************************************************* -->
	<xsl:template match="minimum" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Minimum')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		maximum
		************************************************************************************************************* -->
	<xsl:template match="maximum" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Maximum')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		default
		************************************************************************************************************* -->
	<xsl:template match="default" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Default')" />
			</div>
			<div class="helpContent">
				<xsl:call-template name="ruleType"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		misc
		************************************************************************************************************* -->
	<xsl:template match="misc" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('MiscellaneousRules')" />
			</div>
			<div class="helpContent">
				<xsl:for-each select="*">
					<xsl:apply-templates select="." mode="doc"/>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		list
		************************************************************************************************************* -->
	<xsl:template name="ruleType">
		<xsl:choose>
			<xsl:when test="contains('default,minimum,maximum,value',name()) and @value and ../../*[@id]/descendant::options/option">
				<xsl:variable name="display">
					<xsl:value-of select="../../*[@id]/descendant::options/option[@value=current()/@value]/@caption"/>
				</xsl:variable>
				<xsl:value-of select="$display"/>
				<xsl:if test="@value!=$display">
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('OpenParen')" />
					<xsl:value-of select="@value"/>
					<xsl:value-of select="xslNsODExt:getDictRes('CloseParen')" />
				</xsl:if>
			</xsl:when>
			<xsl:when test="@idref or @value">
				<xsl:call-template name="idrefOrValue"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="*" mode="doc"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--*************************************************************************************************************
		select
		************************************************************************************************************* -->
	<xsl:template match="select" mode="doc">
		<xsl:value-of select="xslNsODExt:getDictRes('LookupOf')" />
		<xsl:text> </xsl:text>
		<em>
			<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
		</em>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('FromDefinedListFollowingKeys')" />
		<table id="selectkeys">
			<tr>
				<th>
					<xsl:value-of select="xslNsODExt:getDictRes('Key')" />
				</th>
				<th>
					<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
				</th>
			</tr>
			<tr>
				<td>
					<xsl:value-of select="xslNsODExt:getDictRes('Item')" />
				</td>
				<td>
					<xsl:call-template name="idrefOrValue"/>
				</td>
			</tr>
		</table>
		<table id="selecttable">
			<tr>
				<td>
					<xsl:value-of select="xslNsODExt:getDictRes('Item')" />
				</td>
				<td>
					<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
				</td>
			</tr>
			<xsl:for-each select="case">
				<tr>
					<td>
						<xsl:value-of select="@select"/>
					</td>
					<td>
						<xsl:call-template name="idrefOrValue"/>
					</td>
				</tr>
			</xsl:for-each>
			<tr>
				<td>
					<xsl:value-of select="xslNsODExt:getDictRes('Otherwise')" />
				</td>
				<td>
					<xsl:for-each select="otherwise">
						<xsl:call-template name="idrefOrValue"/>
					</xsl:for-each>
				</td>
			</tr>
		</table>
	</xsl:template>
	<!--	*************************************************************************************************************
		dataDifference
		************************************************************************************************************* -->
	<xsl:template match="dateDifference" mode="doc">
		<!--
	<dateDifference unit="years" ignoreYear="1" roundUp="1">
		<dateArgument idref="Applicant.CurrentResidenceDate"/>
		<dateArgument idref="Applicant.CurrentResidenceDate"/>
	</dateDifference>
-->
		<xsl:value-of select="xslNsODExt:getDictRes('DifferenceIn')" />
		<xsl:text> </xsl:text>
		<xsl:value-of select="@unit"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('Between')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="dateArgument[1]">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('And')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="dateArgument[2]">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:if test="@ignoreYear">
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsODExt:getDictRes('IgnoreYear')" />
		</xsl:if>
		<xsl:if test="@roundUp">
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsODExt:getDictRes('RoundUp_parens')" />
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		session
		************************************************************************************************************* -->
	<xsl:template match="session" mode="doc">
		<!--
	<session sessionValue="context"/>
-->
		<xsl:choose>
			<xsl:when test="@sessionValue='context'">
				<xsl:value-of select="xslNsODExt:getDictRes('TheCurrentContext')" />
			</xsl:when>
			<xsl:when test="@sessionValue='date'">
				<xsl:value-of select="xslNsODExt:getDictRes('TheCurrentDate')" />
			</xsl:when>
			<xsl:when test="@sessionValue='manuscript'">
				<xsl:value-of select="/page/content/getDocumentation/properties/@caption"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		subString
		************************************************************************************************************* -->
	<xsl:template match="subString" mode="doc">
		<!--
<subString>
	<sourceString idref="Applicant.Address2"/>
	<startPos idref="Applicant.Address2"/>
	<length idref="Applicant.Address2"/>
</subString>
-->
		<xsl:value-of select="xslNsODExt:getDictRes('IsAPortionOf')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="sourceString">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('StartingAtPosition')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="startPos">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('For')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="length">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('Characters')" />
		<xsl:text> </xsl:text>
	</xsl:template>
	<!--	*************************************************************************************************************
		external
		************************************************************************************************************* -->
	<xsl:template match="external" mode="doc">
		<!--
	<external path="foo">
		<manuscriptName idref="Applicant.Address1"/>
		<referenceField idref="Applicant.Address2"/>
	</external>
-->
		<xsl:value-of select="xslNsODExt:getDictRes('TheValueOf')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="referenceField">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('FromExternalSourceOf')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="manuscriptName">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
	</xsl:template>
	<!--	*************************************************************************************************************
		iterator
		************************************************************************************************************* -->
	<xsl:template match="iterator" mode="doc">
		<!--	
	<iterator type="float" scope="first" ="sum" idref="Vehicle">
		<reference idref="Vehicle.Premium" type="float"/>
		<where>
			<comparison compare="eq">
				<operand idref="Vehicle.Premium" type="float"/>
				<operand idref="Vehicle.Premium" type="float"/>
			</comparison>
		</where>
		<orderBy idref="BodilyInjury.BaseRate" type="int" order="ascending"/>
		<orderBy idref="BodilyInjury.BaseRate" type="int" order="descending"/>
	</iterator>
-->
		<xsl:value-of select="xslNsODExt:getDictRes('IterateThrough')" />
		<xsl:text> </xsl:text>
		<xsl:call-template name="scope">
			<xsl:with-param name="value">
				<xsl:value-of select="@scope"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('Of')" />
		<xsl:text> </xsl:text>
		<xsl:call-template name="idrefOrValue"/>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('ToFindThe')" />
		<xsl:text> </xsl:text>
		<xsl:call-template name="action">
			<xsl:with-param name="value">
				<xsl:value-of select="@action"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="@action != 'count' and @action != 'current'">
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsODExt:getDictRes('OfAll')" />
			<xsl:text> </xsl:text>
			<xsl:for-each select="reference">
				<xsl:call-template name="idrefOrValue"/>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="where">
			<xsl:value-of select="xslNsODExt:getDictRes('Where')" />
			<xsl:for-each select="*[1]">
				<xsl:call-template name="ruleType"/>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="orderBy">
			<xsl:value-of select="xslNsODExt:getDictRes('OrderBy')" />
			<div id="orderby">
				<xsl:for-each select="orderBy">
					<xsl:call-template name="idrefOrValue"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('In')" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="@order"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('Order_lowercase')" />
				</xsl:for-each>
			</div>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		scope
		************************************************************************************************************* -->
	<xsl:template name="scope">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="$value='all'">
				<xsl:value-of select="xslNsODExt:getDictRes('AllIterations')" />
			</xsl:when>
			<xsl:when test="$value='allOthers'">
				<xsl:value-of select="xslNsODExt:getDictRes('AllOtherIterations')" />
			</xsl:when>
			<xsl:when test="$value='first'">
				<xsl:value-of select="xslNsODExt:getDictRes('TheFirstIteration')" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		action
		************************************************************************************************************* -->
	<xsl:template name="action">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="$value='min'">
				<xsl:value-of select="xslNsODExt:getDictRes('Minimum_lowercase')" />
			</xsl:when>
			<xsl:when test="$value='max'">
				<xsl:value-of select="xslNsODExt:getDictRes('Maximum_lowercase')" />
			</xsl:when>
			<xsl:when test="$value='current'">
				<xsl:value-of select="xslNsODExt:getDictRes('CurrentIteration')" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$value"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		comparison
		************************************************************************************************************* -->
	<xsl:template match="comparison" mode="doc">
		<!--
	<comparison compare="eq">
	   <operand idref="PolicyInformation.Type" type="string" caption="Type" />
	   <operand type="string" value="N" />
	</comparison>
-->
		<xsl:apply-templates select="*[1]" mode="doc"/>
		<xsl:call-template name="compare">
			<xsl:with-param name="value">
				<xsl:value-of select="@compare"/>
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="*[2]" mode="doc"/>
	</xsl:template>
	<!--	*************************************************************************************************************
		compare
		************************************************************************************************************* -->
	<xsl:template name="compare">
		<xsl:param name="value"/>
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="$value='eq'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsEqualTo')" />
			</xsl:when>
			<xsl:when test="$value='lt'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsLessThan')" />
			</xsl:when>
			<xsl:when test="$value='gt'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsGreaterThan')" />
			</xsl:when>
			<xsl:when test="$value='le'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsLessThanOrEqualTo')" />
			</xsl:when>
			<xsl:when test="$value='ge'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsGreaterThanOrEqualTo')" />
			</xsl:when>
			<xsl:when test="$value='ne'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsNotEqualTo')" />
			</xsl:when>
			<xsl:when test="$value='is'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsOneOfTheFollowing')" />
			</xsl:when>
			<xsl:when test="$value='isnot'">
				<xsl:value-of select="xslNsODExt:getDictRes('IsNotOneOfTheFollowing')" />
			</xsl:when>
			<xsl:when test="$value='contains'">
				<xsl:value-of select="xslNsODExt:getDictRes('Contains')" />
			</xsl:when>
			<xsl:when test="$value='and'">
				<xsl:value-of select="xslNsODExt:getDictRes('And')" />
			</xsl:when>
			<xsl:when test="$value='or'">
				<xsl:value-of select="xslNsODExt:getDictRes('Or')" />
			</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
	</xsl:template>
	<!--	*************************************************************************************************************
		operand
		************************************************************************************************************* -->
	<xsl:template match="operand" mode="doc">
		<xsl:call-template name="idrefOrValue"/>
	</xsl:template>
	<!--	*************************************************************************************************************
		if
		************************************************************************************************************* -->
	<xsl:template match="if" mode="doc">
		<xsl:value-of select="xslNsODExt:getDictRes('When')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="condition">
			<xsl:call-template name="ruleType"/>
		</xsl:for-each>
		<xsl:if test="then">
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsODExt:getDictRes('ThenApply')" />
			<xsl:text> </xsl:text>
			<xsl:for-each select="then">
				<xsl:call-template name="ruleType"/>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="else">
			<xsl:text> </xsl:text>
			<xsl:value-of select="xslNsODExt:getDictRes('ElseApply')" />
			<xsl:text> </xsl:text>
			<xsl:for-each select="else">
				<xsl:call-template name="ruleType"/>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		argument
		************************************************************************************************************* -->
	<xsl:template match="argument" mode="doc">
		<!--
	<calculation>
	   <argument op="eq" idref="BodilyInjury.BaseRate" type="int" caption="Base Rate" />
	   <argument op="multiply" round="1" roundType="round">
	      <calculation>
	         <argument op="eq" idref="BodilyInjury.IncreasedLimitFactor" type="float" />
	         <argument op="add">
	            <calculation>
	               <argument op="eq">
	                  <calculation>
	                     <argument op="eq" idref="BodilyInjury.CSLPropertyDamageAddOn" type="float" />
	                     <argument op="multiply" round="1" roundType="round" idref="LiabFactors.ClassAndPAPFactor" type="float" />
	                  </calculation>
	               </argument>
	               <argument op="multiply" round="1" roundType="round" idref="PolicyInformation.TermFactor" type="float" />
	            </calculation>
	         </argument>
	         <argument op="multiply" round="1" roundType="round" idref="CreditsAndSurcharges.PaidInFullDiscount" type="string" caption="Paid In Full Discount" actionID="ai4" />
	      </calculation>
	   </argument>
	</calculation>
-->
		<tr>
			<!-- Assign Operator -->
			<td>
				<xsl:call-template name="operator">
					<xsl:with-param name="value">
						<xsl:value-of select="@op"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:call-template name="getOpenParens"/>
			</td>
			<td>
				<xsl:for-each select="descendant-or-self::argument[@idref|@value][1]">
					<xsl:call-template name="idrefOrValue"/>
				</xsl:for-each>
			</td>
			<td>
				<xsl:call-template name="round">
					<xsl:with-param name="value">
						<xsl:value-of select="@round"/>
					</xsl:with-param>
					<xsl:with-param name="type">
						<xsl:value-of select="@roundType"/>
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td>
				<xsl:call-template name="getCloseParens"/>
			</td>
		</tr>
		<!-- process any sub-calculation -->
		<xsl:if test="calculation">
			<xsl:apply-templates select="calculation" mode="doc"/>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		getOpenParens
		************************************************************************************************************* -->
	<!--
	<calculation>
	   <argument op="eq" idref="BodilyInjury.BaseRate" type="int" caption="Base Rate" />
	   <argument op="multiply" round="1" roundType="round">
	      <calculation>
	         <argument op="eq" idref="BodilyInjury.IncreasedLimitFactor" type="float" />
	         <argument op="add">
	            <calculation>
	               <argument op="eq">
	                  <calculation>
	                     <argument op="eq" idref="BodilyInjury.CSLPropertyDamageAddOn" type="float" />
	                     <argument op="multiply" round="1" roundType="round" idref="LiabFactors.ClassAndPAPFactor" type="float" />
	                  </calculation>
	               </argument>
	               <argument op="multiply" round="1" roundType="round" idref="PolicyInformation.TermFactor" type="float" />
	            </calculation>
	         </argument>
	         <argument op="multiply" round="1" roundType="round" idref="CreditsAndSurcharges.PaidInFullDiscount" type="string" caption="Paid In Full Discount" actionID="ai4" />
	      </calculation>
	   </argument>
	</calculation>
-->
	<xsl:template name="getOpenParens">
		<xsl:if test="calculation/argument">
			<xsl:value-of select="xslNsODExt:getDictRes('OpenParen')" />
			<xsl:for-each select="calculation/argument[1]">
				<xsl:call-template name="getOpenParens"/>
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		getCloseParens
		************************************************************************************************************* -->
	<!--
	<calculation>
	   <argument op="eq" idref="BodilyInjury.BaseRate" type="int" caption="Base Rate" />
	   <argument op="multiply" round="1" roundType="round">
	      <calculation>
	         <argument op="eq" idref="BodilyInjury.IncreasedLimitFactor" type="float" />
	         <argument op="add">
	            <calculation>
	               <argument op="eq">
	                  <calculation>
	                     <argument op="eq" idref="BodilyInjury.CSLPropertyDamageAddOn" type="float" />
	                     <argument op="multiply" round="1" roundType="round" idref="LiabFactors.ClassAndPAPFactor" type="float" />
	                  </calculation>
	               </argument>
	               <argument op="multiply" round="1" roundType="round" idref="PolicyInformation.TermFactor" type="float" />
	            </calculation>
	         </argument>
	         <argument op="multiply" round="1" roundType="round" idref="CreditsAndSurcharges.PaidInFullDiscount" type="string" caption="Paid In Full Discount" actionID="ai4" />
	      </calculation>
	   </argument>
	</calculation>
-->
	<xsl:template name="getCloseParens">
		<xsl:if test="not(following-sibling::*)">
			<xsl:for-each select="ancestor::argument/calculation">
				<xsl:value-of select="xslNsODExt:getDictRes('CloseParen')" />
			</xsl:for-each>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		calculation
		************************************************************************************************************* -->
	<xsl:template match="calculation" mode="doc">
		<!--
	<calculation>
	   <argument op="eq" idref="BodilyInjury.BaseRate" type="int" caption="Base Rate" />
	   <argument op="multiply" round="1" roundType="round">
	      <calculation>
	         <argument op="eq" idref="BodilyInjury.IncreasedLimitFactor" type="float" />
	         <argument op="add">
	            <calculation>
	               <argument op="eq">
	                  <calculation>
	                     <argument op="eq" idref="BodilyInjury.CSLPropertyDamageAddOn" type="float" />
	                     <argument op="multiply" round="1" roundType="round" idref="LiabFactors.ClassAndPAPFactor" type="float" />
	                  </calculation>
	               </argument>
	               <argument op="multiply" round="1" roundType="round" idref="PolicyInformation.TermFactor" type="float" />
	            </calculation>
	         </argument>
	         <argument op="multiply" round="1" roundType="round" idref="CreditsAndSurcharges.PaidInFullDiscount" type="string" caption="Paid In Full Discount" actionID="ai4" />
	      </calculation>
	   </argument>
	</calculation>
-->
		<xsl:choose>
			<xsl:when test="ancestor::calculation[1]">
				<xsl:for-each select="*">
					<xsl:if test="position()&gt;1 or (position()=1 and ./calculation)">
						<xsl:apply-templates select="." mode="doc"/>
					</xsl:if>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<table id="calculation">
					<tr>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Operator')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('OpenParen')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Round')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('CloseParen')" />
						</th>
					</tr>
					<xsl:apply-templates select="*" mode="doc"/>
				</table>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		operator
		************************************************************************************************************* -->
	<xsl:template name="operator">
		<xsl:param name="value"/>
		<xsl:choose>
			<xsl:when test="$value='eq'">
				<xsl:value-of select="xslNsODExt:getDictRes('Equal')" />
			</xsl:when>
			<xsl:when test="$value='ne'">
				<xsl:value-of select="xslNsODExt:getDictRes('NotEqual_Escaped')" disable-output-escaping="yes" />
			</xsl:when>
			<xsl:when test="$value='lt'">
				<xsl:value-of select="xslNsODExt:getDictRes('LessThan_Escaped')" disable-output-escaping="yes" />
			</xsl:when>
			<xsl:when test="$value='gt'">
				<xsl:value-of select="xslNsODExt:getDictRes('GreaterThan_Escaped')" disable-output-escaping="yes" />
			</xsl:when>
			<xsl:when test="$value='le'">
				<xsl:value-of select="xslNsODExt:getDictRes('LessThanEqualTo_Escaped')" disable-output-escaping="yes" />
			</xsl:when>
			<xsl:when test="$value='ge'">
				<xsl:value-of select="xslNsODExt:getDictRes('GreaterThanEqualTo_Escaped')" disable-output-escaping="yes" />
			</xsl:when>
			<xsl:when test="$value='add'">
				<xsl:value-of select="xslNsODExt:getDictRes('Plus')" />
			</xsl:when>
			<xsl:when test="$value='subtract'">
				<xsl:value-of select="xslNsODExt:getDictRes('Minus')" />
			</xsl:when>
			<xsl:when test="$value='multiply'">
				<xsl:value-of select="xslNsODExt:getDictRes('Multiply')" />
			</xsl:when>
			<xsl:when test="$value='divide'">
				<xsl:value-of select="xslNsODExt:getDictRes('Divide')" />
			</xsl:when>
			<xsl:when test="$value='min'">
				<xsl:value-of select="xslNsODExt:getDictRes('min')" />
			</xsl:when>
			<xsl:when test="$value='max'">
				<xsl:value-of select="xslNsODExt:getDictRes('max')" />
			</xsl:when>
			<xsl:when test="$value='and'">
				<xsl:value-of select="xslNsODExt:getDictRes('and')" />
			</xsl:when>
			<xsl:when test="$value='or'">
				<xsl:value-of select="xslNsODExt:getDictRes('or')" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		round
		************************************************************************************************************* -->
	<xsl:template name="round">
		<xsl:param name="value"/>
		<xsl:param name="type"/>
		<xsl:choose>
			<xsl:when test="$type='up'">
				<xsl:value-of select="xslNsODExt:getDictRes('RoundUp')" />
			</xsl:when>
			<xsl:when test="$type='down'">
				<xsl:value-of select="xslNsODExt:getDictRes('RoundDown')" />
			</xsl:when>
			<xsl:when test="$value='1' and $type='trunc'">
				<xsl:value-of select="xslNsODExt:getDictRes('Truncate')" />
			</xsl:when>
			<xsl:when test="$value='10' and $type='trunc'">
				<xsl:value-of select="xslNsODExt:getDictRes('Truncate10')" />
			</xsl:when>
			<xsl:when test="$value='100' and $type='trunc'">
				<xsl:value-of select="xslNsODExt:getDictRes('Truncate100')" />
			</xsl:when>
			<xsl:when test="$value='1000' and $type='trunc'">
				<xsl:value-of select="xslNsODExt:getDictRes('Truncate1000')" />
			</xsl:when>
			<xsl:when test="$value='1'">
				<xsl:value-of select="xslNsODExt:getDictRes('Dollar')" />
			</xsl:when>
			<xsl:when test="$value='2'">
				<xsl:value-of select="xslNsODExt:getDictRes('FiftyCents')" />
			</xsl:when>
			<xsl:when test="$value='4'">
				<xsl:value-of select="xslNsODExt:getDictRes('Quarter')" />
			</xsl:when>
			<xsl:when test="$value='5'">
				<xsl:value-of select="xslNsODExt:getDictRes('TwentyCents')" />
			</xsl:when>
			<xsl:when test="$value='10'">
				<xsl:value-of select="xslNsODExt:getDictRes('Dime')" />
			</xsl:when>
			<xsl:when test="$value='20'">
				<xsl:value-of select="xslNsODExt:getDictRes('Nickel')" />
			</xsl:when>
			<xsl:when test="$value='100'">
				<xsl:value-of select="xslNsODExt:getDictRes('Penny')" />
			</xsl:when>
			<xsl:when test="$value='1000'">
				<xsl:value-of select="xslNsODExt:getDictRes('ThreeDigits')" />
			</xsl:when>
			<xsl:when test="$value='10000'">
				<xsl:value-of select="xslNsODExt:getDictRes('FourDigits')" />
			</xsl:when>
			<xsl:when test="$value='100000'">
				<xsl:value-of select="xslNsODExt:getDictRes('FiveDigits')" />
			</xsl:when>
			<xsl:when test="$value='0.1'">
				<xsl:value-of select="xslNsODExt:getDictRes('Tens')" />
			</xsl:when>
			<xsl:when test="$value='0.01'">
				<xsl:value-of select="xslNsODExt:getDictRes('Hundreds')" />
			</xsl:when>
			<xsl:when test="$value='0.001'">
				<xsl:value-of select="xslNsODExt:getDictRes('Thousands')" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<!--	*************************************************************************************************************
		lookup
		************************************************************************************************************* -->
	<xsl:template match="lookup" mode="doc">
		<!--
	<lookup>
	   <tableRef idref="SomeField" value="BasePremiums" />
	   <fieldRef value="BI" />
	   <keyRef value="HardCodedValue" idref="VehicleInfo.CountyTest" type="int" name="Territory" />
	</lookup>
-->

		<xsl:value-of select="xslNsODExt:getDictRes('LookupOf')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="fieldRef">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:text> </xsl:text>
		<xsl:value-of select="xslNsODExt:getDictRes('From')" />
		<xsl:text> </xsl:text>
		<xsl:for-each select="tableRef">
			<xsl:call-template name="idrefOrValue"/>
		</xsl:for-each>
		<xsl:choose>
			<xsl:when test="keyRef">
				<xsl:text> </xsl:text>
				<xsl:value-of select="xslNsODExt:getDictRes('BasedOnFollowingKeys')" />
				<table id="keyref">
					<tr>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Key')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
						</th>
					</tr>
					<xsl:for-each select="keyRef">
						<tr>
							<td>
								<xsl:value-of select="@name"/>
							</td>
							<td>
								<xsl:call-template name="idrefOrValue"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</xsl:when>
		</xsl:choose>
		<xsl:for-each select="child::table">
			<xsl:apply-templates select="." mode="doc"/>
		</xsl:for-each>
	</xsl:template>
	<!--	*************************************************************************************************************
		table
		************************************************************************************************************* -->
	<xsl:template match="table" mode="doc">
		<table id="lookuptable">
			<xsl:if test="colKeys">
				<tr>
					<td>
						<xsl:value-of select="colKeys/@name"/>
					</td>
					<xsl:for-each select="colKeys/key">
						<td>
							<xsl:value-of select="@value"/>
						</td>
					</xsl:for-each>
				</tr>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="rowKeys/@pageKey='1'">
					<xsl:for-each select="rowKeys[@pageKey='1']/key">
						<xsl:variable name="pagePos">
							<xsl:value-of select="position()"/>
						</xsl:variable>
						<tr>
							<td>
								<xsl:value-of select="../@name"/>
							</td>
							<td>
								<xsl:value-of select="@value"/>
							</td>
						</tr>
						<tr>
							<xsl:choose>
								<xsl:when test="../../colKeys">
									<xsl:if test="../../rowKeys[not(@pageKey)]/key">
										<td>
											<xsl:value-of select="../../rowKeys[not(@pageKey)]/@name"/>
										</td>
									</xsl:if>
									<xsl:for-each select="../../colKeys/key">
										<xsl:for-each select="../../fields/field">
											<td>
												<xsl:value-of select="@name"/>
											</td>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="../../fields/field">
										<td>
											<xsl:value-of select="@name"/>
										</td>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</tr>
						<xsl:apply-templates select="../../rowKeys[not(@pageKey)]/key" mode="doc">
							<xsl:with-param name="pagePosition" select="$pagePos"/>
							<xsl:with-param name="rowAmount" select="count(../../rowKeys[not(@pageKey)]/key)"/>
						</xsl:apply-templates>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<tr>
						<td>
							<xsl:value-of select="rowKeys/@name"/>
						</td>
						<xsl:choose>
							<xsl:when test="colKeys">
								<xsl:for-each select="colKeys/key">
									<xsl:for-each select="../../fields/field">
										<td>
											<xsl:value-of select="@name"/>
										</td>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<xsl:for-each select="fields/field">
									<td>
										<xsl:value-of select="@name"/>
									</td>
								</xsl:for-each>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
					<xsl:for-each select="rowKeys/key">
						<xsl:variable name="pos">
							<xsl:value-of select="position()"/>
						</xsl:variable>
						<tr>
							<td>
								<xsl:value-of select="@value"/>
							</td>
							<td>
								<xsl:value-of disable-output-escaping="yes" select="xslNsExt:getRow(string(../../data/row[position()=$pos]/@value))"/>
							</td>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
		</table>
	</xsl:template>

	<xsl:template match="key" mode="doc">
		<xsl:param name="pagePosition"/>
		<xsl:param name="rowAmount"/>
		<xsl:variable name="pos">
			<xsl:value-of select="position()+(($pagePosition - 1)*$rowAmount)"/>
		</xsl:variable>
		<tr>
			<td>
				<xsl:value-of select="@value"/>
			</td>
			<td>
				<xsl:value-of disable-output-escaping="yes" select="xslNsExt:getRow(string(../../data/row[position()=$pos]/@value))"/>
			</td>
		</tr>
	</xsl:template>

	<!--	*************************************************************************************************************
		idrefOrValue
		************************************************************************************************************* -->
	<xsl:template name="idrefOrValue">
		<xsl:variable name="showVal">
			<xsl:choose>
				<xsl:when test="@caption">
					<xsl:value-of select="@caption"/>
				</xsl:when>
				<xsl:when test="@idref">
					<xsl:value-of select="@idref"/>
				</xsl:when>
				<xsl:when test="@value">
					<xsl:value-of select="@value"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="fieldID">
			<xsl:choose>
				<xsl:when test="name()='tableRef' and not(@idref)">
					<xsl:value-of select="@value"/>
				</xsl:when>
				<xsl:when test="@idref">
					<xsl:value-of select="@idref"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@value and name()!='tableRef'">
				<em>
					<xsl:value-of select="$showVal"/>
				</em>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$showVal"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="@actionID">
			<a onclick="DCT.Util.processFormSubmit('_{@actionID}')" onmousemove="window.status='';" onmouseout="window.status='';" id="action">
				<img src="{$imageDir}/page.bmp" border="0"/>
			</a>
		</xsl:if>
	</xsl:template>
	<!--	*************************************************************************************************************
		list
		************************************************************************************************************* -->
	<xsl:template name="linkOptions">
		<!--
	<options codeRef="Driver" captionRef="Driver.FirstNameLastName" />
-->
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('CodeRef')" />
			</div>
			<div class="helpContent">
				<xsl:value-of select="@codeRef"/>
			</div>
		</div>
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('CaptionRef')" />
			</div>
			<div class="helpContent">
				<xsl:value-of select="@captionRef"/>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		list
		************************************************************************************************************* -->
	<xsl:template match="options" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Options')" />
			</div>
			<div class="helpContent">
				<xsl:choose>
					<xsl:when test="@codeRef">
						<xsl:call-template name="linkOptions"/>
					</xsl:when>
					<xsl:when test="option">
						<xsl:for-each select="option">
							<xsl:value-of select="@caption"/>
							<xsl:if test="@value!=@caption">
								<xsl:text> </xsl:text>
								<xsl:value-of select="xslNsODExt:getDictRes('OpenParen')" />
								<xsl:value-of select="@value"/>
								<xsl:value-of select="xslNsODExt:getDictRes('CloseParen')" />
							</xsl:if>
							<xsl:if test="position()!=last()">
								<xsl:value-of select="xslNsODExt:getDictRes('Comma')" />
								<xsl:text> </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</xsl:when>
					<xsl:when test="dataService">
						<xsl:apply-templates select="dataService" mode="doc"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="xslNsODExt:getDictRes('ListContentsNotRecognized')" />
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		usedBy
		************************************************************************************************************* -->
	<xsl:template match="usedBy" mode="doc">
		<!--
		<usedBy>
		 <reference refID="BodilyInjury.Premium" caption="Bodily Injury Premium" actionID="ai1" />
		</usedBy>
-->
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('UsedBy')" />
			</div>
			<div class="helpContent">
				<xsl:for-each select="reference">
					<xsl:call-template name="idrefOrValue"/>
					<xsl:if test="position()!=last()">
						<xsl:value-of select="xslNsODExt:getDictRes('Comma')" />
						<xsl:text> </xsl:text>
					</xsl:if>
				</xsl:for-each>
			</div>
		</div>
	</xsl:template>
	<!--	*************************************************************************************************************
		dataService
		************************************************************************************************************* -->
	<!--
	<dataService serviceType="dataKey" value="GEO">
		<dataKey name="InitKey" value="GEO" />
	</dataService>
-->
	<xsl:template match="dataService" mode="doc">
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('ServiceType')" />
			</div>
			<div class="helpContent">
				<xsl:value-of select="@serviceType"/>
			</div>
		</div>
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
			</div>
			<div class="helpContent">
				<xsl:value-of select="@value"/>
			</div>
		</div>
		<div class="helpGroup">
			<div class="helpCaption">
				<xsl:value-of select="xslNsODExt:getDictRes('Keys')" />
			</div>
			<div class="helpContent">
				<table id="dataServiceData">
					<tr>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Name')" />
						</th>
						<th>
							<xsl:value-of select="xslNsODExt:getDictRes('Value')" />
						</th>
					</tr>
					<xsl:for-each select="*">
						<tr>
							<td>
								<xsl:value-of select="@name"/>
							</td>
							<td>
								<xsl:value-of select="@value"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>
