﻿<?xml version="1.0"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="contentInterviewPage.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>


	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="/page/content/getField">
				<xsl:call-template name="doPopup"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="buildContainer"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="processPageHeader">
		<xsl:call-template name="buidPageHeader">
			<xsl:with-param name="pageTitle">
				<xsl:choose>
					<xsl:when test="$docMode='1'">
						<xsl:value-of select="/page/content/getDocumentation/body/@caption"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:choose>
							<xsl:when test="normalize-space(/page/content/getPage/body/@caption)!=''">
								<xsl:value-of select="/page/content/getPage/body/@caption"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="/page/content/getPage/@page"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param>
			<xsl:with-param name="pageInstruction">
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

	<!--*************************************************************************************************************
		This template is for processing the interview response and producing the overall structure of the HTML page.
		************************************************************************************************************* -->
	<xsl:template name="buildPageContent">
		<input type="hidden" name="_PortfolioID" id="_PortfolioID" value="{page/content/@PortfolioID}"/>
		<input type="hidden" name="_UsingPortfolio" id="_usingPortfolio" value="0"/>
		<input type="hidden" name="_ClientID" id="_ClientID" value="{page/state/clientID}"/>
		<input type="hidden" name="_QuoteID" id="_QuoteID" value="{page/state/quoteID}"/>
		<input type="hidden" name="_taskId" id="_taskId" value="{/page/content/ExpressCache/ExtendedTaskContent/@taskId}"/>
		<input type="hidden" name="_SuppressAutoSave" id="_SuppressAutoSave" value="{/page/content/getPage/body/@suppressAutoSave}"/>
		<xsl:if test="/page/content/getPage/body/annotations">
			<a id="annotations" href="javascript:;">
				<!-- 11/12/08 pinkleju issue 101005: Inserted the below line to display page-level annotations. -->
				<xsl:value-of select="/page/content/getPage/body/annotations/annotation"/>
			</a>
		</xsl:if>
		<div id="information">
			<xsl:choose>
				<xsl:when test="$docMode='1'">
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="normalize-space(/page/state/documentPageSet) != ''">
						<a id="documentation" href="javascript:;">
							<xsl:attribute name="onclick">
								<xsl:text>DCT.Util.viewDocumentation()</xsl:text>
							</xsl:attribute>
							<xsl:value-of select="xslNsODExt:getDictRes('OnlineManual')" />
						</a>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</div>
		<xsl:if test="not($isPartyPage)">
			<xsl:if test="/page/content/getPage/body[group//fieldInstance[@required='1' and @readOnly='0']]">
				<div id="requiredTxt">
					<span class="required">
						<xsl:value-of select="xslNsODExt:getDictRes('ReqdFieldMarker')"/>
					</span>
					<xsl:text> </xsl:text>
					<xsl:value-of select="xslNsODExt:getDictRes('IndicatesRequiredField')"/>
				</div>
			</xsl:if>
		</xsl:if>
		<xsl:if test="/page/settings/Panels/TogglePanelButton/@showButton='1'">
			<div id="togglePanelButton" style="display:none">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="name">togglePanelSections</xsl:with-param>
					<xsl:with-param name="ID">togglePanelSections</xsl:with-param>
					<xsl:with-param name="class"></xsl:with-param>
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Sections.toggle();</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('ToggleSections')" />
					</xsl:with-param>
					<xsl:with-param name="type">hyperlink</xsl:with-param>
				</xsl:call-template>
			</div>
			<script>
				<xsl:text>Ext.onReady( function(){DCT.Sections.setToggleButton();})</xsl:text>
			</script>
		</xsl:if>
		<div id="body" class="{/page/content/*/body/@class}">
			<xsl:apply-templates select="/page/content/*/body"/>
		</div>
		<xsl:if test="/page/content/getPage/header//action">
			<!-- Need to check into -->
			<div class="g-actionheader">
				<xsl:for-each select="/page/content/getPage/header//action">
					<!-- Need to check into -->

					<xsl:call-template name="buildLink">
						<xsl:with-param name="name">
							<xsl:text>lineAction</xsl:text>
							<xsl:value-of select="@uid"/>
						</xsl:with-param>
						<xsl:with-param name="value" select="@caption"/>
						<xsl:with-param name="ignoreValidation" select=".//@ignoreValidation"/>
						<xsl:with-param name="cancelChanges" select=".//@cancelChanges"/>
						<xsl:with-param name="class" select="@actClass"/>
					</xsl:call-template>
				</xsl:for-each>
			</div>
		</xsl:if>
		<!-- Process any actions at the dialog level -->
		<xsl:if test="/page/content/getPage/actions[@type='page']/action">
			<div id="navControls" class="g-pageAction">
				<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@command='previous']">
					<xsl:variable name="buttonType">
						<xsl:choose>
							<xsl:when test="/page/content/getPage/body/@previousButtonType != ''">
								<xsl:value-of select="/page/content/getPage/body/@previousButtonType"/>
							</xsl:when>
							<xsl:when test="/page/settings/Buttons/PreviousButtonDefaultType != ''">
								<xsl:value-of select="/page/settings/Buttons/PreviousButtonDefaultType"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="/page/content/getPage/body/@prevButton">
							<xsl:if test="/page/content/getPage/body/@prevButton != ''">
								<div id="previous-div" class="previous">
									<xsl:call-template name="buildLink">
										<xsl:with-param name="name">previous-button</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="/page/content/getPage/body/@prevButton"/>
										</xsl:with-param>
										<xsl:with-param name="class">g-previous</xsl:with-param>
										<xsl:with-param name="id">g-previous</xsl:with-param>
										<xsl:with-param name="type" select="$buttonType"/>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<div id="previous-div" class="previous">
								<xsl:call-template name="buildLink">
									<xsl:with-param name="name">previous-button</xsl:with-param>
									<xsl:with-param name="class">g-previous</xsl:with-param>
									<xsl:with-param name="id">g-previous</xsl:with-param>
									<xsl:with-param name="type" select="$buttonType"/>
								</xsl:call-template>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<xsl:for-each select="/page/content/getPage/actions[@type='page']/action[@command='next']">
					<xsl:variable name="buttonType">
						<xsl:choose>
							<xsl:when test="/page/content/getPage/body/@nextButtonType != ''">
								<xsl:value-of select="/page/content/getPage/body/@nextButtonType"/>
							</xsl:when>
							<xsl:when test="/page/settings/Buttons/NextButtonDefaultType != ''">
								<xsl:value-of select="/page/settings/Buttons/NextButtonDefaultType"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<xsl:choose>
						<xsl:when test="/page/content/getPage/body/@nextButton">
							<xsl:if test="/page/content/getPage/body/@nextButton != ''">
								<div id="next-div" class="next">
									<xsl:call-template name="buildLink">
										<xsl:with-param name="name">next-button</xsl:with-param>
										<xsl:with-param name="value">
											<xsl:value-of select="/page/content/getPage/body/@nextButton"/>
										</xsl:with-param>
										<xsl:with-param name="class">g-next</xsl:with-param>
										<xsl:with-param name="id">g-next</xsl:with-param>
										<xsl:with-param name="type" select="$buttonType"/>
									</xsl:call-template>
								</div>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<div id="next-div" class="next">
								<xsl:call-template name="buildLink">
									<xsl:with-param name="name">next-button</xsl:with-param>
									<xsl:with-param name="class">g-next</xsl:with-param>
									<xsl:with-param name="id">g-next</xsl:with-param>
									<xsl:with-param name="type" select="$buttonType"/>
								</xsl:call-template>
							</div>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</div>
		</xsl:if>
		<xsl:apply-templates select="/page/content/getPage/panel"/>
	</xsl:template>
</xsl:stylesheet>