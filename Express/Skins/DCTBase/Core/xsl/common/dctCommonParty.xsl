﻿
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctCommonControls.xsl"/>
	<!--*************************************************************************************************************
		Common control templates
		************************************************************************************************************* -->
	<!--*************************************************************************************************************
		Party Templates
		************************************************************************************************************* -->
	<xsl:template name="buildSelectedPartyContent">
		<xsl:param name="selectedTitle"/>
		<xsl:variable name="phoneValue">
			<xsl:value-of select="/page/content/ExpressCache/SelectedParty/PhoneNumber"/>
		</xsl:variable>
		<xsl:variable name="addressLine2Value">
			<xsl:value-of select="/page/content/ExpressCache/SelectedParty/LocationAddressLine2"/>
		</xsl:variable>
		<xsl:variable name="phoneExtValue">
			<xsl:value-of select="/page/content/ExpressCache/SelectedParty/PhoneExtenstion"/>
		</xsl:variable>
		<div id="selectedPartyContent">
			<div id="selectedPartyTitle">
				<h2>
					<xsl:value-of select="$selectedTitle"/>
					<xsl:text> </xsl:text>
					<xsl:value-of select="/page/content/ExpressCache/SelectedParty/FullName"/>
				</h2>
			</div>
		</div>
		<div id="selectedPartyDetail">
			<table class="formTbl">
				<tr>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('PhoneNumberExt')"/>
					</td>
					<td>
						<div>
							<span id="phoneNumber">
								<xsl:value-of select="xslNsExt:cultureAwarePhoneFormatter($phoneValue)"/>
								<xsl:if test="$phoneExtValue != ''">
									<xsl:text>(</xsl:text>
									<xsl:value-of select="/page/content/ExpressCache/SelectedParty/PhoneExtenstion"/>
									<xsl:text>)</xsl:text>
								</xsl:if>
							</span>
						</div>
					</td>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Address')"/>
					</td>
					<td>
						<div>
							<span id="address1">
								<xsl:value-of select="/page/content/ExpressCache/SelectedParty/LocationAddressLine1"/>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Email')"/>
					</td>
					<td>
						<div>
							<span id="email">
								<xsl:value-of select="/page/content/ExpressCache/SelectedParty/EmailAddress"/>
							</span>
						</div>
					</td>
					<td class="detailsLabel">
						<label>
						</label>
					</td>
					<td>
						<div>
							<span id="address2">
								<xsl:choose>
									<xsl:when test="$addressLine2Value != ''">
										<xsl:value-of select="$addressLine2Value"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="/page/content/ExpressCache/SelectedParty/CityStateZipDisplay"/>
									</xsl:otherwise>
								</xsl:choose>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="detailsLabel">
						<xsl:value-of select="xslNsODExt:getDictRes('Contact')"/>
					</td>
					<td>
						<div>
							<span id="contact">
							</span>
						</div>
					</td>
					<td class="detailsLabel">
						<label>
						</label>
					</td>
					<td>
						<div>
							<span id="citystatezip">
								<xsl:choose>
									<xsl:when test="$addressLine2Value != ''">
										<xsl:value-of select="/page/content/ExpressCache/SelectedParty/CityStateZipDisplay"/>
									</xsl:when>
									<xsl:otherwise></xsl:otherwise>
								</xsl:choose>
							</span>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>