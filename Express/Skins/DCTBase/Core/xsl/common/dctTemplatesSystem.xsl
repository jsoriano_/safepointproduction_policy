﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xslNsExt="urn:xslExtensions" xmlns:xslNsODExt="urn:xslOnDemandExtensions" xmlns:ext="urn:ext">
	<xsl:import href="dctTemplatesCommon.xsl"/>
	<xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

	<!--*************************************************************************************************************
		Build System Page ExtJs Controls
		************************************************************************************************************* -->
	<xsl:template name="buildSystemControl">
		<xsl:param name="class"/>
		<xsl:param name="type">text</xsl:param>
		<xsl:param name="name"/>
		<xsl:param name="id"/>
		<xsl:param name="size"/>
		<xsl:param name="captions"/>
		<xsl:param name="value"/>
		<xsl:param name="selected"/>
		<xsl:param name="disabled"/>
		<xsl:param name="maxlength"/>
		<xsl:param name="optionlist"/>
		<xsl:param name="dateFormat"/>
		<xsl:param name="rows"/>
		<xsl:param name="readonly"/>
		<xsl:param name="removeEscape"/>
		<xsl:param name="required"/>
		<xsl:param name="controlClass"/>
		<xsl:param name="width"/>
		<xsl:param name="defaultValue"/>
		<xsl:param name="radiobuttons"/>
		<xsl:param name="vertical"/>
		<xsl:param name="columns"/>
		<xsl:param name="checkbox"/>
		<xsl:param name="jSFormatter">string</xsl:param>
		<xsl:param name="extraConfigItems"/>
		<xsl:param name="watermark"/>
		<div class="controlContainer">
			<xsl:choose>
				<xsl:when test="$type='select'">
					<xsl:call-template name="buildSelectDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="required" select="$required"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="value" select="$value"/>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemComboField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="interviewItems">false</xsl:with-param>
						<xsl:with-param name="optionList">
							<xsl:copy-of select="$optionlist"/>
						</xsl:with-param>
						<xsl:with-param name="elementClass">
							<xsl:value-of select="$class"/>
						</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readonly"/>
						<xsl:with-param name="disabled" select="$disabled"/>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="defaultValue" select="$defaultValue"/>
						<xsl:with-param name="watermark" select="$watermark"/>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$type='textarea'">
					<xsl:call-template name="buildTextAreaDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="required" select="$required"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="value">
							<xsl:choose>
								<xsl:when test="$removeEscape='1'">
									<xsl:value-of disable-output-escaping="yes" select="$value"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$value"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemTextAreaField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="interviewItems">false</xsl:with-param>
						<xsl:with-param name="elementClass">
							<xsl:value-of select="$class"/>
						</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readonly"/>
						<xsl:with-param name="disabled" select="$disabled"/>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="rows" select="$rows"/>
						<xsl:with-param name="defaultValue" select="$defaultValue"/>
						<xsl:with-param name="maxLength" select="$maxlength"/>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$type='radio'">
					<xsl:call-template name="buildRadioDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="required" select="$required"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="value" select="$value"/>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemRadioGroup</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="interviewItems">false</xsl:with-param>
						<xsl:with-param name="elementClass">
							<xsl:value-of select="$class"/>
						</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readonly"/>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="vertical" select="$vertical"/>
						<xsl:with-param name="columns" select="$columns"/>
						<xsl:with-param name="defaultValue" select="$defaultValue"/>
						<xsl:with-param name="radiobuttons" select="$radiobuttons"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$type='checkbox'">
					<xsl:call-template name="buildCheckBoxDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="required" select="$required"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="value" select="$value"/>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>interviewCheckBoxGroup</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="interviewItems">false</xsl:with-param>
						<xsl:with-param name="elementClass">
							<xsl:value-of select="$class"/>
						</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readonly"/>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="defaultValue" select="$defaultValue"/>
						<xsl:with-param name="checkbox" select="$checkbox"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="$type='display'">
					<xsl:call-template name="buildReadOnlyDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="value">
							<xsl:value-of select="$value"/>
						</xsl:with-param>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemDisplayField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="buildInputDiv">
						<xsl:with-param name="fieldID" select="$id"/>
						<xsl:with-param name="required" select="$required"/>
						<xsl:with-param name="name" select="$name"/>
						<xsl:with-param name="value">
							<xsl:value-of select="$value"/>
						</xsl:with-param>
						<xsl:with-param name="class">
							<xsl:choose>
								<xsl:when test="normalize-space($controlClass)!=''">
									<xsl:value-of select="$controlClass"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>systemTextField</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="interviewItems">false</xsl:with-param>
						<xsl:with-param name="elementClass">
							<xsl:value-of select="$class"/>
						</xsl:with-param>
						<xsl:with-param name="readOnly" select="$readonly"/>
						<xsl:with-param name="disabled" select="$disabled"/>
						<xsl:with-param name="width" select="$width"/>
						<xsl:with-param name="defaultValue" select="$defaultValue"/>
						<xsl:with-param name="maxLength" select="$maxlength"/>
						<xsl:with-param name="size">
							<xsl:choose>
								<xsl:when test="normalize-space($size)!=''">
									<xsl:value-of select="$size"/>
								</xsl:when>
								<xsl:when test="$type='date'">
									<xsl:text>16</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>20</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
						<xsl:with-param name="dateFormat" select="$dateFormat"/>
						<xsl:with-param name="jSFormatter" select="$jSFormatter"/>
						<xsl:with-param name="extraConfigItems" select="$extraConfigItems"/>
						<xsl:with-param name="type">
							<xsl:choose>
								<xsl:when test="$type='date'">
									<xsl:text>text</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$type"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>
	<!--*************************************************************************************************************
		Build Hidden Input control
		************************************************************************************************************* -->
	<xsl:template name="buildHiddenInput">
		<xsl:param name="name"/>
		<xsl:param name="id"/>
		<xsl:param name="value"/>
		<input>
			<xsl:if test="normalize-space($id)!=''">
				<xsl:attribute name="id">
					<xsl:value-of select="$id"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="type">
				<xsl:text>hidden</xsl:text>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="$name"/>
			</xsl:attribute>
			<xsl:attribute name="value">
				<xsl:value-of select="$value"/>
			</xsl:attribute>
		</input>
	</xsl:template>
	<!--*************************************************************************************************************
		Error widget Tag
		************************************************************************************************************* -->
	<xsl:template name="errors">
		<input type="hidden" name="_QuoteID" id="_QuoteID">
			<xsl:attribute name="value">
				<xsl:value-of select="/page/state/quoteID"/>
			</xsl:attribute>
		</input>
		<div id="errorTextGroup">
			<xsl:for-each select="/page/content//errors/error">
				<p class="errorText">
					<xsl:value-of select="text()"/>
				</p>
			</xsl:for-each>
		</div>
		<xsl:choose>
			<xsl:when test="error[@code='640']">
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.actOnQuote('load','</xsl:text>
						<xsl:value-of select="/page/state/quoteID"/>
						<xsl:text>')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('Reload')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.submitPage('home');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('HomePage')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.submitAction('resetSession');</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('LoginPage')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="ErrorString">
					<xsl:for-each select="/page/content/errors/error">
						<xsl:value-of select="text()"/>
						<xsl:text>.  </xsl:text>
					</xsl:for-each>
				</xsl:variable>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="href">
						<xsl:text disable-output-escaping="yes">javascript:window.open('mailto:</xsl:text>
						<xsl:value-of disable-output-escaping="yes" select="xslNsExt:getConfigSetting('MailTo')"/>
						<xsl:text disable-output-escaping="yes">?subject=Error%20Report&amp;body=</xsl:text>
						<xsl:value-of select="$ErrorString"/>
						<xsl:text>')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('SubmitErrorReport')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">
						<xsl:text>DCT.Submit.submitPage('</xsl:text>
						<xsl:value-of select="/page/content/@page"/>
						<xsl:text>')</xsl:text>
					</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('GoBack')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">DCT.Submit.submitPage('home');</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('HomePage')"/>
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="makeButton">
					<xsl:with-param name="onclick">DCT.Submit.submitAction('resetSession');</xsl:with-param>
					<xsl:with-param name="caption">
						<xsl:value-of select="xslNsODExt:getDictRes('LoginPage')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>